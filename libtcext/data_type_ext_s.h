static const char *user_defined_data_type_array[] = {
		"MSKFFF8",
		"MSKFFF0",
		"MSKFFFC",
		"MSKFF0F",
		"MSKF800",
		"MAX_USER_DEFINED_DATA_TYPE_ENUMS"
};

static const int user_defined_data_type_array_elements =
    sizeof(user_defined_data_type_array) / sizeof(char *);
