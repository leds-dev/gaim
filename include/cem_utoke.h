/*  CEM directive tokens. */

%token	CEM				/* %keyword CEM */
%token	ACK				/* %keyword ACK */
%token	_BYPASS				/* %keyword BYPASS */
%token	RELEASE				/* %keyword RELEASE */
