/*
@(#) gbaccs gbaccs include/avctrl_mon_utoke.h 1.1 99/12/03 16:51:02
*/
/*
@(#) gbaccs gbaccs include/avctrl_mon_utoke.h 1.1 99/08/13 12:37:04
*/
/*
\%Z\% $database $group $folder\%M\% \%I\% \%E\% \%U\%
*/
/*  AVCTRL_MON directive tokens. */

%token	AVCTRL_MON			/* %keyword AVCTRL_MON */
%token	CONFIG				/* %keyword CONFIG */
%token	CMDSRC				/* %keyword CMDSRC */
%token	CMDDEST				/* %keyword CMDDEST */
%token	TLMSRC				/* %keyword TLMSRC */
%token	TLMDEST				/* %keyword TLMDEST */
%token	SCID				/* %keyword SCID */
%token	DESKTOP				/* %keyword DESKTOP */
%token	STATIONID			/* %keyword STATIONID */
%token	ENABLEALLSTREAMS		/* %keyword ENABLEALLSTREAMS */
%token	DISABLEALLSTREAMS		/* %keyword DISABLEALLSTREAMS */
%token	RANGE				/* %keyword RANGE */
%token	DSNMON				/* %keyword DSNMON */
%token  AV				/* %keyword AV */
%token  UNAV				/* %keyword UNAV */
