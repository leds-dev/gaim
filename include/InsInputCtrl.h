/*
\%Z\% $database $group $folder\%M\% \%I\% \%E\% \%U\%
*/
/*******************************************************************************

	Purpose:

		Public data declarations for INSNEC-CORTEX input control

	Origin:

		Written by Eddy Shen, Integral Systems, Inc.

	History:

		971022CCS   Created

*******************************************************************************/
#ifndef INSINPUTCTRL_H
#define INSINPUTCTRL_H


/* Class to control input from the socket that device attached to */
typedef	void (*INPUT_CB)(const unsigned char *, int, void *);
	/* User can specify inputCB callback function for activating */
	/* callback while receiving socket input (in ReadData()) from */
	/* IN-SNEC device. */
	/* Function passing buffer, nbytes, and userdata (came from user) */
	/* 3 arguments. */
typedef void (*IFLOWID_CB)(int, void *);
	/* User can specify iflowidCB callback function that will */
	/* invoke after received 'iflowid' field but before swapping the */
	/* data (swap if necessary). It is useful if IN-SNEC */
	/* input is char or byte orientation. */
	/* For example: Don't try swap the 4 bytes data when it is telemetry. */
	/* Function passing iflowid and conID two arguments. */



typedef struct InsInputCtrl {
	void *userdata; /* Anything: Upto application */
	INPUT_CB input_CB; /* Input callback from IN-SNEC socket */
	IFLOWID_CB iflowid_CB; /* user special callback when receiving */
	                       /* input flow_id field (optional) from */
	                       /* IN-SNEC socket */
} InsInputCtrl;

#endif
