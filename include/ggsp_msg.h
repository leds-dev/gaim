/*******************************************************************************

        Purpose:

                Declarations for the ggsp processes.

        Origin:

                Written by Karen Cumming, Integral Systems, Inc.

        History:

                000320KEC   Created

*******************************************************************************/
#ifndef ggsp_msg_h_DEFINED
#define ggsp_msg_h_DEFINED

#include "db_util.h"

#define ID_MSG		 1
#define ALIVE_MSG      	 2
#define PRIME_MSG        3
#define PRIMARY_DATA_MSG 4
#define LOST_STREAM_MSG  5
#define TURN_OFF_MSG     6
#define TLM_SRC_CHG	 7
#define MAN_START        8
#define MAN_STOP         9

#define NAMELEN	   12
#define MAX_SEQ_NO 65535
#define FIRST_SAT  13
#define NUM_SATS   8

typedef struct gv_value {
        char    name[MNEMONIC_SIZE];
        unsigned int    status;
        union {
                long ivalue;
                double dvalue;
                char svalue[STRING_VAR_SIZE];
                struct timeval tvalue;
        } uv;
} gv_value;

typedef struct prime_value {
        int  sat_index;
	char host[STRING_VAR_SIZE];
	char stream[STRING_VAR_SIZE];
	char output[STRING_VAR_SIZE];
	char user[STRING_VAR_SIZE];
	char avs_src[STRING_VAR_SIZE];
	char oats_src[STRING_VAR_SIZE];
	char nrm_src[STRING_VAR_SIZE];
	char dwl_src[STRING_VAR_SIZE];
	char sxi_src[STRING_VAR_SIZE];
	char wb_src[STRING_VAR_SIZE];

} prime_value;

#define GGSP_BODY  (NUM_SATS * sizeof(prime_value)) 

typedef struct {
	unsigned short	seq_no;
	unsigned short	length;
	unsigned short	msg_type;
	unsigned short	points;
	char		source[NAMELEN];
	char		destination[NAMELEN];
}	ggsp_header;

typedef union {
		char		string[GGSP_BODY];
		short		word[GGSP_BODY / 2];
		int		dword[GGSP_BODY / 4];
		float		fword[GGSP_BODY / 4];
		gv_value	gv[GGSP_BODY / sizeof(gv_value)];
		prime_value	pv[GGSP_BODY / sizeof(prime_value)];
} ggsp_body;


typedef struct {
	ggsp_header	hdr;
	ggsp_body	body;
	
} ggsp_msg_struct;

typedef union {
        ggsp_msg_struct       str;
        unsigned char         text[GGSP_BODY + sizeof(ggsp_header)];
}       ggsp_msg;


#endif /* ggsp_msg_h_DEFINED */
