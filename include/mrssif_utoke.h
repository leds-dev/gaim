/*  MRSS directive tokens. */

%token	ADS				/* %keyword ADS */
%token	APID				/* %keyword APID */
%token	AV				/* %keyword AV */
%token	AVS				/* %keyword AVS */
%token	COMP				/* %keyword COMP */
%token	CYC				/* %keyword CYC */
%token	EVENTS				/* %keyword EVENTS */
%token	FILE				/* %keyword FILE */
%token	FIX				/* %keyword FIX */
%token	GETSTAT				/* %keyword GETSTAT */
%token	HEALTH				/* %keyword HEALTH */
%token	HK				/* %keyword HK */
%token	IMGSUM				/* %keyword IMGSUM */
%token	LOG				/* %keyword LOG */
%token	MRSS				/* %keyword MRSS */
%token	OBS				/* %keyword OBS */
%token	PCM1				/* %keyword PCM1 */
%token	PCM2				/* %keyword PCM2 */
%token	RATE				/* %keyword RATE */
%token	SEQ				/* %keyword SEQ */
%token	SXIDMP				/* %keyword SXIDMP */
%token	TEXT				/* %keyword TEXT */
%token	UNAV				/* %keyword UNAV */
