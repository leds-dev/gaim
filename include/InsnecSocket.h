/*
\%Z\% $database $group $folder\%M\% \%I\% \%E\% \%U\%
*/
/*******************************************************************************

	Purpose:

		Public data declarations for INSNEC-CORTEX socket

	Origin:

		Derived from shin1, version 1.3,
		Written by Eddy Shen, Integral Systems, Inc.

	History:

		981102CCS   Created

*******************************************************************************/
#ifndef INSNECSOCKET_H
#define INSNECSOCEKT_H

#include <time.h>
#include <sys/types.h>

#include "InsInputCtrl.h"

typedef	int (*WAITON_CB)(const unsigned char *, int, void *);
typedef void (*CONNECT_CB)(void *);
typedef void (*DISCONNECT_CB)(void *);

#define INSNEC_MSG_SIZE	1024
#define INSNEC_TYPENAME_SIZE	32
#define INSNEC_RECONNECT_TIME	16
#define MSG_ELAPSE_TIME	16
#define MSG_READ_TIMEOUT	5
#define	MAX_HOST_SIZE	40
#define	MAX_SERVICE_SIZE	40

#define ERR_NO_WAITCB -200
#define ERR_OPEN_SOCKET -201
#define ERR_WAIT_TIMEOUT -202
#define ERR_NO_MATCH_DEV -203
#define ERR_OUT_OF_MEMORY -204
#define ERR_MACHINE_OFFLINE -205
#define ERR_USER_DEFINE1 -206
#define ERR_USER_DEFINE2 -207
#define ERR_EXCEED_MAX_CONN -208
#define ERR_USER_OFF -209
#define ERR_GEN_FAIL -1

enum INSNEC_OPTS
{
	AUTO_RECONNECT = 0x01,
	ADD_CHECKSUM = 0x02,
/* data format */
	INPUT_BYTE_ORIENT = 0x04,
	OUTPUT_BYTE_ORIENT = 0x08,
	TIMER_IS_RUNNING = 0x010
};

union INT_CHAR {
	unsigned int iv;
	char cv[4];
};


typedef struct InsnecSocket {
	int fd;                    /* socket # (-1 if not currently connected) */
	char name[MAX_HOST_SIZE+MAX_SERVICE_SIZE];
	char host[MAX_HOST_SIZE];
	char service[MAX_SERVICE_SIZE];
	unsigned char	*buffer;/* Read and write buffer */
	size_t	size;	/* size of buffer been allocated */
	int	oflowid;/* Flow identification for building header to IN-SNEC */
	int	iflowid;/* Flow identification while extract from IN-SENC */

	int	opts;	/* see INSNEC_OPTS */
			/* default: both input and output byte order are */
			/* integer 4-bytes format (do care about MSB and LSB).  */
			/* i.e. neither INPUT_BYTE_ORIENT nor OUTPUT_BYTE_ORIENT */
	int	rtimeout;	/* Number of seconds for timeout on response */
	time_t	last_connected;	/* Last connected time */
	time_t	last_msg_time;	/* Last message time */
	int	elapse_time;	/* close the socket if 'last_msg_time' */
				/* is exceed this 'elapsed' time. */
				/* 0: don't apply the close rule on this socket. */

/* For input only */
	InsInputCtrl	iic;

/* For waitON */
	void *waitondata; /* Anything: Upto application */
	WAITON_CB waiton_CB; /* Input waiting callback from IN-SNEC socket */

/* For fd connected callback */
	void *connect_data;
	CONNECT_CB connect_CB;
/* For fd disconnected callback */
	void *disconnect_data;
	DISCONNECT_CB disconnect_CB;

} InsnecSocket;



InsnecSocket *InsnecSocketAlloc(const char *, int);
int InsnecSocketConnect(InsnecSocket *);
int InsnecSocketWrite(InsnecSocket *, const unsigned char *, const size_t);
int InsnecSocketRead(InsnecSocket *, int);
int InsnecSocketReadData(InsnecSocket *);
int InsnecSocketWaitOn(InsnecSocket *);
void InsnecSocketDoConnect(InsnecSocket *);

InsnecSocket *FindInsnecSocket(char *);
InsnecSocket *InsnecSocketInit(char *, int, int);
fd_set InsnecSelectMask();
void AppendInsnecSelectMask(fd_set *);
int InsnecMonitorSelect(fd_set *);
int InsnecReadFDs(fd_set *);
void SetInsnecInputCB(INPUT_CB);
void InsnecAddOpts(int flags);
int MonitorAllComm(fd_set *start_mask);
void InsnecSocketClose(void);

#endif
