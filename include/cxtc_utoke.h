/*
@(#) $database $group include/cxtc_utoke.h 1.2 00/03/06 19:13:03
*/
/*  CXTC directive tokens. */

%token	CXTC				/* %keyword CXTC */
%token	TCU_UNLOCK			/* %keyword TCU_UNLOCK */
%token	NOP				/* %keyword NOP */
%token	STOP_IDLING			/* %keyword STOP_IDLING */
%token	PSEUDO_EARTH			/* %keyword PSEUDO_EARTH */
%token	CARRIER_SWEEP			/* %keyword CARRIER_SWEEP */
