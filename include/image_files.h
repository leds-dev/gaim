/*******************************************************************************

        Purpose:

                Declarations for image file processing.

        Origin:

                Written by Karen Cumming, Integral Systems, Inc.

        History:

                020327KEC   Created

*******************************************************************************/
#ifndef image_files_h_DEFINED
#define image_files_h_DEFINED

#define IMAGE_VERSION        		 1

#define IMG_HEAD_SIZE      	       512
#define MAX_ADDR_STATE      		16
#define MAX_PAGE_REG        		16
#define MAX_ACE_PAGES     	      0x9F
#define MAX_ACE_LOGICAL_ADDR	   0xFFFFF
#define MAX_SXI_TABLES      		64
#define MAX_SXI_TBL_SIZE 	     65536
#define MIN_SXI_LOGICAL_ADDR 	0x01000000
#define SXI_SCRATCH_TABLE	        47

#define ACE_MEM_SIZE    	    655360
#define SXI_MEM_SIZE   	           4194304
#define MAX_MEM_SIZE   	           4194304

#define IMG_ERR_SUCCESS			 0
#define IMG_ERR_BAD_PARAM		-1
#define IMG_ERR_OPEN_FAILED		-2
#define IMG_ERR_LSEEK_FAILED		-3
#define IMG_ERR_WRITE_FAILED		-4
#define IMG_ERR_MMAP_FAILED		-5
#define IMG_ERR_FSTAT_FAILED		-6
#define IMG_ERR_MSYNC_FAILED		-7
#define IMG_ERR_NONEXIST_FILE		-8
#define IMG_ERR_VERSION_MISMATCH	-9
#define IMG_ERR_FILE_SIZE_ERR	       -10
#define IMG_ERR_UNK_IMAGE_TYPE	       -11
#define IMG_ERR_INV_RECORD_COUNT       -12
#define IMG_ERR_NO_CONV_TABLE	       -13
#define IMG_ERR_TYPE_MISMATCH	       -14
#define IMG_ERR_IMAGE_NOT_SXI	       -15
#define IMG_ERR_SXI_SCRATCH	       -16
#define IMG_ERR_SXI_TABLE_ERR	       -17
#define IMG_ERR_ADDR_TO_LARGE	       -18
#define IMG_ERR_IMAGE_NOT_MAPPED       -19
#define IMG_ERR_NO_CFG_FILE	       -20
#define IMG_ERR_CFG_FORMAT	       -21
#define IMG_ERR_CFG_TBL_NUM	       -22
#define IMG_ERR_CFG_TBL_SIZE	       -23
#define IMG_ERR_CFG_BASE_ADDR	       -24
#define IMG_ERR_FILE_ALREADY_OPEN      -25
#define IMG_ERR_INV_ACE_PAGE	       -26
#define IMG_ERR_ACE_CFG_SHORT	       -27
#define IMG_ERR_ADDR_IND_ERR	       -27
#define IMG_ERR_LAST_MSG	        29

typedef enum image_type {
	ACE1_IMAGE,
	ACE2_IMAGE,
	SXI_IMAGE,
	MAX_IMAGES
} image_type;

typedef enum file_type {
	CGI_IMAGE,
	TGI_IMAGE,
	PDI_IMAGE,
	MAX_FILE_TYPES
} file_type;

typedef enum addr_space_type {
	OPERAND,
	INSTRUCTION,
	MAX_ADDR_SPACE
} addr_space_type;

typedef struct image_hdr {
	int version;
	image_type type;
	int size;
	int records;
	int table_loaded;
	int filler;
	struct timeval mod_time;
} image_hdr;

typedef struct file_hdr {
	image_hdr header;
	unsigned char spare[IMG_HEAD_SIZE - sizeof(image_hdr)];
} file_hdr;

typedef struct sxi_conv_table {
	int start_addr;
	int size;
} sxi_conv_table;

typedef union conv_table {
	int ace_table[MAX_ADDR_SPACE][MAX_ADDR_STATE][MAX_PAGE_REG];
	sxi_conv_table sxi_table[MAX_SXI_TABLES];
} conv_table;
	
typedef struct location {
	unsigned long seconds;
	unsigned short milliseconds;
	unsigned short  data;
} location;

typedef struct image_file {
	image_hdr  	*hdr;
	conv_table 	*table;
	location   	*image;
	int	   	fd;
} image_file;

#ifdef MAIN
char *image_err_messages[IMG_ERR_LAST_MSG] = {
        "Success",
	"Image file bad parameter list",
	"Image file open failed",
	"Image file lseek failed",
	"Image file write failed",
	"Image file memory map failed",
	"Image file status request (fstat) failed",
	"Image file synchronization failed",
	"Image file does not exist",
	"Image file version does not match build version",
	"Image file size is not expected size for image type",
	"Image file type is unrecognized",
	"Image file record count is not expected count for image type",
	"Image file is missing address conversion table",
	"Image files are not for the same processor type",
	"Image is not an SXI image",
	"SXI image is missing scratch table address",
	"SXI image no physical address for logical table",
	"Image file physical address is larger than memory",
	"Image file is not mapped",
	"Image file unable to find address conversion configuration file",
	"Image config file, bad line format",
	"SXI image config file, line contains bad table number",
	"SXI image config file, line contains bad table size",
	"SXI image config file, line contains bad base address",
	"Image file, image already mapped",
	"ACE image config file, line contains bad page number",
	"ACE image config file, file does not contain enough data",
	"SXI logical address indicator invalid"
        };
#else
        extern char *image_err_messages[IMG_ERR_LAST_MSG];
#endif

#endif /* image_files_h_DEFINED */
