/*
	@(#) goes gtacs include/gtacsversion.h 1.22 07/06/06 08:16:56
*/
/*******************************************************************************

	Purpose:

		GTACS Version Identifier declarations.


*******************************************************************************/
#ifndef GTACSVERSION_H
#define GTACSVERSION_H

#include <stdio.h>
#include "gv_util.p"
#include "log_util.h"

#ifndef GTACS_VERSION
#define GTACS_VERSION	"19.0"
#endif

static char gtacs_version_gvname[] = "GTACS_SW_VERSION";
static gvar *gtacs_version_gvid;
static char gtacs_version_stamp[]= "v"GTACS_VERSION" - "__DATE__"-"__TIME__;


#define SET_GTACS_VERSION \
	if ((gtacs_version_gvid = gv_id(stream_def->dbhead, gtacs_version_gvname))) \
		set_gv(stream_def->dbhead, gtacs_version_gvid, gtacs_version_stamp); \
	else \
		log_err("Error looking up global %s", gtacs_version_gvname) 


#endif
