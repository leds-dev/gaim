/*******************************************************************************

	Purpose: 504-02 message structures                     

	History:


*******************************************************************************/
#ifndef msg_504_02_h_DEFINED
#define msg_504_02_h_DEFINED

#include <sys/time.h>

#define MAX_OATS  11
#define MAX_SPS    9
#define MAX_PM    10
#define MAX_GTACS 12
#define MAX_MRSS  18

/** 	Destination IDs for 504-02 message headers
   	These are defined in the GOES N-Q SSGS Interface Specification 
**/

#define OATS_OFFSET  75
#define SPS_OFFSET   20
#define PM_OFFSET    11
#define MRSS_OFFSET  51
#define GTACS_OFFSET 90
#define ALL_SPS      31
#define ALL_OATS     32
#define ALL_PMS      33
#define ALL_MRSS     34
#define ALL_GTACS    10

/**	Satellite IDs for 504-02 message headers. 
   	These are defined in the GOES N-Q SSGS Interface Specification 
**/
#define FIRST_SAT_ID 	13
#define MAX_SATELLITES 	 8
#define LAST_SAT_ID	(FIRST_SAT_ID + MAX_SATELLITES - 1)

/**	Message types for 504-02 messages.  These are defined in the 
	GOES N-Q SSGS Interface Specification.
**/

#define RANGE_MEASUREMENT         1
#define 	GROUND_PATH_DELAY 1
#define		CAL_TOWER_DELAY   2
#define		SATELLITE_RANGE	  3
#define STAR_MEASUREMENT    	  2
#define OA_DATA_REQUEST     	  3
#define NLUT_DATA_REQUEST   	  5
#define CORT_DATA_REQUEST  	  6
#define SPS_STATUS         	 10
#define WIDEBAND_TELEMETRY 	 11
#define		IMAGER_SUBTYPE 	  1
#define		SOUNDER_SUBTYPE	  2
#define		IMG_ATT_SUBTYPE	  3
#define		SND_ATT_SUBTYPE	  4
#define		RANGE_DATA	  3
#define SPS_TEXT_MSG		 12
#define AVS_TELEMETRY_DATA	 13
#define		AVS_TELEMETRY	  1
#define		AVS_TLM_RATE	  2
#define		ADS_TELEMETRY	  3
#define		ADS_TLM_RATE	  4
#define MRSS_STATUS		 14
#define SXI_TELEMETRY_DATA	 15
#define		SXI_TELEMETRY	 16
#define		SXI_IMAGE_SUM	 19
#define		SXI_HEALTH_DATA	 20
#define		SXI_SEQUENCE	 21
#define		SXI_OBS_EVENTS 	 32
#define		SXI_COMP_EVENTS	 33
#define		SXI_MEM_DUMP	 49
#define		PCM1_TELEMETRY	 254
#define		PCM2_TELEMETRY	 255
#define	SXI_DATA_RATE		 16
#define LANDMARK_MEASUREMENT	 20
#define IMC_QUAL_CHECK_RESP	 21
#define IMC_QUAL_CHECK_COMP	 22
#define RPM_STATUS		 23
#define NORM_LOOKUP_TABLE	 25
#define IMAGE_CCM		 26
#define PM_GTACS_TEXT_MSG	 27
#define OA_DATA_RESP		 33
#define IMC_QUAL_CHECK_REQ	 41
#define TERM_IMC_QUAL_CHECK	 42
#define STAR_CMD_PARAM_REQ	 51
#define IMC_DATA		 53
#define OATS_STATUS		 54
#define		RESPONSE_REQ	  1
#define		RESPONSE_MSG	  2
#define SCAN_FRAME_RESP		 55
#define SK_CMD_PARAMS		 56
#define MOM_DUMP_CMD_PARAMS	 57
#define ECLIPSE_PRED		 59
#define SENSOR_INTRUSION_RESP	 60
#define IMC_SET_READY		 62
#define MOM_TLM_REQ		 68
#define PROP_TLM_REQ		 70
#define STAR_WINDOW_REQ		 71
#define		NORM_WIND 	  1
#define		CONTINGENCY	  2
#define		RET_TO_NORM	  3
#define OATS_ALARM_MSG		 72
#define TORQUE_TABLE_UPD	 73
#define IMCCAL_SCHED_RESP	 75
#define IMCCAL_SCALE_RESP	 80
#define ISEC_SCALE_FACTOR	 81
#define IMC_SCALE_FACTOR	 90
#define SPS_CONFIGURATION	110
#define		CFG_OFFLINE	  1
#define		CFG_ONLINE	  2
#define		CFG_OPER	  3
#define		CFG_PARAMS	  4
#define		CFG_IDLE	  5
#define		CFG_OA		  6
#define		CFG_INLUT	  7
#define		CFG_SNLUT	  8
#define		CFG_CORT	  9
#define SPS_STATUS_REQ		111
#define GTACS_SPS_TEXT_MSG	112
#define AVS_RATE_CONTROL	113
#define MRSS_STATUS_REQ		114
#define SXI_RATE_CONTROL	115
#define RPM_STATUS_REQ		123
#define	GTACS_PM_TEXT_MSG 	129
#define STAR_CMD_REQ		151
#define IMC_COEF_REQ		153
#define GTACS_STATUS		154
#define SCAN_REQ		155
#define ECLIPSE_REQ		159
#define SENSOR_INTRUSION_REQ	160
#define IMC_SET_REQUEST		162
#define IMC_ENABLED		164
#define		IMC_SET		  0
#define		YAW_FLIP_STATE	103
#define IMC_STATUS		165
#define MOM_TLM_RESP		168
#define PROP_TLM_RESP		170
#define IMCCAL_SCHED_REQ	175
#define PROP_TLM_COMP		180
#define GTACS_OATS_TEXT_MSG	199
#define LABELED_STARS		202

/**	Message lengths for 504-02 messages received by GTACS.  These are 
	defined in the GOES N-Q SSGS Interface Specification.  Lengths are 
	in 16 bit half words.  Some messages are variable length, so do 
	not appear in this list.
**/

#define STAR_MEASUREMENT_LEN	296
#define SPS_STATUS_LEN         	 70
#define RPM_STATUS_LEN         	150
#define WIDEBAND_TELEMETRY_LEN 	180
#define SPS_TEXT_MSG_LEN	 40
#define ADS_TELEMETRY_LEN       407
#define	ADS_TLM_RATE_LEN	  6
#define AVS_TELEMETRY_LEN       407
#define	AVS_TLM_RATE_LEN	  6
#define MRSS_STATUS_LEN		 32
#define	SXI_DATA_RATE_LEN	  6
#define IMC_SET_READY_LEN        14
#define STAR_WINDOW_REQ_LEN	  4
#define OATS_ALARM_MSG_LEN	 40
#define GTACS_OATS_TEXT_LEN	 80
#define GTACS_PM_TEXT_LEN	 80

#define MAX_504_02_BODY  3552
#define TRACER_SIZE        10
#define SPARE_SIZE	    7
#define MAX_BUNDLE_SIZE	   10

typedef struct {
	unsigned short	seq_no;
	unsigned char	satellite_id;
	unsigned char	source;
	unsigned long	destination;
	unsigned long	length;
	unsigned char	msg_type;
	unsigned char	msg_subtype;
	char		tracer[TRACER_SIZE];
	unsigned char	count;
	unsigned char	eom;
	unsigned short	spare;
	unsigned long	routing_id;
}	header_504_02;

typedef union {
		unsigned char	string[MAX_504_02_BODY];
		short		word[MAX_504_02_BODY / 2];
		int		dword[MAX_504_02_BODY / 4];
		float		fword[MAX_504_02_BODY / 4];
} body_504_02;


typedef struct {
	header_504_02	hdr;
	body_504_02	body;
}
 	msg_504_02_struct;

typedef union {
	msg_504_02_struct 	str;
	unsigned char		text[MAX_504_02_BODY + sizeof(header_504_02)];
} 	msg_504_02;

typedef struct {
	unsigned char	flag;
	unsigned char	msg_type;
	unsigned char	msg_subtype;
	unsigned char 	source;
	unsigned long	destination;
	unsigned long	length;
	unsigned long	num_msgs;
	unsigned char	satellite_id;
	unsigned char	spare[SPARE_SIZE];
	struct timeval	timestamp;
}	header_tcp_ip;

typedef struct {
	header_tcp_ip	ip_hdr;
	msg_504_02	msgs[MAX_BUNDLE_SIZE];
}	bundle_504_02;


#endif /* msg_504_02_h_DEFINED */


