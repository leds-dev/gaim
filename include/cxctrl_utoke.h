/*
\%Z\% $database $group $folder\%M\% \%I\% \%E\% \%U\%
*/
/*  CXCTRL directive tokens. */

%token	CXCTRL				/* %keyword CXCTRL */
%token	CONFIG				/* %keyword CONFIG */
%token	ACQUIRE				/* %keyword ACQUIRE */
%token	ABORT				/* %keyword ABORT */
%token	TC_WAIT				/* %keyword TC_WAIT */
%token	TC_PENDING			/* %keyword TC_PENDING */
%token	UNIT				/* %keyword UNIT */
%token	AV				/* %keyword AV */
%token	UNAV				/* %keyword UNAV */

