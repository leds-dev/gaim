/*******************************************************************************

        Purpose:

                MAF formats for GOES NOPQ telemetry types.

        Origin:

                Written by Karen Cumming, Integral Systems, Inc.

        History:

                020326 KEC   Created

*******************************************************************************/
#ifndef goes_maf_h_DEFINED
#define goes_maf_h_DEFINED

#define PCM1_MAF	 0
#define PSTE_MAF  	 1
#define PCM2_MAF 	 2
#define DWELL_MAF	 3
#define IMG_WB_MAF	 4
#define SND_WB_MAF	 5
#define SPS_RNG_MAF	 6
#define SPS_STATUS_MAF 	10
#define ADS_MAF		12
#define AVS_MAF		13
#define SXI_HK_MAF	16
#define SXI_COMP_MAF	17
#define SXI_OBS_MAF	18
#define SXI_IMG_MAF	19
#define SXI_MEM_MAF	20
#define SXI_SEQ_MAF	21
#define SXI_HD_MAF	22
#define DWELL_RESET_MAF	40
#define OATS_STATUS_MAF	54
#endif /* goes_maf_h_DEFINED */
