
/*  ENCRYPT directive tokens. */

%token	ENCRYPT				/* %keyword ENCRYPT */
%token	SYNC				/* %keyword SYNC */
%token	VCC				/* %keyword VCC */
%token	KEY				/* %keyword KEY */
%token	KEYOFFSET			/* %keyword KEYOFFSET */
%token	SWW				/* %keyword SWW */
%token	CTCU				/* %keyword CTCU */
%token	CV				/* %keyword CV */
%token	WAIT				/* %keyword WAIT */
%token	NOWAIT				/* %keyword NOWAIT */
%token	VCCFILL				/* %keyword VCCFILL */
%token	KEYSELECT			/* %keyword KEYSELECT */
%token	TIMEOUT				/* %keyword TIMEOUT */
%token	REXMITS				/* %keyword REXMITS */
%token  ICFGCMD				/* %keyword ICFGCMD */
%token  CLEAR				/* %keyword CLEAR */
