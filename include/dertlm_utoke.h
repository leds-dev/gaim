/*	DerTlm derived pseudo telemetry directive tokens. */

%token DERTLM			/* %keyword DERTLM */
%token POINT			/* %keyword POINT */
%token GROUP			/* %keyword GROUP */
%token _ALL			/* %keyword ALL */
%token RELOAD			/* %keyword RELOAD */
%token ENABLE			/* %keyword ENABLE */
%token DISABLE			/* %keyword DISABLE */
