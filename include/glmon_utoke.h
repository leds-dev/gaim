/*
\%Z\% $database $group $folder\%M\% \%I\% \%E\% \%U\%
*/
/*  GLMON directive tokens. */

%token	GENLOAD				/* %keyword GENLOAD */
%token	DBASE				/* %keyword DBASE */
%token	SCID				/* %keyword SCID */
%token	CTCU				/* %keyword CTCU */
%token	RT					/* %keyword RT */
%token	TYPE				/* %keyword TYPE */
%token	FILE				/* %keyword FILE */
%token	MAX					/* %keyword MAX */
%token	SEGSIZE				/* %keyword SEGSIZE */
%token  RTCS_START			/* %keyword RTCS_START */
