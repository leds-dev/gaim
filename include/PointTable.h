/*
\%Z\% $database $group $folder\%M\% \%I\% \%E\% \%U\%
*/
/*******************************************************************************

	Purpose:

		Public data declarations for INSNEC device global table.

	Origin:

		Derived from shin1 version 1.1,
		Written by Eddy Shen, Integral Systems, Inc.

	History:

		981023CCS   Created

*******************************************************************************/
#ifndef	POINTTABLE_H
#define	POINTTABLE_H

#include "db.h"

enum TABLE_TYPE {
	CONFIG_TABLE=1,
	STATUS_TABLE=2
};

typedef struct PointName {
#ifdef SEARCH_INS_PN
	char *name;
#endif
	gvar *gid;
	int n_unused;
	int offset;	/* Using on control, start from 0 */

	struct PointName *next;
} PointName;

typedef struct PointTable {
	char *devname;	/* from config file */
	int code;	/* from config file */
	int offset;	/* from config file */
	int checkloc;	/* from config file */
	int checkbit;	/* from config file */
	int checkval;	/* from config file */
	int type;	/* from config file, see TABLE_TYPE */

	int npoints;
	PointName *pointlist;

	struct PointTable *next;
} PointTable;

int InitPointTable(db_head *, int, char *);
PointTable *FindPointTable(const char *, int);
#ifdef SEARCH_INS_PN
PointName *FindPointName(PointTable *, const char *name);
#endif


extern PointTable *tablelist;	/* Defined in PointTable.c */


#endif
