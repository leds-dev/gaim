#ifndef cfgcmd_h_DEFINED
#define cfgcmd_h_DEFINED

#include "list.p"

#define MAXLINE 255

typedef struct cmd_dest_type {
        char *dest_name;
        char *process_name;
} cmd_dest_type;

/* Function prototypes */
int init_cmd_dests ( LIST * );

#endif
