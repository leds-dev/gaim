/*******************************************************************************

        Purpose:

                Declarations for extra ring buffer manipulation.

        Origin:

                Written by Erik Larson, Integral Systems, Inc.

        History:

                30MAR09	EAL	Created

*******************************************************************************/
#ifndef ring_buffers_h_DEFINED
#define ring_buffers_h_DEFINED

#define RB_OKAY		0x00
#define RB_NO_TIMER	0x01
#define RB_OVERRUN	0x02
#define RB_BLOCKED	0x04
#define RB_TIMEOUT	0x08
#define RB_NO_CTX	0x10
#define RB_NOT_WRITER	0x20

#define RB_DEFAULT_TO	5

extern int ring_util_it;   /* Set to TRUE when using interval timer */

#endif /* ring_buffers_h_DEFINED */
