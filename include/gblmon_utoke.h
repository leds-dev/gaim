/*  gblmon directive tokens. */

%token	ACE1				/* %keyword ACE1 */
%token	ACE2				/* %keyword ACE2 */
%token	BOTH				/* %keyword BOTH */
%token	CGI				/* %keyword CGI */
%token	CHKPT				/* %keyword CHKPT */
%token	COMPRESS			/* %keyword COMPRESS */
%token	DIST				/* %keyword DIST   */
%token  HOST				/* %keyword HOST */
%token  IHOST				/* %keyword IHOST */
%token  IMAGE				/* %keyword IMAGE */
%token  NOW				/* %keyword NOW */
%token  RECOVER                         /* %keyword RECOVER */
%token  REPEAT                          /* %keyword REPEAT */
%token  SXI				/* %keyword SXI */
%token	TGI				/* %keyword TGI */
