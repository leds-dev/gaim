/*
@(#) epoch source include/getopt.h 1.4 99/03/04 11:16:22
*/
/*******************************************************************************

	Purpose:

		Public data declarations for GETOPT.

	Origin:

		Derived from C.A.Measday's getopt.h, version 1.1, by W.C.Stratton,
		Integral Systems, Inc.

	History:

		920602WCS	Created.

*******************************************************************************/
#ifndef getopt_h_DEFINED
#define getopt_h_DEFINED

/*extern  int  getopt () ;*/	/* Function to get command line options. */

extern  char  *optarg ;		/* Set by GETOPT for options expecting arguments. */
extern  int  optind ;		/* Set by GETOPT - index of next ARGV to be processed. */
extern  int  opterr ;		/* Disable (== 0) or enable (!=0) message on standard error. */

#define  NONOPT  (-1)		/* Non-Option - returned by GETOPT when it
				   encounters a non-option argument. */

#endif
