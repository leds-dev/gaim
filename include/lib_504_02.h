/*******************************************************************************

        Purpose:

                Declarations for 504-02 message utilities.

        Origin:

                Written by Karen Cumming, Integral Systems, Inc.

        History:

                990826KEC   Created

*******************************************************************************/
#ifndef lib_50402_h_DEFINED
#define lib_50402_h_DEFINED

#define LOG_504_02_IN  0
#define LOG_504_02_OUT 1
#define MAX_ELEMENTS 25

#include "msg_504_02.h"
#include "db_util.h"
#include "evt_ext.h"

typedef enum log_file_types	/* Type of log file	*/
{	NO_LOG,
	FIXED,
	CYCLIC
} Log_File_Type;

typedef enum log_dbvar_id	/* Logging and event globals	*/
{
	Evt_Stat,
        Log_In,
        Log_Out,
        Log_Size,
        Log_Stat,
        MAX_LOG_DBVAR_IDS
} LOG_DBVar_ID;

#ifdef MAIN
	Log_File_Type file_type[MAX_ELEMENTS];	/* type of log file (none, fixed, cyclic) */
	int           log_events[MAX_ELEMENTS];	/* event logging flag  */

	gv_var LOG_GVS[MAX_ELEMENTS][MAX_LOG_DBVAR_IDS] =
	{
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},

		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
		{
        		Evt_Stat,    "EVT_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
			Log_In,      "LOG_IN",      "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Out,     "LOG_OUT",     "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Size,    "LOG_SIZE",    "", (gvar *) NULL, NULL_CONVERSION,
        		Log_Stat,    "LOG_STAT",    "", (gvar *) NULL, NULL_CONVERSION,
        	},
	};

#else
	extern Log_File_Type file_type[];
	extern int log_events[];
	extern gv_var LOG_GVS[MAX_ELEMENTS][MAX_LOG_DBVAR_IDS];
	
#endif

#endif /* lib_50402_h_DEFINED */
