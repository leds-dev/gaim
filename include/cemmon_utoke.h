/*  CEM directive tokens. */

%token	_ALARM				/* %keyword ALARM */
%token	ACK				/* %keyword ACK */
%token	AV				/* %keyword AV */
%token	_BYPASS				/* %keyword BYPASS */
%token	CEMMON				/* %keyword CEMMON */
%token	DEBUG				/* %keyword DEBUG */
%token	ECHO				/* %keyword ECHO */
%token	GETSTAT				/* %keyword GETSTAT */
%token	HOST				/* %keyword HOST */
%token	KEY				/* %keyword KEY */
%token	LOCAL				/* %keyword LOCAL */
%token	OPER				/* %keyword OPER */
%token	PORT				/* %keyword PORT */
%token	REMOTE				/* %keyword REMOTE */
%token	SELF				/* %keyword SELF */
%token	TEST				/* %keyword TEST */
%token	UNAV				/* %keyword UNAV */
%token	VS				/* %keyword VS */
%token	ZERO				/* %keyword ZERO */
