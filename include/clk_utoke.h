/*  CLK directive tokens. */

%token	ALARM				/* %keyword ALARM */
%token	CLK				/* %keyword CLK */
%token	DEBUG				/* %keyword DEBUG */
%token	GENCOM				/* %keyword GENCOM */
%token	LEAP				/* %keyword LEAP */
%token	OPER				/* %keyword OPER */
%token	RANGE				/* %keyword RANGE */
%token	SPS				/* %keyword SPS */
%token	SYNC				/* %keyword SYNC */
%token	UPCOM				/* %keyword UPCOM */
