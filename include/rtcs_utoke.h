/*
@(#) goes gtacs include/rtcs_utoke.h 1.3 01/11/08 21:18:12
*/

/*  RTCS directive tokens. */

%token	RTCS			/* %keyword RTCS */
%token	SELECT			/* %keyword SELECT */
%token	RT			/* %keyword RT */
%token	ACE_1			/* %keyword ACE_1 */
%token	ACE_2			/* %keyword ACE_2 */
