/*************************************************************************
**          
**  $Header: /Perforce_Conversion/PtpRoot/Inc/AvPtpHeaders/AV_PTP.H 567   5/23/05 6:44p Bsafigan $
**
**  AV_PTP.H :  Header file for AV_PTP Remote Interface Library
**              
**              This file contains prototypes and definitions of all 
**              externally called routines.
**
**  Copyright 1999, Avtec Systems, Inc.
**
***************************************************************************/ 

/* Following lines eliminates redefinition of this header file */

#ifndef _AV_PTP_
#define _AV_PTP_

#include "ptp.h"
#include "ModuleIncludes.h"

#endif /* _AV_PTP_ */



