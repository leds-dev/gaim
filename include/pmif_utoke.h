/*  PM directive tokens. */

%token	AV				/* %keyword AV */
%token	CYC				/* %keyword CYC */
%token	EVENTS				/* %keyword EVENTS */
%token	FIX				/* %keyword FIX */
%token	GETSTAT				/* %keyword GETSTAT */
%token	LOG				/* %keyword LOG */
%token	PM				/* %keyword PM */
%token	TEXT				/* %keyword TEXT */
%token	UNAV				/* %keyword UNAV */
