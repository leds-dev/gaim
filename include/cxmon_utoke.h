/*
\%Z\% $database $group $folder\%M\% \%I\% \%E\% \%U\%
*/
/*  CXMON directive tokens. */

%token	CXMON				/* %keyword CXMON */
%token	RATE				/* %keyword RATE */
%token	MONITOR				/* %keyword MONITOR */
%token	LOGGING				/* %keyword LOGGING */
%token	TIMEOUT				/* %keyword TIMEOUT */
