/* 
*/

/*  CLOAD directive tokens. */
%token	CLOAD				/* %keyword CLOAD */
%token	PING				/* %keyword PING */
%token	_FILE				/* %keyword FILE */
%token	START				/* %keyword START */
%token	NUM				/* %keyword NUM */
%token	ABORT				/* %keyword ABORT */
%token	LOG				/* %keyword LOG */
%token	_OFF				/* %keyword OFF */
%token	EVENTS				/* %keyword EVENTS */
%token	BOTH				/* %keyword BOTH */
%token	GLOBALS				/* %keyword GLOBALS */
%token	UPDATE				/* %keyword UPDATE */
%token	CLEAR				/* %keyword CLEAR */
%token	SKB				/* %keyword SKB */
%token	STAR				/* %keyword STAR */
%token	FRAME				/* %keyword FRAME */
%token	IMC				/* %keyword IMC */
%token	RAM				/* %keyword RAM */
%token	RTCS				/* %keyword RTCS */
%token	STO				/* %keyword STO */
%token	CV				/* %keyword CV */
%token	WAIT				/* %keyword WAIT */
%token	NOWAIT				/* %keyword NOWAIT */
