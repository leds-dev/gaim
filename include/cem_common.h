/*******************************************************************************

        Purpose:

                Declarations for cemmon interface process.

        Origin:

                Written by Karen Cumming, Integral Systems, Inc.

        History:

                990820KEC   Created
                100608DVR   Changed MAX_COMM_ERRORS from 1 to 4 to allow retries

*******************************************************************************/
#ifndef cem_common_h_DEFINED
#define cem_common_h_DEFINED

#include "db_util.h"
#include "evt_ext.h"

#define MAX_MYK 	8
#define MAX_STATUS_PTS  13
#define MAX_BLOCK_SIZE	20
#define CMD_BLOCK_SIZE  5
#define STAT_BLOCK_SIZE 8
#define TLM_BLOCK_SIZE  12
#define PT_CMD_BLK_SIZE 11
#define COMM_BLOCK_SIZE 3
#define MAX_COMM_ERRORS 4
#define MAX_ALARM_CHK   12
#define MAX_KEY_INDEX	255
#define HOSTLEN		80
#define FRAME_LEN	16
#define	MAX_RETRIES	3
#define INVALID_KEY	5

/**	Macros for bit manipulation
**/
#define GETBIT(s,n) (((unsigned char)(s) >> (n)) & 01)
#define SETBIT(s,n) ((unsigned char)(s) |  (01 << (n)))
#define CLRBIT(s,n) ((unsigned char)(s) & ~(01 << (n)))

/**	Command types
**/
typedef enum command_types {
	OPER_CMD,
	XFER_CMD,
	ZERO_CMD,
	ALARM_CHK,
	ALARM_ACK,
	AUTO_CHK,
	LOC_REM_CMD,
	RESET_CMD,
	VS_ECHO_CMD,
	BYPASS_CMD
} command_type;

/**	Encryptor availability
**/
typedef enum enc_avail {
	UNAVAILABLE,
	AVAILABLE
} enc_avail;

/**	Encryptor usage
**/
typedef enum enc_use {
	UNLOCKED,
	LOCKED
} enc_use;

/**	Encryptor control
**/
typedef enum enc_control {
	REMOTE_MODE,
	LOCAL_MODE
} enc_control;

/**	Encryptor operational state
**/
typedef enum enc_oper {
	OFFLINE,
	OPERATIONAL
} enc_oper;

/**	Encryptor bypass mode
**/
typedef enum enc_bypass {
	NORMAL_MODE,
	BYPASS_MODE
} enc_bypass;

/**	Encryptor vehicle sim / echo mode
**/
typedef enum enc_vs_echo {
	ECHO_MODE,
	VS_MODE
} enc_vs_echo;

/**	Encryptor status
**/
typedef enum enc_status {
	READY,
	BUSY,
	FAILED,
	RESETTING
} enc_status;

/**	Block types from MYK-15 operation manual
**/
#define CT_OUT 	(0xC5)
#define PT_ECHO	(0xCD)
#define PT_OUT	(0xFC)
#define BYPASS	(0xE2)
#define STATUS	(0xF3)
#define ALARM	(0xD9)
#define TLM_BLK	(0xFE)
#define PT_CMD	(0xFD)
#define CERROR	(0xFA)
#define	PT_IN	(0xF6)
#define CT_IN	(0xA6)
#define CONTROL (0xCC)
#define STATREQ (0xF9)
#define TLM_REQ (0xFF)

/**	function prototypes
**/
int process_enc_msg(void);
int send_control(command_type command, int index);
int connect_cem(int index);
void disconnect_cem(int status);
int write_cem(unsigned char *buffer, int length);
int read_cem(unsigned char *buffer, int max_length);
void pack(unsigned char *in_data, unsigned char *out_data, int block_type);
void unpack(unsigned char *in_data, unsigned char *out_data);
int request_status();

typedef struct status_decom_struct		/* Status msg decommutation data	*/
{
	short		index;
	short		byte_num;
	short		bit_num;
} status_decom_type;


typedef enum myk_if_dbvar_id		/* myk global variables		*/
{
	MYK_Alarm, 
	MYK_Avail,  
	MYK_Bypass,  
	MYK_Control,  
	MYK_Dec_Fault,  
	MYK_Dec_Test,  
	MYK_Enc_Fault, 
	MYK_Enc_Test,  
	MYK_Fill_State, 
	MYK_Format, 
	MYK_Host,
	MYK_Key,
	MYK_Lock,
	MYK_Mode,
	MYK_Name,  
	MYK_Oper,
	MYK_Parity,
	MYK_Pattern, 
	MYK_Port,  
	MYK_Self_Test, 
	MYK_Status,  
	MYK_Vs_Echo,  
	MYK_Zero, 
	MAX_MYK_DBVAR_IDS
} myk_DBVar_ID;

#ifdef MAIN

	fd_set input_mask;				/* active sockets		*/
	int encryptor_fd = -1;				/* port to encryptor		*/
	int encryptor = -1;				/* encryptor currently in use	*/
	
	unsigned char last_msg[MAX_BLOCK_SIZE];		/* last transmitted block	*/
	int last_msg_len = 0;				/* length of last message	*/
	int comm_err_cnt = 0;				/* number of comm errors	*/
	char hostname[HOSTLEN];				/* name of host machine		*/
	int status_received;				/* status block received	*/
	int data_received;				/* data block received		*/
	int colocated = TRUE;				/* use only colocated encryptor */
	
	gv_var MYK_GVS[MAX_MYK][MAX_MYK_DBVAR_IDS] =	/* MYK  globals	*/
	{
		{	
			MYK_Alarm,	"ALARM",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Avail,	"AVAIL",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Bypass,	"BYPASS",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Control,	"CONTROL",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Fault,	"DEC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Test,	"DEC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Fault,	"ENC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Test,	"ENC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Fill_State,	"FILL_STATE",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Format,	"FORMAT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Host,	"HOST",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Key,	"KEY",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Lock,	"LOCK",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Mode,	"MODE",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Name,	"Name",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Oper,	"OPER",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Parity,	"PARITY",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Pattern,	"PATTERN",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Port,	"PORT",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Self_Test,	"SELF_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Status,	"STATUS",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Vs_Echo,	"VS_ECHO",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Zero,	"ZERO",		"", (gvar *) NULL, NULL_CONVERSION,
		},
		{	
			MYK_Alarm,	"ALARM",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Avail,	"AVAIL",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Bypass,	"BYPASS",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Control,	"CONTROL",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Fault,	"DEC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Test,	"DEC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Fault,	"ENC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Test,	"ENC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Fill_State,	"FILL_STATE",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Format,	"FORMAT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Host,	"HOST",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Key,	"KEY",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Lock,	"LOCK",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Mode,	"MODE",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Name,	"Name",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Oper,	"OPER",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Parity,	"PARITY",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Pattern,	"PATTERN",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Port,	"PORT",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Self_Test,	"SELF_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Status,	"STATUS",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Vs_Echo,	"VS_ECHO",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Zero,	"ZERO",		"", (gvar *) NULL, NULL_CONVERSION,
		},
		{	
			MYK_Alarm,	"ALARM",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Avail,	"AVAIL",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Bypass,	"BYPASS",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Control,	"CONTROL",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Fault,	"DEC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Test,	"DEC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Fault,	"ENC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Test,	"ENC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Fill_State,	"FILL_STATE",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Format,	"FORMAT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Host,	"HOST",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Key,	"KEY",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Lock,	"LOCK",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Mode,	"MODE",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Name,	"Name",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Oper,	"OPER",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Parity,	"PARITY",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Pattern,	"PATTERN",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Port,	"PORT",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Self_Test,	"SELF_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Status,	"STATUS",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Vs_Echo,	"VS_ECHO",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Zero,	"ZERO",		"", (gvar *) NULL, NULL_CONVERSION,
		},
		{	
			MYK_Alarm,	"ALARM",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Avail,	"AVAIL",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Bypass,	"BYPASS",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Control,	"CONTROL",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Fault,	"DEC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Test,	"DEC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Fault,	"ENC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Test,	"ENC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Fill_State,	"FILL_STATE",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Format,	"FORMAT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Host,	"HOST",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Key,	"KEY",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Lock,	"LOCK",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Mode,	"MODE",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Name,	"Name",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Oper,	"OPER",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Parity,	"PARITY",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Pattern,	"PATTERN",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Port,	"PORT",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Self_Test,	"SELF_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Status,	"STATUS",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Vs_Echo,	"VS_ECHO",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Zero,	"ZERO",		"", (gvar *) NULL, NULL_CONVERSION,
		},
		{	
			MYK_Alarm,	"ALARM",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Avail,	"AVAIL",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Bypass,	"BYPASS",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Control,	"CONTROL",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Fault,	"DEC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Test,	"DEC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Fault,	"ENC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Test,	"ENC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Fill_State,	"FILL_STATE",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Format,	"FORMAT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Host,	"HOST",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Key,	"KEY",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Lock,	"LOCK",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Mode,	"MODE",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Name,	"Name",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Oper,	"OPER",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Parity,	"PARITY",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Pattern,	"PATTERN",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Port,	"PORT",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Self_Test,	"SELF_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Status,	"STATUS",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Vs_Echo,	"VS_ECHO",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Zero,	"ZERO",		"", (gvar *) NULL, NULL_CONVERSION,
		},
		{	
			MYK_Alarm,	"ALARM",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Avail,	"AVAIL",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Bypass,	"BYPASS",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Control,	"CONTROL",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Fault,	"DEC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Test,	"DEC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Fault,	"ENC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Test,	"ENC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Fill_State,	"FILL_STATE",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Format,	"FORMAT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Host,	"HOST",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Key,	"KEY",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Lock,	"LOCK",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Mode,	"MODE",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Name,	"Name",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Oper,	"OPER",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Parity,	"PARITY",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Pattern,	"PATTERN",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Port,	"PORT",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Self_Test,	"SELF_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Status,	"STATUS",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Vs_Echo,	"VS_ECHO",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Zero,	"ZERO",		"", (gvar *) NULL, NULL_CONVERSION,
		},
		{	
			MYK_Alarm,	"ALARM",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Avail,	"AVAIL",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Bypass,	"BYPASS",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Control,	"CONTROL",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Fault,	"DEC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Test,	"DEC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Fault,	"ENC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Test,	"ENC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Fill_State,	"FILL_STATE",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Format,	"FORMAT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Host,	"HOST",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Key,	"KEY",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Lock,	"LOCK",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Mode,	"MODE",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Name,	"Name",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Oper,	"OPER",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Parity,	"PARITY",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Pattern,	"PATTERN",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Port,	"PORT",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Self_Test,	"SELF_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Status,	"STATUS",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Vs_Echo,	"VS_ECHO",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Zero,	"ZERO",		"", (gvar *) NULL, NULL_CONVERSION,
		},
		{	
			MYK_Alarm,	"ALARM",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Avail,	"AVAIL",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Bypass,	"BYPASS",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Control,	"CONTROL",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Fault,	"DEC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Dec_Test,	"DEC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Fault,	"ENC_FAULT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Enc_Test,	"ENC_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Fill_State,	"FILL_STATE",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Format,	"FORMAT",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Host,	"HOST",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Key,	"KEY",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Lock,	"LOCK",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Mode,	"MODE",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Name,	"Name",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Oper,	"OPER",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Parity,	"PARITY",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Pattern,	"PATTERN",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Port,	"PORT",		"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Self_Test,	"SELF_TEST",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Status,	"STATUS",	"", (gvar *) NULL, STATE_FORMAT,
			MYK_Vs_Echo,	"VS_ECHO",	"", (gvar *) NULL, NULL_CONVERSION,
			MYK_Zero,	"ZERO",		"", (gvar *) NULL, NULL_CONVERSION,
		},
	};

/**	Table used to decommutate the status block. 
**/
	status_decom_type decom[MAX_STATUS_PTS] = 
	{
		MYK_Enc_Test,	1,	5,	
		MYK_Dec_Test,	1,	6,
		MYK_Oper,	2,	0,
		MYK_Vs_Echo,	2,	1,
		MYK_Format,	2,	2,
		MYK_Parity,	2,	3,
		MYK_Zero,	3,	1,
		MYK_Fill_State,	4,	3,
		MYK_Self_Test,	4,	5,
		MYK_Enc_Fault,	4,	6,
		MYK_Dec_Fault,	5,	0,
		MYK_Bypass,	5,	2,
		MYK_Control,	5,	3
	};
#else
	extern fd_set input_mask;
	extern int encryptor_fd;
	extern int encryptor;

	extern unsigned char last_msg[MAX_BLOCK_SIZE];
	extern int last_msg_len;			
	extern int comm_err_cnt;
	extern char hostname[HOSTLEN];
	extern int status_received;
	extern int data_received;
	extern int colocated;
		
	extern gv_var MYK_GVS[MAX_MYK][MAX_MYK_DBVAR_IDS];
	extern status_decom_type decom[MAX_STATUS_PTS]; 

#endif

#endif /* cem_common_h_DEFINED */

