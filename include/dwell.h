/*******************************************************************************
*
*       Declarations for dwell processing.
*
*
*       VERSION         DATE            AUTHOR          COMMENT
*       _______         ______          ________        ________________________
*
*       1.0             032/02          Karen Cumming   Created.
*
******************************************************************************** ****/
#ifndef DWELL_H
#define DWELL_H

#include "sdb_util.h"
#include "ring_util.p"

#define OFFSET 1
#define MINOR_FRAME_SIZE 512
#define MAJOR_FRAME_SIZE 32
#define FRAME_HEADER_SIZE 7

#define NUM_DWELL_ID_CHANNELS 16

#define START_BUILDING_MAJOR_FRAME 0
#define MAJOR_FRAME_COMPLETE 1
#define BUILDING_MAJOR_FRAME 2

#define NORMAL 0
#define DWELL 1

#define NONACE 0
#define ACE1 1
#define ACE2 2

#define ADDRESSMODE 0
#define DATAMODE 1
#define MEMDUMPMODE 2

#define NONPREEMPTIBLE 0
#define PREEMPTIBLE 1

typedef struct major_frame {
        tlm_frame frame_hdr;
        unsigned char frame [MINOR_FRAME_SIZE];
} major_frame;

typedef struct dwell_id {
        int ace;
        int value;
        int mode;
        int rtaddress;
        unsigned char word[2];
} dwell_id;

#ifdef MAIN
	dwell_id dwell_id_info[NUM_DWELL_ID_CHANNELS];
#else
	extern dwell_id dwell_id_info[NUM_DWELL_ID_CHANNELS];
#endif

#endif /* DWELL_H */

