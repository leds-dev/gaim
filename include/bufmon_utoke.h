/*
 * @(#) goes gtacs include/bufmon_utoke.h 1.12 02/10/18 15:04:56
 */


/* directive tokens. */

%token SCHEDMON			/* %keyword SCHEDMON */
%token MONSCHED			/* %keyword MONSCHED */
%token MONITOR			/* %keyword MONITOR */
%token NAME			/* %keyword NAME */
%token ONBOARD			/* %keyword ONBOARD */
%token STOP			/* %keyword STOP */
%token INITX			/* %keyword INITX */
%token DEBUG			/* %keyword DEBUG */

%token TLMWAIT			/* %keyword TLMWAIT */

%token SCAN			/* %keyword SCAN */
%token SCANDATA			/* %keyword SCANDATA */
%token STAR			/* %keyword STAR */
%token STARDATA			/* %keyword STARDATA */

%token RTCSDATA			/* %keyword RTCSDATA */

%token NEXTSCHED		/* %keyword NEXTSCHED */
%token BIAS			/* %keyword BIAS */
%token SHADOW			/* %keyword SHADOW */
%token START			/* %keyword START */

%token GO			/* %keyword GO */
%token SET			/* %keyword SET */

%token CMDWARN			/* %keyword CMDWARN */

%token PSS			/* %keyword PSS */
%token COLLECT			/* %keyword COLLECT */
%token RESTART			/* %keyword RESTART */
%token CAL			/* %keyword CAL */
%token CALIBRATE		/* %keyword CALIBRATE */
%token CAL			/* %keyword CAL */
%token AZIMUTH			/* %keyword AZIMUTH */
%token AZM			/* %keyword AZM */
%token ELEVATION		/* %keyword ELEVATION */
%token ELV			/* %keyword ELV */




