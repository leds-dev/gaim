/*******************************************************************************

	Purpose:

    	GENLOAD directive structures and declarations.

	Origin:

		Written by B. Miller, Integral Systems, Inc.

	History:

		930525WCS	Created.
		991115BNM	Modified for GOES N-Q

*******************************************************************************/
#ifndef genload_h_DEFINED
#define genload_h_DEFINED

#include <sys/time.h>
#include <sys/param.h>

#ifndef	MAXPATHLEN
#define	MAXPATHLEN	1024
#endif

#define MAX_BYTE_ARRAY 512
#define MAX_HEXSTRING MAX_BYTE_ARRAY/2 + 2
#define IDLE_SEQ_LENGTH 10

#define REVNO "1.0"

#define CTCU1_ID 	 1
#define CTCU1_STR	"1"
#define CTCU2_ID 	 2
#define CTCU2_STR	"2"

#define ACE1_RTADDRESS	4
#define ACE1_STR "ACE_1"
#define ACE2_RTADDRESS	5
#define ACE2_STR "ACE_2"

#define MAX_IMC_TYPE53 6
#define MAX_IMC_TYPE62 7

#define CMD_DIRECTIVE "CMD"

#define SKB_FTYPE_TAG "STOLMAP"
#define SKB_TABLE_VERSION "2.0"
#define SKB_VERSION_COL     1
#define SKB_FTYPE_COL       2
#define SKB_LNUM_COL        1
#define SKB_IDX_START_COL   3
#define SKB_IDX_STOP_COL    4
#define SKB_TTAG_COL        4
#define SKB_CMDDIR_COL      5
#define SKB_HEADER_LINE     1
#define SKB_FIRST_DATA_LINE 3

#define RTCS_FTYPE_TAG "RTCSMAP"
#define RTCS_TABLE_VERSION "1.0"
#define RTCS_HEADER_LINE     1
#define RTCS_VERSION_COL     1
#define RTCS_FTYPE_COL       2
#define RTCS_FIRST_DATA_LINE 3
#define RTCS_IDX_START_COL   3
#define RTCS_IDX_STOP_COL    4
#define RTCS_CMDDIR_COL      5
#define RTCS_LABEL_COL       6
#define RTCS_HEX_COL         7
#define RTCS_ADDRESS_COL     8
#define RTCS_NUM_COL         9
#define RTCS_MAX_DATA_COLS   10

#define STAR_FTYPE_TAG "Star"
#define STAR_TABLE_VERSION "1.0"
#define STAR_IMGR_TAG "Imager"
#define STAR_SNDR_TAG "Sounder"
#define STAR_VERSION_COL 1  /* Col 1 in header line 1 */
#define STAR_FTYPE_COL   2  /* Col 2 in header line 1 */

#define STAR_TIME_COL    1  /* Col 1 in a data line */
#define STAR_ETIME_COL   2  /* Col 2 in a data line */
#define STAR_INSTR_COL   4  /* Col 4 in a data line */
#define STAR_NSCYC_COL  11  /* Col 10 in a data line */
#define STAR_NSINC_COL  12  /* Col 11 in a data line */
#define STAR_EWCYC_COL  13  /* Col 12 in a data line */
#define STAR_EWINC_COL  14  /* Col 13 in a data line */
#define STAR_MAX_COLS   14  /* Max cols in a data line */

#define FRAME_FTYPE_TAG "Frame"
#define FRAME_TABLE_VERSION "1.0"
#define FRAME_IMGR_TAG "Imager"
#define FRAME_SNDR_TAG "Sounder"
#define FRAME_VERSION_COL      1  /* Col 1 in header line 1 */
#define FRAME_FTYPE_COL        2  /* Col 2 in header line 1 */

#define FRAME_MAXFRAMES_COL    1  /* Col 1 in header line 2 */

#define FRAME_BUFF_INDEX_COL   1  /* Col 1 in a data line */
#define FRAME_INSTR_COL        4
#define FRAME_START_NSCYC_COL 13
#define FRAME_START_NSINC_COL 14
#define FRAME_START_EWCYC_COL 15
#define FRAME_START_EWINC_COL 16
#define FRAME_STOP_NSCYC_COL  17
#define FRAME_STOP_NSINC_COL  18
#define FRAME_STOP_EWCYC_COL  19
#define FRAME_STOP_EWINC_COL  20
#define FRAME_MAX_COLS        22  /* Max cols in a data line */

#define MAX_LINE_LEN 256
#define MAX_DIR_SIZE 1024
#define SEPCHARS ","


#define FRAME_INCR_PER_CYC_IMGR 6136
#define FRAME_INCR_PER_CYC_SNDR 2805
#define STAR_INCR_PER_CYC_IMGR 6136
#define STAR_INCR_PER_CYC_SNDR 2805

#define DAT_EXT ".dat"
#define BIN_EXT ".bin"
#define PRC_EXT ".prc"
#define MAP_EXT ".map"
#define ERR_EXT ".err"

char	*DEFAULT_DBASE;		/* Database to use if none specified on command line */

char	*LOAD_DEFAULT_CTCU;
char	*LOAD_DEFAULT_RT;

/* Data Load Command mnemonics */
char 	*RAM_AE_LOAD_CMD;
char 	*RAM_AF_LOAD_CMD;
char 	*SKB_LOAD_CMD;
char	*STAR_LOAD_CMD;
char 	*FRAME_LOAD_CMD;
char 	*IMC_LOAD_CMD;

char	*LOAD_TEMPLATES_DIR;  /* Directory where canned init, load, and cleanup procs are located */ 

char	*RAM_AE_HDR_PRC;
char 	*RAM_AE_INIT_PRC;
char 	*RAM_AE_LOAD_PRC;
char 	*RAM_AE_CLEANUP_PRC;

char	*RAM_AF_HDR_PRC;
char 	*RAM_AF_INIT_PRC;
char 	*RAM_AF_LOAD_PRC;
char 	*RAM_AF_CLEANUP_PRC;

char	*RAM_TORQ_HDR_PRC;
char 	*RAM_TORQ_INIT_PRC;
char	*RAM_TORQ_LOAD_PRC;
char 	*RAM_TORQ_CLEANUP_PRC;

char	*STO_HDR_PRC;
char 	*STO_INIT_PRC;
char 	*STO_LOAD_PRC;
char 	*STO_CLEANUP_PRC;

char	*RTCS_HDR_PRC;
char 	*RTCS_INIT_PRC;
char 	*RTCS_LOAD_PRC;
char 	*RTCS_CLEANUP_PRC;

char	*SKB_HDR_PRC;
char 	*SKB_INIT_PRC;
char 	*SKB_LOAD_PRC;
char 	*SKB_CLEANUP_PRC;

char	*STAR_HDR_PRC;
char 	*STAR_INIT_PRC;
char 	*STAR_LOAD_PRC;
char 	*STAR_CLEANUP_PRC;

char	*FRAME_HDR_PRC;
char 	*FRAME_INIT_PRC;
char 	*FRAME_LOAD_PRC;
char 	*FRAME_CLEANUP_PRC;

char	*IMC_HDR_PRC;
char 	*IMC_INIT_PRC;
char 	*IMC_LOAD_PRC;
char 	*IMC_CLEANUP_PRC;

char	*RAM_BIN_OUTDIR;
char	*RAM_PRC_OUTDIR;
char	*RAM_ERR_OUTDIR;
char	*STO_BIN_OUTDIR;
char	*STO_PRC_OUTDIR;
char	*STO_ERR_OUTDIR;
char	*SKB_BIN_OUTDIR;
char	*SKB_PRC_OUTDIR;
char	*SKB_ERR_OUTDIR;
char	*SKB_MAP_OUTDIR;
char	*RTCS_BIN_OUTDIR;
char	*RTCS_PRC_OUTDIR;
char	*RTCS_ERR_OUTDIR;
char	*RTCS_MAP_OUTDIR;
char	*STAR_BIN_OUTDIR;
char	*STAR_PRC_OUTDIR;
char	*STAR_ERR_OUTDIR;
char	*FRAME_BIN_OUTDIR;
char	*FRAME_PRC_OUTDIR;
char	*FRAME_ERR_OUTDIR;
char	*IMC_BIN_OUTDIR;
char	*IMC_PRC_OUTDIR;
char	*IMC_ERR_OUTDIR;

char	*RAM_INDIR;
char	*SKB_INDIR;
char	*STAR_INDIR;
char	*FRAME_INDIR;
char	*IMC_INDIR;
char	*STO_INDIR;
char	*RTCS_INDIR;
char 	*I_MAX_XCYC;
char	*I_MAX_XINC;
char	*I_MAX_YCYC;
char	*I_MAX_YINC;
char 	*S_MAX_XCYC;
char	*S_MAX_XINC;
char	*S_MAX_YCYC;
char	*S_MAX_YINC;

char	*RTCS_DEFAULT_TOP;
char	*RTCS_DEFAULT_BOTTOM;


/*******************************************************************************

	Error code values

*******************************************************************************/
typedef enum err_num { 
	E_SUCCESS=0,
	E_INV_ARGUMENT,
	E_INV_SCID,
	E_DB_NOT_SPECIFIED,
	E_DB_NOT_FOUND,
	E_EPOCHCFG_NOT_FOUND,
	E_EPOCHCFG_LOAD_ERROR,
	E_PROCESS_TABLE,
	E_STREAM_DEF,
	E_DBLOAD_FAILED,
	E_INIT_GV_UTIL,
	E_INIT_TLM_UTIL,
	E_INIT_CMD_UTIL,
	E_DIR_PARSER,
	E_FILE_OPEN,
	E_BIN_FILE_OPEN,
	E_PROC_FILE_OPEN,
	E_BIN_FILE_WRITE,
	E_PROC_FILE_WRITE,
	E_SRC_FILE_OPEN,
	E_SRC_FILE_READ,
	E_MAX_CMDS_EXCEEDED,
	E_MAX_STARS_EXCEEDED,
	E_MAX_FRAMES_EXCEEDED,
	E_STAR_INV_VALUE,
	E_STAR_INV_FTYPE,
	E_STAR_INV_VERSION,
	E_FRAME_INV_VALUE,
	E_FRAME_INV_FTYPE,
	E_FRAME_INV_VERSION,
	E_FRAME_INV_MAXFRAMES,
	E_CHECKSUM_ERROR,
	E_SKB_INV_FTYPE,
	E_SKB_INV_VERSION,
	E_MAP_FILE_OPEN,
	E_MAP_FILE_WRITE,
	E_PROC_NOT_FOUND,
	E_ENVVAR_NOT_SET,
	E_CFG_FILE_OPEN,
	E_CFG_FILE_INV_RECORD,
	E_MEM_ALLOC_FAILED,
	E_GLOBAL_VAR_LOAD,
	E_RTCS_NO_START,
	E_RTCS_OVERLAP,
	E_RTCS_UPPER_BOUND,
	E_RTCS_LOWER_BOUND,
	E_RTCS_INV_FTYPE,
	E_RTCS_INV_VERSION,
	E_RTCS_ENDIS,
	E_ERR_FILE_OPEN,
	E_SRC_FILE_NOT_SPEC,
	E_LDTYPE_NOT_SPEC,
	E_SCID_NOT_SPEC,
	E_ARG_MISSING,
	E_INV_LOAD_TYPE,
	E_BLOCK_BEFORE_TYPE,
	E_BLOCK_NUM_INCR,
	E_MAX_LENGTH_EXCEEDED,
	E_LENGTH_NOT_IN_BLOCK,
	E_ADDRESS_NOT_FOR_TYPE,
	E_ADDRESS_NOT_IN_BLOCK,
	E_START_NOT_FOR_TYPE,
	E_START_NOT_IN_BLOCK,
	E_TOO_MANY_DATAS,
	E_TOO_FEW_DATAS,
	E_ODD_DATAS,
	E_DATA_NOT_IN_BLOCK,
	E_CHKSUM_NOT_IN_BLOCK,
	E_INV_IMC_TYPE,
	E_INV_CHILD_TYPE,
	E_INV_IMC_DYNRNG,
	E_INV_KEYWORD,
	MAX_ERRORS
} err_num;

#ifdef GENLOAD_DECLARE
char *err_str[MAX_ERRORS] = {
	"Success",
	"Invalid argument specified on command line",
	"Invalid Spacecraft ID specified or Spacecraft ID not set on command line",
	"Database not specified in environment or on command line",
	"Specified database not found",
	"Configuration file epoch.cfg not found",
	"Error loading file epoch.cfg",
	"Error building process table",
	"Error allocating stream_def",
	"Database load failed",
	"Unable to initialize global variable processing",
	"Unable to initialize telemetry processing",
	"Unable to initialize command processing",
	"Directive parsing error while processing input file",
	"Error opening file",
	"Error opening file binary output file",
	"Error opening upload procedure output file",
	"Error writing to binary file",
	"Error writing to procedure file",
	"Error opening source file",
	"Error reading source file",
	"Number of commands exceeds buffer maximum",
	"Number of stars in source file exceeds star buffer maximum",
	"Number of frames in source file exceeds frame buffer maximum",
	"Invalid value read from star record",
	"Input file type tag mismatch, expecting STAR",
	"Star Table version mismatch",
	"Invalid value read from frame record",
	"Input file type tag mismatch, expecting FRAME",
	"Frame Table version mismatch",
	"Error reading max number of frames value from frame table",
	"Error - Calculated checksum does not match supplied checksum",
	"Input file type tag mismatch, expecting MAP",
	"Error - Schedule Map file version mismatch",
	"Error opening schedule map file",
	"Error writing to schedule map output file",
	"Error - unable to locate procedure file",
	"Error - required environment variable not set",
	"Error opening file genload.cfg",
	"Error - invalid record read from file genload.cfg",
	"Error allocating memory for configuration string",
	"Error loading global variable from database",
	"Error - RTCS_START must be supplied for load type RTCS",
	"Error - Start index of RTCS overlaps end of preceding RTCS",
	"Error - RTCS outside upper bound of RTCS portion of STO",
	"Error - RTCS outside lower bound of RTCS portion of STO",
	"Error - Input RTCS Map file type tag mismatch",
	"Error - RTCS Map file version mismatch",
	"Error - Problem determining RTCS enabled-disabled status",
	"Error opening error log file",
	"Error - Required argument <source file> not specified",
	"Error - Required argument <load type> not specified",
	"Error - Required argument <spacecraft ID> not specified",
	"Error - Data value missing following keyword",
	"Error - Invalid load type specified for TYPE parameter",
	"Error - TYPE must be first keyword in HSC format load input file",
	"Error - BLOCK_NUM must start at 1 and increment seqentially",
	"Error - LENGTH value exceeds max allowed for a single block",
	"Error - LENGTH keyword encountered outside of block",
	"Error - ADDRESS keyword only valid for RAM_AF load type",
	"Error - ADDRESS keyword encountered outside of block",
	"Error - START keyword only valid for SKB, STAR, FRAME, and IMC load types",
	"Error - START keyword encountered outside of block",
	"Error - Number of data words read greater than LENGTH of block",
	"Error - Number of datawords read less than LENGTH of block",
	"Error - Odd number of datawords read, aborting load generation",
	"Error - DATA keyword encountered outside of block",
	"Error - CHKSUM keyword encountered outside of block",
	"Error - IMC_TYPE value is invalid",
	"Error - CHILD_TYPE value is invalid",
	"Error - IMC_DYNRNG value must be 1 (low) or 2 (high)",
	"Error - Invalid keyword read from input file"
};
#else
extern	char *err_str[];
#endif


/*******************************************************************************

	Local storage for collecting parsed directive arguments.

*******************************************************************************/

typedef struct command_struct {
	struct command_struct	*owner;
	int 			cmd_index;
	struct command		*cmd_id;
	int			num_datawords;
	dataword_item		*dataword;
	struct syntax_context 	*syntax_ctx;
	struct command_format	*command_fmt;
	struct SCAddress 	*address;
	LIST			commands;
	struct command_struct	*stored_cmd;
	struct command_struct	*next;
	int			load_flag;
	char    		*pathname;
	int			line_num;
	char			*dir;
	int			num_bytes;
	unsigned char		*cmd_frame;
	int                     cmd_frame_offset;
	unsigned char		*load_data;
	int			load_data_size;
	LIST			load_words;
	int			start_idx;
	int			stop_idx;
	char			*name;
	int			num;
	int			enabled;
} command_struct;

/*	Header of binary load files
*/
#define BINFILE_VERSION_TAG "VERSION"
#define BINFILE_VERSION	"VERSION_002" /* Version 1 had no version indicator */

typedef struct bin_hdr_struct {
	char		version[16];
	unsigned int 	ldtype;
	unsigned int 	imctype53;
	unsigned int 	imctype62;
	unsigned int 	imcdynrng;
	unsigned int 	ctcuid;
	unsigned int 	rtaddr;
	unsigned int 	numcmds;
	unsigned int	spare;
} bin_hdr_struct;

#include "funcs.h"

typedef enum loadtype { INV=-1,
			HSC, 
			RAM_AE, 
			RAM_AF, 
			RAM_TORQ, 
			IMC, 
			STO, 
			SKB, 
			STAR, 
			FRAME,
			RTCS,
			MAX_LOAD_TYPES} loadtype;

#ifdef GENLOAD_DECLARE
char *loadtype_str[MAX_LOAD_TYPES]=
	{ 
	"HSC", 
	"RAM_AE", 
	"RAM_AF", 
	"RAM_TORQ", 
	"IMC", 
	"STO", 
	"SKB", 
	"STAR", 
	"FRAME",
	"RTCS" };
#else
extern char *loadtype_str[MAX_LOAD_TYPES];
#endif
		
#ifdef GENLOAD_DECLARE

char			*input_filename=NULL;
char			*source_file;
char			*bin_filename;
char			*map_filename;
char			*prc_filename;
char			*err_filename;
FILE			*error_file;
struct 	command_struct	*cmd;
int 			cmd_index;
struct 	SCAddress	*address;
struct 	command_struct	*subsys_cmd;
unsigned long		sequence_num;
FILE			*report_file = stderr;
Point			*pid;
double			set_value;
char			*filename;
LIST			frames = (LIST) NULL;
int			next_line = 1;
int			line_num = 1;
enum loadtype		load_type = INV;
unsigned int		imc_type53;
unsigned int		imc_type62;
unsigned int		imc_dynrng;
char 			*database=NULL;
LIST			commands = (LIST) NULL;
int			seg_size=0;
char			*curr_dir;
char			*ctcu_id=NULL;
char			*sc_addr=NULL;
char			*rt_addr=NULL;
int			max=0;
LIST			load_blocks=NULL;
int			delfiles=1;
int			rtcs_start=-1;

#else

extern	char			*input_filename;
extern	char			*source_file;
extern	char			*bin_filename;
extern 	char			*map_filename;
extern 	char			*prc_filename;
extern 	char			*err_filename;
extern	FILE			*error_file;
extern	struct 	command_struct	*cmd;
extern	int 			cmd_index;
extern	struct 	SCAddress	*address;
extern	struct 	command_struct	*subsys_cmd;
extern	unsigned long		sequence_num;
extern	FILE			*report_file;
extern	Point			*pid;
extern	double			set_value;
extern	char			*filename;
extern	LIST			frames;
extern 	int			next_line;
extern	int			line_num;
extern	enum loadtype		load_type;
unsigned int			imc_type53;
unsigned int			imc_type62;
unsigned int			imc_dynrng;
extern	char			*database;
extern 	LIST			commands;
extern	int			seg_size;
extern	char			*curr_dir;
extern	char			*ctcu_id;
extern	char 			*sc_addr;
extern	char			*rt_addr;
extern	int			max;
extern 	LIST			load_blocks;
extern	int			delfiles;
extern	int			rtcs_start;
#endif


#endif
