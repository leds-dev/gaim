/*	OATS Data Collector Process  directive tokens. */

%token START			/* %keyword START 	*/
%token MANEUVER			/* %keyword MANEUVER 	*/
%token DUMP			/* %keyword DUMP 	*/
%token MOMENTUM			/* %keyword MOMENTUM 	*/
%token DATA			/* %keyword DATA	*/
%token RATE			/* %keyword RATE    	*/
%token STOP			/* %keyword STOP    	*/
%token SENSOR_START		/* %keyword SENSOR_START	*/
%token SENSOR_STOP		/* %keyword SENSOR_STOP	*/
%token SENSOR			/* %keyword SENSOR	*/
