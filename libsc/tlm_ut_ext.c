/*
@(#) goes gtacs libsc/tlm_ut_ext.c 1.8 06/11/22 11:20:31
*/
/* 
@(#) epoch source libsc/tlm_ut_ext.c 1.9 02/10/21 13:02:41
*/
/*******************************************************************************

	Purpose:

		The TLM_UT_EXT routines initialize for mission specific extensions to
		tlm_util.c.

    Public Functions:

		INIT_TLM_UTIL_EXT	Initialize mission-specific telemetry processing.

    Private Functions:

	Origin:

		Written by W.C.Stratton, Integral Systems, Inc.

	History:

		940328WCS	Created.

        2-2004 PAHD	Goes-specific version of this shared object file
			modified to correct timestamping for:
			1 - Exclude all MAFs except those that actually need
                        timestamping
                        2 - If decom context point is a byte_array, use its
                        timestamp as starting time rather than the frame time
                        3 - For PCM 1 or 2 streams, check global rather than
                        MAF for rate because rate can change during operations

*******************************************************************************/

#include "gv_util.p"
#include "timeval_util.p"
#include "tlm.p"
#include "goes_maf.h"

extern	struct timeval *process_frame_time;

static int format = -1;
static int format_index = 0;
static  gvar *	pcm1_rate_gv = (gvar *) NULL;
static  gvar *	pcm2_rate_gv = (gvar *) NULL;
static  gvar *	dwell_src_gv = (gvar *) NULL;


/*******************************************************************************

Procedure:

    init_tlm_util_ext ()

	Initializes mission-specific telemetry processing for the selected 
	spacecraft data base.


Purpose:

	The INIT_TLM_UTIL_EXT function performs mission-specific initialization
	required for telemetry processing.
	

    Invocation:

        status = init_tlm_util_ext (db_hd);

    where

		<db_hd>
			is the address of the head of the spacecraft data base.
		<status>
			returns the status of initializing mission-specific telemetry 
			processing, 0 if no errors occurred and -1 otherwise.

*******************************************************************************/

int init_tlm_util_ext (db_head *db_hd)
{
        pcm1_rate_gv = gv_id (db_hd, "PCM1_RATE");
        pcm2_rate_gv = gv_id (db_hd, "PCM2_RATE");
        dwell_src_gv = gv_id (db_hd, "DWELL_SRC");
	return (0);
}




/*******************************************************************************

Procedure:

    term_tlm_util_ext ()

	Terminates GOES-specific telemetry processing for the selected 
	spacecraft data base.


Purpose:

	The TERM_TLM_UTIL_EXT function performs any mission-specific termination
	required for telemetry processing.
	

    Invocation:

        status = term_tlm_util_ext (db_hd);

    where

		<db_hd>
			is the address of the head of the spacecraft data base.
		<status>
			returns the status of terminating mission-specific telemetry 
			processing, 0 if no errors occurred and -1 otherwise.

*******************************************************************************/

int term_tlm_util_ext (db_head *db_hd)
{
	if (db_hd && db_hd->tlm_head && db_hd->tlm_head->ext) {
		free (db_hd->tlm_head->ext);
	}
	return (0);
}

/*******************************************************************************

Procedure:

    get_sample_time ()

	Mission-specific telemetry time processing for the selected 
	spacecraft data base.


Purpose:

	The GET_SAMPLE_TIME function performs mission-specific calulation for the 
	sample time of the telemetry point.
	

    Invocation:

        get_sample_time(decom_node);

    where

		<decom_node>
			is the address of the current point decommutation node structure.


NOTE: This function is modified to include the parameter (mnemonic) timestamp. The Parameter
time stamp is calculated using the minor frame time stamp plus the bit offset of the point
within the frame and the downlink bit rate (an NTACTS ground database configuration parameter).

*******************************************************************************/

void get_sample_time(decom_node *current_node)
{

        tlm_value *value;
        double  d;
        int new_format;
        int i;
        struct timeval offset_time, ref_time;
        double num_of_bits_in_frame;
        double bit_rate, offset_secs;
        long int_bit_rate;
	char dwell_src[STRING_VAR_SIZE];

	/*
	value = (t_value *) current_node->cv;
	value->time = *process_frame_time;
	*/

	
	/* Use the sample time of the context point,if any, as the reference time
           IF it is a byte_arry.  For byte_array dependent points,
	   the bit offset is given from the start of the context point. 
           Also if this is dwell (context point is dwell_addr). 
	   If there is no context point, use the frame receipt time.	*/

	if (current_node->decom_ctx_pid) {

           if ( (current_node->decom_ctx_pid->tlm_point->tlm_type == BYTE_ARRAY) ||
                ( !(strcasecmp(current_node->decom_ctx_pid->tlm_point->tlm_mnemonic, 
                 "DWELL_ADDR"))) ||
		(strstr(current_node->decom_ctx_pid->tlm_point->tlm_mnemonic,
                 "MRO_STRUCTURE")) ){

	      value = (tlm_value *)current_node->decom_ctx_pid->value;
	      if (value)
			ref_time = value->time;
	      else
			ref_time = *process_frame_time;
              }
           else  
	      ref_time = *process_frame_time;
	}
	else
		ref_time = *process_frame_time;

        get_raw_value(tlm_hd->mode_pid, &d);
        new_format = (int) d;
 
        if (new_format != format) {
                format = new_format;
                for (i = 0;  i < tlm_hd->num_formats;  i++) {
                        if (((maf_format*)tlm_hd->maf_format_array[i])->format_code == format) {
                                format_index = i;
                                break;
                        }
                }
        }

       /*  
        *  All GOES MAFs should be timestamped.
        *  Only certain GOES MAFs should have time offsets from the
        *  start byte of the minor frame.  They are:
        *  0   PCM 1
        *  2   PCM 2
        *  3   software-generated Dwell frames
        *  12  MRSS ADS 
        *  13  MRSS AVS
        */

        if ( ((int)((maf_format*)tlm_hd->maf_format_array[format_index])->format_code == PCM1_MAF) || 
             ((int)((maf_format*)tlm_hd->maf_format_array[format_index])->format_code == PCM2_MAF) || 
             ((int)((maf_format*)tlm_hd->maf_format_array[format_index])->format_code == DWELL_MAF) || 
             ((int)((maf_format*)tlm_hd->maf_format_array[format_index])->format_code == ADS_MAF) || 
             ((int)((maf_format*)tlm_hd->maf_format_array[format_index])->format_code == AVS_MAF) ) {

           /*  If this is PCM 1 or 2 telemetry, check global for bit rate
            */
           if ((int)((maf_format*)tlm_hd->maf_format_array[format_index])->format_code == PCM1_MAF) { 
              int_bit_rate = 4;
	      if (pcm1_rate_gv != (gvar *) NULL) {
                 get_gv(pcm1_rate_gv, &int_bit_rate);
              }
              bit_rate = (double)((maf_format*)tlm_hd->maf_format_array[format_index])->rate * int_bit_rate;

           } 
           else if ((int)((maf_format*)tlm_hd->maf_format_array[format_index])->format_code == PCM2_MAF) {
              int_bit_rate = 4;
	      if (pcm2_rate_gv != (gvar *) NULL) {
                 get_gv(pcm2_rate_gv, &int_bit_rate);
              }
              bit_rate = (double)((maf_format*)tlm_hd->maf_format_array[format_index])->rate * int_bit_rate;
           }
           else if ((int)((maf_format*)tlm_hd->maf_format_array[format_index])->format_code == DWELL_MAF) {
              int_bit_rate = 4;
	      if (dwell_src_gv != (gvar *) NULL) {
		 get_gv(dwell_src_gv, dwell_src);
		 if (strlen(dwell_src) != 0) {
		    if (strstr(dwell_src, "1694") == NULL) {
	               if (pcm1_rate_gv != (gvar *) NULL) {
                          get_gv(pcm1_rate_gv, &int_bit_rate);
                       }
                    }
		    else {
	               if (pcm2_rate_gv != (gvar *) NULL) {
                          get_gv(pcm2_rate_gv, &int_bit_rate);
                       }
                    }
                 }
	      }
              bit_rate = (double)((maf_format*)tlm_hd->maf_format_array[format_index])->rate * int_bit_rate;
           }
           else   {  
              bit_rate = (double)((maf_format*)tlm_hd->maf_format_array[format_index])->rate;
           }    

           /*  
            *  Timestamp stream_source same as first data byte in frame; if past
            *  stream_source, subtract 1 byte from the database start bit 
            */
           if (current_node->start_byte > 0) 
              offset_secs = (double)(((current_node->start_byte -1) *8)/bit_rate);
           else
              offset_secs = (double)((current_node->start_byte *8)/bit_rate);

           offset_time = secs_to_timeval((double)offset_secs);

           value = (tlm_value *) current_node->cv;
           value->time = timeval_add(ref_time, offset_time);

           /*
	   printf("time before: sec=%d usecs=%d\n",process_frame_time->tv_sec, process_frame_time->tv_usec);
           printf("time after : sec=%d usecs=%d\n",value->time.tv_sec, value->time.tv_usec);
	   */
        }
        /*
         *   All other MAFS, timestamp each point with the original frame time
         *   with NO offset
         */
        else   {

           value = (tlm_value *) current_node->cv;
           value->time = ref_time;

        }

}

int
gps_leapseconds(unsigned long value)
{
return(0);
}

/*******************************************************************************

Procedure:

    met_to_timeval()

	Converts mission-specific MET (mission elapsed time) to UTC.


Purpose:

	The MET_TO_TIMEVAL function performs calculations to convert
	a specified MET (in seconds) to UTC.


    Invocation:

	status = met_to_timeval(met, converted_time);

    where

		<met>
			mission elapsed time in seconds.
		<converted_time>
			converted UTC time.
		<status>
			returns the status of the conversion, 0 if no errors
			occurred, and -1 otherwise.

*******************************************************************************/
int met_to_timeval(double met, struct timeval *converted_time)
{
	
	*converted_time = secs_to_timeval(met);

	return 0;
}


/*******************************************************************************

Procedure:

    timeval_to_met()

	Converts UTC time to mission-specific MET (mission elapsed time).


Purpose:

	The TIMEVAL_TO_MET function performs calculations to convert
	UTC time to specified MET (in seconds).


    Invocation:

	met = timeval_to_met(time);

    where

		<utc>
			universal time code.
		<met>
			mission elapsed time in seconds.

*******************************************************************************/
double timeval_to_met(struct timeval utc)
{
	return (timeval_to_secs(utc));
}
