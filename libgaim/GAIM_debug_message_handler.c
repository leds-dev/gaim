/*  
@(#)  File Name: GAIM_debug_message_handler.c  Release 1.3  Date: 00/09/19, 18:21:05
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       GAIM_debug_message_handler.c
         EXE. NAME       :       All GAIM executables
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles       Initial creation

********************************************************************************
	Invocation:
        
		GAIM_debug_message_handler(char *filename,char *message);
        
	Parameters:

		filename - I
			The name (including path) of the debug file.  A 0-length string
			should be passed if the messages are to be displayed to STDOUT.

		message - I
			The message to add to the debug file.

********************************************************************************
	Functional Description:

		This routine is called whenever a new debug message needs to be written
		to the debug file.  It time stamps the message, and keeps track of how 
		big the debug file is getting.  Once the debug file reaches 5MB, it 
		renames the file to filename.old.  This limiting size can be changed 
		by modifying the #define MAX_DEBUG_SIZE_BYTES value.
	
*******************************************************************************/

/**

PDL:
	IF the name iof the debug file is not null THEN
		Open the debug file.
		IF the size of the debug file is more than the MAX_DEBUG_SIZE_BYTES 
		 THEN
			Close the curent debug file.
			Rename the file to a .OLD file.
			Open a new debug file.
		ENDIF
		Write the debug message and a tiem stamp to the debug file.
		Close the debug file.
	ELSE
		Display the debug message to the screen.
	ENDIF

**/
#include <errno.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

#define MAX_DEBUG_SIZE_BYTES 5000000L

void GAIM_debug_message_handler(char *filename,char *message) {

	FILE *debug_file_ptr=0;
	size_t file_len=0;
	int status=0;
	char old_filename[256];
	time_t current_time;
	struct tm current_time_str;
	
	
	if (strlen(filename)!=0) {
			/* If a debug file name was specified */

		debug_file_ptr=0;
		debug_file_ptr=fopen(filename,"a");
		if (debug_file_ptr==0) {
			fprintf(stderr,"ERROR: Unable to open the current debug file %s\n",
				filename);
			fprintf(stderr,"       Message: %s\n",message);
					/* Open the debug file, for append. */
			return;
		}

		file_len=ftell(debug_file_ptr);
		if (status!=0) 
			fprintf(stderr,"ERROR: Unable to get file size for file %s\n",filename); 
				/* Get the length of the file -- the file pointer is 
					automatically set to the end of the file upon an open for 
					append. */
		
	
		if (file_len>MAX_DEBUG_SIZE_BYTES) {
				/* If the size of the debug file is too big, rename the file */	
			status=fclose(debug_file_ptr);
			debug_file_ptr=0;
			if (status!=0) fprintf(stderr,"ERROR: Unable to close debug file\n");
				/* Close the current debug file */
	
			strcpy(old_filename,filename);
			strcat(old_filename,".old");
				/* Make ther name of the .old file. */
	
			status=remove(old_filename);
			if (status!=0) 
				fprintf(stderr,"ERROR: Unable to remove old debug file %s ERRNO: %d!\n",
					old_filename, errno);
						/* Rename FILENAME to OLD_FILENAME */
	
			status=rename(filename,old_filename);
			if (status!=0) 
				fprintf(stderr,"ERROR: Unable to rename debug file %s to %s ERRNO: %d!\n",
					filename,old_filename, errno);
						/* Rename FILENAME to OLD_FILENAME */
			debug_file_ptr=fopen(filename,"a");
			if (debug_file_ptr==0) {
				fprintf(stderr,"ERROR: Unable to open a new debug file named %s\n",
					filename);
				fprintf(stderr,"       Message: %s\n",message);
			}
					/* Open the debug file, for append. */
		} 
	
		status=time(&current_time);
		if (status==-1) fprintf(stderr,"ERROR: Current time is UNKNOWN!\n");
		current_time_str=*localtime(&current_time);
			/* Get current time. */	
	
		if(debug_file_ptr)
		{
			status=fprintf(debug_file_ptr,"%4.4i-%3.3i-%2.2i:%2.2i:%2.2i  %s\n",
				current_time_str.tm_year+1900,current_time_str.tm_yday+1,
				current_time_str.tm_hour,current_time_str.tm_min,
				current_time_str.tm_sec,message);
			if (status<0) fprintf(stderr,"ERROR: Debug File Entry Write Error.\n"); 
				/* Write message to file */
		
			status=fclose(debug_file_ptr);
		}
		if (status!=0) fprintf(stderr,"ERROR: Unable to close debug file\n"); 
			/* Close the current debug file */

	} else {
			/* Display message to screen.  No time stamp. */

		printf("%s\n",message);
	}


}
