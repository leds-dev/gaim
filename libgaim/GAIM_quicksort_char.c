/*  
@(#)  File Name: GAIM_quicksort_char.c  Release 1.1  Date: 00/09/06, 12:27:46
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       GAIM_quicksort_char.c
         EXE. NAME       :       Playback_Update_Buffer
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        12/00       S. Scoles       Initial creation

********************************************************************************
	Invocation:
        
		GAIM_quicksort_char(int n, char **array);
        
	Parameters:

		n - I
			The number of points in array

		array - I
			The actual array

********************************************************************************
	Functional Description:

		This function runs the quicksort algorithm on a list of integers.

		The algorithm used is taken straight from page 154 of the book 
		"Introduction to Algorithms" by Thomas H. Cormen, CHarles E. Leiserson,
		and Ronald L. Rivest, 1991 printing.
	
*******************************************************************************/

#include <string.h>
void Quicksort_char(char **A, int p, int r, int *Z);
int Partition_char(char **A, int p, int r, int *Z);


/**

PDL for GAIM_quicksort_char:
	CALL Quicksort on the whole array.
	
**/		

void GAIM_quicksort_char(int n, char **array, int *Z) {

	Quicksort_char(array,0,n-1,Z);

}


/**

PDL for Quicksort_char:
	IF we have not reached the finest division possible between indices THEN
		CALL Partition_char to divide the array into two halves such that each 
		 element of A[p...q] is less than or equal to each element of 
         A[q+1...r].
		CALL Quicksort_char on the lower range.
		CALL Quicksort_char on the upper range.
	ENDIF
	
**/		

void Quicksort_char(char **A, int p, int r, int *Z) {

	int q;

	if (p<r){
		q=Partition_char(A,p,r,Z);
		Quicksort_char(A,p,q,Z);
		Quicksort_char(A,q+1,r,Z);
	}
}



/**

PDL for Partition_char:
	Select an element as a pivot point.
	DOWHILE we still need to parition
		Search the data of this partition until the current element is too 
		 large to belong to the bottom region.
		Search the data of this partition until the current element is too 
		 small to belong to the top region.
		IF all elements are not yet accounted for THEN
			Swap the two problem elements.
		ELSE
			RETURN the new pivot point.
		ENDIF
	ENDDO

**/		

int Partition_char(char **A, int p, int r, int *Z) {

	char x[256],*y;
	int i,j,z;
	int retval=-1;

	strcpy(x,A[p]);
	i=p;
	j=r;
	
	while (retval==-1) {
		while (strcmp(A[j],x)>0 && j>0) j--;
		while (strcmp(A[i],x)<0) i++;
		if (i<j) {
			y=A[i];
			A[i]=A[j];
			A[j]=y;
			if (Z) {
				z=Z[i];
				Z[i]=Z[j];
				Z[j]=z;
			}
		} else retval=j;
	}

	return retval;
}
