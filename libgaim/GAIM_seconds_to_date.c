/*  
@(#)  File Name: GAIM_seconds_to_date.c  Release 1.1  Date: 00/09/06, 12:27:46
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       GAIM_seconds_to_date.c
         EXE. NAME       :       Playback_Update_Buffer
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial conversion from PV-WAVE

********************************************************************************
	Invocation:
        
		GAIM_seconds_to_date(char *outdate,double secs);
        
	Parameters:

		outdate - O
			A pointer to where the user wants the date stored.

		secs - I
			The number of seconds since 1970.

********************************************************************************
	Functional Description:

		This function converts SECONDS FROM 1970 to a YYYY-DDD-HH:MM:SS.mmm
		date.

		This routine is based upon the SECONDS_TO_DATE.PRO routine from 
		the ABE product developed by Integral Systems and converted from
		PV-Wave Command Line Language to C by S. Scoles.
	
*******************************************************************************/

/**

PDL:
	Get the current number of years since 1970-001.
	Count the number of leap years since then.
	Subtract the total number of year-days and leapyear-days in seconds from 
	 the current total number of seconds.
	IF the current seconds count is less than zero THEN
		Decrement the year count.
		Add 365 days worth of seconds to the seconds count.
		IF the current year is a leap year THEN
			Add another days worth of seconds to the seconds count
		ENDIF
	ENDIF
	Calculate the number of days from the curent seconds count.
	Subtract the number of days in seconds from the seconds count.
	Calculate the number of hours from the curent seconds count.
	Subtract the number of hourss in seconds from the seconds count.
	Calculate the number of minutes from the curent seconds count.
	Subtract the number of minutes in seconds from the seconds count.
	Add 1970 to the year count.
	Convert this set of numbers into a date string to be returned to the 
	 calling program.
	
**/		
#include <stdio.h>

void GAIM_seconds_to_date(char *outdate,double secs) {

	int year,leapyears,day,hour,minute,currleap;
	double seconds=secs;

	year=(int)(seconds/(86400*365));
	leapyears=(year+1)/4;
	seconds-=(double)(year*86400*365+leapyears*86400);
		/* Calculate the number of years since 1970 */

	if (seconds<0) {
		year--;
		seconds+=365*86400;
		currleap=(year-2)%4;
		if (currleap==0) seconds+=86400;
			/* For 12/31/1972, and similar dates.  Not 12/31/1973 and similar 
				dates. */
	}

	day=(int)(seconds/86400);
	seconds-=(double)(day*86400);
	day++;
		/* Julian days start with 1. */

	hour=(int)(seconds/3600);
	seconds-=(double)(hour*3600);

	minute=(int)(seconds/60);
	seconds-=(double)(minute*60);

	year=year+1970;

	sprintf(outdate,"%4.4i-%3.3i-%2.2i:%2.2i:%2.2i.%3.3i",year,day,hour,minute,
		(int)seconds,(int)(1000*(seconds-(int)seconds)));
			/* Create the date. */

}
/*
void main(int n,char *a[]) {
	char buffer[256];
	int times[2]={983440000,983440800};
	int i;
	for (i=0; i<2; i++) {
		GAIM_seconds_to_date(buffer,times[i]);
		printf("%s\n",buffer);
	}
}
*/
