/*  
@(#)  File Name: GAIM_calculate_checksum.c  Release 1.1  Date: 00/09/06, 12:27:45
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       GAIM_calculate_checksum.c
         EXE. NAME       :       Playback_Update_Buffer, 
                                 Update_Buffer_Generator, 
                                 Update_Buffer_Sender
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial Creation

********************************************************************************
	Invocation:
        
		checksum=GAIM_calculate_checksum(int buffer_length,char *buffer);
        
	Parameters:

		buffer_length - I
			Size of the buffer to calculate the checksum for.

		buffer - I
			A pointer to the buffer.

		checksum - O
			The calculated checksum.

********************************************************************************
	Functional Description:

		To make a standard checksum calculation reoutine so that all GAIM 
		processes can base their checksums upon the same math.

*******************************************************************************/

/**

PDL:
	IF the buffer length is not a multiple of 4 THEN
		DOWHILE the length of the buffer is not a multiple of 4.
			Append a 0x00 byte to the buffer.
		ENDDO
	ENDIF

	Break the buffer into 4-byte units and add them together as series of long 
	 ints.

	RETURN the least significant 4 bytes of the sum as the CRC.

**/

int GAIM_calculate_checksum(int buffer_length,char *buffer) {

	int j;
		/* Counter */

	int crc=0;
		/* Calculated Check sum */

	if ((buffer_length%4)!=0) 
		for (j=0; j<4-(buffer_length%4); j++) buffer[buffer_length+j]=0;
			/* Zero bytes to end of double word */

	for (j=0; j<buffer_length; j+=4) crc+=*(int *)(&buffer[j]);
		/* Sum up the bytes */

	return crc;

}
