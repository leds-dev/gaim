/*  
@(#)  File Name: GAIM_quicksort_int.c  Release 1.1  Date: 00/09/06, 12:27:46
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       GAIM_quicksort_int.c
         EXE. NAME       :       Playback_Update_Buffer
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        12/00       S. Scoles       Initial creation

********************************************************************************
	Invocation:
        
		GAIM_quicksort_int(int n, int *array);
        
	Parameters:

		n - I
			The number of points in array

		array - I
			The actual array

********************************************************************************
	Functional Description:

		This function runs the quicksort algorithm on a list of integers.

		The algorithm used is taken straight from page 154 of the book 
		"Introduction to Algorithms" by Thomas H. Cormen, CHarles E. Leiserson,
		and Ronald L. Rivest, 1991 printing.
	
*******************************************************************************/

void Quicksort(int *A, int p, int r, int *Z);
int Partition(int *A, int p, int r, int *Z);


/**

PDL for GAIM_quicksort_int:
	CALL Quicksort on the whole array.
	
**/		

void GAIM_quicksort_int(int n, int *array, int *associated_data) {
		/* Set associated_data to NULL if thee is no associated data. */

	Quicksort(array,0,n-1,associated_data);

}


/**

PDL for Quicksort:
	IF we have not reached the finest division possible between indices THEN
		CALL Partition to divide the array into two halves such that each 
		 element of A[p...q] is less than or equal to each element of 
         A[q+1...r].
		CALL Quicksort on the lower range.
		CALL Quicksort on the upper range.
	ENDIF
	
**/		

void Quicksort(int *A, int p, int r, int *Z) {

	int q;

	if (p<r){
		q=Partition(A,p,r,Z);
		Quicksort(A,p,q,Z);
		Quicksort(A,q+1,r,Z);
	}
}



/**

PDL for Partition:
	Select an element as a pivot point.
	DOWHILE we still need to parition
		Search the data of this partition until the current element is too 
		 large to belong to the bottom region.
		Search the data of this partition until the current element is too 
		 small to belong to the top region.
		IF all elements are not yet accounted for THEN
			Swap the two problem elements.
		ELSE
			RETURN the new pivot point.
		ENDIF
	ENDDO

**/		

int Partition(int *A, int p, int r, int *Z) {

	int x,y;
	int i,j;
	int retval=-1;

	x=A[p];
	i=p;
	j=r;
	
	while (retval==-1) {
		while (A[j]>x && j>0) j--;
		while (A[i]<x) i++;
		if (i<j) {
			y=A[i];
			A[i]=A[j];
			A[j]=y;
			if (Z) {
				y=Z[i];
				Z[i]=Z[j];
				Z[j]=y;
			}
		} else retval=j;
	}

	return retval;
}
