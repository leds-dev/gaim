/*  
@(#)  File Name: GAIM_binarysearch_int.c  Release 1.1  Date: 00/09/06, 12:27:46
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       GAIM_binarysearch_int.c
         EXE. NAME       :       Playback_Update_Buffer
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        12/00       S. Scoles       Initial creation

********************************************************************************
	Invocation:
        
		retval=GAIM_binarysearch_int(int n, int *A, int k);
        
	Parameters:

		retval - O
			The index of the element, or -1 if not found.

		n - I
			The number of points in array

		k - I
			The key to find

		A - I
			The actual array

********************************************************************************
	Functional Description:

		This function runs the binary search algorithm on a list of integers.
	
*******************************************************************************/

/**

PDL:

	DOWHILE the difference between the upper and lower bounds of our search 
	 range is greater than one.
		Calculated the center element of our range.
		IF the center element matches our search integer THEN 
			return the index of the center element
		ELSEIF the value of our key is more than the value of the center 
		 element THEN
			set the lower bound of our search range to the center element.
		ELSE 
			set the upper bound of our search range to the center element.
		ENDIF
	ENDDO

	IF the value of the lower bound of our search range matches the search key 
	 THEN
		Return the index of the lower bound.  
	ENDIF

	IF the value of the upper bound of our search range matches the search key 
	 THEN
		Return the index of the upper bound.  
	ENDIF

	IF no matches were found THEN
		Return a '-1'
	ENDIF

	
**/		

int GAIM_binarysearch_int(int n, int *A, int k) {

	int ll=0,ul=n-1,c;

	int retval=-1;

	while (ul-ll>1 && retval==-1) {
		c=(ll+ul)/2;
		if (A[c]==k) retval=c;
		else if (A[c]<k) ll=c;
		else ul=c;
	}

	if (A[ll]==k) retval=ll;
	if (A[ul]==k) retval=ul;

	return retval;
}

