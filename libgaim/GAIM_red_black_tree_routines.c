/*  
@(#)  File Name: GAIM_red_black_tree_routines.c  Release 1.2  Date: 00/11/01, 13:36:01
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       GAIM_red_black_tree_routines.c
         EXE. NAME       :       Archive_Manager, DCR_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        04/00       S. Scoles       Initial creation
         V4.0        01/17       A. Raj          Issue #883
         V4.0        05/13/02    A. Raj          SSGS issue-072

********************************************************************************
	Invocation:
        
        
	Parameters:


********************************************************************************
	Functional Description:

	A set of functions to support the red-black tree data structure of 
	filenames already ftp'ed.

	No node deletion routines are included, since they will never be needed.

	Why Red-Black Trees?  If one uses a standard binary tree, the tree will be 
	come very "unbalanced" since all new file names will be added to the right 
	of most branches.  Very little, if anything, will be added to the left 
	branch.

	What about non-tree data structures?  As the list gets longer, the rate 
	of retrieval will be slower.  Even if we sort and save.  Sorting takes 
	O(nlogn) time.  Trees take O(logn) to search through them.  O(1) to add 
	another node to them.  It still would take O(nlogn) if someone wanted to 
	display the nodes in sorted order.  But since we are only searching and 
	adding single nodes, and not full trees, we can accomplish whatever we 
	want in O(logn) time.  
	
*******************************************************************************/

#include <stdio.h>

#define GAIM_MAIN

/* #define TEST */
#ifdef TEST
#include "../include/GAIM_red_black_tree_support.h"
#else
#include "GAIM_red_black_tree_support.h"
#endif
	/* Defines, globals, and statics */

void GAIM_inorder_tree_find_new_nodes(struct red_black_tree_node *x,
		struct red_black_tree_node *new_nodes);
void GAIM_rbt_write_file(struct red_black_tree_node **root,char *filename);
void GAIM_rbt_right_rotate(struct red_black_tree_node **root,
		struct red_black_tree_node *x);
int GAIM_tree_search(struct red_black_tree_node *x,char *k);
void GAIM_rbt_read_file(struct red_black_tree_node **root,char *filename);
void GAIM_tree_insert(struct red_black_tree_node **root,
		struct red_black_tree_node *z);
void GAIM_rbt_insert(struct red_black_tree_node **root,char *new_item,
		struct red_black_tree_node *x,int init_node_set);
void GAIM_rbt_left_rotate(struct red_black_tree_node **root,
		struct red_black_tree_node *x);
void GAIM_inorder_tree_display(struct red_black_tree_node *x);
void GAIM_inorder_tree_write(struct red_black_tree_node *x,FILE *fp);
			/* Prototypes */


/******************************************************************************/

/**

PDL for GAIM_rbt_left_rotate:
	Set the Y node to the right child of the current node.
	Set the right child of the current node to the left child of the Y node.
	IF the left child of the Y node exists THEN
		Set the parent of the left child of the Y node to the current node.
	ENDIF
	Set the parent of the Y node to the parent of the current node.
	IF the parent of the current node does not exist THEN
		Set the root pointer to the Y node.
	ELSE
		IF the parent of the left child of the current node is the same as the 
		 current node THEN
			Set the parent of the left child of the current node to the Y node.
		ELSE
			Set the parent of the right child of the current node to the Y node.
		ENDIF
	ENDIF
	Set the left child of the Y node to the X node.
	Set the parent of the X node to the Y node.


**/

void GAIM_rbt_left_rotate(struct red_black_tree_node **root,
		struct red_black_tree_node *x) {

	struct red_black_tree_node *y;

	y=x->right;
	x->right=y->left;
	if (y->left!=0) y->left->p=x;
	y->p=x->p;
	if (x->p==0) *root=y;
	else {
		if (x==x->p->left) x->p->left=y;
		else x->p->right=y;
	}
	y->left=x;
	x->p=y;

}


/******************************************************************************/

/**

PDL for GAIM_rbt_right_rotate:
	Set the Y node to the left child of the current node.
	Set the left child of the current node to the right child of the Y node.

	IF the right child of the Y node exists THEN
		Set the parent of the right child of the Y node to the current node.
	ENDIF

	Set the parent of the Y node to the parent of the current node.

	IF the parent of the current node does not exist THEN 
		Set the root to the Y node.
	ELSE
		IF the current node is the right child of it's parent THEN
			Set the right child of the paent of the current node to the Y node.
		ELSE
			Set the left child of the parent of the current node to the Y node.
		ENDIF
	ENDIF

	Set the right child of the Y node to the curent node.
	Set the parent of the current node to the Y node.

**/

void GAIM_rbt_right_rotate(struct red_black_tree_node **root,
		struct red_black_tree_node *x) {

	struct red_black_tree_node *y;

	y=x->left;
	x->left=y->right;
	if (y->right!=0) y->right->p=x;
	y->p=x->p;
	if (x->p==0) *root=y;
	else {
		if (x==x->p->right) x->p->right=y;
		else x->p->left=y;
	}
	y->right=x;
	x->p=y;
}


/******************************************************************************/

/**

PDL for GAIM_rbt_insert:
	Allocate space for the new node.
	IF the inex of the current node is zero THEN
		Set the root pointer to the current node.
	ELSE
		CALL GAIM_tree_insert 
	ENDIF
	IF the current node is not the root THEN
		DOWHILE the current node is not the root and the color of the parent of
		 the current node is RED
			IF the parent of the current node is the same as the left child of 
			 the grandparent of the current node THEN
				Set the Y node to the right child of the grandparent of the 
				 current node.
				IF the Y node exists THEN
					IF the color of the Y node is RED THEN
						Set the color of the parent of the current node to 
						 BLACK.
						Set the color of the Y node to BLACK.
						Set the color of the grandparent of the current node 
						 to RED.
						Set the current node to the grandparent of the 
						 current node.
					ELSE
						IF the current node is the right child of it's parent 
						 THEN
							Set the cuurrent node to the parent of the current 
							 node.
							CALL GAIM_rbt_left_rotate
						ENDIF
						Set the color of the parent of the current node to 
						 BLACK,
						Set the color of the grandparent of the current node 
						 to RED.
						CALL GAIM_rbt_right_rotate
					ENDIF
				ELSE
					IF the current node is the right child of it's parent THEN
						Set the current node to the parent of the current node.
						CALL GAIM_rbt_left_rotate
					ENDIF
					Set the color of the parent of the current node to BLACK.
					Set the color of the grandparent of the current node to 
					 RED.
					CALL GAIM_rbt_right_rotate
				ENDIF
			ELSE
				Set the Y node to the left child of the grandparent of the 
				 current node.
				IF the Y node exists THEN
					IF the color of the Y node is RED THEN
						Set the color of the parent curent node to BLACK.
						Set the color of the Y node to BLACK.
						Set the color of the grandparent of the current node 
						 to RED.
						Set the current node to the grandparent of the current
						 node.
					ELSE
						IF the current node is the left child of it's parent 
						 THEN
							Set the current node to the parent of the current 
							 node.
							CALL GAIM_rbt_right_rotate
						ENDIF
						Set the color of the parent of the current node to 
						 BLACK.
						Set the color of the grandparent of the current node 
						 to RED.
						CALL GAIM_rbt_left_rotate
					ENDIF
				ELSE
					IF the current node is the left child of it's parent THEN
						Set the current node to the parent of the current node.
						CALL GAIM_rbt_right_rotate
					ENDIF
					Set the color of the parent of the current node to BLACK.
					Set the color of the grandparent of the current node to 
					 RED.
					CALL GAIM_rbt_left_rotate
				ENDIF
			ENDIF
		ENDDO
	ENDIF
	Set the color of the root to BLACK.

**/

void GAIM_rbt_insert(struct red_black_tree_node **root,char *new_item,
		struct red_black_tree_node *x,int init_node_set) {

	struct red_black_tree_node *y;


	if (init_node_set) red_black_node_count=0;
		/* Initialize this variable */

	x=(struct red_black_tree_node *)calloc(1,
		sizeof(struct red_black_tree_node));
	x->my_index=red_black_node_count;
	x->left=0;
	x->left_index=-1;
	x->right=0;
	x->right_index=-1;
	x->p=0;
	x->p_index=-1;
	strcpy(x->key,new_item);
	red_black_node_count++;
	new_red_black_nodes++;
		/* Allocate the space needed for a new node */

	if (x->my_index==0) *root=x;
	else GAIM_tree_insert(root,x);
		/* If this is the first element, it becomes ROOT, otherwise, an insert 
			is needed */

	x->color=GAIM_RBT_RED;
	if (x!=*root) while (x!=*root && x->p->color==GAIM_RBT_RED) {
		if (x->p==x->p->p->left) {
			y=x->p->p->right;
			if (y!=0) {
				if (y->color==GAIM_RBT_RED) {
					x->p->color=GAIM_RBT_BLACK;
					y->color=GAIM_RBT_BLACK;
					x->p->p->color=GAIM_RBT_RED;
					x=x->p->p;
				} else {
					if (x==x->p->right) {
						x=x->p;
						GAIM_rbt_left_rotate(root,x);
					}
					x->p->color=GAIM_RBT_BLACK;
					x->p->p->color=GAIM_RBT_RED;
					GAIM_rbt_right_rotate(root,x->p->p);
				}
			} else {
				if (x==x->p->right) {
					x=x->p;
					GAIM_rbt_left_rotate(root,x);
				}
				x->p->color=GAIM_RBT_BLACK;
				x->p->p->color=GAIM_RBT_RED;
				GAIM_rbt_right_rotate(root,x->p->p);
			}
		} else {
			y=x->p->p->left;
			if (y!=0) {
				if (y->color==GAIM_RBT_RED) {
					x->p->color=GAIM_RBT_BLACK;
					y->color=GAIM_RBT_BLACK;
					x->p->p->color=GAIM_RBT_RED;
					x=x->p->p;
				} else {
					if (x==x->p->left) {
						x=x->p;
						GAIM_rbt_right_rotate(root,x);
					}
					x->p->color=GAIM_RBT_BLACK;
					x->p->p->color=GAIM_RBT_RED;
					GAIM_rbt_left_rotate(root,x->p->p);
				}
			} else {
				if (x==x->p->left) {
					x=x->p;
					GAIM_rbt_right_rotate(root,x);
				}
				x->p->color=GAIM_RBT_BLACK;
				x->p->p->color=GAIM_RBT_RED;
				GAIM_rbt_left_rotate(root,x->p->p);
			}
		}
	}
	(*root)->color=GAIM_RBT_BLACK;
}


/******************************************************************************/

/**

PDL for GAIM_tree_insert:
	Set the current node to the root.
	DOWHILE the current node exists.
		Set the Y node to the current node.
		IF the key of the new node comes before the key of the current node in 
		 the alphabet THEN
			Set the current node to the left child of the current node.
		ELSE
			Set the current node to the right child of the current node.
		ENDIF
	ENDDO
	Set the parent of the new node to the Y node.
	IF the Y node exists THEN
		Set the root to the new node.
	ELSE
		IF the key of the new node comes before the key of the Y node in the 
		 alphabet THEN
			Set the left child of the Y node to the new node.
		ELSE
			Set the right child of the Y node to the new node.
		ENDIF
	ENDIF

**/

void GAIM_tree_insert(struct red_black_tree_node **root,
		struct red_black_tree_node *z) {

	struct red_black_tree_node *x,*y;


	y=0;
	x=*root;
	while (x!=0) {
		y=x;
		if (strcmp(z->key,x->key)<0) x=x->left;
		else x=x->right;
	}
	z->p=y;
	if (y==0) *root=z;
	else {
		if (strcmp(z->key,y->key)<0) y->left=z;
		else y->right=z;
	}

}


/******************************************************************************/

/**

PDL for GAIM_tree_search:
	IF the curent node does not exist THEN
		RETURN -1
	ENDIF

	IF the search key matches the current key THEN
		RETURN the index of the current node.
	ENDIF

	IF the search key comes before the current key in the alphabet THEN
		RETURN the value returned by a recursive CALL to GAIM_tree_search using 
		 the left child of the current node as the current node for the next 
		 execution.
	ELSE
		RETURN the value returned by a recursive CALL to GAIM_tree_search using 
		 the right child of the current node as the current node for the next 
		 execution.
	ENDIF

**/

int GAIM_tree_search(struct red_black_tree_node *x,char *k) {
		/* Returns MY_INDEX or -1 */

	if (x==0) return -1;
	if (strcmp(k,x->key)==0) return x->my_index;
	if (strcmp(k,x->key)<0) return GAIM_tree_search(x->left,k);
	else return GAIM_tree_search(x->right,k);

}


/******************************************************************************/

/**

PDL for GAIM_rbt_read_file:
	IF the fule does not exist or is less than 4 bytes long THEN 
		Restore the file from backup -- the program must have crashed the last 
		 time it was run.
	ENDIF

	IF the file exists and has data THEN
		Read the file and restore all the pointers based upon the indexes 
		 stored in the file
		After the file has been completely loaded, close it.
	ENDIF

**/

void GAIM_rbt_read_file(struct red_black_tree_node **root,char *filename) {

	FILE *fp;
		/* file pointer to the filename */

	int total_count;
		/* Total red-black nodes */

	fpos_t file_pos;
		/* File Position */

	int tempint,mynum;
		/* Temp INTs */

	char temp_file_name[256],temp_cmd[256];
		/* Temporary Strings */

	int i;
		/* Counter */


	new_red_black_nodes=0;
	fp=fopen(filename,"ab");
	fgetpos(fp,&file_pos);
	fclose(fp);
		/* How big is this file? */

	if (file_pos<=4) {
			/* If file does not exist, or is 4 bytes or less, search for backup 
				file */

		sprintf(temp_file_name,"%s.bak",filename);
		fp=fopen(temp_file_name,"rb");

		if (fp) {
				/* If the backup file exists, copy it, and open it. */

			fclose(fp);
			sprintf(temp_cmd,"cp %s %s",temp_file_name,filename);
			system(temp_cmd);
				/* Copy the backup file */

		}
	}

	fp=fopen(filename,"ab");
	fgetpos(fp,&file_pos);
	fclose(fp);

	if (file_pos>4) {
			/* If the file exists and has data */

		fp=fopen(filename,"rb");
			/* Seek the beginning of the file */

		fread(&red_black_node_count,sizeof(int),1,fp);
			/* Read the number of nodes in the file */

		ftped_files=(struct red_black_tree_node *)calloc(red_black_node_count,
			sizeof(struct red_black_tree_node));

		for (i=0; i<red_black_node_count; i++) {

			fread(&mynum,sizeof(int),1,fp);
         if (mynum < red_black_node_count)  /* ARaj - this will avoid core dump */
         {
			   ftped_files[mynum].my_index=mynum;

			   fread(&tempint,sizeof(int),1,fp);
			   if (tempint>=0) {
				   ftped_files[mynum].p=&ftped_files[tempint];
				   ftped_files[mynum].p_index=tempint;
			   } else {
				   ftped_files[mynum].p=0;
				   ftped_files[mynum].p_index=-1;
			   }
			   if (tempint==-1) *root=&ftped_files[mynum];
   
			   fread(&tempint,sizeof(int),1,fp);
			   if (tempint>=0) {
				   ftped_files[mynum].left=&ftped_files[tempint];
				   ftped_files[mynum].left_index=tempint;
			   } else {
				   ftped_files[mynum].left=0;
				   ftped_files[mynum].left_index=-1;
			   }

			   fread(&tempint,sizeof(int),1,fp);
			   if (tempint>=0) {
				   ftped_files[mynum].right=&ftped_files[tempint];
				   ftped_files[mynum].right_index=tempint;
			   } else {
				   ftped_files[mynum].right=0;
				   ftped_files[mynum].right_index=tempint;
			   }

			   fread(&ftped_files[mynum].color,sizeof(int),1,fp);
			   fread(ftped_files[mynum].key,sizeof(char),MAX_FILENAME_LEN,fp);

         } /* end if mynum check -- */
         else
         {
            red_black_node_count=0; /* ARaj -- */
         }
		} /* end for loop -- */

		fclose(fp);
	} /* end of file_pos check -- */

}


/******************************************************************************/

/**

PDL for GAIM_rbt_write_file:
	Open the output file.
	Write the number of nodes to the file.
	Process the tree in-order and write to the file.
	Close the file.
	Free the memory.
	Nullify the root pointer.
	Backup the file.

**/

void GAIM_rbt_write_file(struct red_black_tree_node **root,char *filename) {

	FILE *file_ptr;
		/* file pointer to the filename */

	char temp_file_name[256],temp_cmd[256];
		/* Temporary Strings */


	file_ptr=fopen(filename,"wb");
		/* Open the file */

	fwrite(&red_black_node_count,sizeof(int),1,file_ptr);
		/* Write the number of nodes to the file */

	GAIM_inorder_tree_write(*root,file_ptr);
		/* Write the data to the file */

	fclose(file_ptr);
		/* Close the file. */

	free(ftped_files);
		/* Free the nodes */

	*root=0;
		/* Nullify the root pointer. */

	sprintf(temp_file_name,"%s.bak",filename);
	sprintf(temp_cmd,"cp %s %s",filename,temp_file_name);
	system(temp_cmd);
		/* Backup the file */

}


/******************************************************************************/

/**

PDL for GAIM_inorder_tree_write:
	IF the current node exists THEN
		CALL GAIM_inorder_write using the left child of the current node as the
		 next current node.
		Write the current index, parent index, left child index, and right
		 child index to the file.
		Write the color of the node to the file.
		Write the key to the file.
		IF the currnt node is not in the same memory space as the root node 
		 (i.e. the node is new since the lase file read) THEN 
			Free the memory used by the node.
		ENDIF
		CALL GAIM_inorder_tree_write using the right child of the current node 
		 as the next current node.
	ENDIF

**/

void GAIM_inorder_tree_write(struct red_black_tree_node *x,FILE *fp) {

	int minusone=-1;

	int bytes;

	if (x!=0) {
		GAIM_inorder_tree_write(x->left,fp);

		fwrite(&x->my_index,sizeof(int),1,fp);

		if (x->p) fwrite(&x->p->my_index,sizeof(int),1,fp);
		else fwrite(&minusone,sizeof(int),1,fp);

		if (x->left) fwrite(&x->left->my_index,sizeof(int),1,fp);
		else fwrite(&minusone,sizeof(int),1,fp);

		if (x->right) fwrite(&x->right->my_index,sizeof(int),1,fp);
		else fwrite(&minusone,sizeof(int),1,fp);

		fwrite(&x->color,sizeof(int),1,fp);

		fwrite(x->key,sizeof(char),MAX_FILENAME_LEN,fp);

/* *** ISSUE #883: 01-17-02

		if (x->my_index>=red_black_node_count-new_red_black_nodes) free(x);
		NOTE: Line commented out -- possible problem when used in the next
		      line of code [GAIM_inorder_tree_write(x->right,fp);}.  Do the
		      newly added nodes nedd to be freed elsewhere in order to
		      prevent a memory leak? Unknown.

**** END ISSUE #883 */

		GAIM_inorder_tree_write(x->right,fp);

	}

}


/******************************************************************************/

/**

PDL for GAIM_inorder_tree_find_new_nodes:
	IF the current node exists THEN
		CALL GAIM_inorder_tree_find_new_nodes using the left child of the 
		 current node as the next current node.
		IF the current node index is the same as the total node count THEN
			Copy the new node into an array.
		ENDIF
		CALL GAIM_inorder_tree_find_new_nodes using the right child of the 
		 current node as the next current node.
	ENDIF

**/

void GAIM_inorder_tree_find_new_nodes(struct red_black_tree_node *x,
		struct red_black_tree_node *new_nodes) {

	int new_index;
		/* new_nodes index of points found */


	if (x!=0) {
		GAIM_inorder_tree_find_new_nodes(x->left,new_nodes);
			/* Walk down the left branch first */

		if (x->my_index>=red_black_node_count) {
				/* If a new node is found, need to copy the data so fwrite can 
					work fast -- on a continuous block of memory */

			new_index=x->my_index-red_black_node_count;
			new_nodes[new_index].color=x->color;
			new_nodes[new_index].my_index=x->my_index;
			strcpy(new_nodes[new_index].key,x->key);
			if (x->left!=0) new_nodes[new_index].left_index=x->left->my_index;
			else new_nodes[new_index].left_index=-1;
			if (x->right!=0) 
				new_nodes[new_index].right_index=x->right->my_index;
			else new_nodes[new_index].right_index=-1;
			if (x->p!=0) new_nodes[new_index].p_index=x->p->my_index;
			else new_nodes[new_index].p_index=-1;
				/* Copy the node to our new nodes list */

			if (x->right!=0) x->right->p_index=x->my_index;
			if (x->left!=0) x->left->p_index=x->my_index;
			if (x->p!=0) {
				if (x->p->right==x) x->p->right_index=x->my_index;
				if (x->p->left==x) x->p->left_index=x->my_index;
			}

		}

		GAIM_inorder_tree_find_new_nodes(x->right,new_nodes);
			/* Walk down thew right branch */

		if (x->my_index>=red_black_node_count) free(x);
			/* Need to free this pointer once it is inserted into our array.  
				Hopefully this will prevent any memory leaks.  We will see. */	

	}
}

/******************************************************************************/

/**

PDL for GAIM_inorder_tree_display:
	IF the current node exists THEN
		Call GAIM_inorder_tree_display using the left node as the next current node.
		Display the current node.
		Call GAIM_inorder_tree_display using the right node as the next current node.
	ENDIF

**/

void GAIM_inorder_tree_display(struct red_black_tree_node *x) {

	if (x!=0) {
		GAIM_inorder_tree_display(x->left);
		printf("%i: %s",x->my_index,x->key);
		if (x->left) printf("  Lindex: %i",x->left->my_index);
		if (x->p) printf("  Parent: %i",x->p->my_index);
		if (x->right) printf("  Rindex: %i",x->right->my_index);
		printf("\n");
		GAIM_inorder_tree_display(x->right);
	}

}

#ifdef TEST

void main() {

	struct red_black_tree_node *root=0;
	struct red_black_tree_node *new_node=0;

	int i,j,nilleft=0,nilright=0;

	char add[256]="AAAAAA";

	root=0;

	GAIM_rbt_insert(&root,add,new_node,INIT_NEW_NODES);
	for (i=1; i<1000; i++) {
		add[5]++;

		for (j=5; j>=0; j--) {
			if (add[j]>'Z') {
				add[j]='A';
				add[j-1]++;
			}
		}
		GAIM_rbt_read_file(&root,"test.file");
		GAIM_rbt_insert(&root,add,new_node,ADD_TO_NEW_NODES);
		GAIM_rbt_write_file(&root,"test.file");
		printf("%3i. RBN: %i (%s)\n",i,red_black_node_count,add); 
	}

}

#endif
