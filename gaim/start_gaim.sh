#!/bin/sh
#   @(#)  File Name: start_gaim.sh  Release 1.9  Date: 07/04/13, 10:52:51  
#
# VER.        DATE           BY           COMMENT
#
# 9C          Jul21, 2005
# 10A         Sep12, 2005
# 11          Mar28, 2007    K. Dolinh    add GAIM_FTP_MODE
#                                         cleanup ftp directory

# define env variables for gaim
# comment out line to disable function

# remove shared mem
ipcrm.sh

# global vars prefix (sarc01->GAIM1 and sarc02->GAIM2)
GAIM_NAME="GAIM2"; export GAIM_NAME; 
GAIM_NAME="GAIM1"; export GAIM_NAME; 

# home directory for gaim
GAIM_HOME=$HOME/gaim; export GAIM_HOME

# max frames to output to *tlmwatch.log
GAIM_TLMWATCH_MAX=10000; export GAIM_TLMWATCH_MAX
GAIM_TLMWATCH_MAX=0; export GAIM_TLMWATCH_MAX

# create a new event logfile daily
GAIM_LOGFILE_DAILY=1; export GAIM_LOGFILE_DAILY

# update gaim stats in global vars every N seconds
GAIM_STATUS_INTERVAL=10; export GAIM_STATUS_INTERVAL

# update global variables on GTACS server
#GAIM_UPDATE_GV="YES"; export GAIM_UPDATE_GV

# output general debug messages
#GAIM_DEBUG="YES"; export GAIM_DEBUG

# output GAS debug messages
#GAIM_DEBUG_GAS="YES"; export GAIM_DEBUG_GAS

# output GVA debug messages
GAIM_DEBUG_GVA="YES"; export GAIM_DEBUG_GVA

# interval between ftp of pseudo checkpoint file (in seconds) 
GAIM_PSEUDO_FTP_RATE=60; export GAIM_PSEUDO_FTP_RATE

# ftp mode =  FTP_PSEUDO_ONLY, FTP_GVA_ONLY, FTP_PSEUDO_GVA
GAIM_FTP_MODE=FTP_GVA_ONLY; export GAIM_FTP_MODE
GAIM_FTP_MODE=FTP_PSEUDO_ONLY; export GAIM_FTP_MODE
GAIM_FTP_MODE=FTP_PSEUDO_GVA; export GAIM_FTP_MODE

# playback debug mode choose ONE only
# GAIM_PLAYBACK_PSEUDO_ONLY="YES"; export GAIM_PLAYBACK_PSEUDO_ONLY
# GAIM_PLAYBACK_GVA_ONLY="YES"; export GAIM_PLAYBACK_GVA_ONLY

# interval between ping of GAS server (in seconds) 
ARCHIVE_PING_RATE=60; export ARCHIVE_PING_RATE

# 
MAX_BYTES_PER_SEC=120000;export MAX_BYTES_PER_SEC

# processing if frame has bad timetag 
# (frame start_time is < previous_frame stop_time)
#GAIM_FRAME_BAD_TIMETAG="DROP";export GAIM_FRAME_BAD_TIMETAG
#GAIM_FRAME_BAD_TIMETAG="WARN";export GAIM_FRAME_BAD_TIMETAG
GAIM_FRAME_BAD_TIMETAG="OFF";export GAIM_FRAME_BAD_TIMETAG

#PSEUDO_TIME_EQ_FRAME_TIME="yes"; export PSEUDO_TIME_EQ_FRAME_TIME
#GAIM_REVERSE_TLM="NO"
#GAIM_PUB_DELAY_USECS=1000; export GAIM_PUB_DELAY_USECS

# unused env variables 
# NO_GOES_ARCHIVE=Y; export NO_GOES_ARCHIVE
# ALTERNATE_SERVICE="none"; export ALTERNATE_SERVICE
# MERGE_TYPE="none", export MERGE_TYPE
# STREAM_TYPE="none", export STREAM_TYPE
# INCLUDE_DEBUG_DECOM_INFO="none"; export INCLUDE_DEBUG_DECOM_INFO
# DEBUG_FILE="none"; export DEBUG_FILE
#GAIM_DEBUG_MAX=1000; export GAIM_DEBUG_MAX
#GAIM_LOGFILE_MAX=16000000; export GAIM_LOGFILE_MAX

# clean up ftp directory
cd $GAIM_HOME/ftp
rm -f *.STOP
rm -f *.out
mv *.sh  SAVE 2>/dev/null
mv *.def SAVE 2>/dev/null
mv *.gva SAVE 2>/dev/null

# start Interface Manager 
cd $GAIM_HOME/bin
./N-Q_Interface_Manager  &
