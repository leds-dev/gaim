#!/bin/ksh

no_gaim="TRUE"
echo "===================================="
echo "             Bringing GAIM down ..."
echo "===================================="

for i in `ps -ef | grep N-Q_Interface_Manager | grep -v grep | awk '{print $2}'`
do
	kill -9 $i
        no_gaim=FALSE
done

for i in `ps -ef | grep Check_GTACS | grep -v grep | awk '{print $2}'`
do
        kill -9 $i
        no_gaim=FALSE
done

for i in `ps -ef | grep Update_Buffer_Sender | grep -v grep | awk '{print $2}'`
do
        kill -9 $i
        no_gaim=FALSE
done

sleep 1

for i in `ps -ef | grep Update_Buffer_Generator | grep -v grep | awk '{print $2}'`
do
        kill -9 $i
        no_gaim=FALSE
done

for i in `ps -ef | grep Playback_Update_Buffer | grep -v grep | awk '{print $2}'`
do
        kill -9 $i
        no_gaim=FALSE
done

for i in `ps -ef | grep Stream_Data_Receiver | grep -v grep | awk '{print $2}'`
do
        no_gaim=FALSE
        kill -9 $i
done

if [ $no_gaim == "TRUE" ] then
   echo "             GAIM is not UP..."
   echo "===================================="
else
   echo "             GAIM is DOWN..."
   echo "===================================="
fi

