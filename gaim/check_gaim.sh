#!/bin/sh
# 
#

no_gaim="TRUE"

echo " "
echo "   GAIM main processes "
echo "---------------------------"

#val=`ps -ewo time,comm | grep -v grep | grep N-Q_Interface_Manager`  
val=`ps -ef | grep -v grep | grep N-Q_Interface_Manager | awk '{print($7 " " $8)}'`
echo  $val > /tmp/check_gaim.out
numl=`wc -l < /tmp/check_gaim.out`
if [ $numl -gt 0 ] 
then	
        no_gaim=FALSE
	printf "%s %s\n"  $val 
fi

#val=`ps -ewo time,comm | grep -v grep | grep Check_GTACS` 
val=`ps -ef | grep -v grep | grep Check_GTACS | awk '{print($7 " " $8)}'`
echo  $val > /tmp/check_gaim.out
numl=`wc -l < /tmp/check_gaim.out`
if [ $numl -gt 0 ]
then	
        no_gaim=FALSE
	printf "%s %s\n"  $val 
fi

#val=`ps -ewo time,comm | grep -v grep | grep Update_Buffer_Sender` 
val=`ps -ef | grep -v grep | grep Update_Buffer_Sender | awk '{print($7 " " $8)}'`
echo $val > /tmp/check_gaim.out
numl=`wc -l < /tmp/check_gaim.out`
if [ $numl -gt 0 ]
then	
        no_gaim=FALSE
	printf "%s %s\n" $val 
fi

# printf "\n"
echo " "
echo "   GAIM archive processes "
echo "---------------------------"

#val=`ps -ewo time,args | grep -v grep | grep Stream_Data_Receiver` 
val=`ps -ef | grep -v grep | grep Stream_Data_Receiver | awk '{print($7 " " $8)}'`
echo  $val > /tmp/check_gaim.out
numl=`wc -l < /tmp/check_gaim.out`
if [ $numl -gt 0 ] 
then	
        no_gaim=FALSE
	printf "%s %s %s %s %s %s %s %s\n" $val 
fi

#val=`ps -ewo time,args | grep -v grep | grep Update_Buffer_Generator` 
val=`ps -ef | grep -v grep | grep Update_Buffer_Generator | awk '{print($7 " " $8)}'`
echo $val > /tmp/check_gaim.out
numl=`wc -l < /tmp/check_gaim.out`
if [ $numl -gt 0 ] 
then	
        no_gaim=FALSE
	printf "%s %s %s %s\n" $val 
fi

#val=`ps -ewo time,comm | grep -v grep | grep Playback_Update_Buffer` 
val=`ps -ef | grep -v grep | grep Playback_Update_Buffer | awk '{print($7 " " $8)}'`
echo  $val > /tmp/check_gaim.out
numl=`wc -l < /tmp/check_gaim.out`
if [ $numl -gt 0 ] 
then	
        no_gaim=FALSE
	printf "%s %s\n" $val 
fi

if [ $no_gaim == "TRUE" ] 
then
   echo "  "
   echo "*** GAIM is not UP..."
fi
