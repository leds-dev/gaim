
###
### for all database files *.lis
### generate *.tag file if not already these
###
### optional arg "echo" only lists *.lis files which
### do not have corresponding *.tag file
###
### invocation
###     genTAG.sh <echo>
###
###
### Last Modified
###     Sep 4, 2007
###

for scid in ${EPOCH_SATELLITES} 
do 
	cd $EPOCH_DATABASE/reports/$scid
	lis_files=`ls *.lis`
	echo " "
	pwd
	for file in $lis_files
	do
		name=`basename $file .lis`
		if [ -e $name.tag ]; then 
	    	echo "*** $name.tag exists"
		else 
			$1 ~/gaim/bin/genTAG $name
		fi
	done
		
done

#!/bin/sh
