#!/bin/sh
#  

# home directory for gaim
#GAIM_HOME=$HOME/gaim; export GAIM_HOME

# create flag to stop sleeping checkpoint copy jobs
cd ~/gaim/ftp
touch GAIM.STOP

#
# kill gaim processes
#
no_gaim="TRUE"
echo "***"
echo "*** Stopping GAIM processes"
echo "***"

for i in `ps -ef | grep N-Q_Interface_Manager | grep -v grep | awk '{print $2}'`
do
	kill -9 $i
	no_gaim=FALSE
	sleep 1
done

for i in `ps -ef | grep Check_GTACS | grep -v grep | awk '{print $2}'` 
do
        kill -9 $i
	no_gaim=FALSE
	sleep 1
done

for i in `ps -ef | grep Update_Buffer_Sender | grep -v grep | awk '{print $2}'`
do
        kill -9 $i
	sleep 1
done

for i in `ps -ef | grep Update_Buffer_Generator | grep -v grep | awk '{print $2}'`
do
        kill -9 $i
	no_gaim=FALSE
	sleep 1
done

for i in `ps -ef | grep Playback_Update_Buffer | grep -v grep | awk '{print $2}'`
do
        kill -9 $i
	no_gaim=FALSE
	sleep 1
done

for i in `ps -ef | grep Stream_Data_Receiver | grep -v grep | awk '{print $2}'`
do
        kill -9 $i
	no_gaim=FALSE
	sleep 1
done

# display sleeping jobs
# ps -eaf | grep sleep

if [ $no_gaim == "TRUE" ] 
then
   echo "             GAIM is not UP..."
   echo "===================================="
else
   echo "             GAIM is DOWN..."
   echo "===================================="

fi
