#!/bin/sh
#
# Top-level script for performing GAIM control functions
#
#
USAGE="usage: gaim check|start|stop|gentag"
GAIM_OWNER="gtacsops"

if [ $# != 1 ]
then
  echo "*** $USAGE"
  exit
fi
if [[ $1 != "check" && $1 != "start" && $1 != "stop" && $1 != "gentag" ]]
then
  echo "*** $USAGE"
  exit
fi

if [[ $1 == "check" ]]
then
  sudo -i -u $GAIM_OWNER check_gaim.sh
elif [[ $1 == "start" ]]
then
  sudo -i -u $GAIM_OWNER start_gaim.sh
elif [[ $1 == "stop" ]]
then
  sudo -i -u $GAIM_OWNER stop_gaim.sh
elif [[ $1 == "gentag" ]]
then
  sudo -i -u $GAIM_OWNER genTAG.sh
else
  echo "*** $USAGE"
  exit
fi
