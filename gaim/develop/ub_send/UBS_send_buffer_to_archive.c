/*
@(#)  File Name: UBS_send_buffer_to_archive.c  Release 1.10  Date: 06/01/12, 22:59:43
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_send_buffer_to_archive.c
         EXE. NAME       :       Update_Buffer_Sender
         PROGRAMMER      :       S. Scoles

         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles       Initial creation
         8.4       mar12-2004    K. Dolinh       - add log_event, log_debug
         8.6       apr01-2004
                   mar29-2005                    remove net_poll
                   apr01-2005                    make MAX_BYTES_PER_SEC an env var
                   apr06-2005                    comment out throttle on/off events  
                   apr07-2005                    add call to setsockopt,getsockopt 
                   apr21-2005                    do shutdown,close after write err
                   oct25-2005                    verify field msgsize before sending
                   nov02-2005                    send update buffer using msgsize
                   nov03-2005                    returns nbytes sent
                   dec14-2005                    check for null mh   
         10B       jan12-2006                    BUILD 10B
        14.1       Jun23-2010    D. Niklewski    reset "total_bytes_sent" so the count of bytes sent
                                                 per second is accurate (otherwise, it may not get reset
                                                 by UBS_main for several seconds).  also add code
                                                 to keep track of the last PCM frame's minor frame
                                                 number in order to identify missing frames.  and add
                                                 more debug capability when using the debug_tcpip option.
         14.2      Nov05-2010    D. Niklewski    Added "channel_last_pcm_time" to keep track of the time
                                                 of PCM frames for reporting PCM outages.
                                                  
********************************************************************************
    Invocation:

        error_number=UBG_send_buffer_to_archive(int buffer_size,char *buffer,
            int channel)

    Parameters:

        buffer_size - I
            The exact size of the update buffer.

        buffer - I
            A pointer to the memory area allocated for the update buffer to
            send.

        channel -I
            Which channel (1 to 32) is this packet coming from?

        error_number - Number bytes sent, 0 if error


********************************************************************************
    Functional Description:

        This routine sends the update buffer to to update buffer sender (the
        process which collects update bufferes from multiple update buffer
        generators and passes them on to the shared GOES archive.

*******************************************************************************/

/**

PDL:
    IF this buffer will overload the socket THEN
        Turn on the buffer throttle.
    ENDIF

    Call UBG_byte_swap for the update buffer counter.

    IF a request to create update buffer files has been made THEN
        Write the new buffer to the file.
    ENDIF

    IF the user has not stated that there is no GOES archive available THEN
        Check to see if a connection currently exists to the requested GOES
         archive.
        IF no connection is found THEN
            Open a socket to the requested archive machine.
        ENDIF
        IF the current archive has an active read/write socket connection THEN
            Double check to make sure the archive is still alive.
            IF the GOES archive is not responding to the poll THEN
                Mark that GOES archive as DEAD.
                Shutdown and close the socket to the dead archive.
                Shift the connections list to fill in the gap(s) caused by the
                 dead archive machine.
            ELSE
                Send the update buffer to the GOES archive machine.
            ENDIF
        ENDIF
    ENDIF

    RETURN an error status.

**/

#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include "net_util.h"
    /* Standard C Library */

#include "util_log.h"
#include "GAIM_strmerr.h"
    /* Stream Error Messages */

#include "UBS_globals.h"
    /* Globals and defines */

/******* NEW (11/05/10) *******/
double GetTime(const short year, const short day, const int milisec);
void GetTimeStr(const short year, const short day, const int milisec, char *strTime);
void GetTimeStr2(const double myTime, char *strTime);
void GetTimeStr3(const short year, const short day, const int milisec, char *strTime);
void GetTimeStr4(const double myTime, char *strTime);
double dfmod(double x, double y);
void UBG_byte_swap(char *string,int numbytes);
/******* NEWEND *******/

extern  int  debug_tcpip;
extern  int  max_bytes_per_sec;
extern  int  tcpip_so_sndbuf;
extern  int  update_buffer_max;

static  int  debug_tcpip_count = 0;
static  int  bad_msgsize_count = 0;

struct MsgHeader {
    int message_size;      /* Length of message */
    short class_id;         /* 102 */
    short message_number;   /* Incremented each new update buffer */
    short node_name_len;    /* Chars in node_name */
    char *node_name;        /* The name of the node which is sending the update 
                                buffer */
    short process_name_len; /* Chars in process_name */
    char *process_name;     /* Update_Buffer_Sender */
};
typedef struct MsgHeader MsgHeader;

/******* NEW (6/23/10) *******/
struct DataHeader {
    short data_size;
    char gtacs_name_code;
    char sc_id;
    char frame_type;
    char minor_frame_number;
    char db_name[32];
    short year;
    short day;
    int millisec;
    unsigned short status;
} dh;
int framenum;
char *ptr;
/******* NEWEND *******/

/******* NEW (11/05/10) *******/
char strTime[22];
char startTime[22];
char endTime[22];
/******* NEWEND *******/

MsgHeader * mh;
int      msg_size;
int       send_size;

int UBS_send_buffer_to_archive(int buffer_size,char *buffer,int channel) {

char message[256];
char filename[256];
char marker[17]="***NEW*BUFFER***";
char stars[17]="****************";
char terminator[16];
int i,num;
FILE *file_ptr;
int retval=0;
int tempint;
int err;
short short_data = 0;
int long_data = 0;

union sockopt {
    int  i_val;
    long l_val;
    char c_val[10];
    struct linger linger_val;
    struct timeval timeval_val;
    } sockopt;
int sockopt_size = sizeof(sockopt);


/* number of bytes sent by net_write */
int bytes_sent = 0;

/* Offset by 16 bytes -- the first 16 bytes tells us which archive
    machine to which to send the update buffer */
char *send_from_here;

/* Which archive is this update buffer going too? (0 to n-1) */
int which_archive=-1;

/* Which service name do we need to connect to */
char archive_service_name[32]="GaIngest13";

/* -1=No match ot the IP address, 0=match found */
int no_match=0;

/* Used for error responses. */
int status,no_goes_archive;

/* Used to help shift elements in the connection list */
int on_connection;

/* skip ip address in buffer and point to actual buffer data */
send_from_here=buffer+16;

/* If this is a new second, reset everything */
current_second=time(0);
if (current_second!=last_second) {
    last_second=current_second;
    per_second_buffer_count=0;

    /******* NEW (6/23/10) *******/
    /* Also reset total_bytes_sent, which is a count of the number of bytes
       sent in the CURRENT second.  If it's not reset here, it won't be reset
       until the end of the "while" loop in UBS_main, possibly after several
       seconds have elapsed. */

    total_bytes_sent=0;

    /******* NEWEND *******/
}

/* Copy the new buffer counter over the old one which was sent by UBG.
    Since there can be multiple UBGs, the only way to insure each buffer
    is numbered correctly is to have UBS number them, no matter where
    they came from. */
memcpy(&send_from_here[6],&update_buffer_counter,sizeof(update_buffer_counter));
short_data = update_buffer_counter;
update_buffer_counter++;

memcpy(&send_from_here[6], (char*)(&short_data), sizeof(update_buffer_counter));

if ((update_buffer_counter%100) == 0)
    log_debug("UBS has processed %i Update Buffers",update_buffer_counter);

if (getenv("CREATE_UPDATE_BUFFER_FILES")) {

    strcpy(filename,getenv("CREATE_UPDATE_BUFFER_FILES"));
    if ((filename[0]=='Y') && (channel > 16)) {
        /* WARNING: This option creates one file per frame. */
        strcpy(filename,"buffer.ubs");

        num=(buffer_size/16+1)*16-buffer_size;
        for (i=0; i<num; i++) terminator[i]='*';
        terminator[num]=0;

        file_ptr = fopen(filename,"ab");
        fwrite(stars,1,16,file_ptr);
        fwrite(marker,1,16,file_ptr);
        fwrite(stars,1,16,file_ptr);
        fwrite(buffer,1,buffer_size,file_ptr);
        fwrite(terminator,1,num,file_ptr);
        fclose(file_ptr);

    }
}

if (getenv("NO_GOES_ARCHIVE")) {
    strcpy(message,getenv("NO_GOES_ARCHIVE"));
    if (message[0]='Y') no_goes_archive = -1;
    else no_goes_archive = 0;
} else
    no_goes_archive = 0;


if (!no_goes_archive) {

    /* Deduct 16 bytes (ip addr) from the buffer size */
    buffer_size -= 16;

    /* init search to no-match */
    no_match = -1;

    /* If there are existing connections  */
    if (number_of_archive_connections >= 1) {

        /* search for ip address match */
        for (i=0; i<number_of_archive_connections; i++) {
            if (strcmp(buffer,archive_connections[i])==0) {
                /* found an IP address match */
                no_match = 0;
                which_archive = i;
            }
        }

    }

    /* If no archive connection matches, we need to make one */
    if (no_match) {

        if (number_of_archive_connections == MAX_ARCHIVE_MACHINES) {
            /* If our connections are maxed out, we need to close
                a connection and open a new one.  To make life simple,
                we will always close and open the MAX_ARCHIVE_MACHINES
                connection.  */
            /* Close down the socket */
            shutdown(archive_sockets[MAX_ARCHIVE_MACHINES-1],SHUT_RDWR);
            close(archive_sockets[MAX_ARCHIVE_MACHINES-1]);
            FD_CLR(archive_sockets[MAX_ARCHIVE_MACHINES-1],&read_mask);
            which_archive=MAX_ARCHIVE_MACHINES-1;
            number_of_archive_connections--;

            log_event_err("send_buffer: closing %s to make room for %s  (max = %d)",
                archive_connections[number_of_archive_connections],buffer,MAX_ARCHIVE_MACHINES);

        } else which_archive = number_of_archive_connections;

        strcpy(archive_service_name,archive_service_base);

        /* If this environment variable is defined, UBS should use
            this service name rather than the default to send the
            buffers.  This is used only for debugging against
            Barbara's NT machine with the GOES Archive Test
            Software. */
        if (getenv("ALTERNATE_SERVICE"))
            strcpy(archive_service_name,getenv("ALTERNATE_SERVICE"));

        /* make connection to archive */
        status = net_call(buffer,archive_service_name,&archive_sockets[which_archive]);

        if (status != 0) {
            /* Connect failed */
            log_event_err("send_buffer: connect to archive=%s.%s failed. err=%i (%s)",
                buffer,archive_service_name,status,stream_err_messages[0-status]);
            retval = -1;
            /* reset socket */
            archive_sockets[which_archive]=-1;

        } else {
            /* connect successful */
            log_event("send_buffer: connected to archive=%s.%s  archive_socket[%d]=%d",
                buffer,archive_service_name,which_archive,archive_sockets[which_archive]);
            /* set up socket for read-write service */
            FD_SET(archive_sockets[which_archive],&read_mask);
            /* save IP addr and incr connections count */
            strcpy(archive_connections[which_archive],buffer);
            number_of_archive_connections++;
            /* set socket parameters */
            if (tcpip_so_sndbuf > 0) {
                setsockopt(archive_sockets[which_archive],SOL_SOCKET,SO_SNDBUF,&tcpip_so_sndbuf,sizeof(tcpip_so_sndbuf));
                log_event("send_buffer: setting socket=%d SO_SNDBUF to %d ",archive_sockets[which_archive],tcpip_so_sndbuf);            
            }
            /* get socket parameters */
            if (getsockopt(archive_sockets[which_archive],SOL_SOCKET,SO_SNDBUF, (char *)&sockopt, &sockopt_size) == -1)
                log_event_err("send_buffer: getsockopt error for socket=%d",archive_sockets[which_archive]);
            else
                log_event("send_buffer: socket=%d  SO_SNDBUF=%d ",archive_sockets[which_archive],sockopt.i_val);
    
        }
    }

    if (archive_sockets[which_archive] != -1) {
    
        mh = (MsgHeader *) send_from_here;
        
        if (mh == NULL) {
            log_event("SEND_BUFFER_TO_ARCHIVE: mh is null");
            retval = 0;
            return retval;
        }
        
        msg_size = mh->message_size;
        long_data = msg_size;
        memcpy((char*)&msg_size, (char*)(&long_data), sizeof(msg_size));
        
        if (debug_tcpip && debug_tcpip_count++ < debug_tcpip) {
            log_event("UPDATE_BUFFER_TRAP0: (buffer_size=%d  msg_size=%ld) size=x%8.8X  class=x%4.4X  msgnum=x%4.4X",
                buffer_size,msg_size,mh->message_size,mh->class_id,mh->message_number); 
        }
            
        /* write buffer to socket 
        bytes_sent = net_write(archive_sockets[which_archive],send_from_here,buffer_size); */
        
        /* buffer send size is msgsize + 4 (size of msg field) */
        send_size = msg_size + 4;
        bytes_sent = net_write(archive_sockets[which_archive],send_from_here,send_size);
        
        /* output event if msg_size different from buffer_size */
        if (send_size != buffer_size) {
            bad_msgsize_count++;
            log_event("UPDATE_BUFFER_TRAP1: ip=(%15.15s)  (%8.8X %4.4X %4.4X)  msgsize=%d bufsize=%d errcount=%d send_size=%d sent=%d chan=%d",
                buffer,mh->message_size,mh->class_id,mh->message_number,msg_size,buffer_size,bad_msgsize_count,send_size,bytes_sent,channel);           
        }
                

        /* if (debug_tcpip) {
            get and clear socket error 
            err = getsockopt(archive_sockets[which_archive],SOL_SOCKET,SO_ERROR, (char *)&sockopt, &sockopt_size);
            log_event("send_buffer: socket=%d  (getsockopt err=%d  SO_ERROR=%d)  buffer_size=%d  bytes_sent=%d",
                archive_sockets[which_archive],err,sockopt.i_val,buffer_size,bytes_sent);
            
        }*/
        
        if (bytes_sent > 0) {
            /* write success */
            total_bytes_sent += bytes_sent;
            retval = bytes_sent;


            /*********************************NEW****************************************/
            /*     Write out information on every message sent to GAS if requested.     */
            /*                   D. Niklewski (NOAA) 6/23/2010                          */
            /*     Modified again by D. Niklewski (NOAA) 11/5/2010 for better           */
            /*                   reporting of missed PCM frames                         */
              
            int sizeof_mh = sizeof(mh->message_size);
            sizeof_mh += sizeof(mh->class_id);
            sizeof_mh += sizeof(mh->message_number);
            sizeof_mh += sizeof(mh->node_name_len);
            short nnl = mh->node_name_len;
            short_data = nnl;
            memcpy((char*)&nnl, (char*)(&short_data), sizeof(nnl));
            sizeof_mh += nnl;
            sizeof_mh += sizeof(mh->process_name_len);
            short pnl = mh->process_name_len;
            short_data = pnl;
            memcpy((char*)&pnl, (char*)(&short_data), sizeof(pnl));
            sizeof_mh += pnl;

            ptr = (char*)(send_from_here + sizeof_mh);

            /* Get the values in the data header {it's difficult to use a simple structure read
               such as "struct DataHeader *dh = (struct DataHeader *) ptr" because of structure
               alignment issues}.  Also swap the bytes for non-char elements. */

            memcpy(&dh.data_size, ptr, sizeof(dh.data_size));                 ptr+=sizeof(dh.data_size);
            memcpy(&dh.gtacs_name_code, ptr, sizeof(dh.gtacs_name_code));     ptr+=sizeof(dh.gtacs_name_code);
            memcpy(&dh.sc_id, ptr, sizeof(dh.sc_id));                         ptr+=sizeof(dh.sc_id);
            memcpy(&dh.frame_type, ptr, sizeof(dh.frame_type));               ptr+=sizeof(dh.frame_type);

            /* Only get the rest of the data header if it's a PCM frame, since currently only PCM
               is being checked for gaps.

               NOTE: channel = 1-16 for real-time streams, 17-32 for playback.  From GAIM_update_buffer_defines.h:
                   #define MAX_SDR_PROCESSES 16
                   #define MAX_PUB_PROCESSES 16
                   #define START_SDR_CHANNEL 1
                   #define START_PUB_CHANNEL 17
                   [Set up 32 sockets - 1 through 16 for SDR, 17 through 32 for PUB]

              Also, 1-8 are for PRIME REALTIME data, 9-16 are for NONPRIME REALTIME data, and 17-32 are for PLAYBACK data.
              (1,3,5,7 are for "PRIME-PRIME" data and 2,4,6,8 are for "SECONDARY_PRIME" data, whatever that is)

            */

            if ((int) dh.frame_type == 20) {  /* PCM Frame */

              memcpy(&dh.minor_frame_number, ptr, sizeof(dh.minor_frame_number));    ptr+=sizeof(dh.minor_frame_number);
              strncpy(dh.db_name, ptr, sizeof(dh.db_name));                          ptr+=sizeof(dh.db_name);
              memcpy(&dh.year, ptr, sizeof(dh.year));                                ptr+=sizeof(dh.year);
              memcpy(&dh.day, ptr, sizeof(dh.day));                                  ptr+=sizeof(dh.day);
              memcpy(&dh.millisec, ptr, sizeof(dh.millisec));                        ptr+=sizeof(dh.millisec);
              memcpy(&dh.status, ptr, sizeof(dh.status));                            ptr+=sizeof(dh.status);

              short_data = dh.data_size;
              memcpy((char*)&dh.data_size, (char*)(&short_data), sizeof(dh.data_size));
              short_data = dh.year;
              memcpy((char*)&dh.year, (char*)(&short_data), sizeof(dh.year));
              short_data = dh.day;
              memcpy((char*)&dh.day, (char*)(&short_data), sizeof(dh.day));
              long_data = dh.millisec;
              memcpy((char*)&dh.millisec, (char*)(&long_data), sizeof(dh.millisec));
              short_data = dh.status;
              memcpy((char*)&dh.status, (char*)(&short_data), sizeof(dh.status));

              framenum = (int)dh.minor_frame_number;
 
              /* Get Ground Receipt Time (GRT) from data header */
              double curr_time = GetTime(dh.year, dh.day, dh.millisec);

              if (channel_last_pcm_frame[channel-1] == -1) {
                GetTimeStr2(curr_time, strTime);
                if (channel >= START_PUB_CHANNEL)
                    log_event("**** GOES%2.2d: First PLAYBACK PCM Frame Received:  channel = %2d  MF = %2d  GRT = %s",(int)dh.sc_id,channel,framenum,strTime);
                else if (channel < 9) /* 1-8 = PRIME REALTIME data */
                    log_event("**** GOES%2.2d:    First PRIME PCM Frame Received:  channel = %2d  MF = %2d  GRT = %s",(int)dh.sc_id,channel,framenum,strTime);
                else                  /* 9-16 = NON-PRIME REALTIME data */
                    log_event("**** GOES%2.2d: First NONPRIME PCM Frame Received:  channel = %2d  MF = %2d  GRT = %s",(int)dh.sc_id,channel,framenum,strTime);
              } else {
                int delta = framenum - channel_last_pcm_frame[channel-1];
                if (delta <= 0) {
                    if (delta == -31)  /* The usual frame roll-over case. */
                        delta += 32;
                    else               /* One or more major frames was missed.  Set delta to a large number to indicate missing frames. */
                        delta = 666;
                }
                /* A PCM frame was missed if delta is greater than 1 OR if one or more entire major frame(s) was missed - in this
                   case, the current frame would have the proper frame number, but the time will show that an entire major frame
                   (or a multiple of major frames) was lost. */
                double diff = curr_time - channel_last_pcm_time[channel-1];
                if (delta > 1 || diff > missing_major_frame_time) {
                    GetTimeStr2(channel_last_pcm_time[channel-1], startTime);
                    GetTimeStr2(curr_time, endTime);
                    if (channel >= START_PUB_CHANNEL)
                        log_event("**** GOES%2.2d: Missing PLAYBACK PCM Frame(s):  channel = %2d  previous MF = %2d (%s)  current MF = %2d (%s)  [Delta-time = %.3f]",(int)dh.sc_id,channel,channel_last_pcm_frame[channel-1],startTime,framenum,endTime,diff);
                    else if (channel < 9) /* 1-8 = PRIME REALTIME data */
                        log_event("**** GOES%2.2d:    Missing PRIME PCM Frame(s):  channel = %2d  previous MF = %2d (%s)  current MF = %2d (%s)  [Delta-time = %.3f]",(int)dh.sc_id,channel,channel_last_pcm_frame[channel-1],startTime,framenum,endTime,diff);
                    else                  /* 9-16 = NON-PRIME REALTIME data */
                        log_event("**** GOES%2.2d: Missing NONPRIME PCM Frame(s):  channel = %2d  previous MF = %2d (%s)  current MF = %2d (%s)  [Delta-time = %.3f]",(int)dh.sc_id,channel,channel_last_pcm_frame[channel-1],startTime,framenum,endTime,diff);
                }
              }
              channel_last_pcm_frame[channel-1] = framenum;
              channel_last_pcm_time[channel-1] = curr_time;
            }

            if (debug_tcpip) {

              char scID[7]; 
              if ((int) dh.sc_id == 13)
                strcpy(scID, "GOES13");
              else if ((int) dh.sc_id == 14)
                strcpy(scID, "GOES14");
              else if ((int) dh.sc_id == 15)
                strcpy(scID, "GOES15");
              else if ((int) dh.sc_id == 16)
                strcpy(scID, "GOES16");
              else if ((int) dh.sc_id == 17)
                strcpy(scID, "GOES17");
              else if ((int) dh.sc_id == 18)
                strcpy(scID, "GOES18");
              else 
                strcpy(scID, "UNKNWN");

              char typ[4];
              if ((int) dh.frame_type == 20)
                strcpy(typ, "PCM"); 
              else if ((int) dh.frame_type == 21)
                strcpy(typ, "DWL"); 
              else if ((int) dh.frame_type == 22)
                strcpy(typ, "WDI"); 
              else if ((int) dh.frame_type == 23)
                strcpy(typ, "WDS"); 
              else if ((int) dh.frame_type == 24)
                strcpy(typ, "AGC"); 
              else if ((int) dh.frame_type == 25)
                strcpy(typ, "SXI"); 
              else if ((int) dh.frame_type == 26)
                strcpy(typ, "PSU"); 
              else 
                strcpy(typ, "UNK"); 

              int ic = channel - 1;
              char rt[3];
              if (ic < MAX_SDR_PROCESSES)
                  strcpy(rt, "RT");
              else
                  strcpy(rt, "PB");

              unsigned short msg_num = (unsigned short) mh->message_number;
              short_data = msg_num;
              memcpy((char*)&msg_num, (char*)(&short_data), sizeof(msg_num));

              log_event("%6.6s %3.3s %2.2s MSG SENT: bytes_sent=%5d send_size=%5d buffer_size=%5d channel=%3d message_number=%6d throttle_on=%2d minor_frame=%2d total_bytes_sent=%6d",scID,typ,rt,bytes_sent,send_size,buffer_size,channel,msg_num,throttle_on,(int)dh.minor_frame_number,total_bytes_sent);

            }
            /********************************ENDNEW**************************************/

        } else {
            /* write error */
            log_event_err("send_buffer: net_write err=%d to %s archive_socket[%d]=%d  buffer_size=%d  channel=%d",
                bytes_sent,
                archive_connections[which_archive],which_archive,archive_sockets[which_archive],
                buffer_size,channel);   
            log_event("strmerr[%d] = (%s)",bytes_sent,stream_err_messages[0-bytes_sent]);
            retval = -1;
            /* reset socket */
            shutdown(archive_sockets[which_archive],SHUT_RDWR);
            close(archive_sockets[which_archive]);

            FD_CLR(archive_sockets[which_archive],&read_mask);
            archive_sockets[which_archive] = -1;
            on_connection = 0;
            archive_connections[which_archive][0] = 0;

            /* Since the connection failed, we need to shift all
               the entries in the connections list. */
            for (i=0; i<number_of_archive_connections; i++) {

                if (archive_sockets[i] != -1) {
                    archive_sockets[on_connection]=archive_sockets[i];
                    strcpy(archive_connections[on_connection],archive_connections[i]);
                    on_connection++;
                }
            }
            /* decrement connection count */
            number_of_archive_connections--;
        }
    }
}

/* If the buffer count has exceeded our safety zone (MAX minus RT),
disable further merge buffers until the next second.  */
if (total_bytes_sent >= max_bytes_per_sec && !throttle_on) {
    throttle_on = -1;
    if (debug_tcpip)
        log_event("merge throttle turned ON. total_bytes_sent=%d  (max=%d)",total_bytes_sent,max_bytes_per_sec);
    
}

return retval;

}

/*********************************NEW****************************************/
/*                   D. Niklewski (NOAA) 11/15/2010                         */

const double SecondsPerDay = 86400.0;
const double MaxUnsignedLong = 4294967295.0;

/* Output: secondsSince1970 = time in seconds since 1970 (including milliseconds as a fractional part) */
double GetTime(const short year, const short day, const int milisec)
{
    double secondsSince1970;

    /* Calculate the number of days since 1970 */
    /* Note:This calculation is only good thru 2100 */
    int daysSince1970 = (1461*(year-1969))/4-365+day-1;

    /* Now add in miliseconds and convert to seconds */
    secondsSince1970 = SecondsPerDay*daysSince1970+milisec/1000.0;

    return secondsSince1970;
}

/* For GetTimeStr, format of output time string = YYYY-DDD-HH:MM:SS.mmm (21 characters) */
/* Output: strTime = time string (dimensioned in calling program as char strTime[22]) */
void GetTimeStr(const short input_year, const short input_day, const int input_milisec, char *strTime)
{
    int days;
    int year;
    int dayOfYear;
    int hour;
    int minute;
    int second;
    int milisec;

    /* Get seconds since 1970, including the fractional part */
    double myTime = GetTime(input_year, input_day, input_milisec);

    /* Force maximum number of seconds to not overflow a 32-bit integer */
    if (myTime > MaxUnsignedLong) {
        sprintf(strTime, "xxxx-xxx-xx:xx:xx.xxx");
        strTime[21] = '\0';
        return;
    }

    /* Get days since 1970 (including fractional part) */
    days = (int) (myTime/86400.0);

    year = ((4*(days+365)+3)/1461)+1969;
    dayOfYear = (days-(1461*(year-1970)+1)/4)+1;
    hour = (int) (dfmod(myTime,86400.0)/3600.0);
    minute = (int) (dfmod(myTime,3600.0)/60.0);
    second = (int) (dfmod(myTime,60.0));
    milisec = (int) (dfmod(myTime,1.0) * 1000.0);   /* Don't round off to nearest millisecond, or else */
                                                    /* 0.999999 will round off to 1000 msec, causing   */
                                                    /* an incorrect time string.                       */

    sprintf(strTime, "%04d-%03d-%02d:%02d:%02d.%03d",year,dayOfYear,hour,minute,second,milisec);
    strTime[21] = '\0';

    return;
}

/* For GetTimeStr2, format of output time string = YYYY-DDD-HH:MM:SS.mmm (21 characters) */
/*  Input:  myTime = (double) Seconds since 01/01/1970 00:00:00.000
/* Output: strTime = time string (dimensioned in calling program as char strTime[22]) */
void GetTimeStr2(const double myTime, char *strTime)
{
    int days;
    int year;
    int dayOfYear;
    int hour;
    int minute;
    int second;
    int milisec;

    /* Force maximum number of seconds to not overflow a 32-bit integer */
    if (myTime > MaxUnsignedLong) {
        sprintf(strTime, "xxxx-xxx-xx:xx:xx.xxx");
        strTime[21] = '\0';
        return;
    }

    /* Get days since 1970 (including fractional part) */
    days = (int) (myTime/86400.0);

    year = ((4*(days+365)+3)/1461)+1969;
    dayOfYear = (days-(1461*(year-1970)+1)/4)+1;
    hour = (int) (dfmod(myTime,86400.0)/3600.0);
    minute = (int) (dfmod(myTime,3600.0)/60.0);
    second = (int) (dfmod(myTime,60.0));
    milisec = (int) (dfmod(myTime,1.0) * 1000.0);   /* Don't round off to nearest millisecond, or else */
                                                    /* 0.999999 will round off to 1000 msec, causing   */
                                                    /* an incorrect time string.                       */

    sprintf(strTime, "%04d-%03d-%02d:%02d:%02d.%03d",year,dayOfYear,hour,minute,second,milisec);
    strTime[21] = '\0';

    return;
}

/* For GetTimeStr3, format of output time string = HH:MM:SS.mmm (12 characters) */
/* Output: strTime = time string (dimensioned in calling program as char strTime[13]) */
void GetTimeStr3(const short year, const short day, const int milisec, char *strTime)
{
    char strTimeFull[22];
    int i;
    GetTimeStr(year, day, milisec, strTimeFull);
    for (i = 0; i < 13; i++)
        strTime[i] = strTimeFull[i+9];   /* Replacement for strcpy */
    return;
}

/* For GetTimeStr4, format of output time string = HH:MM:SS.mmm (12 characters) */
/*  Input:  myTime = (double) Seconds since 01/01/1970 00:00:00.000
/* Output: strTime = time string (dimensioned in calling program as char strTime[13]) */
void GetTimeStr4(const double myTime, char *strTime)
{
    char strTimeFull[22];
    int i;
    GetTimeStr2(myTime, strTimeFull);
    for (i = 0; i < 13; i++)
        strTime[i] = strTimeFull[i+9];   /* Replacement for strcpy */
    return;
}

/* This is a replacement for fmod to be used with "GetTimeStr" and "GetTimeStr2".  It won't work properly
   for all input values.  The input values are limited such that (x/y) must be less than (2^32 - 1).  Since the first input
   represents seconds since 1970, this will allow the function to work properly for about 136 years past
   1970 - up to year 2106. */
double dfmod(double x, double y)
{
    double z;
    double sign;
    unsigned long x_over_y_long;
    double x_over_y_dbl;

    /* Return 0 if either x or y is zero. (Technically, NaN should be returned if y = 0). */
    if (x == 0.0 || y == 0.0)
        return 0.0;

    /* The return value should have the same sign as x.  The sign of y doesn't matter. */
    sign = 1.0;
    if (x < 0.0) {
        sign = -1.0;
        x = -x;
    }
    if (y < 0.0)
        y = -y;

    z = x / y;

    /* Return 0 if x/y is "too large". */
    if (z > MaxUnsignedLong)
        return 0.0;

    /* Get the integer part of x/y (x_over_y). */
    x_over_y_long = (unsigned long) z;
    x_over_y_dbl = (double) x_over_y_long;

    return (sign * (x - x_over_y_dbl * y));

}

/*******************************END NEW**************************************/

