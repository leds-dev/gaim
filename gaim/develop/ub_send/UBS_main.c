/*
@(#)  File Name: UBS_main.c  Release 1.25  Date: 07/06/22, 15:12:22
*/
/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBS_main.c
         EXE. NAME       :       Update_Buffer_Sender
         PROGRAMMER      :       S. Scoles

         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles       Initial creation
         V2.0        05/02       A. Raj          Add ioctl call for actual
                         bytes to read
     8.4       mar12-2004    K. Dolinh       - add log_event, log_debug
     8.5       mar22-2004
     8.6       apr02-2004                    - fixed log_event("buffers lost...")
     8.7       apr15-2004
     8.8       may06-2004
     8.B       sep17-2004                    - GTACS Build 8B
     9.1       jan06-2005                    - GTACS Build 9.1
     9A        feb23-2005                    BUILD 9A
     9AP       mar16-2005                    BUILD 9A Prime
               mar29-2005                      make ARCHIVE_PING_RATE an env var
               mar29-2005                      make MAX_BYTES_PER_SEC an env var              
     9AP.2     apr04-2005                    - BUILD 9A Prime.2
               apr06-2005                    comment out throttle on/off events
     9AP.3     apr07-2005                    - add getsockopt
               apr21-2005                    - add debug_tcp flag
     9B        may01-2005                    BUILD 9B
     9C        aug03-2005                    BUILD 9C
     10A       aug26-2005                    BUILD 10A
     10A.2     sep09-2005                    BUILD 10A.2
               oct24-2005                    - add env var GAIM_OUTPUT_BUFFER_MAX
     10A.3     nov15-2005                    - send buffer using msgsize field
               dec14-2005                    - debug 2nd buffer send
     10B       jan12-2006                    BUILD 10B
     10C       jul10-2006                    BUILD 10C
     11        Apr13-2007                    BUILD 11
     12        Jun22-2007                    BUILD 12
     14.1      Jun23-2010    D. Niklewski    major changes to enforce reading single
                                             complete messages instead of reading all bytes
                                             available and just assuming it's all one 
                                             and only one complete message (which was the
                                             cause of the "UPDATE_BUFFER_TRAP1" messages that
                                             caused PCM frames to be dropped when it read two
                                             messages at once but assumed it was all one message,
                                             and was throwing away the second message).
                                             also, allow for the reading of incomplete messages (i.e.,
                                             continue reading chunks [without blocking] until a single
                                             complete message has been built up) - this allows it to
                                             coexist with a slower network that can't immediately
                                             deliver all bytes in a message.
                     
********************************************************************************
    Invocation:

        Update_Buffer_Sender [-d [{Debug File Name}]]

    Parameters:

        -d [{Debug File Name}]
            Name of the debug file to be created to keep track of
            Update_Buffer_Sender Error Messages.  This is an optional
            parameter.  Default: No debug file.  If {Debug File Name} is not
            specified with the -d parameter, all debug messages will be written
            to STDOUT.

        Environment Variable: CREATE_UPDATE_BUFFER_FILES
            Set this environment variable to Y to create a file containing the
            update buffers that would normally be forwarded to the shared
            archive.  The file is created in ./buffers and is called
            buffers.UBS.  Each buffer is padded with asterisks (*) until a byte
            offet with multiple of 16 bytes is reached.  Each buffer is also
            prefixed with 48 bytes, that when hex-dumped to the screen or a
            file with 16 bytes per line followed by a text view, the 48 bytes
            would look like the following in the text view:
                ****************
                ***NEW*BUFFER***
                ****************

    Example Usage:


    Exit Conditions:

        0: Successful Completion
        1: Invalid Command Line Parameters
        2: Unknown Error
        4: Incomplete Argument List
        6: Unable to allocate space for Signal Action Structure
        7: Unable to set up SIGINT handler

******************************************************************************
    Functional Description:

        The DCR Generator Process generates DCR files.

*****************************************************************************/

/**

PDL:
    Process the command line parameters.
    Act upon the command line parameters.
    Setup the process interrupt handlers for SIGINT and SIGTERM.
    Initialize the TCP/IP interfaces.
    DOWHILE there is no request for program termination
        IF it is time to ping the archive machines THEN
            DOFOR each archive machine
                Poll the TCP/IP socket.
                IF there is no response THEN
                    Mark the archive as dead.
                ENDIF
            ENDDO
            IF any connection to any archive machine was lost THEN
                Shift the connections list to fill in the lost connections.
            ENDIF
        ENDIF

        IF the update buffer throttle is one THEN
            IF this is a new second THEN
                Call UBS_sendit for each real-time update buffer.
                IF there is still space in the bandwidth THEN
                    Call UBS_sendit for each merge buffers until we run out of
                     buffers or the bandwidth has been used up.
                ENDIF
                IF there is no more room in the badwidth THEN
                    Queue the waiting buffers until the next available slot.
                ENDIF
            ENDIF
        ENDIF

        IF any socket is requesting service THEN
            DOFOR each socket
                IF the listening socket is open THEN
                    IF this socket is requesting a connection THEN
                        Answer the request.
                    ENDIF
                ENDIF

                IF this socket is available for read/write requests THEN
                    IF this socket is reqisting read service THEN
                        Read the data from the socket.
                        IF the message is a shutdown message THEN
                            Shutdown and close both the listening socket and
                             the read/write socket.
                        ELSE
                            CALL GAIM_calculate_checksum
                            IF the read length is less than or equal to zero
                             THEN
                                Shutdown the read/write socket.
                            ELSE
                                IF the throttle is not turned on or the socket
                                 is a connection to a Stream_Data_Receiver
                                 process THEN
                                    CALL UBS_sendit
                                ELSE
                                    Place the current buffer in the holding
                                     queue until it can be sent.
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
            ENDDO
        ENDIF
    ENDDO

    Shutdown and close all open sockets.

**/

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>             /* Variable-length argument lists. */
#include <sys/ioctl.h>         /* defined FIONREAD */
#include <sys/socket.h>

extern int optopt;

/* Modified EPOCH file */
#define _STRM_ERR
#include "GAIM_strmerr.h"

/* Globals and defines */
#define UBS_MAIN
#include "UBS_globals.h"
#include "net_util.h"
#include "../include/util_log.h"

/* forward define */
int getopt(int argc, char* const argv[], const char* optstring);

void UBG_byte_swap(char *string,int numbytes);
void UBS_read_gaim_cfg(void);
void UBS_sendit(int read_length,char *buffer,int channel_number,int crc,int read_write_ubg[TOTAL_SOCKETS]);
int GAIM_calculate_checksum(int buffer_length,char *buffer);

int  max_bytes_per_sec = MAX_BYTES_PER_SECOND;
int  tcpip_so_sndbuf = 0;
int  debug_tcpip = 0;

#define SW_VERSION "Build 14.1 Jun-23-2010"
#define SW_ID      "UBS"


/* global variables */
int update_buffer_max = 0;

int main (int argc, char *argv[])

{

int c;
char *env_text;

/* CRC prefix */
long  llength=0;  /* for ioctl call */
short zero=0;

/* Counters */
int i,j;

/* Declare network variables */
struct sigaction *signal_action;
int status;

/* TCP/IP Socket information */
int listen_for_ubg[TOTAL_SOCKETS];
int read_write_ubg[TOTAL_SOCKETS];
char ubg_service_names[TOTAL_SOCKETS][20];
char base_ubg_service_name[16]="gaim_ubs_ubg_";
int  listen_err;
int nfds = 0;

/* Error Message String*/
char message[256];
char message1[256];

/* Time to sleep between checks of the socket */
struct timeval select_time={0,5000};

/* Update Buffer TCP/IP Information */
int read_length;
/* char buffer[65536]; */
/* int max_length=-65535; */
int crc=0;

/******* NEW (6/23/10) *******/
/* (also commented out "buffer" and "max_length" above) */
char *buffer;
int offset;
int bytes_to_read;
int complete_size;
int message_size;
int long_data = 0;
long bytes_left;
#define MAX_BUFF_SIZE 65536
char buff[TOTAL_SOCKETS][MAX_BUFF_SIZE];
char prev_offset[TOTAL_SOCKETS];
/******* NEWEND *******/

/* maximum number of buffers that can be sent during "catch up" phase */
int max_send;

/* Time of next archive machine PING */
int next_ping;
int archive_ping_rate = 60;  /* seconds ? */

/* Before checking for failures, no. of active archive machine connections */
int original_connections;

/* If connections were dropped, this helps keep track of connections
that need to be shifted in our connection list. */
int on_connection;

/* Needed for call to net_poll.  It would normally contain the number
of bytes available for reading, but since we are not really reading
the socket, the number should be 0.  If negative, socket error. */
int tempint;

/* no. of items sent to fill the bandwidth */
int held_items_sent=0;


/* init transmit statistics for all connections
(TOTAL_SOCKETS = MAX_SDR_PROCESSES + MAX_PUB_PROCESSES) */
for (i=0; i< TOTAL_SOCKETS; i++) {
    channel_buffer_count[i]=0;
    channel_buffer_failures[i]=0;
    channel_first_buffer_time[i][0]=0;
    channel_last_buffer_time[i][0]=0;
/******* NEW (6/23/10) *******/
    channel_last_pcm_frame[i] = -1;
/******* NEWEND *******/
}

/* Init the time variable for throttling. */
last_second=time(0);

debug_filename[0]=0;
/* Force the allocated space to become a 0 length string for each of
        the above CHAR variables */

/* get env variable */
gaim_home = (char *) getenv("GAIM_HOME");
if (!gaim_home) {
    /* exit if env var not defined */
    log_err("*** env variable GAIM_HOME is not defined");
    log_err("*** UBS terminating");
    exit(1);
}

/* parse arguments to process */
while ((c = getopt (argc, argv, "dD")) != -1) {
    switch (c)
      {
      case 'd':
        debug = 1;
        break;
      case 'D':
        debug = 1;
        break;
      default:
        log_err("*** Unknown exec option = %c", optopt);
      }
}

if (getenv("GAIM_DEBUG")) debug = 1;

if (getenv("GAIM_UPDATE_BUFFER_MAX"))
    update_buffer_max = atoi(getenv("GAIM_UPDATE_BUFFER_MAX"));
else
    update_buffer_max = 0;

/* init log message ID */
sprintf(logfile_msgid,"%s",SW_ID);
/* write generic startup messages */
log_event("  ");
log_event("Update Buffer Sender (%s) started ...",SW_VERSION);
log_event("logfile_name     = %s",logfile_name);
log_event("debug = %d",debug);
log_event("update_buffer_max = %d",update_buffer_max);

if ( !(env_text = (char *) getenv("CREATE_UPDATE_BUFFER_FILES")) ) env_text = "Not defined";
log_event("env CREATE_UPDATE_BUFFER_FILES = (%s)",env_text);

if ( !(env_text = (char *) getenv("NO_GOES_ARCHIVE")) ) env_text = "Not defined";
log_event("env NO_GOES_ARCHIVE   = (%s)",env_text);

if ( !(env_text = (char *) getenv("ALTERNATE_SERVICE")) ) env_text = "Not defined";
log_event("env ALTERNATE_SERVICE = (%s)",env_text);

if (getenv("ARCHIVE_PING_RATE")) archive_ping_rate = atoi(getenv("ARCHIVE_PING_RATE"));
log_event("archive_ping_rate = %d",archive_ping_rate);

if (getenv("MAX_BYTES_PER_SEC")) max_bytes_per_sec = atoi(getenv("MAX_BYTES_PER_SEC"));
log_event("max_bytes_per_sec = %d",max_bytes_per_sec);

if (getenv("TCPIP_SO_SNDBUF")) tcpip_so_sndbuf = atoi(getenv("TCPIP_SO_SNDBUF"));
log_event("tcpip_so_sndbuf = %d",tcpip_so_sndbuf);

if (getenv("GAIM_DEBUG_TCPIP"))
    debug_tcpip = atoi(getenv("GAIM_DEBUG_TCPIP"));
else
    debug_tcpip = 0;
log_event("debug_tcpip = %d",debug_tcpip);

log_event("  ");

/* Read gaim configuration file */
UBS_read_gaim_cfg();

/* ***************************************
* Initialize TCP/IP service variables *
*************************************** */
/* init sockets for (TOTAL_SOCKETS = MAX_SDR_PROCESSES + MAX_PUB_PROCESSES) */
for (i=0; i<TOTAL_SOCKETS; i++) {

    /* Append service number to end of service name */
    sprintf(ubg_service_names[i],"%s%i",base_ubg_service_name,i+1);
    /* init socket to not-open */
    listen_for_ubg[i]=-1;
    read_write_ubg[i]=-1;
}


/* ****************************************
* Set up process termination routines. *
**************************************** */

/* Allocate space for the signal_action structure */
signal_action=(struct sigaction *)calloc(1, sizeof(struct sigaction));

if (!signal_action) {
    log_event_err("cannot allocate space for signal action structure");
    exit(6);
}

/* Init the signal handler flag */
signal_action->sa_handler=signal_handler;

if (sigaction(SIGINT, signal_action, (struct sigaction *)NULL)) {
    log_event_err("cannot set up signal handler for SIGINT");
    exit(7);
}

if (sigaction(SIGTERM, signal_action, (struct sigaction *)NULL)) {
    log_event_err("cannot set up signal handler for SIGTERM");
    exit(7);
}

/* ****************************
* Set up TCP/IP interfaces *
**************************** */

/* wait one second */
sleep(1);

/* do for each service */
listen_err = 0;
for (i=0; i<TOTAL_SOCKETS; i++) {

    /* Initialize the sockets, but do not try to communicate, yet */
    status = net_answer(ubg_service_names[i], -99, &listen_for_ubg[i],
        &read_write_ubg[i]);

    if (status != 0) {
        log_event_err("cannot set up socket for service=%s.  status=%i",
            ubg_service_names[i],status);
        listen_err++;
    } else {
        if (listen_for_ubg[i] > nfds) nfds = listen_for_ubg[i];
        /* Set the mask for the socket */
        FD_SET(listen_for_ubg[i],&read_mask);
    }
}

log_event("initialized %d listening sockets. errs = %d",i, listen_err);

/* delete old buffer.ubs file before using */
if (getenv("CREATE_UPDATE_BUFFER_FILES")) {
    strcpy(message,getenv("CREATE_UPDATE_BUFFER_FILES"));
    if (message[0]=='Y' || message[0]=='y') {
        remove("buffer.ubs");
        log_event("env CREATE_UPDATE_BUFFER_FILES: buffers will be written to file buffer.ubs");
    }
}

if (getenv("NO_GOES_ARCHIVE")) {
    strcpy(message,getenv("NO_GOES_ARCHIVE"));
    if (message[0]=='Y' || message[0]=='y') {
        log_event("env NO_GOES_ARCHIVE: No buffer will be sent");
    }
}


/* When should we ping the archive machine(s) next? */
next_ping = time(0)+archive_ping_rate;

/********************
* Wait Forever Loop *
*********************/

/* Loop until program is killed by SIGINT/SIGTERM. */
while (!end_program) {

    /* If it is time to see if the archives are still alive... */
    if (time(0)>= next_ping) {

        /* save the current number of connections */
        original_connections = number_of_archive_connections;

        /* Check each archive connection */
        for (i=0; i<number_of_archive_connections; i++) {

            /* Is Archive still alive? */
            status = net_poll(archive_sockets[i],&tempint);
            if (status < 0) {
                /* connection failed, reset the socket. */
                log_event_err("net_poll failed for archive_socket[%i] err=%i (%s)",
                    i,status,stream_err_messages[0-status]);
                FD_CLR(archive_sockets[i],&read_mask);
                archive_sockets[i] = -1;
                number_of_archive_connections--;
            }

        }

        /* If a connection was lost, shift connections in connection list */
        if (original_connections != number_of_archive_connections) {
            on_connection=0;
            for (i=0; i<original_connections; i++) {
                /* Since at least one connection failed, we need to
                    shift all the entries in the connections list. */
                if (archive_sockets[i]!=-1) {
                    archive_sockets[on_connection]=archive_sockets[i];
                    strcpy(archive_connections[on_connection],
                        archive_connections[i]);
                    on_connection++;
                }
            }
        }
        /* set next archive ping time */
        next_ping = time(0)+archive_ping_rate;

    }

    current_second = time(0);
    if (throttle_on) {
        /* If this is a new second */
        if (current_second != last_second) {
        
            /* What is out merge bandwidth? */
            max_send = max_bytes_per_sec - total_bytes_sent;
            if (max_send > hold_total_length) {
                max_send = hold_total_length;
                throttle_on = 0;
                /*log_event("merge data throttle turned OFF.
                hold_length=%d",hold_total_length);*/
            }
            
            /* If we have less packets than merge bandwidth, no throttling needed */
            j=0;
            log_debug("MAXSEND = %i, HOLD_COUNT = %i",max_send,hold_count);

            /* Send each held buffer */
            while (max_send>=hold_length[j] && hold_count>0) {
                UBS_sendit(hold_length[j],hold_buffers[j],hold_channel[j],hold_crc[j],read_write_ubg);
                free(hold_buffers[j]);
                hold_count--;
                hold_total_length-=hold_length[j];
                max_send-=hold_length[j];
                j++;
            }
            /* How many buffers were sent to fill the bandwidth? */
            held_items_sent = j;

            /* If there is no more room in bandwidth */
            if (hold_count>0) {
                /* Move elements down the hold queue. */
                for (j=0; j<hold_count; j++) {
                    hold_buffers[j]=hold_buffers[j+held_items_sent];
                    hold_length[j]=hold_length[j+held_items_sent];
                    hold_crc[j]=hold_crc[j+held_items_sent];
                    hold_channel[j]=hold_channel[j+held_items_sent];
                }

            }
        }
    }

    /* Duplicate the read_mask */
    data_mask = read_mask;

    /* Any sockets waiting with service requests? */
    status = select(/*FD_SETSIZE*/ nfds+1, &data_mask, NULL, NULL, &select_time);

    /* If socket service request is waiting... */
    if (status > 0) {

        /* Which socket has the request? */
        for (i=0; i<TOTAL_SOCKETS; i++) {

            /* Is the listening socket open? */
            if (listen_for_ubg[i] > 0) {

                /* Is socket waiting for service? */
                if (FD_ISSET(listen_for_ubg[i],&data_mask)) {

                    /* Answer request with final connect message */
                    status=net_answer(ubg_service_names[i], 99,
                        &listen_for_ubg[i], &read_write_ubg[i]);
                    if (status!=0) {
                        /* If the connection failed */
                        log_event_err("net_answer failed for UBG-%i service=%s err=%i (%s)",
                            i+1,ubg_service_names[i],status,stream_err_messages[0-status]);
                    } else {
                        if (read_write_ubg[i] > nfds) nfds = read_write_ubg[i];
                        /* set up read-write socket */
                        FD_SET(read_write_ubg[i],&read_mask);
                        log_event("connected to UBG-%i service=%s",i+1,ubg_service_names[i]);
                        /* If real-time connection, increment RT count */
                        if (i < MAX_SDR_PROCESSES) num_rt_connections++;
                    }
                } /* endif fd_isset */
            } /* endif listen_for_ubg */

            /* Is socket available for read-write requests? */
            if (read_write_ubg[i] > 0) {

                /* Is socket the one with data? */
                if (FD_ISSET(read_write_ubg[i],&data_mask)) {

                      /* Input is pending.  Find out how many bytes of data
                      are actually available for input.  If SELECT(2) indicates
                      pending input, but IOCTL(2) indicates zero bytes of
                      pending input, the connection is broken.  - ARaj */

                      /*******OLD*******/

                      /*
                      if (ioctl(read_write_ubg[i],FIONREAD,&llength) == -1){
                         log_event_err("ioctl failed for UBG-%i service=%s",
                            i+1,ubg_service_names[i]);
                         log_event("close connection to UBG-%i",i+1);
                         shutdown(read_write_ubg[i],SHUT_RDWR);
                         close(read_write_ubg[i]);
                         FD_CLR(read_write_ubg[i],&read_mask);
                         read_write_ubg[i] = -1;
                      } else {
                         if ((int) llength <= 0) {
                            log_event_err("no data to read on read_write_ubg service[%i]=%s",
                                i,ubg_service_names[i]);
                            log_event("close connection to UBG-%i",i+1);
                            shutdown(read_write_ubg[i],SHUT_RDWR);
                            close(read_write_ubg[i]);
                            FD_CLR(read_write_ubg[i],&read_mask);
                            read_write_ubg[i] = -1;

                         } else {
                            read_length = net_read(read_write_ubg[i],buffer,max_length);
                            if (buffer[0]=='z' && read_length==8) {
                                log_event("received shutdown msg from UBG-%i",i+1);
                                log_event("close connection to UBG-%i",i+1);
                                shutdown(read_write_ubg[i],SHUT_RDWR);
                                close(read_write_ubg[i]);
                                FD_CLR(read_write_ubg[i],&read_mask);
                                read_write_ubg[i]=-1;
                                if (i<MAX_SDR_PROCESSES) num_rt_connections--;

                            } else {
                                log_debug("received buffer from UBG-%i len=%i",i+1,read_length);
                                crc = GAIM_calculate_checksum(read_length,buffer);
                                if (read_length <= 0) {
                                  log_event("close connection to UBG-%i",i+1);
                                  FD_CLR(read_write_ubg[i],&read_mask);
                                  read_write_ubg[i]=-1;
                                  if (i<MAX_SDR_PROCESSES) num_rt_connections--;

                                } else {
                                  if (!throttle_on || i<MAX_SDR_PROCESSES) {

                                      UBS_sendit(read_length,buffer,i+1,crc,read_write_ubg);
                                  } else {
                                      hold_buffers[hold_count]=(char *)calloc(
                                          read_length+1,sizeof(char));
                                      memcpy(hold_buffers[hold_count],buffer,
                                          read_length);
                                      hold_crc[hold_count]=crc;
                                      hold_channel[hold_count]=i+1;
                                      hold_length[hold_count]=read_length;
                                      hold_count++;
                                      hold_total_length+=read_length;

                                    }
                                }
                            }

                        }
                    }
                {}
                */

                      /*******ENDOLD*******/

                      /******* NEW (6/23/10) *******/

                      if (ioctl(read_write_ubg[i],FIONREAD,&llength) == -1){
                          /* ioctl failed -- */
                          log_event_err("ioctl failed for UBG-%i service=%s",i+1,ubg_service_names[i]);
                          /* close connection */
                          log_event("Error #1, close connection to UBG-%i",i+1);
                          shutdown(read_write_ubg[i],SHUT_RDWR);
                          close(read_write_ubg[i]);
                          FD_CLR(read_write_ubg[i],&read_mask);
                          read_write_ubg[i] = -1;
                          prev_offset[i] = 0;
                          channel_last_pcm_frame[i] = -1;
                      } else if ((int) llength <= 0) {
                          /* Determine if socket was closed on the other end (llength = 0), or there
                             was a socket error (llength < 0). */
                          log_event_err("no data to read on read_write_ubg service[%i]=%s",
                             i,ubg_service_names[i]);
                          /* close ubg connection */
                          log_event("Error #2, close connection to UBG-%i",i+1);
                          shutdown(read_write_ubg[i],SHUT_RDWR);
                          close(read_write_ubg[i]);
                          FD_CLR(read_write_ubg[i],&read_mask);
                          read_write_ubg[i] = -1;
                          prev_offset[i] = 0;
                          channel_last_pcm_frame[i] = -1;
                      } else {
                          /* ioctl was successful -- */
                          /* Read data on socket */

                          bytes_left = llength;
                          buffer = buff[i];    /*  buffer = pointer to actual buffer being used */
													/* clear the buffer before reading in */
													memset(buff[i],0, MAX_BUFF_SIZE);
                          offset = 0;
                          if (prev_offset[i] > 0)
                              offset = prev_offset[i];

                          /* If this is a new message OR less than 8 bytes were read on the last try, read
                             the first 8 bytes to determine if it's a shutdown message.  */

                          if (offset < 8) {

                              bytes_to_read = 8 - offset;
                              if (bytes_to_read > bytes_left)
                                  bytes_to_read = bytes_left;
                              read_length = net_read(read_write_ubg[i],buffer+offset,bytes_to_read);
                              offset += read_length;
                              bytes_left -= read_length;
                              if (read_length != bytes_to_read) {
                                  /* problem reading input */
                                  log_event_err("problem reading data on read_write_ubg service[%i]=%s",i,ubg_service_names[i]);
                                  /* close ubg connection */
                                  log_event("Error #3, close connection to UBG-%i",i+1);
                                  shutdown(read_write_ubg[i],SHUT_RDWR);
                                  close(read_write_ubg[i]);
                                  FD_CLR(read_write_ubg[i],&read_mask);
                                  read_write_ubg[i] = -1;
                                  prev_offset[i] = 0;
                                  channel_last_pcm_frame[i] = -1;
                                  break;
                              }

                              /* received shutdown message from UBG? */
                              if (buffer[0]=='z') {
                                  log_event("received shutdown msg from UBG-%i",i+1);
                                  /* close ubg connection */
                                  log_event("Shutdown message received from UBG, close connection to UBG-%i",i+1);
                                  shutdown(read_write_ubg[i],SHUT_RDWR);
                                  close(read_write_ubg[i]);
                                  FD_CLR(read_write_ubg[i],&read_mask);
                                  read_write_ubg[i]=-1;
                                  prev_offset[i] = 0;
                                  channel_last_pcm_frame[i] = -1;
                                  /* If a real-time connection, decrement RT count */
                                  if (i<MAX_SDR_PROCESSES) num_rt_connections--;
                                  break;
                              }
                          }

                          if (bytes_left <= 0) {
                              prev_offset[i] = offset;
                              break;
                          }

                          /* Now read either the rest of a complete message OR whatever
                             bytes are available. */

                          /* read another 20 bytes to get message length (note: the value of
                             the message length EXCLUDES the 4 bytes that store the message 
                             length variable - it's the number of bytes that follow to complete
                             the message). */

                          /* A complete message consists of a 16-byte string (IP address) followed
                             by the GAIM header [the first 4 bytes of which is the number of bytes 
                             that follow to complete the message], followed by the actual update buffer. */

                          if (offset < 20) {
                              bytes_to_read = 20 - offset;
                              if (bytes_to_read > bytes_left)
                                  bytes_to_read = bytes_left;
                              read_length = net_read(read_write_ubg[i],buffer+offset,bytes_to_read);
                              offset += read_length;
                              bytes_left -= read_length;
                              if (read_length != bytes_to_read) {
                                  /* problem reading input */
                                  log_event_err("problem reading data on read_write_ubg service[%i]=%s",i,ubg_service_names[i]);
                                  /* close ubg connection */
                                  log_event("Error #4, close connection to UBG-%i",i+1);
                                  shutdown(read_write_ubg[i],SHUT_RDWR);
                                  close(read_write_ubg[i]);
                                  FD_CLR(read_write_ubg[i],&read_mask);
                                  read_write_ubg[i] = -1;
                                  prev_offset[i] = 0;
                                  channel_last_pcm_frame[i] = -1;
                                  break;
                              }
                          }

                          if (bytes_left <= 0) {
                              prev_offset[i] = offset;
                              break;
                          }

                          /* At this point, offset should be >= 20. */

                          memcpy(&message_size, &buffer[16], 4);
                          long_data = message_size;
                          memcpy((char*)&message_size,(char*)(&long_data), sizeof(message_size));

                          bytes_to_read = message_size - (offset - 20);
                          if (bytes_to_read > bytes_left)
                              bytes_to_read = bytes_left;
                          if ((offset+bytes_to_read) > MAX_BUFF_SIZE) {
                              /* buffer too small */
                              log_event_err("problem reading data on read_write_ubg service[%i]=%s",i,ubg_service_names[i]);
                              /* close ubg connection */
                              log_event("Error #5, close connection to UBG-%i",i+1);
                              shutdown(read_write_ubg[i],SHUT_RDWR);
                              close(read_write_ubg[i]);
                              FD_CLR(read_write_ubg[i],&read_mask);
                              read_write_ubg[i] = -1;
                              prev_offset[i] = 0;
                              channel_last_pcm_frame[i] = -1;
                              break;
                          }
                          read_length = net_read(read_write_ubg[i],buffer+offset,bytes_to_read);
                          offset += read_length;
                          bytes_left -= read_length;
                          if (read_length != bytes_to_read) {
                              /* problem reading input */
                              log_event_err("problem reading data on read_write_ubg service[%i]=%s",i,ubg_service_names[i]);
                              /* close ubg connection */
                              log_event("Error #6, close connection to UBG-%i",i+1);
                              shutdown(read_write_ubg[i],SHUT_RDWR);
                              close(read_write_ubg[i]);
                              FD_CLR(read_write_ubg[i],&read_mask);
                              read_write_ubg[i] = -1;
                              prev_offset[i] = 0;
                              channel_last_pcm_frame[i] = -1;
                              break;
                          }

                          /* Is this a complete Update Buffer message? */

                          complete_size = 16 + sizeof(message_size) + message_size;  /* IP address + 
                                                                                        message_size variable size +
                                                                                        message */
                          if (offset == complete_size) {
                              /* complete message present */
                          } else if (offset < complete_size) {
                             /* Break out and save the offset for reading more bytes later */
                              prev_offset[i] = offset;
                              break;
                          } else {
                              /* problem reading input */
                              log_event_err("problem reading data on read_write_ubg service[%i]=%s",i,ubg_service_names[i]);
                              /* close ubg connection */
                              log_event("Error #7, close connection to UBG-%i",i+1);
                              shutdown(read_write_ubg[i],SHUT_RDWR);
                              close(read_write_ubg[i]);
                              FD_CLR(read_write_ubg[i],&read_mask);
                              read_write_ubg[i] = -1;
                              prev_offset[i] = 0;
                              channel_last_pcm_frame[i] = -1;
                              break;
                          }

                          /* Now there's a complete Update Buffer message that can be sent to GAS */

                          read_length = complete_size;

                          log_debug("received buffer from UBG-%i len=%i",i+1,read_length);
                          crc = GAIM_calculate_checksum(read_length,buffer);
                          if (read_length <= 0) {
                              /* close ubg connection */
                              log_event("Error #8, close connection to UBG-%i",i+1);
                              FD_CLR(read_write_ubg[i],&read_mask);
                              read_write_ubg[i]=-1;
                              prev_offset[i] = 0;
                              channel_last_pcm_frame[i] = -1;
                              /* If a real-time connection, decrement RT count */
                              if (i<MAX_SDR_PROCESSES)
                                  num_rt_connections--;
                          } else {
                              if (!throttle_on || i < MAX_SDR_PROCESSES) {
                                    /* If throttling is off, or this is
                                        real-time data, send the packet. */
                                  UBS_sendit(read_length,buffer,i+1,crc,read_write_ubg);
                              } else {
                                  /* Since we need to throttle, we need to
                                     save the index number and the CRC value
                                     to be returned once throttling is off.
                                     We also need to store the buffer to be sent */
                                  /* Alloc memory for temporary storage */
                                  hold_buffers[hold_count]=(char *)calloc(read_length+1,sizeof(char));
                                  /* Copy Update Buffer */
                                  memcpy(hold_buffers[hold_count],buffer,read_length);
                                  /* Record other hold parameters */
                                  hold_crc[hold_count]=crc;
                                  hold_channel[hold_count]=i+1;
                                  hold_length[hold_count]=read_length;
                                  hold_count++;
                                  hold_total_length+=read_length;

                              } /* endif throttle */
                          } /* endif read_length check */
                      } /* endif ioctl */
                } /* endif FD_ISSET read-write-ubg  */

                /******* NEWEND *******/

            } /* endif read_write_ubg[i] */
        } /* enddo TOTAL_SOCKETS */
    } /* endif select status */

    if (current_second!=last_second)
        total_bytes_sent=0;

} /* enddo while end_program */


/********************************
* Shutdown Update_Buffer_Sender *
*********************************/


/* This process should not die unless the user cancels it, or some
    other catastrophic failure happened.  The sleep(1) is necessary so
    that UBG can differentiate any CRCs from the z!z!z!z! message. */

log_event_err("exiting main processing loop");
sleep(1);


/* close all UBG connections */
for (i=0; i<TOTAL_SOCKETS; i++) {

    /* If socket is open for read-write */
    if (read_write_ubg[i]>0) {
        /* Send a shutdown message ("z!z!z!z!") */
        log_event("send shutdown message to UBG-%i",i+1);
        net_write(read_write_ubg[i],error_message,strlen(error_message)+1);
        /* Shut down this end of the socket. */
        shutdown(read_write_ubg[i],SHUT_RDWR);
        close(read_write_ubg[i]);
        FD_CLR(read_write_ubg[i],&read_mask);
        read_write_ubg[i]=-1;
    }

}

/* Close all GAS connections */
for (i=0; i<MAX_ARCHIVE_MACHINES; i++) {
    if (archive_sockets[i] > 0) {
        log_event("close archive connection i=%d",i);
        shutdown(archive_sockets[i],SHUT_RDWR);
        close(archive_sockets[i]);
        FD_CLR(archive_sockets[i],&read_mask);
    }
}


log_event("Update_Buffer_Sender Statistics for this session");
for (i=0; i<TOTAL_SOCKETS; i++) {

    log_event("Channel %i : Total Buffer Count = %i",i+1,channel_buffer_count[i]);
    if (channel_buffer_count[i] > 0) {
        log_event("             First Buffer Time = %s",channel_first_buffer_time[i]);
        log_event("             Last  Buffer Time = %s",channel_last_buffer_time[i]);
        log_event("             Buffers Lost      = %d",channel_buffer_failures[i]);
    }

}

log_event("Update_Buffer_Sender normal exit");
exit(0);

} /* end main */


/*************************
* Signal Handler routine *
**************************/

static void signal_handler(int signal) {

if (signal == SIGINT) {
    log_event("Update_Buffer_Sender terminated by ^C");
    end_program = -1;
} else if (signal == SIGTERM) {
    log_event("Update_Buffer_Sender terminated by SIGTERM");
    end_program = -1;
}


} /* end signal_handler */


#include "../include/util_log.c"


