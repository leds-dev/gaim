/*
@(#)  File Name: UBS_sendit.c  Release 1.8  Date: 06/01/12, 22:59:44
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_sendit.c
         EXE. NAME       :       Update_Buffer_Sender
         PROGRAMMER      :       S. Scoles

         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles       Initial creation
         V2.0        05/02       A. Raj          Add net_write status check
		 8.4	   mar12-2004	 K. Dolinh       - add log_event, log_debug
				   nov15-2005					 handle 2-buffer condition 	
				   dec14-2005	 				 debug 2nd buffer send
	 	 10B       jan12-2006               	 BUILD 10B
		 		 
********************************************************************************
	Invocation:

		UBG_sendit(int buffer_size,char *buffer,int channel,int crc,
			int read_write_ubg[TOTAL_SOCKETS])

	Parameters:

		buffer_size - I
			The exact size of the update buffer.

		buffer - I
			A pointer to the memory area allocated for the update buffer to
			send.

		channel -I
			Which channel (1 to 32) is this packet coming from?

		crc - I
			What is the CRC of this packet?

		read_write_ubg - I
			TCP/IP socket list between the UBS and the UBGs

********************************************************************************
	Functional Description:

		This routine sends interfaces between the MAIN routine and the
		SEND_BUFFER routine.  It is needed to support "catchup" (buffered) mode.

*******************************************************************************/

/**

	CALL UBS_send_buffer_to_archive

	IF there is a problem with the buffer sent THEN
		Write a debug message to the debug log.
	ENDIF

	Record the time stamp of this buffer for future reference.
	Send the CRC of this buffer back to the Update_Buffer_Generator process
	 the buffer came from.

**/

#include <stdio.h>
	/* Standard C Library */

#include "UBS_globals.h"
#include "net_util.h"
#include "util_log.h"

	/* Globals and defines */

#define IP_ADDR_LEN  16

extern  int  debug_tcpip;
extern  int  update_buffer_max;

void UBG_byte_swap(char *string,int numbytes);
int UBS_send_buffer_to_archive(int buffer_size,char *buffer,int channel);

void UBS_sendit
	(int read_length,char *buffer,int channel_number,int crc,
	 int read_write_ubg[TOTAL_SOCKETS])
{

/* Error status */
int status;

/* Error message */
char message[256];

char tempbuff[12];
int  bytes_sent;
int  buffer2_len;
char *buffer2;


#define ARRAYSIZE 100
int	  i,n;
char array[ARRAYSIZE];
char text [ARRAYSIZE*3];

/* Send update buffer block to GAS */
bytes_sent = UBS_send_buffer_to_archive(read_length,buffer,channel_number);

if (bytes_sent <= 0) {
	/* send buffer failed 
	log_event("transmit error on channel=%i",channel_number); */
	/* Increment the dropped buffer count */
	if ((short)(*(char *)(buffer+58))>=20 && (short)(*(char *)(buffer+58))<=26)
		channel_buffer_failures[channel_number-1]++;

} else {
	/* successful send, check for another block in read buffer */
	buffer2_len = read_length - (IP_ADDR_LEN + bytes_sent);
	if (buffer2_len > IP_ADDR_LEN) {
		/* calculate pointer to 2nd update buffer */
		buffer2 = buffer + IP_ADDR_LEN + bytes_sent;
		
		/* log_event("UPDATE_BUFFER_DUMP1: buf2_len=%d  read_len=%d  sent=%d",
			buffer2_len,read_length,bytes_sent); */
			
		/* write partial hex dump of 2nd buffer to text */
		memcpy(&array,buffer2,ARRAYSIZE);
		n = 0;
		for (i=0; i<ARRAYSIZE; i++) {
   			sprintf(&text[n],"%2.2X.",array[i]); 
   			n = n+3;  
		}

		/* check for ip_addr at start of buffer */
		/* send 2nd update buffer block to GAS */
		if (update_buffer_max > 0) {
			if ( !strncmp(buffer2,"192.168.",8) ) {
				bytes_sent = UBS_send_buffer_to_archive(read_length,buffer2,channel_number);
				log_event("BUFFER2 IN DOUBLE UPDATE_BUFFER SENT (%d)",bytes_sent);
			} else {
				log_event("BUFFER2 IN DOUBLE UPDATE_BUFFER DUMP (%300s)",text);	
			}
		}
					
	}
}


/* Return received CRC as validation. */
if ((short)(*(char *)(buffer+58))>=20 && (short)(*(char *)(buffer+58))<=26) {

	/* Swap bytes back so they can be used by UBS debug messages */
	memcpy(tempbuff,buffer+92,8);

	/* save buffer transmit time */
	sprintf(channel_last_buffer_time[channel_number-1],"%4.4i-%3.3i-%8.8i",
		*(short *)(tempbuff),*(short *)(tempbuff+2),*(int *)(tempbuff+4));

	/* also save transnit time if first buffer */
	if (channel_buffer_count[channel_number-1] == 0)
		sprintf(channel_first_buffer_time[channel_number-1],"%4.4i-%3.3i-%8.8i",
		*(short *)(tempbuff),*(short *)(tempbuff+2),*(int *)(tempbuff+4));

	/* Increment the total buffers sent count */
	channel_buffer_count[channel_number-1]++;
}

/* Return a validated CRC, no matter what */
status = net_write(read_write_ubg[channel_number-1],(const char*)&crc,4);
if (status != 4)
	log_event_err("error writing validated CRC to UBG-%i", channel_number);
else
	log_debug("returned CRC to UBG-%i [%8.8X]", channel_number,crc);

} /* end UBS_sendit */
