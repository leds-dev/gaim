/*  
@(#)  File Name: UBS_globals.h  Release 1.1  Date: 00/11/10, 13:21:47
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         HEADER          :       UBS_globals.h
         EXE. NAME       :       Update_Buffer_Sender
         PROGRAMMER      :       S. Scoles
    
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
         14.1        Jun23-2010  D. Niklewski    Increased MAX_ARCHIVE_MACHINES from 4 to 6 to allow
                                                 for 3 s/c (13,14,15) plus 3 simulated s/c (16,17,18).
                                                 Also added "channel_last_pcm_frame" to keep track
                                                 of MINOR_FRAME_COUNT for PCM frames in order to report
                                                 any missing frames.
         14.2        Nov05-2010  D. Niklewski    Added "channel_last_pcm_time" to keep track of the time
                                                 of PCM frames for reporting PCM outages.
                

********************************************************************************
         Functional Description:

         This header file contains globals used by the routines in 
         the Update_Bufffer_Sender process.
    
*******************************************************************************/


#include "strmutil.h"
    /* EPOCH includes */

#include "GAIM_update_buffer_defines.h"
    /* Defines for processing update buffers */

#define TOTAL_SOCKETS MAX_SDR_PROCESSES+MAX_PUB_PROCESSES

static void signal_handler(int sig);
    /* More Prototypes */

/*
#define MAX_ARCHIVE_MACHINES 4
*/
/******* NEW (6/23/10) D. Niklewski (NOAA) *******/
#define MAX_ARCHIVE_MACHINES 6
/******************** NEWEND *********************/

    /* Maximum number of connections to the archive machines */

#define MAX_GAIMCFG_ARCH_MACHINES 16
    /* Maximum number of archive machines in the gaim.cfg file */

#define MAX_BYTES_PER_SECOND 110000
    /* Maximum number of Bytes of Update Buffers the GOES Archive Machine can 
        process each second. (What is the max we can send them each second 
        without overloading the scoket?) */

#define ARCHIVE_PING_RATE 10
    /* How often should I ping the archive machines to make sure they are 
        still alive? */


#ifdef UBS_MAIN
    int end_program=0;
        /* -1=End, 0=Keep Going */
    
    int debug_on=0;
    char debug_filename[256];
        /* Debug File Name */   
    
    fd_set read_mask,data_mask;
    svr_stream ubg_server_stream[TOTAL_SOCKETS];
        /* Needed for TCP/IP */
    
    unsigned short update_buffer_counter=0;
        /* Counter (message number) for the update buffers */

    char archive_connections[MAX_ARCHIVE_MACHINES][20]={" "," "};
        /* To which archives do we have open sockets? */

    int archive_sockets[MAX_ARCHIVE_MACHINES]={-1,-1};
        /* Our write sockets to the GOES archive machines */

    int number_of_archive_connections=0;
        /* How many archive connections do we have currently? */

    char archive_service_base[20]="GaIngest";
        /* Base name for the archive machine service socket */

    char ipaddr_sc_map[MAX_GAIMCFG_ARCH_MACHINES][2][20];
    int number_of_machines;
        /* [][0][*]=IP Address, [][1][*]=SCID (2 char/digits) */

    char error_message[1024]=ERROR_MESSAGE_HEADER;
        /* Error message to be sent back through the system */

    int per_second_buffer_count;
        /* How many buffers this second? */

    int last_second;
        /* Last time period */

    int current_second;
        /* This time period */

    int throttle_on=0;
        /* Do we need to throttle (slow down)? */

    int num_rt_connections=0;
        /* How many real-time connections do we currently have? */

    char *hold_buffers[MAX_PUB_PROCESSES];
        /* For holding the unsent buffers -- PUB only because RT buffers will 
            never be buffered or throttled. */

    int hold_crc[MAX_PUB_PROCESSES];
        /* Holding spot for the CRC */

    int hold_channel[MAX_PUB_PROCESSES];
        /* List of indices held */

    int hold_length[MAX_PUB_PROCESSES];
        /* How long are the buffers we are holding? */

    int hold_count=0;
        /* Count of number of holds */

    int channel_buffer_count[TOTAL_SOCKETS];     
        /* Total Buffers Attempted to Send (includes Failues) */

    int channel_buffer_failures[TOTAL_SOCKETS]; 
        /* Buffers that were dropped because of the GOES Archive not 
            responding when net_ping'ed. */

    char channel_first_buffer_time[TOTAL_SOCKETS][20];
        /* Time Stamp of the very first buffer sent to the Goes Archive */

    char channel_last_buffer_time[TOTAL_SOCKETS][20];
        /* Time Stamp of the very last buffer sent to the Goes Archive */

    int total_bytes_sent=0,hold_total_length=0;
        /* How many bytes have been sent this past second? */

/******* NEW (6/23/10) D. Niklewski (NOAA) *******/
    int channel_last_pcm_frame[TOTAL_SOCKETS];
        /* Last value of minor frame count (PCM) */
/******************** NEWEND *********************/

/******* NEW (11/05/10) D. Niklewski (NOAA) *******/
    double channel_last_pcm_time[TOTAL_SOCKETS];
        /* Last value of time when PCM frame was processed */
    double missing_major_frame_time = 24.576;
        /* Time used to test if there was one or more missing major frame(s); set it equal to about (3/4) the major frame rate.
           It only needs to be some large value <= 32.768, since if there's no missing frame at all, the time difference
           between two frames should be 1.024 seconds.  If there's an entire missing MAJOR frame (with the minor frame numbers
           being "correct" for continuous telemetry - e.g., 17 and 18), the time between two "adjacent" frames will be
           32.768 + 1.024 = 33.792 seconds.  */
/********************* NEWEND *********************/

#else

extern char debug_filename[256];
extern int debug_on;
extern short update_buffer_counter;
extern char archive_connections[MAX_ARCHIVE_MACHINES][20];
extern int archive_sockets[MAX_ARCHIVE_MACHINES];
extern int number_of_archive_connections;
extern fd_set read_mask,data_mask;
extern char archive_service_base[20];
extern char ipaddr_sc_map[MAX_GAIMCFG_ARCH_MACHINES][2][20];
extern int number_of_machines;
extern char error_message[1024];
extern int per_second_buffer_count;
extern int last_second;
extern int current_second;
extern int throttle_on;
extern int num_rt_connections;
extern char *buffers[MAX_PUB_PROCESSES];
extern int channel_buffer_count[TOTAL_SOCKETS];
extern int channel_buffer_failures[TOTAL_SOCKETS]; 
extern char channel_first_buffer_time[TOTAL_SOCKETS][20];
extern char channel_last_buffer_time[TOTAL_SOCKETS][20];
extern int total_bytes_sent,hold_total_length;

/******* NEW (6/23/10) D. Niklewski (NOAA) *******/
extern int channel_last_pcm_frame[TOTAL_SOCKETS];
extern double channel_last_pcm_time[TOTAL_SOCKETS];
extern double missing_major_frame_time;
/******************** NEWEND *********************/

#endif


