/*
@(#)  File Name: UBS_read_gaim_cfg.c  Release 1.3  Date: 04/03/17, 19:55:22
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBS_read_gaim_cfg.c
         EXE. NAME       :       Archive_Manager
         PROGRAMMER      :       S. Scoles

         VER.        DATE        BY              COMMENT
         V1.0        04/00       S. Scoles       Initial creation
		 8.4	   mar12-2004	 K. Dolinh       - add log_event, log_debug
********************************************************************************
	Invocation:

		UBS_read_gaim_cfg()

	Parameters:

		NONE

********************************************************************************
	Functional Description:

		This routine reads the gaim.cfg file and extracts what is needed from
		it.

*******************************************************************************/

/**

PDL:
	Open the gaim.cfg file.
	DOFOR each line in the archive file
		Read the next line of the file.
		IF this line is an ARCHIVE line THEN
			Extract the associated information.
		ENDIF
	ENDDO
	Close the gaim.cfg file.

**/

#include "util_log.h"
#include "UBS_globals.h"
	/* Defines, globals, and statics */

void UBS_read_gaim_cfg(void) {

FILE *gaim_cfg_ptr;
	/* File pointer to the gaim.cfg file */

char line_of_file[256];
	/* Read from the file */

int ctr=0;
	/* Current location in string */


number_of_machines=0;
	/* Zero this index */

gaim_cfg_ptr=fopen("gaim.cfg","r");
	/* open the file */

if (gaim_cfg_ptr) {
		/* if the file was found */

	while (!feof(gaim_cfg_ptr)) {
			/* While not end of file */

		fgets(line_of_file,255,gaim_cfg_ptr);
			/* Get next line of file */

		if (line_of_file[0]!='#' && !feof(gaim_cfg_ptr)) {
				/* Is this line not a comment and not the end of the
					file? */

			if (strstr(line_of_file,"ARCHIVE")) {
					/* Does this line contain the key we are looking for? */

				ctr=strlen(line_of_file)-1;
				while (line_of_file[ctr]==9 || line_of_file[ctr]==10 ||
					line_of_file[ctr]==13 || line_of_file[ctr]==' ') ctr--;
						/* Find first non-delimeter character */

				line_of_file[ctr+1]=0;
					/* Mark the end of the real string. */

				while (line_of_file[ctr]!=9 && line_of_file[ctr]!=' ' &&
					ctr>0) ctr--;
						/* Find the next delimeter */

				strcpy(ipaddr_sc_map[number_of_machines][0],
					&line_of_file[ctr+1]);
						/* Copy the destination directory name */

				while ((line_of_file[ctr]==9 || line_of_file[ctr]==' ') &&
					ctr>0) ctr--;
						/* Find the next parameter */

				line_of_file[ctr+1]=0;
					/* Mark the end of the real string. */

//				ctr-=2;
				ctr-=1;
					/* Jump back 2 chars to get SCID */

				if(ctr>=0)
				{
					strcpy(ipaddr_sc_map[number_of_machines][1],
						&line_of_file[ctr]);
						/* Copy the destination directory name */

					if (line_of_file[ctr]<'0' || line_of_file[ctr]>'9' ||
						line_of_file[ctr+1]<'0' || line_of_file[ctr+1]>'9')
							strcpy(ipaddr_sc_map[number_of_machines][1],"00");
							/* If the machine name does not end in a
								two-digit number, mark the SCID as 00 so
								we know there is no SCID attached to this
								ipaddress.  */
				}

				number_of_machines++;
					/* Increment the count */

			}
		}

	}
	fclose(gaim_cfg_ptr);

} else {
	log_event_err("read_gaim_cfg: cannot open file gaim.cfg");
}

} /* end UBS_read_gaim_cfg */

