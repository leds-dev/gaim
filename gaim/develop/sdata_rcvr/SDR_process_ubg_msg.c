/*
@(#)  File Name: SDR_process_ubg_msg.c  Release 1.6  Date: 05/04/04, 18:30:15
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       SDR_process_ubg_msg.c
         EXE. NAME       :       Stream_Data_Receiver
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         V3.0        05/21/02    A. Raj          Add net_read check for 0 length msg
         8.1         mar02-2004  K. Dolinh       - add log_event, log_debug
         	     mar04-2004  K. Dolinh       - restart ubg process if error msg
         	     mar10-2004                  - save ubg logfile
	 8.5	     mar22-2004                  - remove save
	             apr04-2005                  - term SDR on UBG messages

********************************************************************************
         Invocation:

             SDR_process_ubg_msg

********************************************************************************
         Functional Description:

         This routine processes messsages received from the
         Update_Buffer_Generator process.

*******************************************************************************/

/**
    read the data from the socket
    IF the data length is 8 THEN
        IF the UBG process completed THEN
            write log message
            CALL SDR_onintr
        ENDIF
        IF the UBG process failed THEN
            write log message
            CALL SDR_failure
        ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"
#include "net_util.h"

#include "SDR_params.h"
#include "SDR_varsex.h"
#include "util_log.h"

void SDR_onintr (int sig);
void SDR_failure (int from);

void SDR_process_ubg_msg (void)
{
char buffer [MAXBUFFLEN];
char msgtxt [MAXMSGTEXT];
int length;

/*** debug: restart UBG instead of terminating SDR ***/

/*  Read data from ubg socket  */
length = net_read (ubg_client_socket, buffer, MAXNBUFLEN);

if (length == 8) {

	/*  if UBG process completed  */
	if (strncmp (buffer, "zzzzzzzz", 8) == 0) {
		log_event("stream=%s received shutdown-job-complete msg from UBG",stream);

		/*** debug: save copy of log file 
		log_event("save ugb & ubs logfile: *.log --> *.log.save1");
	        system("cp gaim_sdr_ubg.log gaim_sdr_ubg.log.save1");
		system("cp gaim_ubs.log gaim_ubs.log.save1"); */

		/*log_event_err("restarting UBG for stream=%s",stream);
		SDR_spawn_task ();*/
		
		/* terminate sdr */
		SDR_onintr (0); 
	}

	/*  if UBG process failed  */
	if (strncmp (buffer, "z!z!z!z!", 8) == 0) {
		log_event_err("stream=%s received shutdown-job-failed msg from UBG",stream);

		/*** debug: save copy of log file 
		log_event("save ugb & ubs logfile: *.log --> *.log.save2");
	    	system("cp gaim_sdr_ubg.log gaim_sdr_ubg.log.save1");
	    	system("cp gaim_ubs.log gaim_ubs.log.save2"); */

		/* log_event_err("restarting UBG for stream=%s",stream);
		SDR_spawn_task ();*/
		
		/*  terminate sdr */
		SDR_failure (2); 
	}

} else if (length == 0) /* ARaj 06-21-02 */ {
	   log_event_err("stream=%s received zero-length message from UBG",stream);

	   /*** debug: save copy of log file 
	   log_event_err("save ugb & ubs logfile: *.log --> *.log.save3");
	   system("cp gaim_sdr_ubg.log gaim_sdr_ubg.log.save3");
	   system("cp gaim_ubs.log     gaim_ubs.log.save3"); */

	   /*log_event_err("restarting UBG for stream=%s",stream);
	   SDR_spawn_task ();*/
	   
	   /* terminate sdr */
	   SDR_onintr (0); 
}

return;
}
