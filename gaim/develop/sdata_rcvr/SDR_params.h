/*  
@(#)  File Name: SDR_params.h  Release 1.2  Date: 04/03/17, 19:32:38
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         INCLUDE         :       SDR_params.c
         EXE. NAME       :       Stream_Data_Receiver
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation

********************************************************************************
         Functional Description:

         This header file contains parameters used by the routines in 
         the Stream_Data_Receiver process.
	
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#ifndef TRUE
#  define TRUE (1)
#endif
#ifndef FALSE
#  define FALSE (0)
#endif
#ifndef NULL
#  define NULL 0
#endif

#define IFM_SERVICE	"gaim_ifm_sdr_"      /* IFM to SDR socket name prefix */
#define UBG_SERVICE	"gaim_sdr_ubg_"	     /* SDR to UBG socket name prefix */

#define MAXBUFFLEN	256	/* max number of characters in buffer         */	
#define MAXNBUFLEN	-256	/* max number of characters in buffer         */
#define MAXDBLEN	256	/* max number of characters in database name  */
#define MAXLINELEN	512	/* max number of characters in epoch.cfg line */	
#define MAXMSGTEXT	256	/* max number of characters in message text   */
#define MAXGASHOSTS	10	/* max number of GOES Archive host machines   */
#define MAXGTACSHOSTS	12	/* max number of GTACS host machines          */
#define MAXHOSTLEN	21	/* max number of characters in host name      */
#define MAXNUMPB	4	/* max number of playback streams             */
#define MAXNUMRT	16	/* max number of realtime streams             */
#define MAXNUMSIM	4	/* max number of simulation streams           */
#define MAXPROCLEN	21	/* max number of characters in process name   */
#define MAXSCIDLEN	3	/* max number of characters in scid name      */
#define MAXSERVLEN	51	/* max number of characters in service name   */
#define MAXSTRMLEN	21	/* max number of characters in stream name    */
