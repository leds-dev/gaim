/*  
@(#)  File Name: SDR_vars.h  Release 1.1  Date: 00/11/10, 13:32:56
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         INCLUDE         :       SDR_vars.c
         EXE. NAME       :       Stream_Data_Receiver
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation

********************************************************************************
         Functional Description:
         
         This include file contains variables that are used by the 
         Stream_Data_Receiver process.
	
*******************************************************************************/

#include <time.h>
#include "strmutil.p"

char			channel[3];
char			prime_config[3];
char			stream[MAXSTRMLEN];
char			gtacs_name[MAXHOSTLEN];
char			scid_name[MAXSCIDLEN];
char			archive_ip[MAXHOSTLEN];
char 			db_name[MAXDBLEN];
char			*debugfilename;
char			debug_message[MAXMSGTEXT];
int			sdr_failure_flag;
int			wait_for_ifm_flag;
int			ifm_sleep_flag;

fd_set			dt_mask;
fd_set			rd_mask;
int 			ifm_server_socket; 
int			ubg_client_socket; 
int			ubg_listening_socket;

struct timeval		client_timeout;
struct timeval 		max_interval;
struct timeval 		select_time;
client_connection	*connect_id;
client_point 		*db_name_pid;
client_point 		*prime_pid;
client_point 		*prime_arch_pid;
client_point 		*archive_mode_pid;
long			archive_mode;
