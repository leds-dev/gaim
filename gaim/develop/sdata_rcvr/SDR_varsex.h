/*  
@(#)  File Name: SDR_varsex.h  Release 1.1  Date: 00/11/10, 13:32:56
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         INCLUDE         :       SDR_vars.c
         EXE. NAME       :       Stream_Data_Receiver
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation

********************************************************************************
         Functional Description:
         
         This include file contains variables that are used by the 
         Stream_Data_Receiver process.
	
*******************************************************************************/

#include <time.h>
#include "strmutil.p"

extern char			channel[3];
extern char			prime_config[3];
extern char			stream[MAXSTRMLEN];
extern char			gtacs_name[MAXHOSTLEN];
extern char			scid_name[MAXSCIDLEN];
extern char			archive_ip[MAXHOSTLEN];
extern char 			db_name[MAXDBLEN];
extern char			*debugfilename;
extern char			debug_message[MAXMSGTEXT];
extern int			debug;
extern int			sdr_failure_flag;
extern int			wait_for_ifm_flag;
extern int			ifm_sleep_flag;

extern fd_set			dt_mask;
extern fd_set			rd_mask;
extern int 			ifm_server_socket; 
extern int			ubg_client_socket; 
extern int			ubg_listening_socket;

extern struct timeval		client_timeout;
extern struct timeval 		max_interval;
extern struct timeval 		select_time;
extern client_connection	*connect_id;
extern client_point 		*db_name_pid;
extern client_point 		*prime_pid;
extern client_point 		*prime_arch_pid;
extern client_point 		*archive_mode_pid;
extern long			archive_mode;
