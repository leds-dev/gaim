/*
@(#)  File Name: SDR_process_ifm_msg.c  Release 1.7  Date: 04/03/23, 22:08:52
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       SDR_process_ifm_msg.c
         EXE. NAME       :       Stream_Data_Receiver
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	     V3.0        05/21/02    A. Raj          Add net_read check for 0 length msg
		 8.1         mar05-2004  K. Dolinh       - add log_event, log_debug
		 12A         sep06-2007  K. Dolinh       - add terminate process

********************************************************************************
         Invocation:

             SDR_process_ifm_msg

********************************************************************************
         Functional Description:

         This routine processes messsages received from the N-Q_Interface_Manager
         process.

*******************************************************************************/

/**
    read the data from the socket
    IF the data length is greater than 7 THEN
        IF this is a shutdown message from IFM THEN
            write log message
            send sleep message to UBG
            disconnect from the GTACS stream
        ENDIF
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "strmerr.h"
#include "strmutil.p"
#include "net_util.h"

#include "SDR_params.h"
#include "SDR_varsex.h"
#include "util_log.h"

void SDR_onintr (int sig);

void SDR_process_ifm_msg (void)
{
char buffer [MAXBUFFLEN];
char buffer2 [MAXBUFFLEN];
char msgtxt [MAXMSGTEXT];
char tplen [4];
char msg1 [9] = "zzzzzzzz";
char msg2 [9] = "z!z!z!z!";
int length, length2;
short msglen = 8;

/*  Read data from IFM socket  */
length = net_read (ifm_server_socket, buffer, MAXNBUFLEN);

if (length > 7) {

	if ((strncmp (buffer, "zzzzzzzz", 8) == 0) || (strncmp (buffer, "z!z!z!z!", 8) == 0)) {

		/*  received shutdown message from IFM  */
		ifm_sleep_flag = 1;
		log_event("stream=%s received shutdown message from IFM",stream);

		/*  construct and send sleep message to UBG  */
		if (ubg_client_socket != -1){
			memcpy (&buffer2[0], (char *)&msglen, 2);
			memcpy (&buffer2[2], (char *)buffer, 8);
			log_event("stream=%s send IFM shutdown msg to UBG",stream);
			length2 = net_write (ubg_client_socket, buffer2, 10);
		}
		/* terminate process */
		SDR_onintr(0);

	/*  Disconnect from EPOCH stream  */
	if (connect_id != 0) disconnect_stream (connect_id);
	}

} else  if (length == 0) /* - ARaj 6-21-02 */ {

	   log_event("stream=%s received zero-length message from IFM",stream);
	   log_event("stream=%s close ifm socket and terminate SDR",stream);
	   shutdown (ifm_server_socket, 2);
	   close (ifm_server_socket);
	   FD_CLR(ifm_server_socket, &rd_mask);
	   ifm_server_socket = -1;
	   /* debug: terminate process */
	   SDR_onintr(0);
}

return;
}
