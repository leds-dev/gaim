/*
@(#)  File Name: SDR_spawn_task.c  Release 1.4  Date: 04/03/17, 19:32:41
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       SDR_spawn_task.c
         EXE. NAME       :       Stream_Data_Receiver
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         V2.0        05/21/02    A. Raj          Don't write spawn cmd to file
         8.1         mar05-2004  K. Dolinh       format syscall and spawn ubg
	 8.5         mar22-2004                  

********************************************************************************
         Invocation:

             SDR_spawn_task ()

********************************************************************************
         Functional Description:

         This routine writes a file that contains the task name and command
         line arguments along with everything else necessary to spawn the task.

*******************************************************************************/

/**
    open a file
    write the task name and command line arguments to the file
    close the file
    execute the file
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "strmerr.h"
#include "strmutil.p"

#include "SDR_params.h"
#include "SDR_varsex.h"
#include "util_log.h"

void SDR_spawn_task (void)
{
char syscall [MAXMSGTEXT];

/* close any current connection to ubg */
if (ubg_client_socket != -1) {
	shutdown (ubg_client_socket, 2);
	close (ubg_client_socket);
	FD_CLR(ubg_client_socket, &rd_mask);
	ubg_client_socket = -1;
}

strcpy (syscall, "./Update_Buffer_Generator -b ");
strcat (syscall, db_name);
strcat (syscall, " -d gaim_sdr_ubg.log -g ");
strcat (syscall, gtacs_name);
strcat (syscall, " -i ");
strcat (syscall, scid_name);
strcat (syscall, " -a ");
strcat (syscall, archive_ip);
strcat (syscall, " -p ");
strcat (syscall, prime_config);
strcat (syscall, " -r sdr -t ");
strcat (syscall, stream);
strcat (syscall, " -v ");
strcat (syscall, channel);
strcat (syscall, " -z R &");

log_event("start process UBG-%s for stream=%s ",channel,stream);
log_event(syscall);

system (syscall);


return;
}
