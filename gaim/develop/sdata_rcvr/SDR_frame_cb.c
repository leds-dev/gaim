/*
@(#)  File Name: SDR_frame_cb.c  Release 1.3  Date: 04/03/17, 19:32:34
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       SDR_frame_cb.c
         EXE. NAME       :       Stream_Data_Receiver
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	 8.1         mar05-2004  K. Dolinh       - add log_event, log_debug

********************************************************************************
         Invocation:

             SDR_frame_cb()

********************************************************************************
         Functional Description:

         This routine sends the raw telemetry frames received from GTACS to
         the Update_Buffer_Generator process.

*******************************************************************************/

/**
    poll the archive mode point
    IF poll is not successful THEN
        write log message
    ELSE
        set archive mode
    ENDIF
    IF archive mode is ON THEN
        determine the size of the raw telemetry frame
        IF the frame size is greater than 0 THEN
            build the buffer
            send the buffer to UBG
            free the memory allocated to the buffer
        ENDIF
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"
#include "net_util.h"

#include "SDR_params.h"
#include "SDR_varsex.h"
#include "util_log.h"

void SDR_frame_cb (client_frame* cfid)
{
int i, frame_length, length, status;
char *ubg_buffer;
short pkid = 0;
short pklen = 0;

status = poll_point (archive_mode_pid);
if (status != STRM_SUCCESS){
	log_event_err("stream=%s poll_point failed for ARCHIVE_MODE  stat=%d",stream,status);
} else {
	archive_mode = *(long *)archive_mode_pid->value;
  log_event("Got poll_point(ARCHIVE_MODE) = %ld", archive_mode);
}
if (archive_mode == 1) {

	/*  Determine the size of the raw telemetry frame  */
	frame_length = cfid->ufargs->frame_msg.frame_msg_len;

	/*  Build the UBG buffer  */
	if (frame_length > 0) {
		ubg_buffer = (char *)calloc(frame_length+42+2+1, sizeof(char));
		pklen = frame_length+42;
		memcpy (&ubg_buffer[0], (char *)&pklen, 2);
		memcpy (&ubg_buffer[2], (char *)&pkid, 2);
		memcpy (&ubg_buffer[4], (char *)&cfid->ufargs->header->receipt_time, 16);
		memcpy (&ubg_buffer[20], (char *)cfid->ufargs->header,24);
		memcpy (&ubg_buffer[44], (char *)cfid->ufargs->frame_msg.frame_msg_val,
							frame_length);

		/*  Send the UBG buffer to the Update_Buffer_Generator process  */
		length = net_write (ubg_client_socket, ubg_buffer, frame_length+42+2);

		/*  Free memory allocated to the UBG buffer  */
		free (ubg_buffer);
	}
}

return;
}
