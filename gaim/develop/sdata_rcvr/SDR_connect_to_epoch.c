/*
@(#)  File Name: SDR_connect_to_epoch.c  Release 1.4  Date: 04/03/17, 19:32:32
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       SDR_connect_to_epoch.c
         EXE. NAME       :       Stream_Data_Receiver
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         V2.0        05/02       A.Raj           check connect_id before disconnect_stream
	     8.1         mar05-2004  K. Dolinh       - add log_event, log_debug

********************************************************************************
         Invocation:

             SDR_connect_to_epoch ()

********************************************************************************
         Functional Description:

         This routine connects to EPOCH, requests raw frame service, and gets
         the database name.

*******************************************************************************/

/**
    connect to the GTACS stream
    IF connect is not successful THEN
        write log message
        CALL SDR_failure
    ENDIF
    request raw frame service from GTACS
    IF request is not successful THEN
        write log message
        disconnect from the stream
        CALL SDR_failure
    ENDIF
    lookup the database name for this stream
    IF lookup is not successful THEN
        write log message
        disconnect from the stream
        CALL SDR_failure
    ENDIF
    poll the database name for this stream
    IF poll is not successful THEN
        write log message
        disconnect from the stream
        CALL SDR_failure
    ENDIF
    lookup the prime indicator for this stream
    IF lookup is not successful THEN
        write log message
        disconnect from the stream
        CALL SDR_failure
    ENDIF
    lookup the prime archive indicator for this stream
    IF lookup is not successful THEN
        write log message
        disconnect from the stream
        CALL SDR_failure
    ENDIF
    lookup the archive mode indicator for this stream
    IF lookup is not successful THEN
        write log message
        disconnect from the stream
        CALL SDR_failure
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "SDR_params.h"
#include "SDR_varsex.h"
#include "util_log.h"

extern void SDR_frame_cb (client_frame *cfw);
void SDR_failure (int from);
static client_frame *fid;

void SDR_connect_to_epoch (void)
{
int status;
char msgtxt [MAXMSGTEXT];
char strm [MAXSTRMLEN];
char str [6];

/*  Connect to EPOCH stream */
status = connect_stream (stream, client_timeout, &connect_id);
if (status != STRM_SUCCESS) {
	log_event_err("stream=%s connect to GTACS stream failed stat=%d",stream,status);
	SDR_failure (1);
}

/*  Request raw frame service from EPOCH  */
status = request_frame (NULL, client_timeout, SDR_frame_cb, 0, &fid);
if (status != STRM_SUCCESS) {
	log_event_err("stream=%s request for GTACS frame service failed stat=%d",stream,status);
	if (connect_id) disconnect_stream (connect_id);
	SDR_failure (1);
}

/*  Get the database name from EPOCH for this stream  */
strcpy (strm, stream);
strcat (strm,":STREAM_DB");
status = look_up_point (strm, client_timeout, &db_name_pid);
if (status != STRM_SUCCESS) {
	log_event_err("stream=%s look_up_point STREAM_DB failed  stat=%d",stream,status);
	if (connect_id) disconnect_stream (connect_id);
	SDR_failure (1);
}

status = poll_point (db_name_pid);
if (status != STRM_SUCCESS) {
	log_event_err("stream=%s  poll_point STREAM_DB failed  stat=%d",stream,status);
	if (connect_id) disconnect_stream (connect_id);
	SDR_failure (1);
}

strcat (db_name, (char *)db_name_pid->value);

/*  Get the prime indicator from EPOCH for this stream  */
strcpy (strm, stream);
strcat (strm,":PRIME");
status = look_up_point (strm, client_timeout, &prime_pid);
if (status != STRM_SUCCESS) {
	log_event_err("stream=%s look_up_point PRIME failed stat=%d",stream,status);
	if (connect_id) disconnect_stream (connect_id);
	SDR_failure (1);
}

/*  Get the prime archive indicator from EPOCH for this stream  */
strcpy (strm, stream);
strcat (strm,":PRIME_ARCH");
status = look_up_point (strm, client_timeout, &prime_arch_pid);
if (status != STRM_SUCCESS) {
	log_event_err("stream=%s look_up_point PRIME_ARCH failed stat=%d",stream,status);
	if (connect_id) disconnect_stream (connect_id);
	SDR_failure (1);
}

/*  Get the archive mode indicator from EPOCH for this stream  */
strcpy (strm, stream);
strcat (strm,":ARCHIVE_MODE");
status = look_up_point (strm, client_timeout, &archive_mode_pid);
if (status != STRM_SUCCESS) {
	log_event_err("stream=%s look_up_point ARCHIVE_MODE failed stat=%d",stream,status);
	if (connect_id) disconnect_stream (connect_id);
	SDR_failure (1);
}

return;
}
