/*
@(#)  File Name: SDR_onintr.c  Release 1.3  Date: 04/03/17, 19:32:37
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       SDR_onintr.c
         EXE. NAME       :       Stream_Data_Receiver
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
		 8.1         mar05-2004  K. Dolinh       - add log_event, log_debug


********************************************************************************
         Invocation:

             SDR_onintr ()

********************************************************************************
         Functional Description:

         This routine shuts down the Stream_Data_Receiver process cleanly.

*******************************************************************************/

/**
    IF the SDR failure flag is a 1 THEN
        set message to "z!z!z!z!"
    ELSE
        set message to "zzzzzzzz"
    ENDIF
    send the message to IFM
    shutdown and close sockets
    send a message indicating that Stream_Data_Receiver is exiting
    EXIT
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "strmerr.h"
#include "strmutil.p"
#include "net_util.h"

#include <SDR_params.h>
#include <SDR_varsex.h>
#include "util_log.h"

void SDR_onintr (int sig)
{
char msgtxt [MAXMSGTEXT];
char buffer [MAXBUFFLEN];
int length;


/*  Shutdown connection to IFM  */
if (ifm_server_socket != -1) {

	/*  Send appropriate message to IFM  */
	if (sdr_failure_flag == 1)
		strcpy (buffer, "z!z!z!z!");
	else
		strcpy (buffer, "zzzzzzzz");
	length = net_write (ifm_server_socket, buffer, 8);

	shutdown (ifm_server_socket, 2);
	close (ifm_server_socket);
	ifm_server_socket = -1;
}

/*  Shutdown listen connection for UBG */
if (ubg_listening_socket != -1){
	shutdown (ubg_listening_socket, 2);
	close (ubg_listening_socket);
	ubg_listening_socket = -1;
}

/*  Shutdown client connection to UBG  */
if (ubg_client_socket != -1){
	shutdown (ubg_client_socket, 2);
	close (ubg_client_socket);
	ubg_client_socket = -1;
}

FD_ZERO (&rd_mask);

/*  Send message indicating Stream_Data_Receiver exiting  */
log_event("Stream_Data_Receiver is terminating gracefully");

exit (0);
}
