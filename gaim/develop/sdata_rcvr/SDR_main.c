/*
@(#)  File Name: SDR_main.c  Release 1.27  Date: 07/06/22, 15:08:17
*/

/*******************************************************************************
		 Kratos Defense
********************************************************************************

 PROJECT         :       GOES N-Q
 SOURCE          :       SDR_main.c
 EXE. NAME       :       Stream_Data_Receiver
 PROGRAMMER      :       B. Hageman

 VER.        DATE        BY              COMMENT
 V1.0        02/00       B. Hageman      Initial creation
 V2.0        05/02       A. Raj          Call SDR_connect_to_epoch
 V2.0        06/04/02    A. Raj          Don't terminate when stream fails
					 Wait for term from IFM upon read_streams failure
 8.1	     mar02-2004  K. Dolinh       - add log_event, log_debug
 8.2	     mar05-2004  K. Dolinh	 - put format syscall inside SDR_spawn_task()
 8.4	     mar12-2004		         - spawn ubg process again if it has terminated
 8.5	     mar22-2004
 8.7         apr15-2004
 8.8	     may06-2004
 8.B         sep17-2004                  - GTACS Build 8B
 9.1         jan06-2005                  - GTACS Build 9.1
 9A          feb22-2005                  - BUILD 9A
 9AP         mar16-2005                  - BUILD 9A Prime
 9AP         apr04-2005                  - BUILD 9A Prime.2
 9B          may01-2005                  - BUILD 9B
 9C          aug03-2005                  - BUILD 9C
 10A         aug26-2005                  - BUILD 10A
 10A.2       sep09-2005                  - BUILD 10A.2
 10B         jan12-2006                  - BUILD 10B
 10C         jul10-2006                  - BUILD 10C
 11          Apr13-2007                  - BUILD 11 
 12          Jun22-2007                  - BUILD 12
 12A         Sep05-2007                  - BUILD 12A
 										 process_ifm_msg: add call to SDR_onintr 
  
********************************************************************************
Invocation:

Stream_Data_Receiver {config} {channel} {stream} {gtacs} {scid}
                        {gasip} [debug] [{Debug File Name}]

Parameters:

{config}
 Prime configuration (np, p1, or p2).

{channel}
 Channel number for this instance of Stream_Data_Receiver.
 Range is from 1 to 8.

{stream}
 Stream name.

{gtacs}
 GTACS name.

{scid}
 Spacecraft ID.

{gasip}
 GOES Archive Server IP address.

debug
 Print Stream_Data_Receiver debug messages.  This is an
 optional parameter.  Default:  Don't print debug messages.

{Debug File Name}
 Name of the debug file to be created to keep track of
 Stream_Data_Receiver messages.  This is an optional
 parameter.  Default: No debug file (output to stdout).

********************************************************************************
         Functional Description:

         The Stream_Data_Receiver Process receives raw telemetry frames from
         GTACS and passes them on to the Update_Buffer_Generator process.

*******************************************************************************/

/**
    initialize the debug file name
    write log message
    CALL SDR_init
    initialize signal handler routines
    connect to the GAIM Interface Manager
    setup the Update_Buffer_Generator listening socket
    CALL SDR_connect_to_epoch
    gather arguments needed for spawning of UBG
    SDR_spawn_task
    write log message
    DOFOR ever
        IF input on a socket THEN
            IF UBG is requesting connection THEN
                connect to UBG
                IF connect is not successful THEN
                    write log message
                ELSE
                    set mask
                ENDIF
            ENDIF
            IF input on IFM socket THEN
                CALL SDR_process_ifm_msg
            ENDIF
            IF input on UBG socket THEN
                CALL SDR_process_ubg_msg
            ENDIF
        ENDIF
        read the GTACS telemetry stream
        IF read is not successful THEN
            write log message
            disconnect from the stream
            call SDR_failure
        ENDIF
    ENDDO
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>             /* Variable-length argument lists. */
#include "strmerr.h"
#include "strmutil.p"
#include "net_util.h"

#include "SDR_params.h"
#include "SDR_vars.h"

extern void SDR_connect_to_epoch (void);
extern void SDR_init (void);
extern void SDR_onintr (int);
extern void SDR_process_ifm_msg (void);
extern void SDR_process_ubg_msg (void);
extern void SDR_spawn_task (void);
void SDR_failure (int from);

#define SW_VERSION "Build 12A Sep-01-2007"
#define SW_ID      "SDR"

#include "../include/util_log.h"

int main (int  argc, char *argv[])

{
int length, status;
int dont_read_flag = 0;   /* Read from the stream and process */
char buffer [MAXBUFFLEN];
char msgtxt [MAXMSGTEXT];
char servifm [MAXSERVLEN];
char servubg [MAXSERVLEN];
int nfds = 0;

/*  Initialize the debug file name  */
if (argc == 8)
	{
	if (strncmp (argv[7], "debug", 5) == 0)
		{
		/* changed by Lata , originally debug was = 1 */
		debug = 0;
		debugfilename = "";
		}
	else
		{
		/* changed by Lata , originally debug was = 0 and hence not printing debug file */
		debug = 1;
		debugfilename = argv[7];
		}
	}
else if (argc == 9)
	{
	debug = 1;
	debugfilename = argv[8];
	}
else debugfilename = "";

/* copy SDR calling arguments */
strcpy (prime_config, argv[1]);
strcpy (channel, argv[2]);
strcpy (stream, argv[3]);
strcpy (gtacs_name, argv[4]);
strcpy (scid_name, argv[5]);
strcpy (archive_ip, argv[6]);

/*  Initialize SDR variables  */
SDR_init ();

/* get env variables */
gaim_home = (char *) getenv("GAIM_HOME");
if (!gaim_home) {
	/* exit if env var not defined */
	log_err("*** env variable GAIM_HOME is not defined");
	log_err("*** SDR terminating");
	exit(1);
}


/* init log message ID */
sprintf(logfile_msgid,"%s.%s",SW_ID,channel);
/* write generic startup messages */
log_event("  ");
log_event("Stream_Data_Receiver (%s) started ...",SW_VERSION);
log_event("logfile_name     = %s",logfile_name);
log_event("debug flag   = %d",debug);
/* write sdr startup params */
log_event("prime_config = %s",prime_config);
log_event("channel      = %s",channel);
log_event("stream       = %s",stream);
log_event("gtacs_name   = %s",gtacs_name);
log_event("scid_name    = %s",scid_name);
log_event("archive_ip   = %s",archive_ip);
log_event("  ");

/*  Initialize signal handler routines  */
signal (SIGINT, SDR_onintr);
signal (SIGTERM, SDR_onintr);

/*  Connect to the GAIM Interface Manager  */
strcpy (servifm, IFM_SERVICE);
strcat (servifm, channel);

if (net_call (NULL, servifm, &ifm_server_socket)) {
	log_event_err("stream=%s connect to IFM failed",stream);
	/*** SDR_onintr (); */
} else {
	FD_SET (ifm_server_socket, &rd_mask);
  if (ifm_server_socket > nfds) nfds = ifm_server_socket;
}

/*  Setup Update_Buffer_Generator listening socket  */
strcpy (servubg, UBG_SERVICE);
strcat (servubg, channel);

if (net_answer (servubg, -99, &ubg_listening_socket, &ubg_client_socket)){
	log_event_err("stream=%s init UBG listening socket failed",stream);
	/*** SDR_onintr (); */
} else{
	FD_SET (ubg_listening_socket, &rd_mask);
  if (ubg_listening_socket > nfds) nfds = ubg_listening_socket;
}

/*  Connect to EPOCH  */
SDR_connect_to_epoch ();

/*  Start Update Buffer Generator Process  */
SDR_spawn_task ();

/* do forever */
for (;;) {
	dt_mask = rd_mask;

	/*  Check for input on all sockets  */
	if (select (/*FD_SETSIZE*/ nfds+1, &dt_mask, NULL, NULL, &select_time) > 0) {
	
		/*  Check for UBG connection request  */
		if (ubg_listening_socket > 0) {
			if (FD_ISSET (ubg_listening_socket, &dt_mask)){
				if (net_answer (servubg, 99,
					&ubg_listening_socket, &ubg_client_socket)) {
					log_event_err("connect to UBG failed");
					ubg_client_socket = -1;
				} else {
					FD_SET (ubg_client_socket, &rd_mask);
          if (ubg_client_socket > nfds) nfds = ubg_client_socket;
				}
			}
		}

		/*  Process input on IFM server socket  */
		if (ifm_server_socket > 0) {
			if (FD_ISSET (ifm_server_socket, &dt_mask)) {
				SDR_process_ifm_msg ();
			}
		}

		/*  Process input on UBG client socket  */
		if (ubg_client_socket > 0) {
			if (FD_ISSET (ubg_client_socket, &dt_mask)) {
				SDR_process_ubg_msg ();
				}
		}
			
	} /* endif select  */

	 if (connect_id == (client_connection *) NULL ) /* ARaj - 5-29-02 */ {
		if (strncmp(prime_config, "np",2) == 0) {
		   log_event("connecting to stream=%s",stream);
		   SDR_connect_to_epoch ();
		} else {
		   dont_read_flag = 1;   /* stream is not there, just wait for termination from IFM */
								 /* we want to switch to new prime stream */
		}
	 }

	/*  Read GTACS telemetry stream  */
	if ((ubg_client_socket != -1) && (wait_for_ifm_flag == 0) &&
	    (ifm_sleep_flag == 0) && (dont_read_flag == 0))  /* ARaj */ {
		status = read_streams (max_interval);
		if (status != STRM_SUCCESS) {
			log_event_err("stream=%s read stream failure",stream);
			disconnect_stream (connect_id);
			connect_id = (client_connection *) NULL;
			wait_for_ifm_flag = 1;
			/* previously commented by anita.
			But I uncomment it so that when stream goes down, the
			corresponding sdr and ubg should go down . Issue P-371..Lata */
			SDR_failure (1);
		}
	}

} /* end forever loop */

/* end process */
SDR_failure (1);
}

#include "../include/util_log.c"
