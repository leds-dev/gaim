/*
@(#)  File Name: SDR_failure.c  Release 1.4  Date: 04/03/17, 19:32:33
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       SDR_failure.c
         EXE. NAME       :       Stream_Data_Receiver
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         V2.0        05/02       A. Raj          Add call to SDR_onintr
         8.1         mar05-2004  K. Dolinh       - add log_event, log_debug

********************************************************************************
         Invocation:

             SDR_failure (from)

         where

             <from> - I
                 is an integer indicating what caused this failure
                 (1 is GTACS, 2 is Update_Buffer_Generator).

********************************************************************************
         Functional Description:

         This routine handles fatal failures in the Stream_Data_Receiver.

*******************************************************************************/

/**
    IF connected to UBG THEN
        send sleep message to UBG
        CALL SDR_onintr
    ELSE
        CALL SDR_onintr
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"
#include "net_util.h"

#include "SDR_params.h"
#include "SDR_varsex.h"
#include "util_log.h"

void SDR_onintr (int sig);

void SDR_failure (int from)
{
char buffer [MAXBUFFLEN];
char msgtxt [MAXMSGTEXT];
char msg [9] = "zzzzzzzz";
int length;
short msglen = 8;

sdr_failure_flag = 1;

/*  Send sleep message to UBG if we are connected, then terminate sdr  */
if ((ubg_client_socket != -1) && (from == 1)) {
	log_event_err("SDR failure. send term msg to UBG");
	memcpy (&buffer[0], (char *)&msglen, 2);
	memcpy (&buffer[2], (char *)msg, 8);
	length = net_write (ubg_client_socket, buffer, 10);
	SDR_onintr (0);
} else {
	SDR_onintr (0);
}

return;
}
