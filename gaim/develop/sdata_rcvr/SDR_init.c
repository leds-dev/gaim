/*  
@(#)  File Name: SDR_init.c  Release 1.3  Date: 04/03/17, 19:32:35
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       SDR_init.c
         EXE. NAME       :       Stream_Data_Receiver
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation

********************************************************************************
         Invocation:
        
             SDR_init()
                         
********************************************************************************
         Functional Description:

         This routine initializes variables used by the Stream_Data_Receiver.
	
*******************************************************************************/

/**
    initialize the variables needed by Stream_Data_Receiver
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "SDR_params.h"
#include "SDR_varsex.h"

void SDR_init (void)
{
client_timeout.tv_sec = 60;
client_timeout.tv_usec = 0;
max_interval.tv_sec = 10;
max_interval.tv_usec = 0; 
select_time.tv_sec  = 0;
select_time.tv_usec = 200000;		
FD_ZERO (&rd_mask);
ifm_server_socket = -1;
ubg_client_socket = -1;
ubg_listening_socket = -1;
sdr_failure_flag = 0;
wait_for_ifm_flag = 0;
ifm_sleep_flag = 0;
archive_mode = 0;

return;
}
