/*
 @(#)  File Name: genTAG.c  Release 1.1  Date: 07/04/07, 16:13:50
 */


/*******************************************************************************
*	C a l l i n g   S e q u e n c e   D e s c r i p t i o n
********************************************************************************
*
*   Module : genTAG 
*
*	Usage:   
*   		cd $EPOCH_DATABASE/reports/goesNN
*		    genTAG filename 
*
*				filename = a databasename (without the *.lis extension)
*				It will read filename.lis and filename.gsa 
*
*   Version	Date		Author		Description
*   -------	----------	---------	-------------------------------
*   orig	Mar29-2007	K Dolinh    original version
*	   
*
********************************************************************************
*	F u n c t i o n a l   D e s c r i p t i o n
********************************************************************************
*      
*	This program reads files *.gsa and *.lis and generates file *.tag
*   which contains all global variable names with archive=y (from *.lis)
*   and their corresponding tags (from *.gsa).
*	
*
*******************************************************************************/


#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

int main (int argc, char *argv[])

{

#define MNEMONIC_SIZE 35
#define ARRAY_SIZE    20000
#define BUFSIZE    	  1024

char 	filename[1024];
char 	outfilename[1024];
char 	inbuf[BUFSIZE];
char 	outbuf[BUFSIZE];
char 	tmpbuf[BUFSIZE];
FILE 	*inFILE = NULL;
FILE 	*outFILE = NULL;
int		status,i;

char 	*tok1, *tok2, *tok3;
char	*eol;

char 	mnemonic[MNEMONIC_SIZE];
char    array_name[ARRAY_SIZE][MNEMONIC_SIZE];
int     array_tag[ARRAY_SIZE];
int		tag;
int		total_mnemonic = 0;
int		total_pseudo   = 0;
int		total_gvar     = 0;
int		total_match    = 0;

time_t 	current_time;
struct 	tm current_time_str;

char tagname[1024];
int	n;

if (argc <= 1) {
    printf("\nsyntax: genTAG filename (without lis extension)\n\n");
	exit(-1);
}

strcpy(tagname,(char *)argv[1]);
n = strlen(tagname) - 3;
sprintf(&tagname[n],"tag");
printf("n=%d  tagname=(%s)\n",n,tagname);

/* get current time */
status = time(&current_time);
current_time_str = *localtime(&current_time);

total_mnemonic = 0;
total_pseudo = 0;
total_gvar	 = 0;
total_match  = 0;

/***
 *** read *.gsa file and save name and tag of all pseudos
 ***/

/** open file **/
sprintf(filename,"%s.gsa",(char *)argv[1]);
inFILE = fopen(filename,"r");
if (!inFILE) {
	printf("*** Cannot open GSA file (%s)\n",filename);
	exit(-1);
}


/** read file until eof **/
while (!feof(inFILE)) {
			
	/** read a line from file **/
	if (fgets(&inbuf[0],BUFSIZE,inFILE) == NULL) break;
	memcpy(&tmpbuf,&inbuf,BUFSIZE);
			
	/** get first 2 tokens in line **/ 
	tok1 = (char *)strtok(tmpbuf," =;\n");
	tok2 = (char *)strtok(NULL," =;\n");

	if (tok1 && tok2) {
	
		/** save mnemonic name **/
		if (!strcasecmp(tok1,"BeginMnemonic")) {
			strncpy(mnemonic,tok2,sizeof(mnemonic));
			total_mnemonic++;
						
		/** save mnemonic tag **/
		} else if (!strcasecmp(tok1,"Tag")) {
			tag = atoi(tok2);
			
		/** save pseudo name and tag to array **/
		} else if (!strcasecmp(tok2,"PSU")) {
			array_tag[tag] = 1;
			strcpy(array_name[tag],mnemonic);
			total_pseudo++;
			
		}
						
	}
	
	
}

fclose(inFILE);

printf("\n*** processed GSA file (%s)\n",filename);			
printf("*** mnemonic = %d   pseudo = %d   \n\n",total_mnemonic,total_pseudo);

/***
 *** read *.lis file and save name and tag of all pseudos
 ***/

/** open file **/
sprintf(filename,"%s.lis",(char *)argv[1]);
inFILE = fopen(filename,"r");
if (!inFILE) {
	printf("*** Cannot open LIS file (%s)\n",filename);
	exit(-1);
}

/** open output ggsa file **/
sprintf(outfilename,"%s.tag",(char *)argv[1]);
outFILE = fopen(outfilename,"w");
if (!outFILE) {
	printf("*** Cannot open output file (%s)\n",outfilename);
	exit(-1);
} 

printf("*** Opened output file (%s)\n",outfilename);

/** read file until eof **/
while (!feof(inFILE)) {
			
	/** read a line from file **/
	if (fgets(&inbuf[0],BUFSIZE,inFILE) == NULL) break;
	memcpy(&tmpbuf,&inbuf,BUFSIZE);
			
	/** get first 2 tokens in line **/ 
	tok1 = (char *)strtok(tmpbuf," \"[]=;\n");
	tok2 = (char *)strtok(NULL,  " \"[]=;\n");
	tok3 = (char *)strtok(NULL,  " \"[]=;\n");
	
	if (tok1 && tok2 && tok3) {
	
		/** lookup global var name in pseudo array **/
		if (!strcasecmp(tok1,"GLOBAL_VAR")) {
			total_gvar++;
			/*printf("global name = (%s)\n",tok3);*/
			for (i=0; i<ARRAY_SIZE; i++) {
				if (!strcasecmp(tok3,array_name[i]) ) {
					total_match++;
					/* write name and tag to output *.ggsa file */
					fprintf(outFILE,"%6d  %s \n",i,tok3);
				}
			}		
								
		}
						
	}
	
	
}

fclose(inFILE);
fclose(outFILE);

printf("\n*** processed LIS file (%s)\n",filename);			
printf("*** globals = %d   match = %d   \n\n",total_gvar,total_match);
return 0;	

}


