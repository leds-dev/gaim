/*
 * @(#) goes gtacs util/dumpDEF.c 1.4 07/03/06 14:59:38
 */


/*******************************************************************************
*	C a l l i n g   S e q u e n c e   D e s c r i p t i o n
********************************************************************************
*
*   Module : dumpDEF 
*
*	Usage:   dumpDEF filename
*
*   Version	Date		Author		Description
*   -------	----------	---------	-------------------------------
*   orig	Mar06-2007	K Dolinh        original version
*	   
*
********************************************************************************
*	F u n c t i o n a l   D e s c r i p t i o n
********************************************************************************
*      
*	This program reads and dumps the contents of a save/restore file to stdout
*	It can handle 2 file types: *.def and *.gva
*
*******************************************************************************/


#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define MNEMONIC_SIZE 36

void dumpGVA(char *filename);
void dumpDEF(char *filename);

/* define gva record in *.gva file */
struct CheckGV {
	char	name[MNEMONIC_SIZE];	/* global variable name */
	int	status;		/* data quality */
	double	value;		/* data value */
	int	tag;		/* id (tag from gsa file) */
}; 
typedef struct CheckGV CheckGV;

/* define pseudo record in *.def file */
typedef	struct name_value {
	char	name[MNEMONIC_SIZE];
	unsigned int	status;
	double	value;
} name_value;


int main (int argc,char* argv[])
{

char *filename;
char *filetype;
int	len;

if (argc <= 1) {
    printf("\nsyntax: dumpDEF inputfile > outputfile\n\n");
	exit(-1);
}

filename = (char *)argv[1];

len = strlen(filename);
filetype = &filename[len-4];

if (!strcmp(filetype,".gva"))
	dumpGVA(filename);
else if (!strcmp(filetype,".def"))
	dumpDEF(filename);
else {
    printf("*** Cannot process filetype (%s)\n",filetype);
    exit(-1);
}
return 0;	

}

void 
dumpGVA (char *filename)

{
int i;
CheckGV		gva;
FILE  *fp;


/** open checkpoint file **/
if ( !(fp = fopen(filename, "r")) ) {
    printf("*** Cannot fopen %s\n", filename);
    printf("syntax: dumpDEF inputfile > outputfile\n\n");
    exit(-1);
}

printf("\n*** GVA filename = %s\n\n", filename);

printf("Record                           Mnemonic        Tag        Status(hex)     Value\n");
printf("------         --------------------------       ----        ----------      -----------------\n");

/* read all records in file */
i = 0;
while (fread(&gva, sizeof(CheckGV), 1, fp) == 1) {
	i++;
	if (strlen(gva.name) > 0) {
		printf("%4d  %35s      %5d          %8.8X      %g\n",
			i,gva.name,gva.tag,gva.status,gva.value);
	}	
		
}

printf("\n*** Total records read = %d\n",i);

/* close file */
fclose(fp);

}


void 
dumpDEF (char *filename)

{
int i;
int pseudo_count = 0;
int global_count = 0;
name_value def;
FILE  *fp;


/** open checkpoint file **/
if ( !(fp = fopen(filename, "r")) ) {
    printf("*** Cannot fopen %s\n", filename);
    printf("syntax: dumpchk inputfile > outputfile\n\n");
    exit(-1);
}

printf("\n*** DEF filename = %s\n\n", filename);

printf("Record                           Mnemonic       Status(hex)    Value\n");
printf("------         --------------------------       ----------     -----------------\n");

/* read all records in file */
i = 0;
while (fread(&def, sizeof(name_value), 1, fp) == 1) {
	i++;
	if (strlen(def.name) > 0) {
		/** exit loop if end-of-pseudo **/
		if (!strcmp(def.name,"#")) {
			/** start reading global records **/
			goto READ_GLOBALS;
		} else {
		printf("%4d  %35s       %8.8X       %g\n",i,def.name,def.status,def.value);
		pseudo_count = i;
		}
	}	
		
}

READ_GLOBALS:
	/* to be added */

printf("\n*** Total pseudos read = %d",pseudo_count);
printf("\n*** Total globals read = %d\n",global_count);

/* close file */
fclose(fp);

}
