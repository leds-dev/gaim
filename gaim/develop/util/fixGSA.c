/*
 @(#)  File Name: fixGSA.c  Release 1.1  Date: 07/04/07, 16:19:59
*/


/*******************************************************************************
*	C a l l i n g   S e q u e n c e   D e s c r i p t i o n
********************************************************************************
*
*   Module : dumpDEF 
*
*	Usage:   dumpDEF filename
*
*   Version	Date		Author		Description
*   -------	----------	---------	-------------------------------
*   orig	Mar23-2007	K Dolinh        original version
*	   
*
********************************************************************************
*	F u n c t i o n a l   D e s c r i p t i o n
********************************************************************************
*      
*	This program reads file *.gsa and fixes some syntax errors
*   For input "filename.gsa", it will generate file "filename.gsa.NEW"
*	
*
*******************************************************************************/


#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

int main (int argc, char *argv[])

{

#define MNEMONIC_SIZE 35

char 	*filename, *filetype;
char 	buffer[1024];
char 	buffer2[1024];
char 	gsa_text[1024];
char	outFilename[1024];
char 	*tok1;
char	*tok2;
char	*eol;
char 	mnemonic[MNEMONIC_SIZE];
FILE 	*inFILE = NULL;
FILE 	*outFILE = NULL;
int		n;
int		endMNE = 0;
int		beginMNE = 0;
int		total_mnemonic = 0;
int		donotwrite = 0;
int		missing_semi = 0;
int		missing_endMNE = 0;
int 	status;
time_t 	current_time;
struct 	tm current_time_str;


if (argc <= 1) {
    printf("\nsyntax: fixGSA filename\n\n");
	exit(-1);
}

filename = (char *)argv[1];

n = strlen(filename);
filetype = &filename[n-4];

if (strcmp(filetype,".gsa")) {
   printf("*** filename (%s) is not *.gsa \n",filename);
   exit(-1);
}

/** open input file **/
inFILE = fopen(filename,"r");
if (!inFILE) {
	printf("*** Cannot open GSA file (%s)\n",filename);
	exit(-1);
}

/** open output file **/
sprintf(outFilename,"%s.NEW",filename);
outFILE = fopen(outFilename,"w");
if (!outFILE) {
	printf("*** Cannot open output file (%s)\n",outFilename);
	exit(-1);
}

total_mnemonic = 0;

/* get current time */
status = time(&current_time);
current_time_str = *localtime(&current_time);

/** read file until eof **/
while (!feof(inFILE)) {
			
	/** read a line from file **/
	if (fgets(&buffer[0],sizeof(gsa_text),inFILE) == NULL) break;
	memcpy(&gsa_text,&buffer,sizeof(gsa_text));
			
	/** get first 2 tokens in line **/ 
	tok1 = (char *)strtok(gsa_text," =;\n");
	tok2 = (char *)strtok(NULL," =;\n");

	if (tok1) {
	
		/** save mnemonic name **/
		if (!strcasecmp(tok1,"BeginMnemonic")) {
			
			if (beginMNE > 0 && (beginMNE != endMNE) ) {	
				sprintf(buffer2,"EndMnemonic\n\n");
				fputs(buffer2,outFILE);
				endMNE = beginMNE;
				missing_endMNE++;
			} 
			
			n = strlen(buffer) - 2;
			if (buffer[n] != ';') {
				/*printf("*** gsa_text = (%s)\n",buffer);*/
				sprintf(buffer,"%s %s;\n",tok1,tok2);
				missing_semi++;
			}
			
			beginMNE++;
			
		} 
		
		if (!strncasecmp(tok1,"EndMnemonic",strlen("EndMnemonic") )) {
			endMNE++;
		}

		if (!strcasecmp(tok1,"DatabaseDescription")) {
			sprintf(buffer2,"Comment=fixGVA run on %4.4i-%3.3i-%2.2i:%2.2i:%2.2i\n",
            	current_time_str.tm_year+1900,current_time_str.tm_yday+1,
            	current_time_str.tm_hour,current_time_str.tm_min,
            	current_time_str.tm_sec);
			fputs(buffer2,outFILE);	
		}
			
	} else if (beginMNE != endMNE) {
		donotwrite = 1;
	}
	
	
	if (!donotwrite) fputs(buffer,outFILE);
	donotwrite = 0;
	return 0;
}


		
	printf("\n*** inserted %d missing semi-colons\n",missing_semi);
	printf("*** inserted %d missing EndMnemonic\n\n",missing_endMNE);
	
	
	/** close file **/
	fclose(inFILE);
	fclose(outFILE);
	return 0;
}


