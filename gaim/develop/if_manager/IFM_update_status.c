/*
 @(#)  File Name: IFM_update_status.c  Release 1.9  Date: 07/03/25, 08:33:17
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_update_status.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         V2.0        04/02       A. Raj          Reset strings after use
         V2.0        05/02       A. Raj          Correct fullstream length
	     8.1         mar02-2004  K. Dolinh       Add log_event, log_debug
	     8.3         mar10-2004  K. Dolinh		 ...
	 	 9A2         mar07-2005                  do not output fullstream=IDLE event
	 	 9AP	     mar16-2005                  Build 9A Prime
	 	 10A.3       Dec01-2005					 comment out call to set_point	
		 10B    	 Jan12-2006					 - recompile for Build 10B
		 
********************************************************************************
         Invocation:

             IFM_update_status (type, scid)

         where

             <type> - I
                 is an integer indicating 1 for RT, 2 for AM, or 3 for MM.

             <scid> - I
                 is a pointer to a character string containing the sc id

********************************************************************************
         Functional Description:

         This routine updates the stream status global variables on GTACS.

*******************************************************************************/

/**
    build a string containing all of the currently flowing stream names
    IF no streams are flowing THEN
        set string to "IDLE"
    ENDIF
    DOFOR each GTACS
        IF GTACS is operational THEN
            IF spacecraft 13 THEN
                set GTACS global variable
                write log message
            ELSEIF spacecraft 14 THEN
                set GTACS global variable
                write log message
            ELSEIF spacecraft 15 THEN
                set GTACS global variable
                write log message
            ELSEIF spacecraft 16 THEN
                set GTACS global variable
                write log message
            ELSEIF spacecraft 17 THEN
                set GTACS global variable
                write log message
            ELSEIF spacecraft 18 THEN
                set GTACS global variable
                write log message
            ELSEIF spacecraft 19 THEN
                set GTACS global variable
                write log message
            ELSEIF spacecraft 20 THEN
                set GTACS global variable
                write log message
            ENDIF
        ENDIF
    ENDDO
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

void IFM_update_status (int type, char* scid)
{
int i;
int success = 0;
int status = -1;
char msgtxt [MAXMSGTEXT];
/* char fullstream [(4*MAXSTRMLEN)+5]; */
char fullstream [(16*6)+16]; /* ARaj - 5/20/02 */
char statuschar [MAXMSGTEXT];

strcpy (fullstream, " ");
msgtxt[0] = '\0'; /* ARaj */

/*  Build the string containing the stream name(s)  */
if (type == 1) {
	/*  Realtime data (type 1) */
	for (i=1; i< 17; i++) {
		if ((sdr[i].active == 1) && (strncmp (sdr[i].scid, scid, 2) == 0)) {
			strcat (fullstream, sdr[i].stream);
			strcat (fullstream, " ");
			success = 1;
		}
	}

} else if (type == 2) {
	/*  Automerge data  (type 2) */
	for (i=17; i< 33; i++) {
		if ((pub[i].active == 1) && (pub[i].type == 2) &&
				(strncmp (pub[i].scid, scid, 2) == 0)) {
			strcat (fullstream, pub[i].stream);
			strcat (fullstream, " ");
			success = 1;
		}
	}

} else {
	/*  Manualmerge data (type 3) */
	for (i=17; i< 33; i++) {
		if ((pub[i].active == 1) && (pub[i].type == 3) &&
				(strncmp (pub[i].scid, scid, 2) == 0)) {
			strcat (fullstream, pub[i].stream);
			strcat (fullstream, " ");
			success = 1;
		}
	}
}

/* invalid data type */
if (success != 1) strcpy (fullstream, "IDLE");

/*  Set the appropriate global variable on operational GTACS  */

for (i=0; i< num_gtacs_hosts; i++) {

	if (goplist[i].operational == 1) {

		/* output event if archiving a stream */
		if (success == 1) {
		   if (type == 1){
			   log_event("update_status: set_point %s_ARCH_RT%2s on %s to (%s)",
				   gaimname,scid,goplist[i].gtacs_host,fullstream);
		   } else if (type == 2) {
			   log_event("update_status: set_point %s_ARCH_AM%2s on %s to (%s)",
				   gaimname,scid,goplist[i].gtacs_host,fullstream);
		   } else {
			   log_event("update_status: set_point %s_ARCH_MM%2s on %s to (%s)",
				   gaimname,scid,goplist[i].gtacs_host,fullstream);
		   }
		}

		if (strncmp (scid,"13",2) == 0) {
			if (type == 1)
				{
				strcpy ((char *)archive_rt_13_pid[i]->value, fullstream);
				archive_rt_13_pid[i]->status_bits = 0;
				/*status = set_point (archive_rt_13_pid[i]);*/
				}
			else if (type == 2)
				{
				strcpy ((char *)archive_am_13_pid[i]->value, fullstream);
				archive_am_13_pid[i]->status_bits = 0;
				/*status = set_point (archive_am_13_pid[i]);*/
				}
			else
				{
				strcpy ((char *)archive_mm_13_pid[i]->value, fullstream);
				archive_mm_13_pid[i]->status_bits = 0;
				/*status = set_point (archive_mm_13_pid[i]);*/
				}
			status = IFM_set_point_sarc_status("SARC_13_XX",0);

		} else if (strncmp (scid,"14",2) == 0) {
			if (type == 1)
				{
				strcpy ((char *)archive_rt_14_pid[i]->value, fullstream);
				archive_rt_14_pid[i]->status_bits = 0;
				/*status = set_point (archive_rt_14_pid[i]);*/
				}
			else if (type == 2)
				{
				strcpy ((char *)archive_am_14_pid[i]->value, fullstream);
				archive_am_14_pid[i]->status_bits = 0;
				/*status = set_point (archive_am_14_pid[i]);*/
				}
			else
				{
				strcpy ((char *)archive_mm_14_pid[i]->value, fullstream);
				archive_mm_14_pid[i]->status_bits = 0;
				/*status = set_point (archive_mm_14_pid[i]);*/
				}

		} else if (strncmp (scid,"15",2) == 0) {
			if (type == 1)
				{
				strcpy ((char *)archive_rt_15_pid[i]->value, fullstream);
				archive_rt_15_pid[i]->status_bits = 0;
				/*status = set_point (archive_rt_15_pid[i]);*/
				}
			else if (type == 2)
				{
				strcpy ((char *)archive_am_15_pid[i]->value, fullstream);
				archive_am_15_pid[i]->status_bits = 0;
				/*status = set_point (archive_am_15_pid[i]);*/
				}
			else
				{
				strcpy ((char *)archive_mm_15_pid[i]->value, fullstream);
				archive_mm_15_pid[i]->status_bits = 0;
				/*status = set_point (archive_mm_15_pid[i]);*/
				}

		} else if (strncmp (scid,"16",2) == 0) {
			if (type == 1)
				{
				strcpy ((char *)archive_rt_16_pid[i]->value, fullstream);
				archive_rt_16_pid[i]->status_bits = 0;
				/*status = set_point (archive_rt_16_pid[i]);*/
				}
			else if (type == 2)
				{
				strcpy ((char *)archive_am_16_pid[i]->value, fullstream);
				archive_am_16_pid[i]->status_bits = 0;
				/*status = set_point (archive_am_16_pid[i]);*/
				}
			else
				{
				strcpy ((char *)archive_mm_16_pid[i]->value, fullstream);
				archive_mm_16_pid[i]->status_bits = 0;
				/*status = set_point (archive_mm_16_pid[i]);*/
				}

		} else if (strncmp (scid,"17",2) == 0) {
			if (type == 1)
				{
				strcpy ((char *)archive_rt_17_pid[i]->value, fullstream);
				archive_rt_17_pid[i]->status_bits = 0;
				/*status = set_point (archive_rt_17_pid[i]);*/
				}
			else if (type == 2)
				{
				strcpy ((char *)archive_am_17_pid[i]->value, fullstream);
				archive_am_17_pid[i]->status_bits = 0;
				/*status = set_point (archive_am_17_pid[i]);*/
				}
			else
				{
				strcpy ((char *)archive_mm_17_pid[i]->value, fullstream);
				archive_mm_17_pid[i]->status_bits = 0;
				/*status = set_point (archive_mm_17_pid[i]);*/
				}

		} else if (strncmp (scid,"18",2) == 0) {
			if (type == 1)
				{
				strcpy ((char *)archive_rt_18_pid[i]->value, fullstream);
				archive_rt_18_pid[i]->status_bits = 0;
				/*status = set_point (archive_rt_18_pid[i]);*/
				}
			else if (type == 2)
				{
				strcpy ((char *)archive_am_18_pid[i]->value, fullstream);
				archive_am_18_pid[i]->status_bits = 0;
				/*status = set_point (archive_am_18_pid[i]);*/
				}
			else
				{
				strcpy ((char *)archive_mm_18_pid[i]->value, fullstream);
				archive_mm_18_pid[i]->status_bits = 0;
				/*status = set_point (archive_mm_18_pid[i]);*/
				}

		} else if (strncmp (scid,"19",2) == 0) {
			if (type == 1)
				{
				strcpy ((char *)archive_rt_19_pid[i]->value, fullstream);
				archive_rt_19_pid[i]->status_bits = 0;
				/*status = set_point (archive_rt_19_pid[i]);*/
				}
			else if (type == 2)
				{
				strcpy ((char *)archive_am_19_pid[i]->value, fullstream);
				archive_am_19_pid[i]->status_bits = 0;
				/*status = set_point (archive_am_19_pid[i]);*/
				}
			else
				{
				strcpy ((char *)archive_mm_19_pid[i]->value, fullstream);
				archive_mm_19_pid[i]->status_bits = 0;
				/*status = set_point (archive_mm_19_pid[i]);*/
				}

		} else if (strncmp (scid,"20",2) == 0) {
			if (type == 1)
				{
				strcpy ((char *)archive_rt_20_pid[i]->value, fullstream);
				archive_rt_20_pid[i]->status_bits = 0;
				/*status = set_point (archive_rt_20_pid[i]);*/
				}
			else if (type == 2)
				{
				strcpy ((char *)archive_am_20_pid[i]->value, fullstream);
				archive_am_20_pid[i]->status_bits = 0;
				/*status = set_point (archive_am_20_pid[i]);*/
				}
			else
				{
				strcpy ((char *)archive_mm_20_pid[i]->value, fullstream);
				archive_mm_20_pid[i]->status_bits = 0;
				/*status = set_point (archive_mm_20_pid[i]);*/
				}
			} /* endif scid check */

		/*
		if (status != STRM_SUCCESS)
			log_event_err("update_status: set_point failure on %s  status=%d",
				goplist[i].gtacs_host,status);
		*/
		/*status = flush_sets ();
		if (status != STRM_SUCCESS)
			log_event_err("update_status: flush_sets failure on %s  status=%d",
				goplist[i].gtacs_host,status);
		*/
		
	} /* endif gtacs is operational */
	msgtxt[0] = '\0'; /* Raj */

} /* end do num_gtacs_hosts */

fullstream[0] = '\0'; /* Raj */
return;
}
