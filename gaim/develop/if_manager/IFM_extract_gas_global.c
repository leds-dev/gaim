/*  
@(#)  File Name: IFM_extract_gas_global.c  Release 1.5  Date: 07/03/25, 08:32:46
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_extract_gas_global.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	 	 8.1         01/2004     K. Dolinh       Add log_event, log_debug
		 11			 Mar13-2007					 fix compiler warning
		 	 
********************************************************************************
         Invocation:
        
             IFM_extract_gas_global (index, buffer, global)
             
         where
         
             <index> - O
             	 is a pointer to an integer containing the index into the buffer
             <buffer> - I
                 is a pointer to a character string containing the message
             <global> - O
                 is a pointer to a structure containing the whole message
                         
********************************************************************************
         Functional Description:

         This routine extracts the body fields in a global update messsage 
         received from the GOES Archive Server.
	
*******************************************************************************/

/**
    extract the fields in the global update message body
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern char *IFM_swap_bytes (int);

void IFM_extract_gas_global (
		int *index,
		char *buffer,
		GasGlobalUpdateMsg *global)

{
long lnum;
short snum;
char *c;
char msgtxt[MAXMSGTEXT];

/*  Extract the fields in the message body  */

memcpy ((char *)(&snum), &buffer[*index], 2);
global->status = snum;
*index += 2;

memcpy ((char *)(&snum), &buffer[*index], 2);
global->gtacs_name_len = snum;
*index += 2;

if (global->gtacs_name_len > 0)			/* extract the GTACS name */
	{	
	global->gtacs_name = (char *)calloc(snum+1,sizeof(char));
	memcpy (global->gtacs_name, &buffer[*index], snum);
	*index += snum;
	}

memcpy ((char *)(&snum), &buffer[*index], 2);
global->scid_len = snum;
*index += 2;

global->scid = (char *)calloc(snum+1,sizeof(char));		/* extract the */
memcpy (global->scid, &buffer[*index], snum);			/*     scid    */
*index += snum;

memcpy ((char *)(&snum), &buffer[*index], 2);
global->archive_id_len = snum;
*index += 2;

global->archive_id = (char *)calloc(snum+1,sizeof(char));	/* extract the */
memcpy (global->archive_id, &buffer[*index], snum);		/*  archive id */
*index += snum;

memcpy ((char *)(&snum), &buffer[*index], 2);
global->gateway_id_len = snum;
*index += 2;

global->gateway_id = (char *)calloc(snum+1,sizeof(char));	/* extract the */
memcpy (global->gateway_id, &buffer[*index], snum);		/*  gateway id */
*index += snum;

memcpy ((char *)(&snum), &buffer[*index], 2);
global->non_prime_id_len = snum;
*index += 2;

if (global->non_prime_id_len > 0)		/* extract the nonprime id */
	{	
	global->non_prime_id = (char *)calloc(snum+1,sizeof(char));
	memcpy (global->non_prime_id, &buffer[*index], snum);
	*index += snum;
	}

memcpy ((char *)(&snum), &buffer[*index], 2);
global->prime_cmd_len = snum;
*index += 2;

if (global->prime_cmd_len > 0)			/* extract the prime cmd */
	{	
	global->prime_command = (char *)calloc(snum+1,sizeof(char));
	memcpy (global->prime_command, &buffer[*index], snum);
	*index += snum;
	}

memcpy ((char *)(&snum), &buffer[*index], 2);
global->non_prime_cmd_len = snum;
*index += 2;

if (global->non_prime_cmd_len > 0)		/* extract the nonprime cmd */
	{	
	global->non_prime_command = (char *)calloc(snum+1,sizeof(char));
	memcpy (global->non_prime_command, &buffer[*index], snum);
	*index += snum;
	}

if (global->gtacs_name_len > 2)	
	{
	if ((global->prime_cmd_len > 0) && (global->non_prime_cmd_len > 0))
		{	
		sprintf (msgtxt,"    Global message body structure is: ");
		log_debug(msgtxt);
		sprintf (msgtxt,"        %d %d %s %d %s %d %s %d %s %d %s %d %s %d %s",
			global->status, global->gtacs_name_len, global->gtacs_name, 
			global->scid_len, global->scid, 
			global->archive_id_len, global->archive_id, 
			global->gateway_id_len, global->gateway_id, 
			global->non_prime_id_len, global->non_prime_id,
			global->prime_cmd_len, global->prime_command,
			global->non_prime_cmd_len, global->non_prime_command);
		log_debug(msgtxt);
		}
	else if ((global->prime_cmd_len > 0) && (global->non_prime_cmd_len == 0))
		{	
		sprintf (msgtxt,"    Global message body structure is: ");
		log_debug(msgtxt);
		sprintf (msgtxt,"        %d %d %s %d %s %d %s %d %s %d %d %s %d",
			global->status, global->gtacs_name_len, global->gtacs_name, 
			global->scid_len, global->scid, 
			global->archive_id_len, global->archive_id, 
			global->gateway_id_len, global->gateway_id, 
			global->non_prime_id_len, 
			global->prime_cmd_len, global->prime_command,
			global->non_prime_cmd_len);
		log_debug(msgtxt);
		}
	else if ((global->prime_cmd_len == 0) && (global->non_prime_cmd_len > 0))
		{	
		sprintf (msgtxt,"    Global message body structure is: ");
		log_debug(msgtxt);
		sprintf (msgtxt,"        %d %d %s %d %s %d %s %d %s %d %s %d %d %s",
			global->status, global->gtacs_name_len, global->gtacs_name, 
			global->scid_len, global->scid, 
			global->archive_id_len, global->archive_id, 
			global->gateway_id_len, global->gateway_id, 
			global->non_prime_id_len, global->non_prime_id,
			global->prime_cmd_len, 
			global->non_prime_cmd_len, global->non_prime_command);
		log_debug(msgtxt);
		}
	else
		{	
		sprintf (msgtxt,"    Global message body structure is: ");
		log_debug(msgtxt);
		sprintf (msgtxt,"        %d %d %s %d %s %d %s %d %s %d %d %d",
			global->status, global->gtacs_name_len, global->gtacs_name, 
			global->scid_len, global->scid, 
			global->archive_id_len, global->archive_id, 
			global->gateway_id_len, global->gateway_id, 
			global->non_prime_id_len, 
			global->prime_cmd_len, 
			global->non_prime_cmd_len);
		log_debug(msgtxt);
		}
	}
else
	{
	sprintf (msgtxt,"    Global message body structure is: ");
	log_debug(msgtxt);
	sprintf (msgtxt,"        %d %d %d %s %d %s %d %s %d %d %s %d",
		global->status, global->gtacs_name_len, 
		global->scid_len, global->scid, 
		global->archive_id_len, global->archive_id, 
		global->gateway_id_len, global->gateway_id, 
		global->non_prime_id_len, 
		global->prime_cmd_len, global->prime_command,
		global->non_prime_cmd_len);
	log_debug(msgtxt);
	}

return;
}
