/*  
@(#)  File Name: IFM_socket_input.c  Release 1.13  Date: 07/03/25, 08:33:13
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_socket_input.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         V2.0        05/02       A. Raj          Add ioctl call to check if 
						 						 there is data to be read
         V2.0        06/27/02    A. Raj          Reset auto and man merge flags
	 	 8.1         02/2004     K. Dolinh       Add log_event, log_debug
	     10.B        02/16/2006  K. Dolinh       Debug. Change method to calc scidnum
		 10.C        07/10/2006                  recompile for Build 10C

********************************************************************************
         Invocation:
        
             IFM_socket_input()
                                     
********************************************************************************
         Functional Description:

         This routine checks for and processes input on all the sockets.
	
*******************************************************************************/

/**
    IF there is socket input THEN
        DOFOR each GOES Archive socket
            IF there is a connection request on this socket THEN
                DOCASE socket
                CASE 1 spacecraft executive
                    build service name
                CASE 2 auto merge
                    build service name
                CASE 3 manual merge
                    build service name
                CASE 4 ingest
                    build service name
                CASE 5 stats
                    build service name
                CASE 6 telemetry
                    build service name
                CASE 7 update
                    build service name
                CASE 8 ping
                    build service name
                ENDDO
                try to connect to socket
                IF connect fails THEN
                    write log message
                ELSE
                    set socket in mask
                ENDIF
            ENDIF
        ENDDO
        DOFOR each SDR socket
            IF there is a connection request on this socket THEN
                try to connect to socket
                IF connect fails THEN
                    write log message
                ELSE
                    set socket in mask
                ENDIF
            ENDIF
        ENDDO
        DOFOR each PUB socket
            IF there is a connection request on this socket THEN
                try to connect to socket
                IF connect fails THEN
                    write log message
                ELSE
                    set socket in mask
                    send merge buffer to PUB
                    free memory allocated to merge buffer
                ENDIF
            ENDIF
        ENDDO
        IF there is a connection request on CGT socket THEN
            try to connect to socket
            IF connect fails THEN
                write log message
            ELSE
                set socket in mask
            ENDIF
        ENDIF
        DOFOR each GOES Archive socket
            IF socket input exists THEN
                CALL IFM_process_gas_msg
            ENDIF
        ENDDO
        DOFOR each SDR socket
            IF socket input exists THEN
                CALL IFM_process_sdr_msg
            ENDIF
        ENDDO
        DOFOR each PUB socket
            IF socket input exists THEN
                CALL IFM_process_pub_msg
            ENDIF
        ENDDO
        If socket input exists on CGT socket THEN
            CALL IFM_process_cgt_msg
        ENDIF
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <sys/ioctl.h>         /* defined FIONREAD */
#include <sys/select.h>         /* defined FIONREAD */
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_vars.h"

void IFM_socket_input (void)
{
int i;
char str[3];
char strm [MAXSTRMLEN];
char msgtxt [MAXMSGTEXT];
char service [MAXSERVLEN];
long  llength=0;
int   scidnum;


/* commented out feb16-2006 
if (strncmp (pub[i].scid, "13", 2) == 0)  scidnum = 0;
     else if (strncmp (pub[i].scid, "14", 2) == 0)  scidnum = 1;
     else if (strncmp (pub[i].scid, "15", 2) == 0)  scidnum = 2;
     else if (strncmp (pub[i].scid, "16", 2) == 0)  scidnum = 3;
     else if (strncmp (pub[i].scid, "17", 2) == 0)  scidnum = 4;
     else if (strncmp (pub[i].scid, "18", 2) == 0)  scidnum = 5;
     else if (strncmp (pub[i].scid, "19", 2) == 0)  scidnum = 6;
     else if (strncmp (pub[i].scid, "20", 2) == 0)  scidnum = 7;
*/

     dt_mask = rd_mask;

     /*  Check for input on all of the sockets  */

     if (select (/*FD_SETSIZE*/ nfds+1, &dt_mask, NULL, NULL, &select_time) > 0) 
	     {

	     for (i=1; i< 9; i++)		/*  Check for GAS connection requests  */
		     {
		     if (gas_listening_socket[i] > 0) 
			     { 
			     if (FD_ISSET (gas_listening_socket[i], &dt_mask)) 
				     {

				     if (gas_client_socket[i]!=-1) {
					     shutdown (gas_client_socket[i], 2);
					     close (gas_client_socket[i]);
					     FD_CLR (gas_client_socket[i], &rd_mask);
					     gas_client_socket[i] = -1; 
				     }

				     switch (i) 
					     {
					     case 1:
						     strcpy (service, GAS_SCEXEC);
						     break;
					     case 2:
						     strcpy (service, GAS_AUTOMERGE);
						     break;
					     case 3:
						     strcpy (service, GAS_MANMERGE);
						     break;
					     case 4:
						     strcpy (service, GAS_INGEST);
						     break;
					     case 5:
						     strcpy (service, GAS_STATS);
						     break;
					     case 6:
						     strcpy (service, GAS_TLM);
						     break;
					     case 7:
						     strcpy (service, GAS_UPDATE);
						     break;
					     case 8:
						     strcpy (service, GAS_PING);
						     break;
					     }
				     sprintf (str, "%d", i);
				     if (net_answer (service, 99, 
						     &gas_listening_socket[i], &gas_client_socket[i])) 
					     {
					     strcpy (msgtxt, "IFM_socket_input: error connecting to GAS # ");
					     strcat (msgtxt, str);
					     log_event(msgtxt);
					     gas_client_socket[i] = -1;
					     }
				     else 
					     {
					     FD_SET (gas_client_socket[i], &rd_mask);
				     	     if (gas_client_socket[i] > nfds) nfds = gas_client_socket[i];
					     }
				     }
			     }
		     }

	     for (i=1; i< 17; i++)		/*  Check for SDR connection requests  */
		     {
		     if (sdr_listening_socket[i] > 0) 
			     {                    
			     if (FD_ISSET (sdr_listening_socket[i], &dt_mask)) 
				     {
				     strcpy (service, SDR_SERVICE);
				     sprintf (str, "%d", i);
				     strcat (service, str);                    
				     if (net_answer (service, 99, 
					     &sdr_listening_socket[i], &sdr_client_socket[i])) 
					     {
					     strcpy (msgtxt, "IFM_socket_input: error connecting to SDR # ");
					     strcat (msgtxt, str);
					     log_event(msgtxt);
					     sdr_client_socket[i] = -1;
					     }
				     else 
					     {
					     FD_SET (sdr_client_socket[i], &rd_mask);
				     	     if (sdr_client_socket[i] > nfds) nfds = sdr_client_socket[i];
					     }
				     }
			     }
		     }


	     for (i=17; i< 33; i++)		/*  Check for PUB connection requests  */
		     {
		     if (pub_listening_socket[i] > 0) 
			     {                    
			     if (FD_ISSET (pub_listening_socket[i], &dt_mask)) 
				     {

				     strcpy (service, PUB_SERVICE);
				     sprintf (str, "%d", i);
				     strcat (service, str);                    
				     if (net_answer (service, 99, 
					     &pub_listening_socket[i], &pub_client_socket[i])) 
					     {
					     strcpy (msgtxt, "IFM_socket_input: error connecting to PUB # ");
					     strcat (msgtxt, str);
					     log_event(msgtxt);
					     pub_client_socket[i] = -1;
					     }
				     else 
					     {
					     FD_SET (pub_client_socket[i], &rd_mask);
				     	     if (pub_client_socket[i] > nfds) nfds = pub_client_socket[i];
					     net_write (pub_client_socket[i], merge_buffer, mb_num_bytes);
					     free (merge_buffer);
					     }
				     }
			     }
		     }


	     if (cgt_listening_socket > 0)	/*  Check for CGT connection request  */
		     {                    
		     if (FD_ISSET (cgt_listening_socket, &dt_mask)) 
			     {

			     strcpy (service, CGT_SERVICE);
			     if (net_answer (service, 99, 
				     &cgt_listening_socket, &cgt_client_socket)) 
				     {
				     strcpy (msgtxt, "IFM_socket_input: error connecting to CGT");
				     log_event(msgtxt);
				     cgt_client_socket = -1;
				     }
			     else 
				     {
				     FD_SET (cgt_client_socket, &rd_mask);
				     if (cgt_client_socket > nfds) nfds = cgt_client_socket;
				     }
			     }
		     }

	     for (i=1; i< 9; i++)		/*  Process input on GAS sockets  */
		     {
		     if (gas_client_socket[i] > 0) 
			     {
			     if (FD_ISSET (gas_client_socket[i], &dt_mask)) 
				     {
            /* Input is pending.  Find out how many bytes of data
               are actually available for input.  If SELECT(2) indicates
               pending input, but IOCTL(2) indicates zero bytes of
               pending input, the connection is broken.  - ARaj */

               if (ioctl(gas_client_socket[i],FIONREAD,&llength) == -1) 
               {
                  /* ioctl failed -- */
		  sprintf(msgtxt,"IFM_socket_input: ioctl failed for GAS client socket %d", i);
		  log_event(msgtxt);
                  shutdown(gas_client_socket[i],SHUT_RDWR);
                  close(gas_client_socket[i]);
                  FD_CLR(gas_client_socket[i],&rd_mask);
                  gas_client_socket[i]=-1;
               } /* end if ioctl check -- */
               else /* ioctl was successful -- */
               {
                  if ((int) llength > 0)  /* pending input */
                  {
					      IFM_process_gas_msg (i); 
                  } /* end llength check -- */
                  else
                  {
                     sprintf(msgtxt,"IFM_socket_input: no data to read on GAS client socket %d", i);
                     log_debug(msgtxt);

                     shutdown(gas_client_socket[i],SHUT_RDWR);
                     close(gas_client_socket[i]);
                     FD_CLR(gas_client_socket[i],&rd_mask);
                     gas_client_socket[i]=-1;

                  } /* end else llength check */
					} /* end else ioctl check */
				} /* end FDISSET check */
			}
      } /* end for loop */

	     for (i=1; i< 17; i++)		/*  Process input on SDR sockets  */
		     {
		     if (sdr_client_socket[i] > 0) 
			     {
			     if (FD_ISSET (sdr_client_socket[i], &dt_mask)) 
				     {
            /* Input is pending.  Find out how many bytes of data
               are actually available for input.  If SELECT(2) indicates
               pending input, but IOCTL(2) indicates zero bytes of
               pending input, the connection is broken.  - ARaj */

               if (ioctl(sdr_client_socket[i],FIONREAD,&llength) == -1) 
               {
                  /* ioctl failed -- */
		  strcpy (msgtxt,"IFM_socket_input: ioctl failed for SDR socket");
		  log_event(msgtxt);
                  shutdown(sdr_client_socket[i],SHUT_RDWR);
                  close(sdr_client_socket[i]);
                  FD_CLR(sdr_client_socket[i],&rd_mask);
                  sdr_client_socket[i]=-1;

               } /* end if ioctl check -- */
               else /* ioctl was successful -- */
               {
                  if ((int) llength > 0)  /* pending input */
                  {
					      IFM_process_sdr_msg (i);
                  } /* end llength check -- */
                  else
                  {
                     sprintf(msgtxt,"IFM_socket_input: no data to read on SDR socket");
                     log_debug(msgtxt);
                     shutdown(sdr_client_socket[i],SHUT_RDWR);
                     close(sdr_client_socket[i]);
                     FD_CLR(sdr_client_socket[i],&rd_mask);
                     sdr_client_socket[i]=-1;

                  } /* end else llength check */
					} /* end else ioctl check */
				} /* end FD_ISSET check */
         }
		} /* end for loop */

	     for (i=17; i< 33; i++)		/*  Process input on PUB sockets  */
		     {
		     if (pub_client_socket[i] > 0)
			     {
			     if (FD_ISSET (pub_client_socket[i], &dt_mask)) 
				     {
            /* Input is pending.  Find out how many bytes of data
               are actually available for input.  If SELECT(2) indicates
               pending input, but IOCTL(2) indicates zero bytes of
               pending input, the connection is broken.  - ARaj */

               if (ioctl(pub_client_socket[i],FIONREAD,&llength) == -1) 
               {
                  /* ioctl failed -- */
		  		  strcpy (msgtxt,"IFM_socket_input: ioctl failed for PUB socket");
		  		  log_event(msgtxt);
                  shutdown(pub_client_socket[i],SHUT_RDWR);
                  close(pub_client_socket[i]);
                  FD_CLR(pub_client_socket[i],&rd_mask);
                  pub_client_socket[i]=-1;

  				  /* added feb16-2006 */
				  scidnum = atoi(pub[i].scid) - 13;
				  log_event("IFM_socket_input1: pub[%d].scid=(%s) scidnum=%d",i,pub[i].scid,scidnum);
	
	                /*  Reset the pub structure  */
                  pub[i].active = 0;		  
                  if (pub[i].type == 2)  automerge_flag[scidnum] = 0;
                  else  manmerge_flag[scidnum] = 0;

                  /*  Update the GTACS global variable  */
                  IFM_update_status (pub[i].type, pub[i].scid);

               } /* end if ioctl check -- */
               else /* ioctl was successful -- */
               {
                  if ((int) llength > 0)  /* pending input */
                  {
					      IFM_process_pub_msg (i); 
                  } /* end llength check -- */
                  else
                  {
                     sprintf(msgtxt,"IFM_socket_input: no data to read on PUB socket");
                     log_debug(msgtxt);
                     shutdown(pub_client_socket[i],SHUT_RDWR);
                     close(pub_client_socket[i]);
                     FD_CLR(pub_client_socket[i],&rd_mask);
                     pub_client_socket[i]=-1;

				  /* added feb16-2006 */
				  scidnum = atoi(pub[i].scid) - 13;
				  log_event("IFM_socket_input2: pub[%d].scid=(%s) scidnum=%d",i,pub[i].scid,scidnum);
				  

                     /*  Reset the pub structure  */
                     pub[i].active = 0;
                     if (pub[i].type == 2)  automerge_flag[scidnum] = 0;
                     else  manmerge_flag[scidnum] = 0;

                     /*  Update the GTACS global variable  */

                     IFM_update_status(pub[i].type, pub[i].scid);

                  } /* end else llength check */
					} /* end else ioctl check */
				} /* end FD_ISSET check */
			}
      } /* end for loop */

	     if (cgt_client_socket > 0)
		     {
		     if (FD_ISSET (cgt_client_socket, &dt_mask)) 
			     {
            /* Input is pending.  Find out how many bytes of data
               are actually available for input.  If SELECT(2) indicates
               pending input, but IOCTL(2) indicates zero bytes of
               pending input, the connection is broken.  - ARaj */

            if (ioctl(cgt_client_socket,FIONREAD,&llength) == -1) 
            {
               /* ioctl failed -- */
	       strcpy (msgtxt,"IFM_socket_input: ioctl failed for CGT socket");
	       log_event(msgtxt);
               shutdown(cgt_client_socket,SHUT_RDWR);
               close(cgt_client_socket);
               FD_CLR(cgt_client_socket,&rd_mask);
               cgt_client_socket=-1;

            } /* end if ioctl check -- */
            else /* ioctl was successful -- */
            {
               if ((int) llength > 0)  /* pending input */
               {
				      IFM_process_cgt_msg (); 
               } /* end llength check -- */
               else
               {
                  sprintf(msgtxt,"IFM_socket_input: no data to read on CGT socket");
                  log_debug(msgtxt);
                  shutdown(cgt_client_socket,SHUT_RDWR);
                  close(cgt_client_socket);
                  FD_CLR(cgt_client_socket,&rd_mask);
                  cgt_client_socket=-1;

               } /* end else llength check */
				     } /* end else ioctl check */
         } /* end FD_ISSET check */
		     } /* end if */

   } /* end if check */
     return;
}
