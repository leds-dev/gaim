/*  
@(#)  File Name: IFM_get_globals.c  Release 1.12  Date: 07/03/25, 08:32:50
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_get_globals.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         V1.0        04/02       A. Raj          Correct the prime array indexes
         8.0         mar02-2004  K. Dolinh       Add log_event, log_debug
         					                     Add counter poll_point_err 
	 			 8.8	       may06-2004        connected=0 means not-connected
	 			 12A         sep01-2007          Replaced strcat with sprintf
				 								 Replaces strcpy with strncpy
				   

********************************************************************************
         Invocation:
        
             IFM_get_globals()
                         
********************************************************************************
         Functional Description:

         This routine gets global variables from EPOCH.

*******************************************************************************/

/**
    IF it's time to monitor the globals THEN
        DOFOR each GTACS
            IF GTACS is operational THEN
                poll the prime spacecraft stream globals
                IF polls are successful THEN
                    get prime spacecraft streams
                    IF prime spacecraft streams don't match previous values THEN
                        write log message
                        CALL IFM_handle_prime_change
                    ENDIF
                    BREAK
                ELSE
                    write log message
                    disconnect from GTACS ground stream
                ENDIF
            ENDIF
        ENDDO
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"


void IFM_get_globals (void)
{
int i;
int status1, status2, status3, status4;
int status5, status6, status7, status8;
long mon_diff_time;
time_t mon_time2;
char extracted_host [MAXHOSTLEN];
char extracted_stream [MAXSTRMLEN];
char msgtxt [MAXMSGTEXT];
char str [MAXLINELEN];
char *p;



mon_time2 = time(NULL);
mon_diff_time = difftime (mon_time2, mon_time1);

if ((mon_diff_time >= gaim_monitor_rate) || (mon_first_time == 1))
        {
        for (i=0; i< num_gtacs_hosts; i++)
                {
                if (goplist[i].operational == 1)
                        {
                        status1 = poll_point (sc13_prime_pid[i]);
                        status2 = poll_point (sc14_prime_pid[i]);
                        status3 = poll_point (sc15_prime_pid[i]);
                        status4 = poll_point (sc16_prime_pid[i]);
                        status5 = poll_point (sc17_prime_pid[i]);
                        status6 = poll_point (sc18_prime_pid[i]);
                        status7 = poll_point (sc19_prime_pid[i]);
                        status8 = poll_point (sc20_prime_pid[i]);

                        if ((status1 == STRM_SUCCESS) && (status2 == STRM_SUCCESS) && 
                            (status3 == STRM_SUCCESS) && (status4 == STRM_SUCCESS) && 
                            (status5 == STRM_SUCCESS) && (status6 == STRM_SUCCESS) && 
                            (status7 == STRM_SUCCESS) && (status8 == STRM_SUCCESS))
                                {
                                /* clear consecutive poll_point error count */
                                goplist[i].pollpoint_err = 0;

                                mon_time1 = mon_time2;
                                mon_first_time = 0;

                                if (strlen ((char *)sc13_prime_pid[i]->value) > 2)      /*  GOES13  */
                                        {
                                        strcpy (str, (char *)sc13_prime_pid[i]->value);
                                        p = strtok (str, ":");
                                        if (p != NULL)
                                           strcpy (extracted_host, p);
                                        p = strtok ('\0'," ");
                                        if (p != NULL)
                                           strcpy (extracted_stream, p);
                                        /* if the last time was set to do-not-try, it is time to try. 
                                                Hence, set it to disconnected */
                                        if ((prime[0].connected == 2) && (prime[0].requested))
                                                prime[0].connected = 0;
                                        }
                                else
                                        {

                                /*      strcpy (extracted_stream, " ");
                                        strcpy (extracted_host, " ");
                                */
                                        strcpy (extracted_stream, prime[0].strm);
                                        strcpy (extracted_host,prime[0].host);
                                        prime[0].connected = 2; /* do-not-try to connect */

                                        }
                                if (prime[0].requested)
                                {
                                        if ((strcmp (prime[0].strm, extracted_stream) != 0) ||
                                                        (prime[0].connected == 0)) 
                                        {
                                                if (strcmp (prime[0].strm, extracted_stream) != 0)
                                                { 
                                                  strncpy (prime[0].host, extracted_host,MAXHOSTLEN);
                                                  strncpy (prime[0].oldstrm, prime[0].strm,MAXSTRMLEN);
                                                  strncpy (prime[0].strm, extracted_stream,MAXSTRMLEN);
                                                  prime[0].connected = 0; /* say not connected */
                                                  sprintf (msgtxt,"GTACS prime stream for sc 13 is %5.5s",prime[0].strm);
                                                  log_event(msgtxt);
                                                }
                                                IFM_handle_prime_change (prime[0].sc); 
                                        }
                                }

                                if (strlen ((char *)sc14_prime_pid[i]->value) > 2)      /*  GOES14  */
                                        {
                                        strcpy (str, (char *)sc14_prime_pid[i]->value);
                                        p = strtok (str, ":");
                                        if (p != NULL)
                                           strcpy (extracted_host, p);
                                        p = strtok ('\0'," ");
                                        if (p != NULL)
                                           strcpy (extracted_stream, p);
                                        /* if the last time was set to do-not-try, it is time to try.
                                                Hence, set it to disconnected */
                                        if ((prime[1].connected == 2) && (prime[1].requested))
                                                prime[1].connected = 0;

                                        }
                                else
                                        {
                                        strcpy (extracted_stream, prime[1].strm);
                                        strcpy (extracted_host,prime[1].host);
                                        prime[1].connected = 2; /* do-not-try to connect */

                                        }
					
		/* log_event("gtacs=%s  extracted_stream=%s  prime[1].req=%d  .connect=%d  .strm=%s  .oldstrm=%s",
			extracted_host,extracted_stream,prime[1].requested,prime[1].connected,prime[1].strm,prime[1].oldstrm);
			*/
                                if (prime[1].requested)
                                {
                                        if ((strcmp (prime[1].strm, extracted_stream) != 0) ||
                                                (prime[1].connected == 0))
                                        {
                                                if (strcmp (prime[1].strm, extracted_stream) != 0)
                                                {
														strncpy (prime[1].host, extracted_host,MAXHOSTLEN);
                                                        strncpy (prime[1].oldstrm, prime[1].strm,MAXSTRMLEN);
                                                        strncpy (prime[1].strm, extracted_stream,MAXSTRMLEN);
                                                        prime[1].connected = 0;
														sprintf (msgtxt,"GTACS prime stream for sc 14 is %5.5s",prime[1].strm);
                                                        log_event(msgtxt);
                                                }
                                                IFM_handle_prime_change (prime[1].sc);
                                        }
                                }

                                if (strlen ((char *)sc15_prime_pid[i]->value) > 2)      /*  GOES15  */
                                        {
                                        strcpy (str, (char *)sc15_prime_pid[i]->value);
                                        p = strtok (str, ":");
                                        if (p != NULL)
                                           strcpy (extracted_host, p);
                                        p = strtok ('\0'," ");
                                        if (p != NULL)
                                           strcpy (extracted_stream, p);
                                        /* if the last time was set to do-not-try, it is time to try.
                                                Hence, set it to disconnected */
                                        if ((prime[2].connected == 2) && (prime[2].requested))
                                                prime[2].connected = 0;

                                        }
                                else
                                        {
                                        strcpy (extracted_stream, prime[2].strm);
                                        strcpy (extracted_host,prime[2].host);
                                        prime[2].connected = 2; /* do-not-try to connect */

                                        }
                                if (prime[2].requested)
                                {
                                        if ((strcmp (prime[2].strm, extracted_stream) != 0) ||
                                                (prime[2].connected == 0))
                                        {
                                                if (strcmp (prime[2].strm, extracted_stream) != 0)
                                                {
                                                 		strncpy (prime[2].host, extracted_host,MAXHOSTLEN);
                                                  		strncpy (prime[2].oldstrm, prime[2].strm,MAXSTRMLEN);
                                                  		strncpy (prime[2].strm, extracted_stream,MAXSTRMLEN);
                                                        prime[2].connected = 0;
														sprintf (msgtxt,"GTACS prime stream for sc 15 is %5.5s",prime[2].strm);
                                                        log_event(msgtxt);
                                                }
                                                IFM_handle_prime_change (prime[2].sc);
                                        }
                                }

                                if (strlen ((char *)sc16_prime_pid[i]->value) > 2)      /*  GOES16  */
                                        {
                                        strcpy (str, (char *)sc16_prime_pid[i]->value);
                                        p = strtok (str, ":");
                                        if (p != NULL)
                                           strcpy (extracted_host, p);
                                        p = strtok ('\0'," ");
                                        if (p != NULL)
                                           strcpy (extracted_stream, p);
                                        /* if the last time was set to do-not-try, it is time to try.
                                                Hence, set it to disconnected */
                                        if ((prime[3].connected == 2) && (prime[3].requested))
                                                prime[3].connected = 0;

                                        }
                                else
                                        {
                                                strcpy (extracted_stream, prime[3].strm);
                                                strcpy (extracted_host,prime[3].host);
                                                prime[3].connected = 2; /* do-not-try to connect */

                                        }
                                if (prime[3].requested)
                                {
                                        if ((strcmp (prime[3].strm, extracted_stream) != 0) ||
                                                (prime[3].connected == 0))
                                        {
                                                if (strcmp (prime[3].strm, extracted_stream) != 0)
                                                {
                                                		strncpy (prime[3].host, extracted_host,MAXHOSTLEN);
                                                  		strncpy (prime[3].oldstrm, prime[3].strm,MAXSTRMLEN);
                                                  		strncpy (prime[3].strm, extracted_stream,MAXSTRMLEN);
                                                        prime[3].connected = 0;
														sprintf (msgtxt,"GTACS prime stream for sc 16 is %5.5s",prime[3].strm);
                                                        log_event(msgtxt);
                                                }
                                                IFM_handle_prime_change (prime[3].sc);
                                        }
                                }

                                if (strlen ((char *)sc17_prime_pid[i]->value) > 2)      /*  GOES17  */
                                        {
                                                strcpy (str, (char *)sc17_prime_pid[i]->value);
                                                p = strtok (str, ":");
                                                if (p != NULL)
                                                        strcpy (extracted_host, p);
                                                p = strtok ('\0'," ");
                                                if (p != NULL)
                                                        strcpy (extracted_stream, p);
                                                /* if the last time was set to do-not-try, it is time to try.
                                                        Hence, set it to disconnected */
                                                if ((prime[4].connected == 2) && (prime[4].requested))
                                                        prime[4].connected = 0;

                                        }
                                else
                                        {
                                        strcpy (extracted_stream, prime[4].strm);
                                        strcpy (extracted_host,prime[4].host);
                                        prime[4].connected = 2; /* do-not-try to connect */

                                        }
                                if (prime[4].requested)
                                {
                                        if ((strcmp (prime[4].strm, extracted_stream) != 0) ||
                                                (prime[4].connected == 0))
                                        {
                                                if (strcmp (prime[4].strm, extracted_stream) != 0)
                                                {
                                                		strncpy (prime[4].host, extracted_host,MAXHOSTLEN);
                                                  		strncpy (prime[4].oldstrm, prime[4].strm,MAXSTRMLEN);
                                                  		strncpy (prime[4].strm, extracted_stream,MAXSTRMLEN);
                                                        prime[4].connected = 0;
														sprintf (msgtxt,"GTACS prime stream for sc 17 is %5.5s",prime[4].strm);
                                                        log_event(msgtxt);
                                                }
                                                IFM_handle_prime_change (prime[4].sc);
                                        }
                                }

                                if (strlen ((char *)sc18_prime_pid[i]->value) > 2)      /*  GOES18  */
                                        {
                                        strcpy (str, (char *)sc18_prime_pid[i]->value);
                                        p = strtok (str, ":");
                                        if (p != NULL)
                                           strcpy (extracted_host, p);
                                        p = strtok ('\0'," ");
                                        if (p != NULL)
                                           strcpy (extracted_stream, p);

                                        /* if the last time was set to do-not-try, it is time to try.
                                                Hence, set it to disconnected */
                                        if ((prime[5].connected == 2) && (prime[5].requested))
                                                prime[5].connected = 0;

                                        }
                                else
                                        {
                                        strcpy (extracted_stream, prime[5].strm);
                                        strcpy (extracted_host,prime[5].host);
                                        prime[5].connected = 2; /* do-not-try to connect */

                                        }
                                if (prime[5].requested)
                                {
                                        if ((strcmp (prime[5].strm, extracted_stream) != 0) ||
                                                (prime[5].connected == 0))
                                        {
                                                if (strcmp (prime[5].strm, extracted_stream) != 0)
                                                {
                                                		strncpy (prime[5].host, extracted_host,MAXHOSTLEN);
                                                  		strncpy (prime[5].oldstrm, prime[5].strm,MAXSTRMLEN);
                                                  		strncpy (prime[5].strm, extracted_stream,MAXSTRMLEN);
                                                        prime[5].connected = 0;
														sprintf (msgtxt,"GTACS prime stream for sc 18 is %5.5s",prime[5].strm);
                                                        log_event(msgtxt);
                                                }
                                                IFM_handle_prime_change (prime[5].sc);
                                        }
                                }

                                if (strlen ((char *)sc19_prime_pid[i]->value) > 2)      /*  GOES19  */
                                        {
                                        strcpy (str, (char *)sc19_prime_pid[i]->value);
                                        p = strtok (str, ":");
                                        if (p != NULL)
                                           strcpy (extracted_host, p);
                                        p = strtok ('\0'," ");
                                        if (p != NULL)
                                           strcpy (extracted_stream, p);

                                        /* if the last time was set to do-not-try, it is time to try.
                                                Hence, set it to disconnected */
                                        if ((prime[6].connected == 2) && (prime[6].requested))
                                                prime[6].connected = 0;

                                        }
                                else
                                        {
                                        strcpy (extracted_stream, prime[6].strm);
                                        strcpy (extracted_host,prime[6].host);
                                        prime[6].connected = 2; /* do-not-try to connect */

                                        }
                                if (prime[6].requested)
                                {
                                        if ((strcmp (prime[6].strm, extracted_stream) != 0) ||
                                                (prime[6].connected == 0))
                                        {
                                                if (strcmp (prime[6].strm, extracted_stream) != 0)
                                                {
                                                		strncpy (prime[6].host, extracted_host,MAXHOSTLEN);
                                                  		strncpy (prime[6].oldstrm, prime[6].strm,MAXSTRMLEN);
                                                  		strncpy (prime[6].strm, extracted_stream,MAXSTRMLEN);
                                                        prime[6].connected = 0;
														sprintf (msgtxt,"GTACS prime stream for sc 19 is %5.5s",prime[6].strm);
                                                        log_event(msgtxt);
                                                }
                                                IFM_handle_prime_change (prime[6].sc);
                                        }
                                }

                                if (strlen ((char *)sc20_prime_pid[i]->value) > 2)      /*  GOES20  */
                                        {
                                        strcpy (str, (char *)sc20_prime_pid[i]->value);
                                        p = strtok (str, ":");
                                        if (p != NULL)
                                           strcpy (extracted_host, p);
                                        p = strtok ('\0'," ");
                                        if (p != NULL)
                                           strcpy (extracted_stream, p);

                                        /* if the last time was set to do-not-try, it is time to try.
                                                Hence, set it to disconnected */
                                        if ((prime[7].connected ==2) && (prime[7].requested))
                                                prime[7].connected = 0;

                                        }
                                else
                                        {
                                        strcpy (extracted_stream, prime[7].strm);
                                        strcpy (extracted_host,prime[7].host);
                                        prime[7].connected = 2; /* do-not-try to connect */

                                        }
                                if (prime[7].requested)
                                {
                                        if ((strcmp (prime[7].strm, extracted_stream) != 0) ||
                                                (prime[7].connected == 0))
                                        {
                                                if (strcmp (prime[7].strm, extracted_stream) != 0)
                                                {
                                                		strncpy (prime[7].host, extracted_host,MAXHOSTLEN);
                                                  		strncpy (prime[7].oldstrm, prime[7].strm,MAXSTRMLEN);
                                                  		strncpy (prime[7].strm, extracted_stream,MAXSTRMLEN);
                                                        prime[7].connected = 0;
														sprintf (msgtxt,"GTACS prime stream for sc 20 is %5.5s",prime[7].strm);
                                                        log_event(msgtxt);
                                                }
                                                IFM_handle_prime_change (prime[7].sc);
                                        }
                                }
                                break;          /*  We only need to get the values from one GTACS  */
                                }

                        else
                                {
                                /* increment consecutive poll_point error count */
                                goplist[i].pollpoint_err ++;
                                log_event_err("IFM_get_globals: poll_point of %s failed. errcount=%d",
                                        goplist[i].gtacs_host,goplist[i].pollpoint_err);
                                /* disconnect only if 2 consecutive errors */
                                if (goplist[i].pollpoint_err >= 2) {
                                        log_event("GAIM IFM is disconnecting from %s",goplist[i].gtacs_host);
                                        goplist[i].pollpoint_err = 0;
                                        goplist[i].operational = 0;
                                        /* Added next line wrt. issue P-371 ..Lata 
                                        strcpy (prime[i].strm, "UNDEFINED"); */
                                        if (gs_connect_id[i] != 0) disconnect_stream (gs_connect_id[i]);
                                        num_op_gtacs -= 1;
                                        if (num_op_gtacs == 0) 
                                                {
                                                first_gtacs_connection = 1;
                                                log_event("Will try to reconnect to first available GTACS");
                                                }
                                        }
                                }
                        }
                }
        IFM_write_status ();

        }

return;
}


