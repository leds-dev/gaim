/*  
@(#)  File Name: IFM_build_gas_msghdr.c  Release 1.4  Date: 07/03/25, 08:32:42
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_build_gas_msghdr.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation

********************************************************************************
         Invocation:
        
             IFM_build_gas_msghdr (index, buffer)
             
         where
         
             <index> - O
             	 is a pointer to an integer containing the index into the buffer
             <buffer> - O
                 is a pointer to a character string containing the message
                         
********************************************************************************
         Functional Description:

         This routine builds a GOES Archive message header.
	
*******************************************************************************/

/**
    set the message size field
    set the class id field
    set the message number field
    set the node name length field
    set the node name field
    set the process name length field
    set the process name field
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern char *IFM_swap_bytes(int);

void IFM_build_gas_msghdr (
		int *index,
		char *buffer,
		int t1,
		short t2,
		short t3,
		char *c5,
		char *c7 )

{
	short t4, t6;
	char *c;
	
	*index = 0;
	t4 = strlen (c5);
	t6 = strlen (c7);

	/*  Build the message size field  */
	
	long_data = t1;
	memcpy (&buffer[*index], (char *)(&long_data), 4);
	*index += 4;

	/*  Build the class id field  */
		
	short_data = t2;	
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	/*  Build the message number field  */
		
	short_data = t3;	
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;
	
	/*  Build the node name length field  */
	
	short_data = t4;	
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;
	
	/*  Build the node name field  */
		
	memcpy ((&buffer[*index]), c5, 6);
	*index += t4;
	
	/*  Build the process name length field  */
	
	short_data = t6;	
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;
	
	/*  Build the process name field  */
		
	memcpy ((&buffer[*index]), c7, 6); 
	*index += t6;	
	
	return;
}
