/*
@(#)  File Name: IFM_read_config.c  Release 1.8  Date: 07/03/25, 08:33:09
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_read_config.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         V2.0        04/02       A. Raj          Add error checking
	 	 8.1         02/2004     K. Dolinh       Add log_event, log_debug
	 					 						 Add counter poll_point_err
		 11			 Mar14-2007                  Changed gaimstring to 6 chars										 

********************************************************************************
         Invocation:

             IFM_read_config ()

********************************************************************************
         Functional Description:

         This routine reads both the GAIM and EPOCH configuration files.

*******************************************************************************/

/**
    open the GAIM configuration file
    IF open fails THEN
        write log message
        EXIT
    ENDIF
    DOWHILE there are lines to be read in the file
        IF the first 13 characters are "GAIM_MON_RATE" THEN
            set the GAIM monitor rate
        ELSEIF the first 5 characters are "GAIM " THEN
            set the GAIM name
        ELSEIF the first 7 characters are "ARCHIVE" THEN
            set the GOES Archive host names and addresses
        ELSEIF the first 5 characters are "GTACS" THEN
            set the GTACS host name and address
        ENDIF
    ENDDO
    close the GAIM configuration file
    IF close fails THEN
        write log message
    ENDIF
    write log messages
    open the EPOCH configuration file
    IF open fails THEN
        write log message
        EXIT
    ENDIF
    DOWHILE there are lines to be read in the file
        IF the first 6 characters are "STREAM" THEN
            IF this is a ground equipment stream THEN
                set the ground stream
            ELSEIF this is a realtime stream THEN
                set the realtime stream
            ELSEIF this is a simulation stream THEN
                set the simulation stream
            ELSEIF this is a playback stream THEN
                set the playback stream
            ENDIF
        ENDIF
    ENDDO
    close the EPOCH configuration file
    IF close fails THEN
        write log message
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

void IFM_read_config(void)
{
int i;
char msgtxt [MAXMSGTEXT];
char str [MAXLINELEN];
char gaimstring [6];
char hoststring [MAXHOSTLEN];
char ratestring [6];
char temp_host[MAXHOSTLEN];
char temp_stream[MAXSTRMLEN];
char *p;
FILE *fp;

strcpy (ratestring, " ");

/*  Open the GAIM Configuration File  */
if ((fp = fopen ("gaim.cfg", "r")) == NULL)
	{
	log_event("cannot open file=gaim.cfg. Terminating");
	exit(1);
	}

/*  Read the GAIM Configuration File  */

while (!feof(fp))
	{
	if (fgets (str, MAXLINELEN, fp))
		{
		if (strncmp (str, "GAIM_MON_RATE", 13) == 0)
			{
			p = strtok (str, " ");
			p = strtok ('\0'," ");
    		if (p != NULL) /* ARaj */
       			{
			   strncpy (ratestring, p, strlen(p)-1);
			   gaim_monitor_rate = atoi (ratestring);
       		}
		}
		else if (strncmp (str, "GAIM ", 5) == 0)
			{
			p = strtok (str, " ");
			p = strtok ('\0'," ");
    if (p != NULL) /* ARaj */
       {
			   strncpy (hoststring, p, strlen(p));
       }
			p = strtok ('\0'," ");
    if (p != NULL) /* ARaj */
       {
			   strncpy (gaimstring, p, strlen(p)-1);
			   gaimstring[5] = 0;
			   if (strncmp (hoststring, (char *)net_host("localhost"),
						   strlen ((char *)net_host("localhost"))) == 0)
				   strcpy (gaimname, gaimstring);
       }
			}
		else if (strncmp (str, "ARCHIVE", 7) == 0)
			{
			p = strtok (str, " ");
			p = strtok ('\0'," ");
    if (p != NULL) /* ARaj */
			   strncpy (gasoplist[num_gas_hosts].gas_host, p, strlen(p));
			p = strtok ('\0'," ");
    if (p != NULL) /* ARaj */
			   strncpy (gasoplist[num_gas_hosts].gas_addr, p, strlen(p));
			p = strtok ('\0'," ");
    if (p != NULL) /* ARaj */
			   strncpy (gasoplist[num_gas_hosts].gas_db_host, p, strlen(p));
			p = strtok ('\0'," ");
    if (p != NULL) /* ARaj */
       {
			   strncpy (gasoplist[num_gas_hosts].gas_db_addr, p, strlen(p)-1);
			   gasoplist[num_gas_hosts].operational = 0;
			   gasoplist[num_gas_hosts].signon_time = time(NULL);
			   num_gas_hosts += 1;
       }
			}
		else if (strncmp (str, "GTACS", 5) == 0)
			{
			p = strtok (str, " ");
			p = strtok ('\0'," ");
    if (p != NULL) /* ARaj */
			   strncpy (goplist[num_gtacs_hosts].gtacs_host, p, strlen(p));
			p = strtok ('\0'," ");
    if (p != NULL) /* ARaj */
       {
			strncpy (goplist[num_gtacs_hosts].gtacs_addr, p, strlen(p)-1);
			   goplist[num_gtacs_hosts].operational = 0;
			   goplist[num_gtacs_hosts].available = 0;
			   goplist[num_gtacs_hosts].pollpoint_err = 0;
			   if (strncmp (goplist[num_gtacs_hosts].gtacs_host, "egtacs01", 8) == 0)
				   gindex = num_gtacs_hosts;
			   num_gtacs_hosts += 1;
      }
			} /* end if GTACS */
		} /* end if fgets */
	} /* end while */

/*  Close the GAIM Configuration File  */
fclose (fp);


/* output rate event */
log_event("GAIM_MON_RATE = %d",gaim_monitor_rate);


/*  Open the EPOCH Configuration File  */

if ((fp = fopen ("epoch.cfg", "r")) == NULL)
	{
	log_event("cannot open file=epoch.cfg. Terminating");
	exit(1);
	}

/*  Read the EPOCH Configuration File  */

while (!feof(fp))
	{
	if (fgets (str, MAXLINELEN, fp))
		{
		if (strncmp (str, "STREAM", 6) == 0)
			{
			p = strtok (str, " \n\r\t");	/* STREAM */
			p = strtok ('\0'," \n\r\t");	/* stream name */
			strcpy (temp_stream, p);
			p = strtok ('\0'," \n\r\t");	/* HOST */
			p = strtok ('\0'," \n\r\t");	/* host name */
			strcpy (temp_host, p);
			p = strtok ('\0'," \n\r\t");	/* TYPE */
			p = strtok ('\0'," \n\r\t");	/* stream type */

			if (strncmp (p, "ground_equipment", 16) == 0)
				{
				for (i=0; i< num_gtacs_hosts; i++)
					{
					if (strcmp (temp_host, goplist[i].gtacs_host) == 0)
						{
						strcpy (goplist[i].grndstrm, temp_stream);
						break;
						}
					}
				}
			else if (strncmp (p, "REALTIME", 8) == 0)
				{
				for (i=0; i< num_gtacs_hosts; i++)
					{
					if (strcmp (temp_host, goplist[i].gtacs_host) == 0)
						{
						strcpy (goplist[i].rtstrm[num_rt_streams[i]],
							temp_stream);
						num_rt_streams[i] += 1;
						break;
						}
					}
				}
			else if (strncmp (p, "SIMULATION", 10) == 0)
				{
				for (i=0; i< num_gtacs_hosts; i++)
					{
					if (strncmp (temp_host, "local", 5) == 0)
						strcpy (temp_host, (char *)net_host ("localhost"));
					if (strcmp (temp_host, goplist[i].gtacs_host) == 0)
						{
						strcpy (goplist[i].simstrm[num_sim_streams[i]],
							temp_stream);
						num_sim_streams[i] += 1;
						break;
						}
					}
				}
			else if (strncmp (p, "PLAYBACK", 8) == 0)
				{
				for (i=0; i< num_gtacs_hosts; i++)
					{
					if (strncmp (temp_host, "local", 5) == 0)
						strcpy (temp_host, (char *)net_host ("localhost"));
					if (strcmp (temp_host, goplist[i].gtacs_host) == 0)
						{
						strcpy (goplist[i].pbstrm[num_pb_streams[i]],
							temp_stream);
						num_pb_streams[i] += 1;
						break;
						}
					}
				}
			}
		}
	}

/*  Close the EPOCH Configuration File  */
fclose (fp);

return;
}
