/*  
@(#)  File Name: IFM_extract_gas_signon.c  Release 1.5  Date: 07/03/25, 08:32:49
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_extract_gas_signon.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	 	 8.1       01/2004       K. Dolinh       Add log_event, log_debug
		 11		   Mar13-2007					 fix compiler warning

********************************************************************************
         Invocation:
        
             IFM_extract_gas_signon (index, buffer, sign_on)
             
         where
         
             <index> - O
             	 is a pointer to an integer containing the index into the buffer
             <buffer> - I
                 is a pointer to a character string containing the message
             <sign_on> - O
                 is a pointer to a structure containing the whole message
                         
********************************************************************************
         Functional Description:

         This routine extracts the body fields in a signon messsage received
         from the GOES Archive Server.
	
*******************************************************************************/

/**
    extract the fields in the signon message body
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern char *IFM_swap_bytes (int);

void IFM_extract_gas_signon (
		int *index,
		char *buffer,
		GasSignonMsg *sign_on)

{
long lnum;
short snum;
char *c;
char message[256];

/*  Extract the fields in the message body  */

memcpy ((char *)(&snum), &buffer[*index], 2);
sign_on->state = snum;
*index += 2;

memcpy ((char *)(&snum), &buffer[*index], 2);
sign_on->archive_name_len = snum;
*index += 2;

sign_on->archive_name = (char *)calloc(snum+1,sizeof(char));	/* extract the  */
memcpy (sign_on->archive_name, &buffer[*index], snum);		/* archive name */
*index += snum;

memcpy ((char *)(&snum), &buffer[*index], 2);
sign_on->status = snum;
*index += 2;

memcpy ((char *)(&snum), &buffer[*index], 2);
sign_on->gtacs_name_len = snum;
*index += 2;

if (sign_on->gtacs_name_len > 0)		/* extract the GTACS name */
	{	
	sign_on->gtacs_name = (char *)calloc(snum+1,sizeof(char));
	memcpy (sign_on->gtacs_name, &buffer[*index], snum);
	*index += snum;
	}

 if (sign_on->gtacs_name_len > 0)
	{	
	sprintf (message,"    Signon/signoff message body structure is: ");
	log_debug(message);
	sprintf (message,"        %d %d %s %d %d %s",
		sign_on->state, sign_on->archive_name_len, sign_on->archive_name, 
		sign_on->status, sign_on->gtacs_name_len, sign_on->gtacs_name);
	log_debug(message);
	}
else
	{
	sprintf (message,"    Signon/signoff message body structure is: ");
	log_debug(message);
	sprintf (message,"        %d %d %s %d %d",
		sign_on->state, sign_on->archive_name_len, sign_on->archive_name, 
		sign_on->status, sign_on->gtacs_name_len);
	log_debug(message);
	} 

return;
}
