/*  
@(#)  File Name: IFM_send_gas_global.c  Release 1.8  Date: 07/03/25, 08:33:10
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_send_gas_global.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	 	 8.1         02/2004     K. Dolinh       Add log_event, log_debug
		 11			 Mar13-2007					 fix compile warning
		 
********************************************************************************
         Invocation:
        
             IFM_send_gas_global (gas_index, global)
             
         where
         
             <gas_index> - I
                 is an integer indicating which GOES Archive process sent the
                 message.

             <global> - I
                 is the structure containing the received global message
                         
********************************************************************************
         Functional Description:

         This routine sends a global response message to the GOES Archive 
         Server.
	
*******************************************************************************/

/**
    CALL IFM_build_gas_global
    DOFOR each GOES Archive server
        IF global message node name matches this GOES Archive server THEN
            set success flag to true
            BREAK
        ENDIF
    ENDDO
    IF success flag is set to true THEN
        send global message to GOES Archive server
        IF send fails THEN
            write log message
        ELSE
            write log message
            shutdown and close GOES Archive server socket
        ENDIF
    ELSE
        write log message
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern void IFM_onintr_timer (int);
extern char *IFM_swap_bytes(void);

void IFM_send_gas_global (
		int gas_index,
		GasGlobalUpdateMsg global)
		
{
int i, index, length;
int success = 0;
short cid, mnum;
long msize;
char buffer [MAXBUFFLEN];
char host [MAXHOSTLEN];
char global_service [MAXSERVLEN];
char msgtxt [MAXMSGTEXT];
char nname [MAXHOSTLEN];
char proc [MAXPROCLEN];
char cseqnum [6];
char str [3];
char *c;
struct itimerval timer;

timer.it_value.tv_sec = 2;
timer.it_value.tv_usec = 0;
timer.it_interval.tv_sec = 0;
timer.it_interval.tv_usec = 0;

/*  Build the global message  */

IFM_build_gas_global (&index, global, &buffer[0]);

/*  Determine the correct GOES Archive Server to send this message to  */

for (i=0; i< num_gas_hosts; i++)
	{
	if (strcmp (global.msg_hdr.node_name, gasoplist[i].archive_name) == 0)
		{
		strcpy (host, gasoplist[i].gas_addr);
		success = 1;
		break;
		}
	}

/*  Connect to the GOES Archive Server and send the message  */

if (success)
	{
	strcpy (global_service, GAS_SCEXEC);
	/* sprintf (str, "%d", gas_index+12);
	strcat (global_service, str); */

	signal (SIGALRM, IFM_onintr_timer);

	if (setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL))
		{
		strcpy (msgtxt, "IFM_send_gas_global: set itimer failed");
		log_event(msgtxt);
		}

 	if (net_call (host, global_service, &gas_server_socket))  
		{
		timer.it_value.tv_sec = 0;
		timer.it_value.tv_usec = 0;
		timer.it_interval.tv_sec = 0;
		timer.it_interval.tv_usec = 0;
		setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL);

		strcpy (msgtxt, "IFM_send_gas_global: failed to connect to GAS on ");
		strcat (msgtxt, global.msg_hdr.node_name);
		log_event(msgtxt);		
		}
	else 
		{
		timer.it_value.tv_sec = 0;
		timer.it_value.tv_usec = 0;
		timer.it_interval.tv_sec = 0;
		timer.it_interval.tv_usec = 0;
		setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL);

  		length = net_write (gas_server_socket,(char *)&buffer, index);
 		strcpy (msgtxt, "IFM sent Global Status Message # ");
  		sprintf (cseqnum, "%d", global_seq_num);
  		strcat (msgtxt, cseqnum);
  		strcat (msgtxt, " to GaSeExec on ");
  		strcat (msgtxt, global.msg_hdr.node_name);
		log_debug(msgtxt); 
  		shutdown (gas_server_socket, 2); 
 		close (gas_server_socket);
 		gas_server_socket = -1;

 		/*  Update the sequence number for this message type  */

 		global_seq_num += 1;
		if (global_seq_num >= 32768)  global_seq_num = 1;
		}
	}
else
	{
	strcpy (msgtxt, "IFM_send_gas_global: cannot resolve GAS hostname=");
	strcat (msgtxt, global.msg_hdr.node_name);
	log_event(msgtxt);		
	}

return;
}
