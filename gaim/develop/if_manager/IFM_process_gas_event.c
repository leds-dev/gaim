/*
@(#)  File Name: IFM_process_gas_event.c  Release 1.8  Date: 07/03/25, 08:33:01
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_process_gas_event.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	 	 8.1         01/2004     K. Dolinh       Add log_event, log_debug
	 	 9.1         Jan06-2005                  Removed msg "send event to Prime..." 
		 11          Aug11-2006                  use IFM_send_event

********************************************************************************
         Invocation:

             IFM_process_gas_event (gas_index, buffer)

         where

             <gas_index> - I
                 is an integer indicating which GOES Archive process sent the
                 message.

             <buffer> - I
                 is a pointer to a character string containing the message

********************************************************************************
         Functional Description:

         This routine processes event messsages received from the GOES Archive
         Server.

*******************************************************************************/

/**
    CALL IFM_extract_gas_msghdr
    write log message
    CALL IFM_extract_gas_event
    DOFOR each GTACS
        IF event message needs to be sent to all GTACS THEN
            IF GTACS is operational THEN
                send the event message to GTACS
            ENDIF
        ELSE
            IF GTACS is operational THEN
                send the event message to GTACS
            ENDIF
        ENDIF
    ENDDO
    free all of the allocated memory for this message
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"


void IFM_process_gas_event (
		int gas_index,
		char *buffer)

{
int i, scidnum, index = 0;
char msgtxt [MAXMSGTEXT];
GasEventMsg event;

/*  Extract the fields in the message header  */
IFM_extract_gas_msghdr (&index, buffer, &event.msg_hdr);

sprintf (msgtxt,"    Event message header structure is: ");
log_debug(msgtxt);
sprintf (msgtxt,"        %d %d %d %d %s %d %s",
	event.msg_hdr.message_size, event.msg_hdr.class_id,
	event.msg_hdr.message_number, event.msg_hdr.node_name_len,
	event.msg_hdr.node_name, event.msg_hdr.process_name_len,
	event.msg_hdr.process_name);
log_debug(msgtxt);

/*  Extract the fields in the message body  */
IFM_extract_gas_event(&index, buffer, &event);

/* write GAS message to gaim log */
sprintf(msgtxt,"node(%s) %s",event.msg_hdr.node_name,event.event_text);
log_event(msgtxt);

/* send GAS message to gtacs ground streams */
if (event.event_id == 930)
	IFM_send_event(EVENT_GAS_NORMAL,msgtxt);
else
	IFM_send_event(EVENT_GAS_ERR,msgtxt);

/*  Free all of the allocated memory for this message  */
free (event.msg_hdr.node_name);
free (event.msg_hdr.process_name);
free (event.event_text);
free (event.time_tag);
if (event.dest_tcs_len > 0)  free (event.dest_tcs);
if (event.scid_len > 0)  free (event.scid);

return;
}

