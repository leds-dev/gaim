/*
@(#)  File Name: IFM_main.c  Release 1.23  Date: 07/06/22, 15:03:24
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_main.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         8.1       jan01-2004    K. Dolinh       - pass on debug flag to spawned task
                                                     - add log_event, log_debug
                                                     - add poll_point_err
                                                 - open new log file if > max size
                                                 - add GAIM_LOGFILE_MAX
         8.2       mar02-2004    K. Dolinh       - add loop to retry setup_socket
         8.4       mar11-2004    K. Dolinh       - add logfile_msgid
         8.5       mar22-2004
         8.6       apr02-2004
         8.7       apr15-2004
         8.8       may04-2004                    - print field oldstrm to ifm.sta
                   may06-2004                    - put sleep into main loop
                                                 - .connected=0 means not-connected
         8.8.1     may10-2004                    - print gtacs info to ifm.sta
         8.8.1B    may18-2004                    - comment out sleep in main loop
                                                 - change to IFM_process_gas_global
         8.8.1C    may24-2004                    - change to IFM_ping_gas
         8.8.1D    jun16-2004                    - change to IFM_socket_input
         8.8.1E    jun17-2004                    - change to IFM_handle_prime_change
         8.B       sep17-2004                    - GTACS build 8B
         9         jan06-2005                    Build 9.1
         9.2       jan31-2005                    Build 9.2
                   feb11-2005                    Send merge msg to GAS if playback error
                   feb21-2005                    Add env GAIM_DEBUG_GAS
                                                 Fixed archiving of nonprime stream up/down
                                
                                                 Following files were changed
                                                   - IFM_vars.h
                                                   - IFM_struct.h 
                                                   - ....
                                                   - IFM_ping_gas.c
                                                                                                 
         9A        feb23-2005                    Build 9A       
         9A1       mar04-2005                    Following files were changed
                                                   - IFM_ping_gas.c
         9A2       mar07-2005                    - IFM_update_status.c  
                   mar08-2005                    Add env GAIM_DEBUG_CRC and GAIM_DEBUG_TCPIP
         9AP       mar16-2005                    Build 9A Prime                                                                           
         9AP       apr05-2005                    Build 9A Prime.2        
         9AP3      apr19-2005                    - remove start up log_debug,log_err
         9B        may01-2005                    Build 9B
         9C        Jul18-2005                    Add calls to create_eventfile
         10A       Aug26-2005                    Build 10A
         10A.1     Sep02-2005                    - Add env variable gaim_log_daily
         10A.2     Sep09-2005                    - Add env variables gaim_update_gv, gaim_debug                          
                   Sep10-2005                    - Add IFM_set_point_sarc_status 
         10A.3     Dec14-2005                    - comment out another call to set_point
         10B       Jan12-2006                    - recompile for Build 10B       
         10B       Feb16-2006                    - change to IFM_socket_input    
         10C       Jul10-2006                    - recompile for Build 10C
         11        Aug24-2006                    - Build 11 has changes to
                                                     IFM_main 
                                                     IFM_process_gas_event
                                                     IFM_write_status
                                                     IFM_update_statistics (new)
                                                     log_daily_statistics (new)
                                                     gaim_shm_attach/create (new)
                   Mar14-2007                    - Added IFM_set_point_text
                   Mar28-2007                    - remove SEND_EVENT msg
                   Apr12-2007                    - change textvalue of globals in IFM_update_statistics
             12    Jun22-2007                    Build 12 
             12A   Sep01-2007                    Build 12A Changes
                                                   - in UBG, remove STOP flags 
                                                   - in IFM, fix IFM crash
                                                     IFM_get_globals
                                                   - in PLAYB, make event msg optional

                   Nov05-2010    D. Niklewski    - fixed memory leak in IFM_set_point_text by calling
                                                   "cancel_point" before returning.
                                                   (GIR SSGS-1227, "GAIM Memory Leak causes GTACS problems")

                   Jan03-2011    D. Niklewski    - Modified IFM_set_point_text so it doesn't need to call
                                                   "look_up_point" every time it wants to write a global value.
                                                   Instead, IFM_connect_to_epoch calls "look_up_point" once whenever
                                                   a connection is established.  This greatly reduces the
                                                   overhead (on Dev for some reason, around 19Nov2010,
                                                   these calls to "look_up_point" started taking a long time, so that 
                                                   IFM_update_statistics was requiring 16 seconds or more to
                                                   complete, creating havoc with socket I/O).  However, it was found
                                                   that calling "set_point" periodically will NOT keep a GTACS from
                                                   closing the connection, but calling "poll_point" periodically WILL
                                                   keep the connection alive.  Also, it was found that calling
                                                   "set_point" with a "destroyed" connection (presumably one that
                                                   GTACS has closed on the other end) will cause a core dump.  Both
                                                   of these factors have been taken into account.
                                                   Note that calling "look_up_point" on a non-local ground stream
                                                   (e.g., c3_ge) can take a long time relative to calling "set_point"
                                                   (in one test that was working well, it took about 80 times longer
                                                   [0.02 second for "look_up_point" versus 0.00005 second for "set_point"];
                                                   while in another case, it was taking 16-20 seconds for the call to
                                                   IFM_update_statistics to complete because of the VERY slow calls
                                                   to "look_up_point").

********************************************************************************
                                                                                                                                                                        
        DEV note: grep for TEST or TESTONLY to see tmp test code
                                                                                                                                                                                                                 
********************************************************************************
         Invocation:

             N-Q_Interface_Manager -d -D

         Parameters:

             d or D     : enable debug messages

********************************************************************************
         Functional Description:

         The N-Q_Interface_Manager Process is the interface manager between
         the GTACS systems and the GOES Archive Systems.

*******************************************************************************/

/**
    initialize the debug file name
    CALL IFM_init
    initialize the signal handler routines
    CALL IFM_read_config
    CALL IFM_setup_sockets
    CALL IFM_spawn_task
    write log message
    CALL IFM_spawn_task
    write log message
    DOFOR ever
        IFM_connect_to_epoch
        IFM_get_globals
        IFM_socket_input
        IFM_ping_gas
    ENDDO
**/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>             /* Variable-length argument lists. */
#include "strmerr.h"
#include "strmutil.p"

#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_vars.h"

extern void IFM_connect_to_epoch (void);
extern void IFM_get_globals (void);
extern void IFM_init (void);
extern void IFM_onintr (int);
extern void IFM_onintr_timer (int);
extern void IFM_ping_gas (void);
extern void IFM_read_config (void);
extern void IFM_send_to_gas_test (void);
extern int  IFM_setup_sockets (void);
extern void IFM_close_sockets (void);
extern void IFM_socket_input (void);
extern void IFM_spawn_task (char*);

/********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
int set_point_usr (client_point *pointpid);
/******************** End Modification *********************/

#define SW_VERSION "Build 12A Sep-01-2007"
#define SW_ID      "IFM"

#include "../include/util_log.h"

void IFM_write_status (void);
static  int  statusfile_open_err = 0;
static char statusfile_name[1024];
FILE *statusfile_ptr = NULL;
time_t process_starttime;

void IFM_update_statistics (void);
static  int  statfile_open_err = 0;
static char statfile_name[1024];
FILE *statfile_ptr = NULL;


char    *debug_arg;
int             debug_gas = 0;
int             debug_crc = 0;
int             debug_tcpip = 0;

int             check_logfile_count = 200;
int             check_logfile_max = 200; /* 30 seconds */
int             logfile_daily = 0;
int             update_gv = 0;

GaimStat *gaimstat,*s;
int             gaim_status_interval = 0;
time_t  gaim_status_time;
time_t  current_time;

/***
 *** main routine 
 ***/
int main (int argc, char *argv[])

{
int c;
int i, j, status;
char msgtxt [MAXMSGTEXT];
char syscall [MAXMSGTEXT];
char text [MAXBUFFLEN];

/* save process start time */
time(&process_starttime);

/* get env variables */

gaim_home = (char *) getenv("GAIM_HOME");
if (!gaim_home) {
        /* exit if env var not defined */
        log_err("IFM terminating because env variable GAIM_HOME not defined");
        log_err("");
        exit(1);
}

if (getenv("GAIM_DEBUG"))       debug = 1;
if (getenv("GAIM_DEBUG_GAS"))   debug_gas = 1;
if (getenv("GAIM_DEBUG_CRC"))   debug_crc = 1;
if (getenv("GAIM_DEBUG_TCPIP")) debug_tcpip = 1;
if (getenv("GAIM_LOGFILE_DAILY")) logfile_daily = 1;
if (getenv("GAIM_UPDATE_GV"))     update_gv = 1;

/* get status update interval in seconds or default to 60 secs */
if (getenv("GAIM_STATUS_INTERVAL"))gaim_status_interval = atoi(getenv("GAIM_STATUS_INTERVAL"));
if (gaim_status_interval <= 0) gaim_status_interval = 60;

/* parse arguments to process */
debug_arg = " ";
while ((c = getopt (argc, argv, "dD")) != -1) {
    switch (c)
      {
      case 'd':
        debug = 1;
                debug_arg = "-D";
        break;
      case 'D':
        debug = 1;
                debug_arg = "-D";
        break;
      default:
        log_err("***invalid arg = %c", optopt);
      }
}

/* init log message ID */
sprintf(logfile_msgid,"%s",SW_ID);
/* create event log file */
create_eventfile();
sleep(5);

/* write generic startup messages */
log_event("  ");
log_event("GAIM Interface Manager (%s) started ...",SW_VERSION);

log_event("logfile_name     = %s",logfile_name);
log_event("logfile daily    = %d",logfile_daily);
log_event("update_gv        = %d",update_gv);
log_event("gaim_status_interval = %d",gaim_status_interval);

log_event("debug       = %d",debug);
log_event("debug_gas   = %d",debug_gas);
log_event("debug_crc   = %d",debug_crc);
log_event("debug_tcpip = %d",debug_tcpip);

/* create gaim shared memory */
gaimstat = (GaimStat *)gaim_shm_create();

/* Open status file for write-truncate */
sprintf(statusfile_name,"%s/output/ifm.sta",gaim_home);
statusfile_ptr = fopen(statusfile_name,"w");
if (statusfile_ptr==NULL)
    log_event_err("unable to open status file = %s",statusfile_name);
else
        log_event("statusfile_name = %s",statusfile_name);

/* Open stat (new for 10D) file for write-truncate */
sprintf(statfile_name,"%s/output/gaim.sta",gaim_home);
statfile_ptr = fopen(statfile_name,"w");
if (statfile_ptr==NULL)
    log_event_err("unable to open stat file = %s",statfile_name);
else
        log_event("statfile_name = %s",statfile_name);

log_event("  ");

/*  Initialize GAIM variables  */
IFM_init ();

/*  Initialize the signal handler routines  */
signal (SIGINT, IFM_onintr);
signal (SIGTERM, IFM_onintr);
signal (SIGALRM, IFM_onintr_timer);

/*  Read the GAIM and EPOCH configuration files  */
log_event("Reading configuration files");
IFM_read_config ();

/*  Setup all of the GOES Archive and GAIM listening sockets  */
log_event("Setting up listening sockets");
for (i=1; i<=11; i++) {

        /* exit loop if socket setup successful */
        if (IFM_setup_sockets() == 0) break;
        log_err("error initializing sockets. will try %d more times",10-i);
        if (i >= 10) {
                /* term process if socket setup unsuccessful after 10 retries */
                IFM_close_sockets();
                log_err("cannot initialize listening sockets");
                log_err("IFM terminating");
                log_event_err("cannot initialize listening sockets");
                log_event_err("IFM terminating");
                exit(1);
        }
        /* close sockets */
        IFM_close_sockets();
        /* wait 60 seconds and try again */
        sleep(60);
}

/* output message to console */
log_err("%s InterFace Manager running ...",gaimname);

/*  Start Check_GTACS process  */
if (debug) sprintf(text, "./Check_GTACS -d &");
else sprintf(text, "./Check_GTACS &");
IFM_spawn_task (text);
log_event("start process %s",text);

/*  Start Update_Buffer_Sender process  */
if (debug) sprintf(text, "./Update_Buffer_Sender -d &");
else sprintf(text, "./Update_Buffer_Sender &");

IFM_spawn_task (text);
log_event("start process %s",text);

/********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
/* Initialize the pointers to the client_point structures */

for (i = 0; i < MAXGTACSHOSTS; i++) {
    gs_connect_id[i] = NULL;
    gaim_mon_rate_pid[i] = NULL;
    sc13_prime_pid[i] = NULL;
    sc14_prime_pid[i] = NULL;
    sc15_prime_pid[i] = NULL;
    sc16_prime_pid[i] = NULL;
    sc17_prime_pid[i] = NULL;
    sc18_prime_pid[i] = NULL;
    sc19_prime_pid[i] = NULL;
    sc20_prime_pid[i] = NULL;
    gaim_status_pid[i] = NULL;
    sarc13_status_pid[i] = NULL;
    sarc14_status_pid[i] = NULL;
    sarc15_status_pid[i] = NULL;
    sarc16_status_pid[i] = NULL;
    sarc17_status_pid[i] = NULL;
    sarc18_status_pid[i] = NULL;
    sarc19_status_pid[i] = NULL;
    sarc20_status_pid[i] = NULL;
    archive_rt_13_pid[i] = NULL;
    archive_rt_14_pid[i] = NULL;
    archive_rt_15_pid[i] = NULL;
    archive_rt_16_pid[i] = NULL;
    archive_rt_17_pid[i] = NULL;
    archive_rt_18_pid[i] = NULL;
    archive_rt_19_pid[i] = NULL;
    archive_rt_20_pid[i] = NULL;
    archive_am_13_pid[i] = NULL;
    archive_am_14_pid[i] = NULL;
    archive_am_15_pid[i] = NULL;
    archive_am_16_pid[i] = NULL;
    archive_am_17_pid[i] = NULL;
    archive_am_18_pid[i] = NULL;
    archive_am_19_pid[i] = NULL;
    archive_am_20_pid[i] = NULL;
    archive_mm_13_pid[i] = NULL;
    archive_mm_14_pid[i] = NULL;
    archive_mm_15_pid[i] = NULL;
    archive_mm_16_pid[i] = NULL;
    archive_mm_17_pid[i] = NULL;
    archive_mm_18_pid[i] = NULL;
    archive_mm_19_pid[i] = NULL;
    archive_mm_20_pid[i] = NULL;
    gaim_state_pid[i] = NULL;
    sc13_prime_stats_pid[i] = NULL;
    sc14_prime_stats_pid[i] = NULL;
    sc15_prime_stats_pid[i] = NULL;
    sc16_prime_stats_pid[i] = NULL;
    sc17_prime_stats_pid[i] = NULL;
    sc18_prime_stats_pid[i] = NULL;
    sc19_prime_stats_pid[i] = NULL;
    sc20_prime_stats_pid[i] = NULL;
    sc13_nprime_stats_pid[i] = NULL;
    sc14_nprime_stats_pid[i] = NULL;
    sc15_nprime_stats_pid[i] = NULL;
    sc16_nprime_stats_pid[i] = NULL;
    sc17_nprime_stats_pid[i] = NULL;
    sc18_nprime_stats_pid[i] = NULL;
    sc19_nprime_stats_pid[i] = NULL;
    sc20_nprime_stats_pid[i] = NULL;
}

/******************** End Modification *********************/

/* save current time as last status update time */
gaim_status_time = time(0);

/* main processing loop */
for (;;) {

        /*  Try to connect to all of the EPOCH systems  */
        IFM_connect_to_epoch ();

        /*  Get the globals from EPOCH  */
        IFM_get_globals ();

        /*  Check for input on all of the sockets  */
        IFM_socket_input ();

        /*  Ping all operational GOES Archive Servers  */
        IFM_ping_gas ();

        /* this sleep causes ifm to lose cgt msg 
        sleep(1); */
        
        /* open new event log file if new day */ 
        if (logfile_daily && check_logfile_count-- <= 0) {
                create_eventfile();
                check_logfile_count = check_logfile_max;
        }
        
        /* update gaim status and statistics every N seconds */
        current_time = time(0);
        if ( current_time-gaim_status_time > gaim_status_interval) {
                IFM_update_statistics();
                gaim_status_time = current_time;
        }
        
} /* enddo forever */

/* close down and exit */

/***The following line can never be reached:
IFM_onintr();
*/
}

/***
 *** write file ifm.sta  
 ***/
void IFM_write_status (void)
{
int i,j;
int status;
time_t current_time;
struct tm current_time_str;

char    *text_active, *text_prime;
char    *stat_req, *stat_connect;


/* return if status file not open */
if (!statusfile_ptr) return;

/* position to beginning of file */
fseek(statusfile_ptr,0,SEEK_SET);

/* write program start time */
current_time_str=*localtime(&process_starttime);
fprintf(statusfile_ptr,"\nIFM  Start Time:  GMT %4.4i-%3.3i-%2.2i:%2.2i:%2.2i\n",
                  current_time_str.tm_year+1900,current_time_str.tm_yday+1,
                  current_time_str.tm_hour,current_time_str.tm_min,
                  current_time_str.tm_sec);


/* write current time */
status = time(&current_time);
current_time_str=*localtime(&current_time);
fprintf(statusfile_ptr,"IFM Status Time:  GMT %4.4i-%3.3i-%2.2i:%2.2i:%2.2i\n\n",
                  current_time_str.tm_year+1900,current_time_str.tm_yday+1,
                  current_time_str.tm_hour,current_time_str.tm_min,
                  current_time_str.tm_sec);


/* write prime status */
fprintf(statusfile_ptr,"                     SPACECRAFT PRIME STATUS\n\n");
fprintf(statusfile_ptr,"SCID     StreamName         HostName           ArchiveReq   Connected   oldstrm  \n");
fprintf(statusfile_ptr,"----     ----------------   ----------------   ----------   ---------   -------- \n");

for (i=0; i<=7; i++) {

        if (prime[i].requested == 0) {
                stat_req = "NO ";
        } else {
                stat_req = "YES";
        }

        if (prime[i].connected == 0) {
                stat_connect = "NO ";
        } else if (prime[i].connected == 1) {
                stat_connect = "YES";
        } else {
                stat_connect = "NA ";
        }

        fprintf(statusfile_ptr,"%2s     %16.16s   %16.16s      %3s          %3s        %10.10s\n",
                prime[i].sc,prime[i].strm,prime[i].host,stat_req,stat_connect,prime[i].oldstrm);

} /* end dofor */

/* write status of SDR processes */

fprintf(statusfile_ptr,"\n\n                 STATUS OF SDR PROCESSES\n\n");
fprintf(statusfile_ptr,"Process     Active   Prime   GasIP              GTACS              SCID   StreamName      \n");
fprintf(statusfile_ptr,"-------     ------   -----   ----------------   ----------------   ----   ----------------\n");

for (i=1; i<=16; i++) {

        text_active = "   ";
        text_prime  = "   ";
        if (sdr[i].active == 1) {
                text_active = "YES";
                if (sdr[i].prime  == 1) text_prime = "YES";
                else text_prime = "NO ";
        }

    fprintf(statusfile_ptr,"sdr# %2.2d     %3.3s      %3.3s     %16.16s   %16.16s   %2s     %16.16s\n",
                    i,text_active,text_prime,sdr[i].gasip,sdr[i].gtacs,sdr[i].scid,sdr[i].stream);

} /* end dofor */


/* write gtacs status */
fprintf(statusfile_ptr,"\n\n                     GTACS SERVER STATUS\n\n");
fprintf(statusfile_ptr,"Host     Avail   Oper    poll_err   GroundStream   RealtimeStream \n");
fprintf(statusfile_ptr,"----     -----   -----   --------   ------------   -------------- \n");

for (i=0; i<num_gtacs_hosts; i++) {
           
        fprintf(statusfile_ptr,"%8.8s   %1.1d       %1.1d       %3.3d      %8.8s       %8.8s \n",
           &goplist[i].gtacs_host[0],goplist[i].available,goplist[i].operational,goplist[i].pollpoint_err,
               &goplist[i].grndstrm[0],&goplist[i].rtstrm[0][0]);
                   
                if (num_rt_streams[i] > 1) {
                        for (j=1; j<num_rt_streams[i]; j++) { 
                                        fprintf(statusfile_ptr,"                                                   %8.8s\n",
                                        &goplist[i].rtstrm[j][0]);
                        }
                }
                
                        
} /* end dofor */


/* write time of latest gas signon msg */
fprintf(statusfile_ptr,"\n\n                     GAS STATUS\n\n");
fprintf(statusfile_ptr,"GasHostName    OpStatus   ArchiveName        GMT of Last Signon Msg \n");
fprintf(statusfile_ptr,"------------   --------   ----------------   ---------------------- \n");

for (i=0; i< num_gas_hosts; i++) {

        current_time_str=*localtime(&gasoplist[i].signon_time);
        fprintf(statusfile_ptr,"%12.12s      %1.1i       %16.16s   %4.4i-%3.3i-%2.2i:%2.2i:%2.2i \n",
                  gasoplist[i].gas_host, gasoplist[i].operational, gasoplist[i].archive_name,
                  current_time_str.tm_year+1900,current_time_str.tm_yday+1,
                  current_time_str.tm_hour,current_time_str.tm_min,
                  current_time_str.tm_sec);

} /* end dofor */

/* write blank line and force file buffer output */
fprintf(statusfile_ptr," \n");
fflush(statusfile_ptr);

} /* end IFM_write_status */



/***
 *** print merge request 
 ***/
void IFM_print_merge_data (
        char *text,
        int index, 
        GasMergeMsg m)
{

log_event("%s  index=%d  scid=(%s)  node=(%s)  process=(%s)  gtacs=(%s)",
        text,index,
        m.scid,m.msg_hdr.node_name,
        m.msg_hdr.process_name,m.gtacs_name);

} /* end IFM_print_merge_data */


/***
 *** Set gtacs global variable 
 ***/
int IFM_set_point_sarc_status (
        char    *name,
        int             value)
{
int  status;    

/* log_event("IFM_set_point_sarc_status: update_gv=%d  gas_index=%d  gas_name=%s  gtacs_index=%d",
        update_gv,i,gasoplist[i].archive_name,j);
if (update_gv) return status; */


/* call lookup_point */
/* call set_point */

status = STRM_SUCCESS;
return status;
                        
} /* end IFM_set_point_sarc_status */


/********* OLD IFM_set_point_text: *********/
/***
 *** Set text value of gtacs global variable 
 ***/
/*
int IFM_set_point_text (
    char    *stream,
    char    *point,
    char    *text)
{
int  status;    
client_point  *pointpid;
char          pointname[MNEMONIC_SIZE+MNEMONIC_SIZE];

sprintf(pointname,"%s:%s",stream,point);

pointpid = NULL;
status = look_up_point (pointname,client_timeout,&pointpid);
if (status != NULL) return status;

strncpy ((char *)pointpid->value,text,100);
pointpid->status_bits = 0;
status = set_point (pointpid); 
    
return status;
            
}

*/
/************************/


/********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
/* (GIR SSGS-1227: "GAIM Memory Leak causes GTACS problems"):
   "look_up_point" allocates memory that needs to be released
   by calling "cancel_point".  This was the cause of a fairly
   large memory leak.  Pointpid's are now reused and only
   cancelled when necessary.  */

/***
 *** Set text value of gtacs global variable 
 ***/

int IFM_set_point_text (
    client_point    *pointpid,
    char            *text)
{

int  status;    

strncpy ((char *)pointpid->value, text, STRING_VAR_SIZE);
pointpid->status_bits = 0;
status = set_point_usr (pointpid); 
    
return status;
            
}

/******************** End Modification *********************/


/***
 *** Send event message to all GTACS ground streams
 ***/
void IFM_send_event (
        int  type,
        char *text )
{
int             i;
int             eventnum;
char    buffer[1024];
char    prefix[16];
int             log;

/* default: don't write to gaim log file */
log = 0;

switch(type) {
        case EVENT_GAS_NORMAL:
                sprintf(prefix,"%s*GAS",gaimname);
                eventnum = GAIM_EVTNUM_NORMAL;
                break;
        case EVENT_GAS_ERR:
                sprintf(prefix,"%s*GAS",gaimname);
                eventnum = GAIM_EVTNUM_ERR;
                break;          
        case EVENT_WARN:
                sprintf(prefix,"%s*WRN",gaimname);
                eventnum = GAIM_EVTNUM_WARN;
                break;          
        case EVENT_ERR:
                sprintf(prefix,"%s*ERR",gaimname);
                eventnum = GAIM_EVTNUM_ERR;
                break;          
        default:
                sprintf(prefix,"%s*MSG",gaimname);
                eventnum = GAIM_EVTNUM_NORMAL;
                break;          
}

/* output to gaim log 
log_event("***SEND_EVENT(%s)",text);*/

/* output events to connected GTACS ground streams */           
for (i=0; i< num_gtacs_hosts; i++) {
        if (goplist[i].operational == 1) {
                sprintf(buffer,"@%s [%s] %s",goplist[i].grndstrm,prefix,text);
                send_event (eventnum,buffer,client_timeout);
                /* log_event("***SEND_EVENT(%d,%s)",eventnum,buffer); */
        }
}

} /* end IFM_send_event */


/*** 
 *** Write GAIM statistics to file gaim.sta
 *** Update global variables in ground stream
 ***/
void IFM_update_statistics (void)
{
static   int firsttime = 1;
int i,j;
int status;
time_t current_time;
struct tm current_time_str;

GaimStat  *s, *s2;
int                     buffers_automerge;

char line1[256], line2[256], line3[256], line4[256], statline[512];
char gaim_state[8],gaim_state_line[40];
char prime_state[8+1];
char pointname[MNEMONIC_SIZE];
client_point *pid;

/********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
int good_connection[MAXGTACSHOSTS];
/******************** End Modification *********************/

/* send events to GTACS at startup */
if (firsttime) { 
        sprintf(line1,"GAIM %s started",SW_VERSION);
        IFM_send_event(EVENT_NORMAL,line1); 
        IFM_send_event(EVENT_NORMAL,"GAIM test routine message");
        IFM_send_event(EVENT_WARN,  "GAIM test warning message");
        IFM_send_event(EVENT_ERR,   "GAIM test error message");
        IFM_send_event(EVENT_GAS_NORMAL, "GAS test routine message");
        IFM_send_event(EVENT_GAS_ERR,    "GAS test error message"); 
        firsttime = 0;
}

/* return if shared memory not accessible */
if (!gaimstat) return;
/* TESTONLY 
log_daily_statistics(2007,0);
*/

/* return if status file not open */
if (!statfile_ptr) return;

/* position to beginning of file */
fseek(statfile_ptr,0,SEEK_SET);

/* write software version */
fprintf(statfile_ptr,"\n S/W Version:  GAIM %s\n",SW_VERSION);

/* write program start time */
current_time_str=*localtime(&process_starttime);
fprintf(statfile_ptr,"  Start Time:  GMT %4.4i-%3.3i-%2.2i:%2.2i:%2.2i\n",
                  current_time_str.tm_year+1900,current_time_str.tm_yday+1,
                  current_time_str.tm_hour,current_time_str.tm_min,
                  current_time_str.tm_sec);

/* write current time */
status = time(&current_time);
current_time_str=*localtime(&current_time);
fprintf(statfile_ptr," Status Time:  GMT %4.4i-%3.3i-%2.2i:%2.2i:%2.2i\n\n",
                  current_time_str.tm_year+1900,current_time_str.tm_yday+1,
                  current_time_str.tm_hour,current_time_str.tm_min,
                  current_time_str.tm_sec);

/* determine gaim state and update global var GAIMn_STATE */
sprintf(gaim_state,"OK");
sprintf(gaim_state_line,"GMT %4.4i-%3.3i-%2.2i:%2.2i:%2.2i  %6s %6s",
          current_time_str.tm_year+1900,current_time_str.tm_yday+1,
          current_time_str.tm_hour,current_time_str.tm_min,
          current_time_str.tm_sec,gaimname,gaim_state);

/* update global var GAIMx_STATE in all ground streams */       
for (i=0; i< num_gtacs_hosts; i++) {

   /********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
   good_connection[i] = 0;
   /******************** End Modification *********************/

   if (goplist[i].operational == 1) {
                   
           sprintf(pointname,"%s_STATE",gaimname);
/*
           IFM_set_point_text(goplist[i].grndstrm,pointname,gaim_state_line);
*/
           /********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
           /* Modified IFM_set_point_text to prevent "look_up_point" from
              being called every time (look_up_point allocates memory that
              needs to be freed - the cause of the memory leak in
              GIR SSGS-1227 - and it's very slow, especially to non-local
              GTACS's. */

           /* Try to poll the point first to make sure the connection is up.
              If poll_point fails, call disconnect_stream and set the operational
              flag to 0 so IFM_connect_to_epoch will reconnect. Apparently,
              poll_point must be called periodically to keep the GTACS connection
              from timing out, and IFM_get_globals only calls poll_point on the
              *FIRST* GTACS connection, leaving all other ones to (evidently)
              time out.  It also appears that calling "set_point" periodically
              will NOT prevent the GTACS connection from timing out. 
              
              The way IFM_set_point_text USED to work was to call "look_up_point"
              every time before calling "set_point".  This works, because calling
              "look_up_point", like "poll_point", apparently prevents the GTACS
              from closing the connection (the characteristic of a timed-out
              connection appears to be the "destroyed" flag being set to 1; however,
              set_point does NOT check this flag before calling "clnt_call", and
              the program crashes and core-dumps).  The calls to "look_up_point",
              though, can take a long time on a remote connection (up to 400 times
              longer than a call to "set_point" or "poll_point"), and this causes
              problems on a slow network - GAIM can get "stuck" for a long time in
              IFM_update_statistics.  Calling "look_up_point" without calling
              "cancel_point" later on will also cause a rather large memory leak.
           */

           status = poll_point(gaim_state_pid[i]);
           if (status != STRM_SUCCESS) {
               log_event("(WARNING) poll_point failed for ground global %s on stream %s.  Status = %d.  Resetting connection.", pointname, goplist[i].grndstrm, status);
               if (gs_connect_id[i] != NULL)
                   disconnect_stream (gs_connect_id[i]);
               goplist[i].operational = 0;
               continue;   /* Move on to next GTACS connection */
           }

           status = IFM_set_point_text(gaim_state_pid[i], gaim_state_line);
           if (status != STRM_SUCCESS) {
               log_event("(WARNING) IFM_set_point_text failed for ground global %s on stream %s.  Status = %d.  Resetting connection.", pointname, goplist[i].grndstrm, status);
               if (gs_connect_id[i] != NULL)
                   disconnect_stream (gs_connect_id[i]);
               goplist[i].operational = 0;
               continue;   /* Move on to next GTACS connection */
           }

           good_connection[i] = 1;  /* Passed the poll_point test and successfully set a value */

           /******************** End Modification *********************/

   } /* endif goplist */
} /* enddo num_gtacs_hosts */   

/* update statistics if sc is being archived */
/* do for sc 13 to 17 */
for (j=0; j<GAIM_SHM_ITEMS; j++) { 

        /* get address of shared mem for sc */
        s = (GaimStat *) ((unsigned long) gaimstat + (unsigned long) sizeof(GaimStat)*j);
        
        if (s->scid >= 13 && s->scid <= 20) {
                /* write stat lines to file */
                sprintf(line1,"SCID=%2.2d  Prime=%1.1d  Stream=%5.5s\n",
                        s->scid,s->prime,s->stream);
                sprintf(line2,"        Frames=%-6d     Normal=%-6d    Dwell=%-6d   WDI=%-6d  WDS=%-6d  SXI=%-6d  Pseudo=%-6d\n",
                        s->frames_all,s->frames_normal,s->frames_dwell,s->frames_wbs,s->frames_wbi,s->frames_sxi,s->files_pseudo);
                sprintf(line3,"       Buffers=%-6d     Normal=%-6d    Dwell=%-6d   WDI=%-6d  WDS=%-6d  SXI=%-6d  Pseudo=%-6d\n",
                        s->buffers_all,s->buffers_normal,s->buffers_dwell,s->buffers_wbs,s->buffers_wbi,s->buffers_sxi,s->buffers_pseudo);
                sprintf(line4,"        AMerge=%-6d     MMerge=%-6d  BadQual=%-6d  Sent=%-6d\n\n",
                        s->buffers_automerge,s->buffers_manualmerge,s->buffers_badquality,s->buffers_sent);
                sprintf(statline,"%s%s%s%s",line1,line2,line3,line4);   
                fprintf(statfile_ptr,statline);
                        
                /* write stat to global variable */
                
                /*** if (s->prime == 1) sprintf(prime_state,"PRIME   ");
                else sprintf(prime_state,"NONPRIME");           
                sprintf(statline,"SCID=%2.2d  %8s  Stream=%5.5s  FrameIn=%-6d  BufferOut=%-6d  PseudoOut=%-6d  badQ=%-6d",
                        s->scid,prime_state,s->stream,s->frames_all,s->buffers_sent,s->buffers_pseudo,s->buffers_badquality);***/

                if (s->prime == 1) {
                        sprintf(pointname,"%s_STATS_SC%d_PRIME",gaimname,s->scid);
                        /* calc address of shared mem for same sc but nonprime 
                        s2 = (GaimStat *) ((unsigned long) s + (unsigned long) sizeof(GaimStat)*8);
                        buffers_automerge = s->buffers_automerge + s2->buffers_automerge;*/

                        if (!strncmp(s->stream,"NONE",4)) {
                                sprintf(statline,"                                 ");
                        } else {
                                sprintf(statline,"%5.5s  BUF=%-6d (PCM=%-6d PSU=%-6d DWL=%-6d SXI=%-6d WBS=%-6d WBI=%-6d) BAD=%-6d",
                                                s->stream,s->buffers_all,s->buffers_normal,s->buffers_pseudo,
                                                s->buffers_dwell,s->buffers_sxi,s->buffers_wbs,s->buffers_wbi,s->buffers_badquality);
                        }

                } else {
                        sprintf(pointname,"%s_STATS_SC%d_NPRIME",gaimname,s->scid);
                        if (!strncmp(s->stream,"NONE",4)) {
                                sprintf(statline,"                                 ");
                        } else {
                                sprintf(statline,"%5.5s  BUF=%-6d (PCM=%-6d PSU=%-6d DWL=%-6d SXI=%-6d WBS=%-6d WBI=%-6d) BAD=%-6d",
                                                s->stream,s->buffers_all,s->buffers_normal,s->buffers_pseudo,
                                                s->buffers_dwell,s->buffers_sxi,s->buffers_wbs,s->buffers_wbi,s->buffers_badquality);
                        }
                        
                }
        

                /* update corresp. global var in all ground streams */  
                for (i=0; i< num_gtacs_hosts; i++) {

/*
                        if (goplist[i].operational == 1) {
*/
                        /********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
                        /* Check for good connection also */
                        if (goplist[i].operational == 1 && good_connection[i] == 1) {
                        /******************** End Modification *********************/

                                /* update global vars for gaim statistics */
                                /*
                                IFM_set_point_text(goplist[i].grndstrm,pointname,statline);                     
                                */
                                /********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
                                /* Modified IFM_set_point_text to prevent "look_up_point" from
                                   being called every time (look_up_point allocates memory that
                                   needs to be freed - the cause of the memory leak in
                                   GIR SSGS-1227 - and it's very slow, especially to non-local
                                   GTACS's. */

                                pid = NULL;
                                if (s->scid == 13)
                                    if (s->prime == 1)
                                        pid = sc13_prime_stats_pid[i];
                                    else
                                        pid = sc13_nprime_stats_pid[i];
                                else if (s->scid == 14)
                                    if (s->prime == 1)
                                        pid = sc14_prime_stats_pid[i];
                                    else
                                        pid = sc14_nprime_stats_pid[i];
                                else if (s->scid == 15)
                                    if (s->prime == 1)
                                        pid = sc15_prime_stats_pid[i];
                                    else
                                        pid = sc15_nprime_stats_pid[i];
                                else if (s->scid == 16)
                                    if (s->prime == 1)
                                        pid = sc16_prime_stats_pid[i];
                                    else
                                        pid = sc16_nprime_stats_pid[i];
                                else if (s->scid == 17)
                                    if (s->prime == 1)
                                        pid = sc17_prime_stats_pid[i];
                                    else
                                        pid = sc17_nprime_stats_pid[i];
                                else if (s->scid == 18)
                                    if (s->prime == 1)
                                        pid = sc18_prime_stats_pid[i];
                                    else
                                        pid = sc18_nprime_stats_pid[i];
                                else if (s->scid == 19)
                                    if (s->prime == 1)
                                        pid = sc19_prime_stats_pid[i];
                                    else
                                        pid = sc19_nprime_stats_pid[i];
                                else
                                    if (s->prime == 1)
                                        pid = sc20_prime_stats_pid[i];
                                    else
                                        pid = sc20_nprime_stats_pid[i];

                                if (pid)
                                    status = IFM_set_point_text(pid, statline);

                                if (!pid || status != STRM_SUCCESS) {
                                    log_event("(WARNING) IFM_set_point_text failed for ground global %s on stream %s.  Status = %d.  Resetting connection.", pointname, goplist[i].grndstrm, status);
                                    if (gs_connect_id[i] != 0)
                                        disconnect_stream (gs_connect_id[i]);
                                    goplist[i].operational = 0;
                                    good_connection[i] = 0;
                                    continue;   /* Move on to next GTACS connection */
                                 }

                                /******************** End Modification *********************/

                        } 
                        
                } /* enddo num_gtacs_hosts */   
        }
}

/* write blank line and force file buffer output */
fprintf(statfile_ptr," \n");
fflush(statfile_ptr);

} /* end IFM_update_statistics */


#include "../include/util_log.c"

/********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
/* Added a check of the "destroyed" flag.  If set_point is
   called using a connection with the "destroyed" flag set,
   this program will core-dump (NOTE: set_point does NOT
   check the flag, unlike poll_point). */

int set_point_usr (client_point* point_id)
{
    if (!point_id)
        return(STRM_INVALID_ARG);

    if (!point_id->client_connection)
        return(STRM_NO_CONNECTION);

    if (point_id->client_connection->destroyed)
        return(STRM_CONNECTION_SHUTDOWN);

    return (set_point(point_id));
}

/******************** End Modification *********************/
