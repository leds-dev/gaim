/*  
@(#)  File Name: IFM_build_gas_merge.c  Release 1.4  Date: 07/03/25, 08:32:40
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_build_gas_merge.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
		 11			 mar13-2007  K. Dolinh		 fix compile warning

********************************************************************************
         Invocation:
        
             IFM_build_gas_merge (type, index, merge, buffer)
             
         where
         
             <type> - I
                 is an integer indicating the status of this message
                 (0=GTACS available, 1=no GTACS connection,
                 3=request already active, this request rejected.
             <index> - O
             	 is a pointer to an integer containing the index into the buffer
             <merge> - I
                 is the structure containing the received merge message
             <buffer> - O
                 is a pointer to a character string containing the message
                         
********************************************************************************
         Functional Description:

         This routine builds a merge response message.
	
*******************************************************************************/

/**
    build the merge message header
    CALL IFM_build_gas_msghdr
    build the merge message body
    CALL IFM_swap_bytes
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern char *IFM_swap_bytes(int);

void IFM_build_gas_merge (int type,int* index,GasMergeMsg merge,char* buffer)
{
	short cid, mnum;
	int i;
	long msize;
	char nname [MAXHOSTLEN];
	char proc [MAXPROCLEN];
	char *c;
	struct gas_merge_vector *tmp_merge_data;
	
	/*  Build the message header  */
	
	msize = 1;
	cid = GAS_MERGE_ID;
	mnum = merge_seq_num;
	strcpy (nname, merge.msg_hdr.node_name);
	/* strcpy (nname, (char *)net_host ("localhost")); */
	strcpy (proc, merge.msg_hdr.process_name);
	/* strcpy (proc, IFM_PROC_NAME); */
	IFM_build_gas_msghdr (index, &buffer[0], msize, cid, mnum, nname, proc);
	
	/*  Build the message body  */
	
	short_data = (short)type;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	short_data = merge.gtacs_name_len;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	memcpy ((&buffer[*index]), merge.gtacs_name, merge.gtacs_name_len);
	*index += merge.gtacs_name_len;
	
	short_data = merge.scid_len;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	memcpy ((&buffer[*index]), merge.scid, merge.scid_len);
	*index += merge.scid_len;
	
	short_data = merge.stream_type_len;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	memcpy ((&buffer[*index]), merge.stream_type, merge.stream_type_len);
	*index += merge.stream_type_len;
	
	short_data = merge.start_year;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	short_data = merge.start_day;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	short_data = merge.num_data_sections;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	tmp_merge_data = merge.merge_vector;
	while (tmp_merge_data != NULL)
		{
		long_data = tmp_merge_data->start_time;
	        memcpy (&buffer[*index], (char *)(&long_data), 4);
		*index += 4;
		
		long_data = tmp_merge_data->duration;
	        memcpy (&buffer[*index], (char *)(&long_data), 4);
		*index += 4;
		
		tmp_merge_data = tmp_merge_data->next;
		}
		
	/*  Update the value for the message size in the buffer  */
	
	long_data = *index - 4;
	memcpy (&buffer[0], (char *)(&long_data), 4);
		
	return;
}
