/*  
@(#)  File Name: IFM_onintr_timer.c  Release 1.3  Date: 07/03/25, 08:32:56
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_onintr_timer.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	 8.1       01/2004       K. Dolinh       Add log_event, log_debug

********************************************************************************
         Invocation:
        
             IFM_onintr_timer ()
        
********************************************************************************
         Functional Description:

         This routine handles the interval timer interrupt.
	
*******************************************************************************/

/**
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include <IFM_params.h>
#include "IFM_struct.h"
#include <IFM_varsex.h>

void IFM_onintr_timer (int sig)
{
	char msgtxt [MAXMSGTEXT];

/*** log_debug("IFM entered IFM_onintr_timer"); */

}
