/*  
@(#)  File Name: IFM_vars.h  Release 1.6  Date: 07/03/25, 08:33:18
*/

#ifndef IFM_VARS_H
#define IFM_VARS_H

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         INCLUDE         :       IFM_vars.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
    
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         9.2         jan31-2005  K. Dolinh       Add new array pub_merge_data 
                     apr28-2005                  Add nonprime_id array    
         11          Mar13-2007                  fix compile warning
                                                 Add manual merge req queue
                     Jan03-2011  D. Niklewski    Add client_point pointers for
                                                 statistics globals

********************************************************************************
         Functional Description:
         
         This include file contains variables that are used by the 
         N-Q_Interface_Manager process.
    
*******************************************************************************/

#include <time.h>
#include "strmutil.p"
#include "IFM_struct.h"

char        gaimname [7];
char        *debugfilename;
char        debug_message[MAXMSGTEXT];

fd_set      dt_mask;
fd_set      rd_mask;
int         num_rt_streams [MAXNUMRT];
int         num_pb_streams [MAXNUMPB];
int         num_sim_streams [MAXNUMSIM];
int         gas_server_socket;
int         cgt_client_socket;
int         cgt_listening_socket;
int         gas_client_socket [9]; 
int         gas_listening_socket [9];
int         sdr_client_socket [17]; 
int         sdr_listening_socket [17];
int         pub_client_socket [33]; 
int         pub_listening_socket [33];
int         nfds;

struct timeval      client_timeout;
struct timeval      select_time;
time_t              mon_time1;
time_t              ping_time1;
long                gaim_monitor_rate;
int                 mon_first_time;
int                 gindex;
client_connection   *gs_connect_id[MAXGTACSHOSTS];
client_point        *gaim_mon_rate_pid[MAXGTACSHOSTS];
client_point        *sc13_prime_pid[MAXGTACSHOSTS];
client_point        *sc14_prime_pid[MAXGTACSHOSTS];
client_point        *sc15_prime_pid[MAXGTACSHOSTS];
client_point        *sc16_prime_pid[MAXGTACSHOSTS];
client_point        *sc17_prime_pid[MAXGTACSHOSTS];
client_point        *sc18_prime_pid[MAXGTACSHOSTS];
client_point        *sc19_prime_pid[MAXGTACSHOSTS];
client_point        *sc20_prime_pid[MAXGTACSHOSTS];
client_point        *gaim_status_pid[MAXGTACSHOSTS];
client_point        *sarc13_status_pid[MAXGTACSHOSTS];
client_point        *sarc14_status_pid[MAXGTACSHOSTS];
client_point        *sarc15_status_pid[MAXGTACSHOSTS];
client_point        *sarc16_status_pid[MAXGTACSHOSTS];
client_point        *sarc17_status_pid[MAXGTACSHOSTS];
client_point        *sarc18_status_pid[MAXGTACSHOSTS];
client_point        *sarc19_status_pid[MAXGTACSHOSTS];
client_point        *sarc20_status_pid[MAXGTACSHOSTS];
client_point        *archive_rt_13_pid[MAXGTACSHOSTS];
client_point        *archive_rt_14_pid[MAXGTACSHOSTS];
client_point        *archive_rt_15_pid[MAXGTACSHOSTS];
client_point        *archive_rt_16_pid[MAXGTACSHOSTS];
client_point        *archive_rt_17_pid[MAXGTACSHOSTS];
client_point        *archive_rt_18_pid[MAXGTACSHOSTS];
client_point        *archive_rt_19_pid[MAXGTACSHOSTS];
client_point        *archive_rt_20_pid[MAXGTACSHOSTS];
client_point        *archive_am_13_pid[MAXGTACSHOSTS];
client_point        *archive_am_14_pid[MAXGTACSHOSTS];
client_point        *archive_am_15_pid[MAXGTACSHOSTS];
client_point        *archive_am_16_pid[MAXGTACSHOSTS];
client_point        *archive_am_17_pid[MAXGTACSHOSTS];
client_point        *archive_am_18_pid[MAXGTACSHOSTS];
client_point        *archive_am_19_pid[MAXGTACSHOSTS];
client_point        *archive_am_20_pid[MAXGTACSHOSTS];
client_point        *archive_mm_13_pid[MAXGTACSHOSTS];
client_point        *archive_mm_14_pid[MAXGTACSHOSTS];
client_point        *archive_mm_15_pid[MAXGTACSHOSTS];
client_point        *archive_mm_16_pid[MAXGTACSHOSTS];
client_point        *archive_mm_17_pid[MAXGTACSHOSTS];
client_point        *archive_mm_18_pid[MAXGTACSHOSTS];
client_point        *archive_mm_19_pid[MAXGTACSHOSTS];
client_point        *archive_mm_20_pid[MAXGTACSHOSTS];

/********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
/* Add new client_point pointers for statistics globals    */

client_point        *gaim_state_pid[MAXGTACSHOSTS];

client_point        *sc13_prime_stats_pid[MAXGTACSHOSTS];
client_point        *sc14_prime_stats_pid[MAXGTACSHOSTS];
client_point        *sc15_prime_stats_pid[MAXGTACSHOSTS];
client_point        *sc16_prime_stats_pid[MAXGTACSHOSTS];
client_point        *sc17_prime_stats_pid[MAXGTACSHOSTS];
client_point        *sc18_prime_stats_pid[MAXGTACSHOSTS];
client_point        *sc19_prime_stats_pid[MAXGTACSHOSTS];
client_point        *sc20_prime_stats_pid[MAXGTACSHOSTS];

client_point        *sc13_nprime_stats_pid[MAXGTACSHOSTS];
client_point        *sc14_nprime_stats_pid[MAXGTACSHOSTS];
client_point        *sc15_nprime_stats_pid[MAXGTACSHOSTS];
client_point        *sc16_nprime_stats_pid[MAXGTACSHOSTS];
client_point        *sc17_nprime_stats_pid[MAXGTACSHOSTS];
client_point        *sc18_nprime_stats_pid[MAXGTACSHOSTS];
client_point        *sc19_nprime_stats_pid[MAXGTACSHOSTS];
client_point        *sc20_nprime_stats_pid[MAXGTACSHOSTS];

/******************** End Modification *********************/

struct gtacs_op_list    goplist[MAXGTACSHOSTS];
struct gas_op_list      gasoplist[MAXGASHOSTS];
struct sc_prime_strm    prime[MAXSCIDS];
struct sdr_process      sdr[17];
struct pub_process      pub[33];
GasMergeMsg  pub_merge_data[33];

int         first_gtacs_connection;
int         first_prime_cmd[MAXSCIDS];
int         first_nonprime_cmd[MAXSCIDS];
char        prime_cmd[MAXSCIDS][5];
char        nonprime_cmd[MAXSCIDS][5];
char        nonprime_id[MAXSCIDS][9];
int         automerge_flag[MAXSCIDS];
int         manmerge_flag[MAXSCIDS];
int         num_gas_hosts;
int         num_gtacs_hosts;
int         num_op_gtacs;
short       signon_seq_num;
short       global_seq_num;
short       merge_seq_num;
short       ping_seq_num;
char        *merge_buffer;
short       mb_num_bytes;
short       short_data;
int        long_data;

/* Manual Merge Request Queue */
ManualMergeQ  MMergeQ[MANUAL_MERGE_MAX];

#endif
