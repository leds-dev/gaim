/*
@(#)  File Name: IFM_ping_gas.c  Release 1.18  Date: 07/03/25, 08:32:58
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_ping_gas.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         V2.0        04/02       A. Raj          Add comments
         8.1         mar02-2004  K. Dolinh       Add log_event, log_debug
         8.7         apr15-2004
         8.8.1C      may24-2004                  add debug msg
         8.B         sep17-2004                  GTACS build 8B 
         9.2C        feb17-2005                  check pid before calling set_point
         9A1         mar04-2005                  remove call to set_point
         9AP         mar16-2005                  Build 9A Prime
         10A         sep10-2005                  move IFM_set_point_sarc_status to IFM_main
         10A         nov29-2005                  new IFM_set_point_sarc_status
         10B         Jan12-2006                  - recompile for Build 10B 
         11          Aug11-2006                  - use IFM_send_event 
                     Oct02-2006                  - comment out poll_point call 
                     Dec28-2010  D. Niklewski    increased the number of ping failures
                                                 allowed before action is taken from 1 to 3.

********************************************************************************
         Invocation:

             IFM_ping_gas ()

********************************************************************************
         Functional Description:

         This routine pings the GOES Archive Server.

*******************************************************************************/

/**
    IF it's time to ping the GOES Archive system THEN
        build the ping message
        CALL IFM_build_gas_msghdr
        DOFOR each GOES Archive server
            IF GOES Archive server is operational THEN
                send the ping message to the server
                IF ping is not successful THEN
                    write log message
                    DOFOR each possible SDR process
                        IF SDR process is active THEN
                            IF SDR process is sending data to this server THEN
                                kill this SDR process
                            ENDIF
                        ENDIF
                    ENDDO
                    DOFOR each possible PUB process
                        IF PUB process is active THEN
                            IF PUB process is sending data to this server THEN
                                kill this PUB process
                            ENDIF
                        ENDIF
                    ENDDO
                    DOFOR each GTACS
                        IF GTACS is operational THEN
                            send an event message to GTACS
                            set GTACS global variables
                        ENDIF
                    ENDDO
                ELSE
                    shutdown and close the GOES Archive server socket
                ENDIF
            ENDIF
        ENDDO
        CALL IFM_ping_gasdb
        DOFOR each GTACS
            IF GTACS is operational THEN
                poll the GAIM status global variable
                IF poll is not successful THEN
                    write log message
                ENDIF
            ENDIF
        ENDDO
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern void IFM_onintr_timer (int);
extern char *IFM_swap_bytes(int);

/********* Added 12/28/2010 by D. Niklewski (NOAA) *********/
#define MAX_PING_FAILURES_ALLOWED 3
int ping_failures[MAXGASHOSTS];
int firstEntry = 1;
/******************** End Modification *********************/

void IFM_ping_gas (void)
{
short cid, mnum;
int i, j, k, index, length, status;
long ping_diff_time, msize;
time_t ping_time2;
char nname [MAXHOSTLEN];
char proc [MAXPROCLEN];
char buffer [MAXBUFFLEN];
char host [MAXHOSTLEN];
char msgtxt [MAXMSGTEXT];
char ping_service [MAXSERVLEN];
char zzzz [MAXBUFFLEN];
char str [3];
char cseqnum [6];
char statchar [4];
char *c;
char message[MAXMSGTEXT];
struct itimerval timer;

int     poll_point_err = 0; 

/********* Added 12/28/2010 by D. Niklewski (NOAA) *********/
if (firstEntry) {
    for (i = 0; i < MAXGASHOSTS; i++)
        ping_failures[i] = 0;
    firstEntry = 0;
}
/******************** End Modification *********************/

timer.it_value.tv_sec = 2;
timer.it_value.tv_usec = 0;
timer.it_interval.tv_sec = 0;
timer.it_interval.tv_usec = 0;

ping_time2 = time(NULL);
ping_diff_time = difftime (ping_time2, ping_time1);

/* Ping the gas machines if it is time */
if (ping_diff_time >= gaim_monitor_rate) {

        ping_time1 = ping_time2;

        /*  Build ping message  */
        msize = 1;
        cid = GAS_PING_ID;
        mnum = ping_seq_num;
        strcpy (nname, (char *)net_host ("localhost"));
        strcpy (proc, IFM_PROC_NAME);
        IFM_build_gas_msghdr (&index, &buffer[0], msize, cid, mnum, nname, proc);

        /*  Update the value for the message size in the buffer  */

        long_data = index - 4;
	      memcpy (&buffer[0], (char *)(&long_data), 4);

        /*  Ping every operational GOES Archive Server  */
        for (i=0; i< num_gas_hosts; i++) {
        
   		log_event("ping_gas: %s\n", gasoplist[i].gas_addr); 
                if (gasoplist[i].operational == 1) {
                
                        strcpy (ping_service, "GaSeExec");
                        signal (SIGALRM, IFM_onintr_timer);
                        if (setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL)) {
                                log_event_err("ping_gas: setitimer failure");
                        }

                        if (status = net_call(gasoplist[i].gas_addr, ping_service, &gas_server_socket)) {
                        
                                timer.it_value.tv_sec = 0;
                                timer.it_value.tv_usec = 0;
                                timer.it_interval.tv_sec = 0;
                                timer.it_interval.tv_usec = 0;
                                setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL);

/********* Added 12/28/2010 by D. Niklewski (NOAA) *********/

                                ping_failures[i]++;

                                log_event("ping_gas: net_call failed for GAS.  host=%s  addr=%s  service=%s  status=%i  ping_failures = %d  MAX_PING_FAILURES_ALLOWED = %d",
                                        gasoplist[i].gas_host,gasoplist[i].gas_addr,ping_service,status,ping_failures[i],MAX_PING_FAILURES_ALLOWED);

                                if (ping_failures[i] >= MAX_PING_FAILURES_ALLOWED) {

                                        ping_failures[i] = 0;

                                        log_event_err("ping_gas: net_call failed FINAL ATTEMPT for GAS.  host=%s  addr=%s  service=%s  status=%i",
                                        gasoplist[i].gas_host,gasoplist[i].gas_addr,ping_service,status);

/******************** End Modification *********************/
/*
                                log_event_err("ping_gas: net_call failed for GAS.  host=%s  addr=%s  service=%s  status=%i",
                                        gasoplist[i].gas_host,gasoplist[i].gas_addr,ping_service,status);
*/

                                gasoplist[i].operational = 0;

                                /*  Stop all currently running processes sending data to this Archive server  */

                                /* Stop Stream data receiver processes */
                                for (j=1; j<17; j++) {
                                        if (sdr[j].active == 1) {
                                                if (strcmp (sdr[j].gasip, gasoplist[i].gas_addr) == 0){
                                                        strcpy (zzzz, "zzzzzzzz");
                                                        length = net_write (sdr_client_socket[j], (char *) &zzzz, strlen(zzzz));
                                                }
                                        } 
                                } 
                                        
                                /* reset prime requested variable to 0 (not requested ) */
                                for (j=0; j < MAXSCIDS; j++){
                                        prime[j].connected = 0; /* not connected to gtacs */
                                        prime[j].requested = 0; /* not requested by GAS */
                                }

                                /* Stop Playback Update Buffer processes */
                                for (j=17; j<33; j++) {
                                        if (pub[j].active == 1) {
                                                if (strcmp (pub[j].gasip, gasoplist[i].gas_addr) == 0) {
                                                        strcpy (zzzz, "zzzzzzzz");
                                                        length = net_write (pub_client_socket[j], (char *) &zzzz, strlen(zzzz));
                                                }
                                        } 
                                } 

                                /* send event to gtacs */
                                sprintf(msgtxt,"GAS server %s not responding",gasoplist[i].archive_name);
                                IFM_send_event(EVENT_ERR,msgtxt);

                                /* do for all GTACS servers */
                                for (j=0; j< num_gtacs_hosts; j++) {
                                
                                        if (goplist[j].operational == 1) {
                                        /* update global vars in GTACS ground stream */
                                                status = IFM_set_point_sarc_status("ping_gas",0);               
                                        } 
                                } 

                                /* Flush batched set_point operations to all streams 
                                status = flush_sets ();*/

                                /*  Update data archive states for all streams */
                                for (k=1; k < 4; k++) {                         
                                        IFM_update_status (k, "13");
                                        IFM_update_status (k, "14");
                                        IFM_update_status (k, "15");
                                        IFM_update_status (k, "16");
                                        IFM_update_status (k, "17");
                                        IFM_update_status (k, "18");
                                        IFM_update_status (k, "19");
                                        IFM_update_status (k, "20");
                                }

/********* Added 12/28/2010 by D. Niklewski (NOAA) *********/
                                }
/******************** End Modification *********************/
                                
                        } else { 
                        
                                /* connection to GAS successful */
                                timer.it_value.tv_sec = 0;
                                timer.it_value.tv_usec = 0;
                                timer.it_interval.tv_sec = 0;
                                timer.it_interval.tv_usec = 0;
                                setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL);

/********* Added 12/28/2010 by D. Niklewski (NOAA) *********/
                                /* Reset ping_failures counter since the
                                   ping was successful */
                                ping_failures[i] = 0;
/******************** End Modification *********************/

                                /* send ping msg to GAS */
                                length = net_write (gas_server_socket,(char *) &buffer, index);

                                /* strcpy (msgtxt, "IFM sent Ping Message # ");
                                sprintf (cseqnum, "%d", ping_seq_num);
                                strcat (msgtxt, cseqnum);
                                strcat (msgtxt, " to GOES Archive Server on ");
                                strcat (msgtxt, gasoplist[i].gas_host);
                                log_debug(msgtxt); */
                                
                                /* close connection */
                                shutdown (gas_server_socket, 2);
                                close (gas_server_socket);
                                gas_server_socket = -1;

                                /*  Update ping msg sequence number */
                                ping_seq_num += 1;
                                if (ping_seq_num >= 32768)  ping_seq_num = 1;

                        } /* endif ping status check */
                } /* endif GAS is operational */
/********* Added 12/28/2010 by D. Niklewski (NOAA) *********/
                else {
                        /* Reset ping_failures counter for non-operational GAS servers */
                        ping_failures[i] = 0;
                }
/******************** End Modification *********************/
        } /* enddo for every GAS host */

        /*  Ping the Archive database machine  */

        /* IFM_ping_gasdb (); */
        /* Commented out on 2/19/02 -- Yuning says they used to need a
                multicast ping, but when the N-Q system was placed on another
                network, the multicast ping could not be used successfully.
                The fix was to have the GAS do a system PING to see if the
                GAIM is alive. */

        /*  Call poll_point to prevent EPOCH from timing out  */

        /*** commented out by khai 
        for (j=0; j< num_gtacs_hosts; j++) {
        
                if (goplist[j].operational == 1) {
                
                        status = poll_point (gaim_status_pid[j]);
                        if (status != STRM_SUCCESS) {
                                
                                if (++poll_point_err < 10)
                                        log_event_err("ping_gas: poll_point failure for GAIM_STATUS in %s",goplist[j].grndstrm);

                                if (gs_connect_id[j] != 0) disconnect_stream (gs_connect_id[j]);
                                
                                status = connect_stream (goplist[j].grndstrm, client_timeout,&gs_connect_id[j]);
                                if (status != STRM_SUCCESS) {
                                        log_event_err("ping_gas: reconnect to ground stream %s failed. err=%d",goplist[j].grndstrm,status);
                                        goplist[j].operational = 0;
                                                                                
                                        if (gs_connect_id[j] != 0) {
                                                log_event_err("ping_gas: IFM is disconnecting from %s",goplist[j].gtacs_host);
                                                disconnect_stream (gs_connect_id[j]);
                                        }
                                        num_op_gtacs -= 1;
                                        if (num_op_gtacs == 0) first_gtacs_connection = 1;

                                } else {
                                        log_event("ping_gas: reconnected to ground stream %s",goplist[j].grndstrm);     
                                        poll_point_err = 0; 
                                }
                                
                        } 
                        
                } 
        } 
        ***/
                
} /* endif time to ping */

return;
}


