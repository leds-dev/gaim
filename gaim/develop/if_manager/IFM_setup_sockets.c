/*
@(#)  File Name: IFM_setup_sockets.c  Release 1.4  Date: 07/03/25, 08:33:12
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_setup_sockets.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	 8.1         mat02-2004  K. Dolinh       Add log_event, log_debug
	 					 Add IFM_close_sockets()


********************************************************************************
         Invocation:

             IFM_setup_sockets()

********************************************************************************
         Functional Description:

         This routine sets up all of the listening sockets.

*******************************************************************************/

/**
    setup the 8 GOES Archive listening sockets
    setup the 16 Stream_Data_Receiver listening sockets
    setup the 16 Playback_Update_Buffer listening sockets
    setup the Check_GTACS listening socket
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

int IFM_setup_sockets (void)
{
int i, status, n;
char str[3];
char msgtxt [MAXMSGTEXT];
char service [MAXSERVLEN];

/*  Setup the 8 GOES Archive listening sockets  */
n = 1;

strcpy (service, GAS_SCEXEC);
if (net_answer (service, -99, &gas_listening_socket[n], &gas_client_socket[n]))
		{
		log_event_err("net_answer error for socket GAS SCEXEC");
		return 1;
		}
if (gas_listening_socket[n] > nfds) nfds = gas_listening_socket[n];
FD_SET (gas_listening_socket[n], &rd_mask);
n++;
log_debug("setup successful for GAS SCEXEC listen socket");

strcpy (service, GAS_AUTOMERGE);
if (net_answer (service, -99, &gas_listening_socket[n], &gas_client_socket[n]))
		{
		log_event_err("net_answer error for socket GAS AUTOMERGE");
		return 1;
		}
if (gas_listening_socket[n] > nfds) nfds = gas_listening_socket[n];
FD_SET (gas_listening_socket[n], &rd_mask);
n++;
log_debug("setup successful for GAS AUTOMERGE listen socket");


strcpy (service, GAS_MANMERGE);
if (net_answer (service, -99, &gas_listening_socket[n], &gas_client_socket[n]))
		{
		log_event_err("net_answer error for socket GAS MANMERGE");
		return 1;
		}
if (gas_listening_socket[n] > nfds) nfds = gas_listening_socket[n];
FD_SET (gas_listening_socket[n], &rd_mask);
n++;
log_debug("setup successful for GAS MANMERGE listen socket");


strcpy (service, GAS_INGEST);
if (net_answer (service, -99, &gas_listening_socket[n], &gas_client_socket[n]))
		{
		log_event_err("net_answer error for socket GAS INGEST");
		return 1;
		}
if (gas_listening_socket[n] > nfds) nfds = gas_listening_socket[n];
FD_SET (gas_listening_socket[n], &rd_mask);
n++;
log_debug("setup successful for GAS INGEST listen socket");

strcpy (service, GAS_STATS);
if (net_answer (service, -99, &gas_listening_socket[n], &gas_client_socket[n]))
		{
		log_event_err("net_answer error for socket GAS STATS");
		return 1;
		}
if (gas_listening_socket[n] > nfds) nfds = gas_listening_socket[n];
FD_SET (gas_listening_socket[n], &rd_mask);
n++;
log_debug("setup successful for GAS STATS listen socket");

strcpy (service, GAS_TLM);
if (net_answer (service, -99, &gas_listening_socket[n], &gas_client_socket[n]))
		{
		log_event_err("net_answer error for socket GAS TLM");
		return 1;
		}
if (gas_listening_socket[n] > nfds) nfds = gas_listening_socket[n];
FD_SET (gas_listening_socket[n], &rd_mask);
n++;
log_debug("setup successful for GAS TLM listen socket");

strcpy (service, GAS_UPDATE);
if (net_answer (service, -99, &gas_listening_socket[n], &gas_client_socket[n]))
		{
		log_event_err("net_answer error for socket GAS UPDATE");
		return 1;
		}
if (gas_listening_socket[n] > nfds) nfds = gas_listening_socket[n];
FD_SET (gas_listening_socket[n], &rd_mask);
n++;
log_debug("setup successful for GAS UPDATE listen socket");


strcpy (service, GAS_PING);
if (net_answer (service, -99, &gas_listening_socket[n], &gas_client_socket[n]))
		{
		log_event_err("net_answer error for socket GAS PING");
		return 1;
		}
if (gas_listening_socket[n] > nfds) nfds = gas_listening_socket[n];
FD_SET (gas_listening_socket[n], &rd_mask);
n++;
log_debug("setup successful for GAS PING listen socket");


/*  Setup the 16 Stream_Data_Receiver listening sockets  */

for (i=1; i< 17; i++)
	{
	strcpy (service, SDR_SERVICE);
	sprintf (str, "%d", i);
	strcat (service, str);
	if (net_answer (service, -99, &sdr_listening_socket[i],&sdr_client_socket[i]))
		{
		log_event_err("net_answer error for listen socket SDR # %d",i);
		return 1;
		}
        if (sdr_listening_socket[i] > nfds) nfds = sdr_listening_socket[i];
	FD_SET (sdr_listening_socket[i], &rd_mask);
	log_debug("setup successful for listen socket SDR # %d",i);
	}

/*  Setup the 16 Playback_Update_Buffer listening sockets  */

for (i=17; i< 33; i++)
	{
	strcpy (service, PUB_SERVICE);
	sprintf (str, "%d", i);
	strcat (service, str);
	if (net_answer (service, -99, &pub_listening_socket[i],&pub_client_socket[i]))
		{
		log_event_err("net_answer error for listen socket PUB # %d",i);
		return 1;
		}

        if (pub_listening_socket[i] > nfds) nfds = pub_listening_socket[i];
	FD_SET (pub_listening_socket[i], &rd_mask);
	log_debug("setup successful for listen socket PUB # %d",i);
	}

/*  Setup the Check_GTACS listening socket  */

strcpy (service, CGT_SERVICE);
if (net_answer (service, -99, &cgt_listening_socket,&cgt_client_socket))
	{
	log_event_err("net_answer error for listen socket CGT");
	return 1;
	}
FD_SET (cgt_listening_socket, &rd_mask);
if (cgt_listening_socket > nfds) nfds = cgt_listening_socket;
log_debug("setup successful for listen socket CGT");

/* all sockets setup successful */
log_event("setup of all sockets completed...");
return 0;
}



void IFM_close_sockets (void)
{
int i;

/* close all sockets */

for (i=1; i< 9; i++)
	{
	if (gas_listening_socket[i] != -1)
		{
		shutdown (gas_listening_socket[i], 2);
		close (gas_listening_socket[i]);
		gas_listening_socket[i] = -1;
		}

	if (gas_client_socket[i] != -1)
		{
		shutdown (gas_client_socket[i], 2);
		close (gas_client_socket[i]);
		gas_client_socket[i] = -1;
		}
	}

for (i=1; i< 17; i++)
	{
	if (sdr_listening_socket[i] != -1)
		{
		shutdown (sdr_listening_socket[i], 2);
		close (sdr_listening_socket[i]);
		sdr_listening_socket[i] = -1;
		}

	if (sdr_client_socket[i] != -1)
		{
		shutdown (sdr_client_socket[i], 2);
		close (sdr_client_socket[i]);
		sdr_client_socket[i] = -1;
		}
	}

for (i=17; i< 33; i++)
	{
	if (pub_listening_socket[i] != -1)
		{
		shutdown (pub_listening_socket[i], 2);
		close (pub_listening_socket[i]);
		pub_listening_socket[i] = -1;
		}

	if (pub_client_socket[i] != -1)
		{
		shutdown (pub_client_socket[i], 2);
		close (pub_client_socket[i]);
		pub_client_socket[i] = -1;
		}
	}

if (cgt_listening_socket != -1)
	{
	shutdown (cgt_listening_socket, 2);
	close (cgt_listening_socket);
	cgt_listening_socket = -1;
	}

if (cgt_client_socket != -1)
	{
	shutdown (cgt_client_socket, 2);
	close (cgt_client_socket);
	cgt_client_socket = -1;
	}

if (gas_server_socket != -1)
	gas_server_socket = -1;

FD_ZERO (&rd_mask);		

}
