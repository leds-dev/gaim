/*  
@(#)  File Name: IFM_varsex.h  Release 1.6  Date: 07/03/25, 08:33:20
*/

#ifndef IFM_VARSEX_H
#define IFM_VARSEX_H

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         INCLUDE         :       IFM_vars.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
    
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         9.2         jan31-2005  K. Dolinh       Add new array pub_merge_data   
                     apr28-2005                  Add nonprime_id array    
         11          Mar13-2007                  fix compile warning
                     Jan03-2011  D. Niklewski    Add client_point pointers for
                                                 statistics globals
    
********************************************************************************
         Functional Description:
         
         This include file contains variables that are used by the 
         N-Q_Interface_Manager process.
    
*******************************************************************************/

#include <time.h>
#include "strmutil.p"
#include "IFM_struct.h"

extern char         gaimname [7];
extern char         *debugfilename;
extern char         debug_message[MAXMSGTEXT];

extern fd_set           dt_mask;
extern fd_set           rd_mask;
extern int          num_rt_streams [MAXNUMRT];
extern int          num_pb_streams [MAXNUMPB];
extern int          num_sim_streams [MAXNUMSIM];
extern int          gas_server_socket;
extern int          cgt_client_socket;
extern int          cgt_listening_socket;
extern int          gas_client_socket [9]; 
extern int          gas_listening_socket [9];
extern int          sdr_client_socket [17]; 
extern int          sdr_listening_socket [17];
extern int          pub_client_socket [33]; 
extern int          pub_listening_socket [33];
extern int          nfds;

extern struct timeval       client_timeout;
extern struct timeval       select_time;
extern time_t           mon_time1;
extern time_t           ping_time1;
extern long         gaim_monitor_rate;
extern int          mon_first_time;
extern int          gindex;
extern client_connection    *gs_connect_id[MAXGTACSHOSTS];
extern client_point     *gaim_mon_rate_pid[MAXGTACSHOSTS];
extern client_point     *sc13_prime_pid[MAXGTACSHOSTS];
extern client_point     *sc14_prime_pid[MAXGTACSHOSTS];
extern client_point     *sc15_prime_pid[MAXGTACSHOSTS];
extern client_point     *sc16_prime_pid[MAXGTACSHOSTS];
extern client_point     *sc17_prime_pid[MAXGTACSHOSTS];
extern client_point     *sc18_prime_pid[MAXGTACSHOSTS];
extern client_point     *sc19_prime_pid[MAXGTACSHOSTS];
extern client_point     *sc20_prime_pid[MAXGTACSHOSTS];
extern client_point     *gaim_status_pid[MAXGTACSHOSTS];
extern client_point     *sarc13_status_pid[MAXGTACSHOSTS];
extern client_point     *sarc14_status_pid[MAXGTACSHOSTS];
extern client_point     *sarc15_status_pid[MAXGTACSHOSTS];
extern client_point     *sarc16_status_pid[MAXGTACSHOSTS];
extern client_point     *sarc17_status_pid[MAXGTACSHOSTS];
extern client_point     *sarc18_status_pid[MAXGTACSHOSTS];
extern client_point     *sarc19_status_pid[MAXGTACSHOSTS];
extern client_point     *sarc20_status_pid[MAXGTACSHOSTS];
extern client_point     *archive_rt_13_pid[MAXGTACSHOSTS];
extern client_point     *archive_rt_14_pid[MAXGTACSHOSTS];
extern client_point     *archive_rt_15_pid[MAXGTACSHOSTS];
extern client_point     *archive_rt_16_pid[MAXGTACSHOSTS];
extern client_point     *archive_rt_17_pid[MAXGTACSHOSTS];
extern client_point     *archive_rt_18_pid[MAXGTACSHOSTS];
extern client_point     *archive_rt_19_pid[MAXGTACSHOSTS];
extern client_point     *archive_rt_20_pid[MAXGTACSHOSTS];
extern client_point     *archive_am_13_pid[MAXGTACSHOSTS];
extern client_point     *archive_am_14_pid[MAXGTACSHOSTS];
extern client_point     *archive_am_15_pid[MAXGTACSHOSTS];
extern client_point     *archive_am_16_pid[MAXGTACSHOSTS];
extern client_point     *archive_am_17_pid[MAXGTACSHOSTS];
extern client_point     *archive_am_18_pid[MAXGTACSHOSTS];
extern client_point     *archive_am_19_pid[MAXGTACSHOSTS];
extern client_point     *archive_am_20_pid[MAXGTACSHOSTS];
extern client_point     *archive_mm_13_pid[MAXGTACSHOSTS];
extern client_point     *archive_mm_14_pid[MAXGTACSHOSTS];
extern client_point     *archive_mm_15_pid[MAXGTACSHOSTS];
extern client_point     *archive_mm_16_pid[MAXGTACSHOSTS];
extern client_point     *archive_mm_17_pid[MAXGTACSHOSTS];
extern client_point     *archive_mm_18_pid[MAXGTACSHOSTS];
extern client_point     *archive_mm_19_pid[MAXGTACSHOSTS];
extern client_point     *archive_mm_20_pid[MAXGTACSHOSTS];

/********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
/* Add new client_point pointers for statistics globals    */

extern client_point     *gaim_state_pid[MAXGTACSHOSTS];

extern client_point     *sc13_prime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc14_prime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc15_prime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc16_prime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc17_prime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc18_prime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc19_prime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc20_prime_stats_pid[MAXGTACSHOSTS];

extern client_point     *sc13_nprime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc14_nprime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc15_nprime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc16_nprime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc17_nprime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc18_nprime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc19_nprime_stats_pid[MAXGTACSHOSTS];
extern client_point     *sc20_nprime_stats_pid[MAXGTACSHOSTS];

/******************** End Modification *********************/

extern struct gtacs_op_list     goplist[MAXGTACSHOSTS];
extern struct gas_op_list   gasoplist[MAXGASHOSTS];
extern struct sc_prime_strm prime[MAXSCIDS];
extern struct sdr_process   sdr[17];
extern struct pub_process   pub[33];
extern GasMergeMsg  pub_merge_data[33];

extern int          first_gtacs_connection;
extern int          first_prime_cmd[MAXSCIDS];
extern int          first_nonprime_cmd[MAXSCIDS];
extern char         prime_cmd[MAXSCIDS][5];
extern char         nonprime_cmd[MAXSCIDS][5];
extern char         nonprime_id[MAXSCIDS][9];
extern int          automerge_flag[MAXSCIDS];
extern int          manmerge_flag[MAXSCIDS];
extern int          num_gas_hosts;
extern int          num_gtacs_hosts;
extern int          num_op_gtacs;
extern short            signon_seq_num;
extern short            global_seq_num;
extern short            merge_seq_num;
extern short            ping_seq_num;
extern char         *merge_buffer;
extern short            mb_num_bytes;
extern short            short_data;
extern int         long_data;

/* Manual Merge Request Queue */
extern ManualMergeQ  MMergeQ[MANUAL_MERGE_MAX];

#endif
