/*
@(#)  File Name: IFM_process_sdr_msg.c  Release 1.12  Date: 07/03/25, 08:33:08
*/

/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       Kratos Defense
         SOURCE          :       IFM_process_sdr_msg.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         V2.0        04/02       A. Raj          Reinitialize sdr structure
						 						 when a stream terminates
         V3.0        06/21/02    A. Raj          Reinitialize sdr structure
						 						after sending critical error msg
	 	8.1         mar02-2004  K. Dolinh       Add log_event, log_debug
	 					 						Clear sdr structure
		8.3	     	mar08-2004  K. Dolinh		 ...
	 	     		apr01-2004                  fixed log_event_err call
	 	8.8         may06-2004                  connected=0 means not-connected
	 					 						don't set prime->connected to 0 
												when normal shutdown
		9.2         feb21-2005                  clear saved nonprime cmd "UPDx"
		11			Mar13-2007					fix compile warning
		 										changed to GAIM_EVTNUM
												 
********************************************************************************
         Invocation:

             IFM_process_sdr_msg (sdr_index)

         where

             <sdr_index> - I
                 is an integer indicating which Stream_Data_Receiver (1-16)
                 sent the message.

********************************************************************************
         Functional Description:

         This routine processes messsages received from the Stream_Data_Receiver
         process.

*******************************************************************************/
/**
    read the data from the socket
    IF the data length is greater than 7 THEN
        IF the first 8 characters are "zzzzzzzz" THEN
            write log message
            shutdown and close the SDR socket
            reset the SDR structure
            CALL IFM_update_status
        ENDIF
        IF the first 8 characters are "z!z!z!z!" THEN
            write log message
            shutdown and close the SDR socket
            reset the SDR structure
            CALL IFM_update_status
            DOFOR each GTACS
                IF GTACS is operational THEN
                    send a critical event message to GTACS
                ENDIF
            ENDDO
        ENDIF
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

void IFM_process_sdr_msg (int sdr_index)
{
char str [3];
char buffer [MAXBUFFLEN];
char msgtxt [MAXMSGTEXT];
char statuschar [MAXMSGTEXT];
int i, length;
int scidnum;

/*  Read the data from the socket  */
length = net_read (sdr_client_socket[sdr_index], (char *)&buffer, MAXNBUFLEN);

log_event("received msg from SDR-%d  len=%d  text=%8.8s",sdr_index,length,buffer);

/* exit if msglen not valid */
if (length <= 7) return;

/*  Did the SDR process complete?  */
if (strncmp (buffer, "zzzzzzzz", 8) == 0) {

	/*  Shutdown, close, and clear the socket  */
	log_event("close SDR-%d client socket. job completed",sdr_index);

	shutdown (sdr_client_socket[sdr_index], 2);
	close (sdr_client_socket[sdr_index]);
	FD_CLR (sdr_client_socket[sdr_index], &rd_mask);
	sdr_client_socket[sdr_index] = -1;

	/*  Reset sdr structure  */
	sdr[sdr_index].active = 0;
	sdr[sdr_index].prime = 0;
	strcpy (sdr[sdr_index].gasip, " ");
	strcpy (sdr[sdr_index].gtacs, " ");
	strcpy (sdr[sdr_index].scid, " ");
	strcpy (sdr[sdr_index].stream, " ");

	/*  Update the GTACS global variable  */
	IFM_update_status (1, sdr[sdr_index].scid);
}

/*  Did the SDR process fail?  */
if (strncmp (buffer, "z!z!z!z!", 8) == 0) {

	/*  Shutdown, close, and clear the socket  */
	log_event_err("close SDR-%d client socket due to sdr error",sdr_index);

	shutdown (sdr_client_socket[sdr_index], 2);
	close (sdr_client_socket[sdr_index]);
	FD_CLR (sdr_client_socket[sdr_index], &rd_mask);
	sdr_client_socket[sdr_index] = -1;

	log_event_err("archiving has been stopped for scid=%s stream=%s on %s",
		sdr[sdr_index].scid,sdr[sdr_index].stream,sdr[sdr_index].gtacs);

	if (strncmp (sdr[sdr_index].scid, "13", 2) == 0)  scidnum = 0;
	else if (strncmp (sdr[sdr_index].scid, "14", 2) == 0)  scidnum = 1;
	else if (strncmp (sdr[sdr_index].scid, "15", 2) == 0)  scidnum = 2;
	else if (strncmp (sdr[sdr_index].scid, "16", 2) == 0)  scidnum = 3;
	else if (strncmp (sdr[sdr_index].scid, "17", 2) == 0)  scidnum = 4;
	else if (strncmp (sdr[sdr_index].scid, "18", 2) == 0)  scidnum = 5;
	else if (strncmp (sdr[sdr_index].scid, "19", 2) == 0)  scidnum = 6;
	else if (strncmp (sdr[sdr_index].scid, "20", 2) == 0)  scidnum = 7;
	else return;

	if (sdr[sdr_index].prime == 1) {
		/* clear prime[scid] connected status  */
		prime[scidnum].connected = 0;
	} else {
		/* clear saved nonprime cmd "UPDx" */
		strcpy (nonprime_cmd[scidnum], "    ");
		/* clear nonprime id (streamname) */
		nonprime_id[scidnum][0] = 0;
	}
					
	/*  Reset sdr structure  */
	sdr[sdr_index].active = 0;
	sdr[sdr_index].prime = 0;
	strcpy (sdr[sdr_index].gasip, " ");
	strcpy (sdr[sdr_index].gtacs, " ");
	strcpy (sdr[sdr_index].scid, " ");
	strcpy (sdr[sdr_index].stream, " ");

	/*  Update the GTACS global variable  */
	IFM_update_status (1, sdr[sdr_index].scid);

	/*  Send a critical event message to all operational GTACS  */
	if ((sdr_index != 2) && (sdr_index != 4) &&
	    (sdr_index != 6) && (sdr_index != 8)) {

		for (i=0; i< num_gtacs_hosts; i++) {
			if (goplist[i].operational == 1) {
				strcpy (msgtxt, "@");
				strcat (msgtxt, goplist[i].grndstrm);
				strcat (msgtxt, " archiving of realtime tlm (");
				strcat (msgtxt, sdr[sdr_index].stream);
				strcat (msgtxt, ") from ");
				strcat (msgtxt, sdr[sdr_index].gtacs);
				strcat (msgtxt, " to the GAS failed");
				send_event (GAIM_EVTNUM_ERR, msgtxt, client_timeout);
			}
		} /* end dofor */
	}

}

return;
}

