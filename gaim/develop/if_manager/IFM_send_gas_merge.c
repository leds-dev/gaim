/*  
@(#)  File Name: IFM_send_gas_merge.c  Release 1.10  Date: 07/03/25, 08:33:11
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_send_gas_merge.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
		 8.1         02/2004     K. Dolinh       Add log_event, log_debug
         9.2         jan31-2005                  Change event message	
		 11			 mar13-2007					 fix compile warning 

********************************************************************************
         Invocation:
        
             IFM_send_gas_merge (type, gas_index, merge)
             
         where
         
             <type> - I
                 is an integer indicating the status of this message
                 (0=GTACS available, 1=no GTACS connection,
                 3=request already active, this request rejected.

             <gas_index> - I
                 is an integer indicating which GOES Archive process sent the
                 message.
			     NOTE: THIS IS NOT CURRENTLY USED

             <merge> - I
                 is the structure containing the received merge message
                         
********************************************************************************
         Functional Description:

         This routine sends a merge response message to the GOES Archive 
         Server.
	
*******************************************************************************/

/**
    CALL IFM_build_gas_merge
    DOFOR each GOES Archive server
        IF merge message node name matches this GOES Archive server THEN
            set success flag to true
            BREAK
        ENDIF
    ENDDO
    IF success flag is set to true THEN
        send message message to GOES Archive server
        IF send fails THEN
            write log message
        ELSE
            write log message
            shutdown and close GOES Archive server socket
        ENDIF
    ELSE
        write log message
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern void IFM_onintr_timer (int);
extern char *IFM_swap_bytes(void);

void IFM_send_gas_merge (
		int type,
		int gas_index,
		GasMergeMsg merge )
		
{
int i, index, length;
int success = 0;
short cid, mnum;
long msize;
char buffer [MAXBUFFLEN];
char host [MAXHOSTLEN];
char msgtxt [MAXMSGTEXT];
char nname [MAXHOSTLEN];
char proc [MAXPROCLEN];
char merge_service [MAXSERVLEN];
char cseqnum [6];
char str [3];
char *c;
struct itimerval timer;

timer.it_value.tv_sec = 2;
timer.it_value.tv_usec = 0;
timer.it_interval.tv_sec = 0;
timer.it_interval.tv_usec = 0;


/*  Build the merge message  */
IFM_build_gas_merge (type, &index, merge, &buffer[0]);

/*  Determine the correct GAS to send this message to  */
for (i=0; i< num_gas_hosts; i++) {
	if (strcmp (merge.msg_hdr.node_name, gasoplist[i].archive_name) == 0) {
		strcpy (host, gasoplist[i].gas_addr);
		success = 1;
		break;
	}
}

log_event("send_gas_merge: type=%d  node_name=%s  len=%d  scid=%s  day=%d  i=%d  success=%d",
	type, merge.msg_hdr.node_name, merge.msg_hdr.node_name_len, merge.scid, merge.start_day, i, success);

if (success) {

	/*  Determine the correct GAS process to send this message to  */
	if (strncmp (merge.msg_hdr.process_name, "GaAutoMerge", 11) == 0) {
		strcpy (merge_service, GAS_AUTOMERGE);
		/* sprintf (str, "%d", gas_index+8);
		strcat (merge_service, str); */
	} else {
		strcpy (merge_service, GAS_MANMERGE);
		/* sprintf (str, "%d", gas_index+4);
		strcat (merge_service, str); */
	}

	/*  Connect to the GAS and send the message  */
	signal (SIGALRM, IFM_onintr_timer);

	if (setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL)) {
		strcpy (msgtxt, "send_gas_merge: set itimer failed");
		log_event_err(msgtxt);
	}

 	if (net_call (host, merge_service, &gas_server_socket)) {
		timer.it_value.tv_sec = 0;
		timer.it_value.tv_usec = 0;
		timer.it_interval.tv_sec = 0;
		timer.it_interval.tv_usec = 0;
		setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL);

		/*strcpy (msgtxt, "Cannot connect to GAS Merge on ");
		strcat (msgtxt, merge.msg_hdr.node_name);*/
		log_event_err("send_gas_merge: Cannot connect to %s",merge.msg_hdr.node_name);	
			
	} else {
		timer.it_value.tv_sec = 0;
		timer.it_value.tv_usec = 0;
		timer.it_interval.tv_sec = 0;
		timer.it_interval.tv_usec = 0;
		setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL);

  		length = net_write (gas_server_socket,(char *)&buffer, index);
		
  		/*strcpy (msgtxt, "sent Merge Message # ");
  		sprintf (cseqnum, "%d", merge_seq_num);
  		strcat (msgtxt, cseqnum);
  		strcat (msgtxt, " to GAS on ");
  		strcat (msgtxt, merge.msg_hdr.node_name);
		log_event(msgtext);*/
		
		log_event("sent GAS Merge Message (status=%d  seq=%d) to %s", type, merge_seq_num, merge.msg_hdr.node_name); 
  		shutdown (gas_server_socket, 2); 
 		close (gas_server_socket);
 		gas_server_socket = -1;

 		/*  Update the sequence number for this message type  */
 		merge_seq_num += 1;
		if (merge_seq_num >= 32768)  merge_seq_num = 1;
	}
		
} else {
	strcpy (msgtxt, "send_gas_merge: cannot resolve GAS hostname=");
	strcat (msgtxt, merge.msg_hdr.node_name);
	log_event_err(msgtxt);		
}

return;
}
