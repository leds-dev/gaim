/*  
@(#)  File Name: IFM_swap_bytes.c  Release 1.4  Date: 07/03/25, 08:33:16
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_swap_bytes.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation

********************************************************************************
         Invocation:
        
             swapchar = IFM_swap_bytes (type)
             
         where
         
             <type> - I
                 is an integer specifying whether to byte swap a short (=1)
                 or a long (=2)
             <swapchar> - O
                 is a pointer to the byte swapped character string
                         
********************************************************************************
         Functional Description:

         This routine swaps bytes in short integers and long integers so that
         the GOES Archive software and GAIM can understand the data that is 
         read or written.
	
*******************************************************************************/

/**
    IF byte swapping a short integer THEN
        swap the bytes
    ELSEIF byte swapping a long integer THEN
        swap the bytes
    ELSE
        convert input to character
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

char *IFM_swap_bytes (int type)
{
int i;
static char sc[2], lc[4];
char *swapchar, *sp, *lp;
int smap[2] = {1,0};
int lmap[4] = {3,2,1,0};

if (type == 1)	/*  Are we byte swapping a short integer?  */
	{
	sp = (char *)(&short_data);
	for (i=0; i<2; i++)   sc[i] = sp[smap[i]];
	swapchar = sc;
	}
else if (type == 2)	/*  Or are we byte swapping a long integer?  */
	{
	lp = (char *)(&long_data);
	for (i=0; i<4; i++)   lc[i] = lp[lmap[i]];
	swapchar = lc;
	}
else			/*  Just in case nothing got passed in  */
	{
	sprintf (sc,"%d",0);					
	swapchar = sc;
	}

return (swapchar);
}
