/*
@(#)  File Name: IFM_process_gas_global.c  Release 1.23  Date: 07/03/25, 08:33:02
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_process_gas_global.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         V2.0        04/29       A. Raj          Verify the stream name and
						 scid in the global update
         V2.0        05/29/02    A. Raj          connect_stream for no-primeUPD
	 8.1         mar06-2004  K. Dolinh       Add log_event, log_debug
	 8.6         apr02-2004                  Add debug_arg
	 8.8	     may06-2004                  .connected=0 means not-connected
	 8.8.1B      may18-2004                  add streamid to msg "Start NonPrime"
	 8.B         sep17-2004                  GTACS build 8B
	 9.2         feb17-2005                  Print header/body of GAS global msg
	 9AP3        apr19-2005                  fix "cannot resolve..." msg
	             apr27-2005                  ignore UPDx when processing nonprime msg
		 11		 Mar13-2007					 fix compile warning
		 								 	 fix call to log_event(spawn nonprime ...)

********************************************************************************
         Invocation:

             IFM_process_gas_global (gas_index, buffer)

         where

             <gas_index> - I
                 is an integer indicating which GOES Archive process sent the
                 message.

             <buffer> - I
                 is a pointer to a character string containing the message

********************************************************************************
         Functional Description:

         This routine processes global update/status messsages received from
         the GOES Archive Server.

*******************************************************************************/

/**
    CALL IFM_extract_gas_msghdr
    CALL IFM_extract_gas_global
    DOFOR each GOES Archive server
        IF global message node name matches this GOES Archive server THEN
            get host name
            set success flag to true
            BREAK
        ENDIF
    ENDDO
    IF success flag is true THEN
        IF this is a prime command THEN
            write log message
            IF starting a data flow THEN
                DOFOR each GTACS
                    DOFOR each realtime stream
                        IF the prime stream matches this stream THEN
                            IF this GTACS is operational THEN
                                DOFOR each possible prime SDR process
                                    IF this SDR process is not active THEN
                                        build parameters needed to spawn 1st SDR
                                        CALL IFM_spawn_task
                                        write log message
                                        build SDR structure
                                        build parameters needed to spawn 2nd SDR
                                        CALL IFM_spawn_task
                                        write log message
                                        build SDR structure
                                        CALL IFM_update_status
                                        BREAK
                                    ENDIF
                                ENDDO
                            ELSE
                                CALL IFM_send_gas_global
                                BREAK
                            ENDIF
                        ENDIF
                    ENDDO
                ENDDO
            ELSEIF stopping a data flow THEN
                DOFOR each possible prime SDR process
                    IF this SDR process is active THEN
                        IF this SDR spacecraft matches the message THEN
                            kill this SDR process
                            IF the 2nd SDR process is also active THEN
                                kill this SDR process
                                BREAK
                            ENDIF
                        ENDIF
                    ENDIF
                 ENDDO
            ELSE
                write log message
            ENDIF
        ENDIF
        IF this is a nonprime command THEN
            write log message
            --- ARaj - check the SCID for the requested stream name ---
            Lookup the PID of the SPACECRAFT_ID variable.
            Get the current value of SC_504_02_ID.
            -- ARaj --
            IF starting a data flow THEN
              DOFOR each GTACS
                  IF the message GTACS name matches this GTACS THEN
                      IF this GTACS is operational THEN
                          DOFOR each possible nonprime SDR process
                              IF this SDR process is not active THEN
                                  build parameters needed to spawn SDR
                                  CALL IFM_spawn_task
                                  write log message
                                  build SDR structure
                                  CALL IFM_update_status
                                  BREAK
                              ENDIF
                          ENDDO
                      ELSE
                          CALL IFM_send_gas_global
                      ENDIF
                      BREAK
                  ENDIF
              ENDDO
            ELSEIF stopping a data flow THEN
                DOFOR each possible nonprime SDR process
                    IF this SDR process is active THEN
                        IF parameters match this SDR process THEN
                            kill this SDR process
                            BREAK
                        ENDIF
                    ENDIF
                ENDDO
            ELSE
                write log message
            ENDIF
        ENDIF
    ELSE
        write log message
    ENDIF
    free all of the allocated memory for this message
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#define _STRM_ERR
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern  char *debug_arg;
extern  int  debug_gas;

void IFM_process_gas_global (
	int gas_index,
	char *buffer)
{
int i, j, k, length, scindex, cont, index = 0;
int status;
int success = 0;
int success2 = 0;
int success3 = 0;
int tmpscindex;

char gtname [MAXHOSTLEN];
char host [MAXHOSTLEN];
char msgtxt [MAXMSGTEXT], tmpstr[MAXMSGTEXT];
char syscall [MAXMSGTEXT];
char statuschar [MAXMSGTEXT];
char str [3];
char zzzz [MAXBUFFLEN];
GasGlobalUpdateMsg global;
char fifteen_spaces[16]="               ";

char global_name[32]; /* Global variable name */
char ground_stream_name[32]; /* The name of the ground stream */
char stream_scid[3];
client_point *spacecraft_id_pid;
client_connection    *rts_connect_id;
char non_prime_stream[6]; /* ARaj - non-prime stream name */

stream_scid[0]='\0';
non_prime_stream[0]='\0';

/*  Extract the fields in the message header  */
IFM_extract_gas_msghdr (&index, buffer, &global.msg_hdr);

/*  log_event("GAS GLOBAL HEADER: %d %d %d %d %s %d %s",
	global.msg_hdr.message_size, global.msg_hdr.class_id,
	global.msg_hdr.message_number, global.msg_hdr.node_name_len,
	global.msg_hdr.node_name, global.msg_hdr.process_name_len,
	global.msg_hdr.process_name);*/

/*  Extract the fields in the message body  */
IFM_extract_gas_global (&index, buffer, &global);

/***DEBUG print */
if (debug_gas) {

	log_event("GAS GLOBAL HEADER: size=%d id=%d msgnum=%d node_len=%d node_name=(%s) proc_len=%d proc_name=(%s)",
	global.msg_hdr.message_size, global.msg_hdr.class_id,
	global.msg_hdr.message_number, global.msg_hdr.node_name_len,
	global.msg_hdr.node_name, global.msg_hdr.process_name_len,
	global.msg_hdr.process_name);

    log_event("GAS GLOBAL MSG   : stat=%d gtacslen=%d gtacs=%8.8s sc=%2.2s gas=%6.6s pcmd=%4.4s npcmd=%4.4s npid=%s",
	global.status,
	global.gtacs_name_len,global.gtacs_name,global.scid,global.archive_id,
	global.prime_command,global.non_prime_command,global.non_prime_id);
}

if (strncmp (global.scid, "13", 2) == 0)  scindex = 0;
else if (strncmp (global.scid, "14", 2) == 0)  scindex = 1;
else if (strncmp (global.scid, "15", 2) == 0)  scindex = 2;
else if (strncmp (global.scid, "16", 2) == 0)  scindex = 3;
else if (strncmp (global.scid, "17", 2) == 0)  scindex = 4;
else if (strncmp (global.scid, "18", 2) == 0)  scindex = 5;
else if (strncmp (global.scid, "19", 2) == 0)  scindex = 6;
else if (strncmp (global.scid, "20", 2) == 0)  scindex = 7;
else  return;

for (i=0; i< num_gas_hosts; i++) {
	if (strcmp (global.msg_hdr.node_name, gasoplist[i].archive_name) == 0) {
		strcpy (host, gasoplist[i].gas_addr);
		success = 1;
		log_debug("process_gas_global: node_name=%s, hostaddr=%s",
			global.msg_hdr.node_name, gasoplist[i].gas_addr);
		break;
	}
} /* end dofor num_gas_hosts */


if (success) {
	/*  Is this a prime command?  */
	if (first_prime_cmd[scindex] == 1) {
		first_prime_cmd[scindex] = 0;
		cont = 1;
	} else {
		log_debug("prime_cmd[%d]=%s, global.prime_command=%s",
		scindex,prime_cmd[scindex], global.prime_command);
		log_debug("gtacs_name_len=%d, %s, nonprime_cmd[%d]=%s, global.non_prime_command=%s ",
				global.gtacs_name_len, global.gtacs_name, scindex,
				nonprime_cmd[scindex], global.non_prime_command);

		if (strcmp (prime_cmd[scindex], global.prime_command) == 0)
			cont = 0;
		else
			cont = 1;
	}

	if (cont) {
		/* save prime cmd "UPDx" */
		strcpy (prime_cmd[scindex], global.prime_command);

		if (strncmp (global.prime_command, "UPD", 3) == 0) {
			/* received start-prime cmd, Spawn SDR process(es) */
			log_event("received GAS Start Prime cmd=%s  node=%s  scid=%2.2s",
				global.prime_command,global.msg_hdr.node_name,global.scid);

			/* whenever the UPD changes, stop prime stream and restart ..Lata */
			for (j=1; j<8; j+=2) {
				if (sdr[j].active == 1) {
					if ((sdr[j].prime == 1) &&
						(strncmp (sdr[j].scid, global.scid, 2) == 0)){
							log_event("send terminate msg to Prime Stream SDR-%d",j);
							strcpy (zzzz, "zzzzzzzz");
							length = net_write (sdr_client_socket[j], (char *)&zzzz, strlen(zzzz));

						/* set prime status */
						prime[scindex].connected = 0;
					}
				}
			}

			/* restart the prime stream */
			prime[scindex].requested = 1;
			for (i=0; i< num_gtacs_hosts; i++) {

				for (k=0; k< num_rt_streams[i]; k++) {
					if (strncmp (prime[scindex].strm, goplist[i].rtstrm[k],strlen(goplist[i].rtstrm[k])) == 0) {
						if (goplist[i].operational == 1) {

							for (j=1; j<8; j+=2) {
								if (sdr[j].active == 0) {
									sprintf (str, "%d", j);
									sprintf(syscall, "./Stream_Data_Receiver p1 %s %s %s %s %s %s &",
										str,prime[scindex].strm,goplist[i].gtacs_host,global.scid,host,debug_arg);
									log_event("start Prime Stream SDR-%d for stream=%s",j,prime[scindex].strm);
									log_event("%s",syscall);
									IFM_spawn_task (syscall);
									sdr[j].active = 1;
									sdr[j].prime = 1;

									/* set prime status to connected */
									prime[scindex].connected = 1;
									/* update sdr table */
									strcpy (sdr[j].gasip, host);
									strcpy (sdr[j].gtacs, goplist[i].gtacs_host);
									strcpy (sdr[j].scid, global.scid);
									strcpy (sdr[j].stream, prime[scindex].strm);

									success3 = 1;
									IFM_update_status (1, global.scid);
									break;
								}
							}

						} else {
							/*  Send response to GAS  */
							IFM_send_gas_global (gas_index, global);
						}
						break;
					}
				} /* end dofor num_rt_streams */
				if (success3)  break;

			} /* end dofor num_gtacs_hosts */

		} else if (strncmp (global.prime_command, "STP", 3) == 0) {

				/* Received stop-prime command, term appropriate SDR process(es) */
				log_event("received GAS Stop Prime cmd=%s  node=%s  scid=%2.2s",
					global.prime_command,global.msg_hdr.node_name,global.scid);
				success2 = 0;
				prime[scindex].requested = 0;
				/* stop all sdr processes with specified scid */
				for (j=1; j<8; j+=2) {
					if (sdr[j].active == 1) {
						if ((sdr[j].prime == 1) &&
							(strncmp (sdr[j].scid, global.scid, 2) == 0)) {
							log_event("send terminate msg to Prime Stream SDR-%d",j);
							strcpy (zzzz, "zzzzzzzz");
							length = net_write (sdr_client_socket[j], (char *)&zzzz, strlen(zzzz));
							success2 = 1;

							/* set prime status to not-connected */
							prime[scindex].connected = 0;
							
						}
					}
				}

				if (success2 == 0)	{
					/*  There was no process to stop  */
					log_event("cannot process GAS Prime cmd=%s: not archiving scid=%2.2s",
						global.prime_command,global.scid);
				}

			} else {
				/*  Received invalid-prime command  */
				log_event_err("cannot process Prime cmd=%s from GAS: scid=%2.2s",
					global.prime_command,global.scid);
			}
		} /* endif cont check */

	/*  Is this a nonprime command?  */
	if (global.gtacs_name_len > 2) {		/*  NONPRIME COMMAND  */

		/* do not process if the non-prime id is all spaces */
		if (!strncmp(fifteen_spaces,global.non_prime_id,strlen(global.non_prime_id))) cont = 0; 
		/* process if the non-prime cmd has changed */
		else if (strcmp(nonprime_cmd[scindex],global.non_prime_command)) cont = 1;
		/* process if the non-prime id has changed */
		else if	(strcmp(nonprime_id[scindex],global.non_prime_id)) cont = 1;
		/* do not process otherwise */
		else cont = 0;	

		if (cont) {
			/* save nonprime cmd "UPDx" or "STPx" and id (streamname) */
			strcpy (nonprime_cmd[scindex], global.non_prime_command);
			strcpy (nonprime_id[scindex], global.non_prime_id);
			
			if (strncmp(global.non_prime_command, "UPD", 3) == 0) {
				
				/* output event msg if first time */
				if (first_nonprime_cmd[scindex]) log_event("received GAS Start NonPrime cmd=%s  node=%s  scid=%2.2s  stream=%s",
					global.non_prime_command,global.msg_hdr.node_name,global.scid,global.non_prime_id);

				strncpy(non_prime_stream, global.non_prime_id, 5);
				non_prime_stream[5]= '\0';
				status = connect_stream(non_prime_stream, client_timeout, &rts_connect_id);
				if (status != STRM_SUCCESS) {
					if (first_nonprime_cmd[scindex]) {
						log_event_err("process_gas_global: connect_stream(%s) err=%i (%s)",
							non_prime_stream,status,stream_err_messages[0-status]);
						/* disable connect error event msg */
						first_nonprime_cmd[scindex] = 0;
					}
					cont = 0;
					/* clear saved nonprime cmd "UPDx" */
					strcpy (nonprime_cmd[scindex], "    ");
					/* clear nonprime id (streamname) */ 
					nonprime_id[scindex][0] = 0; 
						
					if (rts_connect_id != 0) {
							disconnect_stream(rts_connect_id);
					}
				} else {
					/* stream connected, enable connect error event msg */ 
					first_nonprime_cmd[scindex] = 1;
				}
				
				if (cont) {
				
					/* Attempt to get the pid of the SC_504_02_ID global */
					sprintf(global_name,"%s:SC_504_02_ID",non_prime_stream);
					status = look_up_point(global_name,client_timeout, &spacecraft_id_pid);

					if (status != STRM_SUCCESS) {
						log_event_err("process_gas_global: look_up_point(%s) err=%i (%s)",
								global_name,status,stream_err_messages[0-status]);
						cont = 0;
					} else {
						/* Get current value of SC_504_02_ID */
						status = poll_point(spacecraft_id_pid);
						if (status != STRM_SUCCESS) {
							log_event_err("process_gas_global: poll_point(%s) err=%i (%s)",
											global_name,status,stream_err_messages[0-status]);
							cont = 0;
						} else {
							/* Get the current SCID status */
							sprintf(stream_scid,"%i",*(int *)spacecraft_id_pid->value);
							if (strncmp (global.scid, stream_scid, 2) != 0) {
								/*  Received invalid nonprime command  */
								log_event_err("stream=%s in NonPrime cmd is invalid for sc=%s",
									non_prime_stream, global.scid);
								cont = 0;
							} /* end check scid */

						} /* End poll_point status check */

					} /* end lookup_point status check */
					/* --- End Verify the SCID for the requested stream-- ARaj */
					if (rts_connect_id != 0) disconnect_stream(rts_connect_id);

				}
				
			} /* endif nonprime upd check */

			/*  continue processing nonprime UPD command  */
			if (cont && (strncmp(global.non_prime_command, "UPD", 3) == 0)) {

				/* bring down existing nonprime SDR processes for same sc */
				for (j=9; j<17; j++) {
					if (sdr[j].active == 1) {
						/*  Kill the currently running SDR process  */
						if ((sdr[j].prime == 0) && (strncmp (sdr[j].scid, global.scid, 2) == 0)) {
							log_event("send terminate msg to NonPrime Stream SDR-%d",j);
							strcpy (zzzz, "zzzzzzzz");
							length = net_write (sdr_client_socket[j], (char*)&zzzz, strlen(zzzz));
							break;
						}
					}
				}

				/* start new nonprime SDR processes for sc */
				for (i=0; i< num_gtacs_hosts; i++) {

					if (strncmp (global.gtacs_name, goplist[i].gtacs_host,strlen(goplist[i].gtacs_host)) == 0) {

						for (k=0; k< num_rt_streams[i]; k++) {
							if (strncmp (global.non_prime_id, goplist[i].rtstrm[k], strlen(goplist[i].rtstrm[k])) == 0) {
								if (goplist[i].operational == 1) {
									for (j=9; j<17; j++) {
										if (sdr[j].active == 0) {
											sprintf (str, "%d", j);
											sprintf(syscall, "./Stream_Data_Receiver np %s %s %s %s %s %s &",
												str,global.non_prime_id,global.gtacs_name,global.scid,host,debug_arg);
											/*log_event("start NonPrime Stream SDR-%d %s",j);*/
											log_event("start NonPrime Stream SDR-%d for stream=%s",j,global.non_prime_id);
											log_event("%s",syscall);
											IFM_spawn_task (syscall);

											sdr[j].active = 1;
											sdr[j].prime = 0;
											strcpy (sdr[j].gasip, host);
											strcpy (sdr[j].gtacs, global.gtacs_name);
											strcpy (sdr[j].scid, global.scid);
											strcpy (sdr[j].stream, global.non_prime_id);

											IFM_update_status (1, global.scid);
											break;
										}
									}
								} else {
									/*  Send response to GAS  */
									IFM_send_gas_global (gas_index, global);
								} /* endif operational check */
								break;
							}

						} /* end dofor num_rt_streams */
						break;
					}
				} /* end dofor num_gtacs_hosts */

			} /* endif cont check */

			else if (strncmp (global.non_prime_command, "STP", 3) == 0) {

				/*  received stop nonprime cmd, stop appropriate SDR process  */
				log_event("received GAS Stop NonPrime cmd=%s  node=%s  scid=%2.2s  stream=%s",
					global.non_prime_command,global.msg_hdr.node_name,global.scid,global.non_prime_id);
				success2 = 0;
				for (j=9; j<17; j++) {
					if (sdr[j].active == 1) {
						if ((sdr[j].prime == 0) &&
						 (strcmp (sdr[j].gtacs, global.gtacs_name) == 0) &&
						 (strncmp (sdr[j].scid, global.scid, 2) == 0) &&
						 (strncmp (sdr[j].stream, global.non_prime_id, strlen(sdr[j].stream)) == 0)) {
								log_event("send terminate msg to NonPrime Stream SDR-%d",j);
								strcpy (zzzz, "zzzzzzzz");
								length = net_write (sdr_client_socket[j], (char *)&zzzz, strlen(zzzz));
								success2 = 1;
								break;
						}
					}
				} /* end dofor j */
				if (success2 == 0){
					/*  error processing nonprime stop cmd  */
					log_event("cannot process GAS NonPrime cmd=%s: not archiving scid=%2.2s stream=%s",
						global.non_prime_command,global.scid,global.non_prime_id);
				}

			} else {
				/*  error processing nonprime start cmd  */
				if (first_nonprime_cmd[scindex]) log_event("cannot process GAS NonPrime cmd=%s scid=%2.2s stream=%s",
					global.non_prime_command,global.scid,global.non_prime_id);
			}
		} /* endif cont */
	} /* endif nonprime check */

} else {
	log_event_err("process_gas_global: cannot resolve GAS hostname=(%s)",global.msg_hdr.node_name);
	for (i=0; i< num_gas_hosts; i++) {
		log_event("gasoplist[%d].archive_name = (%s)",i,gasoplist[i].archive_name); 
	} /* end dofor num_gas_hosts */
} /* endif success */

/*  Free allocated memory for this message  */
free (global.msg_hdr.node_name);
free (global.msg_hdr.process_name);
if (global.gtacs_name_len > 0)  free (global.gtacs_name);
free (global.scid);
free (global.archive_id);
free (global.gateway_id);
if (global.non_prime_id_len > 0)  free (global.non_prime_id);
if (global.prime_cmd_len > 0)  free (global.prime_command);
if (global.non_prime_cmd_len > 0)  free (global.non_prime_command);

return;
}

