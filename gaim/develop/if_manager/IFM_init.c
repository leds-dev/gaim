/*  
@(#)  File Name: IFM_init.c  Release 1.8  Date: 07/03/25, 08:32:52
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_init.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         V2.0        04/02       A. Raj          UPDATE PDL
	 	 8.8         may06-2004  K. Dolinh       connected=0 means not-connected
		 11			 Mar14-2007                  removed gaimname init

********************************************************************************
         Invocation:
        
             IFM_init()
                         
********************************************************************************
         Functional Description:

         This routine initializes variables used by the N-Q_Interface_Manager.
	
*******************************************************************************/

/**
    Initialize variables needed by the N-Q_Interface_Manager
    Initialize GOES Archive server socket 
    Initialize GOES Archive client sockets
    Initialize GAIM CGT client socket
    Initialize GAIM SDR client sockets
    Initialize GAIM PUB client sockets


    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

void IFM_init (void)
{
int i;
char str[3];

client_timeout.tv_sec = 60;
client_timeout.tv_usec = 0; 
select_time.tv_sec  = 0;
/*select_time.tv_usec = 200000;*/		
select_time.tv_usec = 500000;		
FD_ZERO (&rd_mask);
first_gtacs_connection = 1;
mon_first_time = 1;
num_gas_hosts = 0;
num_gtacs_hosts = 0;
num_op_gtacs = 0;
signon_seq_num = 1;
global_seq_num = 1;
merge_seq_num = 1;
ping_seq_num = 1;

for (i=0; i< MAXNUMRT; i++)  num_rt_streams[i] = 0;
for (i=0; i< MAXNUMPB; i++)  num_pb_streams[i] = 0;
for (i=0; i< MAXNUMSIM; i++)  num_sim_streams[i] = 0;

for (i=0; i< MAXSCIDS; i++)
	{
	sprintf (str, "%d", i+13);
	strcpy (prime[i].sc, str);
	strcpy (prime[i].strm, "UNDEFINED");
	strcpy (prime[i].oldstrm, "UNDEFINED");
	prime[i].connected = 0;  /* not-connected */
	prime[i].requested = 0;  /* do-not-archive */
	first_prime_cmd[i] = 1;
	first_nonprime_cmd[i] = 1;
	automerge_flag[i] = 0;
	manmerge_flag[i] = 0;
	}

/*  Get the first time to be used in monitor and pinging calculations  */

mon_time1 = time(NULL);
ping_time1 = time(NULL);

/*  Initialize GOES Archive server socket  */

gas_server_socket = -1;

/*  Initialize GOES Archive client sockets  */

for (i=1; i< 9; i++)
	{
	gas_client_socket[i] = -1;
	gas_listening_socket[i] = -1;
	}

/*  Initialize GAIM CGT client socket  */

cgt_client_socket = -1;
cgt_listening_socket = -1;

/*  Initialize GAIM SDR client sockets  */

for (i=1; i< 17; i++)
	{
	sdr_client_socket[i] = -1;
	sdr_listening_socket[i] = -1;
	sdr[i].active = 0;
	sdr[i].prime = 0;
	strcpy (sdr[i].gasip, " ");
	strcpy (sdr[i].gtacs, " ");
	strcpy (sdr[i].scid, " ");
	strcpy (sdr[i].stream, " ");
	}

/*  Initialize GAIM PUB client sockets  */

for (i=17; i< 33; i++)
	{
	pub_client_socket[i] = -1;
	pub_listening_socket[i] = -1;
	pub[i].active = 0;
	pub[i].type = 0;
	strcpy (pub[i].gasip, " ");
	strcpy (pub[i].gtacs, " ");
	strcpy (pub[i].scid, " ");
	strcpy (pub[i].stream, " ");
	}
nfds = 0;

return;
}
