/*
@(#)  File Name: IFM_prototypes.h  
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         INCLUDE         :       IFM_prototypes.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       K. Dolinh

         VER.        DATE        BY              COMMENT

         11          Mar08-2007  K. Dolinh       Created
                     Jan03-2011  D. Niklewski    Changed prototype for
                                                 IFM_set_point_text                     
         
********************************************************************************
         Functional Description:

         This header file defines prototype of subroutines in
         the N-Q_Interface_Manager process.

*******************************************************************************/

#include <unistd.h>
#include "net_util.h"

struct GasMsgHeader {              /* GOES Archive Message Header */
    int message_size;
    short class_id;
    short message_number;
    short node_name_len;
    char *node_name;
    short process_name_len;
    char *process_name;
};
typedef struct GasMsgHeader GasMsgHeader;

struct gas_merge_vector {              /* Merge Data Request/Response */
    int start_time;
    int duration;
    struct gas_merge_vector *next;
};

struct GasMergeMsg {
    GasMsgHeader msg_hdr;
    short status;
    short gtacs_name_len;
    char *gtacs_name;
    short scid_len;
    char *scid;
    short stream_type_len;
    char *stream_type;
    short start_year;
    short start_day;
    short num_data_sections;
    struct gas_merge_vector *merge_vector;
    int gas_index;
};
typedef struct GasMergeMsg GasMergeMsg;

struct GasGlobalUpdateMsg {         /* Global Update/Status */
    GasMsgHeader  msg_hdr;
    short status;
    short gtacs_name_len;
    char *gtacs_name;
    short scid_len;
    char *scid;
    short archive_id_len;
    char *archive_id;
    short gateway_id_len;
    char *gateway_id;
    short non_prime_id_len;
    char *non_prime_id;
    short prime_cmd_len;
    char *prime_command;
    short non_prime_cmd_len;
    char *non_prime_command;
};
typedef struct GasGlobalUpdateMsg GasGlobalUpdateMsg; 

struct GasSignonMsg {                /* Sign On Request/Response */
    GasMsgHeader  msg_hdr;
    short state;
    short archive_name_len;
    char *archive_name;
    short status;
    short gtacs_name_len;
    char *gtacs_name;
};
typedef struct GasSignonMsg GasSignonMsg;

struct GasEventMsg {                 /* Event Message */
    GasMsgHeader  msg_hdr;
    short event_text_len;
    char *event_text;
    short time_tag_len;
    char *time_tag;
    short dest_tcs_len;
    char *dest_tcs;
    short scid_len;
    char *scid;
    short input_source;
    short event_id;
};
typedef struct GasEventMsg GasEventMsg;

struct ManualMergeQ {                /* Manual Merge Req Queue item */
    int     stat;
    int     gas_index;
    char    buffer[MAXBUFFLEN];
};
typedef struct ManualMergeQ ManualMergeQ;

void create_eventfile (void);
void log_event (char *fmt,...);
void log_event_err (char *fmt,...);
void log_debug (char *fmt,...);
void log_err   (char *fmt,...);
void log_daily_statistics (int year,int day);

void IFM_build_gas_global (int *index, GasGlobalUpdateMsg global, char *buffer);
void IFM_build_gas_merge (int type, int *index, GasMergeMsg msg, char *buf);
void IFM_build_gas_msghdr (int *index, char *buf, int t1, short t2, short t3, char *c5, char *c7);
void IFM_build_gas_signon (int g_host, int *index, GasSignonMsg sign_on, char *buffer);
void IFM_extract_gas_event (int *index, char *buffer, GasEventMsg *event);
void IFM_extract_gas_global (int *index, char *buffer, GasGlobalUpdateMsg *global);
void IFM_extract_gas_merge (int *index, char *buffer, GasMergeMsg *merge_data );
void IFM_extract_gas_msghdr (int *index, char *buffer, GasMsgHeader *msghdr);
void IFM_extract_gas_signon (int *index, char *buffer, GasSignonMsg *sign_on);
void IFM_handle_prime_change (char *scid);
void IFM_process_cgt_msg (void);
void IFM_process_gas_event (int gas_index, char *buffer);
void IFM_process_gas_global (int gas_index,char *buffer);
void IFM_process_gas_merge (int gas_index, char *buffer);
void IFM_process_gas_msg (int gas_index);
void IFM_process_gas_signon (int gas_index, char *buffer);
void IFM_process_pub_msg (int pub_index);
void IFM_process_sdr_msg (int sdr_index);
void IFM_send_event(int type, char *text); 
void IFM_send_gas_global (int gas_index, GasGlobalUpdateMsg global);
void IFM_send_gas_merge (int type, int gas_index, GasMergeMsg msg);
int  IFM_set_point_sarc_status(char *name, int value);
/*
int  IFM_set_point_text (char *stream, char *point, char *text);
*/
/********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
/* Commented out the above prototype for set_point_text and
   replaced it with the following:  */
int IFM_set_point_text (client_point *pointpid, char *text);
/******************** End Modification *********************/
void IFM_send_event(int type, char *text);
void IFM_spawn_task(char *syscall);
void IFM_update_status (int type, char *scid);
void IFM_write_status (void);
