/*  
@(#)  File Name: IFM_process_gas_merge.c  Release 1.10  Date: 07/03/25, 08:33:03
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_process_gas_merge.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         V2.0        05/02       A. Raj          Reset manmerge_flag upon error
         8.1         01/2004     K. Dolinh       Add log_event, log_debug
		 9.2         jan31-2005                  Send err msg to GAS if playback error
		 11			 Mar13-2007					 fix compile warning
		 
********************************************************************************
         Invocation:
        
             IFM_process_gas_merge (gas_index, buffer)
             
         where
         
             <gas_index> - I
                 is an integer indicating which GOES Archive process sent the
                 message.

             <buffer> - I
                 is a pointer to a character string containing the message
                         
********************************************************************************
         Functional Description:

         This routine processes merge request messsages received from the GOES 
         Archive Server.
	
*******************************************************************************/

/**
    CALL IFM_extract_gas_msghdr
    CALL IFM_extract_gas_merge
    IF this is an auto merge THEN
        IF auto merge flag is not set THEN
            set auto merge flag
        ELSE
            CALL IFM_send_gas_merge
            write log message
            RETURN
        ENDIF
    ELSE
        If manual merge flag is not set THEN
            set manual merge flag
        ELSE
            CALL IFM_send_gas_merge
            write log message
            RETURN
        ENDIF
    ENDIF
    DOFOR each GOES Archive server
        IF merge message node name matches this GOES Archive server THEN
            get host name
            set success flag to true
            BREAK
        ENDIF
    ENDDO
    IF success flag is set to true THEN
        DOFOR each possible PUB process
            IF this PUB process is not active THEN
                DOFOR each GTACS
                    IF merge message GTACS matches this GTACS THEN
                        IF this GTACS is operational THEN
                            DOFOR each simulated stream
                                IF merge message stream matches this stream THEN
                                    set match flag to true
                                    BREAK
                                ENDIF
                            ENDDO
                            BREAK
                        ELSE
                            CALL IFM_send_gas_merge
                            RETURN
                        ENDIF
                    ENDIF
                ENDDO
                IF match flag is set to true THEN
                    build parameters needed to spawn PUB process
                    CALL IFM_spawn_task
                    write log message
                    build PUB structure
                    CALL IFM_update_status
                    CALL IFM_send_gas_merge
                    BREAK
                ENDIF
            ENDIF
        ENDDO
    ELSE
        write log message
    ENDIF
    free all of the allocated memory for this message
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern void IFM_print_merge_data(char *text, int index, GasMergeMsg m);

void IFM_process_gas_merge (
		int gas_index,
		char *buffer )

{
	int i, j, k, l, index = 0;
	int success = 0;
	int match, scidnum, status;
	char host [MAXHOSTLEN];
	char msgtxt [MAXMSGTEXT];
	char syscall [MAXMSGTEXT];
	char str [3];
	char str2 [2];
	char str3 [5];
	char str4 [4];
	char str5 [2];
	char tmpgrnd [MAXSTRMLEN];
	GasMergeMsg merge_data;
	
	/*  Extract the fields in the message header  */
	IFM_extract_gas_msghdr (&index, buffer, &merge_data.msg_hdr);
	
	log_event("received GAS Merge Message from %s",merge_data.msg_hdr.node_name);
  				
	sprintf (msgtxt,"    Merge message header structure is: ");
	log_debug(msgtxt);
	sprintf (msgtxt,"        %d %d %d %d %s %d %s",
		merge_data.msg_hdr.message_size, merge_data.msg_hdr.class_id, 
		merge_data.msg_hdr.message_number, merge_data.msg_hdr.node_name_len, 
		merge_data.msg_hdr.node_name, merge_data.msg_hdr.process_name_len, 
		merge_data.msg_hdr.process_name);
	log_debug(msgtxt);
	
	/*  Extract the fields in the message body  */
	IFM_extract_gas_merge (&index, buffer, &merge_data);
	
	/*  Only allow 1 automerge request and 1 manual merge request per spacecraft  */

	if (strncmp (merge_data.scid, "13", 2) == 0)  scidnum = 0;
	else if (strncmp (merge_data.scid, "14", 2) == 0)  scidnum = 1;
	else if (strncmp (merge_data.scid, "15", 2) == 0)  scidnum = 2;
	else if (strncmp (merge_data.scid, "16", 2) == 0)  scidnum = 3;
	else if (strncmp (merge_data.scid, "17", 2) == 0)  scidnum = 4;
	else if (strncmp (merge_data.scid, "18", 2) == 0)  scidnum = 5;
	else if (strncmp (merge_data.scid, "19", 2) == 0)  scidnum = 6;
	else if (strncmp (merge_data.scid, "20", 2) == 0)  scidnum = 7;
	else  return;
	
	if (strncmp (merge_data.msg_hdr.process_name, "GaAutoMerge", 11) == 0) {
		strcpy (str2, "A");
		if (automerge_flag[scidnum] == 0) {
			automerge_flag[scidnum] = 1;
		} else {
			/*  Send a rejected response back to the GOES Archive Server  */
			IFM_send_gas_merge (3, gas_index, merge_data);
			strcpy (msgtxt, "rejected auto merge request from GOES Archive");
			log_event(msgtxt);
			return;
		}
		
	} else {
		strcpy (str2, "M");		
		if (manmerge_flag[scidnum] == 0) {
			manmerge_flag[scidnum] = 1;
		} else {
			/*  Send a rejected response back to the GOES Archive Server  
			IFM_send_gas_merge (3, gas_index, merge_data);
			strcpy (msgtxt, "rejected manual merge request from GOES Archive");*/
			
			/* queue this request */
			for (i=0; i<MANUAL_MERGE_MAX; i++) {
				if (MMergeQ[i].stat == 0) {
					MMergeQ[i].stat = 1;
					MMergeQ[i].gas_index = gas_index;
					memcpy ((char *)(&MMergeQ[i].buffer), buffer, MAXBUFFLEN);
					log_event("Manual Merge is busy. Request(%d) has been queued",i+1);
					return;
				}
			}
			IFM_send_gas_merge (3,gas_index,merge_data);
			log_event("Manual Merge Queue is full. Manual Merge Request rejected");		
			return;
		}
	}
	
	for (k=0; k< num_gas_hosts; k++) {
		if (strcmp (merge_data.msg_hdr.node_name, gasoplist[k].archive_name) == 0) {
			strcpy (host, gasoplist[k].gas_addr);
			success = 1;
			break;
		}
	}
		
	/*  Spawn the Playback_Update_Buffer process to handle this request  */

	if (success)
		{	
		for (j=17; j<33; j++)
			{
			if (pub[j].active == 0)
				{
				sprintf (str, "%d", j);
				sprintf (str3, "%d", merge_data.start_year);
				sprintf (str4, "%d", merge_data.start_day);
				match = 0;
				for (k=0; k< num_gtacs_hosts; k++)
					{
					if (strcmp (merge_data.gtacs_name, goplist[k].gtacs_host) == 0)
						{
						if (goplist[k].operational == 1)
							{
							strcpy (tmpgrnd, goplist[k].grndstrm);
							for (l=0; l< num_sim_streams[k]; l++)
								{
								if (strcmp (merge_data.stream_type, 
							    		goplist[k].simstrm[l]) == 0)
									{
									match = 1;
									break;
									}
								}
							break;
							}
						else
							{
							/* Send a no connect response back to the GOES Archive Server  */
							IFM_send_gas_merge (GAS_MERGE_STAT_NOCONNECT, gas_index, merge_data);

                     /* Reset the auto and manual merge flags - ARaj -5/20/02 */
                     if (strncmp (merge_data.msg_hdr.process_name, "GaAutoMerge", 11) == 0)
                        {
			               automerge_flag[scidnum] = 0;
                        }
                     else  /* manual merge */
                        {
			               manmerge_flag[scidnum] = 0;
                        }
							return;
							}  /* connection error check */
						}
					} /* end for num gtacs */
			
				if (match)  strcpy (str5, "S");
				else  strcpy (str5, "R");
					
				strcpy (syscall, "./Playback_Update_Buffer");
				strcat (syscall, " -a ");
				strcat (syscall, host);
				strcat (syscall, " -d gaim_pub.log -g ");
				strcat (syscall, merge_data.gtacs_name);
				strcat (syscall, " -m ");
				strcat (syscall, str2);
				strcat (syscall, " -pt ");
				strcat (syscall, str3);
				strcat (syscall, "/");
				strcat (syscall, str4);
				strcat (syscall, " -r nqim -i ");
				strcat (syscall, merge_data.scid);
				strcat (syscall, " -t ");
				strcat (syscall, merge_data.stream_type);
				strcat (syscall, " -c ");
				strcat (syscall, tmpgrnd);
				strcat (syscall, " -v ");
				strcat (syscall, str);
				strcat (syscall, " -z ");
				strcat (syscall, str5);
				strcat (syscall, " &");
				IFM_spawn_task (syscall);
			
				strcpy (msgtxt, "spawning process ");
				strcat (msgtxt, syscall);
				log_event(msgtxt);
				
				pub[j].active = 1;
				if (strcmp (str2, "A") == 0)  pub[j].type = 2;
				else  pub[j].type = 3;
				strcpy (pub[j].gasip, host);
				strcpy (pub[j].gtacs, merge_data.gtacs_name);
				strcpy (pub[j].scid, merge_data.scid);
				strcpy (pub[j].stream, merge_data.stream_type);
								
				IFM_update_status (pub[j].type, merge_data.scid);
				
				/* save merge data to use if sending err msg to GAS later */
				merge_data.gas_index = gas_index;
				pub_merge_data[j] = merge_data;
														
				/*  Send a good response back to the GOES Archive Server  */
				/* IFM_send_gas_merge (0, gas_index, merge_data); */
				IFM_print_merge_data("PROCESS_GAS_MERGE",j,pub_merge_data[j]);
				IFM_send_gas_merge (GAS_MERGE_STAT_CONNECT,gas_index, pub_merge_data[j]);
				break;
				}
			}  /* end for loop */
		} /* end if success check */
	else
		{
		strcpy (msgtxt, "process_gas_merge: cannot resolve GAS hostname=");
		strcat (msgtxt, merge_data.msg_hdr.node_name);
		log_event_err(msgtxt);		
		}				
		
	/*  Free all of the allocated memory for this message 
	    NOTE: commented out. this will now be done by IFM_process_pub_msg	
	free (merge_data.msg_hdr.node_name);
	free (merge_data.msg_hdr.process_name);
	free (merge_data.gtacs_name);
	free (merge_data.scid);
	free (merge_data.stream_type);	
	*/
		
	return;
}
