/*  
@(#)  File Name: IFM_extract_gas_event.c  Release 1.6  Date: 07/03/25, 08:32:45
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_extract_gas_event.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	 	 8.1       mar16-2004    K. Dolinh       add log_event, log_debug
		 11		   Mar13-2007					 fix compile warning
	 
********************************************************************************
         Invocation:
        
             IFM_extract_gas_event (index, buffer, event)
             
         where
         
             <index> - O
             	 is a pointer to an integer containing the index into the buffer
             <buffer> - I
                 is a pointer to a character string containing the message
             <event> - O
                 is a pointer to a structure containing the whole message
                         
********************************************************************************
         Functional Description:

         This routine extracts the body fields in an event messsage received
         from the GOES Archive Server.
	
*******************************************************************************/

/**
    extract the fields in the event message body
    RETURN
**/
    
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern char *IFM_swap_bytes (int);

void IFM_extract_gas_event (
		int *index,
		char *buffer,
		GasEventMsg *event)

{
long lnum;
short snum;
short unk1, unk2;
char msgtxt[256];
char *c;

/* printf ("event message body is %8.8X %8.8X %8.8X %8.8X %8.8X %8.8X\n",
		*(int *)(&buffer[*index]),*(int *)(&buffer[*index+4]),
		*(int *)(&buffer[*index+8]),*(int *)(&buffer[*index+12]),
		*(int *)(&buffer[*index+16]),*(int *)(&buffer[*index+20]));	
printf ("event message body is %8.8X %8.8X %8.8X %8.8X %8.8X %8.8X\n",
		*(int *)(&buffer[*index+24]),*(int *)(&buffer[*index+28]),
		*(int *)(&buffer[*index+32]),*(int *)(&buffer[*index+36]),
		*(int *)(&buffer[*index+40]),*(int *)(&buffer[*index+44])); */	

/*  Extract the fields in the message body  */

memcpy ((char *)(&snum), &buffer[*index], 2);
event->event_text_len = snum;
*index += 2;

event->event_text = (char *)calloc(snum+1,sizeof(char));	/* extract the  */
memcpy (event->event_text, &buffer[*index], snum);		/*  event text  */
*index += snum;

memcpy ((char *)(&snum), &buffer[*index], 2);
event->time_tag_len = snum;
*index += 2;

event->time_tag = (char *)calloc(snum+1,sizeof(char));		/* extract the  */
memcpy (event->time_tag, &buffer[*index], snum);		/*   time tag   */
*index += snum;

memcpy ((char *)(&snum), &buffer[*index], 2);
event->dest_tcs_len = snum;
*index += 2;

if (event->dest_tcs_len > 0)
	{		
	event->dest_tcs = (char *)calloc(snum+1,sizeof(char));	/* extract the  */
	memcpy (event->dest_tcs, &buffer[*index], snum);	/*   dest tcs   */
	*index += snum;
	}

memcpy ((char *)(&snum), &buffer[*index], 2);
event->scid_len = snum;
*index += 2;

if (event->scid_len > 0)
	{
	event->scid = (char *)calloc(snum+1,sizeof(char));	/* extract the  */
	memcpy (event->scid, &buffer[*index], snum);		/*    scid      */
	*index += snum;
	}

memcpy ((char *)(&snum), &buffer[*index], 2);
event->input_source = snum;
*index += 2;

memcpy ((char *)(&snum), &buffer[*index], 2);
event->event_id = snum;
*index += 2;

if (event->scid_len > 0)
        {
	sprintf (msgtxt,"    Event message body structure is: ");
	log_debug(msgtxt);
	sprintf (msgtxt,"    %d %s %d %s %d %d %s %d %d",
		event->event_text_len, event->event_text, 
		event->time_tag_len, event->time_tag,
		event->dest_tcs_len, event->scid_len, event->scid,
		event->input_source, event->event_id);
	log_debug(msgtxt);
        }
else
        {		
	sprintf (msgtxt,"    Event message body structure is: ");
	log_debug(msgtxt);
	sprintf (msgtxt,"    %d %s %d %s %d %d %d %d",
		event->event_text_len, event->event_text, 
		event->time_tag_len, event->time_tag,
		event->dest_tcs_len, event->scid_len,
		event->input_source, event->event_id);
	log_debug(msgtxt);
	}

return;
}
