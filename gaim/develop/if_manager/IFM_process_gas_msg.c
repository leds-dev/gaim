/*
@(#)  File Name: IFM_process_gas_msg.c  Release 1.6  Date: 07/03/25, 08:33:04
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_process_gas_msg.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
------------------------------------------------------------------------------
         V1.0        02/00       B. Hageman      Initial creation
         V2.0        04/02       A. Raj          Reject gas messages if GAS
                                                 machine can't be pinged
		 8.1         01/2004     K. Dolinh       Add log_event, log_debug
		 11			 mar13-2007                  fix compile warning

********************************************************************************
         Invocation:

             IFM_process_gas_msg (gas_index)

         where

             <gas_index> - I
                 is an integer indicating which GOES Archive process sent the
                 message.

********************************************************************************
         Functional Description:

         This routine processes messsages received from the GOES Archive Server.

*******************************************************************************/

/**
    read the data from the socket
    IF the data length is greater than 0 THEN
        IF GAS machine is operational THEN
           get the class id
           DOCASE class id
           CASE 1 event message
               CALL IFM_process_gas_event
           CASE 2 merge message
               CALL IFM_process_gas_merge
           CASE 3 global message
               CALL IFM_process_gas_global
           CASE 4 signon message
               CALL IFM_process_gas_signon
           ELSE
               write log message
           ENDDO
       ELSE

    ELSE
        shutdown and close the GOES Archive server socket
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern char *IFM_swap_bytes (int);

void IFM_process_gas_msg (int gas_index)
{

	char buffer [MAXBUFFLEN];
	char msgtxt [MAXMSGTEXT];
	int length;
	short classid;
	char tpclass [4];
	char *c;

	/*  Read the data from the socket  */

	length = net_read (gas_client_socket[gas_index],(char *)&buffer, MAXNBUFLEN);

	if (length > 0)
		{
	/*	printf ("\nReceived %d bytes from GAS\n",length);
		printf ("    %8.8X %8.8X %8.8X %8.8X %8.8X %8.8X\n",
			*(int *)(&buffer[0]),*(int *)(&buffer[4]),
			*(int *)(&buffer[8]),*(int *)(&buffer[12]),
			*(int *)(&buffer[16]),*(int *)(&buffer[20])); */

		/*  Determine the class id and process accordingly  */
/*
sprintf(msgtxt,"Received %d bytes from GAS",length);
log_debug(msgtxt);
sprintf(msgtxt,"    %8.8X %8.8X %8.8X %8.8X %8.8X %8.8X",
                        *(int *)(&buffer[0]),*(int *)(&buffer[4]),
                        *(int *)(&buffer[8]),*(int *)(&buffer[12]),
                        *(int *)(&buffer[16]),*(int *)(&buffer[20]));
log_debug(msgtxt);
*/

		memcpy ((char *)(&classid), &buffer[4], 2);
		sprintf (tpclass, "%d", classid);

      /* ARAJ 4-16-02 make sure GAS is operational before processing the request*/
/*      if (gasoplist[gas_index].operational == 1)  /* ARAJ 4-16-02*/
 /*     {*/

 		/*log_event("received msg from GAS (classID=%s)",tpclass);*/

		   switch (classid)
			   {
			   case GAS_EVENT_ID:
				   IFM_process_gas_event (gas_index, buffer);
				   break;
			   case GAS_MERGE_ID:
				   IFM_process_gas_merge (gas_index, buffer);
				   break;
			   case GAS_GLOBAL_ID:
				   IFM_process_gas_global (gas_index, buffer);
				   break;
			   case GAS_SIGNON_ID:
				   IFM_process_gas_signon (gas_index, buffer);
				   break;
			   default:
				   log_event("%s Did not process msg from GAS (Unknown ID=%d)",ERRMSG,classid);

			   }  /* end switch */

      /*} /* end if gasoplist[gas...  - ARAJ*/
/*      else
      {   strcpy (msgtxt, "GOES Archive is not operational or IFM can not ping it");
          log_event(msgtxt);
      } /* end else - ARAJ */

		}

   	shutdown (gas_client_socket[gas_index], 2);
	close (gas_client_socket[gas_index]);
	FD_CLR (gas_client_socket[gas_index], &rd_mask);
	gas_client_socket[gas_index] = -1;

	return;
}
