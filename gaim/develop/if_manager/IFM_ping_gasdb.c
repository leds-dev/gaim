/*  
@(#)  File Name: IFM_ping_gasdb.c  Release 1.4  Date: 04/03/17, 19:20:51
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_ping_gasdb.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	 	 8.1       01/2004       K. Dolinh       Add log_event, log_debug
		 11		   Mar13-2007	 				 fix compile warning

********************************************************************************
         Invocation:
        
             IFM_ping_gasdb ()
                                      
********************************************************************************
         Functional Description:

         This routine pings the GOES Archive Server database machine.
	
*******************************************************************************/

/**
    build the ping message
    CALL IFM_build_gas_msghdr
    DOFOR each GOES Archive server
        IF GOES Archive server is operational THEN
            send the ping message to the server
            IF ping is successful THEN
                shutdown and close the GOES Archive server socket
            ENDIF
            BREAK
        ENDIF
    ENDDO
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern char *IFM_swap_bytes(int);
extern void IFM_onintr_timer (int);

void IFM_ping_gasdb (void)
{
	short cid, mnum;
	int i, index, length, call_status, status;
	long msize;
	char nname [MAXHOSTLEN];
	char proc [MAXPROCLEN];
	char buffer [MAXBUFFLEN];
	char host [MAXHOSTLEN];
	char msgtxt [MAXMSGTEXT];
	char ping_addr [MAXHOSTLEN];
	char ping_host [MAXHOSTLEN];
	char ping_service [MAXSERVLEN];
	char str [3];
	char cseqnum [6];
	char *c;
	struct itimerval timer;
	
	timer.it_value.tv_sec = 2;
	timer.it_value.tv_usec = 0;
	timer.it_interval.tv_sec = 0;
	timer.it_interval.tv_usec = 0;
	
	/*  Build the ping message  */
	
	msize = 1;
	cid = GAS_PING_ID;
	mnum = ping_seq_num;
	strcpy (nname, (char *)net_host ("localhost"));
	strcpy (proc, IFM_PROC_NAME);
	IFM_build_gas_msghdr (&index, &buffer[0], msize, cid, mnum, nname, proc);
	
	/*  Update the value for the message size in the buffer  */
	
	long_data = index - 4;
	memcpy (&buffer[0], (char *)(&long_data), 4);
	
	/*  Ping the GOES Archive Server Database machine  */
	
	for (i=0; i< num_gas_hosts; i++)
		{
		if (gasoplist[i].operational == 1)
			{
			strcpy (ping_addr, gasoplist[i].gas_db_addr);
			strcpy (ping_host, gasoplist[i].gas_db_host);
			strcpy (ping_service, GAS_PING);

			signal (SIGALRM, IFM_onintr_timer);

			if (setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL))
				{
				log_event("IFM_ping_gasdb: set itimer failed");
				}
		
 			if (net_call (ping_addr, ping_service, &gas_server_socket)) 
				{
				timer.it_value.tv_sec = 0;
				timer.it_value.tv_usec = 0;
				timer.it_interval.tv_sec = 0;
				timer.it_interval.tv_usec = 0;
				setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL);
				
				strcpy (msgtxt, "IFM_ping_gasdb: failed to connect to GOES Archive DB machine on ");
				strcat (msgtxt, ping_host);
				log_event(msgtxt);
				}
			else 
				{
				timer.it_value.tv_sec = 0;
				timer.it_value.tv_usec = 0;
				timer.it_interval.tv_sec = 0;
				timer.it_interval.tv_usec = 0;
				setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL);
				
  				length = net_write (gas_server_socket, (char *)&buffer, index);
  				
 				strcpy (msgtxt, "IFM sent Ping Message # ");
  				sprintf (cseqnum, "%d", ping_seq_num);
  				strcat (msgtxt, cseqnum);
  				strcat (msgtxt, " to GOES Archive DB machine on ");
  				strcat (msgtxt, ping_host);
				log_debug(msgtxt);
			
  				shutdown (gas_server_socket, 2); 
 				close (gas_server_socket);
 				gas_server_socket = -1; 		
				}		
			break;
			}
		}
				
	return;
}
