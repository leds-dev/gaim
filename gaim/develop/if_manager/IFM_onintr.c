/*
@(#)  File Name: IFM_onintr.c  Release 1.7  Date: 07/03/25, 08:32:55
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_onintr.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         8.1       01/2004       K. Dolinh       Add log_event, log_debug
         11        Aug11-2006                    - use IFM_send_event
                   Jan03-2011    D. Niklewski    Modified the calls to
                                                 IFM_set_point_text

********************************************************************************
         Invocation:

             IFM_onintr ()

********************************************************************************
         Functional Description:

         This routine shuts down the N-Q_Interface_Manager process cleanly.

*******************************************************************************/

/**
    shutdown and close sockets
    DOFOR each GTACS
        IF GTACS is operational THEN
            send event message to GTACS
            set GTACS global variable
        ENDIF
    ENDDO
    write log message
    EXIT
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include <IFM_params.h>
#include "IFM_struct.h"
#include <IFM_varsex.h>

extern void IFM_close_sockets (void);

void IFM_onintr (int sig)
{
int i, status;
int scid;
char msgtxt [MAXMSGTEXT];
char pointname[MNEMONIC_SIZE];

/* send shutdown message to gtacs ground streams */
IFM_send_event(EVENT_WARN,"GAIM is shutting down");

/*  Shutdown and close sockets  */
IFM_close_sockets();

/*  Send GTACS event message and set global to indicate GAIM is going down  */

for (i=0; i< num_gtacs_hosts; i++) {

    if (goplist[i].operational == 1)
        {
        sprintf(pointname,"%s_STATE",gaimname);
        sprintf(msgtxt,"%s DOWN",gaimname);
        /*
        IFM_set_point_text(goplist[i].grndstrm,pointname,msgtxt);
        */
        /********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
        /* IFM_set_point_text was modified to prevent "look_up_point" from
           being called every time (look_up_point allocates memory that
           needs to be freed - the cause of the memory leak in
           GIR SSGS-1227 - and it's very slow, especially to non-local
           GTACS's. */

        status = IFM_set_point_text(gaim_state_pid[i], msgtxt);

        /******************** End Modification *********************/

        /*
        for (scid=13; scid<=20; scid++) {
            sprintf(pointname,"%s_STATS_SC%d_PRIME",gaimname,scid);
            IFM_set_point_text(goplist[i].grndstrm,pointname,"           ");            
            sprintf(pointname,"%s_STATS_SC%d_NPRIME",gaimname,scid);
            IFM_set_point_text(goplist[i].grndstrm,pointname,"           ");            
        }
        */
        /********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
        /* IFM_set_point_text was modified to prevent "look_up_point" from
           being called every time (look_up_point allocates memory that
           needs to be freed - the cause of the memory leak in
           GIR SSGS-1227 - and it's very slow, especially to non-local
           GTACS's. */

        status = IFM_set_point_text(sc13_prime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc14_prime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc15_prime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc16_prime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc17_prime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc18_prime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc19_prime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc20_prime_stats_pid[i], "           ");

        status = IFM_set_point_text(sc13_nprime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc14_nprime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc15_nprime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc16_nprime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc17_nprime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc18_nprime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc19_nprime_stats_pid[i], "           ");
        status = IFM_set_point_text(sc20_nprime_stats_pid[i], "           ");

        /******************** End Modification *********************/

    }
}

status = flush_sets ();
if (status != STRM_SUCCESS)
    {
    strcpy (msgtxt, "IFM GTACS global flush_sets failure");
    strcat (msgtxt, goplist[i].gtacs_host);
    log_event(msgtxt);
    }

/*  Send message indicating N-Q_Interface_Manager exiting  */
log_event("GAIM Interface Manager is terminating");
printf ("\nGAIM Interface Manager is terminating\n\n");

exit (0);
}
