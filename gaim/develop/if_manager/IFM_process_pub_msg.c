/*  
@(#)  File Name: IFM_process_pub_msg.c  Release 1.8  Date: 07/03/25, 08:33:06
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_process_pub_msg.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	     8.1         01/2004     K. Dolinh       Add log_event, log_debug
		 9.2         jan31-2005                  Send err msg to GAS if playback error
		 11			 Mar13-2007					 fix compile warning
		 										 changed to GAIM_EVTNUM
												 process Manual Merge Queue
		 
********************************************************************************
         Invocation:
        
             IFM_process_pub_msg (pub_index)
             
         where
         
             <pub_index> - I
                 is an integer indicating which Playback_Update_Buffer (17-32)
                 sent the message.
                         
********************************************************************************
         Functional Description:

         This routine processes messsages received from the 
         Playback_Update_Buffer process.
	
*******************************************************************************/

/**
    read the data from the socket
    IF the data length is greater than 7 THEN
        IF the first 8 characters are "zzzzzzzz" THEN
            write log message
            shutdown and close the PUB socket
            reset the PUB structure
            CALL IFM_update_status
        ENDIF
        IF the first 8 characters are "z!z!z!z!" THEN
            write log message
            shutdown and close the PUB socket
            reset the PUB structure
            CALL IFM_update_status
            DOFOR each GTACS
                IF GTACS is operational THEN
                    send a critical event message to GTACS
                ENDIF
            ENDDO
        ENDIF
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern void IFM_print_merge_data(char *text, int index, GasMergeMsg m);

void IFM_process_pub_msg (
		int pub_index)

{
char str [3];
char buffer [MAXBUFFLEN];
char msgtxt [MAXMSGTEXT];
int i, length, scidnum;

if (strncmp (pub[pub_index].scid, "13", 2) == 0)  scidnum = 0;
else if (strncmp (pub[pub_index].scid, "14", 2) == 0)  scidnum = 1;
else if (strncmp (pub[pub_index].scid, "15", 2) == 0)  scidnum = 2;
else if (strncmp (pub[pub_index].scid, "16", 2) == 0)  scidnum = 3;
else if (strncmp (pub[pub_index].scid, "17", 2) == 0)  scidnum = 4;
else if (strncmp (pub[pub_index].scid, "18", 2) == 0)  scidnum = 5;
else if (strncmp (pub[pub_index].scid, "19", 2) == 0)  scidnum = 6;
else if (strncmp (pub[pub_index].scid, "20", 2) == 0)  scidnum = 7;

/*  Read the data from the socket  */

length = net_read (pub_client_socket[pub_index], (char *)&buffer, MAXNBUFLEN);

if (length > 7) {

	/*  Did the PUB process complete?  */
	if (strncmp (buffer, "zzzzzzzz", 8) == 0) {
	
		sprintf (str, "%d", pub_index);
		strcpy (msgtxt, "IFM/PUB socket #");
		strcat (msgtxt, str);
		strcat (msgtxt, " is being closed - job completed");
		log_event(msgtxt);

		/*  Shutdown, close, and clear the socket  */
		shutdown (pub_client_socket[pub_index], 2);
		close (pub_client_socket[pub_index]);
		FD_CLR (pub_client_socket[pub_index], &rd_mask);
		pub_client_socket[pub_index] = -1;

		/*  Reset the pub structure  */
		pub[pub_index].active = 0;
		if (pub[pub_index].type == 2)  automerge_flag[scidnum] = 0;
		else  manmerge_flag[scidnum] = 0;

		/*  Update the GTACS global variable  */
		IFM_update_status (pub[pub_index].type, pub[pub_index].scid);
	}

	/*  Did the PUB process fail?  */
	if (strncmp (buffer, "z!z!z!z!", 8) == 0) {
	
		log_event("Closed IFM/PUB socket # %d because playback failed",pub_index);

		/* send error message to GAS */
		IFM_print_merge_data("PROCESS_PUB_MSG",pub_index,pub_merge_data[pub_index]);
		IFM_send_gas_merge (GAS_MERGE_STAT_PUB_ERROR, 
			pub_merge_data[pub_index].gas_index, 
			pub_merge_data[pub_index]);

		/*  Shutdown, close, and clear the socket  */
		shutdown (pub_client_socket[pub_index], 2);
		close (pub_client_socket[pub_index]);
		FD_CLR (pub_client_socket[pub_index], &rd_mask);
		pub_client_socket[pub_index] = -1;

		/*  Reset the pub structure  */
		pub[pub_index].active = 0;
		if (pub[pub_index].type == 2)  automerge_flag[scidnum] = 0;
		else  manmerge_flag[scidnum] = 0;

		/*  Update the GTACS global variable  */
		IFM_update_status (pub[pub_index].type, pub[pub_index].scid);

		/*  Send a critical event message to all operational GTACS  */
		for (i=0; i< num_gtacs_hosts; i++) {
			if (goplist[i].operational == 1) {
				strcpy (msgtxt, "@");
				strcat (msgtxt, goplist[i].grndstrm);
				strcat (msgtxt, " The streaming of merge data (");
				strcat (msgtxt, pub[pub_index].stream);
				strcat (msgtxt, ") from ");
				strcat (msgtxt, pub[pub_index].gtacs);
				strcat (msgtxt, " to the GOES Archive failed");
				send_event (GAIM_EVTNUM_ERR, msgtxt, client_timeout);
			}
		}
	}
	/* free space used by merge request */
	free (pub_merge_data[pub_index].msg_hdr.node_name);
	free (pub_merge_data[pub_index].msg_hdr.process_name);
	free (pub_merge_data[pub_index].gtacs_name);
	free (pub_merge_data[pub_index].scid);
	free (pub_merge_data[pub_index].stream_type);	
	
	/* if merge done was manual merge, process a queued manual merge request */
	if (manmerge_flag[scidnum] = 0) {
	
		for (i=0; i<MANUAL_MERGE_MAX; i++) {
			if (MMergeQ[i].stat != 0) 	{
				MMergeQ[i].stat != 0;
				IFM_process_gas_merge(MMergeQ[i].gas_index, (char *)&MMergeQ[i].buffer);
				break;
			}
		}
		
	}		
	
}

return;
}
