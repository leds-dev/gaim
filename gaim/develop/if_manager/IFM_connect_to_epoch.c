/*
@(#)  File Name: IFM_connect_to_epoch.c  Release 1.9  Date: 07/03/25, 08:32:44
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_connect_to_epoch.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0       02/00        B. Hageman      Initial creation
         8.4       mar16-2004    K. Dolinh       Add log_event, log_debug
         8.8.1     may10-2004                    reformat only
         8.B       sep17-2004                    GTACS build 8B 
         11        Aug11-2006                    remove send_event
                   Jan03-2011    D. Niklewski    added globals for statistics
                                                 and GAIM state and added calls
                                                 to cancel_point before calling
                                                 look_up_point (to prevent
                                                 memory leaks)

********************************************************************************
         Invocation:

             IFM_connect_to_epoch ()

********************************************************************************
         Functional Description:

         This routine establishes a client socket with any operational EPOCH.

*******************************************************************************/

/**
    DOFOR each GTACS
        IF GTACS is not operational but is available THEN
            try to connect to GTACS ground stream
            IF cannot connect to GTACS ground stream THEN
                write log message
            ELSE
                lookup GTACS global variables
                IF any lookup fails THEN
                    write log message
                    disconnect from GTACS ground stream
                ELSE
                    write log message
                    send GTACS an event message
                    set GTACS global variables
                ENDIF
            ENDIF
        ENDIF
    ENDDO
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

/********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
extern int set_point_usr (client_point *pointpid);
/******************** End Modification *********************/

void IFM_connect_to_epoch (void)
{
int i, k, length, index = 0;
int status, status1, status2, status3, status4, status5;
int status6, status7, status8, status9, status10;
int status11, status12, status13, status14, status15;
int status16, status17, status18, status19, status20;
int status21, status22, status23, status24, status25;
int status26, status27, status28, status29, status30;
int status31, status32, status33, status34, status35;
int status36, status37, status38, status39, status40, status41;

/********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
/*              For statistics globals                     */

int status42, status43, status44, status45, status46;
int status47, status48, status49, status50, status51;
int status52, status53, status54, status55, status56;
int status57, status58;

char stats [MAXBUFFLEN];

/******************** End Modification *********************/

char msgtxt [MAXMSGTEXT];
char statchar [4];
/*
char strm [MAXSTRMLEN];
*/
/********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
/* Modified the above declaration of "strm", since MAXSTRMLEN
   (21) is too small for, e.g., "s1_ge:GAIM01_ARCH_AM13", which
   is 23 characters (including a final null).  The lines:
        strcpy (strm, goplist[i].grndstrm);
        strcat (strm,":GAIM01_ARCH_AM13");
   will go out-of-bounds and overwrite memory at strm[21]
   and strm[22].  In this case, it will probably only overwrite
   talmsg[0] and talmsg[1] (which is never used), but this is
   not certain since the behavior is undefined.  */

char strm [MAXBUFFLEN];

/******************** End Modification *********************/

char talmsg [MAXBUFFLEN];
for (i=0; i< num_gtacs_hosts; i++) {

        if ((goplist[i].operational == 0) && (goplist[i].available == 1)) {
        
                /*  Try to connect to GTACS ground stream  */

                if (first_gtacs_connection == 1)
                        log_debug("trying to connect to first available GTACS ... grndstrm = %s", goplist[i].grndstrm);

                status = connect_stream (goplist[i].grndstrm, client_timeout,&gs_connect_id[i]);

                if (status != STRM_SUCCESS) {
                
                        goplist[i].operational = 0;
                        /* added next line wrt. issue P-371 ..Lata
                        strcpy (prime[i].strm, "UNDEFINED"); */
                        
                } else {
                        /*  Look up GTACS global variables  */

                        /********* Added 01/03/2011 by D. Niklewski (NOAA) **********/
                        /* Modified all of the "look_up_point" calls below to first */
                        /* call "cancel_point".  If the memory allocated for the    */
                        /* previous structure isn't freed first (by calling         */
                        /* cancel_point), there will be a memory leak.              */
                        /* NOTE: These pointers are initialized to NULL in IFM_main */
                        /* and cancel_point will just return an error code if it's  */
                        /* called on a NULL pointer, so it won't cause a problem.   */
                        /******************** End Modification **********************/

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":GAIM_MON_RATE");
                        cancel_point (gaim_mon_rate_pid[i]);
                        status = look_up_point (strm, client_timeout,
                                &gaim_mon_rate_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SC13_PRIME");
                        cancel_point (sc13_prime_pid[i]);
                        status1 = look_up_point (strm, client_timeout,
                                &sc13_prime_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SC14_PRIME");
                        cancel_point (sc14_prime_pid[i]);
                        status2 = look_up_point (strm, client_timeout,
                                &sc14_prime_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SC15_PRIME");
                        cancel_point (sc15_prime_pid[i]);
                        status3 = look_up_point (strm, client_timeout,
                                &sc15_prime_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SC16_PRIME");
                        cancel_point (sc16_prime_pid[i]);
                        status4 = look_up_point (strm, client_timeout,
                                &sc16_prime_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SC17_PRIME");
                        cancel_point (sc17_prime_pid[i]);
                        status22 = look_up_point (strm, client_timeout,
                                &sc17_prime_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SC18_PRIME");
                        cancel_point (sc18_prime_pid[i]);
                        status23 = look_up_point (strm, client_timeout,
                                &sc18_prime_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SC19_PRIME");
                        cancel_point (sc19_prime_pid[i]);
                        status24 = look_up_point (strm, client_timeout,
                                &sc19_prime_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SC20_PRIME");
                        cancel_point (sc20_prime_pid[i]);
                        status25 = look_up_point (strm, client_timeout,
                                &sc20_prime_pid[i]);

                        if (strcmp (gaimname, "GAIM1") == 0)
                                {
                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_STATUS");
                                cancel_point (gaim_status_pid[i]);
                                status5 = look_up_point (strm, client_timeout,
                                        &gaim_status_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_RT13");
                                cancel_point (archive_rt_13_pid[i]);
                                status10 = look_up_point (strm, client_timeout,
                                        &archive_rt_13_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_RT14");
                                cancel_point (archive_rt_14_pid[i]);
                                status11 = look_up_point (strm, client_timeout,
                                        &archive_rt_14_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_RT15");
                                cancel_point (archive_rt_15_pid[i]);
                                status12 = look_up_point (strm, client_timeout,
                                        &archive_rt_15_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_RT16");
                                cancel_point (archive_rt_16_pid[i]);
                                status13 = look_up_point (strm, client_timeout,
                                        &archive_rt_16_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_RT17");
                                cancel_point (archive_rt_17_pid[i]);
                                status26 = look_up_point (strm, client_timeout,
                                        &archive_rt_17_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_RT18");
                                cancel_point (archive_rt_18_pid[i]);
                                status27 = look_up_point (strm, client_timeout,
                                        &archive_rt_18_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_RT19");
                                cancel_point (archive_rt_19_pid[i]);
                                status28 = look_up_point (strm, client_timeout,
                                        &archive_rt_19_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_RT20");
                                cancel_point (archive_rt_20_pid[i]);
                                status29 = look_up_point (strm, client_timeout,
                                        &archive_rt_20_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_AM13");
                                cancel_point (archive_am_13_pid[i]);
                                status14 = look_up_point (strm, client_timeout,
                                        &archive_am_13_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_AM14");
                                cancel_point (archive_am_14_pid[i]);
                                status15 = look_up_point (strm, client_timeout,
                                        &archive_am_14_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_AM15");
                                cancel_point (archive_am_15_pid[i]);
                                status16 = look_up_point (strm, client_timeout,
                                        &archive_am_15_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_AM16");
                                cancel_point (archive_am_16_pid[i]);
                                status17 = look_up_point (strm, client_timeout,
                                        &archive_am_16_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_AM17");
                                cancel_point (archive_am_17_pid[i]);
                                status30 = look_up_point (strm, client_timeout,
                                        &archive_am_17_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_AM18");
                                cancel_point (archive_am_18_pid[i]);
                                status31 = look_up_point (strm, client_timeout,
                                        &archive_am_18_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_AM19");
                                cancel_point (archive_am_19_pid[i]);
                                status32 = look_up_point (strm, client_timeout,
                                        &archive_am_19_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_AM20");
                                cancel_point (archive_am_20_pid[i]);
                                status33 = look_up_point (strm, client_timeout,
                                        &archive_am_20_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_MM13");
                                cancel_point (archive_mm_13_pid[i]);
                                status18 = look_up_point (strm, client_timeout,
                                        &archive_mm_13_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_MM14");
                                cancel_point (archive_mm_14_pid[i]);
                                status19 = look_up_point (strm, client_timeout,
                                        &archive_mm_14_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_MM15");
                                cancel_point (archive_mm_15_pid[i]);
                                status20 = look_up_point (strm, client_timeout,
                                        &archive_mm_15_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_MM16");
                                cancel_point (archive_mm_16_pid[i]);
                                status21 = look_up_point (strm, client_timeout,
                                        &archive_mm_16_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_MM17");
                                cancel_point (archive_mm_17_pid[i]);
                                status34 = look_up_point (strm, client_timeout,
                                        &archive_mm_17_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_MM18");
                                cancel_point (archive_mm_18_pid[i]);
                                status35 = look_up_point (strm, client_timeout,
                                        &archive_mm_18_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_MM19");
                                cancel_point (archive_mm_19_pid[i]);
                                status36 = look_up_point (strm, client_timeout,
                                        &archive_mm_19_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM01_ARCH_MM20");
                                cancel_point (archive_mm_20_pid[i]);
                                status37 = look_up_point (strm, client_timeout,
                                        &archive_mm_20_pid[i]);
                                }
                        else
                                {
                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_STATUS");
                                cancel_point (gaim_status_pid[i]);
                                status5 = look_up_point (strm, client_timeout,
                                        &gaim_status_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_RT13");
                                cancel_point (archive_rt_13_pid[i]);
                                status10 = look_up_point (strm, client_timeout,
                                        &archive_rt_13_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_RT14");
                                cancel_point (archive_rt_14_pid[i]);
                                status11 = look_up_point (strm, client_timeout,
                                        &archive_rt_14_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_RT15");
                                cancel_point (archive_rt_15_pid[i]);
                                status12 = look_up_point (strm, client_timeout,
                                        &archive_rt_15_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_RT16");
                                cancel_point (archive_rt_16_pid[i]);
                                status13 = look_up_point (strm, client_timeout,
                                        &archive_rt_16_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_RT17");
                                cancel_point (archive_rt_17_pid[i]);
                                status26 = look_up_point (strm, client_timeout,
                                        &archive_rt_17_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_RT18");
                                cancel_point (archive_rt_18_pid[i]);
                                status27 = look_up_point (strm, client_timeout,
                                        &archive_rt_18_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_RT19");
                                cancel_point (archive_rt_19_pid[i]);
                                status28 = look_up_point (strm, client_timeout,
                                        &archive_rt_19_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_RT20");
                                cancel_point (archive_rt_20_pid[i]);
                                status29 = look_up_point (strm, client_timeout,
                                        &archive_rt_20_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_AM13");
                                cancel_point (archive_am_13_pid[i]);
                                status14 = look_up_point (strm, client_timeout,
                                        &archive_am_13_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_AM14");
                                cancel_point (archive_am_14_pid[i]);
                                status15 = look_up_point (strm, client_timeout,
                                        &archive_am_14_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_AM15");
                                cancel_point (archive_am_15_pid[i]);
                                status16 = look_up_point (strm, client_timeout,
                                        &archive_am_15_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_AM16");
                                cancel_point (archive_am_16_pid[i]);
                                status17 = look_up_point (strm, client_timeout,
                                        &archive_am_16_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_AM17");
                                cancel_point (archive_am_17_pid[i]);
                                status30 = look_up_point (strm, client_timeout,
                                        &archive_am_17_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_AM18");
                                cancel_point (archive_am_18_pid[i]);
                                status31 = look_up_point (strm, client_timeout,
                                        &archive_am_18_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_AM19");
                                cancel_point (archive_am_19_pid[i]);
                                status32 = look_up_point (strm, client_timeout,
                                        &archive_am_19_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_AM20");
                                cancel_point (archive_am_20_pid[i]);
                                status33 = look_up_point (strm, client_timeout,
                                        &archive_am_20_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_MM13");
                                cancel_point (archive_mm_13_pid[i]);
                                status18 = look_up_point (strm, client_timeout,
                                        &archive_mm_13_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_MM14");
                                cancel_point (archive_mm_14_pid[i]);
                                status19 = look_up_point (strm, client_timeout,
                                        &archive_mm_14_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_MM15");
                                cancel_point (archive_mm_15_pid[i]);
                                status20 = look_up_point (strm, client_timeout,
                                        &archive_mm_15_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_MM16");
                                cancel_point (archive_mm_16_pid[i]);
                                status21 = look_up_point (strm, client_timeout,
                                        &archive_mm_16_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_MM17");
                                cancel_point (archive_mm_17_pid[i]);
                                status34 = look_up_point (strm, client_timeout,
                                        &archive_mm_17_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_MM18");
                                cancel_point (archive_mm_18_pid[i]);
                                status35 = look_up_point (strm, client_timeout,
                                        &archive_mm_18_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_MM19");
                                cancel_point (archive_mm_19_pid[i]);
                                status36 = look_up_point (strm, client_timeout,
                                        &archive_mm_19_pid[i]);

                                strcpy (strm, goplist[i].grndstrm);
                                strcat (strm,":GAIM02_ARCH_MM20");
                                cancel_point (archive_mm_20_pid[i]);
                                status37 = look_up_point (strm, client_timeout,
                                        &archive_mm_20_pid[i]);
                                }

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SARC13_STATUS");
                        cancel_point (sarc13_status_pid[i]);
                        status6 = look_up_point (strm, client_timeout,
                                &sarc13_status_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SARC14_STATUS");
                        cancel_point (sarc14_status_pid[i]);
                        status7 = look_up_point (strm, client_timeout,
                                &sarc14_status_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SARC15_STATUS");
                        cancel_point (sarc15_status_pid[i]);
                        status8 = look_up_point (strm, client_timeout,
                                &sarc15_status_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SARC16_STATUS");
                        cancel_point (sarc16_status_pid[i]);
                        status9 = look_up_point (strm, client_timeout,
                                &sarc16_status_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SARC17_STATUS");
                        cancel_point (sarc17_status_pid[i]);
                        status38 = look_up_point (strm, client_timeout,
                                &sarc17_status_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SARC18_STATUS");
                        cancel_point (sarc18_status_pid[i]);
                        status39 = look_up_point (strm, client_timeout,
                                &sarc18_status_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SARC19_STATUS");
                        cancel_point (sarc19_status_pid[i]);
                        status40 = look_up_point (strm, client_timeout,
                                &sarc19_status_pid[i]);

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm,":SARC20_STATUS");
                        cancel_point (sarc20_status_pid[i]);
                        status41 = look_up_point (strm, client_timeout,
                                &sarc20_status_pid[i]);

                        /********* Added 01/03/2011 by D. Niklewski (NOAA) *********/

                        /* First get the prefix "s1_ge:GAIM1", etc., as "strm" */

                        strcpy (strm, goplist[i].grndstrm);
                        strcat (strm, ":");
                        strcat (strm, gaimname);

                        strcpy (stats, strm);
                        strcat (stats, "_STATE");
                        cancel_point (gaim_state_pid[i]);
                        status42 = look_up_point (stats, client_timeout,
                                &gaim_state_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC13_PRIME");
                        cancel_point (sc13_prime_stats_pid[i]);
                        status43 = look_up_point (stats, client_timeout,
                                &sc13_prime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC13_NPRIME");
                        cancel_point (sc13_nprime_stats_pid[i]);
                        status44 = look_up_point (stats, client_timeout,
                                &sc13_nprime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC14_PRIME");
                        cancel_point (sc14_prime_stats_pid[i]);
                        status45 = look_up_point (stats, client_timeout,
                                &sc14_prime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC14_NPRIME");
                        cancel_point (sc14_nprime_stats_pid[i]);
                        status46 = look_up_point (stats, client_timeout,
                                &sc14_nprime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC15_PRIME");
                        cancel_point (sc15_prime_stats_pid[i]);
                        status47 = look_up_point (stats, client_timeout,
                                &sc15_prime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC15_NPRIME");
                        cancel_point (sc15_nprime_stats_pid[i]);
                        status48 = look_up_point (stats, client_timeout,
                                &sc15_nprime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC16_PRIME");
                        cancel_point (sc16_prime_stats_pid[i]);
                        status49 = look_up_point (stats, client_timeout,
                                &sc16_prime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC16_NPRIME");
                        cancel_point (sc16_nprime_stats_pid[i]);
                        status50 = look_up_point (stats, client_timeout,
                                &sc16_nprime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC17_PRIME");
                        cancel_point (sc17_prime_stats_pid[i]);
                        status51 = look_up_point (stats, client_timeout,
                                &sc17_prime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC17_NPRIME");
                        cancel_point (sc17_nprime_stats_pid[i]);
                        status52 = look_up_point (stats, client_timeout,
                                &sc17_nprime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC18_PRIME");
                        cancel_point (sc18_prime_stats_pid[i]);
                        status53 = look_up_point (stats, client_timeout,
                                &sc18_prime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC18_NPRIME");
                        cancel_point (sc18_nprime_stats_pid[i]);
                        status54 = look_up_point (stats, client_timeout,
                                &sc18_nprime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC19_PRIME");
                        cancel_point (sc19_prime_stats_pid[i]);
                        status55 = look_up_point (stats, client_timeout,
                                &sc19_prime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC19_NPRIME");
                        cancel_point (sc19_nprime_stats_pid[i]);
                        status56 = look_up_point (stats, client_timeout,
                                &sc19_nprime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC20_PRIME");
                        cancel_point (sc20_prime_stats_pid[i]);
                        status57 = look_up_point (stats, client_timeout,
                                &sc20_prime_stats_pid[i]);

                        strcpy (stats, strm);
                        strcat (stats, "_STATS_SC20_NPRIME");
                        cancel_point (sc20_nprime_stats_pid[i]);
                        status58 = look_up_point (stats, client_timeout,
                                &sc20_nprime_stats_pid[i]);


                        /******************** End Modification *********************/

                        if ((status != STRM_SUCCESS) || (status1 != STRM_SUCCESS) ||
                            (status2 != STRM_SUCCESS) || (status3 != STRM_SUCCESS) ||
                            (status4 != STRM_SUCCESS) || (status5 != STRM_SUCCESS) ||
                            (status6 != STRM_SUCCESS) || (status7 != STRM_SUCCESS) ||
                            (status8 != STRM_SUCCESS) || (status9 != STRM_SUCCESS) ||
                            (status10 != STRM_SUCCESS) || (status11 != STRM_SUCCESS) ||
                            (status12 != STRM_SUCCESS) || (status13 != STRM_SUCCESS) ||
                            (status14 != STRM_SUCCESS) || (status15 != STRM_SUCCESS) ||
                            (status16 != STRM_SUCCESS) || (status17 != STRM_SUCCESS) ||
                            (status18 != STRM_SUCCESS) || (status19 != STRM_SUCCESS) ||
                            (status20 != STRM_SUCCESS) || (status21 != STRM_SUCCESS) ||
                            (status22 != STRM_SUCCESS) || (status23 != STRM_SUCCESS) ||
                            (status24 != STRM_SUCCESS) || (status25 != STRM_SUCCESS) ||
                            (status26 != STRM_SUCCESS) || (status27 != STRM_SUCCESS) ||
                            (status28 != STRM_SUCCESS) || (status29 != STRM_SUCCESS) ||
                            (status30 != STRM_SUCCESS) || (status31 != STRM_SUCCESS) ||
                            (status32 != STRM_SUCCESS) || (status33 != STRM_SUCCESS) ||
                            (status34 != STRM_SUCCESS) || (status35 != STRM_SUCCESS) ||
                            (status36 != STRM_SUCCESS) || (status37 != STRM_SUCCESS) ||
                            (status38 != STRM_SUCCESS) || (status39 != STRM_SUCCESS) ||
/*
                            (status40 != STRM_SUCCESS) || (status41 != STRM_SUCCESS))  {}
*/
                        /********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
                            (status40 != STRM_SUCCESS) || (status41 != STRM_SUCCESS) ||
                            (status42 != STRM_SUCCESS) || (status43 != STRM_SUCCESS) ||
                            (status44 != STRM_SUCCESS) || (status45 != STRM_SUCCESS) ||
                            (status46 != STRM_SUCCESS) || (status47 != STRM_SUCCESS) ||
                            (status48 != STRM_SUCCESS) || (status49 != STRM_SUCCESS) ||
                            (status50 != STRM_SUCCESS) || (status51 != STRM_SUCCESS) ||
                            (status52 != STRM_SUCCESS) || (status53 != STRM_SUCCESS) ||
                            (status54 != STRM_SUCCESS) || (status55 != STRM_SUCCESS) ||
                            (status56 != STRM_SUCCESS) || (status57 != STRM_SUCCESS) ||
                            (status58 != STRM_SUCCESS))  {
                        /******************** End Modification *********************/

                                log_event_err("initial look_up_point failed for GTACS global var");
                                goplist[i].operational = 0;
                                /* added next line wrt. issue P-371 ..Lata
                                strcpy (prime[i].strm, "UNDEFINED"); */
                                if (gs_connect_id[i] != 0)
                                        disconnect_stream (gs_connect_id[i]);
                                        
                                } else {
                                
                                /*  Everything is OK, we have an operational GTACS  */
                                goplist[i].operational = 1;
                                goplist[i].available = 1;
                                num_op_gtacs += 1;

                                log_event("%s connected to %s",gaimname,goplist[i].gtacs_host);

                                *(long *)gaim_status_pid[i]->value = 1;
                                gaim_status_pid[i]->status_bits = 0;
                                /*
                                status = set_point (gaim_status_pid[i]);
                                */
                                /********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
                                /* Use set_point_usr instead of set_point because, unlike
                                   set_point, it checks the "destroyed" flag (calling
                                   set_point on a destroyed connection will cause a crash
                                   and core dump) */
                                status = set_point_usr (gaim_status_pid[i]);
                                /******************** End Modification *********************/
                                if (status != STRM_SUCCESS)
                                        log_event_err("initial set_point failed for GAIMnn_STATUS");

                                *(long *)gaim_mon_rate_pid[i]->value = gaim_monitor_rate;
                                gaim_mon_rate_pid[i]->status_bits = 0;
                                /*
                                status = set_point (gaim_mon_rate_pid[i]);
                                */
                                /********* Added 01/03/2011 by D. Niklewski (NOAA) *********/
                                /* Use set_point_usr instead of set_point because, unlike
                                   set_point, it checks the "destroyed" flag (calling
                                   set_point on a destroyed connection will cause a crash
                                   and core dump) */
                                status = set_point_usr (gaim_mon_rate_pid[i]);
                                /******************** End Modification *********************/
                                if (status != STRM_SUCCESS)
                                        log_event_err("initial set_point failed for GAIM_MON_RATE");

                                status = flush_sets ();
                                if (status != STRM_SUCCESS)
                                        {
                                        log_event_err("initial flush_sets failed for %s",goplist[i].gtacs_host);
                                        }

                                for (k=1; k< 4; k++) {  /*  Update all of the data  */
                                                        /*      archive states      */
                                        IFM_update_status (k, "13");
                                        IFM_update_status (k, "14");
                                        IFM_update_status (k, "15");
                                        IFM_update_status (k, "16");
                                        IFM_update_status (k, "17");
                                        IFM_update_status (k, "18");
                                        IFM_update_status (k, "19");
                                        IFM_update_status (k, "20");
                                }
                                
                                /* The first time in this routine, we only want to connect to one GTACS
                                so that we don't waste time trying to connect to unavailable GTACS */
                                if (first_gtacs_connection == 1)           {                            
                                        first_gtacs_connection = 0;
                                        goplist[i].available = 1;   
                                        break;                           
                                }                               
                        }
                }
        }
                
} /* enddo num_gtacs_hosts */

return;
}
