/*  
@(#)  File Name: IFM_build_gas_signon.c  Release 1.4  Date: 07/03/25, 08:32:43
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_build_gas_signon.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
		 11			 Mar13-2007					 Fix compiler warning

********************************************************************************
         Invocation:
        
             IFM_build_gas_signon (g_host, index, sign_on, buffer)
             
         where
         
             <g_host> - I
                 is the GTACS host number (0 - MAXGTACSHOSTS)
             <index> - O
             	 is a pointer to an integer containing the index into the buffer
             <sign_on> - I
                 is the structure containing the received signon message
             <buffer> - O
                 is a pointer to a character string containing the message
                         
********************************************************************************
         Functional Description:

         This routine builds a signon response message.
	
*******************************************************************************/

/**
    build the signon message header
    CALL IFM_build_gas_msghdr
    build the signon message body
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern char *IFM_swap_bytes(int);

void IFM_build_gas_signon (
		int g_host,
		int *index,
		GasSignonMsg sign_on,
		char *buffer)

{
	short cid, mnum;
	int i, tmpstat;
	long msize;
	char nname [MAXHOSTLEN];
	char proc [MAXPROCLEN];
	char *c;
	
	/*  Build the message header  */
	
	msize = 1;
	cid = GAS_SIGNON_ID;
	mnum = signon_seq_num;
	strcpy (nname, sign_on.msg_hdr.node_name);
	/* strcpy (nname, (char *)net_host ("localhost")); */
	strcpy (proc, sign_on.msg_hdr.process_name);
	/* strcpy (proc, IFM_PROC_NAME); */
	IFM_build_gas_msghdr (index, &buffer[0], msize, cid, mnum, nname, proc);
	
	/*  Build the message body  */
	
	short_data = sign_on.state;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	short_data = sign_on.archive_name_len;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	memcpy ((&buffer[*index]), sign_on.archive_name, sign_on.archive_name_len);
	*index += sign_on.archive_name_len;
	
	if (goplist[g_host].operational == 0)
		{
		short_data = 1;
		tmpstat = 1;
		}
	else
		{
		short_data = 0;
		tmpstat = 0;
		}

	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;
	
	short_data = strlen (goplist[g_host].gtacs_host);
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;
	
	memcpy ((&buffer[*index]), goplist[g_host].gtacs_host, strlen (goplist[g_host].gtacs_host));
	*index += strlen (goplist[g_host].gtacs_host);
	
	/*  Update the value for the message size in the buffer  */
	
	long_data = *index - 4;
	memcpy (&buffer[0], (char *)(&long_data), 4);
		
	/* printf ("\n    Signon response header structure is: \n");
	printf ("        %d %d %d %s %s\n",
		long_data, cid, mnum, nname, proc);
	
	printf ("    Signon/signoff response body structure is: \n");
	printf ("        %d %d %s %d %d %s\n",
		sign_on.state, sign_on.archive_name_len, sign_on.archive_name, tmpstat, 
		strlen (goplist[g_host].gtacs_host), goplist[g_host].gtacs_host); */

	return;
}
