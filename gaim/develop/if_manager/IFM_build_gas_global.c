/*  
@(#)  File Name: IFM_build_gas_global.c  Release 1.4  Date: 07/03/25, 08:32:39
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_build_gas_global.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
		 11			 Mar13-2007					 fix compiler warning

********************************************************************************
         Invocation:
        
             IFM_build_gas_global (index, global, buffer)
             
         where
         
             <index> - O
             	 is a pointer to an integer containing the index into the buffer
             <global> - I
                 is the structure containing the received global message
             <buffer> - O
                 is a pointer to a character string containing the message
                         
********************************************************************************
         Functional Description:

         This routine builds a global status message.
	
*******************************************************************************/

/**
    build the global status message header
    CALL IFM_build_gas_msghdr
    build the global status message body
    CALL IFM_swap_bytes
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern char *IFM_swap_bytes(int);

void IFM_build_gas_global (
		int *index,
		GasGlobalUpdateMsg global,
		char *buffer)

{
	short cid, mnum;
	int i;
	long msize;
	char nname [MAXHOSTLEN];
	char proc [MAXPROCLEN];
	char *c;
	
	/*  Build the message header  */
	
	msize = 1;
	cid = GAS_GLOBAL_ID;
	mnum = global_seq_num;
	strcpy (nname, global.msg_hdr.node_name);
	/* strcpy (nname, (char *)net_host ("localhost")); */
	strcpy (proc, global.msg_hdr.process_name);
	/* strcpy (proc, IFM_PROC_NAME); */
	IFM_build_gas_msghdr (index, &buffer[0], msize, cid, mnum, nname, proc);
	
	/*  Build the message body  */
	
	short_data = 0;					/*  No GTACS connection  */
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	short_data = global.gtacs_name_len;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	if (global.gtacs_name_len > 0)
		{
		memcpy ((&buffer[*index]), global.gtacs_name, global.gtacs_name_len);
		*index += global.gtacs_name_len;
		}
	
	short_data = global.scid_len;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	memcpy ((&buffer[*index]), global.scid, global.scid_len);
	*index += global.scid_len;
	
	short_data = global.archive_id_len;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	memcpy ((&buffer[*index]), global.archive_id, global.archive_id_len);
	*index += global.archive_id_len;
	
	short_data = global.gateway_id_len;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	memcpy ((&buffer[*index]), global.gateway_id, global.gateway_id_len);
	*index += global.gateway_id_len;
	
	short_data = global.non_prime_id_len;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	if (global.non_prime_id_len > 0)
		{
		memcpy ((&buffer[*index]), global.non_prime_id, global.non_prime_id_len);
		*index += global.non_prime_id_len;
		}
	
	short_data = global.prime_cmd_len;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	if (global.prime_cmd_len > 0)
		{
		memcpy ((&buffer[*index]), global.prime_command, global.prime_cmd_len);
		*index += global.prime_cmd_len;
		}
	
	short_data = global.non_prime_cmd_len;
	memcpy (&buffer[*index], (char *)(&short_data), 2);
	*index += 2;

	if (global.non_prime_cmd_len > 0)
		{
		memcpy ((&buffer[*index]), global.non_prime_command, global.non_prime_cmd_len);
		*index += global.non_prime_cmd_len;
		}
	
	/*  Update the value for the message size in the buffer  */
	
	long_data = *index - 4;
	memcpy (&buffer[0], (char *)(&long_data), 4);
		
	return;
}
