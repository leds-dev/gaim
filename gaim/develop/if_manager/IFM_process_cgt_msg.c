/*  
@(#)  File Name: IFM_process_cgt_msg.c  Release 1.8  Date: 07/03/25, 08:33:00
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_process_cgt_msg.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	     8.1       01/2004       K. Dolinh       Add log_event, log_debug
	     8.8.1     may10-2004                    reformat & debug
	     8.B       sep17-2004                    GTACS build 8B
	     9.2       feb02-2005                    parse multiple msgs in buffer
		 11		   mar12-2007				     fix compile warning

********************************************************************************
         Invocation:
        
             IFM_process_cgt_msg ()
                                      
********************************************************************************
         Functional Description:

         This routine processes messsages received from the Check_GTACS
         process.
	
*******************************************************************************/

/**
    read the data from the socket
    IF the data length is greater than 9 THEN
        IF the first 6 characters are "GTACS" THEN
            IF GTACS is "UP" THEN
                set GTACS to available
                write log message
            ELSE
                set GTACS to not available
                write log message
                DOFOR each possible nonprime SDR process
                    IF SDR process is active THEN
                        IF SDR process is for this GTACS then
                            kill this SDR process
                        ENDIf
                    ENDIF
                ENDDO
                DOFOR each possible PUB process
                    IF PUB process is active THEN
                        IF PUB process is for this GTACS then
                            kill this PUB process
                        ENDIf
                    ENDIF
                ENDDO
            ENDIF
        ENDIF
    ENDIf
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

#define MSGLEN 10

void IFM_process_cgt_msg (void)
{
int i, j, index, length;
char *buffer_adr;
char buffer [MAXBUFFLEN];
char msgtxt [MAXMSGTEXT];
char condition [3];
char str [3];
char zzzz [MAXBUFFLEN];
char *p;

/*
 * buffer has format: "GTACS 0 UP GTACS 1 UP GTACS 2 UP ..."
 */

/*  Read the data from the socket  */

length = net_read (cgt_client_socket, (char *)&buffer, MAXNBUFLEN);
if (length > 0) log_event("process_cgt_msg: len=%d  msg=(%s)",length,&buffer);

buffer_adr = &buffer[0];

while  (strlen(buffer_adr) > MSGLEN) {

	if (strncmp (buffer_adr, "GTACS ", 6) == 0) {
		p = strtok (buffer_adr, " ");
		p = strtok ('\0'," ");
		strcpy (str, p);
		index = atoi (str);
		p = strtok ('\0'," ");
		strcpy (condition, p);
		if (strncmp (condition, "UP", 2) == 0) {
			goplist[index].available = 1;
			strcpy (msgtxt, "process_cgt_msg: ");
			strcat (msgtxt, goplist[index].gtacs_host);
			strcat (msgtxt, " is up");
			log_event(msgtxt);
		} else {
			goplist[index].available = 0;
			goplist[index].operational = 0;
			/* added next line wrt issue P-371 
			strcpy (prime[index].strm, "UNDEFINED"); */
			if (gs_connect_id[index] != 0)
				disconnect_stream (gs_connect_id[index]);
			strcpy (msgtxt, "process_cgt_msg: ");
			strcat (msgtxt, goplist[index].gtacs_host);
			strcat (msgtxt, " is down");
			log_event(msgtxt);

			/*  Stop all currently running processes getting data from this GTACS  */
			/*  Don't stop prime streams, just wait for new prime GTACS to be defined  */

			for (j=9; j<17; j++) {
				if (sdr[j].active == 1) {
					if (strcmp (sdr[j].gtacs, goplist[index].gtacs_host) == 0) {
					    	strcpy (zzzz, "zzzzzzzz");
					    	length = net_write (sdr_client_socket[j], (char *)&zzzz, strlen(zzzz));
					}
				}
			}
			
			for (j=17; j<33; j++) {
				if (pub[j].active == 1){
					if (strcmp (pub[j].gtacs, goplist[index].gtacs_host) == 0){
					    	strcpy (zzzz, "zzzzzzzz");
					    	length = net_write (pub_client_socket[j],(char *)&zzzz, strlen(zzzz));
					}
				}
			}

		}
	}
	
	/* increment buffer to next msg if buffer contains multiple msgs */
	buffer_adr = buffer_adr + MSGLEN + 1;
}

return;
}
