/*
@(#)  File Name: IFM_handle_prime_change.c  Release 1.11  Date: 07/03/25, 08:32:51
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_handle_prime_change.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         8.1       01/2004       K. Dolinh       Add log_event, log_debug
         8.1       mar04-2004    K. Dolinh       ...
         		   mar26-2004
	 8.6       apr02-2004
	 8.8       may05-2004	 K.Dolinh	 fix prime change
	           may06-2004                    .connected=0 means not-connected 
		                                 send normal-shutdown to sdr 
	 8.8.1E    jun17-2004                    do not call log_event each time
	 8.B       sep17-2004                    GTACS build 8B   
	 	11	   Mar13-2007				 fix compile warning         

********************************************************************************
         Invocation:

             IFM_handle_prime_change (scid)

         where

             <scid> - I
                 is a pointer to a character string containing the sc id

********************************************************************************
         Functional Description:

         This routine handles changes in the prime streams.

*******************************************************************************/

/**
    DOFOR each possible prime SDR process
        IF SDR process is active THEN
            IF this SDR process matches THEN
                kill the SDR process
                write log message
                DOFOR each GTACS
                    FOFOR each realtime stream
                        IF prime stream is the same as this realtime stream THEN
                            DOFOR each possible prime SDR process
                                IF this SDR process is not active THEN
                                    determine parameters needed to spawn prime SDR
                                    CALL IFM_spawn_task
                                    write log message
                                    set success flag to true
                                    CALL IFM_update_status
                                    BREAK
                                ENDIF
                            ENDDO
                        ENDIF
                        BREAK
                    ENDDO
                ENDDO
                IF success flag is set to true THEN
                    BREAK
                ENDIF
            ENDIF
            BREAK
        ENDIF
    ENDDO
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern char *debug_arg;

void IFM_handle_prime_change (char* scid)
{
int i, j, k, n, length, scidnum, status;
int success = 0;
char gtname [MAXHOSTLEN];
char host [MAXHOSTLEN];
char msgtxt [MAXMSGTEXT];
char syscall [MAXMSGTEXT];
char str [3], tmpstr [3];
char zzzz [MAXBUFFLEN];
char gasipaddr[MAXHOSTLEN];
int found; /* found status for ip address of Goes archive server */


/* exit if scid not valid */
if (!scid) {
	log_event_err("handle_prime_change: no scid specified");
	return;
}

if (strncmp (scid, "13", 2) == 0)  scidnum = 0;
else if (strncmp (scid, "14", 2) == 0)  scidnum = 1;
else if (strncmp (scid, "15", 2) == 0)  scidnum = 2;
else if (strncmp (scid, "16", 2) == 0)  scidnum = 3;
else if (strncmp (scid, "17", 2) == 0)  scidnum = 4;
else if (strncmp (scid, "18", 2) == 0)  scidnum = 5;
else if (strncmp (scid, "19", 2) == 0)  scidnum = 6;
else if (strncmp (scid, "20", 2) == 0)  scidnum = 7;
else {
	log_event_err("handle_prime_change: invalid scid = %s",scid);
	return;
}

/* clear GAS ip address */
memset(gasipaddr,0,sizeof(gasipaddr));

/* init found flag to false */
found = 0;

/*  check list of running sdr processes */
for (i=1; i< 8; i+=2) {

	if (sdr[i].active == 1)  {
		log_debug("handle_prime_change: scidnum=%d  i=%d  sdr[i].scid=%s  sdr[i].stream=%s",
			scidnum,i,sdr[i].scid,sdr[i].stream);
		log_debug("                   : prime[scidnum].strm=%s  prime[scidnum].oldstrm=%s",
			prime[scidnum].strm,prime[scidnum].oldstrm);
			
		if (!strncmp (sdr[i].scid, scid, 2)) {
		
			/* scid match a current sdr's scid */
			if (!strcmp (sdr[i].stream, prime[scidnum].strm)) {
				/* streamid also same, correct sdr already running */
				return;
			} else { 
				/* streamid not same, kill sdr process  */
				log_event("handle_prime_change: scid=%s stop archiving stream=%s",scid,sdr[i].stream);
				log_event("send terminate msg to Prime Stream SDR-%d",i);
				strcpy (zzzz, "zzzzzzzz");
				length = net_write (sdr_client_socket[i], (char *)&zzzz, strlen(zzzz));
				/* added below 2 closing brace to separate killing sdr and spawing sdr ..Lata */
				prime[scidnum].connected = 0;
				strcpy(gasipaddr, sdr[i].gasip);
				found = 1;
			}
			
		}
	}
}

/* find the GAS ip address */
if (strlen(gasipaddr) == 0){
	i = 0;
	while ((!found) && (i < num_gas_hosts)) {
		if (gasoplist[i].scidnum == atoi(scid)) {
			strcpy(gasipaddr, gasoplist[i].gas_addr);
			found = 1;
		}
		i++;
	}
}

if (!found) {
	log_event_err("handle_prime_change: GAS ip address not found for scid = %s",scid);
	return;
	}

/* look for active sdr */
for (i=1; i<8; i+=2) {

	/*  spawn SDR process for new prime stream  */
	for (j=0; j< num_gtacs_hosts; j++) {

		for (k=0; k< num_rt_streams[j]; k++) {
		
			sprintf(msgtxt, "prime[%d].strm = %s, goplist[%d].rtstrm[%d] = %s, operational = %d",
							scidnum, prime[scidnum].strm, j, k, goplist[j].rtstrm[k],
							goplist[j].operational);
			log_debug(msgtxt);

			if (strncmp (prime[scidnum].strm, goplist[j].rtstrm[k],strlen (goplist[j].rtstrm[k])) == 0) {

				if (goplist[j].operational == 1)
				{
					for (n=1; n<8; n+=2) {

						if (sdr[n].active == 0) {

							sprintf(syscall,"./Stream_Data_Receiver p1 %d %s %s %s %s %s &",
								n, prime[scidnum].strm, goplist[j].gtacs_host, scid, gasipaddr, debug_arg);

							log_event("start Prime Stream SDR-%d for stream=%s",n,prime[scidnum].strm);
							log_event("%s",syscall);
							IFM_spawn_task (syscall);
							sdr[n].active = 1;
							sdr[n].prime = 1;
							/* ..lata
							strcpy (sdr[n].gasip, sdr[i].gasip); */
							strcpy (sdr[n].gasip, gasipaddr);
							strcpy (sdr[n].scid, scid);
							strcpy (sdr[n].stream, prime[scidnum].strm);

							success = 1;
							prime[scidnum].connected = 1;
							IFM_update_status (1, scid);
							break;
						}
					}
				}
				break;
			}

		} /* enddo num_rt_streams */
		if (success)  break;

	} /* enddo num_gtacs_hosts */

	if (success) break;
	else {
			/* print the status of sdr process */
			sprintf(msgtxt, "sdr struct : prime=%d, active=%d, scid=%s, gasip=%s, gtacs=%s, stream=%s\n",
					sdr[i].prime, sdr[i].active, sdr[i].scid, sdr[i].gasip, sdr[i].gtacs, sdr[i].stream);
			log_debug(msgtxt);
	}
}


return;
}

