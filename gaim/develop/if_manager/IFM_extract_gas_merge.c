/*  
@(#)  File Name: IFM_extract_gas_merge.c  Release 1.6  Date: 07/03/25, 08:32:47
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_extract_gas_merge.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	 	8.1       01/2004        K. Dolinh       Add log_event, log_debug
	 	11        Mar13-2007     K. Dolinh       Fix compile warning
		
********************************************************************************
         Invocation:
        
             IFM_extract_gas_merge (index, buffer, merge_data)
             
         where
         
             <index> - O
             	 is a pointer to an integer containing the index into the buffer
             <buffer> - I
                 is a pointer to a character string containing the message
             <merge_data> - O
                 is a pointer to a structure containing the whole message
                         
********************************************************************************
         Functional Description:

         This routine extracts the body fields in a merge request messsage 
         received from the GOES Archive Server.
	
*******************************************************************************/

/**
    extract the fields in the merge request message body
    build a linked list for the data sections
    build the message containing the merge data sections to be sent to PUB
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern char *IFM_swap_bytes (int);

void IFM_extract_gas_merge (
	int *index,
	char *buffer,
	GasMergeMsg *merge_data )
	

{
int i, mbindex;
int lnum;
short snum;
char msgtxt[256];
char *c;
struct gas_merge_vector *first_merge;
struct gas_merge_vector *last_merge;
struct gas_merge_vector *merge, *tmp_merge;

first_merge = NULL;

/*  Extract the fields in the message body  */

memcpy ((char *)(&snum), &buffer[*index], 2);
merge_data->status = snum;
*index += 2;

memcpy ((char *)(&snum), &buffer[*index], 2);
merge_data->gtacs_name_len = snum;
*index += 2;

merge_data->gtacs_name = (char *)calloc(snum+1,sizeof(char));	/* extract the */
memcpy (merge_data->gtacs_name, &buffer[*index], snum);		/*  gtacs name */
*index += snum;

memcpy ((char *)(&snum), &buffer[*index], 2);
merge_data->scid_len = snum;
*index += 2;

merge_data->scid = (char *)calloc(snum+1,sizeof(char));		/* extract the */
memcpy (merge_data->scid, &buffer[*index], snum);		/*    scid     */
*index += snum;

memcpy ((char *)(&snum), &buffer[*index], 2);
merge_data->stream_type_len = snum;
*index += 2;

merge_data->stream_type = (char *)calloc(snum+1,sizeof(char));	/* extract the */
memcpy (merge_data->stream_type, &buffer[*index], snum);	/* stream type */
*index += snum;

memcpy ((char *)(&snum), &buffer[*index], 2);
merge_data->start_year = snum;
*index += 2;

memcpy ((char *)(&snum), &buffer[*index], 2);
merge_data->start_day = snum;
*index += 2;

memcpy ((char *)(&snum), &buffer[*index], 2);
merge_data->num_data_sections = snum;
*index += 2;

sprintf (msgtxt,"    Merge message body structure is: ");
log_debug(msgtxt);
sprintf (msgtxt,"        %d %d %s %d %s %d %s %d %d %d",
	merge_data->status, merge_data->gtacs_name_len, merge_data->gtacs_name, 
	merge_data->scid_len, merge_data->scid,
	merge_data->stream_type_len, merge_data->stream_type,
	merge_data->start_year, merge_data->start_day,
	merge_data->num_data_sections);
log_debug(msgtxt);

for (i=0; i< merge_data->num_data_sections; i++)	/*  Build a linked list  */
	{						/* for the data sections */
	merge = (struct gas_merge_vector *)calloc(sizeof(struct gas_merge_vector),1);
	merge->next = NULL;
	if (first_merge == NULL)
		{
		first_merge = merge;
		last_merge = merge;
		}
	else
		{
		last_merge->next = merge;
		last_merge = merge;
		}

  memcpy ((char *)(&lnum), &buffer[*index], 4);
	merge->start_time = lnum;
	*index += 4;

  memcpy ((char *)(&lnum), &buffer[*index], 4);
	merge->duration = lnum;
	*index += 4;

	/* printf ("        %d ms / %d ms\n", merge->start_time, merge->duration); */
	}

merge_data->merge_vector = first_merge;

/*  Build the message containing the merge data sections to be sent to PUB  */

mb_num_bytes = 2 + (merge_data->num_data_sections * 8);
merge_buffer = (char *)calloc(mb_num_bytes+1, sizeof(char));
mbindex = 0;
memcpy (&merge_buffer[mbindex], (char *)&(merge_data->num_data_sections), 2);
mbindex += 2;

tmp_merge = first_merge;
while (tmp_merge != NULL)
	{
	memcpy (&merge_buffer[mbindex], (char *)&tmp_merge->start_time, 4);
	memcpy (&merge_buffer[mbindex+4], (char *)&tmp_merge->duration, 4);
	mbindex += 8;
	tmp_merge = tmp_merge->next;
	}

/*** free (merge); ***/

return;
}
