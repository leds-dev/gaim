/*  
@(#)  File Name: IFM_extract_gas_msghdr.c  Release 1.4  Date: 07/03/25, 08:32:48
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_extract_gas_msghdr.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
		 11			 Mar13-2007					 fix compile warning

********************************************************************************
         Invocation:
        
             IFM_extract_gas_msghdr (index, buffer, msghdr)
             
         where
         
             <index> - O
             	 is a pointer to an integer containing the index into the buffer
             <buffer> - I
                 is a pointer to a character string containing the message
             <msghdr> - O
                 is a pointer to a structure containing the message header
                         
********************************************************************************
         Functional Description:

         This routine extracts the header fields in a message received from the 
         GOES Archive Server.
	
*******************************************************************************/

/**
    extract the message size
    extract the class id
    extract the message number
    extract the node name length
    extract the node name
    extract the process name length
    extract the process name
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern char *IFM_swap_bytes (int);

void IFM_extract_gas_msghdr (
		int *index,
		char *buffer,
		GasMsgHeader *msghdr)

{
int lnum =0;
short snum = 0;
char *c;

/* printf ("\nmessage:  %8.8X %8.8X %8.8X %8.8X %8.8X %8.8X\n",
		*(int *)(&buffer[*index]),*(int *)(&buffer[*index+4]),
		*(int *)(&buffer[*index+8]),*(int *)(&buffer[*index+12]),
		*(int *)(&buffer[*index+16]),*(int *)(&buffer[*index+20]));
printf ("          %8.8X %8.8X %8.8X %8.8X %8.8X %8.8X\n",
		*(int *)(&buffer[*index+24]),*(int *)(&buffer[*index+28]),
		*(int *)(&buffer[*index+32]),*(int *)(&buffer[*index+36]),
		*(int *)(&buffer[*index+40]),*(int *)(&buffer[*index+44])); */

/*  Extract the message size  */

memcpy ((char *)(&lnum), &buffer[*index], 4);
msghdr->message_size = lnum;
*index += 4;

/*  Extract the class id  */

memcpy ((char *)(&snum), &buffer[*index], 2);
msghdr->class_id = snum;
*index += 2;

/*  Extract the message number  */

memcpy ((char *)(&snum),  &buffer[*index], 2);
msghdr->message_number = snum;
*index += 2;

/*  Extract the node name length  */

memcpy ((char *)(&snum), &buffer[*index], 2);
msghdr->node_name_len = snum;
*index += 2;

/*  Extract the node name  */

msghdr->node_name = (char *)calloc(snum+1,sizeof(char));
memcpy (msghdr->node_name, &buffer[*index], snum);
*index += snum;

/*  Extract the process name length  */

memcpy ((char *)(&snum), &buffer[*index], 2);
msghdr->process_name_len = snum;
*index += 2;

/*  Extract the process name  */

msghdr->process_name = (char *)calloc(snum+1,sizeof(char));
memcpy (msghdr->process_name, &buffer[*index], snum);
*index += snum;

return;
}
