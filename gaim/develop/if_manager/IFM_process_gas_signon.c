/*
@(#)  File Name: IFM_process_gas_signon.c  Release 1.13  Date: 07/03/25, 08:33:05
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_process_gas_signon.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
	 	 8.1         01/2004     K. Dolinh       Add log_event, log_debug
     	 10A	     nov29-2005			         new IFM_set_point_sarc_status
		 10B    	 Jan12-2006					 - recompile for Build 10B
		 11          Aug11-2006                  - call IFM_send_event
		 
********************************************************************************
         Invocation:

             IFM_process_gas_signon (gas_index, buffer)

         where

             <gas_index> - I
                 is an integer indicating which GOES Archive process sent the
                 message.

             <buffer> - I
                 is a pointer to a character string containing the message

********************************************************************************
         Functional Description:

         This routine processes signon messsages received from the GOES
         Archive Server.

*******************************************************************************/

/**
    CALL IFM_extract_gas_msghdr
    CALL IFM_extract_gas_signon
    DOFOR each GOES Archive server
        IF signon message node name matches this GOES Archive server THEN
            IF signing on THEN
                DOFOR each GTACS
                    IF GTACS is operational THEN
                        IF signing on to this GTACS THEN
                            send event message to GTACS
                            set GTACS global variable
                        ENDIF
                    ENDIF
                ENDDO
                write log message
            ELSE
                DOFOR each GTACS
                    IF GTACS is operational THEN
                        IF signing off of this GTACS THEN
                            send event message to GTACS
                            set GTACS global variable
                        ENDIF
                    ENDIF
                ENDDO
                write log message
            ENDIF
            set success flag to true
            BREAK
        ENDIF
    ENDDO
    IF success flag is set to true THEN
        DOFOR each GTACS
            IF message host matches this GTACS THEN
                try to connect to GOES Archive server socket
                IF connect fails THEN
                    write log message
                ELSE
                    CALL IFM_build_gas_signon
                    send signon message response to GOES Archive server
                    write log message
                    shutdown and close the socket
                    sleep for 1 second
                ENDIF
            ENDIF
        ENDDO
    ELSE
        write log message
    ENDIF
    free all of the allocated memory for this message
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

extern void IFM_onintr_timer (int);

void IFM_process_gas_signon (
		int gas_index,
		char *buffer)

{
	int i, j, index2, length, status, index = 0;
	int success = 0;
	int signon_all;
	long val;
	char cseqnum [6];
	char host [MAXHOSTLEN];
	char buffer2 [MAXBUFFLEN];
	char msgtxt [MAXMSGTEXT];
	char signon_service [MAXSERVLEN];
	char str [3];
	GasSignonMsg sign_on;
	char message[MAXMSGTEXT];
	struct timeval ping_time;
	struct itimerval timer;
	char tmp_scid[3];

	timer.it_value.tv_sec = 2;
	timer.it_value.tv_usec = 0;
	timer.it_interval.tv_sec = 0;
	timer.it_interval.tv_usec = 0;

	ping_time.tv_sec = 1;
	ping_time.tv_usec = 0;

	/*  Extract the fields in the message header  */

	IFM_extract_gas_msghdr (&index, buffer, &sign_on.msg_hdr);

	sprintf (msgtxt,"    Signon message header structure is: ");
	log_debug(msgtxt);
	sprintf (msgtxt,"        %d %d %d %d %s %d %s",
		sign_on.msg_hdr.message_size, sign_on.msg_hdr.class_id,
		sign_on.msg_hdr.message_number, sign_on.msg_hdr.node_name_len,
		sign_on.msg_hdr.node_name, sign_on.msg_hdr.process_name_len,
		sign_on.msg_hdr.process_name);
	log_debug(msgtxt);

	/*  Extract the fields in the message body  */
	IFM_extract_gas_signon (&index, buffer, &sign_on);

	if (sign_on.gtacs_name_len <= 2)  signon_all = 1;
	else signon_all = 0;

	/*  Send GTACS event message and set global for Archive status  */

	for (i=0; i< num_gas_hosts; i++)
		{
		if (strcmp (sign_on.msg_hdr.node_name, gasoplist[i].gas_host) == 0)
			{
			strcpy (host, gasoplist[i].gas_addr);
			if (sign_on.state == 1)
				{
				/* change gas operational state to ON if previously OFF */
				if (gasoplist[i].operational != 1) {
					gasoplist[i].operational = 1;
					/* send GAS signon event to GTACS ground streams */
					sprintf(msgtxt,"GAS server %s is signing on to %s",sign_on.archive_name,gaimname);
					IFM_send_event (EVENT_NORMAL, msgtxt);
					log_event(msgtxt);
				}
				
				/* save gas hostname: SARC13,SARC14,... */	
				strcpy (gasoplist[i].archive_name,sign_on.archive_name);
				/* save gas scid: 13,14,...*/
				strncpy(tmp_scid, gasoplist[i].archive_name+(strlen(gasoplist[i].archive_name) -2), 2);
				tmp_scid[2] ='\0';
				gasoplist[i].scidnum  = atoi(tmp_scid);
		
				/* set global var SARCNN_STATUS to state "ready/GAIMO1" or "ready/GAIM02" */
				for (j=0; j< num_gtacs_hosts; j++) {
					if (goplist[j].operational == 1){
						if ((sign_on.gtacs_name_len <= 2) ||
						    ((sign_on.gtacs_name_len > 2) && (strncmp (sign_on.gtacs_name,
						      goplist[j].gtacs_host, strlen(goplist[j].gtacs_host)) == 0)))
							{
							/* select value (state) for global SARCNN_STATUS */
							if (strncmp (gaimname, "GAIM1", 6) == 0)  val = 1;
							else  val = 2;
							
							/* set value of global SARCNN_STATUS */
							if (strncmp (sign_on.archive_name,"SARC13",6) == 0)
								{
								*(long *)sarc13_status_pid[j]->value = val;
								sarc13_status_pid[j]->status_bits = 0;
								/*status = set_point (sarc13_status_pid[j]);*/
								}
							else if (strncmp (sign_on.archive_name,"SARC14",6) == 0)
								{
								*(long *)sarc14_status_pid[j]->value = val;
								sarc14_status_pid[j]->status_bits = 0;
								/*status = set_point (sarc14_status_pid[j]);*/
								}
							else if (strncmp (sign_on.archive_name,"SARC15",6) == 0)
								{
								*(long *)sarc15_status_pid[j]->value = val;
								sarc15_status_pid[j]->status_bits = 0;
								/*status = set_point (sarc15_status_pid[j]);*/
								}
							else
								{
								*(long *)sarc16_status_pid[j]->value = val;
								sarc16_status_pid[j]->status_bits = 0;
								/*status = set_point (sarc16_status_pid[j]);*/
								}
							status = IFM_set_point_sarc_status(sign_on.archive_name,0);
							}
						}
					} /* enddo num_gtacs_hosts */

				/* save time of gas signon message */
				status = time(&gasoplist[i].signon_time);
				} 
				
			else
				{
				gasoplist[i].operational = 0;
				/* change gas operational state to OFF if previously ON */
				if (gasoplist[i].operational != 0) {
					gasoplist[i].operational = 0;
					/* send GAS signon event to GTACS ground streams */
					sprintf(msgtxt,"GAS server %s is signing off %s",sign_on.archive_name,gaimname);
					sprintf(msgtxt,"GAS server %s is signing off %s",sign_on.msg_hdr.node_name,gaimname);
					IFM_send_event (EVENT_NORMAL,msgtxt);
					log_event(msgtxt);
				}
				
				/* set global var SARCNN_STATUS to state "signed off" */
				for (j=0; j< num_gtacs_hosts; j++) {
					if (goplist[j].operational == 1) {
						if ((sign_on.gtacs_name_len <= 2) ||
						    ((sign_on.gtacs_name_len > 2) && (strncmp (sign_on.gtacs_name,
						      goplist[j].gtacs_host, strlen(goplist[j].gtacs_host)) == 0)))
							{
							if (strncmp (sign_on.archive_name,"SARC13",6) == 0) {
								*(long *)sarc13_status_pid[j]->value = 3;
								sarc13_status_pid[j]->status_bits = 0;
								/*status = set_point (sarc13_status_pid[j]);*/
								}
							else if (strncmp (sign_on.archive_name,"SARC14",6) == 0)
								{
								*(long *)sarc14_status_pid[j]->value = 3;
								sarc14_status_pid[j]->status_bits = 0;
								/*status = set_point (sarc14_status_pid[j]);*/
								}
							else if (strncmp (sign_on.archive_name,"SARC15",6) == 0)
								{
								*(long *)sarc15_status_pid[j]->value = 3;
								sarc15_status_pid[j]->status_bits = 0;
								/*status = set_point (sarc15_status_pid[j]);*/
								}
							else
								{
								*(long *)sarc16_status_pid[j]->value = 3;
								sarc16_status_pid[j]->status_bits = 0;
								/*status = set_point (sarc16_status_pid[j]);*/
								}
							/* update GTACS ground stream global var ..SARCnn */
							status = IFM_set_point_sarc_status(sign_on.archive_name,0);
							}
						}
					} /* enddo num_gtacs_hosts */

				} 
			success = 1;
			break;
			}
		}

	if (success)
		{
		/* status = flush_sets ();*/

		/*  Connect to the GOES Archive Server and send all of the messages  */
		strcpy (signon_service, GAS_SCEXEC);
		/* sprintf (str, "%d", gas_index+12);
		strcat (signon_service, str); */

		for (i=0; i< num_gtacs_hosts; i++)
			{
			if ((sign_on.gtacs_name_len <= 2) ||
		   	 ((sign_on.gtacs_name_len > 2) && (strncmp (sign_on.gtacs_name,
		     	 goplist[i].gtacs_host, strlen(goplist[i].gtacs_host)) == 0)))
				{
				signal (SIGALRM, IFM_onintr_timer);

				if (setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL))
					{
					strcpy (msgtxt, "IFM_process_gas_signon: set itimer failed");
					log_event(msgtxt);
					}

 				if (status=net_call (host, signon_service, &gas_server_socket))
					{
					timer.it_value.tv_sec = 0;
					timer.it_value.tv_usec = 0;
					timer.it_interval.tv_sec = 0;
					timer.it_interval.tv_usec = 0;
					setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL);

					strcpy (msgtxt, "failed to connect to GAS on ");
					strcat (msgtxt, sign_on.msg_hdr.node_name);
					strcat (msgtxt, " address=");
					strcat (msgtxt, host);
					strcat (msgtxt, " service=");
					strcat (msgtxt, signon_service);
					sprintf(message,"%s (Error %i)",msgtxt,status);
					log_event(message);
					}
				else
					{
					/*  Build the signon message  */

					timer.it_value.tv_sec = 0;
					timer.it_value.tv_usec = 0;
					timer.it_interval.tv_sec = 0;
					timer.it_interval.tv_usec = 0;
					setitimer(ITIMER_REAL, &timer, (struct itimerval *) NULL);

 					IFM_build_gas_signon (i, &index2, sign_on, &buffer2[0]);

  					length = net_write (gas_server_socket, (char *)&buffer2, index2);

  					if (sign_on.state == 1)
  						{
  						strcpy (msgtxt, "sent Signon Message # ");
  						}
  					else
  						{
  						strcpy (msgtxt, "sent Signoff Message # ");
  						}
  					sprintf (cseqnum, "%d", signon_seq_num);
  					strcat (msgtxt, cseqnum);
  					strcat (msgtxt, " to GAS on ");
  					strcat (msgtxt, sign_on.msg_hdr.node_name);
  					strcat (msgtxt, " for ");
  					strcat (msgtxt, goplist[i].gtacs_host);
					log_debug(msgtxt);

 					/*  Update the sequence number for this message type  */

 					signon_seq_num += 1;
					if (signon_seq_num >= 32768)  signon_seq_num = 1;

					/*  Shutdown the socket  */

  					shutdown (gas_server_socket, 2);
 					close (gas_server_socket);
 					gas_server_socket = -1;

 					if (signon_all == 1)  sleep (1);
 					/*** if (signon_all == 1)
 						{
 						select (FD_SETSIZE, NULL, NULL, NULL, &ping_time);
 						} */

					}
				}
			}
		}
	else
		{
		strcpy (msgtxt, "IFM_process_gas_signon: cannot resolve GAS hostname=");
		strcat (msgtxt, sign_on.msg_hdr.node_name);
		log_event(msgtxt);
		}

	/*  Free all of the allocated memory for this message  */
	free (sign_on.msg_hdr.node_name);
	free (sign_on.msg_hdr.process_name);
	free (sign_on.archive_name);
	if (sign_on.gtacs_name_len > 0)  free (sign_on.gtacs_name);

	return;
}
