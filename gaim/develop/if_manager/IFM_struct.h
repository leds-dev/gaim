/*
@(#)  File Name: IFM_struct.h  Release 1.8  Date: 07/03/25, 08:33:15
*/

#ifndef IFM_STRUCT_H
#define IFM_STRUCT_H

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         HEADER          :       IFM_struct.h
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         8.1		         K. Dolinh       Add signon_time to gas_op_list
         					 Add pollpoint_err to gtacs_op_list
	 8.8        may06-2004                  .connected=0 means not-connected
	 9.2        jan31-2005                   Add gas_index to gas_merge_data_message
	 	11		nar13-2007					 use typedef to define structs

********************************************************************************
         Functional Description:

         This header file contains structures used by the routines in
         the N-Q_Interface_Manager process.

*******************************************************************************/

struct sc_prime_strm {
	char sc [MAXSCIDLEN];
	char host [MAXHOSTLEN];
	char strm [MAXSTRMLEN];
	char oldstrm [MAXSTRMLEN];
	int  connected;		/* 0 = not-connected, 1 = connected, 2 = do-not-try */
	int  requested;  	/* 1 = archive this prime, 0 do-not-archive */
};

struct gtacs_op_list {
	char gtacs_host [MAXHOSTLEN];
	char gtacs_addr [MAXHOSTLEN];
	char grndstrm [MAXSTRMLEN];
	char rtstrm [MAXNUMRT] [MAXSTRMLEN];
	char simstrm [MAXNUMSIM] [MAXSTRMLEN];
	char pbstrm [MAXNUMPB] [MAXSTRMLEN];
	short available;
	short operational;
	int pollpoint_err;
};

struct gas_op_list {
	char gas_host [MAXHOSTLEN];
	char gas_addr [MAXHOSTLEN];
	char gas_db_host [MAXHOSTLEN];
	char gas_db_addr [MAXHOSTLEN];
	char archive_name [MAXHOSTLEN];
	short scidnum;
	short operational;
	time_t  signon_time;
};

struct sdr_process {
	int active;
	int prime;
	char gasip [MAXHOSTLEN];
	char gtacs [MAXHOSTLEN];
	char scid [MAXSCIDLEN];
	char stream [MAXSTRMLEN];
};

struct pub_process {
	int active;
	int type;
	char gasip [MAXHOSTLEN];
	char gtacs [MAXHOSTLEN];
	char scid [MAXSCIDLEN];
	char stream [MAXSTRMLEN];
};

#endif
