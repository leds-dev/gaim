/*
@(#)  File Name: IFM_params.h  Release 1.6  Date: 07/03/25, 08:32:56
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         INCLUDE         :       IFM_params.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation
         9.2         jan31-2005  K. Dolinh       Add constants GAS_MERGE_STAT_...
		 11          Aug11-2006                  Change EVT_...
		 			 Mar08-2007					 Added util_log.p
					 Mar13-2007					 Updated GAIM_EVTNUM...
		 
********************************************************************************
         Functional Description:

         This header file contains parameters used by the routines in
         the N-Q_Interface_Manager process.

*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "db_util.p"

#ifndef TRUE
#  define TRUE (1)
#endif
#ifndef FALSE
#  define FALSE (0)
#endif
#ifndef NULL
#  define NULL 0
#endif

#define PUB_SERVICE	"gaim_ifm_pub_"	     /* IFM to PUB socket name prefix */
#define SDR_SERVICE	"gaim_ifm_sdr_"	     /* IFM to SDR socket name prefix */
#define CGT_SERVICE	"gaim_ifm_cgt"	     /* IFM to CGT socket name prefix */
#define GAS_SCEXEC	"GaSeExec"           /* Archive ScExec service        */
#define GAS_AUTOMERGE	"GaAutoMerge"        /* Archive Auto Merge service    */
#define GAS_MANMERGE	"GaManMerge"         /* Archive Man Merge service     */
#define GAS_INGEST	"GaIngest"           /* Archive Ingest service        */
#define GAS_STATS	"GaRetStats"         /* Archive Stats service         */
#define GAS_TLM 	"GaRetTlm"           /* Archive Tlm service           */
#define GAS_UPDATE	"GaUpdArch"          /* Archive Update service        */
#define GAS_EVENTS	"GaMoEvents"         /* Archive Event Mon service     */
#define GAS_PING	"GwGmPing"           /* Archive Ping service          */

#define IFM_PROC_NAME	"N-Q_Interface_Manager"		/* process name       */

#define MAXBUFFLEN	256	/* max number of characters in buffer         */
#define MAXNBUFLEN	-256	/* max number of characters in buffer         */
#define MAXDBLEN	256	/* max number of characters in database name  */
#define MAXLINELEN	512	/* max number of characters in epoch.cfg line */
#define MAXMSGTEXT	256	/* max number of characters in message text   */
#define MAXGASHOSTS	10	/* max number of GOES Archive host machines   */
#define MAXGTACSHOSTS	12	/* max number of GTACS host machines          */
#define MAXHOSTLEN	33	/* max number of characters in host name      */
#define MAXNUMPB	16	/* max number of playback streams             */
#define MAXNUMRT	16	/* max number of realtime streams             */
#define MAXNUMSIM	16	/* max number of simulation streams           */
#define MAXPROCLEN	36	/* max number of characters in process name   */
#define MAXSCIDLEN	5	/* max number of characters in scid name      */
#define MAXSCIDS	8	/* max number of spacecraft                   */
#define MAXSERVLEN	51	/* max number of characters in service name   */
#define MAXSTRMLEN	36	/* max number of characters in stream name    */

#define GAS_EVENT_ID	4	/* class id for event messages                */
#define GAS_PING_ID		11	/* class id for ping messages                 */
#define GAS_MERGE_ID	101	/* class id for merge requests/responses      */
#define GAS_GLOBAL_ID	105	/* class id for global requests/responses     */
#define GAS_SIGNON_ID	107	/* class id for signon requests/responses     */

#define GAS_MERGE_STAT_CONNECT 		0	/* GTACS is available */
#define GAS_MERGE_STAT_NOCONNECT	1	/* GTACS is not available */
#define GAS_MERGE_STAT_REQ_ERR		2	/* insufficient info */
#define GAS_MERGE_STAT_BUSY			3	/* merge request already active */
#define GAS_MERGE_STAT_PUB_ERROR	4	/* playback error */

#define EVENT_NORMAL		1	/* event type normal */
#define EVENT_WARN			2	/* event type warning */
#define EVENT_ERR			3	/* event type error */
#define EVENT_GAS_NORMAL	4	/* event type GAS */
#define EVENT_GAS_ERR		5	/* event type GAS */

#define GAIM_EVTNUM_NORMAL	6600	/* GAIM GTACS routine event message          */
#define GAIM_EVTNUM_WARN	6601	/* GAIM GTACS warning event message          */
#define GAIM_EVTNUM_ERR		6602	/* GAIM GTACS err-critical event message     */

#define ERRMSG "(ERR)"

#define MANUAL_MERGE_MAX  24		/* number of items in Manual Merge Req Queue */

#include "IFM_prototypes.h"
