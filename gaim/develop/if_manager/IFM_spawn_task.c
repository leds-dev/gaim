/*  
@(#)  File Name: IFM_spawn_task.c  Release 1.5  Date: 07/03/25, 08:33:14
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       IFM_spawn_task.c
         EXE. NAME       :       N-Q_Interface_Manager
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       B. Hageman      Initial creation

********************************************************************************
         Invocation:
        
             IFM_spawn_task (syscall)
             
         where
         
             <syscall> - I
                 is a pointer to the string containing the command
                         
********************************************************************************
         Functional Description:

         This routine writes a file that contains the task name and command
         line arguments along with everything else necessary to spawn the task.
	
*******************************************************************************/

/**
    open a temporary file
    write spawn information to file
    close the file
    execute the file
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "IFM_params.h"
#include "IFM_struct.h"
#include "IFM_varsex.h"

void IFM_spawn_task (char* syscall)
{
	system (syscall);

	return;
}
