/*
 @(#)  File Name: DBS_main.c  Release 1.1  Date 01/03/13, 14:12:32
*/
/****************************************************************************

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       DBS_main.c
         EXE. NAME       :       DB_Shrinker
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        04/00       S. Scoles       Initial creation

********************************************************************************
  	Invocation:

		DB_Shrink old_db new_db

	Parameters:

		old_db - The name of the original DB file.
		new_db - The name of the DB file to create.
			

	Example Usage:

		DB_Shrink goesn.gsa goesn.gsb


	Exit Conditions:

		1: Invalid number of parameters
		2: Error opening input DB
		3: Line too long 
				
******************************************************************************
	Functional Description:

	
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
	/* Standard C Libraries */

struct text_conv {
	int min;
	int max;
	char state[32];
};

void DBS_process_poly_conversion(char *buffer) {

/*	char *new_buff; */
	char new_buff[300000];
		/* New buffer */

	int num_contexts;
	int context_index_list[32];
		/* Where does each context list start in the conversions array? */

	int location,last_location;
		/*  0=outside [], 
			1=inside [], outside {}
			2=inside {}, outside ()
			3=inside () */

	int paren;
		/* -1=Currnt char is a paren.  Do nothing else. */

	int context_state;
		/* What context are we in right now */

	int state_number,new_state_number;
		/* Which state number are we on? */

	int i,j;
		/* Counters */

	char addto[256];
		/* Next piece to add to the conversion list */

	double coeffs[32][10];
		/* 32 context states, 10 coeffs each */

	char float_number[32];
		/* Current float number being extracted */

	int on_coeff;
		/* Which coefficient are we looking at? */

	int max_coeff;
		/* What is the max, non-zero coefficient? */


/*	new_buff=(char *)calloc(strlen(buffer)+2,sizeof(char)); */
		/* Allocate the memory */

	num_contexts=0;
	state_number=0;
	location=0;
		/* We start outside of everything */

	for (i=0; i<strlen(buffer); i++) {
			/* For each character in the line */

		paren=0;
			/* Assume no paren, unless otherwise detected */

		last_location=location;
		if (buffer[i]=='[' || buffer[i]=='{' || buffer[i]=='(') {
				/* If an opening paren */

			location++;
			paren=-1;
				/* Make a note of the fact we have changed paren levels, and 
					the current char is a paren */

			if (location==1) context_state=0;
		}

		if (buffer[i]==']' || buffer[i]=='}' || buffer[i]==')') {
				/* If a closing paren */

			location--;
			paren=-1;
				/* Make a note of the fact we have changed paren levels, and 
					the current char is a paren */
		}

		if (location==2 && last_location==1) {
				/* If we are in () */

			context_index_list[context_state-1]=state_number;
			num_contexts++;
				/* Record the current location of the state list. */

			float_number[0]=0;
			on_coeff=0;
				/* Reset the coefficient and the coefficient pointer */

		}

		if (!paren && buffer[i]!=' ') {
			if (location==1 && buffer[i]!=',')
				context_state=10*context_state+(int)(buffer[i]-'0');
			if (location==2 && buffer[i]!=',') {
				float_number[strlen(float_number)+1]=0;
				float_number[strlen(float_number)]=buffer[i];
			} else if (location==2 && buffer[i]==',') {
				coeffs[context_state-1][on_coeff]=atof(float_number);
				on_coeff++;
				float_number[0]=0;
			}
		}

		if (paren && on_coeff==7) {
			coeffs[context_state-1][on_coeff]=atof(float_number);
			on_coeff++;
			float_number[0]=0;
			context_state=0;
		}
	}

	sprintf(new_buff,"Conversion=POLY[");
	for (j=0; j<num_contexts; j++) {
		sprintf(addto,"%i(",j+1);
		strcat(new_buff,addto);

		max_coeff=0;
		for (i=0; i<8; i++) if (coeffs[j][i]!=0.0) max_coeff=i;
		for (i=0; i<=max_coeff; i++) {
			sprintf(addto,"%.7le",coeffs[j][i]);
			strcat(new_buff,addto);
			if (i!=max_coeff) strcat(new_buff,",");
		}
		strcat(new_buff,")");
		if (j!=num_contexts-1) strcat(new_buff,",");
	}
	strcat(new_buff,"];");
	strcpy(buffer,new_buff);


}

void DBS_process_text_conversion(char *buffer) {

/*	char *new_buff; */
	char new_buff[300000];
		/* New buffer */

/*	struct text_conv *conversions; */
	struct text_conv conversions[20000];
		/* Extracted conversions */

	int num_contexts;
	int context_index_list[32];
		/* Where does each context list start in the conversions array? */

	int location,last_location;
		/*  0=outside [], 
			1=inside [], outside {}
			2=inside {}, outside ()
			3=inside () */

	int paren;
		/* -1=Currnt char is a paren.  Do nothing else. */

	int wherein;
		/*  0=Not inside (),
			1=Processing MIN parameter,
			2=Processing MAX parameter,
			3=Starting State Name,
			4=Processing State Name [after skipping spaces] */

	int context_state;
		/* What context are we in right now */

	int state_number,new_state_number;
		/* Which state number are we on? */

	int newin;
		/* -1=Newly in () */

	int i,j;
		/* Counters */

	char addto[256];
		/* Next piece to add to the conversion list */

	int min_state,max_state;
		/* For the current context state, what is the MIN and what is the 
			MAX state numbers? */

/*	new_buff=(char *)calloc(strlen(buffer)+2,sizeof(char)); */
		/* Allocate the memory */

/*	conversions=(struct text_conv *)calloc(strlen(buffer)/10,
		sizeof(struct text_conv)); */
			/* Conversions array.  One entry is never shorter than 10 bytes 
				in the input buffer */

	num_contexts=0;
	state_number=0;
	wherein=0;
	location=0;
		/* We start outside of everything */

	for (i=0; i<strlen(buffer); i++) {
			/* For each character in the line */

		paren=0;
			/* Assume no paren, unless otherwise detected */

		last_location=location;
		if (buffer[i]=='[' || buffer[i]=='{' || buffer[i]=='(') {
				/* If an opening paren */

			location++;
			paren=-1;
				/* Make a note of the fact we have changed paren levels, and 
					the current char is a paren */

			if (location==1) context_state=0;
		}

		if (buffer[i]==']' || buffer[i]=='}' || buffer[i]==')') {
				/* If a closing paren */

			location--;
			paren=-1;
				/* Make a note of the fact we have changed paren levels, and 
					the current char is a paren */
		}

		if (location==2 && last_location==1) {
				/* If we are in {}, but not () */

			context_index_list[context_state-1]=state_number;
			num_contexts++;
				/* Record the current location of the state list. */

		}

		if (!paren && (buffer[i]!=' ' || location==3) && location>0) {
				/* If not a paren, and not a space outside of (), or in () */

			if (location==1) 
				context_state=10*context_state+(int)(buffer[i]-'0');
					/* If we are in [] but not in {}, keep incrementing the 
						context state */

			newin=0;
			if (location==3 && wherein==0) {
					/* If we have just arrived in () */

				conversions[state_number].min=0;
				wherein=1;
				newin=-1;
			}

			if (location==3 && wherein==1 && buffer[i]=='-' && !newin) {
					/* If this is the divider between MIN and MAX */

				conversions[state_number].max=0;
				wherein=2;
				newin=-1;

			} 

			if (location==3 && wherein==2 && buffer[i]==',') {
					/* If this is the divider between MAX and NAME */

				conversions[state_number].state[0]=0;
				wherein=3;
				newin=-1;
			} 

			if (location==3 && wherein==1) {
					/* Extract MIN value */

				if (buffer[i]!='.' && buffer[i]!='-') {
					conversions[state_number].min=
						10*conversions[state_number].min+(int)(buffer[i]-'0');
				} else {
					printf("FUNNY CHAR <%c> MIN FOUND in %s!\n",buffer[i],
						buffer);
					exit(4);
				}
			}

			if (location==3 && wherein==2 && !newin) {
					/* Extract MAX value */

				if (buffer[i]!='.' && buffer[i]!='-') {
					conversions[state_number].max=
						10*conversions[state_number].max+(int)(buffer[i]-'0');
				} else {
					printf("FUNNY CHAR <%c> MAX FOUND in %s!\n",buffer[i],
						buffer);
					exit(4);
				}
			}


			if (location==3 && wherein==3 && buffer[i]!=' ' && !newin) 
				wherein=4;
					/* If extracting states and we have skipped any prefixed 
						spaces */

			if (wherein==4 && !paren) {
					/* If recording state names, and not the closing paren */

				conversions[state_number].state[strlen(conversions[
					state_number].state)+1]=0;
						/* First make sure the character beyond the null 
							terminator is also a null. */

				conversions[state_number].state[strlen(conversions[
					state_number].state)]=buffer[i];
						/* Then add the next character from the buffer to the 
							state name. */
			}
 		}

		if (wherein==4 && paren) {
				/* If recording state names, but we are at the closing paren. */

			while(conversions[state_number].state[strlen(conversions[
				state_number].state)-1]==' ') conversions[state_number
				].state[strlen(conversions[state_number].state)-1]=0;
					/* Chop off trailing spaces. */

			state_number++;
			wherein=0;
		}
	}

	sprintf(new_buff,"Conversion=TEXT[");
	for (j=0; j<num_contexts; j++) {
		sprintf(addto,"%i{",j+1);
		strcat(new_buff,addto);

		min_state=context_index_list[j];
		max_state=state_number;
		if (j!=num_contexts-1) max_state=state_number;

		new_state_number=0;
		for (i=min_state; i<max_state; i++) {
			if (strcmp(conversions[new_state_number].state,
				conversions[i].state)==0)
					conversions[new_state_number].max=conversions[i].max;
			else {
				new_state_number++;
				conversions[new_state_number].min=conversions[i].min;
				conversions[new_state_number].max=conversions[i].max;
				strcpy(conversions[new_state_number].state,
					conversions[i].state);
			}
		}

		for (i=0; i<=new_state_number; i++) {
			sprintf(addto,"(%i-%i,%s)",conversions[i].min,conversions[i].max,
				conversions[i].state);
			strcat(new_buff,addto);
			if (i!=new_state_number) strcat(new_buff,",");
		}
		strcat(new_buff,"}");
		if (j!=num_contexts-1) strcat(new_buff,",");
	}

	strcat(new_buff,"];");
	strcpy(buffer,new_buff);

/*	free(conversions); */
		/* Free the memory */

}


int main(int num_params, char *params[]) {

	char old_name[256],new_name[256];
		/* DB Names */

	FILE *infile,*outfile;
		/* File Pointers */

	char buffer[256];
		/* Input buffer */

	char line_buffer[131072];
		/* Holds one complete line of the file */

	int append_line=0;
		/* -1=Append the current line to the line buffer */

	int pos;
		/* Start of non-whitespace text */

	int end_char;
	int last_end_char;
		/* Current "end" char and last "end" char. */

	char temp;
		/* Temp char storage */


	if (num_params!=3) {
		printf("Usage: \n");
		printf("    DB_Shrink old_db_name new_db_name\n");
		exit(1);
	}
		/* Do we have the correct number of parameters? */

	strcpy(old_name,*++params);
	strcpy(new_name,*++params);
		/* Copy the two file names. */

	infile=fopen(old_name,"r");
		/* Open the input file */

	if (infile) {
			/* If the input file exists */

		outfile=fopen(new_name,"w");
			/* Open the output file */

		if (outfile) {
				/* If there is no error opening the output file */

			do {	/* Read until end of file */

				fgets(buffer,256,infile);
					/* Get the next line of the file */

				if (!feof(infile)) {
						/* If not at the end of the file */

					while (strlen(buffer)>0 && (buffer[strlen(buffer)-1]==10 ||
						buffer[strlen(buffer)-1]==13 || 
						buffer[strlen(buffer)-1]==9 || 
						buffer[strlen(buffer)-1]==' '))
							buffer[strlen(buffer)-1]=0;
									/* Chop off ASCII 10 and 13 (CR/LF), as 
										well as any TABS (ASCII 9) and spaces */

					pos=0;
					while(buffer[pos]==' ' && pos<strlen(buffer)) pos++;
						/* Skip preliminary spaces */

					if (!append_line) strcpy(line_buffer,&buffer[pos]);
						/* Duplicate the buffer, since we are writing 
							line_buffer ro the output file */

					if (strncmp(&buffer[pos],"Conversion=TEXT",15)==0 || 
							strncmp(&buffer[pos],"Conversion=POLY",15)==0) {
								/* If this is one of the two lines we need 
									to adjust */

						append_line=-1;
							/* Copy the line to our array, and set the flag 
								to keep track of the appending */

					} else if (append_line) {
							/* If the appending flag is set, append the line. */

						if (strlen(line_buffer)>130000) {
							printf("THIS MAKES THE LINE LINE TOO LONG: %s\n",
								buffer);
							exit(3);
						}
						strcat(line_buffer,&buffer[pos]);
							/* Append the line */
					}

					if (buffer[strlen(buffer)-1]==';') {
							/* If the end-of-line character is seen */

						append_line=0;
							/* Reset the APPEND flag */

						if (strncmp(line_buffer,"Conversion=TEXT",15)==0) 
							DBS_process_text_conversion(line_buffer);
								/* If this is a state list */

						if (strncmp(line_buffer,"Conversion=POLY",15)==0)
							DBS_process_poly_conversion(line_buffer);
								/* If this is a polynomial conversion */
					}

					if (!append_line) {
							/* If we are no longer appending lines... */
	
						if (strlen(line_buffer)<75) 
							fprintf(outfile,"%s\n",line_buffer);
								/* Write the new line to the output file */
						else {
							last_end_char=0;
							end_char=75;
							do {
								while (end_char>last_end_char && 
									line_buffer[end_char]!=',') end_char--;
								end_char++;
								temp=line_buffer[end_char];
								line_buffer[end_char]=0;
								fprintf(outfile,"%s\n",
									&line_buffer[last_end_char]);
								line_buffer[end_char]=temp;
								last_end_char=end_char;
								end_char+=75;
							} while (end_char<strlen(line_buffer));
							fprintf(outfile,"%s\n",&line_buffer[last_end_char]);
						}

						line_buffer[0]=0;
							/* Nullify the buffer */
					}
				}
			} while (!feof(infile));

			fclose(infile);
			fclose(outfile);

		} else {
			fclose(infile);
			printf("Error opening %s for writing.\n",new_name);
			exit(3);
		}

	} else {
		printf("Error opening %s for read.\n",old_name);
		exit(2);
	}
	return 0;
}

