/*
@(#)  File Name: CGT_onintr.c  Release 1.6  Date: 04/01/08, 21:56:15
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       CGT_onintr.c
         EXE. NAME       :       Check_GTACS
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        06/00       B. Hageman      Initial creation
         8.4        mar16-2004   K. Dolinh       check socket before closing 

********************************************************************************
         Invocation:
        
             CGT_onintr ()
        
********************************************************************************
         Functional Description:

         This routine shuts down the Check_GTACS process cleanly.
	
*******************************************************************************/

/**
    send shutdown message to IFM
    shutdown and close sockets
    send a message indicating that Check_GTACS is exiting
    EXIT
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "strmerr.h"
#include "strmutil.p"
#include "net_util.h"

#include "CGT_params.h"
#include "CGT_struct.h"
#include "CGT_varsex.h"
#include "util_log.h"

void CGT_onintr (int sig)
{
	char msgtxt [MAXMSGTEXT];
	char buffer [MAXBUFFLEN];
	int length;
	int i;
	
	/* close all opened sockets between gaim and gtacs servers */
	for (i=0; i < num_gtacs_hosts; i++)
	{
		if (goplist[i].dcr_server_socket != -1) {
			FD_CLR(goplist[i].dcr_server_socket, &rd_mask);
			close (goplist[i].dcr_server_socket);
			goplist[i].dcr_server_socket = -1;
		}
	}
	
		
	/*  Send shutdown message to IFM  and close socket */
	if (ifm_server_socket != -1)
		{
		strcpy (buffer, "zzzzzzzz");
		length = net_write (ifm_server_socket, buffer, strlen(buffer));
		FD_CLR(ifm_server_socket, &rd_mask);		
  		/* shutdown (ifm_server_socket, 2); */
 		close (ifm_server_socket);
		ifm_server_socket = -1;
		}

	 FD_ZERO (&rd_mask); 

	/*  write event message  */
	log_event("Check_GTACS is exiting gracefully");
		
	exit (0); 
}

