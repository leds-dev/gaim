/*
@(#)  File Name: CGT_connect_to_epoch.c  Release 14  Date: 09/01/2009, 18:34:43
*/

/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       GOES N-Q
         HOST SYSTEM     :       SARCS
         SOURCE          :       CGT_connect_to_epoch.c
         EXE. NAME       :       Check_GTACS
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        06/00       B. Hageman      Initial creation
	 8.4         mar16-2004  K. Dolinh       use rsh("ps...")
	 12B         010802007   B. Miller       change rsh to more-secure ssh
         14.0        10/09       D. Robinson     SSGS-1075  New GTACS file structure for 
                                                 software releases
                                                 Modified code to look for executable in bin
                                                 instead of /epoch/bin because now running
												 from epochsw/bin.  Just left it generic.
         14.0	     10/09       M. Dalal        SSGS-1052  Modified to timeout if
  						 non-responsive GTACS is found. This
						 is accomplished via first pinging
						 the host.

********************************************************************************
         Invocation:

             CGT_connect_to_epoch ()

********************************************************************************
         Functional Description:

         This routine checks if archive manager is running on a GTACS server
	 (for all GTACS)

*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"
#include "net_util.h"

#include "CGT_params.h"
#include "CGT_struct.h"
#include "CGT_varsex.h"

#define PING_TIMEOUT 3

void log_event (char *fmt,...);
void log_debug (char *fmt,...);
void log_err   (char *fmt,...);

void CGT_connect_to_epoch (void)
{
int 	i;
int 	status;
int	found;
int	send_ifm;
char    *read_status;
char 	cmd[MAXBUFFLEN];
char 	fname[MAXBUFFLEN];
char	line[MAXBUFFLEN];
char 	msg[MAXBUFFLEN];
FILE 	*fp;

/* for each gtacs server, find out if ground stream is up by
   executing a remote shell command "ps .." to look for archive manager
   process (assumed to be ../bin/arch_mgr */

for (i=0; i< num_gtacs_hosts; i++) {

	/* set foundflag to false */
	found = 0;
	/* send IFM sendflag to false */
	send_ifm = 0;

	/* MSD:10/2009: SSGS-1052 */
	/* Ping the host first before trying to ssh it to prevent it from hanging on dead hosts */
	sprintf(cmd, "ping %s -c 4 -W %d >/dev/null 2>/dev/null", goplist[i].gtacs_host,
		PING_TIMEOUT);
	if(!system(cmd)) {
	   /* construct and execute remote ssh command on gtacs server */
	   sprintf (fname,"/tmp/check_gtacs_%s",goplist[i].gtacs_host);
	   sprintf (cmd,"ssh %s \"ps -eaf | grep arch_mgr\" >%s.out 2>%s.err",
		    goplist[i].gtacs_host,fname,fname);
	   system(cmd);
	   /* search output of ssh ps command for archive manager process */
	   sprintf (fname,"/tmp/check_gtacs_%s.out",goplist[i].gtacs_host);
	   fp = fopen(fname, "r");
	   if (fp) {
		while (!feof(fp) && found==0) {
			/* read a line */
			read_status = fgets(line, sizeof(line), fp);
			/* set flag if line contains text */
			if (read_status) {
				if (strstr(line,"/bin/arch_mgr")) found = 1;
			}
		}
		fclose(fp);
	   }
	}
	if (found) {
		/* process arch_mgr is running on gtacs server
		   send msg to IFM if first-time status or status changed */
		if (goplist[i].operational == -1 || goplist[i].operational == 0) {
			goplist[i].operational = 1;
			sprintf(msg, "GTACS %d UP ", i);
			send_ifm = 1;
		}

	} else {
		/* process arch_mgr is not running on gtacs server
		   send msg to IFM if first-time status or status changed */
		if (goplist[i].operational == -1 || goplist[i].operational == 1) {
			goplist[i].operational = 0;
			sprintf(msg, "GTACS %d DN ", i);
			send_ifm = 1;
		}
	}
	/*  Send message to IFM  */
	if (send_ifm) {
		status = net_write (ifm_server_socket, msg, strlen(msg));
		log_event("status=%d   ifm msg=(%s)",status,msg);
	}

} /* end do loop */

return;
}  /* end CGT_connect_to_epoch */
