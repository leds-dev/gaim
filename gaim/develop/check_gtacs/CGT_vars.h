/*  
@(#)  File Name: CGT_vars.h  Release 1.1  Date: 00/11/10, 13:26:47
*/

#ifndef CGT_VARS_H
#define CGT_VARS_H
/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         INCLUDE         :       CGT_vars.c
         EXE. NAME       :       Check_GTACS
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        06/00       B. Hageman      Initial creation

********************************************************************************
         Functional Description:
         
         This include file contains variables that are used by the 
         Check_GTACS process.
	
*******************************************************************************/

#include <time.h>
#include "CGT_struct.h"
#include "strmutil.p"

char			*debugfilename;
char			debug_message[MAXMSGTEXT];

fd_set			dt_mask;
fd_set			rd_mask;
int 			ifm_server_socket; 

struct timeval		client_timeout;
struct timeval 		select_time;
client_connection	*gs_connect_id[MAXGTACSHOSTS];
client_point		*gaim_mon_rate_pid[MAXGTACSHOSTS];
struct gtacs_op_list 	goplist[MAXGTACSHOSTS];
int			num_gtacs_hosts;

#endif
