/*  
@(#)  File Name: CGT_read_config.c  Release 1.4  Date: 04/03/17, 18:34:48
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       CGT_read_config.c
         EXE. NAME       :       Check_GTACS
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        06/00       B. Hageman      Initial creation
	 8.1         01/03       K. Dolinh       init ->operational to -1
	 8.4         mar16-2004  K. Dolinh       add log_event

********************************************************************************
         Invocation:
        
             CGT_read_config ()
                         
********************************************************************************
         Functional Description:

         This routine reads both the GAIM and EPOCH configuration files.
	
*******************************************************************************/

/**
    open the GAIM configuration file
    IF open is not successful THEN
        EXIT
    ENDIF
    DOWHILE there are lines in the file to be read
        IF first 5 characters are "GTACS" THEN
            get GTACS host name and address
        ENDIF
    ENDDO
    close the GAIM configuration file
    open the EPOCH configuration file
    IF open is not successful THEN
        EXIT
    ENDIF
    DOWHILE there are lines in the file to be read
        IF first 6 characters are "STREAM" THEN
            get stream host name and stream name
            IF this is a ground equipment stream THEN
                DOFOR each GTACS
                    IF stream host name is GTACS host name THEN
                        set ground stream name for this GTACS
                        BREAK
                    ENDIF
                ENDDO
            ENDIF
        ENDIF
    ENDDO
    close the EPOCH configuration file
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "CGT_params.h"
#include "CGT_struct.h"
#include "CGT_varsex.h"
#include "util_log.h"

void CGT_read_config(void)
{
	int i;
	char msgtxt [MAXMSGTEXT];
	char str [MAXLINELEN];
	char gaimstring [7];
	char hoststring [MAXHOSTLEN];
	char ratestring [6];
	char temp_host[MAXHOSTLEN];
	char temp_stream[MAXSTRMLEN];
	char *p;
	FILE *fp;
	
	/*  Open the GAIM Configuration File  */
	
	if ((fp = fopen ("gaim.cfg", "r")) == NULL)
		{
		log_event_err("cannot open configuration file gaim.cfg");		
		exit(1);
		}
		
	/*  Read the GAIM Configuration File  */
	
	while (!feof(fp))
		{
		if (fgets (str, MAXLINELEN, fp))
			{
			if (strncmp (str, "GTACS", 5) == 0)
				{
				p = strtok (str, " ");
				p = strtok ('\0'," ");
				strncpy (goplist[num_gtacs_hosts].gtacs_host, p, strlen(p));
				p = strtok ('\0'," ");
				strncpy (goplist[num_gtacs_hosts].gtacs_addr, p, strlen(p)-1);
				goplist[num_gtacs_hosts].operational = -1;
				goplist[num_gtacs_hosts].dcr_server_socket = -1;
				num_gtacs_hosts += 1;
				}
			}
		}

	/*  Close the GAIM Configuration File  */
	
	if (fclose (fp))
		log_event_err("cannot close configuration file gaim.cfg");
	
	/*  Open the EPOCH Configuration File  */
	
	if ((fp = fopen ("epoch.cfg", "r")) == NULL)
		{
		log_event_err("cannot open configuration file epoch.cfg");		
		exit(1);
		}
		
	/*  Read the EPOCH Configuration File  */
	
	while (!feof(fp))
		{
		if (fgets (str, MAXLINELEN, fp))
			{
			if (strncmp (str, "STREAM", 6) == 0)
				{
				p = strtok (str, " \n\r\t");	/* STREAM */
				p = strtok ('\0'," \n\r\t");	/* stream name */
				strcpy (temp_stream, p);
				p = strtok ('\0'," \n\r\t");	/* HOST */
				p = strtok ('\0'," \n\r\t");	/* host name */
				strcpy (temp_host, p);
				p = strtok ('\0'," \n\r\t");	/* TYPE */
				p = strtok ('\0'," \n\r\t");	/* stream type */
				
				if (strncmp (p, "ground_equipment", 16) == 0)
					{
					for (i=0; i< num_gtacs_hosts; i++)
						{
						if (strcmp (temp_host, goplist[i].gtacs_host) == 0)
							{
							strcpy (goplist[i].grndstrm, temp_stream);
							break;
							}
						}
					}
				}
			}
		}

	/*  Close the EPOCH Configuration File  */
	
	if (fclose (fp))
		log_event_err("cannot close configuration file epoch.cfg");
	
	return;
}

