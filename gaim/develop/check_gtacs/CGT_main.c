/*
@(#)  File Name: CGT_main.c  Release 1.23  Date: 07/06/22, 14:52:36
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       CGT_main.c
         EXE. NAME       :       Check_GTACS
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        06/00       B. Hageman      Initial creation
	 	 8.2         mar02-2004  K. Dolinh       - Add log_event, log_debug
	                                         	 - Clean up code
						 						 - Use rsh instead of socket
						 						 - send status to ifm only on change
                                                 - parse params using getopt
     8.4     mar11-2004     	 K. Dolinh
	 8.5	 mar22-2004
	 8.7	 apr15-2004
	 8.8     may06-2004
	 8.B     sep17-2004                      - GTACS Build 8B   
	 9.1     jan06-2005                      - GTACS Build 9.1 
	 9A      feb23-2005                      - BUILD 9A           
	 9AP     mar16-2005                      - BUILD 9A Prime 
	 	 	 apr04-2005          		 	 - BUILD 9A Prime.2
	 	 	 may01-2005          		 	 - BUILD 9B
		 	 aug03-2005					 	 - BUILD 9C
		 	 aug26-2005					 	 - BUILD 10A
		 	 sep09-2005					 	 - BUILD 10A.2
			 jan12-2006					 	 - BUILD 10B
			 jul10-2006					 	 - BUILD 10C
			 Aug15-2006						 - Build 11
			 Apr13-2007 					 - Build 11
			 Jun22-2007 					 - Build 12
			 Jan09-2008 					 - Build 12B
			 			 		 
********************************************************************************
         Invocation:

             Check_GTACS -D -d

         Parameters:

             d,D	enable debug message output

********************************************************************************
         Functional Description:

         The Check_GTACS Process determines which of the GTACS are
         operational.

*******************************************************************************/

/**
    initialize the debug file name
    write log message
    CALL CGT_init
    initialize signal handler routines
    CALL CGT_read_config
    connect to the GAIM Interface Manager
    DOFOR ever
        sleep for 5 seconds
        CALL CGT_connect_to_epoch
        IF input on a socket THEN
            IF input is on the IFM socket THEN
                CALL CGT_process_ifm_msg
            ENDIF
        ENDIF
    ENDDO
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>             /* Variable-length argument lists. */
#include <unistd.h>
#include "strmerr.h"
#include "strmutil.p"
#include "net_util.h"

#include "CGT_params.h"
#include "CGT_struct.h"
#include "CGT_vars.h"

extern void CGT_connect_to_epoch (void);
extern void CGT_init (void);
extern void CGT_onintr (int);
extern void CGT_process_ifm_msg (void);
extern void CGT_read_config (void);

#define SW_VERSION "Build 12B Jan-09-2008"
#define SW_ID      "CGT"

#include "../include/util_log.h"

/* forward define */
int getopt(int argc, char* const argv[], const char* optstring);

extern int optopt;

void CGT_write_gtacs_status (void);
static	int  statusfile_open_err = 0;
static char statusfile_name[1024];
FILE *statusfile_ptr = NULL;
time_t process_starttime;

int main (int argc, char *argv[])
{

int c;
int length, status;
char buffer [MAXBUFFLEN];
char msgtxt [MAXMSGTEXT];
char syscall [MAXMSGTEXT];
char servifm [MAXSERVLEN];
char servubg [MAXSERVLEN];

/* save process start time */
time(&process_starttime);

/* parse arguments to process */
while ((c = getopt (argc, argv, "dD")) != -1) {
    switch (c)
      {
      case 'd':
        debug = 1;
        break;
      case 'D':
        debug = 1;
        break;
      default:
        log_err("*** Unknown exec option = %c", optopt);
      }
}

/* get env variables */
gaim_home = (char *) getenv("GAIM_HOME");
if (!gaim_home) {
	/* exit if env var not defined */
	log_err("*** env variable GAIM_HOME is not defined");
	log_err("*** check_gtacs terminating");
	exit(1);
}

if (getenv("GAIM_DEBUG")) debug = 1;

/* init log message ID */
sprintf(logfile_msgid,"%s",SW_ID);
/* write generic startup messages */
log_event("  ");
log_event("check_gtacs (%s) started ...",SW_VERSION);
log_event("logfile_name    = (%s)",logfile_name);
log_event("debug = %d",debug);

/* Open status file for write-truncate */
sprintf(statusfile_name,"%s/output/cgt.sta",gaim_home);
statusfile_ptr = fopen(statusfile_name,"w");
if (statusfile_ptr==NULL) {
	log_event_err("unable to open status file = %s",statusfile_name);
}

/* write cgt startup params */
log_event("statusfile_name = (%s)",statusfile_name);
log_event("  ");

/*  Initialize CGT variables  */
CGT_init ();

/*  Initialize signal handler routines  */
signal (SIGINT, CGT_onintr);
signal (SIGTERM, CGT_onintr);
signal (SIGPIPE, CGT_onintr);

/*  Read the GAIM and EPOCH configuration files  */
CGT_read_config ();

/*  Connect to the GAIM Interface Manager  */
strcpy (servifm, IFM_SERVICE);

if (net_call (NULL, servifm, &ifm_server_socket)) {
	log_event_err("Error connecting to IFM");
	/*** CGT_onintr (); */
} else {
	log_event("Connected to IFM");
	FD_SET (ifm_server_socket, &rd_mask);
}

for (;;) {

	/*  determine status of the GTACS systems  */
	CGT_connect_to_epoch ();
	/* output GTACS status to file */
	CGT_write_gtacs_status ();
	/* wait */
	sleep (5);

	/*  Check for input on all of the sockets  */
	dt_mask = rd_mask;

	if (select (FD_SETSIZE, &dt_mask, NULL, NULL, &select_time) > 0) {
		/*  Process input on IFM server socket  */
		if (ifm_server_socket > 0) {
			if (FD_ISSET (ifm_server_socket, &dt_mask)) {
				CGT_process_ifm_msg ();
			}
		}
	}

} /* enddo forever */

}


void CGT_write_gtacs_status (void)
{
int i;
int status;
time_t current_time;
struct tm current_time_str;

/* return if status file not open */
if (!statusfile_ptr) return;

/* position to beginning of file */
fseek(statusfile_ptr,0,SEEK_SET);

/* write program start time */
current_time_str=*localtime(&process_starttime);
fprintf(statusfile_ptr,"\nCGT  Start Time:  GMT %4.4i-%3.3i-%2.2i:%2.2i:%2.2i\n",
                  current_time_str.tm_year+1900,current_time_str.tm_yday+1,
                  current_time_str.tm_hour,current_time_str.tm_min,
                  current_time_str.tm_sec);


/* write current time */
status = time(&current_time);
current_time_str=*localtime(&current_time);
fprintf(statusfile_ptr,"CGT Status Time:  GMT %4.4i-%3.3i-%2.2i:%2.2i:%2.2i\n\n",
                  current_time_str.tm_year+1900,current_time_str.tm_yday+1,
                  current_time_str.tm_hour,current_time_str.tm_min,
                  current_time_str.tm_sec);

/* write status for all gtacs to file */
for (i=0; i< num_gtacs_hosts; i++) {

	if (goplist[i].gtacs_host) {
		if (goplist[i].operational != 0) {
        	fprintf(statusfile_ptr,"%16.16s UP\n",goplist[i].gtacs_host);
		} else {
        	fprintf(statusfile_ptr,"%16.16s DOWN\n",goplist[i].gtacs_host);
		}
	}

} /* end do for */

/* write blank line and force file buffer output */
fprintf(statusfile_ptr," \n");
fflush(statusfile_ptr);

} /* end CGT_write_status */

#include "../include/util_log.c"


