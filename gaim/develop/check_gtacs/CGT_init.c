/*  
@(#)  File Name: CGT_init.c  Release 1.4  Date: 04/03/17, 18:34:44
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       CGT_init.c
         EXE. NAME       :       Check_GTACS
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        06/00       B. Hageman      Initial creation

********************************************************************************
         Invocation:
        
             CGT_init()
                         
********************************************************************************
         Functional Description:

         This routine initializes variables used by Check_GTACS.
	
*******************************************************************************/

/**
    Initialize variables needed by this process
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"

#include "CGT_params.h"
#include "CGT_struct.h"
#include "CGT_varsex.h"

void CGT_init (void)
{
	client_timeout.tv_sec = 60;
	client_timeout.tv_usec = 0;
	select_time.tv_sec  = 0;
	select_time.tv_usec = 200000;		

	FD_ZERO (&rd_mask);
	FD_SET(0, &rd_mask); /* stdin's file descriptor */

	ifm_server_socket = -1;
	num_gtacs_hosts = 0;
	
	return;
}
