/*  
@(#)  File Name: CGT_varsex.h  Release 1.1  Date: 00/11/10, 13:26:47
*/

#ifndef CGT_VARSEX_H
#define CGT_VARSEX_H
/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         INCLUDE         :       CGT_vars.c
         EXE. NAME       :       Check_GTACS
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        06/00       B. Hageman      Initial creation

********************************************************************************
         Functional Description:
         
         This include file contains variables that are used by the 
         Check_GTACS process.
	
*******************************************************************************/

#include <time.h>
#include "strmutil.p"
#include "CGT_struct.h"

extern char			*debugfilename;
extern char			debug_message[MAXMSGTEXT];
extern int			debug;

extern fd_set			dt_mask;
extern fd_set			rd_mask;
extern int 			ifm_server_socket; 

extern struct timeval		client_timeout;
extern struct timeval 		select_time;
extern client_connection	*gs_connect_id[MAXGTACSHOSTS];
extern client_point		*gaim_mon_rate_pid[MAXGTACSHOSTS];
extern struct gtacs_op_list 	goplist[MAXGTACSHOSTS];
extern int			num_gtacs_hosts;
#endif
