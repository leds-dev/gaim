/*  
@(#)  File Name: CGT_struct.h  Release 1.2  Date: 03/08/07, 16:15:47
*/

#ifndef CGT_STRUCT_H
#define CGT_STRUCT_H

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         HEADER          :       CGT_struct.h
         EXE. NAME       :       Check_GTACS
         PROGRAMMER      :       B. Hageman
	
         VER.        DATE        BY              COMMENT
         V1.0        06/00       B. Hageman      Initial creation

********************************************************************************
         Functional Description:

         This header file contains structures used by the routines in 
         the Check_GTACS process.
	
*******************************************************************************/

struct gtacs_op_list {
	char gtacs_host [MAXHOSTLEN];
	char gtacs_addr [MAXHOSTLEN];
	char grndstrm [MAXSTRMLEN];
	int  dcr_server_socket;
	short operational;
};
#endif
