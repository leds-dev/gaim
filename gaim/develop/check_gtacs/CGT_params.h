/*
@(#)  File Name: CGT_params.h  Release 1.2  Date: 04/03/17, 18:34:46
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         INCLUDE         :       CGT_params.c
         EXE. NAME       :       Check_GTACS
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        06/00       B. Hageman      Initial creation

********************************************************************************
         Functional Description:

         This header file contains parameters used by the routines in
         the Check_GTACS process.

*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#ifndef TRUE
#  define TRUE (1)
#endif
#ifndef FALSE
#  define FALSE (0)
#endif
#ifndef NULL
#  define NULL 0
#endif

#define IFM_SERVICE	"gaim_ifm_cgt"       /* IFM to CGT socket name prefix */

#define MAXBUFFLEN	256	/* max number of characters in buffer         */
#define MAXNBUFLEN	-256	/* max number of characters in buffer         */
#define MAXDBLEN	256	/* max number of characters in database name  */
#define MAXLINELEN	512	/* max number of characters in epoch.cfg line */
#define MAXMSGTEXT	256	/* max number of characters in message text   */
#define MAXGASHOSTS	10	/* max number of GOES Archive host machines   */
#define MAXGTACSHOSTS	12	/* max number of GTACS host machines          */
#define MAXHOSTLEN	21	/* max number of characters in host name      */
#define MAXNUMPB	4	/* max number of playback streams             */
#define MAXNUMRT	16	/* max number of realtime streams             */
#define MAXNUMSIM	4	/* max number of simulation streams           */
#define MAXPROCLEN	21	/* max number of characters in process name   */
#define MAXSCIDLEN	3	/* max number of characters in scid name      */
#define MAXSERVLEN	51	/* max number of characters in service name   */
#define MAXSTRMLEN	21	/* max number of characters in stream name    */

#define ERRMSG "(ERR)"
