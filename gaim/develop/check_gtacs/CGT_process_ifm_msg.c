/*
@(#)  File Name: CGT_process_ifm_msg.c  Release 1.3  Date: 04/03/17, 18:34:47
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       CGT_process_ifm_msg.c
         EXE. NAME       :       Check_GTACS
         PROGRAMMER      :       B. Hageman

         VER.        DATE        BY              COMMENT
         V1.0        06/00       B. Hageman      Initial creation
	 8.4         mar16-2004  K. Dolinh       add log_event
	 
********************************************************************************
         Invocation:

             CGT_process_ifm_msg

********************************************************************************
         Functional Description:

         This routine processes messsages received from the N-Q_Interface_Manager
         process.

*******************************************************************************/

/**
    read the data from the socket
    IF the data length is greater than 7 THEN
        IF first 8 characters of data are "zzzzzzzz" THEN
            write log message
            CALL CGT_onintr
        ENDIF
    ENDIF
    RETURN
**/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "strmerr.h"
#include "strmutil.p"
#include "net_util.h"

#include "CGT_params.h"
#include "CGT_struct.h"
#include "CGT_varsex.h"
#include "util_log.h"

void CGT_onintr (int sig);

void CGT_process_ifm_msg (void)
{
	char buffer [MAXBUFFLEN];
	char msgtxt [MAXMSGTEXT];
	int length;

	/*  Read the data from the socket  */
	length = net_read (ifm_server_socket, buffer, MAXNBUFLEN);

	if (length > 7)
		{
		if (strncmp (buffer, "zzzzzzzz", 8) == 0)
			{
			/* Got a shutdown message from IFM  */
			log_event("received shutdown message from IFM");
			/* Shutdown */
			CGT_onintr (0);
			}
		}

	return;
}

