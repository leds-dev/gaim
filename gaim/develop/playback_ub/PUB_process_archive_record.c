/*  
@(#)  File Name: PUB_process_archive_record.c  Release 1.8  Date: 07/04/13, 11:09:42
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       PUB_process_archive_record.c
         EXE. NAME       :       Playback_Update_Buffer
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
		 8.5	     mar23-2004  K. Dolinh	 	 add log_event, log_debug
	 	 11			 apr12-2007					 enable case GLOBAL_VAR
		 12A		 sep01-2007                  make (NOT_SENT) event optional
												 

********************************************************************************
	Invocation:
        
		PUB_process_archive_record()
        
	Parameters:

		NONE

********************************************************************************
	Functional Description:

		This function scans through the archive file and extracts the globals 
		and frames requested by the shared archive.

*******************************************************************************/

/**

PDL:
	IF this is a new archive file and we are not waiting for a response from the
	 associated Update_Buffer_Generator process and we are connected to the 
	 associated Update_Buffer_Generator process THEN
		Extract the database name from the archive file.
		Send the database name to the associated Update_Buffer_Generator 
		 process.
		Set the 'waiting-for-a-response' flag.
	ELSEIF we are not waiting for a response from the associated 
	 Update_Buffer_Generator process and we are connected to the associated 
	 Update_Buffer_Generator process THEN
		IF there is data in the archive file and we are not at the end of the 
		 file THEN
			DOUNTIL an archive record has been found that is within the 
			 requested data time range or we have reached the end of the file.
				Read the next archive record.
				Check the time stamp on the record.
			ENDDO
		ENDIF
		IF the read archive record is a partial record or an end-of-file record
		 THEN
			Signal the end of the current archive file has been reached.
		ELSE
			DOCASE archive record type
				CASE 1: Archive record is a telemetry frame
					Extract all important information from the telemetry frame.
					IF the telemetry frame falls within our requested time 
					 range THEN
						Mark the frame for sending to the 
						 Update_Buffer_Generator.
					ELSE
						Ignore the telemetry frame.
					ENDIF
				ENDCASE

				CASE 2: Archive record is one of three event message types
				CASE 3: Archive record is a global value
					Skip this record
				ENDCASE

				CASE 4: Archive record is a pseudotelemetry record
					IF this pseudotelemetry record is within the requested time
					 range THEN
						IF there are pseudotelemetry points waiting to be 
						 forwarded and the time difference between the first 
						 and current pseudotelemetry point is more than 65 
						 seconds THEN
							Copy the pseudotelemetry list so that it can be 
							 sent at the next possible time.
						ENDIF
						IF a new pseudotelemetry packet needs to be started THEN
							Initialize the new packet.
						ENDIF
						Copy the current pseudotelemetry point to the active 
						 pseudotelemetry packet.
						IF the size of our pseudotelemetry packet has reached 
						 the maximum allowed size THEN
							Copy the pseudotelemetry list so that it can be sent
							 at the next possible time.
						ENDIF
					ENDIF
				ENDCASE

				CASE 5: An unused case that used to be used for Global 
				 Variables.  NOTE: This case has been kept in here just in case
				 there is a CCR request to change the software back to 
				 forwarding global variables to the GOES Shared Archive.
					IF the time of the global variable is within our requested 
					 time range THEN
						IF there are global variables waiting to be forwarded 
						 and the time difference between the first and current 
						 global variable is more than 65 seconds THEN
							Copy the global variable list so that it can be sent
							 at the next possible time.
						ENDIF
						IF a new global variable packet needs to be started THEN
							Initialize the new packet.
						ENDIF
						Copy the current global variable value to the active 
						 global variable packet.
						IF the size of our global variable packet has reached 
						 the maximum allowed size THEN
							Copy the global variable list so that it can be sent
							 at the next possible time.
						ENDIF
					ENDIF
				ENDCASE

				CASE 6: Archive record is an End of Block Record
					Ignore this record.
				ENDCASE

				CASE 7: Archive record is an End of File Record
					Set a flag stating that the end of the current archive file
					 has been reached.
				ENDCASE

				DEFAULT CASE: 
					Write a message to the debug file stating that this record 
					 is an unknown record type.
				ENDCASE
			ENDDO
		ENDIF

		IF the telemetry buffer is ready to send to the Update_Buffer_Generator
		 and we are not in the middle of clearing buffers after an end of file 
		 THEN
			Send the telemetry frame packet to the Update_Buffer_Generator.
			Calculate the checksum of the packet.
		ELSEIF the pseudotelemetry packet is ready to send or we are emptying 
		 out all buffers from memory due to an end of file THEN
			IF we are clearing out memory due to end of file THEN
				Copy the pseudotelemetry creation buffer to the pseudotelemetry
				 output buffer.
			ENDIF
			IF there is pseudotelemetry to send THEN
				Send the packet to the Update_Buffer_Generator
				Calculate the checksum of the packet.
			ENDIF
		ENDIF
	ENDIF

	IF the end of file has been reached and we have finished emptying all 
	 buffers from memory THEN 
		Close the archive file.
	ENDIF

**/

#include "util_log.h"
#include "outp_util.h"
#include "net_util.h"

#include "PUB_defines.h"

void process_gva (void);
void GAIM_seconds_to_date(char *outdate,double secs);
int GAIM_calculate_checksum(int buffer_length,char *buffer);

extern  int		debug_gva;
extern  int		gaim_playback;

gvar    		*gid;
char 			*gva_buf;
static char 	gva_name[MNEMONIC_SIZE];
static int 		gva_index;
static int 		gva_tag;
static u_int 	gva_status;
int				gva_send;
int				gva_size;
static	double	gva_dvar;
static	u_int	gva_lvar;

static int firsttime = 1;

/* pointer to stream info */	
extern streamdef *stream_def;

void PUB_process_archive_record(void) {

struct timeval32 pseudo_time;
struct timeval32 gva_time;
struct timeval32 the_frame_time_64 = {0,0}; 

char message[256];
int status;
int i;
	
/* Convert seconds to date. */
char newdate[32];
	
/* Temporary integer storage */
int tempint=0;
unsigned int tempuint=0;
double tempdouble=0;
	
/* If this is a new archive file, and we are not waiting for a
	response, and we are connected to UBG */
if (new_archive && !waiting_for_response && !ubg_no_connection) {

	/* Point to the start of the buffer */
	tlm_buffer_ptr = tlm_buffer;
	tlm_message_length = 0;
		
	/* Point to the start of the buffer */
	memcpy(tlm_buffer_ptr,&packet_ids[UBG_DBASE_PKT_NUMBER],2);
	tlm_buffer_ptr += 2;
	tlm_message_length += 2;
		
	/* Include NULL pointer in count. */
	dbase_name_len = strlen(database_name)+1;
		
	/* Copy database name length */
	memcpy(tlm_buffer_ptr,&dbase_name_len,2);
	tlm_buffer_ptr += 2;
	tlm_message_length += 2;
		
	/* Copy the database name */
	memcpy(tlm_buffer_ptr,database_name,dbase_name_len);
	tlm_message_length += dbase_name_len;
	
	/* Send the database name. */	
	log_event("send database name %s to UBG",database_name);
	net_write(read_write_ubg,tlm_buffer,tlm_message_length);
	last_crc = GAIM_calculate_checksum(tlm_message_length,tlm_buffer);
		
	/* Set flags */
	waiting_for_response = -1;
	new_archive = 0;
	pseudo_ctr = 0;
	global_ctr = 0;	
		
/* Only process next archive block if not waiting for a response 
   from UBG from the last packet sent */
} else if (!waiting_for_response && !ubg_no_connection) {
		
	if (archive_pointer->arch_len==-1 && !end_of_file) {
		do {
			/* Read the next archive block */
			status = archive_read(archive_pointer);
				
			/* Read the next archive block */
			block_counter++;

			if (status) end_of_file=-1;
			
			/* Swap bytes, if needed */
			outp_timeval32(&archive_pointer->arch_block.start_time);
			outp_timeval32(&archive_pointer->arch_block.stop_time);
				
			/* Set the time to use for global variables */
			global_time.tv_sec = archive_pointer->arch_block.start_time.tv_sec;
			global_time.tv_usec = archive_pointer->arch_block.start_time.tv_usec;
				
			if ((block_counter%100)==0 && !end_of_file) {
				GAIM_seconds_to_date(newdate,(double)archive_pointer->arch_block.start_time.tv_sec);
				log_debug("processing archive block # %i (start time %s)",block_counter,newdate);
			} 

		} while ((archive_pointer->arch_block.start_time.tv_sec>=stop_secs ||
			      archive_pointer->arch_block.stop_time.tv_sec<start_secs) && 
		          !end_of_file);
	}

	/* Swap bytes, if needed */
	outp_int(&archive_pointer->arch_len);
			
	/* Copy next record header */
	archive_buffer = archive_pointer->arch_buf+archive_pointer->arch_len;
	memcpy((char *)&rec,archive_buffer,HEADERSIZE);
		
	/* Swap bytes as needed */
	outp_short(&rec.type);
	outp_short(&rec.size);
		
	if (archive_pointer->arch_len + rec.size>ARCHBLOCK || end_of_file) {
	
		/* If partial record or end of file, do nothing */
		archive_pointer->arch_len = -1;
		
	} else {
	
		/* use greater of global or frame time */
		if ((double)global_time.tv_sec+
			(double)global_time.tv_usec/1000000.0 >
			(double)the_frame_time.tv_sec+
			(double)the_frame_time.tv_usec/1000000.0) 
				current_time=global_time;
		else current_time=the_frame_time;

		/* truncate usec to simplify time compare */
		current_time.tv_usec = 0;			
		switch (rec.type) {
		
			/* Telemetry Frame */
			case TLMFRAME:
				
				/* Point to the start of the buffer */
				tlm_buffer_ptr = tlm_buffer;
				tlm_message_length = 0;
					
				/* Copy packet id */
				memcpy(tlm_buffer_ptr,&packet_ids[UBG_FRAME_PKT_NUMBER],2);
				tlm_buffer_ptr += 2;
				tlm_message_length += 2;
					
				tlm_frame_overlay=(struct tlm_frame *)(archive_buffer+sizeof(rec));
				memcpy(&the_frame_time,&tlm_frame_overlay->header.receipt_time.tv_sec,sizeof(struct timeval32));
				/* Extract the frame time from the frame */
				outp_timeval32(&the_frame_time);
					
				/* Add the frame time to the frame packet */
				memcpy(tlm_buffer_ptr,&the_frame_time.tv_sec,4);
				tlm_buffer_ptr += 4;
				tlm_message_length += 4;

				memcpy(tlm_buffer_ptr,&the_frame_time_64.tv_sec,4);
				tlm_buffer_ptr += 4;
				tlm_message_length += 4;

				memcpy(tlm_buffer_ptr,&the_frame_time.tv_usec,4);
				tlm_buffer_ptr += 4;
				tlm_message_length += 4;

				memcpy(tlm_buffer_ptr,&the_frame_time_64.tv_usec,4);
				tlm_buffer_ptr += 4;
				tlm_message_length += 4;

        /* receipt time */
				memcpy(tlm_buffer_ptr,&the_frame_time.tv_sec,4);
				tlm_buffer_ptr += 4;
				tlm_message_length += 4;

				memcpy(tlm_buffer_ptr,&the_frame_time_64.tv_sec,4);
				tlm_buffer_ptr += 4;
				tlm_message_length += 4;

				memcpy(tlm_buffer_ptr,&the_frame_time.tv_usec,4);
				tlm_buffer_ptr += 4;
				tlm_message_length += 4;

				memcpy(tlm_buffer_ptr,&the_frame_time_64.tv_usec,4);
				tlm_buffer_ptr += 4;
				tlm_message_length += 4;

				/* Copy the telemetry frame, ignoring the archive header */
				memcpy(tlm_buffer_ptr,archive_buffer+sizeof(rec)+8,rec.size-8);
				tlm_message_length += (rec.size-8);
					
				/* Move the archive pointer */
				archive_pointer->arch_len += HEADERSIZE+rec.size;
					
				/* send tlm frame only if time is inside  requested time range */
				if (the_frame_time.tv_sec>=start_secs && the_frame_time.tv_sec<stop_secs) send_tlm = -1;
				else send_tlm = 0;

				break;

			case EVENTTEXTMSG:
			case EVENTBINMSG:
			case EVENTXDRMSG:
					/* Ignore all event messages and pseudos --
						Pseudotelemetry is automatically calculated 
						whenever a frame is decommed and therefore, since 
						we will always be decomming a frame, no need to 
						pass pseudos as a separate packet type to UBG. 
						AND, as of 3/23/00, all globals are to be ignored, 
						too. */

				archive_pointer->arch_len+=HEADERSIZE+rec.size;
				break;
							
			/* Pseudotelemetry Fields */
			case PSEUDOVALUE:
					
				/* Is this pseudo within the time range requested */
				if (current_time.tv_sec>=start_secs && current_time.tv_sec<stop_secs) {
				
					/*
					log_event("(PUB_process_archive_rec) CASE PSEUDO  pseudo_ctr=%d  pseudo_ctr_to_send=%d  pseudo_msglen=%d",
					pseudo_ctr,pseudo_ctr_to_send,pseudo_message_length);
					*/
					
					/* If time difference > 64 secs (64.999-0.000 is ok), start a new packet */		
					if (pseudo_ctr>0 && current_time.tv_sec-first_pseudo.tv_sec>64) {
								
						/* Copy the buffer to send */
						memcpy(pseudos_to_send,pseudo_buffer,MAX_MESSAGE_LENGTH);
						send_pseudo = -1;
							
						/* Save information and zero counter so that the next IF will be executed.*/
						pseudo_message_length_to_send = pseudo_message_length;
						pseudo_ctr_to_send = pseudo_ctr;
						pseudo_ctr = 0;
							
					}

					/* new pseudo packet */
					if (pseudo_ctr == 0) {
											
						/* Reset the pointers to the start of the buffer */
						pseudo_buffer_ptr = pseudo_buffer;
						pseudo_message_length = 0;
							
						/* Copy packet id and zero global count */
						memcpy(pseudo_buffer_ptr,&packet_ids[UBG_PSEUDO_PKT_NUMBER],2);
						pseudo_buffer_ptr[2] = 0;
						pseudo_buffer_ptr[3] = 0;
						pseudo_buffer_ptr += 4;
						pseudo_message_length += 4;
						/* keep track of the first global time */
						memcpy(&first_pseudo,&current_time,8);
						/* save current pseudo_time */
						pseudo_time = current_time;	
						if (debug_gva) log_event("(process_archive_rec) NEW PACKET PSU  first_pseudo=%d",first_pseudo.tv_sec);	
					}
					
					/* Copy Time into buffer */
					/*log_event("(process_archive_rec) CASE PSEUDO  tv_secs=%d  tv_usecs=%d (in buffer)",current_time.tv_sec,current_time.tv_usec);*/
					memcpy(pseudo_buffer_ptr,&current_time,8);
					pseudo_buffer_ptr += 8;
					pseudo_message_length += 8;
					/* save current pseudo_time */
					pseudo_time = current_time;		
					
					/* Increment index by 1 because GSA file uses indices one higher than
                       what is in the EPOCH pseudo file. */
					archive_buffer_ptr = archive_buffer+sizeof(rec);
					memcpy(&tempint,archive_buffer_ptr,4);
					tempint++;
					/*outp_int(&tempint);*/ /* possibly required? outp_setup() never called */
					memcpy(pseudo_buffer_ptr,&tempint,4);
					
					/* Copy Index  into buffer */	
					(*pseudo_buffer_ptr) += 1;  
					pseudo_buffer_ptr += 4; 
					archive_buffer_ptr += 4;
					pseudo_message_length += 4;
						
					/* Copy Status into buffer */
					memcpy(&tempuint,archive_buffer_ptr,4);
					/*outp_uint(&tempuint);*/ /* possibly required? outp_setup() never called */
					memcpy(pseudo_buffer_ptr,&tempuint,4);
					pseudo_buffer_ptr += 4; 
					archive_buffer_ptr += 4;
					pseudo_message_length += 4;
						
					/* Copy Value into buffer */
					memcpy(&tempdouble,archive_buffer_ptr,8);
					/*outp_double(&tempdouble);*/ /* possibly required? outp_setup() never called */
					memcpy(pseudo_buffer_ptr,&tempdouble,8);
					pseudo_buffer_ptr += 8; 
					archive_buffer_ptr += 8;
					pseudo_message_length += 8;
						
					/* send packet if max size reached */
					pseudo_ctr++;
					if (pseudo_message_length > MAX_BUFFER_SIZE) {

						memcpy(pseudos_to_send,pseudo_buffer,MAX_MESSAGE_LENGTH);
						send_pseudo = -1;
						/* Reset the pointers to start of the buffer */
						pseudo_message_length_to_send = pseudo_message_length;
						pseudo_ctr_to_send = pseudo_ctr;
						pseudo_buffer_ptr = pseudo_buffer;
						pseudo_message_length = 0;
						pseudo_ctr = 0;
							
					}
				}

				archive_pointer->arch_len += HEADERSIZE+rec.size;
				break;

			/*case UNUSED_CASE:*/
			case GLOBALVALUE:
						
				/* global is within time range requested */
				if (current_time.tv_sec>=start_secs && current_time.tv_sec<stop_secs) {
	
					/*log_event("(PUB_process_archive_rec) CASE_GLOBAL  global_ctr=%d  global_ctr_to_send=%d  global_msglen=%d",
					global_ctr,global_ctr_to_send,global_message_length);
					*/
						
					if (global_ctr>0 && current_time.tv_sec-first_global.tv_sec>64) {
											
						/* time difference > 64 secs (64.999-0.000 is okay), start new packet */
						memcpy(globals_to_send,global_buffer,MAX_MESSAGE_LENGTH);
						send_global=-1;
						/* Copy the buffer to send */

						/* Reset the pointers to the start of the buffer */
						global_message_length_to_send = global_message_length;
						global_ctr_to_send = global_ctr;
						global_buffer_ptr = global_buffer;
						global_message_length = 0;
						global_ctr = 0;
					}

					/* new global packet */
					if (global_ctr == 0) {
											
						/* Reset the pointers to the start of the buffer */
						global_buffer_ptr = global_buffer;
						global_message_length = 0;

						/* Copy packet id and zero global count */
						/* NOTE: make it a PSEUDO packet */
						/* memcpy(global_buffer_ptr,&packet_ids[UBG_GLOBAL_PKT_NUMBER],2); */
						memcpy(global_buffer_ptr,&packet_ids[UBG_PSEUDO_PKT_NUMBER],2);
						global_buffer_ptr[2] = 0;
						global_buffer_ptr[3] = 0;
						global_buffer_ptr += 4;
						global_message_length += 4;
							
						/* keep track of the first global time */
						/*memcpy(&first_global,&current_time,8);*/
						if (current_time.tv_sec > pseudo_time.tv_sec)
							first_global = current_time;	
						else
							first_global = pseudo_time;
						gva_time = first_global;
						if (debug_gva) log_event("(process_archive_rec) NEW PACKET GVA  first_global=%d",first_global.tv_sec);
					}
					
					/* get value, status, tag for global */
					process_gva();				

					if (gva_send) {
						/* Copy time into buffer */
						/*memcpy(global_buffer_ptr,&current_time,8);*/
						/*log_event("(process_archive_rec) CASE GVA     tv_secs=%d  tv_usecs=%d (in buffer)",first_pseudo.tv_sec,first_pseudo.tv_usec);
						log_event("(process_archive_rec)              tv_secs=%d  tv_usecs=%d (current)",current_time.tv_sec,current_time.tv_usec);
						*/
						if (current_time.tv_sec > pseudo_time.tv_sec) {
							memcpy(global_buffer_ptr,&current_time,8);
							gva_time = current_time;
						} else {
						 	memcpy(global_buffer_ptr,&pseudo_time,8);
							gva_time = pseudo_time;
						}
						global_buffer_ptr += 8;
						global_message_length += 8;

						/* Copy tag into buffer */	
						memcpy(global_buffer_ptr,&gva_tag,4);
						/*(*global_buffer_ptr) += 1;*/  
						global_buffer_ptr += 4; 
						global_message_length += 4;
						
						/* Copy status into buffer */
						memcpy(global_buffer_ptr,&gva_status,4);
						global_buffer_ptr += 4; 
						global_message_length += 4;
						
						/* Copy value into buffer */
						memcpy(global_buffer_ptr,&gva_dvar,8);
						global_buffer_ptr += 8; 
						global_message_length += 8;

						/*send packet if maximum allowed size reached */
						global_ctr++;
						if (global_message_length > MAX_BUFFER_SIZE) {

							memcpy(globals_to_send,global_buffer,MAX_MESSAGE_LENGTH);
							send_global = -1;
							/* Reset pointers to start of buffer */	
							global_message_length_to_send = global_message_length;
							global_ctr_to_send = global_ctr;
							global_buffer_ptr = global_buffer;
							global_message_length = 0;
							global_ctr = 0;

						}
					}
				}

				archive_pointer->arch_len+=HEADERSIZE+rec.size;
				break;

			case ENDOFBLOCK:
				archive_pointer->arch_len=-1;
				break;

			case ENDOFFILE:
				end_of_file=-1;
				break;

			default:
				//log_event_err("Unknown Archive Record type = %i",rec.type);
				archive_pointer->arch_len += HEADERSIZE+rec.size;
				break;
		}
	}

	/* If need to send tlm frame */
	if (send_tlm && clear_buffers==0) {

		GAIM_seconds_to_date(newdate,(double)the_frame_time.tv_sec);
		net_write(read_write_ubg, tlm_buffer, tlm_message_length);
		last_crc = GAIM_calculate_checksum(tlm_message_length, tlm_buffer);
		send_tlm = 0;
		tlm_message_length = 0;
		waiting_for_response = -1;

	/* Are we sending pseudo mnemonic buffer?  Or cleaning up at the end of a file? */
	} else if (send_pseudo || clear_buffers==2) {
			
		if (clear_buffers==2) {
			/* Copy the buffer to send */
			memcpy(pseudos_to_send,pseudo_buffer,MAX_MESSAGE_LENGTH);
			send_pseudo = -1;			
			/* Save information and zero counter so that next IF will be executed.*/
			pseudo_message_length_to_send = pseudo_message_length;
			pseudo_ctr_to_send = pseudo_ctr;
			pseudo_ctr = 0;
		}

		/* As long as there are pseudos to send */
		if (pseudo_ctr_to_send != 0) {
		
			if (debug_gva) {
				log_event("(process_archive_rec) SEND_BUF_PSU n=%d  msglen=%d  pseudo_time: %d %d  gva_time: %d %d",
				pseudo_ctr_to_send,pseudo_message_length_to_send,
				first_pseudo.tv_sec,pseudo_time.tv_sec,first_global.tv_sec,gva_time.tv_sec);
			}			
			GAIM_seconds_to_date(newdate,(double)current_time.tv_sec);
				
			/* Insert pseudo count into packet */
			memcpy(pseudos_to_send+2,&pseudo_ctr_to_send,2);
			
			if (gaim_playback != PLAYBACK_GVA_ONLY) {	
				net_write(read_write_ubg, pseudos_to_send, pseudo_message_length_to_send);
				last_crc = GAIM_calculate_checksum(pseudo_message_length_to_send, pseudos_to_send);
				waiting_for_response = -1;	
			}	
			send_pseudo = 0;
			
		}
		
		/* clear buffers */
		if (clear_buffers==2) clear_buffers--;

	/* Are we sending global mnemonic buffer?  Or cleaning up at the end of a file? */
	} else if (send_global || clear_buffers==2) {
			
		if (clear_buffers==2) {

			/* Copy the buffer to send */
			memcpy(globals_to_send,global_buffer,MAX_MESSAGE_LENGTH);
			send_global = -1;
			/* Save information and zero counter so that next IF will be executed.*/
			global_message_length_to_send = global_message_length;
			global_ctr_to_send = global_ctr;
			global_ctr = 0;			
				
		}

		/* As long as there are globals to send */
		if (global_ctr_to_send != 0) {
		
			if (debug_gva) {
				log_event("(process_archive_rec) SEND_BUF_GVA n=%d  msglen=%d  pseudo_time: %d %d  gva_time: %d %d",
				pseudo_ctr_to_send,pseudo_message_length_to_send,
				first_pseudo.tv_sec,pseudo_time.tv_sec,first_global.tv_sec,gva_time.tv_sec);
			}			
			GAIM_seconds_to_date(newdate,(double)current_time.tv_sec);
				
			/* Insert pseudo count into packet */	
			memcpy(globals_to_send+2,&global_ctr_to_send,2);
			
			if (gaim_playback != PLAYBACK_PSEUDO_ONLY) {
				if (first_global.tv_sec >= first_pseudo.tv_sec) {	
					net_write(read_write_ubg, globals_to_send, global_message_length_to_send);
					last_crc = GAIM_calculate_checksum(global_message_length_to_send, globals_to_send);
					waiting_for_response = -1;
				} else {
					/* do not send packet */
					if (debug_gva) 
						log_event("(process_archive_rec) SEND_BUF_GVA (NOT_SENT) n=%d  msglen=%d  first_pseudo=%d  first_gva=%d",
						pseudo_ctr_to_send,pseudo_message_length_to_send,
						first_pseudo.tv_sec,first_global.tv_sec);
				}
			}
			send_global = 0;
			
		}

		/* clear buffers */
		if (clear_buffers==2) clear_buffers--;
							
	} /* endif send */
	
	
} /* endif waiting_for_response */

if (end_of_file && clear_buffers==0) {
	archive_close(archive_pointer);
	clear_buffers = 2;
}

} /* end PUB_process_archive_record */

/***
 *** process a global variable
 ***
 */
void process_gva (void) 
{
gva_buf = archive_buffer + sizeof(rec);			
gva_send = 0;

/* get global variable index */
memcpy((char *)&gva_index,gva_buf,sizeof(int));
outp_uint(&gva_index);
gva_buf+=sizeof(int);

/* get global variable status */
memcpy((char *)&gva_status,gva_buf,sizeof(u_int));
outp_uint(&gva_status);
gva_buf+=sizeof(u_int);

/* assume this is version 2 *.arc file */
gva_buf+=sizeof(struct timeval);

/* calc size of variable */
gva_size = rec.size - sizeof(int) - sizeof(u_int);
gva_dvar = 0.0;
gva_lvar = 0;
/* get global variable name using db index 
   global_var->set holds the tag value 
   (done by load_tagfile previously).
   send only types GLOBAL_LONG GLOBAL_DOUBLE */

if (stream_def && stream_def->dbhead) {
	gid = (gvar *)(stream_def->dbhead->gv_head->gv_array[gva_index]);
	if (gid) {

		if (gid->global_var->global_type==GLOBAL_LONG) {
			memcpy((char *)&gva_lvar,gva_buf,gva_size);
			outp_uint(&gva_lvar);
			gva_dvar = gva_lvar;
			gva_send = 1;
		} else if (gid->global_var->global_type==GLOBAL_DOUBLE) {
			memcpy((char *)&gva_dvar,gva_buf,gva_size);
			outp_double(&gva_dvar);
			gva_send = 1;
		}

		if (gva_send) {
			strncpy(gva_name,gid->global_var->global_name,MNEMONIC_SIZE);
			gva_tag  = gid->global_var->set;
			outp_int(&gva_tag);
			/* if (gva_tag > 10400)
				log_event("GVA_SEND: size=%d  tag=%d  type=%d  stat=X%8.8X  val=%g  name=(%s)",
				gva_size, gva_tag, gid->global_var->global_type, gva_status, gva_dvar, gva_name);*/
		}
	}
}  

return;

} /* end process_gva */


