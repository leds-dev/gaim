/*  
@(#)  File Name: PUB_service_NQIM_init.c  Release 1.4  Date: 04/03/24, 20:09:09
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       PUB_service_NQIM_init.c
         EXE. NAME       :       Playback_Update_Buffer
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
	 8.5	     mar23-2004  K. Dolinh	 add log_event, log_debug
	
********************************************************************************
	Invocation:
        
		PUB_service_NQIM_init()
        
	Parameters:

		NONE

********************************************************************************
	Functional Description:

		This function connects and services the initialization message coming 
		from the socket to the NQIM.

*******************************************************************************/

/**

PDL:
	Request a socket connection from the N-Q_Interface_Manager.
	DOWHILE there is no request for program termination and we have not 
	 received any data
		IF there is a socket that has requested service and the read/write 
		 socket with N-Q_Interface_Manager exists THEN
			IF there is data waiting to be serviced on the N-Q_Interface_Manager
			 read/write socket THEN
				Read the data on the socket.
				IF the message is a shutdown message THEN
					Request a program termination for Playback_Update_Buffer.
				ELSEIF the message length is more than zero THEN
					Process the message as a list of playback/merge parameters.
					Record each merge request contained in the message to the 
					 debug file.
				ELSE
					Write an 'Unknown Message Size' message to the debug log.
					Request a program termination for Playback_Update_Buffer.
				ENDIF
			ENDIF			
		ENDIF
		IF no data has been received yet THEN
			Sleep for 1 second.
		ENDIF
	ENDDO

**/

#include <unistd.h>
#include "net_util.h"
#include "util_log.h"

#include "PUB_defines.h"

void GAIM_seconds_to_date(char *outdate,double secs);

void PUB_service_NQIM_init (void) {

char message[256];
	/* Error Message String*/

int status;
	/* Status flag */

int received_data=0;
	/* -1=We have received playback parameters, 0=we have not */

char display_start[20],display_stop[20];
	/* Arrays to show start and stop times (in GMT) of requested
		playbacks */

int i;
	/* Loop Counter */

char *buffer_ptr;
	/* Pointer into BUFFER */

/* Connect to IFM */
status = net_call(NULL, nqim_service_name, &read_write_nqim);
if (status!=0) {

	/* connection failed */
	sprintf(message,"error connecting to IFM  service=%s status=%i.",nqim_service_name,status);
	log_event_err(message);
	strcat(error_message,message);
	end_program=-1;

} else {
	/* set up read-write socket */
	FD_SET(read_write_nqim,&read_mask);
		
	/* We are now connected and can receive data. */
	nqim_no_connection = 0;			
	log_event("connected to IFM");
}

/* If we do not need to stop this program and have not received 
   playback parameters from NQIM */

while (!end_program && !received_data) {
	
	/* Save the current read_mask value */
	data_mask=read_mask;
		
	/* Any sockets requesting service? */
	status = select(FD_SETSIZE, &data_mask, NULL, NULL, &select_time);
		
	/* If socket has requested service... */
	if (status > 0) {
				
		/* Is socket available for read-write requests? */
		if (read_write_nqim > 0) {
				
			/* Is socket "i" the one with the data? */
			if (FD_ISSET(read_write_nqim,&data_mask)) {
					
				/* Read socket data */
				message_length = net_read(read_write_nqim,buffer,max_length);				
				if (buffer[0]=='z' && message_length==8) {
						
					/* Handle shutdown message from NQIM */
					end_program = -1;
					log_event("received shutdown message from IFM");
							
				} else if (message_length > 0) {
				
					/* not shutdown message, it must be list of  playback params */
					received_data=-1;
					number_merge_requests=(long)(*(short *)buffer);
					
					if (number_merge_requests < 1) {
						sprintf(message,"invalid number of merge requests = %i",number_merge_requests);
						log_event_err(message);
						strcat(error_message,message);
						end_program=-1;
					}
					
					if (number_merge_requests*8+2 != message_length) {
						sprintf(message,"invalid received packet len = %i  (expected len = %i)",
							message_length,number_merge_requests*8+2);
						log_event_err(message);
						strcat(error_message,message);
						end_program=-1;
					}

					if (!end_program) {
						day_offset_start_ms = (int *)calloc(number_merge_requests,sizeof(int));
						duration_ms = (int *)calloc(number_merge_requests,sizeof(int));

						log_debug("%i Merge Requests %s in packet",number_merge_requests);
							
						buffer_ptr=buffer+2;
						for (i=0; i<number_merge_requests; i++) {
							/* For each merge request */
							memcpy(&day_offset_start_ms[i],buffer_ptr,4);
							buffer_ptr += 4;
							memcpy(&duration_ms[i],buffer_ptr,4);
							buffer_ptr += 4;
														
							/* Extract parameters from the packet */
							GAIM_seconds_to_date(display_start,
								(double)start_secs+
								(double)day_offset_start_ms[i]/1000.0);
							GAIM_seconds_to_date(display_stop,
								(double)start_secs+
								(double)day_offset_start_ms[i]/1000.0+
								(double)duration_ms[i]/1000.0);
									
							log_debug("request number %i: Millisec Start = %f  (%s)",
								i+1,(double) day_offset_start_ms[i],display_start);
							log_debug("                   Millisec Stop  = %f  (%s)",
								    (double)duration_ms[i],display_stop);									

						}
					}

				} else {

					sprintf(message,"invalid msg length %i received from IFM",message_length);
					log_event_err(message);
					/* If message length is 0 or less, kill PUB. */
					if (message_length <= 0) {
						end_program = -1;
						strcat(error_message,message);
					}
						
				}
			}
		}
	}
	if (!received_data) sleep(1);
}

} /* end PUB_service_nqim_init */
