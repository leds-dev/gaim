/*  
 @(#)  File Name: PUB_get_archive_files.c  Release 1.5  Date: 05/04/04, 18:18:41
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       PUB_get_archive_files.c
         EXE. NAME       :       Playback_Update_Buffer
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles       Initial creation
         8.5         mar23-2004  K. Dolinh       add log_event, log_debug
	             mar25-2005  K. Dolinh       removed debug events
         V2.0        12/21/10    M. Dalal        Modified the way we retrieve archfiles

********************************************************************************
	Invocation:
        
		num_files=PUB_get_archive_files(char *yjd_start,char *yjd_stop)
        
	Parameters:

		yjd_start - I
			The requested start time in YYY-JJJ-HH:MM:SS form.

		yjd_stop - I
			The requested stop time in YYY-JJJ-HH:MM:SS form.  

		num_files - O
			The number of files found.

********************************************************************************
	Functional Description:

		This function gets the list of archive files which need to be played
		back.
	
*******************************************************************************/

/**

PDL:
	CALL PUB_yjd_to_seconds for both the requested start and stop times.
	Determine the directory in which to find the archive files.
	Open the archive file directory.

	DOFOR each valid archive file in the directory
		Compare the start and stop times in the archive file with the requested 
		 archive start and stop times.
		IF the current archive file contains data within the requested time 
		 range THEN
			Mark this archive as one to play back.
		ENDIF
	ENDDO

	Sort the marked archive files by time.
	RETURN the number of archive files found or -1 if there was an error.

**/

#include <sys/types.h>
#include <dirent.h>
#include <strings.h>
#include <unistd.h>
#include "outp_util.h"
#include "util_log.h"

/* Standard C Include files */

#include "PUB_defines.h"
/* Playback_Update_Buffer Defines */

/* The file_select function is used to only return selected files from the directory */
int file_select(const struct dirent *entry);
/* The is_archive_valid() function is used to determine if the selected .arc file has the frames we need */
int is_archive_valid(char *filename, int start_secs, int stop_secs);
int PUB_yjd_to_seconds(char *yjd);

int PUB_get_archive_files(char *yjd_start,char *yjd_stop) {
  /* Returns either the number of files or error -1 */
  
  int i,j;
  /* Loop Counters */
  
  int status;
  /* Return status of various functions */
  
  int start_secs, stop_secs;
  /* Start and stop time in seconds */
  
  /*DIR *archive_dir_ptr;*/
  struct dirent **filelist = {0};
  //struct dirent *current_entry;
  int file_counter;
  int fcount = -1;
  /* Directory pointers and other information*/
  
  char temp_filename[256];
  //char *extension;
  /* Used as temporary storage to determine whether the current file is a .arc file or not. */
  
  struct archive_cb *archive_pointer;
  /* Archive control block */
  
  char message[256];
  /* Debug Message */
  
  int temp_int;
  /* Temp Int used for swapping */
  
  /* Allocate the archive control block pointer */
  archive_pointer=(struct archive_cb *)calloc(1,sizeof(struct archive_cb));
  
  /* Convert Year-Julian Date to Seconds */
  start_secs = PUB_yjd_to_seconds(yjd_start);
  stop_secs  = PUB_yjd_to_seconds(yjd_stop);
  
  /***DEBUG*/ 
  log_event("get_archive_files: start_time(%s) = %d   stop_time(%s) = %d",
	    yjd_start,start_secs,yjd_stop,stop_secs);
  
  /* If the base archive_dir was not found in the gaim.cfg file */
  if (strlen(base_archive_dir) < 2) {
    
    /* If ARCHIVE_DIR defined, get it. */
    if (getenv("ARCHIVE_DIR")) strcpy(base_archive_dir,getenv("ARCHIVE_DIR"));	
    log_event_err("archive_dir for %s not in gaim.cfg. using $ARCHIVE_DIR",gtacs_name);
    
  }
  
  if (strlen(base_archive_dir) > 1) {
    
    strcpy(archive_dir,base_archive_dir);
    strcat(archive_dir,"/");
    /* Sim 85 is GOES 13 */
    if (atoi(scid_number) > 80) 
      strcat(archive_dir,scnames[atoi(scid_number)-72]);		
    else 
      strcat(archive_dir,scnames[atoi(scid_number)]);
    strcat(archive_dir,"/");
    strcat(archive_dir,stream_name);
    
    /* Open the archive file directory */
    file_counter=0;
    //archive_dir_ptr=opendir(archive_dir);
    fcount = scandir(archive_dir, &filelist, file_select, alphasort);
    
    if(fcount < 0) {
      log_event_err("Cannot open archive directory %s\n", archive_dir);
    }
    else {
      for(i=0; i<fcount; i++) {
	if(is_archive_valid(filelist[i]->d_name, start_secs, stop_secs)) {

	  /* Now verify the header info to absolutely make sure it has our data */
	  /* Copy the fullly qualified filename first */
	  sprintf(temp_filename, "%s/%s", archive_dir, filelist[i]->d_name);
	  log_event("Possibly valid file: %s\n", temp_filename);
	  status = archive_open(archive_pointer, temp_filename);
	  if(status) {
	    log_event_err("Cannot open archive file %s", temp_filename);
	  } else {
	    // Swap bytes, if needed
	    outp_timeval32(&archive_pointer->arch_header.start_time);
	    outp_timeval32(&archive_pointer->arch_header.stop_time);
	    
	    /* Ensure the time range falls within our range */
	    log_event("Start_secs = %d, Stop_Secs = %d\n", start_secs, stop_secs);
	    log_event("Arch_header.start_time.tv_sec = %d\n",archive_pointer->arch_header.start_time.tv_sec);
	    log_event("Arch_header.stop_time.tv_sec = %d\n",archive_pointer->arch_header.stop_time.tv_sec); 
	    if(archive_pointer->arch_header.start_time.tv_sec <= stop_secs &&
	       archive_pointer->arch_header.stop_time.tv_sec >= start_secs) {
	      /* archive has our data, add to the list */
	      /* Copy the name of the file */
	      strcpy(archive_file_list[file_counter],temp_filename);
	  
	      /* Set the start time of the file */
	      archive_file_start[file_counter] = archive_pointer->arch_header.start_time.tv_sec;
	      
	      /* increment counter */
	      file_counter++;
	    }
	  }
	  archive_close(archive_pointer);
	}
	// Free the filelist array pointer
	free(filelist[i]);
      }
      //Free the filelist pointer
      free(filelist);
    }
  } else {
    log_event_err("env var $ARCHIVE_DIR is not defined");
    file_counter=-1;
  }
  
  
  //if (archive_dir_ptr) {
  
  /* do for each entry in the directory. */
  //do {
      
      /* Get the next entry */
      //current_entry = readdir(archive_dir_ptr);
      
      /* If not NULL, continue */
      //if (current_entry) {
	
	/* Copy the file name to a temporary char array */
	//sprintf(temp_filename,"%s/%s",archive_dir,current_entry->d_name);
	
	/* IF name > 4 characters, it may be a .arc file */
	//if (strlen(temp_filename) > 4) {
	  
	  /* find position of file extension */	
	  //extension = temp_filename+strlen(temp_filename)-4;
	  
	  /* The file has a .arc extension */
	  //if (extension[0]=='.' && extension[1]=='a' && 
	  //    extension[2]=='r' && extension[3]=='c') {
	    
	    /* Open the current archive file */	
	    //status=archive_open(archive_pointer,temp_filename);
	    
	    //if (status) {
	    //  log_event_err("cannot open archive file %s",temp_filename);
	      
	    //} else {
	      /* Swap bytes, if needed */
            //  outp_timeval(&archive_pointer->arch_header.start_time);
            //      outp_timeval(&archive_pointer->arch_header.stop_time);
	      
	      /* Do we want this file */
	      //if (archive_pointer->arch_header.start_time.tv_sec <= stop_secs && 
  //  archive_pointer->arch_header.stop_time.tv_sec  >= start_secs) {
		
		/* Copy the name of the file */
		//strcpy(archive_file_list[file_counter],temp_filename);
		
		/* Set the start time of the file */
		//archive_file_start[file_counter] = archive_pointer->arch_header.start_time.tv_sec;
		
		/* increment counter */
		//file_counter++;
		
		/***DEBUG 
		    log_event("get_archive_files: archive_file=(%s) start=%d  stop=%d",current_entry->d_name,
		    archive_pointer->arch_header.start_time.tv_sec,
		    archive_pointer->arch_header.stop_time.tv_sec); */
		
  //}
	      /* Close the archive file */
	      //archive_close(archive_pointer);
	      
	      
  //}
  //  }
  //}
  //  }
  //} while (current_entry);
    
    
    /* Close the archive directory */
    //closedir(archive_dir_ptr);
    /*
      } else {
      sprintf(message,"cannot open archive dir %s",archive_dir);
      log_event_err(message);
      strcat(error_message,message);
      file_counter=-1;
      }
    */
  //{} 
  
  /* Free the archive control block pointer */
  free(archive_pointer);
  
  /* Sort playback order indices */
  for (i=0; i<file_counter; i++) playback_order[i]=i;
  for (i=0; i<file_counter-1; i++) {
    for (j=i+1; j<file_counter; j++) {
      if (archive_file_start[j]<archive_file_start[i]) {
	temp_int=playback_order[i];
	playback_order[i]=playback_order[j];
	playback_order[j]=temp_int;
      }
    }
  }
  
  /* Return either the number of files found or error -1 */
  return file_counter;
}

/* The following function is passed as a parameter to scandir. It is thread-safe.
   It return 1 if the file has .arc extension and 0 otherwise */
int file_select(const struct dirent *entry)
{
  char *ptr;
  
  if ((strcmp(entry->d_name, ".") == 0) || strcmp(entry->d_name, "..") == 0) {
    return (0);
  }
  
  ptr = rindex(entry->d_name, '.');
  if((ptr != NULL) && ((strcmp(ptr, ".arc") == 0)))
    return (1);
  else
    return (0);
}

/* The following function is used to determine the filename being passed in qualifies for playback.
   It parses the filename to retrieve the approximate timeframe of the frames within the file. In order to get
   a full list of files, we extend the start/stop window by 3610 seconds before and 10 seconds after the
   times passed in. This is done to cover the filename granulality of 1 hour.
   It return 1 to indicate file qualifies, and 0 otherwise. In addition, it also return the timetag
   retrieved from archfile based on its filename. */

int is_archive_valid(char *filename, int start_secs, int stop_secs)
{
  int status = 0; // 0=invalid, 1=valid
  int arc_time_in_int=0;
  char temp_time[20];
  char year[5], doy[4], mm[4], hh[4]; 

  strncpy(year, filename+1, 4);
  year[4]='\0';
  strncpy(doy, filename+5, 3);
  doy[3]= '\0';
  strncpy(hh, filename+8, 2);
  hh[2]= '\0';
  strncpy(mm, filename+10, 2);
  mm[2]= '\0';
  sprintf(temp_time, "%s-%s-%s:%s:00", year, doy, hh, mm);
  
  arc_time_in_int = PUB_yjd_to_seconds(temp_time);

  // Check to see if file date falls within the range.
  if(arc_time_in_int <= (stop_secs+10) && arc_time_in_int >= (start_secs-3610)) {
    status = 1;
  }

  return status;
}
