/*  
@(#)  File Name: PUB_service_UBG_socket.c  Release 1.4  Date: 07/04/07, 15:56:27
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       PUB_service_UBG_socket.c
         EXE. NAME       :       Playback_Update_Buffer
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
		 8.5	     mar23-2004  K. Dolinh	 add log_event, log_debug
	
********************************************************************************
	Invocation:
        
		PUB_service_UBG_socket()
        
	Parameters:

		NONE

********************************************************************************
	Functional Description:

		This function services and message coming from the socketr to the UBG.

*******************************************************************************/

/**

PDL:
	IF there is a socket requesting service THEN
		IF the Update_Buffer_Generator listening socket exists and a service 
		 request is waiting THEN
			Finalize the TCP/IP connection.
		ENDIF
		IF the Update_Buffer_Generator read/write socket exists and a service 
		 request exists for this socket THEN
			Read the data from the socket.
			IF the message is a shutdown message THEN
				Shutdown and close the socket.
				Write a shutdown message to the debug log.
			ELSEIF the message is a failure message THEN
				Shutdown and close the socket.
				Write a failure message to the debug log.
			ELSEIF the message is a CRC THEN
				Reset the waiting-for-a-response flag.
				IF the CRC does not match what was sent THEN
					Write a CRC failure message to the debug log.
				ENDIF
			ELSE
				Write an 'Unknown Message' debug message to the debug log.
				Shutdown the Playback_Update_Buffer.
			ENDIF
		ENDIF
	ENDIF

**/

#include <unistd.h>
#include "net_util.h"
#include "util_log.h"
#include "PUB_defines.h"


void PUB_service_UBG_socket (void) {

char message[256];
	/* Error Message String*/

int status;
	/* Status flag */

/* Save the current read_mask value */
data_mask = read_mask;
	
/* Any sockets requesting service? */
status = select(FD_SETSIZE, &data_mask, NULL, NULL, &select_time);

/* If socket has requested service... */		
if (status > 0) {
		
	/* If the socket exists */
	if (listen_for_ubg > 0) {
			
		/* And this is the socket the request is on */
		if (FD_ISSET(listen_for_ubg,&data_mask)) {
				
			/* Answer the request with a final connect message */
			status = net_answer(ubg_service_name, 99,	&listen_for_ubg, &read_write_ubg);
			if (status != 0) {
			
				/* connection failed */
				log_event_err("error connecting to UBG  service=%s status=%i",ubg_service_name,status);
					
			} else {
				
				/* set up read-write socket */
				FD_SET(read_write_ubg,&read_mask);
					
				/* We are now connected and can send frames. */
				ubg_no_connection = 0;
				log_event("connected to UBG");
			}
		}
	}

	/* Is socket available for read-write requests? */
	if (read_write_ubg > 0) {
			
		/* Is socket "i" the one with the data? */
		if (FD_ISSET(read_write_ubg,&data_mask)) {
			
			/* Read socket data */
			message_length = net_read(read_write_ubg,buffer,max_length);
				
			if (buffer[0]=='z' && message_length==8) {
			
				/* Handle shutdown message from UBG */
				/* If message is "z!z!z!z!", then UBG died with an 
					error that was not handled properly.  z!z!z!z! 
					should ALWAYS be followed by an error message */

				shutdown(read_write_ubg,SHUT_RDWR);
				close(read_write_ubg); 
				FD_CLR(read_write_ubg,&read_mask);
				read_write_ubg = -1;
				end_program = -1;
				
				if (buffer[1] == '!') strcat(error_message,"Unknown failure of UBG");
				ubg_shutdown = -1;
				waiting_for_response = 0;

				if (buffer[1] == '!') 
					log_event_err("Received error-shutdown message from UBG");
				else 
					log_event("Received normal-shutdown message from UBG");


			} else if (strstr(buffer,ERROR_MESSAGE_HEADER)) {
					
				/* Handle error message from UBG */
				shutdown(read_write_ubg,SHUT_RDWR);
				close(read_write_ubg); 
				FD_CLR(read_write_ubg,&read_mask);
				read_write_ubg = -1;
				end_program = -1;
				strcpy(error_message,buffer);
				ubg_shutdown = -1; 
				waiting_for_response = 0;

				log_event_err("received error message from UBG: '%s'",&buffer[8]);
					
			} else if (message_length == 4) {
				/* If not a shutdown message, it must be a CRC. */
					
				/* No longer waiting */
				waiting_for_response = 0;
					
				if (last_crc != *(int *)buffer) {
						/* CRC failed */
						log_event_err("Packet CRC failure  Expected=%8.8X  Computed=%8.8X",
							last_crc,*(int *)buffer);
				} else {
						/* CRC validated */
						log_debug(" Packet CRC validated = %8.8X",last_crc);
				}

			} else {

				sprintf(message,"did not process msg with length=%i from UBG",message_length);
				log_event_err(message);

				/* No longer waiting */
				waiting_for_response = 0;
					
				/* If message length is 0 or less, kill PUB. */
				if (message_length <= 0) {
					strcat(error_message,message);
					end_program = -1;
					waiting_for_response = 0;
					read_write_ubg = -1;
					ubg_shutdown = -1; 
				}
					
			}
		
		} /* endif FD_ISSET */
				
	} /* endif read_write_ubg */
	
} /* endif select status */

} /* PUB_service_UBG_socket */
