/*  
@(#)  File Name: PUB_service_NQIM_socket.c  Release 1.3  Date: 04/03/24, 20:09:10
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       PUB_service_NQIM_socket.c
         EXE. NAME       :       Playback_Update_Buffer
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
	 	 8.5	     mar23-2004  K. Dolinh	 	 add log_event, log_debug
	
********************************************************************************
	Invocation:
        
		PUB_service_NQIM_socket()
        
	Parameters:

		NONE

********************************************************************************
	Functional Description:

		This function services and message coming from the socket to the NQIM.

*******************************************************************************/

/** 

PDL:
	IF a socket has requested service THEN
		IF the N-Q_Interface_Manager socket is available for reading and 
		 writing THEN
			IF the N-Q_Interface_Manager has requested read/write service THEN
				Read the data from the socket.
				IF the message is a shutdown message THEN
					Request a program termination for Playback_Update_Buffer.
				ELSE
					Write an 'Unknown Message' message to the debug log.
					IF the message length is less than or equal to zero THEN
						Request a program termination for 
						 Playback_Update_Buffer.
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

**/

#include "net_util.h"
#include "util_log.h"

#include "PUB_defines.h"


void PUB_service_NQIM_socket (void) {

char message[256];
	/* Error Message String*/

int status;
	/* Status flag */

/* Save the current read_mask value */
data_mask = read_mask;
	
/* Any sockets requesting service? */
status = select(FD_SETSIZE, &data_mask, NULL, NULL, &select_time);
	
/* If socket has requested service... */	
if (status > 0) {
		
	/* Is socket available for read/write requests? */
	if (read_write_nqim > 0) {
	
		/* Is socket "i" the one with the data? */
		if (FD_ISSET(read_write_nqim,&data_mask)) {
			

			/* Read socket data */
			message_length = net_read(read_write_nqim,buffer,max_length);
			if (buffer[0]=='z' && message_length==8) {
			
				/* Handle shutdown message from NQIM */
				end_program = -1;
				waiting_for_response = 0;
				log_event("received shutdown message from IFM");

			} else {

				sprintf(message,"invalid msg length %i received from IFM",message_length);
				log_event_err(message);
				/* If message length <= 0, exit program */
				if (message_length <= 0) {
					end_program = -1;
					waiting_for_response = 0;
					strcat(error_message,message);
				}
					
			}
		} /* endif FD_ISSET */
		
	} /* endif read_write_nqim */
	
} /* endif select status */

} /* end PUB_service_NQIM_socket */

