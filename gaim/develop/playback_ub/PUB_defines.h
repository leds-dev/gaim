/*  
@(#)  File Name: PUB_defines.h  Release 1.6  Date: 07/04/13, 11:09:40
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         HEADER          :       PUB_defines.h
         EXE. NAME       :       Playback_Update_Buffer
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
         V2.0        05/02       A. Raj          Set select timeout value to 0
                                                 to speed up the merge process
         10A.2      sep09-2005   K Dolinh        fixed mismatch define of error_message
		 11			apr12-2007					 add gaim_playback flag

********************************************************************************
         Functional Description:

         This header file contains defines used by Playback_Update_Buffer.
	
*******************************************************************************/

/* Epoch database structures and routines 
#include "db.h"
#include "db_util.p"
#include "net_util.h"
#include "list.h"
#include "strmutil.p"
*/

/* EPOCH Include files */
#include "tlm.h"
#include "arch_util.h"

#include "GAIM_update_buffer_defines.h"
	/* Used for multiple programs */

#define MAX_ARCHIVE_FILES 4096
	/* 1MB of file names allowed */

#define HEADERSIZE sizeof(arch_rec_header)
	/* Size of archive record header */

#define MAX_BUFFER_SIZE 10000
	/* How long should the longest GLOABL or PSEUDO buffer be? */

#define MAX_MESSAGE_LENGTH 16384
	/* Maximum message length for packets to be sent to UBG */

#define UNUSED_CASE 12345

static void signal_handler(int sig);
	/* More Prototypes */

#define PLAYBACK_NORMAL  		0
#define PLAYBACK_PSEUDO_ONLY  	1
#define PLAYBACK_GVA_ONLY  		2

#ifdef PUB_MAIN
	int end_program=0;
		/* -1=End, 0=Keep Going */

	char stream_name[32];
		/* Global Command Line Inputs */

	char debug_filename[256];
	int debug_on=0;
		/* Debug file name, if supplied */

	int run_mode=0;
		/* 0=Run from Command Line [def], 1=Spawned by N-Q_Interface Manager */

	char process_number[4];
	char ubg_service_name[20]="gaim_pub_ubg_";
	int read_write_ubg=-1;
	int listen_for_ubg=-1;
	char nqim_service_name[20]="gaim_ifm_pub_";
	int read_write_nqim=-1;
	fd_set read_mask,data_mask;
	/*struct timeval select_time={0,10000};*/
	struct timeval select_time={0,0}; /* ARaj - make merge go faster */
	int crc=0;	
		/* Generic TCP/IP Stuff */

	char archive_file_list[MAX_ARCHIVE_FILES][256];
	int archive_file_start[MAX_ARCHIVE_FILES];
	int playback_order[MAX_ARCHIVE_FILES];
		/* List of archive files */

	int ubg_shutdown=0;
	int nqim_shutdown=0;
		/* Has Update_Buffer_Generator or N-Q_Interface_Manager sent us a 
			shutdown message? */

	int waiting_for_response=0;
		/* Waiting for a response from UBG */

	int message_length;
	char buffer[16384];
	int max_length=-16383;
		/* More TCP/IP stuff */

	int ubg_no_connection=-1;
	int nqim_no_connection=-1;
		/* -1=No connection established to UBG/NQIM, 0=connected */

	int last_crc;
		/* Checksum of last mesage sent to UBG */

	int new_archive;
		/* -1=New Archive file, 0=Not new.  This is used to determine whether 
			to send the database packet to UBG */

	char database_name[1024];
	short dbase_name_len;
		/* Database name from archive file */

	struct archive_cb *archive_pointer;
		/* Archive control block */

	int tlm_message_length;
	char tlm_buffer[MAX_MESSAGE_LENGTH];
	int global_message_length,global_message_length_to_send;
	char global_buffer[MAX_MESSAGE_LENGTH],globals_to_send[MAX_MESSAGE_LENGTH];
	int pseudo_message_length,pseudo_message_length_to_send;
	char pseudo_buffer[MAX_MESSAGE_LENGTH],pseudos_to_send[MAX_MESSAGE_LENGTH];
		/* Buffers and counters for packets to be sent to UBG */

	char *tlm_buffer_ptr;
	char *global_buffer_ptr;
	char *pseudo_buffer_ptr;
		/* Current write location in BUFFER */

	int end_of_file;
		/* End of archive file? */

	char *archive_buffer;
		/* Pointer used with archive block buffer */

	arch_rec_header rec;
		/* Archive record header */

	int start_secs,stop_secs;
		/* Requested start and stop times in seconds */

	int end_of_block;
		/* Are we at the end of an archive block, yet? */

	int block_counter;
		/* Archive block counter */

	struct timeval32 current_time;
		/* Global or Frame time, whichever is more recent. */

	struct tlm_frame *tlm_frame_overlay;
	struct timeval32 the_frame_time;
		/* Used for determining whether the requested frame is within the 
			request time */

	int send_tlm=0;
	int send_global=0;
	int send_pseudo=0;
		/* -1=send this packet, 0=do not send yet */

	char *archive_buffer_ptr;
		/* Where are we in the archive buffer? */

	unsigned short global_ctr,global_ctr_to_send;
	unsigned short pseudo_ctr,pseudo_ctr_to_send;
		/* How many points are in the buffer? */

	int clear_buffers;
		/* 2=Still need to clear globals, 
			1=done cleanup -- time to go to next file, 
			0=no cleaning needed right now. */

	struct timeval32 first_global,first_pseudo;
		/* We need to keep the time span of globals and pseudos less than 
			65.535 seconds. */

	struct timeval32 global_time,pseudo_time;
		/* Global and pseudo Time */

	char gtacs_name[16];
		/* The name of the GTACS from where the data comes */

	char scid_number[4];
		/* The number from the SPACECRAFT_ID global, passed to PUB from NQIF */

	char merge_type[4];
		/* The merge type requested. A=Auto-merge, M=Manual-merge */

	int on_last_file;
		/* Are we on the last file? */

	char stream_type[4];
		/* The merge type requested. S=Simultaed stream, R=Real S/C Stream */

	int number_merge_requests=1;
		/* How many merge requests have been requested? */

	int *day_offset_start_ms;
	int *duration_ms;
		/* Start offset and duration of merge requests */

	char archive_ip_address[20]="172.0.0.1";
		/* IP Address of the archive machine making the archive request */

	char archive_dir[256]=" ";
		/* Archive directory */

	char base_archive_dir[256]=" ";
		/* Archive directory */

	char error_message[256]=ERROR_MESSAGE_HEADER;
		/* Error message buffer */

	char connect_to_stream[256];
		/* -c parameter value */

	/* playback pseudo or gva flag */
	int	gaim_playback = 0;
	
#else

extern char debug_filename[256];
extern int debug_on;
extern char stream_name[32];
extern char archive_file_list[MAX_ARCHIVE_FILES][256];
extern int archive_file_start[MAX_ARCHIVE_FILES];
extern int playback_order[MAX_ARCHIVE_FILES];
extern struct timeval32 global_time;
extern fd_set read_mask,data_mask;
extern int read_write_ubg;
extern int listen_for_ubg;
extern char nqim_service_name[20];
extern int read_write_nqim;
extern int nqim_shutdown;
extern int nqim_no_connection;
extern char process_number[4];
extern char ubg_service_name[20];
extern struct timeval select_time;
extern int ubg_shutdown;
extern int waiting_for_response;
extern int message_length;
extern char buffer[16384];
extern int max_length;
extern int ubg_no_connection;
extern int last_crc;
extern int end_program;
extern int new_archive;
extern char database_name[1024];
extern short dbase_name_len;
extern struct archive_cb *archive_pointer;
extern int tlm_message_length;
extern char tlm_buffer[MAX_MESSAGE_LENGTH];
extern int global_message_length,global_message_length_to_send;
extern char global_buffer[MAX_MESSAGE_LENGTH];
extern char globals_to_send[MAX_MESSAGE_LENGTH];
extern char *tlm_buffer_ptr;
extern char *global_buffer_ptr;
extern int pseudo_message_length,pseudo_message_length_to_send;
extern char pseudo_buffer[MAX_MESSAGE_LENGTH];
extern char pseudos_to_send[MAX_MESSAGE_LENGTH];
extern char *pseudo_buffer_ptr;
extern int end_of_file;
extern char *archive_buffer;
extern arch_rec_header rec;
extern int start_secs,stop_secs;
extern int end_of_block;
extern int block_counter;
extern struct timeval32 current_time;
extern struct tlm_frame *tlm_frame_overlay;
extern struct timeval32 the_frame_time;
extern int send_tlm;
extern int send_global;
extern char *archive_buffer_ptr;
extern unsigned short global_ctr,global_ctr_to_send;
extern int send_pseudo;
extern unsigned short pseudo_ctr,pseudo_ctr_to_send;
extern int clear_buffers;
extern struct timeval32 first_global,first_pseudo;
extern struct timeval32 global_time,pseudo_time;
extern char merge_type[4];
extern int on_last_file;
extern char stream_type[4];
extern int number_merge_requests;
extern int *day_offset_start_ms;
extern int *duration_ms;
extern char scid_number[4];
extern char archive_ip_address[20];
extern char gtacs_name[16];
extern char archive_dir[256];
extern char base_archive_dir[256];
extern char error_message[256];
extern char connect_to_stream[256];
extern int	gaim_playback;

#endif
