/*  
@(#)  File Name: PUB_read_GAIM_config.c  Release 1.3  Date: 04/03/24, 20:09:08
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       PUB_read_GAIM_config.c
         EXE. NAME       :       Playback_Update_Buffer
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        04/00       S. Scoles       Initial creation
	 8.5	     mar23-2004  K. Dolinh	 add log_event, log_debug
	
********************************************************************************
	Invocation:
        
		PUB_read_GAIM_config()
        
	Parameters:

		NONE

********************************************************************************
	Functional Description:

		This function reads the gaim.cfg file and extracts what is needed for 
		Playback.
	
*******************************************************************************/

/**

PDL:
	Open the gaim.cfg file.
	DOFOR each line in the gaim.cfg file
		Read the line from the file.
		IF this is a GTACS line THEN
			Process and record the associated information for future reference.
		ENDIF
	ENDDO

**/

#include <stdio.h>
#include "util_log.h"
	/* Standard includes */

#include "PUB_defines.h"
	/* Playback_Update_Buffer Defines */

void PUB_read_GAIM_config(void) {

FILE *gaim_cfg_ptr;
	/* file pointer */

char buffer[256];
	/* Read buffer for file */

char match_string[256];
	/* What line are we looking for in the file? */

char message[256];
	/* Debug message */


gaim_cfg_ptr=fopen("gaim.cfg","r");

if (gaim_cfg_ptr) {

	sprintf(match_string,"GTACS %s ",gtacs_name);
		/* What key are we looking for?  The space at the ed of the
			text is important */ 

	while (!feof(gaim_cfg_ptr)) {
			/* Dop while not end of file */

		fgets(buffer,255,gaim_cfg_ptr);
			/* Get the next line */

		if (!feof(gaim_cfg_ptr)) {
			/* If not at EOF, continue */

			buffer[strlen(buffer)-1]=0;
				/* Chop off the new line character */

			if (strncmp(buffer,match_string,strlen(match_string))==0) {
					/* Does this line match what we want? */

				strcpy(base_archive_dir,strchr(buffer,'/'));
					/* Get the directory name */

				log_debug("GTACS %s archive directory is %s.",gtacs_name,base_archive_dir);
					
			}
		}
	}

	fclose(gaim_cfg_ptr);

} else {
	log_event_err("cannot find gaim.cfg file");
}

} /* end PUB_read_gaim_config */

