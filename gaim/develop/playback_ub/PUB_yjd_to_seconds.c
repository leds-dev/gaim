/*  
@(#)  File Name: PUB_yjd_to_seconds.c  Release 1.3  Date: 04/03/24, 20:09:12
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       PUB_yjd_to_seconds.c
         EXE. NAME       :       Playback_Update_Buffer
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles       Initial creation
         V1.1        12/10       M. Dalal        Made it more efficient

********************************************************************************
	Invocation:
        
		seconds=PUB_yjd_to_seconds(char *yjd_time)
        
	Parameters:

		yjd - I
			The time to convert from YYY-JJJ-HH:MM:SS form to seconds.  

		seconds - O
			The number of seconds since EPOCH.

********************************************************************************
	Functional Description:

		This function cvonverts a text time to a number of seconds since 
		1970-001-00:00:00.

*******************************************************************************/

/**

PDL:
	Call strptime() with the right format to convert to tm struct
	Call mktime() to convert to time_t which is int.
	RETURN the calculated number of seconds.

**/
#define _XOPEN_SOURCE 500
#include <string.h>
#include <time.h>

int PUB_yjd_to_seconds(char *yjd) {
  /* YJD in YYYY-DDD-HH:MM:SS form, only! */
  
  int total_secs;
  char yjd_copy[18];
  /* Calculated seconds from 1970-001-00:00:00 to be returned to the 
     calling routine */

  struct tm yjd_tm; /* Temp tm */
  const char yjd_format[] = "%Y-%j-%H:%M:%S";

  strncpy(yjd_copy, yjd, 17);
  yjd_copy[17]='\0';
    
  strptime(yjd_copy, yjd_format, &yjd_tm);
  total_secs = (int)mktime(&yjd_tm);
  
  return total_secs;
  /* Return the calculated seconds */
  
}
