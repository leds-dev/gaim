/*
@(#)  File Name: PUB_main.c  Release 1.20  Date: 07/06/22, 15:05:57
*/
/****************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       PUB_main.c
         EXE. NAME       :       Playback_Update_Buffer
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles       Initial creation
	 8.5	     mar23-2004  K. Dolinh	 add log_event, log_debug
	 8.7	     apr15-2004
	 8.B         sep17-2004                  GTACS Build 8B
	 9.1         jan06-2005                  GTACS Build 9.1
	 9.2         feb11-2005                  debug msg in get_archive_files 
	 9A          feb22-2005                  BUILD 9A
	 9AP         mar16-2005                  BUILD 9A Prime 
	 			 mar21-2005                  Add PUB_DELAY_USECS env (but not used)
				 mar25-2005	                 Add/Remove info to various event msgs        
	 9AP         apr04-2005                  BUILD 9A Prime.2
	 9B          may01-2005                  BUILD 9B
	 9C          aug03-2005                  BUILD 9C	 
	 10A         aug26-2005                  BUILD 10A
	 10A.2       sep08-2005                  - send "z!z!z!z!" err message to IFM
	 10B         jan12-2006                  BUILD 10B
	 10C         jul10-2006                  BUILD 10C
	 11	     Mar21-2007			 BUILD 11
	 	     Mar26-2007			 Enable global variables processing
	       	     Apr02-2007			 Add call to load_db, load_tagfile
						 Mod process_archive_record to handle GVA
	 12	     Jun22-2007			 BUILD 12
	 12A	     Sep01-2007			 BUILD 12A
	 					 - make msg (NOT_SENT) depend on debug_gva
         14          Jan04-2011  M. Dalal        Updated SW ID to Build 14.
	 				 	 	 	 	 
********************************************************************************
  	Invocation:

		Playback_Update_Buffer [-a {archive_machine_ip_address}]
			[-b {database name}] [-c {connect_to_stream_name}]
			[-d [{Debug File Name}]] [-g {gtacs_name}]
			[-i {spacecraft_id_number}] [-m {merge_type}] 
			[-pb YYYY-DDD-HH:MM:SS] [-pe yyyy-ddd-hh:mm:ss] [-pt YYYY/ddd] 
			[-r {nqim/test}] [-t {stream name}] [-v {process_number}] 
			[-z {stream_type}]

	Parameters:

		-a {archive_machine_ip_address}
			Which archive machine made the playback request?

		-b {database name} 
			The name of the database to use for decomming the frames.  If this
			is not specified, the database name is taken from the archive file.

		-c {connect_to_stream_name}
			This option was included because the EPOCH load_db function 
			requires that whatever process is loading the database be connected
			to a stream.  It does not matter what stream, just any stream.  The
			reason for this is unknown to this programmer, but is a necessary 
			evil when trying to decommutate archived data.

		-d [{Debug File Name}]
			Name of the debug file to be created to keep track of 
			Playback_Update_Buffer Error Messages.  This is an optional 
			parameter.  Default: No debug file.  If {Debug File Name} is not 
			specified with the -d parameter, all debug messages will be written
			to STDOUT.

		-g {gtacs_name}
			The name of the GTACS from which to retrieve the archive files.

		-i {spacecraft_id_number}
			The value from the SPACECRAFT_ID global.  This should be passed 
			from the NQIF.

		-m {merge_type}
			Set to A for Auto-Merge, M for Manual Merge.  If not present, this 
			is taken from the MERGE_TYPE environment variable.

		-pb YYYY-DDD-HH:MM:SS 
			Playback Begin time in Year-Julian Day format.  

		-pe yyyy-ddd-hh:mm:ss
			Playback End time in Year-Julian Day format

		-pt YYYY/ddd
			Playback start year and day -- for use with NQIM.  Packets will be 
			sent by the NQIM to signify a day offset and a start offset for 
			multiple playbacks. 

		-r {nqim/test} Spawned by (run from) N-Q_Interface_Manager (nqim), 
			or run from command line (test) -- test mode.  Command Line is the 
			default if -r is not specified.

		-t {stream name}
			Name of the stream you wish to connect to, to create the UBs from. 
			If not given, this is taken from the STREAMID environment variable.

		-v {process_number}
			The number (17-32) to be appended to the base service name, 
			identifying which service is to be used to communicate between 
			processes.

		-z {stream_type}
			S For simulated data stream, R for real spacecraft data stream.  
			The STREAM_TYPE global variable can be checked by the calling 
			program to figure out what type of stream we are looking at.  If
			no -z is specified, the value is taken from the STREAM_TYPE
			environment variable.


	Example Usage:


	Exit Conditions:

		0: Successful Completion
		1: Invalid -r argument
		2: Invalid process number
		3: Invalid argument type
		4: Invalid -p argument
		5: Unknown parameter
		6: Incomplete Argument List
		7: Missing start and/or stop time
		8: Cannot allocate space for signal handler.
		9: Cannot set up SIGINT handler
		10: Cannot set up SIGTERM handler
		11: Cannot determine stream name
		12: Cannot determine spacecraft name
		13: Cannot determine SCID number
		14: Cannot determine merge type
		15: Cannot determine stream type

				
******************************************************************************
	Functional Description:

		The Playback Update Buffer Process sends raw frames from exisiting 
		GTACS EPOCH archived data to the Playback_Update_Buffer.  
		UBG processes these raw frames and passes the Update Buffers on to 
		the GOES shared archive.
	
*****************************************************************************/

/**

PDL:
	Process Command Line Parameters.
	Act upon the command line parameters.
	Set up interrupt handlers for SIGINT and SIGTERM.
	IF some of the required parameters were not sent as command line arguments 
	 THEN
		Get the missing parameters from the environment.
	ENDIF
	Initialize socket communications.
	Spawn the Update_Buffer_Generator to be associated with the current 
	 Playback_Update_Buffer process.

	DOFOR each merge request
		IF there is no request for program termaination THEN
			CALL PUB_get_archive_files to get a list of archive files that 
			 needs to be read.
			IF there is no request for program termination and we are on the 
			 last merge request THEN
				Begin shutting down the Update_Buffer_Generator
			ELSE 
				Initialize the variables needed for archive playback.
			ENDIF
		ENDIF

		IF there is no request for program termination THEN
			DOFOR each archive file 
				Open the archive file.
				DOWHILE there is no request for program termination and all 
				 buffers have not been played back from the current archive 
				 file, or we are waiting for a response from the
				 Update_Buffer_Generator.
					IF there are still initialization message to be sent and we
					 still have an active connection with the 
					 Update_Buffer_Generator and we are not waiting for a 
					 response from the Update_Buffer_Generator THEN
						Send the next initialization message.
					ENDIF
					IF Playback_Update_Buffer was not run as a standalone 
					 process THEN 
						CALL PUB_service_UBG_socket
					ENDIF
					IF Playback_Update_Buffer was spawned from the 
					 N-Q_Interface_Manager THEN
						CALL PUB_service_NQIM_socket
					ENDIF
					IF Playback_Update_Buffer in not in the process of shutting
					 down THEN
						CALL PUB_process_archive_record
						IF the current archive is the last file and the end of 
						 file has been seen and we are not waiting for a 
						 response and all buffers have been cleared THEN
							Send the 'end-of-data' message to the 
							 Update_Buffer_Generator.
						ENDIF
					ENDIF
				ENDDO
			ENDDO
		ENDIF
	ENDDO
	Shutdown all active sockets.

**/

#include <signal.h>
#include <stdio.h>
#include <sys/socket.h>
#include <time.h>
#include <strings.h>
#include <stdarg.h>             /* Variable-length argument lists. */
#include <unistd.h>
#include "strmutil.p"
#include "net_util.h"
#include "gv_util.p"
#include "db_util.p"

/* Modified EPOCH file */
#define _STRM_ERR
#include "GAIM_strmerr.h"

/* Defines needed for PUB */
#define PUB_MAIN
#include "PUB_defines.h"

/* variables needed for load_db */	
static struct timeval network_timeout={60,0};
static struct timeval max_wait_time={10,0};
struct client_connection *network_connection;
streamdef *stream_def;

int PUB_yjd_to_seconds(char *yjd);
void PUB_read_GAIM_config(void);
void PUB_service_NQIM_init (void);
int PUB_get_archive_files(char *yjd_start,char *yjd_stop);
void PUB_service_UBG_socket (void);
void PUB_service_NQIM_socket (void);
void PUB_process_archive_record(void);
void GAIM_seconds_to_date(char *outdate,double secs);
int GAIM_calculate_checksum(int buffer_length,char *buffer);

void dump_generic_packet(char *pkt);
void load_tagfile(char *db_name);

/* global variables for pseudo and gva ftp */
int debug_pseudo = 0;
int debug_gva = 0;

#define SW_VERSION "Build 14 Jan-04-2011"
#define SW_ID      "PUB"

#include "../include/util_log.h"

/* default delay time in usecs between reads of archive file */
int	pub_delay_usecs = 1000;  /* 1 millisecs */

int main(int num_params, char *params[]) {
  
  int i,j;
  struct sigaction *signal_action;
  int status;
  
  /* Command line inputs */
  char current_param[256];
  
  /* -1=No '-' argument seen, yet, or finished processing previous '-' argument. */
  int argument_type=-1;
  
  /* Error Message String*/
  char message[256];
  
  /* Used for storing the node name. */
  char nodename[32];
  int nodenamelen=32;
  
  /* Text describing who spawned PUB */
  char run_modes[3][36]={
    /* **123456789*123456789*123456789*123456** */
    "run from the UNIX command line.", 
    "spawned by N-Q_Interface_Manager."};
  
  /* Start and stop times */
  char yjd_start_time[32],yjd_stop_time[32];
  
  /* Number of files returned by PUB_get_archive_file_list */
  int num_files;
  
  /* Command executed to start the UBG from PUB */
  char ubg_call[512];
  
  /* GTACS Name length */	
  short gtacs_name_len;
  
  int no_archive_found = -1; /* raj */
  int orig_start_secs  = -1; /* raj */
  int delta_secs = 0;
  
  gethostname(nodename,nodenamelen);
  /* Get the current host name */
  
  connect_to_stream[0]=0;
  process_number[0]=0;
  stream_name[0]=0;
  debug_filename[0]=0;
  database_name[0]=0;
  yjd_start_time[0]=0;
  yjd_stop_time[0]=0;
  gtacs_name[0]=0;
  merge_type[0]=0;
  stream_type[0]=0;
  /* Force the allocated space to become a 0 length string for each of 
     the above CHAR variables */
  
  /* ************************************
   * Proccess Command Line Parameters *
   ************************************ */
  
  for (i=1; i<num_params; i++) {
    /* Parameter Processing Loop */
    
    strcpy(current_param,*++params);
    /* Copy the next parameter to a temporary variable */
    
    if (strlen(current_param)!=0) {
      /* If there is another parameter */
      
      if (current_param[0]!='-') {
	/* If this is a not a new parameter flag */
	
	switch (argument_type) {
	  
	case 0:		/* Stream Name */
	  strcpy(stream_name,current_param);
	  break;
	  
	case 1:		/* Debug File Name */
	  strcpy(debug_filename,current_param);
	  break;
	  
	case 2:		/* Run mode */
	  if (current_param[0]=='t') run_mode=0;
	  else if (current_param[0]=='n') run_mode=1;
	  else {
	    printf("Invalid -r argument.  Program exiting.\n");
	    exit(1);
	  }
	  break;
	  
	case 3: 	/* Database Name */
	  strcpy(database_name,current_param);
	  break;
	  
	case 4: 	/* Archive Machine IP Address */
	  strcpy(archive_ip_address,current_param);
	  break;
	  
	case 5:		/* Playback Begin */
	  strcpy(yjd_start_time,current_param);
	  break;
	  
	case 6:
	  strcpy(process_number,current_param);
	  if (atoi(process_number)<START_PUB_CHANNEL || 
	      atoi(process_number)>START_PUB_CHANNEL+
	      MAX_PUB_PROCESSES-1) {
	    printf("Invalid Process Number: '%s'.  %s\n",
		   process_number,"Program exitting.");
	    exit(2);
	  } else {
	    strcat(ubg_service_name,process_number);
	    strcat(nqim_service_name,process_number);
	  }
	  break;
	  
	case 7:		/* Playback End */
	  strcpy(yjd_stop_time,current_param);
	  break;
	  
	case 8:		/* GTACS Name */
	  strcpy(gtacs_name,current_param);
	  break;
	  
	case 9:		/* SCID Number */
	  strcpy(scid_number,current_param);
	  break;
	  
	case 10:	/* Merge Type */
	  strcpy(merge_type,current_param);
	  break;
	  
	case 11:	/* Stream Type */
	  strcpy(stream_type,current_param);
	  break;
	  
	case 12:	/* Day Reference */
	  strcpy(yjd_stop_time,current_param);
	  yjd_stop_time[4]=0;
	  sprintf(yjd_start_time,"%s-%3.3i-00:00:00",
		  yjd_stop_time,atoi(&yjd_stop_time[5]));
	  break;
	  
	case 13:	/* "Connect To" Stream Name */
	  strcpy(connect_to_stream,current_param);
	  break;
	  
	default:	/* Unknown Error */
	  printf("Unknown Error in ARGUMENT_TYPE\n");
	  exit(3);
	  break;
	}
	
	argument_type=-1;
	/* Reset the Argument Type */
	
      } else {
	
	switch (current_param[1]) {
	  
	case 'A':	/* Flag the next parameter as an archive */
	case 'a':	/*	machine IP address */
	  argument_type=4;
	  break;
	  
	case 'B':	/* Flag the next parameter as a database name */
	case 'b':
	  argument_type=3;
	  break;
	  
	case 'C':	/* Flag the next parameter as a "connect to" */
	case 'c':	/*	stream name.                             */
	  argument_type=13;
	  break;
	  
	case 'D':	/* Flag the next param as a debug file name */
	case 'd':
	  argument_type=1;
	  debug_on=-1;
	  break;							
	  
	case 'F':	/* Flag the next parameter as a frame rate */
	case 'f':
	  argument_type=5;
	  break;
	  
	case 'G':	/* Flag the next parameter as a GTACS name */
	case 'g':
	  argument_type=8;
	  break;
	  
	case 'I':	/* Flag the next parameter as a SCID number */
	case 'i':
	  argument_type=9;
	  break;
	  
	case 'M':	/* Flag the next parameter as a Merge Type */
	case 'm':
	  argument_type=10;
	  break;
	  
	case 'P':
	case 'p':
	  if (current_param[2]=='B' || current_param[2]=='b') 
	    argument_type=5;
	  else if (current_param[2]=='E' || current_param[2]=='e')
	    argument_type=7;
	  else if (current_param[2]=='T' || current_param[2]=='t')
	    argument_type=12;
	  else {
	    printf("Invalid -p argument.  Program exitting.\n");
	    exit(4);
	  }
	  break;
	  
	case 'R':	/* Flag the next parameter as a Run Mode */
	case 'r':
	  argument_type=2;
	  break;
	  
	case 'T':	/* Flag the next parameter as a stream name */
	case 't':
	  argument_type=0;
	  break;
	  
	case 'V':	/* Process number (1 to 24) */
	case 'v':
	  argument_type=6;
	  break;
	  
	case 'Z':	/* Stream Type (S/R) */
	case 'z':
	  argument_type=11;
	  break;
	  
	default:	/* User Error */
	  printf("Unknown Parameter: %s\n",current_param);
	  exit(5);
	  break;
	  
	}
      }
    } 
  } 
  
  
  /* ****************************************
   * Act upon the command line parameters *
   **************************************** */
  
  if (strlen(debug_filename)==0 && !debug_on) {
    /* Was debug requested? */
    
    if (getenv("DEBUG_FILE")) {
      debug_on=-1;
      
      if (strlen(getenv("DEBUG_FILE"))>0) 
	strcpy(debug_filename,getenv("DEBUG_FILE"));
      /* Get the debug file name from the environment */
    }
  }
  
  
  /* get env variable */
  gaim_home = (char *) getenv("GAIM_HOME");
  if (!gaim_home) {
    /* exit if env var not defined */
    log_err("*** env variable GAIM_HOME is not defined");
    log_err("*** PUB terminating");
    exit(99);
  }
  
  if (getenv("GAIM_DEBUG_GVA"))   debug_gva = 1;
  
  gaim_playback = PLAYBACK_NORMAL;
  if (getenv("GAIM_PLAYBACK_GVA_ONLY"))     gaim_playback = PLAYBACK_GVA_ONLY;
  if (getenv("GAIM_PLAYBACK_PSEUDO_ONLY"))  gaim_playback = PLAYBACK_PSEUDO_ONLY;
  
  if (getenv("GAIM_PUB_DELAY_USECS"))
    pub_delay_usecs = atoi(getenv("GAIM_PUB_DELAY_USECS"));
  
  debug = 0;
  /* init log message ID */
  sprintf(logfile_msgid,"%s.%s",SW_ID,process_number);
  /* write startup messages */
  log_event("  ");
  log_event("Playback_Update_Buffer (%s) %s",SW_VERSION,run_modes[run_mode]);
  log_event("gaim_playback   = %d",gaim_playback);
  log_event("debug_gva       = %d",debug_gva);
  /*log_event("logfile_name     = %s",logfile_name);*/
  /*log_event("playback delay   = %d usecs",pub_delay_usecs);*/
  /*log_event("debug flag       = %d",debug);*/
  
  if (argument_type!=-1 && argument_type!=1) {
    log_event_err("arguments list incomplete. exiting");
    exit(6);
  }
  
  if (strlen(yjd_start_time)==0 || (strlen(yjd_stop_time)==0 && run_mode==0)) {
    log_event_err("Missing start and/or stop time. PUB exiting");
    exit(7);
  }
  
  /* *********************************************
   * Write initialization parameters to a file *
   ********************************************* */
  
  if (strlen(scid_number) != 0) {
    log_event("PLAYBACK  scid_number=%s  stream=%s",scid_number,stream_name);
  }
  
  if (run_mode == 0) {
    log_event("PLAYBACK StartTime : %s",yjd_start_time);
    log_event("PLAYBACK StopTime  : %s",yjd_stop_time);
  } 
  
  if (strlen(stream_name) != 0) {
    log_event("stream_name = %s",stream_name);
  }
  
  if (strlen(connect_to_stream) != 0) {
    log_event("connect_to_stream = %s",connect_to_stream);
  }
  
  if (strlen(database_name) != 0) {
    log_event("database_name = %s",database_name);
  }
  
  if (strlen(gtacs_name) != 0) {
    log_event("gtacs_name = %s",gtacs_name);
  } else {
    log_event("nodename = %s",nodename);
  }
  
  if (strlen(archive_ip_address)!=0) {
    log_event("archive_ip_address = %s",archive_ip_address);
  }
  
  /* set node name to current node if no gtacs name specified */
  if (strlen(gtacs_name)==0) strcpy(gtacs_name,nodename);
  
  /* Convert Year-Julian Date to Seconds */
  start_secs = PUB_yjd_to_seconds(yjd_start_time);
  if (run_mode==0) stop_secs = PUB_yjd_to_seconds(yjd_stop_time);
  
  /* process GAIM config file */
  PUB_read_GAIM_config();
  
  /* *******************************************************
   * Set up stream activation and cancellation routines. *
   ******************************************************* */
  
  /* Allocate space for the signal_action structure */
  signal_action = (struct sigaction *)calloc(1, sizeof(struct sigaction));
  if (!signal_action) {
    log_event_err("cannot allocate space for signal_action");
    exit(8);
  }
  
  /* Init the signal handler flag */
  signal_action->sa_handler = signal_handler;
  
  if (sigaction(SIGINT, signal_action, (struct sigaction *)NULL)) {
    log_event_err("cannot set up SIGINT signal handler");
    exit(9);
  }
  
  if (sigaction(SIGTERM, signal_action, (struct sigaction *)NULL)) {
    log_event_err("cannot set up SIGTERM signal handler");
    exit(10);
  }
  
  
  /* ************************************************************************
   * Get value of STRMID from the environment.  STREAMID is automatically *
   *     set by EPOCH when a stream is initialiazed.                      *
   ************************************************************************ */
  
  /* get stream name from env if not specified */
  if (strlen(stream_name) == 0) {
    if (!getenv("STREAMID")) {
      log_err("*** env variable STREAMID is not defined");
      exit(11);
    }
    strcpy(stream_name,getenv("STREAMID"));
  }
  
  /* must connect to a stream (any stream) in order for load_db to work */
  if (strlen(connect_to_stream) > 0) {
    status = connect_stream(connect_to_stream,network_timeout,&network_connection);
    if (status != STRM_SUCCESS) {
      log_event_err("connect_stream(%s) err=%d %s",connect_to_stream,status,stream_err_messages[0-status]);
    } else {
      log_event("connected to stream %s",connect_to_stream);
    }
  }
  
  /* get SCID from env if not specified */
  if (strlen(scid_number) == 0) {
    if (!getenv("SPACECRAFT_ID")) {
      log_err("*** env variable SPACECRAFT_ID is not defined");
      exit(13);
    }
    strcpy(scid_number,getenv("SPACECRAFT_ID"));
  }
  
  
  /* get merge type from env if not specified  */
  if (strlen(merge_type) == 0) {	
    if (!getenv("MERGE_TYPE")) {
      log_err("*** env variable MERGE_TYPE is not defined");
      exit(14);
    }
    strcpy(merge_type,getenv("MERGE_TYPE"));
  }
  
  /* get stream type from env if not specified  */
  if (strlen(stream_type)==0) {
    if (!getenv("STREAM_TYPE")) {
      log_err("*** env variable STREAM_TYPE is not defined");
      exit(15);
    }
    strcpy(stream_type,getenv("STREAM_TYPE"));
  }
  
  
  /* ****************************************
   * TCP/IP Communications Initialization *
   **************************************** */
  
  /* If process (service) number is given, then connect to Update_Buffer_Generator */
  if (strlen(process_number) > 0) {
    
    /* Initialize the sockets, but do not try to communicate, yet */	
    status = net_answer(ubg_service_name,-99,&listen_for_ubg,&read_write_ubg);
    if (status != 0) {
      
      /* connect failed, terminate */
      sprintf(message,"connect to UBG failed. status = %i",status);
      log_event_err(message);
      strcat(error_message,message);
      end_program = -1;
      
    } else {
      
      /* connect successful, set up socket for read-write service */
      FD_SET(listen_for_ubg,&read_mask);
      log_event("connected to UBG");
      
    }
    
  } else {
    log_event_err("no process number specified. no connection attempted");
  }
  
  if (run_mode==1 && strlen(process_number)>0) PUB_service_NQIM_init();
  
  
  /* *************************************
   * Start the Update_Buffer_Generator *
   ************************************* */
  
  if (!end_program) {
    
    sprintf(ubg_call,"/bin/sh -c \"rm -rf temp_env_list; printenv | sort > temp_env_list ; ./Update_Buffer_Generator -r pub -v %s -i %s -m %s -z %s -a %s -c %s &\"",
	    process_number,scid_number,merge_type,stream_type,archive_ip_address,connect_to_stream);
    system(ubg_call);
    
    if (status) {
      sprintf(message,"Cannot start Update_Buffer_Generator. PUB exiting");
      log_event_err(message);
      strcat(error_message,message);
      end_program = -1;
    }
    
  }
  
  /* process merge requests */
  orig_start_secs = start_secs; /* raj */
  for (j=0; j<number_merge_requests; j++) {
    
    if (run_mode==1) {
      start_secs = orig_start_secs;
      GAIM_seconds_to_date(yjd_start_time,(double)start_secs+
			   (double)day_offset_start_ms[j]/1000.0);
      GAIM_seconds_to_date(yjd_stop_time,(double)start_secs+
			   (double)day_offset_start_ms[j]/1000.0+
			   (double)duration_ms[j]/1000.0);
      
      log_event("processing merge req %i-of-%i  mergetype=%s  stream=%s  sc=%s",j+1,number_merge_requests,
		merge_type,stream_name,scid_number);
      log_event("PLAYBACK  start_time = %s",yjd_start_time);		
      log_event("PLAYBACK  stop_time  = %s",yjd_stop_time);
      
      /* summary for export to spreadsheet */
      delta_secs = PUB_yjd_to_seconds(yjd_stop_time) - PUB_yjd_to_seconds(yjd_start_time);
      log_event("%sMERGE  %s  %s  %s  %s  %d",
		merge_type,scid_number,stream_name,yjd_start_time,yjd_stop_time,delta_secs);
      
    }
    
    /* ******************************************
     * Get list of Archive Files to read from *
     ****************************************** */
    
    if (!end_program) {
      
      num_files = PUB_get_archive_files(yjd_start_time, yjd_stop_time);
      if (num_files <= 0) {
	
	sprintf(message,"error locating archive files to playback. status=%i",num_files);
	log_event_err(message);
	strcat(error_message,message);
	end_program=-1;
	
	
      } else {
	no_archive_found = 0;  /* raj */
	/*log_event("playing back %i archive files",num_files);*/
	for (i=0; i<num_files; i++) {
	  log_debug("   %4i  %s",i+1,archive_file_list[i]);
	}
	
      }
      
      /* If this is the last set to playback and no archives exist */
      if (end_program && no_archive_found) {   /* raj */
	/* answer request with final connect msg,so we can tell UBG to shutdown */	
	status = net_answer(ubg_service_name, 99, &listen_for_ubg, &read_write_ubg); 
	
      } else {
	/* allocate the archive control block pointer */
	archive_pointer=(struct archive_cb *)calloc(1,sizeof(struct archive_cb));
	
	/* Convert Year-Julian Date to Seconds */
	start_secs=PUB_yjd_to_seconds(yjd_start_time);
	stop_secs=PUB_yjd_to_seconds(yjd_stop_time);
	
	/* Point to the start of the global buffer */
	global_buffer_ptr=global_buffer;
	global_message_length=0;
	global_ctr=0;	
      }
    }
    
    
    /* *********************
     * Wait Forever Loop *
     ********************* */
    
    /* If program has not had an error condition... */
    if (!end_program) {
      
      /* Are we on the last file? */
      on_last_file=0;
      
      for (i=0; i<num_files; i++) {
	
	if (i==num_files-1) on_last_file=-1;
	
	/* Done with previous file.  Start anew with buffer cleanup when needed. */	
	clear_buffers=0;
	
	/* Not end of archive file, yet */
	end_of_file=0;
	
	/* No archive blocks read yet */
	block_counter=0;
	
	/* New archive file */
	new_archive=-1;
	
	/* Open the current archive file */
	status = archive_open(archive_pointer,archive_file_list[playback_order[i]]);
	if (status) {
	  /* open failed */
	  log_event_err("cannot open archive file %s",archive_file_list[playback_order[i]]);
	  end_of_file = -1;
	  
	} else {
	  log_event("playing back file %d-of-%d : %s",i+1,num_files,archive_file_list[playback_order[i]]);
	  /* force read of first archive block */
	  end_of_block=-1;
	}		
	
	/* Set the time to use for global variables */
	global_time.tv_sec=archive_pointer->arch_header.start_time.tv_sec;
	global_time.tv_usec=archive_pointer->arch_header.start_time.tv_usec;
	
	/* Extract database name from archive file */
	strcpy(database_name,archive_pointer->arch_header.database);
	
	/* Allocate memory for the stream definition structure */
	stream_def = (struct streamdef *)calloc(1,sizeof(struct streamdef));
	stream_def->database = (char *)calloc(512,sizeof(char));
	strcpy(stream_def->database,database_name);
	stream_def->stream_id = 0;
	
	/* load stream definition */
	if (load_db (&stream_def->dbhead, stream_def, NULL)) {
	  log_event_err("(PUB_main) load_db error. db=(%s) stream_id=%d",stream_def->database,stream_def->stream_id);
	  free(stream_def->database);
	  free(stream_def);
	  stream_def = NULL;
	} else {
	  log_event("(PUB_main) load_db done. db=(%s) stream_id=%d",stream_def->database,stream_def->stream_id);
	  status = init_gv_util(stream_def->dbhead);
	  if (status) {
	    log_event("(PUB_main) init_gv_util error = %i",status);
	    free(stream_def->database);
	    free(stream_def);
	    stream_def = NULL;
	  }
	  
	} 
	
	/* load *.tag file to get tags for archived global vars */
	load_tagfile(database_name);
	
	while ((!end_program && (!end_of_file || clear_buffers>1)) || 
	       waiting_for_response) {
	  
	  /* **************************
	   * Send GTACS name to UBG *
	   ************************** */	
	  /* Send initialization messages */
	  if (init_messages>0 && !ubg_no_connection && !waiting_for_response) {
	    
	    /* Point to the start of the buffer */
	    tlm_buffer_ptr=tlm_buffer;
	    tlm_message_length=0;
	    
	    /* Copy packet id */
	    memcpy(tlm_buffer_ptr,
		   &packet_ids[UBG_GTACS_PKT_NUMBER],2);
	    tlm_buffer_ptr+=2;
	    tlm_message_length+=2;
	    
	    /* Include NULL pointer in count. */
	    gtacs_name_len=strlen(gtacs_name)+1;
	    
	    /* Copy GTACS name length */
	    memcpy(tlm_buffer_ptr,&gtacs_name_len,2);
	    tlm_buffer_ptr+=2;
	    tlm_message_length+=2;
	    
	    /* Copy the gtacs name */
	    memcpy(tlm_buffer_ptr,gtacs_name,gtacs_name_len);
	    tlm_message_length+=gtacs_name_len;
	    
	    /* Send the database name */
	    log_debug("send gtacs database name %s to UBG",gtacs_name);
	    net_write(read_write_ubg,tlm_buffer,tlm_message_length);
	    last_crc = GAIM_calculate_checksum(tlm_message_length,tlm_buffer);
	    
	    /* Set flags */
	    waiting_for_response=-1;
	    init_messages--;
	    
	  }
	  
	  
	  /* ********************************************************
	   * Check Update_Buffer_Generator socket for information *
	   ********************************************************
	   */
	  /* If connected to UBG */
	  if (strlen(process_number)>0) PUB_service_UBG_socket();
	  
	  /* ******************************************************
	   * Check N-Q_Interface_Manager socket for information *
	   ****************************************************** */
	  
	  /* If connected to NQIM */
	  if (strlen(process_number)>0 && run_mode==1) PUB_service_NQIM_socket();
	  
	  
	  /* ***************************************
	   * Get and process next archive record *
	   *************************************** */
	  
	  /* Process next archive record, only if not in the process of dying. */
	  if (strlen(error_message) <= 8) {
	    
	    /* usleep(pub_delay_usecs); make playback too slow */	
	    PUB_process_archive_record();
	    
	    /* If this is the last file, and we are at the 
	       end of the file, and we are not waiting on 
	       a response and we are not clearing the 
	       PSEUDO buffer, send an "END_OF_DATA" message */
	    
	    if (on_last_file && end_of_file && 
		!waiting_for_response && clear_buffers<2) {
	      /* Point to the start of the buffer */
	      tlm_buffer_ptr=tlm_buffer;
	      tlm_message_length=0;
	      
	      /* GAS claims we should only send 1 EOH/EOI per merge request */
	      if (j+1 == number_merge_requests) {
		/* Copy packet id */
		memcpy(tlm_buffer_ptr,&packet_ids[UBG_EOH_EOI_PKT_NUMBER],2);
		tlm_buffer_ptr+=2;
		tlm_message_length+=2;
		/* Send the database name. */
		log_event("Send EOH/EOI message to UBG");	
		net_write(read_write_ubg,tlm_buffer,tlm_message_length);
		last_crc = GAIM_calculate_checksum(tlm_message_length,tlm_buffer);
		/* Wait for response, but do not repeat this 
		   code once the response is seen */							
		waiting_for_response = -1;
		on_last_file = 0;
		
	      } /* Raj */
	    }
	  }
	  
	  if (end_program) waiting_for_response=0;
	}	
	
	if (end_program) {	
	  /* If program end requested, do it */
	  i = num_files;					
	  /* Close the archive file to end program, too */
	  archive_close(archive_pointer);		
	} 
	
      } /* enddo i < num_files */
    } /* endif end_program */
  } /* enddo j < number_merge_requests */
  
  /* ***********************************
   * Shutdown Playback_Update_Buffer *
   *********************************** */
  
  if (strlen(process_number)>0 && !ubg_shutdown) {
    /* If this was spawned with a process number and it is shutting down
       on its own (i.e. UBG is not shutting down), send a socket 
       shutdown message to UBG and shut down the socket */
    
    if (read_write_ubg>-1) {
      net_write(read_write_ubg,"zzzzzzzz",8);
      /* Tell UBG to shutdown */
      
      shutdown(read_write_ubg,SHUT_RDWR);
      close(read_write_ubg);
      FD_CLR(read_write_ubg,&read_mask);
      read_write_ubg = -1;
      /* Close down the read/write socket */
    }
    
    if (listen_for_ubg>-1) {
      shutdown(listen_for_ubg,SHUT_RDWR);
      close(listen_for_ubg);
      FD_CLR(listen_for_ubg,&read_mask);
      listen_for_ubg = -1;
      /* Close down the listening socket */
    }
  }
  
  /* If this was spawned with a process number and it is shutting down
     on its own (i.e. IFM is not shutting down), send a socket 
     shutdown message to IFM and shut down the socket */
  
  if (strlen(process_number)>0) { /* && !nqim_shutdown) { */
    
    /* Tell IFM that PUB is terminating */
    if (read_write_nqim > -1) {	
      
      if (strlen(error_message) == 8) {
	log_event("send normal shutdown msg to IFM: (%s)",error_message);
	net_write(read_write_nqim,"zzzzzzzz",8);
      } else { 
	/* net_write(read_write_nqim,error_message,strlen(error_message+1)); */
	log_event("send error shutdown msg to IFM: (%s)",error_message);
	net_write(read_write_nqim,ERROR_MESSAGE_HEADER,8);
      }
      
      /* Close socket */
      shutdown(read_write_nqim,SHUT_RDWR);
      close(read_write_nqim);
      FD_CLR(read_write_nqim,&read_mask);
      read_write_nqim=-1;	
    }
    
  }
  
  /* Free pointers assigned during the NQIM socket initialization */
  if (run_mode==1 && number_merge_requests>0) {
    free(day_offset_start_ms);
    free(duration_ms);
  }
  
  /* Free the archive control block pointer */
  free(archive_pointer);
  
  /* release memory for stream definition */
  if (stream_def) {
    free(stream_def->database);
    free(stream_def);
  }
  
  /* No Error exit */
  log_event("Playback_Update_Buffer normal exit");
  exit(0);
  
}


/* *********************
 * ^C Signal Handler *
 ********************* */

static void signal_handler(int signal) {
  
  char message[256];
  
  if (signal==SIGINT)
    sprintf(message,"Playback_Update_Buffer killed by ^C");
  else if (signal==SIGTERM)
    sprintf(message,"Playback_Update_Buffer killed by SIGTERM");
  
  if (signal==SIGINT || signal==SIGTERM) {
    end_program = -1;
    log_event(message);
    strcat(error_message,message);
  }
  
} /* end signal_handler */


void dump_generic_packet(char *pkt) {
  
  int i;
  
  for (i=0; i<16; i+=2) printf("%4.4X ",*((unsigned short *)(pkt+i)));
  printf("\n");
  
} /* end dump_generic_packet */


/***
 *** load ggsa file
 ***
 */
void load_tagfile (char *db_name) {
  
#define  NAMESIZE  1024
#define  BUFSIZE  1024
  
  char 	filename[NAMESIZE];
  char 	inbuf[BUFSIZE];
  FILE 	*inFILE = NULL;
  int		status,n;
  int		total_tag = 0;
  int		tagvalue;
  char 	*tag, *name;
  char	*eol;
  gvar 	*gid;
  
  strcpy(filename,db_name);
  n = strlen(filename) - 3;		
  if (n <= 0) return;
  
  sprintf(&filename[n],"tag");
  
  /** open file **/
  inFILE = fopen(filename,"r");
  if (!inFILE) {
    log_event("Cannot open tag file (%s)",filename);
    return;
  } 
  
  log_event("opened tag file (%s)",filename);
  total_tag = 0;
  
  /** read file until eof **/
  while (inFILE) {
    
    /** read a line from file **/
    if (fgets(&inbuf[0],BUFSIZE,inFILE) == NULL) break;
    
    /** get first 2 tokens in line **/ 
    tag = (char *)strtok(inbuf," =;\n");
    name = (char *)strtok(NULL," =;\n");
    
    if (tag && name) {
      /* look up global variable */
      tagvalue = atoi(tag);
      gid = (gvar *)gv_id(stream_def->dbhead, name);
      if (gid ) {
	gid->global_var->set = tagvalue;
	total_tag++;
      } else {
	log_event_err("(load_tagfile) gv_id(%s) error",name);
      }
      /*if (total_tag >= 1 && total_tag <= 10) 
	log_event("*** name=(%s)  tag=(%d)",name,tagvalue);*/
      
    }
    
  }
  
  log_event("saved %d tags from tagfile",total_tag);
  fclose(inFILE);
  
} /* end load_ggsa */



#include "../include/util_log.c"
