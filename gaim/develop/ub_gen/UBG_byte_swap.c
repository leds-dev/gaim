/*  
@(#)  File Name: UBG_byte_swap.c  Release 1.1  Date: 00/11/10, 13:25:58
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_byte_swap.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles       Initial creation

********************************************************************************
	Invocation:
        
		UBG_byte_swap(char *string, int numbytes)
        
	Parameters:

		*string - I
			A pointer to the start of the bytes to be swapped.

		numbytes - I
			Number of bytes to swap.  

********************************************************************************
	Functional Description:

		This routine swaps bytes to go from network order to NT order.
	
*******************************************************************************/

/**

PDL:
	DOFOR each byte in the input string 
		Copy the byte to the corresponding location when counting from the 
		 other end of the string.
	ENDDO

**/
#include <string.h>

void UBG_byte_swap(char *string,int numbytes) {

	char buffer[32];
		/* Temporary Storage */

	int i;
		/* Counter */


	for (i=0; i<numbytes; i++) buffer[i]=string[numbytes-i-1];
		/* Swap the bytes. */

	memcpy(string,buffer,numbytes);
		/* Copy the swapped bytes back into the source string */

}
