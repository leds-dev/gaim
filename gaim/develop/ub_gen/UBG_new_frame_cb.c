/*  
@(#)  File Name: UBG_new_frame_cb.c  Release 1.9  Date: 07/03/25, 09:12:26
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_new_frame_cb.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles       Initial creation
	 	 8.4         mar18-2004  K. Dolinh	 	 Add log_event, log_debug
         8.B1        sep23-2004                  - remove frame_type check in debug
		 11										 change call to UBG_send_buffer

********************************************************************************
	Invocation:
        
		UBG_new_frame_cb(struct client_frame *current_frame);
        
	Parameters:

		current_frame - I
			The structure that contains the frame which invoked this callback.  
			It also contains additional information about the frame as defined 
			in the EPCOH stream services documentation for the client_frame 
			structure.

********************************************************************************
	Functional Description:

		This routine is defined as the callback to be called each time the 
		EPOCH stream services has received a new frame.

		NOTE: This routine is only used during testing.  It is not a routine 
		that is used under normal operations.  It is only used when the user
		wants to bypass the main GAIM software and feed data directly from an 
		EPOCH stream to the Update_Buffer_Sender (bypassing Stream_Data_Receiver
		and Playback_Update_Buffer)
	
*******************************************************************************/

/**

PDL: 
	Get the frame type from the frame.
	Get the telemetry rate from the frame.
	Extract the time from the frame.
	Set other Update Buffer parameters that can be found in the frame.
	Decommutate the frame.
	Create the data records for the Update Buffer from the decommutated data.
	CALL UBG_create_buffer_to_send	
	CALL UBG_send_buffer.

**/

#include "strmutil.p"
#include "db.h"
#include "tlm.p"

	/* EPOCH Libraries */

#include "UBG_structures.h"

	/* Update_Buffer_Generator structures */

void UBG_new_frame_cb(struct client_frame *current_frame) {
		/* Callback for each frame */

	int frame_ms,frame_day,frame_year;
		/* Time Variables */

	int i;
		/* Loop Counter */

	tlm_frame *to_be_decommed;
		/* Telemetry frame structure */

	char *buffer_to_send;
		/* The update buffer to be sent. */

	char message[256];
		/* Debug message buffer */

	int full_buffer_length=0;
		/* The length of the full update buffer */

	int telemetry_rate=0;
		/* PCM: 0=1Kbps, 1=4Kbps.  SXI/AXS 1, 2, ..., 10 seconds per frame */

	int frame_type;
		/* Frame Type */

	int status;
		/* EPOCH stream service status. */

	int mask_to_byte_translation[UBG_MASK_TO_BYTE_SIZE]=UBG_MASK_TO_BYTE;
			/* Translate from a mask to a number of bytes */

if (strlen(scid)==0) strcpy(scid,"goes14");
saw_a_frame=-1;
send_data=-1;
	/* Since this routine only runs in TEST mode, we need to force the 
		above three parameters to always send frames and pseudos onto the 
		UBS. */

update_buffer.number_of_mnemonics=0;
	/* Start with a fresh buffer */

update_buffer.data_header.frame_type=UBG_PCM1_CODE;
frame_type=UBG_PCM1_CODE;
	/* Force to say PCM1 when not connected to Barb's code. */

if (run_mode==0) {
	/* We only need to look for these values ourselves when being run 
	in test mode.  If run from SDR or PUB, they should let UBG 
	know what the current value is. */
	
	/* Get current value of FRAME_TYPE */
	status=poll_point(telemetry_rate_pid);
	if (frame_type==PCM1_FRAME_TYPE || frame_type==PCM2_FRAME_TYPE) {
		/* Get the current PCM TELEMETRY_RATE.  0=1Kbps, 1=4Kbps */
		telemetry_rate=*(int *)frame_type_pid->value;	
	} else {
		log_event_err("unknown bitrate for frame_type = %i",frame_type);
	}
	
	/* By C/L, this number should be correct */
} else telemetry_rate = current_telemetry_rate;
	
/* Calculate the number of milliseconds since the start of the day */
frame_ms=(current_frame->ufargs->header->receipt_time.tv_sec%86400)*1000 +
	current_frame->ufargs->header->receipt_time.tv_usec/1000;

/* Calculate the day of the current frame. */
frame_day=(current_frame->ufargs->header->receipt_time.tv_sec/86400);

/* Convert from days to year-day */	
UBG_days_and_years(&frame_day,&frame_year);
	
/* Modify the data header with the base time. */
update_buffer.data_header.year=frame_year;
update_buffer.data_header.day=frame_day;
update_buffer.data_header.milliseconds=frame_ms;

/* Questionable Data */
update_buffer.data_header.status = 0;
if ((current_frame->ufargs->header->quality & QUESTIONABLE_FRAME)==QUESTIONABLE_FRAME) 
	update_buffer.data_header.status|=UBG_BAD_QUALITY;
	
/* clear Mnemonic Counter */		
update_buffer.number_of_mnemonics=0;
	
/* Initialize the message length to zero */
update_buffer.message_header.message_size=0;

/* Allocate space for the new frame */
to_be_decommed=(struct tlm_frame *)calloc(1,sizeof(struct tlm_frame)+
	current_frame->ufargs->frame_msg.frame_msg_len);
		
/* Set up the header for the telemetry frame to be decommed. */
to_be_decommed->header.receipt_time.tv_sec=
	current_frame->ufargs->header->receipt_time.tv_sec;
to_be_decommed->header.receipt_time.tv_usec=
	current_frame->ufargs->header->receipt_time.tv_usec;
to_be_decommed->header.quality=current_frame->ufargs->header->quality;
to_be_decommed->header.size=current_frame->ufargs->header->size;
	
/* Copy received frame to the structure to be passed to the decom routine. */
memcpy(to_be_decommed->frame,
	current_frame->ufargs->frame_msg.frame_msg_val,
	current_frame->ufargs->frame_msg.frame_msg_len);
			

log_debug("received frame with time = %i.%6.6i",
		current_frame->ufargs->header->receipt_time.tv_sec,
		current_frame->ufargs->header->receipt_time.tv_usec);

/* Decommutate the frame. */
decom_frame(stream_def->dbhead,to_be_decommed);
	
/* Set the status bits in the data header */
UBG_set_data_header_status();
	
/* Calculate size of update buffer */
update_buffer.data_header.data_size=
	SIZE_DATA_HEADER_BASE+FORCED_MAX_DB_NAME_LEN;
for (i=0; i<update_buffer.number_of_mnemonics; i++) {
	if (update_buffer.tlm_or_pseudo[i]==0)
		update_buffer.data_header.data_size+=SIZE_DATA_BODY_BASE+
		mask_to_byte_translation[UBG_GET_DATA_MASK(update_buffer.data_body[i].status_flags)];
}
	
/* Allocate space for the update buffer.  The total space allocated 
  will be the size of the message header plus the size of the 
  data header plus the size of each mnemonics in the buffer.  
  The extra 256 bytes allocated allow for strings to fill in
  the spots of char pointers. */					
buffer_to_send=(char *)calloc(sizeof(update_buffer.message_header)+
	update_buffer.data_header.data_size+256,sizeof(char));
		
/* Create the actual buffer to send (only telemetry). */
UBG_create_buffer_to_send(&full_buffer_length,buffer_to_send,0);
	
/* Send the buffer */
UBG_send_buffer(DATATYPE_FRAME,full_buffer_length,buffer_to_send);
	
/* Increment message number for next message */
update_buffer.message_header.message_number++;
	
/* release allocated memory */
free(buffer_to_send);
free(to_be_decommed);
	

}
