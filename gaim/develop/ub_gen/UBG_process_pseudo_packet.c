/*  
@(#)  File Name: UBG_process_pseudo_packet.c  Release 1.9  Date: 07/06/04, 16:42:48
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_process_pseudo_packet.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        04/00       S. Scoles       Initial creation
	     8.4         mar18-2004  K. Dolinh	 	Add log_event, log_debug
		 11			 Aug24-2006					Build 11
												- update stats in shared mem
												- update field status_flags
					 Nov01-2006                 - check/set data-questionable flag
		 										- add new arg datatype
												- del ref to PSEUDO_TIME_EQ_FRAME_TIME
												- check time for playback data 
					 Apr24-2007					- add tlmwatch
					 May14-2007					- add info to BAD QUALITY msg	
												
********************************************************************************
	Invocation:
        
		UBG_process_pseudo_packet(int datatype, int bytes,char *packet)
        
	Parameters:

		nytes - I
			The size of the pseudo packet from PUB.

		frame_buffer - I
			The pseudo packet from PUB.

********************************************************************************
	Functional Description:

		This routine takes the pseudo list from PUB and formats it to be 
		converted to an update buffer.
	
*******************************************************************************/

/**

PDL:
	DOFOR each pseudotelemetry point in the packet
		Extract the sample time, value, index, and status.
		IF this is the first pseudotelemetry pint in the list THEN
			Prepare the Update Buffer for the pseudotelemetry data.
		ENDIF
		Add the current pseudotelemetry point to the Update Buffer.
	ENDDO

	CALL UBG_Set_Data_header_status
	Combine all pseudotelemetry points in a single block and add it to the 
	 Update Buffer header.
	CALL UBG_create_buffer_to_send
	CALL UBG_send_buffer		

**/


#include "db.h"
#include "db_util.p"
#include "tlmsim_util.p"
	/* Epoph database structures and routines */

#include "UBG_structures.h"
#include "../include/util_log.h"

/* global variables */
extern GaimStat *gaimstat;

extern int ftp_mode;
extern int debug_pseudo;
extern int debug_gva;
extern int gva_count;

static struct timeval prev_pseudo_time = {0.0};

void UBG_process_pseudo_packet (int datatype, int bytes, char *packet) {


/* Where are we in the packet? */
char *packet_ptr;

/* How many pseudos are in this packet? */
short total_pseudos;

/* Counter */
int i;

/* Time stamp */
struct timeval pseudo_time;

/* Global variable index number */
int pseudo_index;

/* Global variable status flags */
unsigned int pseudo_status;

/* Global Varible DB Pointer */
struct gvar *gid;

/* extracted pseudo converted to double */
double raw_value;

/* temporary long value */
unsigned int temp_long;

/* Temporary character buffer */
char temp_char[1024];

/* Error message array */
char message[1024],message1[1024];

/* Time Variables */
int frame_ms,frame_day,frame_year;

/* The update buffer to be sent. */
char *buffer_to_send;

/* More update buffer information */
int offset_ms;

/* How long is the data field? */
short int num_bytes;

/* buffer for hex dump of data */
char short_buff[10];
char hex_buffer[64];

/* 0=Integer Raw, 1=Float Raw, 2=Byte Array */
int int_or_float=0;

/* Is the point an integer, float, or byte array? */
char int_or_float_letter[3]={'I','F','B'};

/* The length of the full update buffer */
int full_buffer_length=0;

/* number of pseudos with bad quality */
int bad_pseudo_count = 0;
int bad_pseudo_index = 0;
int bad_pseudo_status = 0;

int dump_fd, dump_offset, dump_bytes;

/* variables for state context switch */
point *pid;
tlm_point  *tlm_pid;
LIST db_points;
int number_of_mnems_in_db;	
int state_table_number;	
int help_index;

/* point struct from database */
tlm_value * cv;

/* Set the packet index pointer to the packet start -- the 2-byte 
   packet ID was skipped over in the calling function. */
packet_ptr = packet;

/* dump_the_packet(packet,bytes); */

/* Extract the pseudo count out of the packet.  */
total_pseudos=*(short *)packet_ptr;
packet_ptr+=2;

/* No status info for pseudos (?) */
update_buffer.data_header.status=0;

/* Set Mnemonic Counter */
update_buffer.number_of_mnemonics=total_pseudos;

/* Initialize the message length to zero */
update_buffer.message_header.message_size=0;

/* int number of points with bad quality */
bad_pseudo_count = 0;
bad_pseudo_index = 0;
bad_pseudo_status = 0;

if (debug_gva && datatype == DATATYPE_PB_PSEUDO)
	log_event("(UBG_process_pseudo_packet) DATATYPE_PB_PSEUDO  bytes=%d  total_pseudos=%d",bytes,total_pseudos);

for (i=0; i<total_pseudos; i++) {
		/* For each pseudo in the packet */

	/* *********************************************
	   * First extract information from PUB packet *
	   ********************************************* */

	/* dump_the_packet(packet_ptr,24); */

	
	/* extract sample time */
	memcpy(&pseudo_time,packet_ptr,8);
	packet_ptr+=8;

	if (datatype == DATATYPE_PB_PSEUDO) {
		/* use prev time if new time is not same or increasing */
		if (pseudo_time.tv_sec < prev_pseudo_time.tv_sec) {
			if (debug_gva) log_event("(process_pseudo_packet) playback pseudo_time=%d %d  LESS THAN prev_time=%d %d",
				pseudo_time.tv_sec,pseudo_time.tv_usec,
				prev_pseudo_time.tv_sec,prev_pseudo_time.tv_usec);
			pseudo_time = prev_pseudo_time;
		} else {
			prev_pseudo_time = pseudo_time;
		}
	}
		
	/* Extract pseudo variable index */
	memcpy(&pseudo_index,packet_ptr,4);
	packet_ptr+=4;

	/* Extract pseudo variable status flags */
	memcpy(&pseudo_status,packet_ptr,4);
	packet_ptr+=4;

	/* How many bytes of data? */
	num_bytes=8;

	memcpy(&raw_value,packet_ptr,8);
	int_or_float=1;
	memcpy(&update_buffer.data_body[i].raw_value[0],&raw_value,num_bytes);

	/* point to the start of the next record */
	packet_ptr+=8;
		
	if (datatype == DATATYPE_PB_PSEUDO) {
		  /* convert gsa index to database help index */
		  help_index = (pseudo_index & 0XFFFF);
		  /* init loop */
		  number_of_mnems_in_db = 0;
		  db_points = (LIST) HeadOfList(stream_def->dbhead->points);
		  pid = NULL;
		  /* find and set value of pseudo in shared memory */
		  while (db_points!=(void *)NULL) {

			  pid = (point *)(stream_def->dbhead->tlm_head->point_array[number_of_mnems_in_db]);
			  if (pid) tlm_pid = (tlm_point *)pid->tlm_point;
			  if (tlm_pid && tlm_pid->help_index == help_index) {
				  if (debug_gva) 
				  		log_tlmwatch("PB_PSEUDO %32.32s  pseudo.x=%d  help.x=%d  number=%d  value=%g",
				  		TLM_MNEMONIC(stream_def->dbhead,number_of_mnems_in_db),
				  		pseudo_index,help_index,number_of_mnems_in_db,raw_value);
				  /* save pseudo to shared memory */
				  cv = (tlm_value *) pid->value;
				  cv->update_status = pseudo_status;
				  cv->raw_value = raw_value;
				  if (pid->shared_value != NULL) {
					((tlm_value *)(pid->shared_value))->update_status = pseudo_status;
					((tlm_value *)(pid->shared_value))->raw_value = raw_value;
				  }
				  set_raw_value(raw_value, pid);
				  break;
			  }		
			  number_of_mnems_in_db++;
			  db_points = db_points->next;
			  
		  }

	}
	
			
	/* ***********************************
	   * Next, fill in the update buffer *
	   *********************************** */

	/* We need to init the data_header when the first pseudo is analyzed */
	if (i==0) {

		/* Calculate the number of milliseconds since the start of the day */
		frame_ms=(pseudo_time.tv_sec%86400)*1000+pseudo_time.tv_usec/1000;
		/* Calculate the day of the current frame. */
		frame_day=pseudo_time.tv_sec/86400;
			
		/* Convert from days to year-day */
		UBG_days_and_years(&frame_day,&frame_year);
			
		/* Modify the data header with the base time. */
		update_buffer.data_header.year=frame_year;
		update_buffer.data_header.day=frame_day;
		update_buffer.data_header.milliseconds=frame_ms;
			
	}

	/* Calculate the difference between the pseudo time and the bufferstart time */
	offset_ms=(pseudo_time.tv_sec%86400)*1000+pseudo_time.tv_usec/1000;
	offset_ms-=update_buffer.data_header.milliseconds; 

	/* If the number is negative, it means midnight passed in the 
		middle of the pseudo packet.  We need to add a days worth of 
		milliseconds to get the correct offset. */
	if (offset_ms<0) offset_ms+=86400000; 

	/* Convert from long int to unsigned short int */
	update_buffer.data_body[i].time_offset=(unsigned short)(offset_ms);

	/* Init the status flags. */
	update_buffer.data_body[i].status_flags=0;
	

/***** BEGIN get context state 
	
	number_of_mnems_in_db = 0;
	db_points = (LIST) HeadOfList(stream_def->dbhead->points);
	pid = NULL;
	while (db_points!=(void *)NULL) {

		pid = (point *)(stream_def->dbhead->tlm_head->point_array[number_of_mnems_in_db]);
		if (pid && pid->tlm_point_index == pseudo_index) {
			state_table_number = get_telemetry_context(pid);
			if (++printSTATE==1) log_event("(process_pseudo_pkt)  index=%d	state_num=%d",pseudo_index,state_table_number);
		}		
		number_of_mnems_in_db++;
		db_points = db_points->next;

	}
	
END get context state ***/

/* conversion context always zero for pseudo */
state_table_number = 0;

/* set conversion context into buffer */
update_buffer.data_body[update_buffer.number_of_mnemonics].status_flags =
	(state_table_number & UBG_CONTEXT_MASK);

	/* DB Index for variable */
	update_buffer.data_body[i].mnemonic_index=pseudo_index;
		
	/* Set the bits to represent the data length */
	update_buffer.data_body[i].status_flags |= byte_to_mask_translation[num_bytes];

	/* Set the IEEE float flag...pseudo will be sent as 8 byte Ieee float ..Lata */
	update_buffer.data_body[i].status_flags |= UBG_IEEE_FLOAT;
		
	/* set data questionable flag if bad quality */
	if (pseudo_status & status_mask [QUESTIONABLE_VALUE]) {
		update_buffer.data_body[i].status_flags |= UBG_QUESTIONABLE_QUALITY;
		/* save info if first bas pseudo */
		if (++bad_pseudo_count == 1) {
			bad_pseudo_index  = pseudo_index;
			bad_pseudo_status = pseudo_status;
		}
		/*log_event("PSEUDO BAD QUALITY found for index = %d  pseudo_status=x%8.8X  status_flags=x%8.8X",
			pseudo_index,pseudo_status,update_buffer.data_body[i].status_flags);*/
	}
		
	/* This is a pseudo point */
	update_buffer.tlm_or_pseudo[i]=1;

	/*DEBUG_START
	if (datatype == DATATYPE_GVA && gva_count==1) {	
		log_event("process_packet(GVA): tag=%5.5d   offset=%d",update_buffer.data_body[i].mnemonic_index,offset_ms);
	}
	
	/* 
	if (datatype == DATATYPE_PSEUDO && debug_pseudo) {
		log_event("process_packet(PSU): tag=%5.5d   offset=%d",update_buffer.data_body[i].mnemonic_index,offset_ms);
	}
	DEBUG_END*/
			
	/* If variable is defined and debug is on */
	if (getenv("INCLUDE_DEBUG_DECOM_INFO") && debug_on) {
			
		strcpy(hex_buffer,getenv("INCLUDE_DEBUG_DECOM_INFO"));
		if ((hex_buffer[0]=='Y' || hex_buffer[0]=='G') && debug_on) {
				/* If we want to include all the decom information about
					the current point, be forewarned that this option yields
					megabytes and megabytes of debug information! */

			for (i=0; i<num_bytes; i++) {
				sprintf(short_buff,"%8.8X",
					(short)(update_buffer.data_body[i].raw_value[i]));
				if (i==0) strcpy(hex_buffer,&short_buff[6]);
				else strcat(hex_buffer,&short_buff[6]);
			}
			/*
			sprintf(message1,", msOffset: %i",offset_ms);
			sprintf(message,
				"[UBG%2s] %s %s(%i), Size: %i%c bytes, Rw: %lf, HX: %s%s",
				process_number,"Pseudo pt",
				"NAME_NOT_LOOKED_UP",pseudo_index,
				mask_to_byte_translation[UBG_GET_DATA_MASK(
					update_buffer.data_body[i].status_flags)],
				int_or_float_letter[int_or_float],raw_value,hex_buffer,
				message1); 
				log_debug(message); */
			/* Which point did we receive? */
		}
	}
}

/* output event if bad quality found */
if (bad_pseudo_count > 0) {
	log_event("%d pseudo points have BAD QUALITY  (index=%d  status=x%8.8X)",
	bad_pseudo_count,bad_pseudo_index,bad_pseudo_status);
}

/* Set the status bits in the data header */
UBG_set_data_header_status();

/* Make sure this is set to the correct data type */
update_buffer.data_header.frame_type=UBG_PSEUDO_CODE;

/* According to Suman on 5/17/00, GaIngest ignores this field for 
		pseudos.  In GOES I-M, this field is zeroed. */
update_buffer.data_header.minor_frame_number = 0;

update_buffer.data_header.data_size = SIZE_DATA_HEADER_BASE+FORCED_MAX_DB_NAME_LEN;

/* Calculate the size of the buffer that will be sent. */
for (i=0; i<update_buffer.number_of_mnemonics; i++)
	update_buffer.data_header.data_size += SIZE_DATA_BODY_BASE+
		mask_to_byte_translation[UBG_GET_DATA_MASK(
			update_buffer.data_body[i].status_flags)];
			
/*DEBUG_START write buffer to dump file 			
	if (datatype == DATATYPE_GVA && debug_gva) {	
		dump_fd = NULL;
		dump_fd = open("/tmp/dump.gva", O_CREAT|O_TRUNC|O_RDWR, 0666);
		dump_offset = lseek(dump_fd, (long) 0, SEEK_SET);
		dump_bytes = write(dump_fd, &update_buffer, bytes); 
		log_event("process_packet(GVA): bufsize=%d  dumpsize=%d   buffer=(%40.40X)",bytes,dump_bytes,update_buffer);
	}
	
	if (datatype == DATATYPE_PSEUDO && debug_pseudo) {
		dump_fd = NULL;
		dump_fd = open("/tmp/dump.psu", O_CREAT|O_TRUNC|O_RDWR, 0666);
		dump_offset = lseek(dump_fd, (long) 0, SEEK_SET);
		dump_bytes = write(dump_fd, &update_buffer, bytes); 
		log_event("process_packet(PSU): bufsize=%d  dumpsize=%d   buffer = (%40.40X)",bytes,dump_bytes,update_buffer);
	}
DEBUG_END */			
				
/* Allocate space for the update buffer.  The total space allocated 
			will be the size of the message header plus the size of the 
			data header plus the size of each mnemonics in the buffer.  
			The extra 256 bytes allocated allow for strings to fill in
			the spots of char pointers. */
buffer_to_send = (char *)calloc(sizeof(update_buffer.message_header)+
	update_buffer.data_header.data_size+256,sizeof(char));

/* Create the actual buffer to send. */
UBG_create_buffer_to_send(&full_buffer_length,buffer_to_send,1);

/*DEBUG_START
if (datatype == DATATYPE_PSEUDO)
	log_event("process_packet(PSU): msgid=%d  points=%d  bufsize=%d",
	update_buffer.message_header.message_number,update_buffer.number_of_mnemonics,full_buffer_length);
else if (datatype == DATATYPE_GVA)
	log_event("process_packet(GVA): msgid=%d  points=%d  bufsize=%d",
	update_buffer.message_header.message_number,update_buffer.number_of_mnemonics,full_buffer_length);
*DEBUG_END */

/* Send the buffer */
UBG_send_buffer(datatype,full_buffer_length,buffer_to_send); 

/* Increment message number for next message */
update_buffer.message_header.message_number++;

/* update stats in shared memory */
gaimstat->buffers_pseudo++;

/* Free up memory */
free(buffer_to_send);


}

