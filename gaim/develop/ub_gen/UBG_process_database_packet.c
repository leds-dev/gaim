/*  
@(#)  File Name: UBG_process_database_packet.c  Release 1.1  Date: 00/11/10, 13:26:00
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_process_database_packet.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation

********************************************************************************
	Invocation:
        
		UBG_process_database_packet(int bytes,char *packet)
        
	Parameters:

		bytes - I
			The size of the database packet.

		packet - I
			The database packet from PUB.

********************************************************************************
	Functional Description:

		This routine takes the database name from PUB and does everything that 
		is needed to load the new database, including shutting down the usage 
		of the previous database when necessary.
	
*******************************************************************************/

/**

PDL:
	Extract the database name from the packet.
	IF this is the first database packet received or the database name has 
	 changed since the last database packet THEN
		CALL UBG_initialize_epoch
	ENDIF

**/

#include "db.h"
#include "db_util.p"
	/* Epoph database structures and routines */

#include "UBG_structures.h"
	/* Structures, defines, and statics */

int UBG_process_database_packet(int bytes,char *packet) {

	char *packet_ptr;
		/* Where are we in the packet? */

	short dbase_name_len;
		/* length of database name */

	char new_database_name[1024];
		/* Database name */

	int retval=0;


	packet_ptr=packet;
		/* Set the packet index pointer to the packet start -- the 2-byte 
			packet ID was skipped over in the calling function. */

	memcpy(&dbase_name_len,packet_ptr,2);
	packet_ptr+=2;
		/* Get length of database name */

	memcpy(new_database_name,packet_ptr,dbase_name_len);
		/* Get the database name */

	if (strcmp(database_name,new_database_name)) 
		retval=UBG_initialize_epoch(new_database_name);
			/* Database name has changed. */

	return retval;
		/* Return error code */

}
