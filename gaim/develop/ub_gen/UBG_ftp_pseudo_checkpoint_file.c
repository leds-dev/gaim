/*
@(#)  File Name: UBG_ftp_pseudo_checkpoint_file.c  Release 1.11  Date: 07/04/16, 08:41:44
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_ftp_pseudo_checkpoint_file.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles

         VER.        DATE        BY              COMMENT
         V1.0        05/00       S. Scoles       Initial creation
	 8.1         mar03-2004  K. Dolinh	 	 Add log_event, log_debug
	 8.4         mar18-2004  K. Dolinh        New name for pseudo checkpoint file
	 8.5	     mar22-2004                   process env var GAIM_B7 
	 9.2        jan12-2004                   remove gaim_binary_search and
	                                           use point_id to lookup help_index 
	 10A        sep02-2005                   remove reference to B7
	 10A3		sep28-2005					 process context switching	
	 10A3.1		oct10-2005					 	 										   
     10B        Jan12-2006                   Build 10B
	 11         Feb09-2007					 Build 11
	 			Mar22-2007					 Use backgnd cp script instead of ftp
				Mar27-2007					 removed "points sent" event 
		  		Apr11-2007					 fixed log_tlmwatch msg
											 truncate *.def filename in event msg
											 
********************************************************************************
	Invocation:

		UBG_ftp_pseudo_checkpoint_file()

	Parameters:

		NONE

********************************************************************************
	Functional Description:

		This routine ftp's the checkpoint file from the correct gtacs.

*******************************************************************************/

/**

PDL:
	Create the ftp script.
	Spawn the ftp command as a child of this routine.
	Open the ftp output file.
	Search the file for possible communication errors such as "No connection",
	 "Please login with USER and PASS", and "No such file".
	IF any ftp errors THEN
		Write a message to the debug log.
	ENDIF
	IF the ftp did not fail THEN
		Read the pseudotelemetry checkpoint file.
		DOFOR each point in the checkpoint file
			CALL UBG_make_pseudo_buffer_from_cp
		ENDDO
		CALL UBG_process_pseudo_packet
	ENDIF

	RETURN an error status
**/
#include <string.h>
#include "tlm.p"

/* Globals and structures */
#include "UBG_structures.h"
#include "../include/util_log.h"

extern int debug_pseudo;
extern int debug_gva;
extern int pseudo_ftp_rate;

/* global vars for shared mem */
extern GaimStat *gaimstat;

/* 
 * data used by tlm watch 
 */
extern char *tlmwatch_buf;
extern int	 tlmwatch_max;

char cmd[256] = "";

static  int  pseudo_first_time = 1;
int  pseudo_count = 0;
static  int  pseudo_count_max = 100;
static  int  total_pseudo_points = 0;
static  int  total_pseudo_buffers = 0;

int UBG_ftp_pseudo_checkpoint_file(void) 

{
Pseudo_struct pseudo;
FILE *pseudoFILE;

/* point struct from database */
point  	  *pid;
tlm_point *tlm_pid;
tlm_value * cv;

char *found1,*found2;
int  shared_value;

/* Counter */
int i;

/* Time used for timestamping of pseudos */
int current_time;

int pseudo_index;
int status;
double raw_value;

/* start background job to copy checkpoint file from gtacs to gaim */
if (pseudo_first_time) {
	UBG_ftp_start_job (DATATYPE_PSEUDO);
	pseudo_first_time = 0;
	return 0;
}

/* open checkpoint file */
/* exit if file is not yet copied to gaim */
pseudoFILE = fopen(gaim_filename_pseudo,"rb");
if (!pseudoFILE) return 0;

/* output statistics */
pseudo_count++;
if (pseudo_count > pseudo_count_max) {
	/*log_event("PSEUDO processing: files=%d  buffers=%d  (%s)",pseudo_count,total_pseudo_buffers,&gaim_filename_pseudo[30]);*/
	pseudo_count = 0;
	/*total_pseudo_buffers = 0;*/
}


/* read checkpoint file and make update buffer */

/* Init counter */
i = 0;

/* Get the current time */
current_time = time(0);

/* Init pseudo output buffer */
pseudo_cp_length=0;
	
/* While not end of file */
	while (!feof(pseudoFILE)) {

		fread(&pseudo,sizeof(pseudo),1,pseudoFILE);
		if (!feof(pseudoFILE)) {
			/* look up point name in db to get help index */
			if (strlen(pseudo.mnemonic) > 0) {
				pid = (point *) point_id (stream_def->dbhead,pseudo.mnemonic);
				if (pid) {
					/* look up pseudo point structure */
					tlm_pid = (tlm_point *)pid->tlm_point;

					/* save pseudo status in local memory */
					cv = (tlm_value *) pid->value;
					cv->update_status = pseudo.flags;
					cv->raw_value = pseudo.value;	
										
					/* save pseudo status in shared memory */
					if (pid->shared_value) {
						cv = (tlm_value *) pid->shared_value;
						cv->update_status = pseudo.flags;
						cv->raw_value = pseudo.value;
						/* cv->time = ? */
						shared_value = 1;
					} else {
						shared_value = 0;
					}

					/* save pseudo value in shared memory 
					status = set_raw_value(pseudo.value,pid);*/
										
					/* get pseudo help index */
					pseudo_index = tlm_pid->help_index;	
					
					/* add pseudo to buffer */		
					UBG_make_pseudo_buffer_from_cp(DATATYPE_PSEUDO,&pseudo,current_time,pseudo_index);
					total_pseudo_points++;
					i++;
					
          if (tlmwatch_max) {
					/* output point info to tlmwatch file if pointname is in tlmwatch.cfg */
					found1 = (char *)strstr(tlmwatch_buf,pseudo.mnemonic);
					found2 = (char *)strstr(tlmwatch_buf,"*ALL*");
					if ((found1 && strlen(pseudo.mnemonic) > 8) || found2) {
						raw_value = 123.456;  /* dummy value */
						get_raw_value (pid, &raw_value);
				   	    log_tlmwatch("PSEUDO=%32s  index=%5d  value=%g  flags=x%8.8X)  shared=%d  get_raw_value=%lf",
				   			pseudo.mnemonic,pseudo_index,pseudo.value,pseudo.flags,shared_value,raw_value,&raw_value);
					}		
          }

				}
			} 
		}

	} /* end dowhile */

/* close file */
fclose(pseudoFILE);

/* remove checkpoint file so that a new one can be fetched by script */
sprintf(cmd,"rm %s",gaim_filename_pseudo);
system(cmd);

/* update stats in shared memory */
gaimstat->files_pseudo++;

/* Send pseudo packet */
memcpy(pseudo_cp_buffer,&pseudo_cp_counter,2);
if (i > 0) {
	/*log_event("processed %d PSU points  total_psu_buffers to date = %d",i,total_pseudo_buffers);*/
	UBG_process_pseudo_packet(DATATYPE_PSEUDO,pseudo_cp_length,pseudo_cp_buffer);
	total_pseudo_buffers++;
	if (total_pseudo_buffers >= 1 && total_pseudo_buffers <= 10) { 
		found1 = (char *)strstr(gaim_filename_pseudo,"ftp/");
		if (found1) found1 = found1+4;
		else found1 = gaim_filename_pseudo;
		log_event("PSEUDO processing totals: (%s) %d buffers sent  %d points in current buffer",found1,total_pseudo_buffers,i);
	}
}

return 0;
} /* end UBG_ftp_pseudo_checkpoint */

