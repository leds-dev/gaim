/*  
@(#)  File Name: UBG_process_gtacs_packet.c  Release 1.6  Date: 07/03/25, 09:12:30
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_process_gtacs_packet.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
	 8.4         mar18-2004  K. Dolinh	 Add log_event, log_debug
	 
********************************************************************************
	Invocation:
        
		UBG_process_gtacs_packet(int bytes,char *packet)
        
	Parameters:

		bytes - I
			The size of the gtacs packet.

		packet - I
			The gtacs packet from PUB.

********************************************************************************
	Functional Description:

		This routine takes the gtacs name from PUB and does everything that 
		is needed to load the new gtacs, including shutting down the usage 
		of the previous gtacs when necessary.
	
*******************************************************************************/

/**

PDL:
	Extract the GTACS name from the packet.
	Find the GTACS name in the known GTACS list and extract its' code number to 
	 be used in the Update Buffer.

**/

#include "UBG_structures.h"
	/* Structures, defines, and statics */

void dump_generic_packet(char *pkt);
	/* Prototype */

void UBG_process_gtacs_packet(int bytes,char *packet) {

char *packet_ptr;
	/* Where are we in the packet? */

short gtacs_name_len;
	/* length of gtacs name */

char message[256];
	/* Debug Message */

int i;
	/* Counter */

/* Set the packet index pointer to the packet start -- the 2-byte 
   packet ID was skipped over in the calling function. */
packet_ptr = packet;

/* Get length of gtacs name */
memcpy(&gtacs_name_len,packet_ptr,2);
packet_ptr+=2;
	
/* Get the gtacs name */
memcpy(gtacs_name,packet_ptr,gtacs_name_len);
	
/* Search the valid GTACS list and pick the associated code
number to include in the update buffer */
for (i=0; i<UBG_LAST_GTACS; i++) {

	if (!strcmp(gtacs_name,gtacs_list[i])) 
		update_buffer.data_header.gtacs_name= gtacs_array[i];
}
	
if (update_buffer.data_header.gtacs_name == -1)
	log_event_err("GTACS name=(%s) not in known list",gtacs_name);
else
	log_event("GTACS name from PUB/SDR = %s",gtacs_name);

}

void dump_generic_packet(char *pkt) {

int i;

for (i=0; i<16; i+=2) 
	printf("%4.4X ",*((unsigned short *)(pkt+i)));
printf("\n");
}

