/*  
@(#)  File Name: UBG_process_eoh_eoi_packet.c  Release 1.8  Date: 07/03/25, 09:12:28
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_process_eoh_eoi_packet.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
		 8.4         mar18-2004  K. Dolinh	 	 Add log_event, log_debug
	 	 9.A2        mar02-2005                  output EOH/EOI event msg
	 	 9AP	     mar16-2005		 	 		 Build 9A Prime
		 11										 change call to UBG_send_data

********************************************************************************
	Invocation:
        
		UBG_process_eoh_eoi_packet()
        
	Parameters:

		NONE

********************************************************************************
	Functional Description:

		This routine sends an empty update buffer to UBS with the EOH/EOI
		flag set.
	
*******************************************************************************/

/**

PDL:
	Set the Update Buffer flags to represent the end of interval packet.
	CALL UBG_create_buffer_to_send
	CALL UBG_send_buffer.

**/

#include "UBG_structures.h"
	/* Structures, defines, and statics */

void UBG_process_eoh_eoi_packet(void) {

	char *buffer_to_send;
		/* The Update Buffer */

	int full_buffer_length=0;
		/* The length of the full update buffer */

	char message[256];
		/* Error Message */
  short short_data = 0;


/* Start with a fresh buffer, and stay that way. */
update_buffer.number_of_mnemonics=0;

/* Set end-of-interval/end-of-hour flag in status field */	
short_data = update_buffer.data_header.status;
memcpy((char*)&update_buffer.data_header.status, (char*)(&short_data), 2);
update_buffer.data_header.status|=UBG_EOH_EOI;
	
/* added by lata to initialize frame_type */
update_buffer.data_header.frame_type = UBG_NO_CODE;

log_event("Send EOH/EOI Buffer to GAS.  BufferStatus=x%4.4X",update_buffer.data_header.status);

/* Set the size to that of just the data header.  
   There is no data body with this update buffer. */
update_buffer.data_header.data_size = SIZE_DATA_HEADER_BASE+FORCED_MAX_DB_NAME_LEN;
			
/* Allocate space for the update buffer.  The total space 
   allocated will be the size of the message header plus the 
   size of the data header.  The extra 256 bytes allocated 
   allow for strings to fill in the spots of char pointers. */
buffer_to_send = (char *)calloc(sizeof(update_buffer.message_header)+
	update_buffer.data_header.data_size+256,sizeof(char));

/* Create the actual buffer to send. */
UBG_create_buffer_to_send(&full_buffer_length,buffer_to_send,2);

/* Send the buffer */	
UBG_send_buffer(DATATYPE_EOH,full_buffer_length,buffer_to_send);

/* Increment message number for next message */
update_buffer.message_header.message_number++;

/* Free memory */
free(buffer_to_send);


}
