/*
@(#)  File Name: UBG_main.c  Release 3.3  Date: 07/08/23, 15:21:03
*/
/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_main.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles

         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles   Initial creation
         8.1         mar02-2004  K. Dolinh	 Add log_event, log_debug
         8.1         mar03-2004  K. Dolinh	 checking for termination
         8.4         mar18-2004  K. Dolinh   ftp pseudo for nonprime stream
         8.5 	     mar22-2004              ...
         8.6	     apr02-2004              ...
         8.7	     apr15-2004 
	              ...
         8.8         may06-2004              make sxi mismatch msg debug msg
         8.8.1E      aug13-2004              add watchtlm option
	 
         8B          sep17-2004              GTACS Build 8B
         8.B.1       sep22-2004              - UBG_main.c: add tlmwatch_max
                                             - UBG_structures.h
                                             - UBG_process_tlm_frame  
                                             - UBG_newframe_callback.c
         8.B.2       sep27-2004              fix archive of SXI frame
                                             - UBG_decom_callback.c
                                             - UBG_init_globals.c
         8.B.3       oct05,2004              check stream_name when spawned by PUB
	                                     	 - add env var GAIM_REVERSE_TLM
         9.1         jan06-2005              build 9.1 
		 9.2         jan12-2005              changes to 
		                                        UBG_ftp_pseudo_checkpoint_file.c
												UBG_decom_callback.c
												UBG_process_tlm_frame.c
		 9A			 feb23-2005				 build 9A 	
		 9A.1        feb25-2005
		 9A.2        mar02-2005    
		             mar09-2005         output CRC err msg only if DEBUG_CRC set
		 9AP	     mar16-2005		Build 9A Prime
		 9AP	     apr04-2005		Build 9A Prime.2
		 9AP	     apr25-2005		Build 9A Prime.3
		 9B          may01-2005     Build 9B
		 							- Add log_watchtlm(..)
									- Add frame_bad_timetag flag
		 9C          jul20-2005     - Add new array gsa_tag						
		 10A         aug26-2005     Build 10A	
		 							- Add NORMAL_DWELL_MODE check
		 10A.2       sep09-2005     Build 10A.2	
		 10A.3       sep15-2005     - debug get_telemetry_context
		 							- return error if gsa file not found
					 oct10-2005		- output pseudos to tlmwatch file
					 				- save pseudo value to shared memory
		 10A.3.1	 oct21-2005		- add env var GAIM_UPDATE_BUFFER_MAX				
		 10B    	 jan12-2006		Build 10B                    
		 10C    	 jul10-2006		Build 10C		 
		 11    	 	 Aug18-2006		Build 11 has changes to
		 							   UBG_main
									   UBG_send_buffer
									   UBG_process_tlm_frame
									   UBG_new_frame_cb 
									   UBG_set_data_header_status
									   UBG_initialize_epoch	
									- Don't output event msg when poll_point error 
									- Add routine UBG_gva_pseudo_checkpoint_file
									- fix status check on log_tlmwatch
		 12    	 	 May31-2007		Build 12 has changes to
		 							- Add 2 new SXI frame types
		 12A    	 Aug22-2007		Build 12A has changes to
		 							- remove STOP file before starting copy script
		 		 		 		 		 		 		 		 		 		 							
********************************************************************************
  	Invocation:

		Update_Buffer_Generator [-a {archive_machine_ip_address}]
			[-b {database name}] [-c {connect_to_stream_name}]
			[-d [{Debug File Name}]] [-f {frame_rate}] [-g {GTACS_name}]
			[-i {spacecraft_id_number}] [-m {merge_type}] [-r {sdr/pub/tst}]
			[-s {scid}] [-t {stream name}] [-v {process_number}]
			[-z {stream_type}]

	Parameters:

		-a {archive_machine_ip_address}
			Which archiev machine made the playback request?

		-b {database name}
			The name of the database to use for decomming the frames.

		-c {connect_to_stream_name}
			This option was included because the EPOCH load_db function
			requires that whatever process is loading the database be connected
			to a stream.  It does not matter what stream, just any stream.  The
			reason for this is unknown to this programmer, but is a necessary
			evil when trying to decommutate archived data.

		-d [{Debug File Name}]
			Name of the debug file to be created to keep track of
			Update_Buffer_Generator Error Messages.  This is an optional
			parameter.  Default: No debug file.  If {Debug File Name} is not
			specified with the -d parameter, all debug messages will be written
			to STDOUT.

		-f {frame_rate}
			For PCM Data, this should either be 0 or 1, representing 1Kbps, or
			4Kbps.
			For AVS/SXI, this should be the number of seconds between frames.

		-g {GTACS_name}
			What is the name of the gtacs this data is coming from?

		-i {spacecraft_id_number}
			The value from the SPACECRAFT_ID global.  This should be passed
			from PUB or SDR.  Id UBG is run from the cmmand line, this parameter
			is ignored and ther SCID number is taken from the S/C globals,
			directly.

		-m {merge type}
			Set to A for Auto-Merge, M for Manual Merge.  This is only useful
			if run from PUB.

		-p {prime/nonprime stream}
			Set to P for Prime Stream, N for Non-Prime stream.  This is only
			useful if run from SDR.

		-r {sdr/pub/tst} Spawned by (run from) Stream_Data_Receiver (sdr),
			(run from) Playback_Update_Buffer (pub), or run from command line
			(tst) -- test mode.  Command Line is the default if -r is not
			specified.

		-s {scid}
			Spacecraft ID for which to create the DCR files.  If this is not
			given, it is taken from the SC_NAME variable.

		-t {stream name}
			Name of the stream you wish to connect to, to create the DCR files.
			If not given, this is taken from the STREAMID environment variable.

		-v {process_number}
			The number (1 to 8 from SDR, 9-24 from PUB) to be appended to the
			base service name, identifying which service is to be used to
			communicate between processes.

		-z {stream_type}
			S For simulated data stream, R for real spacecraft data stream.

		Environment Variable: INCLUDE_DEBUG_DECOM_INFO
			Set this environment variable to Y to include information about
			every single point decommutated by UBG to the debug file.  This
			will produce megabytes and megabytes of information to be written
			to the debug file.


	Example Usage:


	Exit Conditions:

		0: Successful Completion
		1: Invalid Command Line Parameters
		2: Unknown Error
		3: Error finding STREAM_DB
		4: Incomplete Argument List
		5: Error polling STREAM_DB
		6: Unable to allocate space for Signal Action Structure
		7: Unable to set up SIGINT handler
		8: DB Load Error
		9: Stream Connection Error
		10: Frame Service Request Error
		11: Error initializing telemetry utility
		12: No stream, data type, or scid specified
		13: Cannot find PID of TELEMETRY_RATE
		14: Cannot find PID of FRAME_TYPE
		15: Cannot determine Stream Name
		16: Invalid -r parameter.
		17: Unable to connect to Playback_Update_Buffer.
		18: Unable to connect to Stream_Data_Receiver.
		19: Spacecraft Name Undefined.
		20: No database name given.
		21: Invalid process number.
		22: Cannot find DB file in REPORTS path.
		23: Error polling SC_NAME
		24: Cannot determine S/C Name
		25: No database name given

******************************************************************************
	Functional Description:

		The Update Buffer Generator process takes raw tlm frames from
      Stream_Data_Receiver and Playback_Update_Buffer, decommutates them,
      converts them to an Update Buffer, then forwards the Update Buffers
      to Update_Buffer_Sender.

      NOTE: Update_Buffer_Generator needs the EPOCH install_db routine
            available in its search PATH and it also needs write access
            to the directory in which install_db is placed. The load_db
            call in UBG_initialize_epoch needs install_db to build the
            stream database in shared memory.

*****************************************************************************/

/**

PDL:
	Process Command Line Parameters.
	Act upon the command line parameters.
	Add a list of initialization parameters to the debug log.
	Set up signal handlers for SIGINT and SIGTERM.
	Connect to the ground stream.
	CALL UBG_initialize_epoch
	Initialize the fixed parameters in the Update Buffer.
	CALL UBG_tcpip_setup

	DOWHILE there is no program termination request and we are waiting for a
	 response.
		IF this Update_Buffer_Generator is processing real-time data THEN
			IF it is time to ftp the pseudotelemetry checkpoint file from the
			 appropriate gtacs THEN
				IF at least one telemetry frame has been seen since this
				 process started THEN
					CALL UBG_ftp_pseudo_checkpooint_file
				ENDIF
				IF data has been sent to the Update_Buffer_Sender THEN
					Set the waiting-for-a-response flag.
				ENDIF
				IF the ftp has failed THEN
					Reset the waiting-for-a-response.
				ENDIF
			ENDIF
		ENDIF
		CALL UBG_wait_forever.
	ENDDO

	IF this is the prime-prime real-time data stream and the stream has not
	 failed THEN
		Set the EPOCH global GAIM_FORMAT_RT_VALUE to 'Not Active'.
	ENDIF

	Send shutdown messages to all active sockets.
	Shutdown and close all active sockets.
	Terminate telemetry and global variable utilities.
	Disconnect from any stream we are currently connected too.

**/

/* Standard C Libraries */
#include <signal.h>
#include <stdio.h>
#include <sys/socket.h>
#include <time.h>
#include <stdarg.h>             /* Variable-length argument lists. */
	
/* Modified EPOCH file */
#include "tlm.p"
#include "gv_util.p"
#define _STRM_ERR
#include "GAIM_strmerr.h"
	
/* Update_Buffer_Generator structures */
#define UBG_MAIN
#include "UBG_structures.h"
#include "UBG_prototypes.h"

/* More Prototypes */
static void signal_handler(int sig);
	
#define SW_VERSION "Build 12A Aug-23-2007"
#define SW_ID      "UBG"


#include "../include/util_log.h"
void log_tlmwatch (char *fmt,...);

/* global variables for pseudo and gva ftp */

int debug_pseudo = 0;
int debug_gva = 0;

int ftp_mode = FTP_PSEUDO_ONLY;
int ftp_next = FTP_PSEUDO_ONLY;
char *ftp_mode_text;
int pseudo_ftp_rate = PSEUDOTLM_FTP_RATE;

/* global variables */
int debug_crc = 0;
int frame_bad_timetag = 0;
int update_buffer_max = 0;
char cmd[256];

/* vars used for tlmwatch */ 
char *tlmwatch_buf;
long tlmwatch_size = 0;
int	tlmwatch_max = 0;
static FILE *tlmwatch_ptr = NULL;
static char tlmwatch_filename[1024];
static int  tlmwatch_open_err = 0;

/* global vars for tlm format */
int pcm1_format;
int pcm2_format;
int normal_dwell_mode;

/* global vars for tlm rate */
int	pcm1_rate = 0;
int	pcm2_rate = 0;

/* global vars for shared mem */
GaimStat *gaimstat;
			
int main(int num_params, char *params[]) {

int i;
short short_data = 0;
	
struct sigaction *signal_action;
int status;
	/* Declare error and kill variables */

char current_param[256];
	/* Command line inputs */

int argument_type=-1;
	/* -1=No '-' argument seen, yet, or finished processing previous '-'
		argument. 0=Stream Name, 1=Debug File Name.  2=File Length in
		Minutes.  3=File Length in Hours, 4=SCID, 5=Data Type */

int input_error=0;
	/* -1=No scid, stream, and/or data type, 0=all is okay */

char message[256];
	/* Error Message String*/

char nodename[32];
int nodenamelen=32;
	/* Used for storing the node name. */

/* Text describing how UBG was executed. */
char run_modes[3][36]={
	"run from the UNIX command line.",
	"spawned by Playback_Update_Buffer",
	"spawned by Stream_Data_Receiver"};
		
/* Has Update_Buffer_Sender sent us a shutdown message? */
int ubs_shutdown=0;
	
/* Has Stream_Data_receiver/Playback_Update_Buffer sent us a shutdown message? */
int sdr_pub_shutdown=0;

/* Error code */
int retval=0;
	
/* Used for timing ftp'ing of pseudotlm checkpoint files */
time_t curr_time,last_time;
	
/* process_number as a long */
int my_channel_number;
	
/* Telemetry frame structure */
tlm_frame *to_be_decommed;
	
/* -1=ftp failure, 0=No ftp problems */
int failure;
	

/* Get the current host name */
gethostname(nodename,nodenamelen);

/* Init pointers */
gsa_index_list=0;
gsa_index_mode=0;	

/* Force the allocated space to become a 0 length string for char variables */
connect_to_stream[0]=0;
process_number[0]=0;
stream_name[0]=0;
debug_filename[0]=0;
scid[0]=0;
database_name[0]=0;
gtacs_name[0]=0;
merge_type[0]=0;
prime_nonprime_stream[0]=0;
stream_type[0]=0;
mnemonic_list=0;


/* ************************************
* Proccess Command Line Parameters *
************************************ */

for (i=1; i<num_params; i++) {
		/* Parameter Processing Loop */

	strcpy(current_param,*++params);
		/* Copy the next parameter to a temporary variable */


	if (strlen(current_param)!=0) {
			/* If there is another parameter */

		if (current_param[0]!='-') {
				/* If this is a not a new parameter flag */

			switch (argument_type) {

				case 0:		/* Stream Name */
					strcpy(stream_name,current_param);
					break;

				case 1:		/* Debug File Name */
					strcpy(debug_filename,current_param);
					break;

				case 2:		/* Run mode */
					if (current_param[0]=='t') run_mode=0;       /* tst = cmdline  */
					else if (current_param[0]=='p') run_mode=1;  /* pub = playback */
					else if (current_param[0]=='s') run_mode=2;  /* sdr = realtime */
					else {
						printf("Invalid -r argument.  Program exitting.\n");
						exit(15);
					}
					break;

				case 3: 	/* Database Name */
					strcpy(database_name,current_param);
					break;

				case 4:		/* SCID */
					strcpy(scid,current_param);
					break;

				case 5:
					current_telemetry_rate=atoi(current_param);
					break;

				case 6:
					strcpy(process_number,current_param);
					if (atoi(process_number)<START_SDR_CHANNEL ||
							atoi(process_number)>MAX_SDR_PROCESSES+
							MAX_PUB_PROCESSES) {
						printf("Invalid Process Number = %s UBG exiting\n",
							process_number);
						exit(21);
					} else {
						strcat(ubs_service_name,process_number);
						strcat(pub_service_name,process_number);
						strcat(sdr_service_name,process_number);
					}
					process_number_value=atoi(process_number);
					break;

				case 7:
					spacecraft_id_value=atoi(current_param);
					if (spacecraft_id_value<22)
						strcpy(scid,scnames[spacecraft_id_value]);
					else strcpy(scid,"-i param .gt. 21!");
					break;

				case 8:		/* Merge Type */
					strcpy(merge_type,current_param);
					break;

				case 9:		/* Stream Type */
					strcpy(stream_type,current_param);
					break;

				case 10:		/* Prime or Non-Prime */
					strcpy(prime_nonprime_stream,current_param);
					if (prime_nonprime_stream[0]<'A' ||
						prime_nonprime_stream[0]>'Z')
							prime_nonprime_stream[0]=
								prime_nonprime_stream[0]-('a'-'A');
					break;

				case 11:		/* GTACS name */
					strcpy(gtacs_name,current_param);
					break;

				case 12: 	/* Archive Machine IP Address */
					strcpy(archive_ip_address,current_param);
					break;

				case 13:	/* "Connect To" Stream Name */
					strcpy(connect_to_stream,current_param);
					break;

				default:	/* Unknown Error */
					printf("Unknown Error in ARGUMENT_TYPE\n");
					exit(2);
					break;
			}

			argument_type=-1;
				/* Reset the Argument Type */

		} else {

			switch (current_param[1]) {

				case 'A':	/* Flag the next parameter as an archive */
				case 'a':	/*	machine IP address */
					argument_type=12;
					break;

				case 'B':	/* Flag the next parameter as a database name */
				case 'b':
					argument_type=3;
					break;

				case 'C':	/* Flag the next parameter as a "connect to" */
				case 'c':	/*	stream name.                             */
					argument_type=13;
					break;

				case 'D':	/* Flag the next param as a debug file name */
				case 'd':
					argument_type=1;
					debug_on=-1;
					break;

				case 'F':	/* Flag the next parameter as a frame rate */
				case 'f':
					argument_type=5;
					break;

				case 'G':	/* Flag the next parameter as a frame rate */
				case 'g':
					argument_type=11;
					break;

				case 'I':	/* Flag the next parameter as a scid number */
				case 'i':
					argument_type=7;
					break;

				case 'M':	/* Flag the next parameter as merge type */
				case 'm':
					argument_type=8;
					break;

				case 'P':	/* Flag the next parameter as a Run Mode */
				case 'p':
					argument_type=10;
					break;

				case 'R':	/* Flag the next parameter as a Run Mode */
				case 'r':
					argument_type=2;
					break;

				case 'S':	/* Flag the next parameter as a SCID */
				case 's':
					argument_type=4;
					break;

				case 'T':	/* Flag the next parameter as a stream name */
				case 't':
					argument_type=0;
					break;

				case 'V':	/* Flag the next parameter as process number */
				case 'v':
					argument_type=6;
					break;

				case 'Z':	/* Flag the next parameter as stream type */
				case 'z':
					argument_type=9;
					break;

				default:	/* User Error */
					printf("Unknown Parameter: %s\n",current_param);
					exit(1);
					break;

			}
		}
	}
}

/* ****************************************
* Act upon the command line parameters *
**************************************** */

if (strlen(debug_filename)==0 && !debug_on) {
		/* Was debug requested? */

	if (getenv("DEBUG_FILE")) {
		debug_on=-1;

		if (strlen(getenv("DEBUG_FILE"))>0)
			strcpy(debug_filename,getenv("DEBUG_FILE"));
				/* Get the debug file name from the environment */
	}
}

/* get env variable */
gaim_home = (char *) getenv("GAIM_HOME");
if (!gaim_home) {
	/* exit if env var not defined */
	log_err("*** env variable GAIM_HOME is not defined");
	log_err("*** UBG terminating");
	exit(1);
	}

if (getenv("GAIM_TLMWATCH_MAX"))
	tlmwatch_max = atoi(getenv("GAIM_TLMWATCH_MAX"));

if (getenv("GAIM_PSEUDO_FTP_RATE"))
	pseudo_ftp_rate = atoi(getenv("GAIM_PSEUDO_FTP_RATE"));
else
	pseudo_ftp_rate = PSEUDOTLM_FTP_RATE;

if (getenv("GAIM_DEBUG"))       debug = 1;	
if (getenv("GAIM_DEBUG_CRC"))   debug_crc = 1;
if (getenv("GAIM_DEBUG_GVA"))   debug_gva = 1;

if (getenv("GAIM_FRAME_BAD_TIMETAG")) {
 	if (!strcasecmp(getenv("GAIM_FRAME_BAD_TIMETAG"),"WARN")) frame_bad_timetag = 1;
	if (!strcasecmp(getenv("GAIM_FRAME_BAD_TIMETAG"),"DROP")) frame_bad_timetag = 2;
}

if (getenv("GAIM_UPDATE_BUFFER_MAX"))
	update_buffer_max = atoi(getenv("GAIM_UPDATE_BUFFER_MAX"));
else
	update_buffer_max = 0;

ftp_mode_text = (char *)getenv("GAIM_FTP_MODE");
if (!ftp_mode_text) {
	/* default mode */
	ftp_mode = FTP_PSEUDO_ONLY;
	ftp_mode_text = "FTP_PSEUDO_ONLY (default)";
} else {
	if (!strcasecmp(ftp_mode_text,"FTP_GVA_ONLY")) ftp_mode = FTP_GVA_ONLY;
	else if (!strcasecmp(ftp_mode_text,"FTP_PSEUDO_GVA")) ftp_mode = FTP_PSEUDO_GVA; 
}

/* make ftp interval twice as fast if doing both pseudo and gva */
if (ftp_mode == FTP_PSEUDO_GVA) pseudo_ftp_rate = pseudo_ftp_rate/2;
	
/* 
 * read tlmwatch.cfg file 
 */
tlmwatch_ptr = fopen("tlmwatch.cfg","rb");
if (tlmwatch_ptr) {
	/* get size of tlmwatch file */
	fseek(tlmwatch_ptr,01,SEEK_END);
	tlmwatch_size =  ftell(tlmwatch_ptr);
	/* alloc buffer memory */
	if ( tlmwatch_buf = malloc(tlmwatch_size) ) {
		/* read contents of file into buffer */
		fseek(tlmwatch_ptr,01,SEEK_SET);
		fread(tlmwatch_buf,1,tlmwatch_size,tlmwatch_ptr);
		fclose(tlmwatch_ptr);
	}	
}
tlmwatch_ptr = NULL;

/*DEBUG_START */
debug = 0;
debug_pseudo = 0;
/*DEBUG_END */

/* init log message ID */
sprintf(logfile_msgid,"%s.%s",SW_ID,process_number);
/* write startup messages */
log_event("  ");
log_event("Update_Buffer_Generator (%s) was %s",SW_VERSION,run_modes[run_mode]);
log_event("stream_name       = (%s)",stream_name);
log_event("run_mode          = %d",run_mode);
log_event("ftp_mode          = %d (%s)",ftp_mode,ftp_mode_text);
log_event("pseudo_ftp_rate   = %d",pseudo_ftp_rate);
log_event("tlmwatch_max      = %d",tlmwatch_max);
log_event("update_buffer_max = %d",update_buffer_max);
log_event("frame_bad_timetag = %d",frame_bad_timetag);
log_event("debug_gva         = %d",debug_gva);
/*log_event("logfile_name      = %s",logfile_name);
log_event("debug             = %d",debug);
log_event("debug_crc         = %d",debug_crc);*/

if (argument_type!=-1 && argument_type!=1) {
	log_event_err("incomplete argument list. terminate UBG process");
	exit(4);
}

/* *********************************************
   * Write initialization parameters to a file *
   ********************************************* */

if (strlen(scid)!=0)
	log_event("Spacecraft = %s",scid);

if (strlen(stream_name) > 0) {
	if (strlen(prime_nonprime_stream) > 0) {
		if (prime_nonprime_stream[0] == 'P')
			log_event("stream=%s is PRIME",stream_name);
		else
			log_event("stream=%s is NON-PRIME",stream_name);
	}
}

if (strlen(gtacs_name)!=0)
	log_event("GTACS name = %s",gtacs_name);

if (strlen(database_name)!=0)
	log_event("database name = %s",database_name);

if (strlen(archive_ip_address)!=0)
	log_event("archive IP address = %s",archive_ip_address);

if (current_telemetry_rate!=-1)
	log_event("Frame Rate Code = %i",current_telemetry_rate);

/* last startup msg */
log_event("  ");

/* Default to 0, in case TELEMETRY_RATE point not found in the database */
if (current_telemetry_rate==-1) current_telemetry_rate=0;


/* *******************************************************
   * Set up stream activation and cancellation routines. *
   ******************************************************* */

/* Allocate space for the signal_action structure */
signal_action=(struct sigaction *)calloc(1, sizeof(struct sigaction));

if (!signal_action) {
	log_event_err("cannot allocate space for signal_action");
	exit(6);
}

/* Init the signal handler flag */
signal_action->sa_handler=signal_handler;

if (sigaction(SIGINT, signal_action, (struct sigaction *)NULL)) {
	log_event_err("cannot set up SIGINT signal handler");
	exit(7);
}

if (sigaction(SIGTERM, signal_action, (struct sigaction *)NULL)) {
	log_event_err("cannot set up SIGTERM signal handler");
	exit(7);
}


/* *************************
   * Connect to the stream *
   *********************** */
update_buffer.data_header.database_name = 0;
update_buffer.data_body = 0;

if (strlen(connect_to_stream) == 0) strcpy(connect_to_stream,stream_name);

/* run_mode is 0 if spawned by TST cmd line 
 *             1 if spawned by PUB
 *			   2 is spawned by SDR
 */ 
if (run_mode == 0) {
	UBG_init_stream();
	if (UBG_initialize_epoch(database_name)) end_program = -1;
} else {
	/* Attempt to connect to any stream so that the load_db
	   function call for playback and sdr works correctly. */
	status = connect_stream(connect_to_stream,network_timeout,&network_connection);
	if (status != STRM_SUCCESS) {
		/* connect failed... */
		log_event_err("error(%i) from connect_stream(%s): %s",
			status,connect_to_stream,stream_err_messages[0-status]);
		end_program=-1;
	} else {
		log_event("connected to stream %s",connect_to_stream);
	}
}


/* *****************************************
   * Decommuntation support initialization *
   ***************************************** */

	if (strlen(database_name)>0 && run_mode!=0)
		if (UBG_initialize_epoch(database_name)) end_program=-1;


/* ********************************
   * Initialize the Update Buffer *
   ******************************** */
/* Initialize the Update Buffer Message Header. */
short_data = UBG_NON_DWELL_MSG_ID;
memcpy((char*)&update_buffer.message_header.class_id, (char *)(&short_data), sizeof(update_buffer.message_header.class_id));
update_buffer.message_header.message_number = 0;
short_data = strlen(nodename);
memcpy((char*)&update_buffer.message_header.node_name_len, (char *)(&short_data), sizeof(update_buffer.message_header.node_name_len));
update_buffer.message_header.node_name = (char *)calloc(strlen(nodename)+1,sizeof(char));
strcpy(update_buffer.message_header.node_name,nodename);
short_data = strlen(UBG_PROCESS_NAME);
memcpy((char*)&update_buffer.message_header.process_name_len, (char *)(&short_data), sizeof(update_buffer.message_header.process_name_len));
update_buffer.message_header.process_name = (char *)calloc(strlen(UBG_PROCESS_NAME)+1,sizeof(char));
strcpy(update_buffer.message_header.process_name,UBG_PROCESS_NAME);
	
update_buffer.data_header.data_size=sizeof(struct ubg_non_dwell_data_header);

/* If spawned by SDR, need to modify based on C/L parameters */
update_buffer.data_header.gtacs_name=1;
if (run_mode==2)
	for (i=0; i<UBG_LAST_GTACS; i++)
		if (!strcmp(gtacs_name,gtacs_list[i]))
			update_buffer.data_header.gtacs_name=gtacs_array[i];

/* Initialize the Update Buffer Data Header */
if (spacecraft_id_pid)
	update_buffer.data_header.scid = *(int *)spacecraft_id_pid->value;
else 
	update_buffer.data_header.scid=spacecraft_id_value;

if (update_buffer.data_header.scid<13) update_buffer.data_header.scid+=13;

update_buffer.data_header.frame_type=20;
update_buffer.data_header.minor_frame_number=0;
update_buffer.data_header.year=-1;
update_buffer.data_header.day=-1;
update_buffer.data_header.milliseconds=-1;
update_buffer.data_header.status=0;
	

/* initialize GAIM shared memory */
if (run_mode == 1) {
	/* this is merge data, use prime sc shared mem */
	gaimstat = (GaimStat *)gaim_shm_attach(update_buffer.data_header.scid,'P');
	/*strcpy(gaimstat->stream,"MERGE");*/
} else {
	gaimstat = (GaimStat *)gaim_shm_attach(update_buffer.data_header.scid,prime_nonprime_stream[0]);
	strcpy(gaimstat->stream,stream_name);
}

/* ****************************************
   * TCP/IP Communications Initialization *
   **************************************** */
UBG_tcpip_setup();
sleep(1);


/* ****************************************************************
   * Let node_mgr know that this routine has successfully started *
   **************************************************************** */

/* Only needed for test mode.  SDR and PUB will normally spawn UBG */
if (run_mode==0 && !end_program) {
	/* Let the node_mgr know this process has started successfully.
	   This message MUST be written to stdout. */
	printf("Alive.\n");
	fflush(stdout);
}

/* *********************
   * Wait Forever Loop *
   ********************* */

/* do no pseudo buffers until a	frame is seen. */
saw_a_frame = 0;

my_channel_number = atol(process_number);
waiting_for_response = 0;
last_time = time(0);

/* Loop until program is killed by SIGINT/SIGTERM. */
while (!end_program || waiting_for_response) {

	if (my_channel_number <= MAX_SDR_PROCESSES) {

		/* Get the current time */
		curr_time = time(0);

		if (curr_time-last_time > pseudo_ftp_rate/2) {

			/* do poll_point every 60 secs or less to keep stream connection active 
			   using ftp_rate (default 16 secs) for convenience */			
			status = poll_point(gaim_format_rt_pid);
			/*if (status != 0) log_event_err("poll_point error %i (%s)",status,stream_err_messages[0-status]);*/

			failure = 0;
			waiting_for_response = 0;
			
			if (saw_a_frame && send_data) { 
			
				/* ftp pseudos and/or globals */
				if (ftp_mode == FTP_PSEUDO_ONLY) {
					failure = UBG_ftp_pseudo_checkpoint_file();
					
				} else if (ftp_mode == FTP_GVA_ONLY) {
					failure = UBG_ftp_gva_checkpoint_file();
				
				} else {
					if (ftp_next == FTP_PSEUDO_ONLY) {
						failure = UBG_ftp_pseudo_checkpoint_file();
						ftp_next = FTP_GVA_ONLY;
					} else {
						failure = UBG_ftp_gva_checkpoint_file();
						ftp_next = FTP_PSEUDO_ONLY;
					}					
				}
				/* wait for response if ftp was successful */
				if (!failure) waiting_for_response = -1;
				
			}
				
			last_time = curr_time;

			/* Now that we have ftp'ed the pseudos, we need to see if
			   another frame is sent to us before we send pseudos again. */
			saw_a_frame = 0;

		}

	}

	/* If we are supposed to end the program, force the program to
	   no longer wait for a response. */
	retval = UBG_wait_forever();
	if (retval) end_program = -1;
	if (end_program) waiting_for_response = 0;

} /* end dowhile */

log_event("exiting UBG_main loop");

/* Set the point, but only if we are the PRIME-PRIME stream 
  (channels 1, 3, 5, and 7) AND no stream failure message from SDR. 
  retval==999 means that UBG received a stream failure message from SDR. 
*/
if (retval!=999 && run_mode==2 && process_number_value<8 && (process_number_value%2)==1) {
	
	if (status == STRM_SUCCESS) {
		/* ARaj -- set_point call was core dumping */
		/* moved next 3 lines from out of this if st. to inside.
		   was giving core dump when gaim_format_rt_pid is nil..Lata */
		strcpy(gaim_format_rt_value,"Not Active");
		if (gaim_format_rt_pid != (void *)NULL) {
			strcpy((char *)gaim_format_rt_pid->value,gaim_format_rt_value);
			gaim_format_rt_pid->status_bits=0;
		}

		status=set_point(gaim_format_rt_pid);
		if (status!=STRM_SUCCESS) {
		   log_event_err("set_point error(%i) for GAIM_FORMAT_RTxx: %s",
				status,stream_err_messages[0-status]);
		}

		status=flush_sets();
		if (status!=STRM_SUCCESS) {
		   log_event_err("flush_sets error(%i): %s",status,stream_err_messages[0-status]);
		}
	} 
}


/* ************************************
   * Shutdown Update_Buffer_Generator *
   ************************************ */

/* stop checkpoint copy backr=ground jobs */
sprintf(cmd,"touch %s/ftp/UBG%d.GVA.STOP\n",gaim_home,process_number_value);
system(cmd);
sprintf(cmd,"touch %s/ftp/UBG%d.DEF.STOP\n",gaim_home,process_number_value);
system(cmd);

if (strlen(process_number)>0 && ubs_socket>0) {
		/* If this was spawned with a process number and it is shutting down
			on its own (i.e. UBS is not shutting down), send a socket
			shutdown message to UBG and shut down the socket */

	log_event("send normal-shutdown msg to UBS");
	net_write(ubs_socket,"zzzzzzzz",8);
	shutdown(ubs_socket,SHUT_RDWR);
	close(ubs_socket);
	FD_CLR(ubs_socket,&read_mask);
	ubs_socket=-1;
		/* Close down the socket */
}

if (strlen(process_number)>0 && sdr_pub_socket>0) {
		/* If this was spawned with a process number and it is shutting down
			on its own (i.e. UBS is not shutting down), send a socket
			shutdown message to UBG and shut down the socket */

	if (strlen(error_message)>8) {
		log_event("send error-shutdown msg to SDR");
		net_write(sdr_pub_socket,error_message,strlen(error_message)+1);
	} else {
		log_event("send normal-shutdown msg to SDR");
		net_write(sdr_pub_socket,"zzzzzzzz",8);
	}

	/* Close down the socket */
	shutdown(sdr_pub_socket,SHUT_RDWR);
	close(sdr_pub_socket);
	FD_CLR(sdr_pub_socket,&read_mask);
	sdr_pub_socket=-1;

}

/* Terminate the telemetry and global variable utilities */
if (stream_def) if (stream_def->dbhead) {
	term_tlm_util(stream_def->dbhead);
	term_gv_util(stream_def->dbhead);
	delete_db(stream_def->dbhead);
}

/* Disconnect from all streams */
disconnect_streams();

if (stream_def) {
	free(stream_def->database);
	free(stream_def);
}

/* Free allocated memory */
free(update_buffer.message_header.node_name);
free(update_buffer.message_header.process_name);
free(update_buffer.data_header.database_name);
free(update_buffer.data_body);
free(update_buffer.tlm_or_pseudo);

/* No Error exit */
log_event("Update Buffer Generator normal exit");
exit(0);

}


/* *********************
   * ^C Signal Handler *
   ********************* */

static void signal_handler(int signal) {

char message[256];

if (signal==SIGINT)
	sprintf(message,"Playback_Update_Buffer killed by ^C");
else if (signal==SIGTERM)
	sprintf(message,"Playback_Update_Buffer killed by SIGTERM");

if (signal==SIGINT || signal==SIGTERM) {
	end_program = -1;
	log_event(message);
	strcat(error_message,message);
	
/* ************************************
* Shutdown Update_Buffer_Generator *
************************************ */

/* stop checkpoint copy background jobs */
sprintf(cmd,"touch %s/ftp/UBG%d.GVA.STOP\n",gaim_home,process_number_value);
system(cmd);
sprintf(cmd,"touch %s/ftp/UBG%d.DEF.STOP\n",gaim_home,process_number_value);
system(cmd);

	if (ubs_socket>0) {
		/* If this was spawned with a process number and it is shutting down
		   on its own (i.e. UBS is not shutting down), send a socket
		   shutdown message to UBG and shut down the socket */
		log_event("send normal-shutdown msg to UBS");
        	net_write(ubs_socket,"zzzzzzzz",8);
        	shutdown(ubs_socket,SHUT_RDWR);
        	close(ubs_socket);
        	FD_CLR(ubs_socket,&read_mask);
        	ubs_socket=-1;
                	/* Close down the socket */
	}

	if (sdr_pub_socket>0) {
		/* If this was spawned with a process number and it is shutting down
		   on its own (i.e. UBS is not shutting down), send a socket
		   shutdown message to UBG and shut down the socket */

		if (strlen(error_message)>8) {
			log_event("send error-shutdown msg to SDR");
			net_write(sdr_pub_socket,error_message,strlen(error_message)+1);
		} else {
			log_event("send normal-shutdown msg to SDR");
			net_write(sdr_pub_socket,"zzzzzzzz",8);
		}

        	shutdown(sdr_pub_socket,SHUT_RDWR);
        	close(sdr_pub_socket);
        	FD_CLR(sdr_pub_socket,&read_mask);
        	sdr_pub_socket=-1;
                	/* Close down the socket */
        }

	/* Terminate the telemetry and global variable utilities */
	if (stream_def) if (stream_def->dbhead) {
        	term_tlm_util(stream_def->dbhead);
        	term_gv_util(stream_def->dbhead);
        	delete_db(stream_def->dbhead);
	}

	/* Disconnect from all streams */
	disconnect_streams();

	if (stream_def) {
        	free(stream_def->database);
        	free(stream_def);
	}

	/* Free allocated memory */
	free(update_buffer.message_header.node_name);
	free(update_buffer.message_header.process_name);
	free(update_buffer.data_header.database_name);
	free(update_buffer.data_body);
	free(update_buffer.tlm_or_pseudo);

	log_event("UBG has terminated gracefully from signal handler");
	exit(0);
}
	

} /* end UBG_main */

void log_tlmwatch (char *fmt,...)
{
static  char    outbuf[1024];
        int     len;
        va_list ap;

int status;
time_t current_time;
struct tm current_time_str;
fpos_t filelen;
char timetag[32];

/* get current time */
status = time(&current_time);
current_time_str = *localtime(&current_time);

/* format message text into outbuf */
if (fmt) {
	va_start (ap, fmt);
	if (!strchr(fmt, '%'))
		strcpy(outbuf, fmt);
	else
		vsprintf(outbuf, fmt, ap);
	va_end(ap);
} else {
	return;
}

len = strlen(outbuf);
if (len > 0 && outbuf[len-1] == '\n') {
	outbuf[len-1] = '\0';
 	len--;
}

if (tlmwatch_ptr == NULL && tlmwatch_open_err == 0) {

	/* open tlmwatch file */
	if (run_mode == 0 || run_mode == 2)
		sprintf(tlmwatch_filename,"%s/output/%s_tlmwatch.log",gaim_home,stream_name);
	else
		sprintf(tlmwatch_filename,"%s/output/%s_tlmwatch.log",gaim_home,"playback");
			
	tlmwatch_ptr = fopen(tlmwatch_filename,"w");
	if (tlmwatch_ptr == NULL) {
		log_err("*** cannot open tlmwatch log file = %s",tlmwatch_filename);
		tlmwatch_open_err++;
		return;
	} else {
		log_event("opened tlmwatch log file = %s",tlmwatch_filename);
		fprintf(tlmwatch_ptr,"\n");
		fprintf(tlmwatch_ptr,"tlmwatch filename   = %s\n",tlmwatch_filename);
		fprintf(tlmwatch_ptr,"tlmwatch input size = %ld\n",tlmwatch_size);
		fprintf(tlmwatch_ptr,"tlmwatch_max        = %d\n",tlmwatch_max);
		fprintf(tlmwatch_ptr,"tlmwatch_buffer     = (%40s)\n",tlmwatch_buf);
		fflush(tlmwatch_ptr);
	}
		
}

/* write message to tlmwatch file */
status = fprintf(tlmwatch_ptr,"%4.4i-%3.3i-%2.2i:%2.2i:%2.2i  %s\n",
            current_time_str.tm_year+1900,current_time_str.tm_yday+1,
            current_time_str.tm_hour,current_time_str.tm_min,
            current_time_str.tm_sec,
			outbuf);

if (status <= 0) {
	log_err("*** error writing to tlmwatch log file = %s",tlmwatch_filename);
	tlmwatch_ptr == NULL;
} else
	fflush(tlmwatch_ptr);


} /* end log_tlmwatch */

#include "../include/util_log.c"

