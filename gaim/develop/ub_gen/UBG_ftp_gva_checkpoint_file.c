/*
@(#)  File Name: UBG_ftp_gva_checkpoint_file.c  Release 3.3  Date: 07/08/23, 15:17:10
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_ftp_gva_checkpoint_file.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles

         VER.        DATE        BY              COMMENT
		 11         Mar12-2007	 K. Dolinh		 Created
					Mar27-2007					 add GAIM.STOP and script_stop
					Mar28-2007                   create file *.ggsa to save 
					                             global vars tag and name
					Mar30-2007					 commented out above 
					Apr02-2007					 append P/NP to checkp filename
												 truncate *.gva filename in event msg
		 12A        Aug22-2007					 rm STOP file before starting cp script
		  
********************************************************************************
	Invocation:

		UBG_ftp_gva_checkpoint_file()

	Parameters:

		NONE

********************************************************************************
	Functional Description:

		This routine ftp's the checkpoint file from the correct gtacs.

*******************************************************************************/

/**

PDL:
	Create the ftp script.
	Spawn the ftp command as a child of this routine.
	Open the ftp output file.
	Search the file for possible communication errors such as "No connection",
	 "Please login with USER and PASS", and "No such file".
	IF any ftp errors THEN
		Write a message to the debug log.
	ENDIF
	IF the ftp did not fail THEN
		Read the gva checkpoint file.
		DOFOR each point in the checkpoint file
			CALL UBG_make_pseudo_buffer_from_cp
		ENDDO
		CALL UBG_process_pseudo_packet
	ENDIF

	RETURN an error status
**/


#include <string.h>

/* Globals and structures */
#include "UBG_structures.h"

extern int debug_pseudo;
extern int debug_gva;
extern int pseudo_ftp_rate;
/* 
 * data used by tlm watch 
 */
extern char *tlmwatch_buf;
extern int	 tlmwatch_max;


static  int  gva_first_time = 1;
static  int  gva_count = 0;
static  int  gva_count_max = 100;
static  int  total_gva_points  = 0;
static  int  total_gva_buffers = 0;
static  int  gva_call_total = 0;

/*
#define GGSA_MAX 20000
static FILE *ggsaFILE;
static int  ggsa_tag[GGSA_MAX];
static char gaim_filename_ggsa[256] = "";
*/

int UBG_ftp_gva_checkpoint_file(void) 

{
char cmd1[256] = "";
GVA_struct gva;
Pseudo_struct pseudo;
FILE *gvaFILE;

/* point struct from database */
point  	  *pid;
tlm_point *tlm_pid;
tlm_value * cv;

char *found1,*found2;
int  shared_value;

/* -1=failure */
int ftp_failed = 0;

/* Counter */
int i;

/* Time used for timestamping of globals */
int current_time;

int tag;
int status;
double raw_value;

/* Init pseudo output buffer 
pseudo_cp_length=0;
*/

/* start background job to copy checkpoint file from gtacs to gaim */
if (gva_first_time) {
	UBG_ftp_start_job (DATATYPE_GVA);
	gva_first_time = 0;
	gva_count = 0;	
	return 0;
}
		

/* open checkpoint file */
/* exit if file is not yet copied to gaim */
gvaFILE = fopen(gaim_filename_gva,"rb");
if (!gvaFILE) return 0;

/* increment counter */
gva_count++;
if (gva_count >= gva_count_max) {
	/*log_event("   GVA processing: files=%d  buffers=%d (%s)",gva_count,total_gva_buffers,&gaim_filename_gva[30]);*/
	gva_count = 0;
	total_gva_buffers = 0;
}
	
/* read checkpoint file and make update buffers */

/* Init counter */
i = 0;

/* Get the current time */
current_time = time(0);

/* Init pseudo output buffer */
pseudo_cp_length=0;

/* While not end of file */
while (!feof(gvaFILE)) {

	fread(&gva,sizeof(gva),1,gvaFILE);
	if (!feof(gvaFILE) && strlen(gva.name)) {

		 /* add pseudo to buffer */	
		 tag = gva.tag;	
		 memcpy(&pseudo,&gva,sizeof(pseudo));
		 UBG_make_pseudo_buffer_from_cp(DATATYPE_GVA, &pseudo, current_time,tag);
		 total_gva_points++;
		 i++;
		 
     if (tlmwatch_max) {
		 /* output point info to tlmwatch file if pointname is in tlmwatch.cfg */
		 found1 = (char *)strstr(tlmwatch_buf,gva.name);
		 found2 = (char *)strstr(tlmwatch_buf,"*ALL*");
		 /* if ((found1 && strlen(pseudo.mnemonic) > 8) || found2) { */
		 if (debug_gva && gva_count == 1)
			log_tlmwatch("GVA name=(%32.32s)  tag=%5d  value=%g  status=X%8.8X)",
			  gva.name,gva.tag,gva.value,gva.status);				
     }
	}

} /* end dowhile */

/* close file */
fclose(gvaFILE);

/* remove checkpoint file so that a new one can be fetched by script */
sprintf(cmd1,"rm %s",gaim_filename_gva);
system(cmd1);

/* Send gva as pseudo packet */
memcpy(pseudo_cp_buffer,&pseudo_cp_counter,2);
if (i > 0) {
	UBG_process_pseudo_packet(DATATYPE_GVA,pseudo_cp_length,pseudo_cp_buffer);
	total_gva_buffers++;
	gva_call_total++;
	if (gva_call_total >= 1 && gva_call_total <= 10) { 
		found1 = (char *)strstr(gaim_filename_pseudo,"ftp/");
		if (found1) found1 = found1+4;
		else found1 = gaim_filename_gva;
		log_event("GVA processing totals: (%s) %d buffers sent  %d points in current buffer",found1,total_gva_buffers,i);
	}

}


return 0;
} /* end UBG_ftp_gva_checkpoint */


/***
 *** This routine creates 2 shell scripts to perform the ftp of checkpoint files
 ***   UBGn_get_type.sh 
 
		 while true
		 do

		 cd /home/gtacsops/gaim/ftp

		 if [ -r stop.gva ]; then
			echo *** exiting ftp
			exit
		 fi

		 if [ -r test.gva ]; then
			echo *** file test.gva exists
		 else 
			echo *** file test.gva does not exist
			echo *** starting ftp ...
			ftp sgtacs04 < xxx..ftp.sh > /home/gtacsops/gaim/ftp/xxx..ftp.out
		 fi

		 sleep N
		 done

 
 ***   UBGn_type_ftp.sh   = this ftp script actually executes the ftp operation
         bin
         prompt
         get /home/gtacsops/epoch/output/sgtacs04/goes13/s4rt2/*.gva /home/gtacsops/gaim/ftp/*.gva
         quit

 *** where
 ***   n = ubg process number (1,3,5,...)
 ***   type = def or gva
 *** The ftp is performed only if the *.def or *.gva file is not in the ftp dir
 *** The script will exit if UBGn.type.STOP  or GAIM.STOP is found in the ftp dir
 ***/    
int UBG_ftp_start_job (int datatype) 

{
char ftp_dir[256] = "";
char scriptname[256] = "";
char cmd[256] = "";
char gtacs_filename[256] = "";
char gaim_filename[256]  = "";
char script_stop[256]  = "";
char buffer[256];
char *tok,*db_name;
FILE *scriptFILE;
int	i;
char prime;


if (prime_nonprime_stream[0] == 'P')
	prime = 'P';
else
	prime = 'N';
	
/* set db_name to database_name wihout dir path and ".lis" */
db_name = (char *) "none";
strncpy(buffer,database_name,sizeof(buffer));
tok = (char *)strtok(buffer,"/.");
while (tok) {
	tok = (char *)strtok(NULL,"/.");
	if (tok) { 
		if (!strcasecmp(tok,"lis")) break;
		db_name = (char *)tok;
	}	
}

/* construct name of pseudo file on the gtacs server and locally 
   gtacs: /home/gtacsops/epoch/output/sgtacs05/goes13/s5rt1/g13_415_def_040316.gva (or def)
   gaim : $GAIM_HOME/ftp/s5rt1_g13_415_def_040316.gva (or def)
 */
sprintf(gtacs_filename,"%s/%s/%s/%s/%s.gva",getenv("EPOCH_OUT"),gtacs_name,scid,stream_name,db_name);

/* construct name of ftp directory */
sprintf(ftp_dir,"%s/ftp",getenv("GAIM_HOME"));

/* construct other filenames as type gva or def */
if (datatype == DATATYPE_GVA) {
	sprintf(scriptname,"%s/UBG%d_get_gva.sh",ftp_dir,process_number_value);
	sprintf(gtacs_filename,"%s/%s/%s/%s/%s.gva",getenv("EPOCH_OUT"),gtacs_name,scid,stream_name,db_name);
	sprintf(gaim_filename_gva, "%s/%s_%s_%c.gva",ftp_dir,stream_name,db_name,prime);
	sprintf(gaim_filename,     "%s/%s_%s_%c.gva",ftp_dir,stream_name,db_name,prime);
	sprintf(script_stop,"UBG%d.%s.STOP",process_number_value,"GVA");
	
	/* open new *.ggsa file 
	sprintf(gaim_filename_ggsa,"%s/reports/%s/%s_%s.ggsa",getenv("EPOCH_DATABASE"),scid,stream_name,db_name);
	ggsaFILE = fopen(gaim_filename_ggsa,"w");
	if (!ggsaFILE) {
		log_event_err("Cannot open ggsa file (%s)",gaim_filename_ggsa);
		ggsaFILE = NULL;
	} else {
		log_event("Opened ggsa file (%s)",gaim_filename_ggsa);
		for (i=0; i<GGSA_MAX; i++) {
			ggsa_tag[i] = 0;
		}
	}*/

} else {
	sprintf(scriptname,"%s/UBG%d_get_def.sh",ftp_dir,process_number_value);
	sprintf(gtacs_filename,"%s/%s/%s/%s/%s.def",getenv("EPOCH_OUT"),gtacs_name,scid,stream_name,db_name);
	sprintf(gaim_filename_pseudo, "%s/%s_%s_%c.def",ftp_dir,stream_name,db_name,prime);
	sprintf(gaim_filename,        "%s/%s_%s_%c.def",ftp_dir,stream_name,db_name,prime);
	sprintf(script_stop,"UBG%d.%s.STOP",process_number_value,"DEF");
	/*if (getenv("GAIM_FTP_COUNT_MAX")) psu_count_max = atoi(getenv("GAIM_FTP_COUNT_MAX"));*/
}


/* remove existing checkpoint file */
sprintf(cmd,"rm %s",gaim_filename);
system(cmd);

/* remove existing stop files */
sprintf(cmd,"rm %s/UBG%d.*.STOP",ftp_dir,process_number_value);
system(cmd);
log_event("remove stop files (%s)",cmd);

/* write script to get checkpoint file */
scriptFILE = fopen(scriptname,"w");
if (!scriptFILE) {
	log_event_err("cannot open script file (%s)",scriptname);
	return 0;
}

fprintf(scriptFILE,"#!/bin/sh\n");
fprintf(scriptFILE,"while true \n");
fprintf(scriptFILE,"   do \n");
fprintf(scriptFILE,"   cd ~gtacsops/gaim/ftp \n");

/* exit script if stop_gaim.sh */
fprintf(scriptFILE,"   if [ -r GAIM.STOP ]; then \n");
fprintf(scriptFILE,"      echo *** GAIM.STOP exit\n");
fprintf(scriptFILE,"      exit \n" );
fprintf(scriptFILE,"   fi \n" );
/* exit script if ubg process terminates */
fprintf(scriptFILE,"   if [ -r %s ]; then \n",script_stop);
fprintf(scriptFILE,"      rm  %s \n",script_stop);
fprintf(scriptFILE,"      echo *** %s exit\n",script_stop);
fprintf(scriptFILE,"      exit \n" );
fprintf(scriptFILE,"   fi \n" );
/* copy file from remotely mounted gtacs dir to local gaim dir */
fprintf(scriptFILE,"   if [ -r %s ]; then \n",gaim_filename);
fprintf(scriptFILE,"      echo *** waiting for %s to be removed \n",gaim_filename); 
fprintf(scriptFILE,"      sleep %d \n",pseudo_ftp_rate/10);
fprintf(scriptFILE,"   else \n");
fprintf(scriptFILE,"      cp %s %s \n",gtacs_filename,gaim_filename);
fprintf(scriptFILE,"      sleep %d \n",pseudo_ftp_rate);
fprintf(scriptFILE,"   fi \n" );

fprintf(scriptFILE,"done \n");
fclose(scriptFILE);

/* execute script using Bourne shell */
sprintf(cmd,"/bin/sh %s > %s.out &",scriptname,scriptname);
system(cmd);

log_event("started get_checkpoint_file script (%s)",scriptname);
return 0;

} /* end UBG_ftp_start_job */
