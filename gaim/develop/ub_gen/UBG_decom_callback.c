/*  
@(#)  File Name: UBG_decom_callback.c  Release 1.27  Date: 07/03/25, 09:12:17
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_decom_callback.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles       Initial creation
	 8.4         mar18-2004  K. Dolinh	 Add log_event, log_debug
	 8.8.1E      aug13-2004                  Add tlmwatch output
	 8.B         sep17-2004                  GTACS Build 8B
	 8.B.1                                   add tlmwatch_max
	 8.B.2       sep27-2004                  debug gsa search
	 8.B.3       oct14-2004                  get reversed tlm value
	             oct20-2004                  write debug tlm values in decimal
				 oct28-2004                  use help_index as tag id
				                             archive tlm_variety=SCALED_ANALOG  
	             nov17-2004                  removed tlm_variety=MODE as type to archive
				 
	      9.1   jan06-2005      K. Dolinh    use get_sc_value instead of get_raw_value
		  9.2   jan14-2005                   removed unused function: get_IEEE_value 
		  9.2A  jan24-2005                   if (tlm is reversed or inverted or SIGNED_MAG)
		                                     use get_raw_value, otherwise use get_sc_value
		        jan27-2005                   fix copying of SIGNED_MAG value to update buffer
				feb11-2005                   add tlmwatch message
		  9A1   feb25-2005                   Remove tlmwatch_max check	
	 	  9AP	mar16-2005		 			 Build 9A Prime	
		        apr25-2005                   - Do not archive stale point
											 - Do not archive point with negative offset  
											 - Use log_tlmwatch									  
											 - Check for possible gap in frames
		  9C	Jul20-2005		 			 Do not archive point if not in gsa file
		  10A   Aug26-2005                   Build 10A
		                                     - save value of NORMAL_DWELL_MODE
		        Sep15-2005                   - debug get_telemetry_context											 							
		  10B   Jan12-2006                   Build 10B
		  11    Oct02-2006                   throttle GSA err message
		  											 									  		
********************************************************************************
	Invocation:
        
		UBG_decom_callback(point *pid,int reason);
        
	Parameters:

		pid - I
			The structure that contains the point id which invoked this 
			callback.  It also contains additional information about the point 
			as defined by EPOCH for the pint structure.

		reason - I
			Reason the callback is called.

********************************************************************************
	Functional Description:

		This routine is defined as the callback to be called each time the 
		EPOCH telemetry utilities decommutate a point from a frame.
	
*******************************************************************************/

/**

PDL for UBG_decom_callback:
	IF a null PID is received THEN
		RETURN
	ENDIF

	IF the PID is a pseudotelemetry point THEN
		RETURN
	ENDIF

	IF the PID is a non-GSA mnemonic THEN
		RETURN
	ENDIF

	Extract the value, time, telemetry context, status flags, point index, and 
	 limit violations.
	Add this mnemonic and its' information to the current update buffer.

	IF this point is a floating point number THEN
		Set the floating point flag.
		Copy the value to the update buffer.
	ELSEIF this point is a byte array THEN
		Mark this point as a byte array.
		IF this point is not a 1, 2, 4, or 8 byte array, prefix the string with
		 zeros -- we are assuming an unsigned integer.
	ELSEIF this point is an integer THEN
		Set the integer point flag.
		Copy the value to the update buffer.
	ELSE
		Write a message to the debug log -- unknown data type.
		Assume the unknown type is 8 bytes and copy the 8 bytes to our update 
		 buffer.
	ENDIF

	Collect the current value of all the important telemetry points 
	 (TELEMETRY_RATE, FRAME_TYPE, PSM_STREAM_ID, MINOR_FRAME_COUNT, 
	 QUALITY_BIT, and TLM_FORMAT)

**/

#include <math.h>
#include "db.h"
#include "list.h"
#include "tlm.p"
#include "eu_util.p"
#include "UBG_structures.h"

double get_IEEE_value(double raw_value, int num_bytes);
int get_telemetry_context(point *pid);

/* 
 * data used by tlm watch 
 */
extern char *tlmwatch_buf;
extern int	 tlmwatch_max;
int		hh,mm,ss,ms,tt;

/* frame format info */
extern int pcm1_format;
extern int pcm2_format;
extern int normal_dwell_mode;

/* env processing flags */
extern int prev_frame_end_year;
extern int prev_frame_end_day;
extern int prev_frame_end_ms;

int status;
time_t current_time;
struct tm current_time_str;
char *found1,*found2;

void UBG_decom_callback(point *pid,unsigned long reason) {

/* Loop Counter */
int i;
	
/* Debug Message Buffer */
char message[1024],message1[1024];

static int printbuffer=0;
	
/* decommed telemetry point */
tlm_point *decommed_point;
/* extracted process flags */
unsigned long  tlm_reverse_flag;	
unsigned long  tlm_invert_flag;	
	
/* Local storage for raw value */
double reduced_raw_value=0.0;
double raw_value=0.0;
double temp_raw_value=0.0;

signed	int		int_raw_value;
signed	long	long_raw_value;
char	raw_bytes[8];

/* Variables needed for limit processing */	
unsigned long sc_raw_value;
status_type limits_value;
int limits_message;
short limit_flags;
	
/* How many bytes per sample? */
short int num_bytes=0,temp_bytes=0;
	
/* buffer for hex dump of data */
char short_buff[10];
char hex_buffer[65];
	
/* Translate from the minimum number of bytes to a power of two byte */
short int byte_translation[17]={0,1,2,4,4,8,8,8,8,16,16,16,16,16,16,16,16};
	
/* Which state table are we using? */
int state_table_number;
	
/* sample time */
int gap_ms;
int offset_ms;
int curr_sample_ms;
static int prev_frame_count = -1;
static int expected_frame_count = -1;

/* convert RAW_VALUE to 4-byte float, if data size <= 4 bytes */
float four_byte_float;
	
/* 0=Integer Raw, 1=Float Raw, 2=Byte Array */
int int_or_float;
	
/* Is the point an integer, float, or byte array? */
char int_or_float_letter[3]={'I','F','B'};
	
/* Used to get current point time */
tlm_value *value;
	
/* Index into the gsa_index_list and gsa_index_mode lists */
int gsa_index_index=0;
	
int override=0;
double raw_raw_value=0;

/* begin UBG_decom_callback */

if (!pid) {
	/* Check for a valid point */
	log_event_err("decom_callback: received NULL Point");
	return;
}

/* Extract only what we need */
decommed_point=(tlm_point *)pid->tlm_point;
value = (tlm_value *)pid->value;

/* Get the current raw value */
get_raw_value(pid,&raw_value);

/* if point is checksum, save its timetag as end-of-frame time */
if (!strcasecmp(decommed_point->tlm_mnemonic,"MINOR_FRAME_CHECKSUM")) {
	prev_frame_end_day = value->time.tv_sec/86400;	
	UBG_days_and_years(&prev_frame_end_day,&prev_frame_end_year);
	prev_frame_end_ms  = (value->time.tv_sec%86400)*1000 + (value->time.tv_usec/1000);
}

/* get frame format info */
if (!strcasecmp(decommed_point->tlm_mnemonic,"PCM1_FORMAT")) pcm1_format = raw_value;
if (!strcasecmp(decommed_point->tlm_mnemonic,"PCM2_FORMAT")) pcm2_format = raw_value;
if (!strcasecmp(decommed_point->tlm_mnemonic,"NORMAL_DWELL_MODE")) normal_dwell_mode = raw_value;

/* Get current telemetry_rate. */
if (pid->tlm_point_index==telemetry_rate_index)
	current_telemetry_rate=(int)raw_value;
		
/* Get current stream_source (frame_type_pid is actually the STREAM_SOURCE pid). */
if (pid->tlm_point_index==frame_type_index)
	frame_type_value=(int)raw_value;
		
/* Get current pcm stream id. */
/* 	if (pid->tlm_point_index==pcm_stream_id_index) 
	pcm_stream_id_value=(int)raw_value; */
	
/* Get current minor frame count */
if (pid->tlm_point_index==minor_frame_count_index) {
	minor_frame_count_value=(int)raw_value;
	if (run_mode==2 && prev_frame_end_ms > 0) {
		gap_ms = update_buffer.data_header.milliseconds - prev_frame_end_ms;
		/* realtime archive, check for possible gap in tlm frames */
		if ((gap_ms > 5000 ) ||
		    (gap_ms > 1004 && minor_frame_count_value == prev_frame_count) ||
		    (gap_ms > 1004 && minor_frame_count_value != expected_frame_count) ) {
			/* log_event("WARNING: possible tlm gap in stream=(%s)  frame: current=%2.2d  last=%2.2d expected=%2.2d  gap_ms=%d",
				stream_name,minor_frame_count_value,prev_frame_count,expected_frame_count,gap_ms);*/
		}
		prev_frame_count = minor_frame_count_value;
		expected_frame_count = minor_frame_count_value + 1;
		if (expected_frame_count >= 32) expected_frame_count = 0;
	} else {
		/* first frame received */
		prev_frame_count = minor_frame_count_value;
		expected_frame_count = minor_frame_count_value + 1;
		if (expected_frame_count >= 32) expected_frame_count = 0;
	} 	
} 

		
/* Get current quality bit. */
if (pid->tlm_point_index==quality_bit_index) {	
	quality_bit_value=(int)raw_value;
	log_debug("quality_bit_value = %d",quality_bit_value);		
}

/* Get current tlm_format entry. 0,1,2==NORMAL, 8,9,10==DWELL */
if (pid->tlm_point_index==tlm_format_index) 
	tlm_format_value=(int)raw_value;

/* Do not archive PseudoTlm here. PseudoTlm is extracted via ftp'ed checkpoint file */
if (pid->tlm_point->tlm_variety==PSEUDO) return;


/* do not archive a stale point */
if (value->update_status & status_mask[STALE_VALUE]) return;

/* Do not archive mnemonic STREAM_SOURCE */
if (pid->tlm_point_index==frame_type_index) return;

/* Archive only the following tlm types */	
if (pid->tlm_point->tlm_variety!=ANALOG && 
    pid->tlm_point->tlm_variety!=SCALED_ANALOG &&
	pid->tlm_point->tlm_variety!=DIGITAL &&
	pid->tlm_point->tlm_variety!=FRAME_COUNTER) return;		

/* do not archive a point if not in gsa file */
if (decommed_point->help_index < GSA_TAG_MAX && gsa_tag[decommed_point->help_index] != 1) {
	/* output err message if first time */
	if (gsa_tag[decommed_point->help_index] != 2) {
		gsa_tag[decommed_point->help_index] = 2;
		log_tlmwatch("WARNING: index=%4.4d  name=%s  point not in gsa file is not archived",
			decommed_point->help_index,decommed_point->tlm_mnemonic);
	}
	return;
}


/***DEBUG: remove this check.
   Is this point part of the "keep" list?  If not, ignore it. 	
gsa_index_index=GAIM_binarysearch_int(mnemonic_to_gsa_count,gsa_index_list,pid->tlm_point_index);
if (gsa_index_index==-1) {
	if (frame_type_value==MRSSHK_FRAME_TYPE || 
	    frame_type_value==IMGWB_FRAME_TYPE  || 
		frame_type_value==SNDWB_FRAME_TYPE) {
	} else {
		return;
	}
}		
ENDDEBUG***/

/*	if (gsa_index_mode[gsa_index_index]!=frame_type_value) {
	sprintf(message,"[UBG%2s] Unexpected mnemonic %s seen!  %s %i, %s %i.",
		process_number,decommed_point->tlm_mnemonic,"Current Frame Type:",
		frame_type_value,"Expected in Frame Type:",
		gsa_index_mode[gsa_index_index]);
	log_debug(message);
	return;
}
*/

/* Calculate difference between sample-time and frame-start-time */
curr_sample_ms = (value->time.tv_sec%86400)*1000 + (value->time.tv_usec/1000);
offset_ms = curr_sample_ms - update_buffer.data_header.milliseconds;
	
/* If offset is negative, midnight occurred in frame, add one day's ms to offset */  
if (offset_ms < 0) {
	/* offset_ms += 86400000; */
	/*  Do not archive this sample */	
	log_tlmwatch("WARNING: mignight in frame (frame=%2.2d  ms=%d q=%4.4X)  point_ms=%d  offset=%d  q=%4.4X  n=%5.5d  point=%s not archived",
			minor_frame_count_value,update_buffer.data_header.milliseconds,quality_bit_value,
			curr_sample_ms,offset_ms, value->update_status, update_buffer.number_of_mnemonics, decommed_point->tlm_mnemonic);	
	return;
}
		
/* Convert from long int to unsigned short int */
update_buffer.data_body[update_buffer.number_of_mnemonics].time_offset=(unsigned short)(offset_ms);
		
/* Get current context state */
state_table_number = get_telemetry_context(pid);

/* if current state table > maximum allowed, mask out any MSBs */	
if (state_table_number>UBG_CONTEXT_MASK) {		
	log_debug("State Table Number is too big for the field (%i>%i).",
		state_table_number,UBG_CONTEXT_MASK);
}

/* set-initialize the status flags */
update_buffer.data_body[update_buffer.number_of_mnemonics].status_flags=
	(state_table_number & UBG_CONTEXT_MASK);
		
/* Mnemonic Index from DB. Need the +1 to match with HELP INDEX - same index used in GSA. 
update_buffer.data_body[update_buffer.number_of_mnemonics].mnemonic_index=pid->tlm_point_index+1;
*/

/* use help_index instead of tlm_point_index+1 */
update_buffer.data_body[update_buffer.number_of_mnemonics].mnemonic_index = decommed_point->help_index;
if (decommed_point->help_index <= 0 || 
    decommed_point->help_index > mnemonic_count)
	log_event_err("decom_callback: invalid help_index=%d for point=%s",decommed_point->help_index,decommed_point->tlm_mnemonic);

/* Size of data. Force up to next byte count if within the valid 
   range, otherwise use the exact byte count. */
if (decommed_point->data_size<=64) 
	num_bytes=byte_translation[(decommed_point->data_size-1)/8+1];
else 
	num_bytes=(decommed_point->data_size-1)/8;

/* Get the limit violations */
get_limits_value(pid,&limits_value,&limits_message);

	
	/* ******************************* */
	/* * NO DELTA LIMIT SUPPORT, YET * */
	/* ******************************* */


switch (limits_value) {
		/* Which limit, if any, was violated */

	case ALARM_HIGH:	/* Red High = 7 */
		limit_flags = UBG_RED_OR_YELLOW_VIOLATION|UGB_RED_VIOLATION|UGB_HIGH_VIOLATION;
		break;

	case ALARM_LOW:		/* Red Low = 6 */
		limit_flags = UBG_RED_OR_YELLOW_VIOLATION|UGB_RED_VIOLATION|UGB_LOW_VIOLATION;
		break;

	case WARNING_HIGH:	/* Yellow High = 5 */
		limit_flags = UBG_RED_OR_YELLOW_VIOLATION|UGB_YELLOW_VIOLATION|UGB_HIGH_VIOLATION;
		break;

	case WARNING_LOW:	/* Yellow Low = 4 */
		limit_flags = UBG_RED_OR_YELLOW_VIOLATION|UGB_YELLOW_VIOLATION|UGB_LOW_VIOLATION;
		break;

	default:			/* Normal */
		limit_flags = 0;
		break;
}
/* Set the current limits value */
update_buffer.data_body[update_buffer.number_of_mnemonics].status_flags |= limit_flags;
		
if (quality_bit_value!=0)
	update_buffer.data_body[update_buffer.number_of_mnemonics].status_flags |=
                                 UBG_QUESTIONABLE_QUALITY;
/* If FP format, keep as such */
if (decommed_point->tlm_type==FLOAT32_IEEE || 
		decommed_point->tlm_type==FLOAT64_IEEE || 
		decommed_point->tlm_type==FLOAT32_INTEL || 
		decommed_point->tlm_type==FLOAT16_1750T || 
		decommed_point->tlm_type==FLOAT16_1750H || 
		decommed_point->tlm_type==FLOAT24_1750T || 
		decommed_point->tlm_type==FLOAT32_1750A || 
		decommed_point->tlm_type==FLOAT40_1750T || 
		decommed_point->tlm_type==FLOAT48_1750A || 
		decommed_point->tlm_type==FLOAT32_RX2010 || 
		decommed_point->tlm_type==FLOAT48_RX2010 || 
		decommed_point->tlm_type==RB_FLOAT) {
			
	/* Set raw float flag for code in the future */
	int_or_float = 1; 
	/* Set the IEEE float flag in buffer header */	
	update_buffer.data_body[update_buffer.number_of_mnemonics].status_flags |= UBG_IEEE_FLOAT;
			
/*	No need to convert to IEEE value ...get_raw_value() converts all points to double -Lata */
/*	Convert from 1750 to IEEE 	
	raw_raw_value = raw_value;
	if (decommed_point->tlm_type==FLOAT16_1750T || 
		decommed_point->tlm_type==FLOAT16_1750H || 
		decommed_point->tlm_type==FLOAT32_1750A || 
		decommed_point->tlm_type==FLOAT48_1750A)
			raw_value = get_IEEE_value(raw_value,(decommed_point->data_size-1)/8+1);					  
*/
				 
	/* Four or fewer bytes for this float? */
	if (num_bytes<=4) {
		/* Copy the float to our buffer */	
		four_byte_float = (float)raw_value;
		memcpy(&update_buffer.data_body[update_buffer.number_of_mnemonics].raw_value[0],&four_byte_float,4);
		num_bytes = 4;

	} else {
			
		/* More than a 4-byte float? */
		if (num_bytes > 8) {
			log_event_err("floating point size > 8-byte NOT SUPPORTED");
		}
		/* Copy the float to our buffer */
		memcpy(&update_buffer.data_body[update_buffer.number_of_mnemonics].raw_value[0],&raw_value,8);
		num_bytes = 8;
			
	} 

		/* If not 1, 2, 4, or 8 bytes, prefix with zeros -- we are 
			assuming an unsigned integer */
} else if (decommed_point->tlm_type==BYTE_ARRAY) {

	/* Make sure the code in the future knows that this is a RAW byte array. */
	int_or_float=2;

	/* What is the true size of this byte array? */
	temp_bytes=(decommed_point->data_size-1)/8+1;
	
	/* Copy only the correct bytes */	
	memcpy(&update_buffer.data_body[update_buffer.number_of_mnemonics
		].raw_value[num_bytes-temp_bytes],&raw_value,temp_bytes);
			
	/* Zero any excess bytes, prior to the actual data */
	while (temp_bytes<num_bytes) {
		temp_bytes++;
		update_buffer.data_body[update_buffer.number_of_mnemonics].raw_value[num_bytes-temp_bytes]=0;
	}
		

} else if ( decommed_point->tlm_type==UNSIGNED || 
			decommed_point->tlm_type==SIGNED || 
			decommed_point->tlm_type==SIGNED_MAG || 
			decommed_point->tlm_type==RB_UNSIGNED || 
			decommed_point->tlm_type==RB_SIGNED) { 
			
	/* init value field in buffer */
	for (i=0; i<=15; i++) 
	   update_buffer.data_body[update_buffer.number_of_mnemonics].raw_value[i] = 0;
	/* init values */
	long_raw_value = 0;
	reduced_raw_value = 0;
	sc_raw_value = 0;
	
	/* Make sure the code in the future knows that this is a RAW int and not a RAW float. */
	int_or_float=0;
		
	/* Set the INT flag. */
	update_buffer.data_body[update_buffer.number_of_mnemonics].status_flags|=UBG_INT;			

	/* set flags for reversed or inverted tlm */
	tlm_reverse_flag = decommed_point->process_flags & tlm_process_mask[TLM_REVERSE_FLAG];
	tlm_invert_flag  = decommed_point->process_flags & tlm_process_mask[TLM_INVERT_FLAG];
	
	/* get raw value gives converted value .. better to get SC value and continue with logic -Lata */
	/* get_raw_value performs bit-reverse and bit-invert. get_sc_value does not */
	   
	if (tlm_reverse_flag || tlm_invert_flag) {
		reduced_raw_value = (double)raw_value;
	} else if (decommed_point->tlm_type == SIGNED_MAG) {
	    reduced_raw_value = (double)raw_value;
		long_raw_value = (long) reduced_raw_value;
		memcpy(&raw_bytes[0],&long_raw_value,sizeof(long_raw_value));
	} else{
		get_sc_value(pid, &sc_raw_value); 
		reduced_raw_value = (double)sc_raw_value;		
	}


/* copy point value to buffer */
if (decommed_point->tlm_type == SIGNED_MAG) {
		
	if (num_bytes == 1) {
	   memcpy(&update_buffer.data_body[update_buffer.number_of_mnemonics].raw_value[0],&raw_bytes[3],1);
	} else if (num_bytes == 2) {
	   memcpy(&update_buffer.data_body[update_buffer.number_of_mnemonics].raw_value[0],&raw_bytes[2],2);
	} else if (num_bytes == 4) {
	   memcpy(&update_buffer.data_body[update_buffer.number_of_mnemonics].raw_value[0],&raw_bytes[0],4);
	} else if (num_bytes == 8) {
	   memcpy(&update_buffer.data_body[update_buffer.number_of_mnemonics].raw_value[0],&raw_bytes[0],8);
	}
		

} else {

    /* Reduce the number to 65536 or less by using powers of 256 
    (2^8) such as 2^64, 2^48, 2^32, and 2^16 */	

	for (i=num_bytes-1; i>=0; i--) {
			
		if (i>8) {
			if (i > 10) {
				log_event_err("data size > 10-byte NOT SUPPORTED");
			}
			temp_raw_value=reduced_raw_value/pow(2.0,64.0);
			temp_raw_value=reduced_raw_value-(double)((long)(temp_raw_value))*pow(2.0,64.0);
		} else if (i>6) {
			temp_raw_value=reduced_raw_value/pow(2.0,48.0);
			temp_raw_value=reduced_raw_value-(double)((long)(temp_raw_value))*pow(2.0,48.0);
		} else if (i>4) {
			temp_raw_value=reduced_raw_value/pow(2.0,32.0);
			temp_raw_value=reduced_raw_value-(double)((long)(temp_raw_value))*pow(2.0,32.0);
		} else if (i>2) {
			temp_raw_value=reduced_raw_value/pow(2.0,16.0);
			temp_raw_value=reduced_raw_value-(double)((long)(temp_raw_value))*pow(2.0,16.0);
		} else {
			temp_raw_value=reduced_raw_value;
		}

		/* copy the byte to our data array */
		update_buffer.data_body[update_buffer.number_of_mnemonics].raw_value[i]=(char)((short)((long)(temp_raw_value)%256));
		reduced_raw_value-=(double)((long)(temp_raw_value)%256);
		reduced_raw_value/=(double)(256.0);			
	}
}
		
/* tlm type not supported */
} else {

	/* Assume 8 bytes. Probably wrong, but there should be no data of this type anyway */
	log_event_err("tlm type %i not supported. sending raw value as 64-bit char field",decommed_point->tlm_type);	
	num_bytes=8;
	memcpy(&update_buffer.data_body[update_buffer.number_of_mnemonics].raw_value[num_bytes-temp_bytes],&raw_value,num_bytes);
			
}

/* Set the bits to represent the data length */
update_buffer.data_body[update_buffer.number_of_mnemonics].status_flags|=
	byte_to_mask_translation[num_bytes];
		
update_buffer.tlm_or_pseudo[update_buffer.number_of_mnemonics] = 0;

/* output point info to tlmwatch file if pointname is in tlmwatch.cfg */

if (tlmwatch_max) 
{
found1 = (char *)strstr(tlmwatch_buf,decommed_point->tlm_mnemonic);
found2 = (char *)strstr(tlmwatch_buf,"*ALL*");
if ((found1 && strlen(decommed_point->tlm_mnemonic) > 8) || found2) {
	
	status = time(&current_time);
	current_time_str = *localtime(&current_time);

	get_sc_value(pid, &sc_raw_value);
	temp_raw_value = (double)sc_raw_value;

	/* extract hh:mm:ss.ms from frame-start millisecs */
	hh = update_buffer.data_header.milliseconds / (3600*1000);
	tt = update_buffer.data_header.milliseconds - (3600*1000*hh);
	mm = tt / (60*1000);
	tt = tt - (60*1000*mm);
	ss = tt / 1000;
	ms = tt - (1000*ss);
			
	log_tlmwatch("(ftype=%d  %3.3d-%2.2d:%2.2d:%2.2d.%3.3d  mnem=%4.4d)  offset_ms=%5.5d  index=%5.5d  point=%s  ptype=%2.2d  raw=%lf  sc=x%8.8X  context=%d",
		frame_type_value,update_buffer.data_header.day,
		hh,mm,ss,ms,
		update_buffer.number_of_mnemonics,
		offset_ms,
		update_buffer.data_body[update_buffer.number_of_mnemonics].mnemonic_index,
		decommed_point->tlm_mnemonic,
		decommed_point->tlm_type,
		raw_value,sc_raw_value,state_table_number);
}		
}
	

/* Increment mnemonic count */
update_buffer.number_of_mnemonics++;
	
/* override=-1; */

/* If this environment variable is defined and debug is on... */
if ((getenv("INCLUDE_DEBUG_DECOM_INFO") && debug_on) || override) {
		
	if (!override) strcpy(hex_buffer,getenv("INCLUDE_DEBUG_DECOM_INFO"));
	else strcpy(hex_buffer,"YES");

	if ((hex_buffer[0]=='Y' || hex_buffer[0]=='T') && debug_on) {
			/* If we want to include all the decom information about
				the current point, be forewarned that this option yields
				megabytes and megabytes of debug information! */

		temp_bytes=num_bytes;
		if (num_bytes>32) temp_bytes=32;
		for (i=0; i<temp_bytes; i++) {
			sprintf(short_buff,"x%8.8X",(short)(update_buffer.data_body[
					update_buffer.number_of_mnemonics-1].raw_value[i]));
			if (i==0) strcpy(hex_buffer,&short_buff[6]);
			else strcat(hex_buffer,&short_buff[6]);
		}
		if (num_bytes>32) strcpy(&hex_buffer[60],"...");

		sprintf(message1,", msOffset: %i (from %d.%3.3d) [%i:%i]",offset_ms,
			update_buffer.data_header.milliseconds/1000,
			update_buffer.data_header.milliseconds%1000,frame_type_value,
			gsa_index_mode[gsa_index_index]);
		sprintf(message,
			"[UBG%2s]%s %s(%i),Sz: %i bits (%i%c bytes), Rw: %lf, HX: %s%s",
			process_number," Decom pt",
			decommed_point->tlm_mnemonic,pid->tlm_point_index,
			decommed_point->data_size,
			mask_to_byte_translation[UBG_GET_DATA_MASK(
				update_buffer.data_body[update_buffer.number_of_mnemonics-1
				].status_flags)],
			int_or_float_letter[int_or_float],raw_value,hex_buffer,
			message1);
		log_debug(message);
			/* Which point did we receive? */
	}
} 

/* end UBG_decom_callback */
}  

/** 

PDL for get_telemetry_context:
Get the PID of the context mnemonic.
DOCASE context type
	CASE 1: Raw
		Get the raw value of the context mnemonic.
	ENDCASE

	CASE 2: EU
		Get the EU value of the context mnemonic.
	ENDCASE
ENDDO

IF there is more than one context state THEN
	Walk the linked list until the correct context state table is found.
ENDIF

RETURN the context table number

**/


int get_telemetry_context(point *pid) {
/* 
 * Derived from EPOCH routine set_telemetry_context 
 */
	
double raw_value;
LIST tcs;
tlm_eus	*eus;
tlm_value * value;

int table_number=0;
int end_loop=0;

if (pid->conv_ctx_pid) {
	value=(tlm_value *)pid->value;

	switch (pid->tlm_point->context_type) {
		case RAW_TYPE:
			get_raw_value (pid->conv_ctx_pid, &raw_value);
			break;
		case EU_TYPE:
			if (get_eu_value (pid->conv_ctx_pid, &raw_value))
				get_linear_value (pid->conv_ctx_pid, &raw_value);
			break;
	}

	tcs=(LIST)HeadOfList(pid->tlm_eus);
		/* Start at the head of our list. */

	if (tcs) {
			/* If there is more than one contextr state...  */

		eus=(tlm_eus *)tcs->current;
		while ((tcs != (void *) NULL) && eus && !end_loop) {
				/* Walk the linked list until we find the correct context 
					state table */

			if ((eus->context_low <= raw_value) && 
				(eus->context_high >= raw_value)) end_loop=-1;
			else {
				table_number++;
				tcs=tcs->next;
				eus=(tlm_eus *)tcs->current;
			}
		}
	}
}

return table_number;
}

