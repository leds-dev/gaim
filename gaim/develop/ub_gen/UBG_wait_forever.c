/*
@(#)  File Name: UBG_wait_forever.c  Release 1.7  Date: 07/03/25, 09:12:38
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_wait_forever.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles

         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
         8.1         mar03-2004  K. Dolinh       Add log_event, log_debug
         8.6         apr01-2004
         9.2A        feb18-2005                  add read_streams to main loop 
                     mar07-2005                  output CRC err msg only if DEBUG_CRC set
         9AP         mar16-2005                  Build 9A Prime
         11          Mar13-2007                  fix compile warning
         14.1        Jun23-2010  D. Niklewski    fix spurious warnings about "ignoring
                                                 unknown-shutdown msg from UBS" 

********************************************************************************
    Invocation:

        UBG_wait_forever()

    Parameters:

        NONE

********************************************************************************
    Functional Description:

        This routine does all the "wait forever" processing.

*******************************************************************************/

/**

PDL:
    IF a socket has requested service THEN
        IF the Update_Buffer_Sender socket exists and the Update_Buffer_Sender
         has sent data to this Update_Buffer_Generator THEN
            Read the data from the socket.
            IF the message is 4 bytes long THEN
                Compare the CRCs and write a message to the Debug Log based
                 upon a good or bad comarison.
                Clear the waiting-for-a-response flag.
            ELSEIF the message is a shutdown message THEN
                Request a program termination.
            ELSEIF the message length is less than zero THEN
                Request a program termination.
            ELSE
                Write a message to the debug log stating that the message is
                 unknown.
                IF the message length is a multiple of 4 THEN
                    For each group of 4 bytes, write a message to the debug log.
                ENDIF
            ENDIF
        ENDIF

        IF the Stream_Data_Receiver or Playback_Update_Buffer socket exists and
         there is data waiting to be read THEN
            Read the data from the socket.
            IF the message is a shutdown message THEN
                Request a program termination.
            ELSEIF the message length is less than zero THEN
                Request a program termination.
                Write a message to the debug log.
            ELSE
                Set the waiting-for-a-response flag.
                Calculate the checksum of the packet.
                Get the packet type.
                DOCASE which packet type?
                    CASE 1: The packet is a telemetry packet.
                        CALL UBG_process_tlm_frame
                    ENDCASE

                    CASE 2: The packet is a global variable packet.
                        CALL UBG_process_global_packet
                    ENDCASE

                    CASE 3: The packet is a pseudotelemetry packet.
                        CALL UBG_process_pseudo_packet
                    ENDCASE

                    CASE 4: The packet is a database name packet.
                        CALL UBG_process_database_packet
                        Clear the waiting-for-a-response flag.
                    ENDCASE

                    CASE 5: The packet is a GTACS name packet.
                        CALL UBG_process_gtacs_packet.
                        Clear the waiting-for-a-response flag.
                    ENDCASE

                    CASE 6: The packet is an end-of-data packet.
                        CALL UBG_process_eoh_eoi_packet
                    ENDCASE

                    ELSECASE: The packet is an unknown packet type.
                        Write an 'unknown packet type' message to the debug log.
                    ENDCASE
                ENDDO

                Return a CRC to the process that sent the above packet.
            ENDIF
        ENDIF
    ENDIF

**/

#include "UBG_structures.h"
    

/* global variables */
extern  int  debug_crc;

int UBG_wait_forever(void) {

char message[1024]={0};
    /* Error message */

int status;
    /* Status Flag */

int retval=0;
    /* Error code */

short temp_length;
    /* Packet Size */

char crc_text[12];
    /* Temporary CRC Storage */

int i;
    /* Counter */


/* If connected to UBS */
if (strlen(process_number)>0) {

    /* Save the current read_mask value */
    data_mask = read_mask;

    /* Any sockets requesting service? */
    status = select(/*FD_SETSIZE*/ nfds+1, &data_mask, NULL, NULL, &select_time);

    /* If socket has requested service... */
    if (status > 0) {

        /* If the socket exists */
        if (ubs_socket > 0) {

            /* And this is the socket the request is on */
            if (FD_ISSET(ubs_socket,&data_mask)) {

                /* Read up to 255 bytes from socket (normally 4 or 8 bytes) */
                message_length = net_read(ubs_socket,ub_buffer,-255);

                /* If msglen is 4 bytes, it must be a CRC value. */
                if (message_length == 4) {

                    if (debug_crc) {
                        /* If CRC does not match what was sent last, assume failure. */
                        if (ub_crc != *(int *)ub_buffer)
                            log_event("UB-CRC failure. expected=x%8X  computed=x%8X",ub_crc,*(int *)ub_buffer);
                        else /* CRC Validated */
                            log_event("UB-CRC validated. value=x%8X",ub_crc);
                    }
                    /* No more waiting once CRC has been validated. */
                    waiting_for_response=0;
                    
                } else if (message_length <= 0) {

                    /* If msg len <= zero, assume fatal error and shutdown UBG */
                    log_event_err("received UBS msg with invalid len=%i. will shutdown",message_length);
                    end_program = -1;
                    waiting_for_response = 0;
                    strcat(error_message,message);
                    /* Shut down the socket */

/*
                } else if (ub_buffer[0] == 'z' && message_length >= 8) {

                    if (strstr(ub_buffer,ERROR_MESSAGE_HEADER)) {
                        strcpy(error_message,ub_buffer);
                        log_event_err("received failure msg from UBS : %s",&error_message[8]);
                        end_program = -1;
                    } else if (strstr(ub_buffer,"zzzzzzzz")) {
                        strcpy(error_message,ub_buffer);
                        log_event_err("received shutdown msg from UBS : %s",&error_message[8]);
                        end_program = -1;
                    } else
                        log_event_err("ignoring unknown-shutdown msg from UBS : (%8s)",&ub_buffer[0]);

                } else if ((message_length%4) == 0)  {

                    waiting_for_response = 0;
                    sprintf(message,"Assuming a multiple CRC message. CRC values are: ");
                    for (i=0; i<message_length; i+=4) {
                        sprintf(crc_text," %8.8X",*(unsigned int *)(ub_buffer+i));
                        strcat(message,crc_text);
                    }
                    log_debug(message);
*/

                /******NEW (6/23/2010) ******/
                /* Need to check more of the characters to be sure it's either an error or a shutdown
                   message since two CRC messages could have come in with the first one just happening
                   to have the ascii code for 'z' in the first byte. */

                } else if (message_length >= 8) {

                    char *ptr;

                    /* Handle error message from UBS with 8-byte ERROR_MESSAGE_HEADER */
                    if (ptr = strstr(ub_buffer,ERROR_MESSAGE_HEADER)) {

                        log_event_err("received error msg from UBS : %s",(ptr+8));
                        end_program = -1;

                    /* Handle 8-byte shutdown message from UBS */
                    } else if (ptr = strstr(ub_buffer,"zzzzzzzz")) {

                        log_event_err("received shutdown msg from UBS : %8s",ptr);
                        end_program = -1;

                    /* Handle 2 or more 4-byte CRC messages from UBS */
                    } else if ((message_length%4) == 0)  {

                        /* If len is 4*N, it may be a set of CRC messages. should not happen */
                        waiting_for_response = 0;
                        sprintf(message,"Assuming a multiple CRC message. CRC values are: ");
                        for (i=0; i<message_length; i+=4) {
                            sprintf(crc_text," %8.8X",*(unsigned int *)(ub_buffer+i));
                            strcat(message,crc_text);
                        }
                        log_debug(message);

                    } else {
                        log_event_err("ignoring unknown message (length = %i) from UBS : (%8s)",message_length,&ub_buffer[0]);
                    }

                /******NEWEND******/

                } else {
                    log_event_err("received UBS msg with invalid len = %i",message_length);

                } /* endif message_length */

            } /* endif FD_ISSET */

        } /* endif ubs_socket */

    } /* endif status */

    /* If the socket exists */
    if (sdr_pub_socket>0 && !waiting_for_response) {

        /* And this is the socket the request is on */
        if (FD_ISSET(sdr_pub_socket,&data_mask)) {

            if (process_number_value<=MAX_SDR_PROCESSES) {
                    /* If SDR called UBG, the first two bytes
                        represents a packet length.  This code is
                        separate ffrom the PUB calling UIBG code since
                        Dwell data can have many short packets.  During
                        the ftp of the pseudo mnemonic file, the short
                        packets were getting backed up and UBG could
                        not recover from that problem.  Now it should
                        be able to. */

                /* Read the packet length. */
                message_length = net_read(sdr_pub_socket,pkt_buffer,2);
                temp_length = *(short *)pkt_buffer;

                /* Read the packet from SDR */
                message_length=net_read(sdr_pub_socket,pkt_buffer,temp_length);


            } else
                /* Read the packet from PUB */
                message_length=net_read(sdr_pub_socket,pkt_buffer,max_length);

            /* Read up to MAX_LENGTH bytes from the socket. */
            buffer_ptr = pkt_buffer;


            if (pkt_buffer[0]=='z' && message_length==8) {

                /* Handle 8-byte shutdown message from SDR or PUB */
                if (pkt_buffer[1]=='!') {
                    /* Failure of GTACS Stream */
                    end_program=-1;
                    retval=999;

                    if (run_mode==2)
                        log_event_err("received failure msg from SDR");
                    else if (run_mode==1)
                        log_event_err("received failure msg from PUB");

                } else {
                    /* GAS commanded end to archiving GTACS Stream */
                    end_program=-1;
                    if (run_mode==2)
                        log_event("received shutdown msg from SDR");
                    else if (run_mode==1)
                        log_event("received shutdown msg from PUB");

                }

            } else if (message_length <= 0) {
                /* If length <= 0, assume fatal error and shut down UBG. */
                if (run_mode==2)
                    log_event_err("received SDR msg with invalid len=%i. will shutdown",message_length);
                else if (run_mode==1)
                    log_event_err("received PUB msg with invalid len=%i. will shutdown",message_length);

                end_program=-1;
                waiting_for_response=0;
                /* Shut down the socket. */

            } else  {
                /* If not 8 bytes, message is a RAW frame or GLOBAL or PSEUDO data. */
                waiting_for_response = -1;
                /* Got a packet to send.  Do not respond to PUB
                   until UBS responds to our update buffer. */

                /* CRC Check */
                pkt_crc = GAIM_calculate_checksum(message_length,pkt_buffer);

                log_debug("received PUB/SDR msg:  len=%i  CRC=%8.8X  Type=%i",
                    message_length,pkt_crc,(long)(*(short *)pkt_buffer));

                /* Get the packet type */
                packet_type=*(short *)pkt_buffer;
                buffer_ptr+=2;

                switch(packet_type) {
                    case UBG_FRAME_PKT_NUMBER:      /* TLM */
                        UBG_process_tlm_frame(message_length,buffer_ptr);
                        saw_a_frame=-1;
                        break;

                    case UBG_GLOBAL_PKT_NUMBER:     /* GLOBAL */
                        UBG_process_global_packet(message_length,buffer_ptr);
                        break;

                    case UBG_PSEUDO_PKT_NUMBER:     /* PSEUDO */
                         UBG_process_pseudo_packet(DATATYPE_PB_PSEUDO,message_length,buffer_ptr);
                        break;

                    case UBG_DBASE_PKT_NUMBER:
                        retval=UBG_process_database_packet(message_length,buffer_ptr);
                        waiting_for_response=0;
                        break;

                    case UBG_GTACS_PKT_NUMBER:
                        UBG_process_gtacs_packet(message_length,buffer_ptr);
                        waiting_for_response=0;
                        break;

                    case UBG_EOH_EOI_PKT_NUMBER:
                        UBG_process_eoh_eoi_packet();
                        break;

                    default:    /* Unknown */
                        log_event_err("received unknown packet type=%i from SDR/PUB",
                            packet_type);
                        break;
                }

                if (atoi(process_number)>8)
                    net_write(sdr_pub_socket,(char *)&pkt_crc,4);
                    /* Return received CRC as validation. */
            }
        }
    }
} /* endif strlen(process_number)>0 */


/* If not running from SDR/PUB */
if (run_mode==0 || run_mode==2) {

    if ((status=read_streams(max_wait_time))!=STRM_SUCCESS) {
        /* Verify EPOCH connection */
        log_event_err("ERROR(%i) from read_streams: %s",status,stream_err_messages[0-status]);
        end_program=-1;
    }
}


/* Rest for 1 second, but only if not connected to UBS. */
if (strlen(process_number)==0) sleep(1);

return retval;
}
