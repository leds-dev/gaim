/*  
@(#)  File Name: UBG_init_stream.c  Release 1.4  Date: 07/03/25, 09:12:21
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_init_stream.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
	 8.4         mar18-2004  K. Dolinh	 Add log_event, log_debug

********************************************************************************
	Invocation:
        
		UBG_init_stream()
        
	Parameters:

		NONE

********************************************************************************
	Functional Description:

		This routine initializes the stream.
	
		NOTE: This routine is only used during testing.  It is not a routine 
		that is used under normal operations.  It is only used when the user
		wants to bypass the main GAIM software and feed data directly from an 
		EPOCH stream to the Update_Buffer_Sender (bypassing Stream_Data_Receiver
		and Playback_Update_Buffer)
	
*******************************************************************************/

/**

PDL:
	Get the current stream id.
	Connect to the stream.
	Request frame service from EPOCH for the stream.

**/


#include "UBG_structures.h"
	/* Globals and structures */

extern void UBG_new_frame_cb(struct client_frame *current_frame);
	/* Callback function for frame service */

void UBG_init_stream(void) {

	char message[1024];
		/* Error message */

	int status;
		/* Status Flag */


/* ************************************************************************
   * Get value of STRMID from the environment.  STREAMID is automatically *
   *     set by EPOCH when a stream is initialiazed.                      *
   ************************************************************************ */

if (strlen(stream_name)==0) {

	if (getenv("STREAMID")) strcpy(stream_name,getenv("STREAMID"));
		/* Get the Stream Name from the environment */
	else {
		log_event_err("env var STREAMID is not defined. exiting");
		exit(14);
	}
}


/* *************************************
* Attempt to connect to the stream. *
************************************* */

if (run_mode==0) {
	/* Test Mode */
	/* Attempt to connect to the requested stream */
	status=connect_stream(stream_name,network_timeout,&network_connection);
	if (status != STRM_SUCCESS) {
		/* If connect failed... */
		log_event_err("%s connect_stream error(%i): %s",
			stream_name,status,stream_err_messages[0-status]);
		disconnect_streams();
		exit(9);
	} else {
		log_event("connected to stream %s",stream_name);
	}

} 


/***************************************
* Request Frame Service for the stream *
****************************************/

if (run_mode==0) {
	/* Test Mode */
	/* Attempt to request Frame Service */
	status = request_frame(NULL, network_timeout, UBG_new_frame_cb, 0, &frame_ptr);
	if (status != STRM_SUCCESS) {
		log_event_err("request_frame error(%i): %s",status,stream_err_messages[0-status]);
		disconnect_streams();
		exit(10);
	}
}

}
