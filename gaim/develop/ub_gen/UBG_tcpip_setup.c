/*  
@(#)  File Name: UBG_tcpip_setup.c  Release 1.4  Date: 07/03/25, 09:12:37
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_tcpip_setup.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
	 8.4         mar18-2004  K. Dolinh	 Add log_event, log_debug

********************************************************************************
	Invocation:
        
		UBG_tcip_setup()
        
	Parameters:

		NONE

********************************************************************************
	Functional Description:

		This routine sets up the TCP/IP sockets needed for communication 
		between the UBG and PUB/SDR/UBS routines.
	
*******************************************************************************/

/**

PDL:
	Open a socket to the Update_Buffer_Sender.
	IF the Stream_Date_receiver spawned this particular Update_Buffer_Generator
	 process THEN
		Open a scoket to the Stream_Data_Receiver which spawned this process.
	ELSE
		Open a socket to the Playback_Update_Buffer that spawned this process.
	ENDIF

**/

#include "UBG_structures.h"
	/* Globals and structures */

void UBG_tcpip_setup(void) {

/* Error message */
char message[1024];
	
/* Status Flag */
int status;
	

/* If a process (service) number is given, then connect to the Update_Buffer_Sender */
if (strlen(process_number) > 0) {

	/* Request a connection from the UBS */
	status=net_call(NULL, ubs_service_name, &ubs_socket);
	if (status != 0) {
		/* Connect failed */
		sprintf(message,"connection to UBS failed status=%i",status);
		strcat(error_message,message);
		log_event_err(message);
		/* set flag to term process */
		end_program=-1;
		ubs_socket=-1;
	} else {
		/* set up the socket for read-write service */
		FD_SET(ubs_socket,&read_mask);
    if (ubs_socket > nfds) nfds = ubs_socket;
		log_event("connected to Update_Buffer_Sender");
	}

	if (run_mode == 2) {
	
		/* UBG was called by SDR, make sdr connection */
		status = net_call(NULL, sdr_service_name, &sdr_pub_socket);
		if (status != 0) {
			/* Connect to SDR failed */	
			sprintf(message,"connection to SDR failed status=%i",status);
			strcat(error_message,message);
			log_event_err(message);
			/* set flag to term process */
			end_program = -1;
			sdr_pub_socket = -1;
		} else {
			/* set up the socket for read-write service */
			FD_SET(sdr_pub_socket,&read_mask);
      if (sdr_pub_socket > nfds) nfds = sdr_pub_socket;
			log_event("connected to Stream_Data_Receiver");
		}

	} else if (run_mode == 1) {
	
		/* UBG was called by PUB, make pub connection */
		status = net_call(NULL, pub_service_name, &sdr_pub_socket);
		if (status != 0) {
			/* Connect to PUB failed */	
			sprintf(message,"connection to PUB failed status=%i",status);
			strcat(error_message,message);
			log_event_err(message);
			/* set flag to term process */
			end_program = -1;
			sdr_pub_socket = -1;
		} else {
			/* set up the socket for read-write service */
			FD_SET(sdr_pub_socket,&read_mask);
      if (sdr_pub_socket > nfds) nfds = sdr_pub_socket;
			log_event("connected to Playback_Update_Buffer");
		}
		
	}

} else {
	log_event_err ("tcpip_setup: no process number specified");	
}

}
