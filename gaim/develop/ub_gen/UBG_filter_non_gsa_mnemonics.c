/*  
@(#)  File Name: UBG_filter_non_gsa_mnemonics.c  Release 1.10  Date: 07/03/25, 09:12:18
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_filter_non_gsa_mnemonics.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        12/00       S. Scoles       Initial creation
	 8.4         mar18-2004  K. Dolinh	 Add log_event, log_debug
	 
********************************************************************************
	Invocation:
        
		retval=UBG_filter_non_gsa_mnemonics(int number_of_points, 
			char *output_name)
        
	Parameters:

		retval - O
			-1=Error, 0=No Error

		number_of_points - I
			How many points are in this DB?

		output_name - I
			The name of the output file in which the indices of mnemonics to 
			keep is written.

********************************************************************************
	Functional Description:

		This routine creates a list of mnemonics that either need to be 
		forwarded to the GSA or should not be forwarded to the GSA.
	
*******************************************************************************/

/**

PDL: 
	Determine the mnemonic that all frame formats are based upon.

	Search the whole DB for mnemonics that are of frame format 0x00 and 0x0F 
	 (PCM and SXI frames).
	Search the whole DB for mnemonics that depend on the previously found 
	 PCM and SXI mnemonics.  This includes recursive dependencies.

	Search the whole DB for mnemonics that are of frame format 0x0B (Wideband 
	 Data frames).
	Search the whole DB for mnemonics that depend on the previously found 
	 mnemonics when those mnemonics are of values 0x01 and 0x02 (WBI and WBS 
	 Frames).  
	Search the whole DB for mnemonics that depend on the previously found 
	 WBI and WBS mnemonics.  This includes recursive dependencies.

**/

#include "UBG_structures.h"

int UBG_filter_non_gsa_mnemonics(int number_of_points, char *output_name) {

	char message[1024];
		/* Debug messages */

	int i,j,k;
		/* Counter */

	char frame_format_mnemonic[MNEMONIC_SIZE];
		/* Starting menmonic */

	int current_layer_count=0,layer_count=0;
		/* How many mnemonics do we need to check? */

	char **check_list,**db_mnemonic_names;
	int *check_mode;
	int *db_index_numbers,*db_mode_numbers,*db_mnem_to_gsa;
		/* List of mnemonics and add additional information */

	LIST tlm_location_list;
	struct tlm_location *tlm_loc;
		/* Needed to check all location records */

	FILE *output_file;
		/* Pointer to the output file */

	int retval=0;
		/* Return Value */

	int matching_location;
		/* Location of a match */

	int done;
		/* -1=Done WHILE, 0=Not Done While */


db_mnem_to_gsa=(int *)calloc(number_of_points,sizeof(int));
db_index_numbers=(int *)calloc(number_of_points,sizeof(int));
db_mode_numbers=(int *)calloc(number_of_points,sizeof(int));
db_mnemonic_names=(char **)calloc(number_of_points,sizeof(char *));
check_list=(char **)calloc(number_of_points,sizeof(char *));
check_mode=(int *)calloc(number_of_points,sizeof(int));
for (i=0; i<number_of_points; i++) {
	check_list[i]=(char *)calloc(MNEMONIC_SIZE,sizeof(char));
	db_mnemonic_names[i]=(char *)calloc(MNEMONIC_SIZE,sizeof(char));
}

strcpy(frame_format_mnemonic, 
	stream_def->dbhead->sc_fmt->sc_format->frame_fmt);
		/* Which mnemonic is the main context mnemonic? */

log_debug("Frame Format Mnemonic = %s",frame_format_mnemonic);


/* *************************************
* Handle Format Codes 0x00 and 0x0F *
*       (PCM and SXI Frames)        *
************************************* */

for (i=0; i<number_of_points; i++) {
	strcpy(db_mnemonic_names[i], ((point *)(stream_def->dbhead->
		tlm_head->point_array[i]))->tlm_point->tlm_mnemonic);
	db_index_numbers[i]=((point *)(stream_def->dbhead->
		tlm_head->point_array[i]))->tlm_point_index;
	db_mnem_to_gsa[i]=0;
		/* Initialize the list of mnemonics and assume none go to the 
			GSA. */

	tlm_location_list=(LIST)HeadOfList(((point *)(stream_def->dbhead->
		tlm_head->point_array[i]))->tlm_locations);

	while (tlm_location_list!=(void *)NULL && db_mnem_to_gsa[i]==0) {

		tlm_loc=(struct tlm_location *)(tlm_location_list->current);
			/* Get the current item. */

		if (strcmp(frame_format_mnemonic,tlm_loc->context_name)==0 && 
				(tlm_loc->context_value==0 || tlm_loc->context_value==15)) {
					/* If this mnemonic is part of a PCM or SXI frame, 
						keep it. */

			db_mnem_to_gsa[i]=-1;
			db_mode_numbers[i]=tlm_loc->context_value;
			strcpy(check_list[layer_count],db_mnemonic_names[i]);
			check_mode[layer_count]=tlm_loc->context_value;
			layer_count++;
				/* Add the mnemonic to the list to check "recursively" */
		}

		tlm_location_list=tlm_location_list->next;
			/* Go to the next item in the list */
	}
}
GAIM_quicksort_char(layer_count,check_list,check_mode);

done=0;
	/* Enter the loop */

while (!done) {
		/* While we are still adding points to the GSA list */

	current_layer_count=layer_count;
		/* Save the current size of the check array */

	done=-1;
		/* Assume no more points to be added */

	for (j=0; j<number_of_points; j++) {
			/* Loop through the whole db list */

		if (!db_mnem_to_gsa[j]) {
				/* If the current mnemonic is not already being sent */

			tlm_location_list=(LIST)HeadOfList(((point *)(stream_def->
				dbhead->tlm_head->point_array[j]))->tlm_locations);

			while (tlm_location_list!=(void *)NULL && 
					db_mnem_to_gsa[j]==0) {
						/* Loop through the telemetry locations list */

				tlm_loc=(struct tlm_location *)(tlm_location_list->current);
					/* Get the current item. */

				matching_location=GAIM_binarysearch_char(
					current_layer_count,check_list,tlm_loc->context_name);

				if (matching_location!=-1) {
						/* If a match was found */

					strcpy(check_list[layer_count],db_mnemonic_names[j]);
					check_mode[layer_count]=check_mode[matching_location];
					layer_count++;
						/* Add the mnemonic to the check list */

					db_mnem_to_gsa[j]=-1;
					db_mode_numbers[j]=check_mode[matching_location];
						/* Send this mnemonic to the GSA */

					done=0;
						/* Since a match was found, need to check again */
				}

				tlm_location_list=tlm_location_list->next;
					/* Go to the next item in the list */
			}
		}
	}

	GAIM_quicksort_char(layer_count,check_list,check_mode);
		/* Resort the check list. */
}

/* ***************************
* Handle Format Code 0x0B *
*  (WBI and WBS Frames)   *
*************************** */

for (i=0; i<number_of_points; i++) {
		/* Loop through the list of mnemonics for find all modes that 
			match 0x0B */

	tlm_location_list=(LIST)HeadOfList(((point *)(stream_def->dbhead->
		tlm_head->point_array[i]))->tlm_locations);

	while (tlm_location_list!=(void *)NULL && db_mnem_to_gsa[i]==0) {

		tlm_loc=(struct tlm_location *)(tlm_location_list->current);
			/* Get the current item. */

		if (strcmp(frame_format_mnemonic,tlm_loc->context_name)==0 && 
				tlm_loc->context_value==11) {
					/* If this mnemonic is part of a PCM or SXI frame, 
						keep it. */

			db_mnem_to_gsa[i]=-1;
			strcpy(check_list[layer_count],db_mnemonic_names[i]);
			layer_count++;
				/* Add the mnemonic to the list to check "recursively" */
		}

		tlm_location_list=tlm_location_list->next;
			/* Go to the next item in the list */
	}
}
GAIM_quicksort_char(layer_count,check_list,0);

current_layer_count=layer_count;

for (j=0; j<number_of_points; j++) {
		/* Loop through the whole db list */

	if (!db_mnem_to_gsa[j]) {
			/* If the current mnemonic is not already being sent */

		tlm_location_list=(LIST)HeadOfList(((point *)(stream_def->dbhead->
			tlm_head->point_array[j]))->tlm_locations);

		while (tlm_location_list!=(void *)NULL && db_mnem_to_gsa[j]==0) {

			tlm_loc=(struct tlm_location *)(tlm_location_list->current);
				/* Get the current item. */

			matching_location=GAIM_binarysearch_char(current_layer_count,
				check_list,tlm_loc->context_name);
					/* Find the context name in our check list */

			if (matching_location!=-1 && (tlm_loc->context_value==1 || 
					tlm_loc->context_value==2)) {
						/* If the current mnemonic we are checking 
							matches the context name of any mnemonic 
							in the db */

				strcpy(check_list[layer_count],db_mnemonic_names[j]);
				layer_count++;
					/* Add the mnemonic to the check list */

				db_mnem_to_gsa[j]=-1;
				db_mode_numbers[j]=11;
					/* Send this mnemonic to the GSA */
			}

			tlm_location_list=tlm_location_list->next;
					/* Go to the next item in the list */
		}
	}
}
GAIM_quicksort_char(layer_count,check_list,0);

done=0;
	/* Enter the loop */

while (!done) {
		/* While we are still adding points to the GSA list */

	current_layer_count=layer_count;
		/* Save the current size of the check array */

	done=-1;
		/* Assume no more points to be added */

	for (j=0; j<number_of_points; j++) {
			/* Loop through the whole db list */

		if (!db_mnem_to_gsa[j]) {
				/* If the current mnemonic is not already being sent */

			tlm_location_list=(LIST)HeadOfList(((point *)(stream_def->
				dbhead->tlm_head->point_array[j]))->tlm_locations);

			while (tlm_location_list!=(void *)NULL && 
					db_mnem_to_gsa[j]==0) {
						/* Loop through the telemetry locations list */

				tlm_loc=(struct tlm_location *)(tlm_location_list->current);
					/* Get the current item. */

				matching_location=GAIM_binarysearch_char(
					current_layer_count,check_list,tlm_loc->context_name);

				if (matching_location!=-1) {
						/* If a match was found */

					strcpy(check_list[layer_count],db_mnemonic_names[j]);
					layer_count++;
						/* Add the mnemonic to the check list */

					db_mnem_to_gsa[j]=-1;
					db_mode_numbers[j]=11;
						/* Send this mnemonic to the GSA */

					done=0;
						/* Since a match was found, need to check again */
				}

				tlm_location_list=tlm_location_list->next;
					/* Go to the next item in the list */
			}
		}
	}

	GAIM_quicksort_char(layer_count,check_list,0);
		/* Resort the check list. */
}


/* **************************
* Create Index List File *
************************** */

for (i=0; i<number_of_points; i++) 
	if (db_mnem_to_gsa[i]) mnemonic_to_gsa_count++;
		/* Count the GSA mnemonics */

if (mnemonic_to_gsa_count>0) {
	gsa_index_list=(int *)calloc(mnemonic_to_gsa_count,sizeof(int));
	gsa_index_mode=(int *)calloc(mnemonic_to_gsa_count,sizeof(int));
	mnemonic_to_gsa_count=0;
	for (i=0; i<number_of_points; i++) if (db_mnem_to_gsa[i]) {
		gsa_index_list[mnemonic_to_gsa_count]=db_index_numbers[i];
		gsa_index_mode[mnemonic_to_gsa_count]=db_mode_numbers[i];
		mnemonic_to_gsa_count++;
	}
}
	/* Create an array of the mnemonics */

GAIM_quicksort_int(mnemonic_to_gsa_count,gsa_index_list,gsa_index_mode);
	/* Sort the list of indices */

//output_file=fopen(output_name,"wb");
output_file=fopen("/home/gtacsops/g13_normal_20200701.gaim","wb");
if (output_file) {
	fwrite(&mnemonic_to_gsa_count,sizeof(int),1,output_file);
	if (mnemonic_to_gsa_count>0) {
		fwrite(gsa_index_list,sizeof(int),mnemonic_to_gsa_count,
			output_file);
		fwrite(gsa_index_mode,sizeof(int),mnemonic_to_gsa_count,
			output_file);
	}
	fclose(output_file);
		/* Write the file. */
} else {
	sprintf(message,"error opening gsa index file %s",output_name);
	log_event_err(message);
	strcat(error_message,message);
	retval=-1;
}

free(gsa_index_list);
gsa_index_list=0;
free(gsa_index_mode);
gsa_index_mode=0;
mnemonic_to_gsa_count=0;
	/* Free up the memory, since this file will be read and memory 
		allocated by UBG_initialize_epoch */

free(db_mnem_to_gsa);
free(db_index_numbers);
free(db_mode_numbers);
for (i=0; i<number_of_points; i++) {
	free(check_list[i]);
	free(db_mnemonic_names[i]);
}
free(db_mnemonic_names);
free(check_list);

return retval;

}
