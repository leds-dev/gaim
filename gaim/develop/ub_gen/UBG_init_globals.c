/*  
@(#)  File Name: UBG_init_globals.c  Release 1.7  Date: 07/03/25, 09:12:20
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_init_globals.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
	 8.4         mar18-2004  K. Dolinh	 Add log_event, log_debug
	 8.B.2       sep28-2004                  process global vars PCM1_RATE, PCM2_RATE
	 8.B.3       oct14-2004                  check stream_name
	 9AP	     mar16-2005		 	 Build 9A Prime

********************************************************************************
	Invocation:
        
		UBG_init_globals()
        
	Parameters:

		NONE

********************************************************************************
	Functional Description:

		This routine grabs the initial values of the necessary EPOCH globals.
		
		run_mode = 1   means spawned by PUB
		         = 2   means spawned by SDR
	
*******************************************************************************/

/**

PDL:
	Lookup the PID of the SPACECRAFT_ID variable.
	Get the current value of SPACECRAFT_ID.

	Lookup the PID of the STREAM_DB variable.
	Get the current value of STREAM_DB.

	Lookup the PID of the TELEMETRY_RATE variable.
	Get the current value of TELEMETRY_RATE.

	Lookup the PID of the STREAM_SOURCE variable.
	Get the current value of STREAM_SOURCE.

	Lookup the PID of the SPACECRAFT_ID variable.
	Get the current value of SPACECRAFT_ID.

	Lookup the PID of the GAIM_FORMAT_RTxx variable (xx=current spacecraft id).
	Get the current value of GAIM_FORMAT_RTxx.
	IF the getting of the value of GAIM_FORMAT_RTxx worked THEN
		Set the new value to 'No Frames Seen'
	ENDIF

**/

#include "strmutil.h"
#include "gv_util.p"
#include "UBG_structures.h"
	
/* Callback routine for frame service */
extern void UBG_new_frame_cb(struct client_frame *current_frame);
	
/* callback routine for global variables */
static void UBG_global_point_cb (client_point *cpid);

#define  ID_PCM1_RATE   1
#define  ID_PCM2_RATE   2

/* global vars for tlm rate */
extern int pcm1_rate;
extern int pcm2_rate;

gvar	*gvar_pcm1_rate;
gvar	*gvar_pcm2_rate;
client_point *pid_pcm1_rate;
client_point *pid_pcm2_rate;

void UBG_init_globals(void) {

	char message[1024];
		/* Error message */

	int status;
		/* Status Flag */

	int i;
		/* Counter */

	char global_name[32];
		/* Global variable name */

	char ground_stream_name[32];
		/* The name of the ground stream */


/* ****************************************************************
   * Get value of SCID from first the environment, then from a GV *
   **************************************************************** */
   
/* Is SCID defined yet? */
if (strlen(scid)==0 && run_mode==0) {
		
	/* Get the S/C Name from the environment */
	if (getenv("SPACECRAFT_ID")) 
		strcpy(scid,getenv("SPACECRAFT_ID"));			
	else {
		log_event_err("env var SPACECRAFT_ID is not defined");
		if (run_mode!=0) exit(19);
	}
}

if (strlen(scid)==0 && run_mode==0) {
		/* No SCID */
	/* Attempt to get the pid of the SPACECRAFT_ID global */
	sprintf(global_name,"%s:SPACECRAFT_ID",stream_name);
	status = look_up_point(stream_name,network_timeout,&spacecraft_id_pid);
	if (status != STRM_SUCCESS) {
		log_event_err("SPACECRAFT_ID look_up_point error=%i: %s",status,stream_err_messages[0-status]);

	} else {
		/* Get current value of SPACECRAFT_ID */
		status = poll_point(spacecraft_id_pid);
		if (status != STRM_SUCCESS) {
			log_event_err(" SPACECRAFT_ID poll_point error=%i: %s",status,stream_err_messages[0-status]);
		}
		/* Get the current SCID status */
		sprintf(scid,"GOES-%i",*(int *)spacecraft_id_pid->value);
		update_buffer.data_header.scid = *(int *)spacecraft_id_pid->value;	
	}
}


/* *************************************
* Get value of STREAM_DB  from a GV *
************************************* */

if (strlen(database_name)==0 && run_mode==0) {
	/* Is the database name defined yet. */
	/* Attempt to get the pid of the STREAM_DB global */
	sprintf(global_name,"%s:STREAM_DB",stream_name);
	status = look_up_point(stream_name,network_timeout,&stream_db_pid);
	if (status!=STRM_SUCCESS) {
		/* If point lookup failed... */
		log_event_err("STREAM_DB look_up_point error=%i: %s",status,stream_err_messages[0-status]);
	} else {
		/* Get current value of STREAM_DB */
		status = poll_point(stream_db_pid);
		if (status!=STRM_SUCCESS) {
			/* If point polling failed... */
			log_event_err("STREAM_DB poll_point error=%i: %s",status,stream_err_messages[0-status]);
			disconnect_streams();
		}
		/* Get the current archive status */
		strcpy(database_name,(char *)stream_db_pid->value);		
	}
	
} else if (strlen(database_name)==0 && strlen(process_number)==0) {
	log_event_err("no database name given");

}

/* **********************************************
* Get value of TELEMETRY_RATE from whereever *
********************************************** */

if (current_telemetry_rate==-1 && run_mode==0) {
	/* Is the telemetry rate defined yet? */
	/* Attempt to get the pid of the TELEMETRY_RATE global */
	sprintf(global_name,"%s:TELEMETRY_RATE",stream_name);
	status=look_up_point(stream_name, network_timeout, &telemetry_rate_pid);
	if (status!=STRM_SUCCESS) {
		/* Did the point lookup fail? */
		log_event_err("TELEMETRY_RATE look_up_point error=%i: %s",status,stream_err_messages[0-status]);
	} 
} else if (current_telemetry_rate==-1) {
	log_event_err("no telemetry rate given");
}

/* look up pointers to global vars PCMx_RATE in local shared memory */
sprintf(global_name,"PCM1_RATE");
gvar_pcm1_rate = (gvar *)gv_id(stream_def->dbhead,global_name);
if (!gvar_pcm1_rate) log_event_err("gv_id error for global var (%s)",global_name);

sprintf(global_name,"PCM2_RATE");
gvar_pcm2_rate = (gvar *)gv_id(stream_def->dbhead,global_name);
if (!gvar_pcm2_rate) log_event_err("gv_id error for global var (%s)",global_name);

/* if archiving realtime tlm, request updates for global vars PCMx_RATE from stream */
if (run_mode == 2)	{

	sprintf(global_name,"%s:PCM1_RATE",stream_name);
	status = request_point (global_name,
				NO_CONVERSION,	
				CHANGED_SAMPLES,
				ASYNC_SERVICE,
				1,				
				network_timeout,
				UBG_global_point_cb,
				(void *) ID_PCM1_RATE,	
				&pid_pcm1_rate);	
	if (status != STRM_SUCCESS) 
		log_event_err("request_point(%s) error=%i: %s",global_name,status,stream_err_messages[0-status]);
		
	sprintf(global_name,"%s:PCM2_RATE",stream_name);
	status = request_point (global_name,
				NO_CONVERSION,	
				CHANGED_SAMPLES,
				ASYNC_SERVICE,
				1,				
				network_timeout,
				UBG_global_point_cb,
				(void *) ID_PCM2_RATE,	
				&pid_pcm2_rate);	
	if (status != STRM_SUCCESS) 
		log_event_err("request_point(%s) error=%i: %s",global_name,status,stream_err_messages[0-status]);
		
}

status = read_streams(max_wait_time);
if (run_mode == 2 && status!= STRM_SUCCESS) 
	log_event_err("init_globals: read_streams err=%i : %s",status,stream_err_messages[0-status]);


	
/* ******************************************
* Get value of STREAM_SOURCE from wherever *
****************************************** */

if (run_mode==0) {
	/* Attempt to get the pid of the STREAM_SOURCE telemetry point */
	sprintf(global_name,"%s:STREAM_SOURCE",stream_name);
	status = look_up_point(stream_name,network_timeout,&frame_type_pid);
	if (status!=STRM_SUCCESS) {
		/* Did the point lookup fail? */	
		log_event_err("STREAM_SOURCE look_up_point error=%i: %s",status,stream_err_messages[0-status]);
	}
} 

/* ************************************************
* Get point ID of the GAIM_FORMAT_RTxx globals *
************************************************ */

/* Do only for SDR */
if (run_mode == 2) {

	strcpy(ground_stream_name,stream_name);
	ground_stream_name[2]=0;
	strcat(ground_stream_name,"_ge");
		/* Get the name of the ground stream */

	sprintf(global_name,"%s:GAIM_FORMAT_RT%2.2i",ground_stream_name,spacecraft_id_value);
	status = look_up_point(global_name,network_timeout,&gaim_format_rt_pid);	
	if (status != STRM_SUCCESS) {
		/* Did the point lookup fail? */	
		log_event_err("%s look_up_point error=%i: %s",global_name,status,stream_err_messages[0-status]);
		
	} else {

		if (process_number_value<8 && (process_number_value%2)==1) {
		
			/* Initialize the point, but only if we are the PRIME-PRIME 
			   stream (channels 1, 3, 5, and 7). */

			strcpy(gaim_format_rt_value,"No Frames Seen");
			strcpy((char *)gaim_format_rt_pid->value,gaim_format_rt_value);
			gaim_format_rt_pid->status_bits = 0;
			status = set_point(gaim_format_rt_pid);
			if (status!=STRM_SUCCESS) {
				log_event_err("%s set_point error=%i: %s",global_name,status,stream_err_messages[0-status]);
			}

			status = flush_sets();
			if (status!=STRM_SUCCESS) {
				log_event_err("flush_sets error=%i: %s",status,stream_err_messages[0-status]);
			}

		} else gaim_format_rt_value[0] = 0;
	}
}

}

/*****************************
 global point callback routine
******************************/
static void UBG_global_point_cb (client_point *cpid)
{
int	user_data;
long	rate;

user_data = (int)(long) cpid->user_data;
	
switch (user_data) {

	case ID_PCM1_RATE:
		/* set value of global in shared memory */
		if (gvar_pcm1_rate) {
			rate = 0;
			set_gv(stream_def->dbhead,gvar_pcm1_rate,*(long *)cpid->value);
			get_gv(gvar_pcm1_rate,&rate);
			pcm1_rate = rate;
			log_event("PCM1_RATE = %d   pcm1_rate = %d\n", *(u_long *)cpid->value,pcm1_rate);
		}
		break;
		
	case ID_PCM2_RATE:
		/* set value of global in shared memory */
		if (gvar_pcm2_rate) {
			rate = 0;
			set_gv(stream_def->dbhead,gvar_pcm2_rate,*(long *)cpid->value);
			get_gv(gvar_pcm2_rate,&rate);
			pcm2_rate = rate;
			log_event("PCM2_RATE = %d   pcm2_rate = %d\n", *(u_long *)cpid->value,pcm2_rate);
		}		
		break;

	}

}

