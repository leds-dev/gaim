/*  
@(#)  File Name: UBG_structures.h  Release 1.12  Date: 07/06/04, 16:42:50
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         HEADER          :       UGB_structures.h
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles   Initial creation
	     8.B1        sep22-2004  K. Dolinh   - changed frame types for SXI,WDB
	                                         - removed WBI_SUBTYPE, WBS_SUBTYPE
	                                         - add types MRSS...
	     10A         Sep09-2005              - add array gsa_tag
	                                         - remove include archive.h 
		 11			 Mar03-07		 		 - add gva record 
		 									 - removed array data_type_codes[]  										 					 
		 12			 May31-07		 		 - add 2 new SXI frame types
		 
********************************************************************************
         Functional Description:

         This header file contains structures used by the routines in 
         the Update_Bufffer_Generator process.
	
*******************************************************************************/

#include <string.h>

/* Epoch database structures and routines */
#include "db.h"
#include "db_util.p"
#include "net_util.h"

/* EPOCH Linked-List header */
#include "list.h"

/* EPOCH Stream Utility Libraries */
#include "strmutil.p"

/* Used for multiple programs */
#include "GAIM_update_buffer_defines.h"
	
/* structure of pseudo record in *.def checkpoint file */
struct Pseudo_struct {
	char mnemonic[MNEMONIC_SIZE];
	int flags;
	double value;
}; 
typedef struct Pseudo_struct Pseudo_struct;

/* structure of globalvar record in *.gva checkpoint file */
struct GVA_struct {
	char	name[MNEMONIC_SIZE];	/* global variable name */
	int		status;			/* data quality */
	double	value;			/* data value */
	int		tag;			/* id (tag from gsa file) */
};
typedef struct GVA_struct GVA_struct;

#include "UBG_prototypes.h"

#ifdef UBG_MAIN
#define MAIN

#endif

/* EPOCH Archive globals */

/* How often (in seconds) to ftp pseudo checkpoint file */
#define PSEUDOTLM_FTP_RATE 16

/* Seconds to wait before sending PCM2 if PCM1 drops out */
#define PCM1_TIMEOUT 10
	

/* FRAME_TYPE constants are same as STREAM_SOURCE on GTACS */
#define PCM1_FRAME_TYPE        0x0
#define GROUND_FRAME_TYPE      0x1
#define PCM2_FRAME_TYPE        0x2
#define MICRO_DWELL_FRAME_TYPE 0x3
#define IMGWB_FRAME_TYPE       0x4
#define SNDWB_FRAME_TYPE       0x5
#define SPSRNG_FRAME_TYPE      0x6

#define SPSSTAT_FRAME_TYPE     0xA

#define MRSSADS_FRAME_TYPE     0xC
#define MRSSAVS_FRAME_TYPE     0xD
#define MRSSHK_FRAME_TYPE      0x10
#define MRSSCOMP_FRAME_TYPE    0x11
#define MRSSOBS_FRAME_TYPE     0x12
#define MRSSIMG_FRAME_TYPE     0x13
#define SXIMEM_FRAME_TYPE      0x14
#define SXI_SEQ_FRAME_TYPE     0x15
#define SXI_HEALTH_FRAME_TYPE  0x16
#define OATSSTAT_FRAME_TYPE    0x36

#define AGC_FRAME_TYPE         13    /* agc to be deleted ? */

/* ... */
#define SPS_MRSS_FRAME_BYTE 14
#define SPS_MRSS_SOURCE_BYTE 4
#define WBI_SUBTYPE 1
#define WBS_SUBTYPE 2

#define NORMAL_FRAME 0
#define DWELL_FRAME 1

#define MAX_PACKET_BUFFER_SIZE 32768

#define NORMAL_DWELL_BYTE 5
#define NORMAL_DWELL_BIT_MASK 0x02

#define GSA_TAG_MAX		20000

/* ****************************
   * Non-Dwell Message Header *
   **************************** */

#define UBG_NON_DWELL_MSG_ID	103
#define UBG_PROCESS_NAME		"Update_Buffer_Sender"

struct ubg_non_dwell_message_header {
	int message_size;		/* Length of message */
	short class_id;			/* 102 */
	short message_number; 	/* Incremented each new update buffer */
	short node_name_len;	/* Chars in node_name */
	char *node_name;		/* The name of the node which is sending the update 
								buffer */
	short process_name_len;	/* Chars in process_name */
	char *process_name;		/* Update_Buffer_Sender */
};


/* *************************
   * Non-Dwell Data Header *
   ************************* */

#define UBG_SGTACS01_CODE 1
#define UBG_SGTACS02_CODE 2
#define UBG_SGTACS03_CODE 3
#define UBG_SGTACS04_CODE 4
#define UBG_SGTACS05_CODE 5
#define UBG_CGTACS01_CODE 6
#define UBG_CGTACS02_CODE 7
#define UBG_CGTACS03_CODE 8
#define UBG_GGTACS01_CODE 9
#define UBG_GGTACS02_CODE 10
#define UBG_SARC1_CODE    11
#define UBG_LAST_GTACS    11

static char gaim_tlm_mode_names[11][20]={"Normal Format 1","Normal Format 2",
	"Normal Format 3","Unused","Unused","Unused","Unused","Unused",
	"Dwell Format 1","Dwell Format 2","Dwell Format 3"};

static char gtacs_list[UBG_LAST_GTACS][12]={"sgtacs01","sgtacs02","sgtacs03",
	"sgtacs04","sgtacs05","cgtacs01","cgtacs02","cgtacs03","ggtacs01",
	"ggtacs02","sarc01"};
static short gtacs_array[UBG_LAST_GTACS]={UBG_SGTACS01_CODE,UBG_SGTACS02_CODE,
	UBG_SGTACS03_CODE,UBG_SGTACS04_CODE,UBG_SGTACS05_CODE,UBG_CGTACS01_CODE,
	UBG_CGTACS02_CODE,UBG_CGTACS03_CODE,UBG_GGTACS01_CODE,UBG_GGTACS02_CODE,
	UBG_SARC1_CODE};

#define UBG_NO_CODE       -1
#define UBG_PCM1_CODE     20
#define UBG_PCM2_CODE     20
#define UBG_DWELL_CODE    21
#define UBG_WBI_CODE      22
#define UBG_WBS_CODE      23
#define UBG_AGC_CODE      24
#define UBG_SXI_CODE      25
#define UBG_PSEUDO_CODE   26
#define UBG_NUM_DATA_TYPES 7

/* static data_type_codes[UBG_NUM_DATA_TYPES]={UBG_PCM1_CODE,UBG_PCM2_CODE,
	UBG_WBI_CODE,UBG_WBS_CODE,UBG_AGC_CODE,UBG_SXI_CODE,UBG_PSEUDO_CODE};
*/

#define UBG_SOURCE_PCM1			0x0000
#define UBG_SOURCE_PCM2			0x0001
#define UBG_NORMAL_DATA			0x0000
#define UBG_DWELL_DATA			0x0002
#define UBG_IN_ELCIPSE			0x0010
#define UBG_BAD_QUALITY			0x0020
#define UBG_PRIME				0x0000
#define UBG_NON_PRIME			0x0040
#define UBG_AUTO_MERGE			0x0080
#define UBG_MANUAL_MERGE		0x00C0
#define UBG_DATA_TYPE_MASK      0x00C0
#define UBG_EOH_EOI				0x0100
#define UBG_SIMULATED_STREAM	0x0200
#define UBG_REAL_STREAM			0x0000
#define UBG_PCM_1KBPS			0x0000
#define UBG_PCM_4KBPS			0x0400
#define UBG_NON_PCM_RATE_MASK	0x7800

#define FORCED_MAX_DB_NAME_LEN 32

struct ubg_non_dwell_data_header {
	short data_size;			/* The size in bytes of the data header plus 
									the data body */
	char gtacs_name;			/* Single byte representing which GTACS node 
									name */
	char scid;					/* Single byte representing the S/C id that 
									appears in a configuration on the TCS */
	char frame_type;			/* Frame Type */
	char minor_frame_number;	/* Value for the minor frame number */
	short database_name_len;	/* length of database_name */
	char *database_name;		/* The name of the telemetry database.  
									No path */
	short year;					/* 4-digit Year of Ground Receipt Time */
	short day;					/* Julian Day of the Ground Receipt Time */
	int milliseconds;			/* Milliseconds since start-of-day from GRT */
	unsigned short status;		/* Status Bits */
};

#define SIZE_DATA_HEADER_BASE (4*sizeof(short)+4*sizeof(char)+sizeof(int))
	/* DO NOT COUNT THE DBNAMELEN SHORT INT!  It is not included in the 
		output. Don't include data_size itself */


/* ***********************
   * Non-Dwell Data Body *
   *********************** */

#define UBG_CONTEXT_MASK			0x001F
#define UBG_ONE_BYTE_DATA			0x0000
#define UBG_TWO_BYTE_DATA			0x0020
#define UBG_FOUR_BYTE_DATA			0x0040
#define UBG_EIGHT_BYTE_DATA			0x0060
#define UBG_DATA_SIZE_MASK			0x0060
#define UBG_IEEE_FLOAT				0x0080
#define UBG_INT						0x0000
#define UBG_QUESTIONABLE_QUALITY	0x0100
#define UBG_DELTA_VIOLATION			0x0200
#define UBG_RED_OR_YELLOW_VIOLATION	0x0400
#define UGB_YELLOW_VIOLATION		0x0000
#define UGB_RED_VIOLATION			0x0800
#define UGB_LOW_VIOLATION			0x0000
#define UGB_HIGH_VIOLATION			0x1000
	/* Status Bit Masks */

#define UBG_DATA_SIZE_SHIFT_BITS	5
#define UBG_GET_DATA_MASK(x) ((x&UBG_DATA_SIZE_MASK)>>UBG_DATA_SIZE_SHIFT_BITS)

#define UBG_MASK_TO_BYTE			{1,2,4,8}
#define UBG_MASK_TO_BYTE_SIZE 		4
	/* Conversion Macros */

struct ubg_non_dwell_data_body {
	unsigned short mnemonic_index;	/* DB Mnemonic Index for this mnemonic */
	unsigned short status_flags;	/* Data status flags, including data size */
	unsigned short time_offset;		/* Milliseconds since start of frame */
	char raw_value[16];				/* Raw value */
};

#define SIZE_DATA_BODY_BASE (2*sizeof(unsigned short)+2*sizeof(char))
	/* The size of all fields in the data body, except the raw value field. */

/* ********************************************
   * Non-Dwell Update Buffer Memory Structure *
   ******************************************** */

struct update_buffer_str {
	struct ubg_non_dwell_message_header message_header;
	struct ubg_non_dwell_data_header data_header;
	int number_of_mnemonics;
	struct ubg_non_dwell_data_body *data_body;
	char *tlm_or_pseudo;			/* 0=tlm, 1=pseud */
};


/* Translate from number of bytes to a mask */
static short int byte_to_mask_translation[17]={0,UBG_ONE_BYTE_DATA,
	UBG_TWO_BYTE_DATA,0,UBG_FOUR_BYTE_DATA,0,0,0,UBG_EIGHT_BYTE_DATA,
	0,0,0,0,0,0,0,0};
		
/* Translate from a mask to a number of bytes */
static int mask_to_byte_translation[UBG_MASK_TO_BYTE_SIZE]=UBG_MASK_TO_BYTE;
	
/* Previous value of current_frame_time */
static struct timeval last_current_frame_time={0,0};
	
/* EPOCH Function Global Variables */
static struct client_frame *frame_ptr;
static struct timeval network_timeout={60,0};
static struct timeval max_wait_time={10,0};
	
/* Point ID of SC_NAME global */
static client_point *spacecraft_id_pid;
		
/* Point ID of SC_NAME global */
static client_point *stream_db_pid;
	
/* Wait for select time for socket flags */
static struct timeval select_time={0,10000};
	

	
#ifdef UBG_MAIN

int nfds = 0;
/* Send the buffer? */
int send_data=0;
		
/* Value off the process number */
int process_number_value;
		
/* -1=Saw a frame, 0=Saw no frame since the last pseudo update */
int saw_a_frame=0;

/* -1=End, 0=Keep Going */
int end_program=0;
		
/* Point ID of TELEMETRY_RATE telemetry point */
client_point *telemetry_rate_pid;
int telemetry_rate_index;
		
/* Point ID of FRAME_TYPE telemetry point */	
client_point *frame_type_pid;
int frame_type_index;
int frame_type_value;
		
/* Point ID and values of the GAIM_FORMAT_RTxx variables. */	
client_point *gaim_format_rt_pid;
char gaim_format_rt_value[32];
		
/* Point ID of PCM_STREAM_ID telemetry point */
int pcm_stream_id_index;
int pcm_stream_id_value;

/* Point ID of MINOR_FRAME_COUNT telemetry point */
int minor_frame_count_index;
int minor_frame_count_value;

/* Point index of QUALITY_BIT telemetry point */		
int quality_bit_index;
int quality_bit_value;
		
/* Point ID of NORMAL_DWELL_MODE telemetry point */
int tlm_format_index;
int tlm_format_value=-1;
int last_tlm_format_value=-1;
		
/* Value of SPACECRAFT_ID -- passed from SDR or PUB. */
int spacecraft_id_value=-1;
		
/* Global Command Line Inputs */	
char stream_name[32];
char scid[32];
		
/* Debug file name, if supplied */
char debug_filename[256];
int debug_on=0;
		
/* A pointer to all the imformation anybody awants or cares about a 
stream and its' associated database */	
streamdef *stream_def;
		
/* Update Buffer */
struct update_buffer_str update_buffer;
		
/* For PCM, 0=1Kbps, 1=4Kbps, For others, number of seconds per frame */	
int current_telemetry_rate=-1;	
		
/* 0=Run from Command Line [def], 1=Spawned by Playback_Update_Buffer, 
2=Spwaned by Stream_Data_Receiver */
int run_mode=0;

/* Telemetry frame time from frame packet */		
struct timeval current_frame_time;
		
/* Name of the database currently in use */
char database_name[1024];
		
/* TCPIP Stuff */
char process_number[4];
char ubs_service_name[20]="gaim_ubs_ubg_";
char pub_service_name[20]="gaim_pub_ubg_";
char sdr_service_name[20]="gaim_sdr_ubg_";
int ubs_socket=-1;
int sdr_pub_socket=-1;
fd_set read_mask,data_mask;
int message_length;
char ub_buffer[MAX_PACKET_BUFFER_SIZE];
char pkt_buffer[MAX_PACKET_BUFFER_SIZE];
int max_length=-16383;
int ub_crc,pkt_crc;
char pseudo_buffer[MAX_PACKET_BUFFER_SIZE];
int pseudo_buffer_length;
int send_pseudo=0;

/* EPOCH Stream item. */
struct client_connection *network_connection;
		
/* -1=Waiting for a response from the UBS */
int waiting_for_response=0;
		
/* Buffer pointer */
char *buffer_ptr;
		
/* 0=TLM, 1=GLOB, 2=PSEUDO */
int packet_type;
		
/* Name of the GTACS from which we are receiving data */
char gtacs_name[16];
		
/* The merge type requested. A=Auto-merge, M=Manual-merge */
char merge_type[4]="A";
		
/* The merge type requested. A=Auto-merge, M=Manual-merge */
char stream_type[4]="R";

/* prime status of stream.  P=Prime, N=Nonprime */
char prime_nonprime_stream[4]="P";
		
/* IP Address of the archive machine making the archive request */
char archive_ip_address[20]="172.0.0.1";
		
/* Error message buffer */
char error_message[256]=ERROR_MESSAGE_HEADER;
		
/* For the checkpoint file */
char pseudo_cp_buffer[MAX_PACKET_BUFFER_SIZE];
int pseudo_cp_length;
short pseudo_cp_counter;

/* -c parameter value */
char connect_to_stream[256];
		
/* Used to keep track of the HELP_INDEX of each mnemonic to forward
to the GOES Shared Archive */
int mnemonic_to_gsa_count,*gsa_index_list,*gsa_index_mode;

/* All mnemonics in the database */
char **mnemonic_list;
int *mnemonic_indices;
int mnemonic_count;
			
/* gsa_tag value[N] = 1 if mnemonic is listed in gsa file, 0 otherwise */
/* the TAG value (or help_index) N is used as index into this array */
char gsa_tag[GSA_TAG_MAX] = {GSA_TAG_MAX*0};

/* checkpoint filenames on gaim servers */
char gaim_filename_pseudo[256] = "";
char gaim_filename_gva[256] = "";
	
#else

extern int nfds;
extern int send_data;
extern int process_number_value;
extern int saw_a_frame;
extern struct update_buffer_str update_buffer;
extern char debug_filename[256];
extern int debug_on;
extern struct update_buffer_str update_buffer;
extern streamdef *stream_def;
extern struct timeval current_frame_time;
extern int current_telemetry_rate;
extern int telemetry_rate_index;
extern char database_name[1024];
extern client_point *telemetry_rate_pid;
extern client_point *frame_type_pid;
extern client_point *gaim_format_rt_pid;
extern char gaim_format_rt_value[32];
extern int run_mode;
extern char process_number[4];
extern int ub_crc;
extern int ubs_socket;
extern struct client_connection *network_connection;
extern char stream_name[32];
extern char scid[32];
extern char ubs_service_name[20];
extern char pub_service_name[20];
extern char sdr_service_name[20];
extern fd_set read_mask,data_mask;
extern int end_program;
extern int sdr_pub_socket;
extern int waiting_for_response;
extern int message_length;
extern char ub_buffer[MAX_PACKET_BUFFER_SIZE];
extern char pkt_buffer[MAX_PACKET_BUFFER_SIZE];
extern int max_length;
extern int ub_crc,pkt_crc;
extern char *buffer_ptr;
extern int packet_type;
extern char gtacs_name[16];
extern int frame_type_index;
extern int frame_type_value;
extern int pcm_stream_id_index;
extern int pcm_stream_id_value;
extern int tlm_format_index;
extern int tlm_format_value;
extern int last_tlm_format_value;
extern char pseudo_buffer[MAX_PACKET_BUFFER_SIZE];
extern int pseudo_buffer_length;
extern int send_pseudo;
extern int spacecraft_id_value;	
extern int minor_frame_count_index;
extern int minor_frame_count_value;
extern char merge_type[4];
extern char prime_nonprime_stream[4];
extern int quality_bit_index;
extern int quality_bit_value;
extern char stream_type[4];
extern char archive_ip_address[20];
extern char error_message[256];
extern char pseudo_cp_buffer[MAX_PACKET_BUFFER_SIZE];
extern int pseudo_cp_length;
extern short pseudo_cp_counter;
extern char connect_to_stream[256];
extern int mnemonic_to_gsa_count,*gsa_index_list,*gsa_index_mode;
extern char **mnemonic_list;
extern int *mnemonic_indices;
extern int mnemonic_count;
extern char gsa_tag[GSA_TAG_MAX];
extern char gaim_filename_pseudo[256];
extern char gaim_filename_gva[256];

#endif
