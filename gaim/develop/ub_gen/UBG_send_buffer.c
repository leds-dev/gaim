/*  
@(#)  File Name: UBG_send_buffer.c  Release 1.8  Date: 07/04/07, 15:02:32
*/

/*******************************************************************************
        Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_send_buffer.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE    	BY              COMMENT
         V1.0        02/00   S. Scoles      	Initial creation
	 	8.4         mar18-2004  K. Dolinh	 	Add log_event, log_debug
	 	9A1         feb25-2005                 	Add buffer debug print
	 	9AP	     	mar16-2005		 	 		Build 9A Prime
	 			 	apr25-2005          		- Remove output to tlmwatch.log
		10A		 	oct19-2005            		- Check buffersize in header
	 	10B	     	jan12-2006		 	 		Build 10B
		11			Aug18-2006					Build 11
												- update stats in shared mem
												- comment out log_tlmwatch
				               
********************************************************************************
	Invocation:
        
		UBG_send_buffer(int buffer_size,char *buffer)
        
	Parameters:

		buffer_size - I
			The exact size of the update buffer.

		buffer - I
			A pointer to the memory area allocated for the update buffer to 
			send.  

********************************************************************************
	Functional Description:

		This routine sends the update buffer to to update buffer sender (the 
		process which collects update bufferes from multiple update buffer 
		generators and passes them on to the shared GOES archive.
	
*******************************************************************************/

/**

PDL:
	Calculate the checksum of the update buffer.
	Send the update buffer to the Update_Buffer_Sender.

**/

#include <stdio.h>
#include "UBG_structures.h"
#include "../include/util_log.h"

extern int update_buffer_max;
extern int tlmwatch_max;

/* shared mem */
extern GaimStat *gaimstat;

extern int ftp_mode;
extern int debug_pseudo;
extern int debug_gva;
extern int gva_count;

void UBG_send_buffer (int datatype, int buffer_size, char *buffer) {

struct MsgHeader {
	char ip[16];			/* IP Address */
	int message_size;		/* Length of message */
	short class_id;			/* 102 */
	short message_number; 	/* Incremented each new update buffer */
	short node_name_len;	/* Chars in node_name */
	char *node_name;		/* The name of the node which is sending the update 
								buffer */
	short process_name_len;	/* Chars in process_name */
	char *process_name;		/* Update_Buffer_Sender */
};
typedef struct MsgHeader MsgHeader;

MsgHeader * mh;

int	 size_in_header;
char message[256];
char filename[256];
int i,num;
FILE *file_ptr;
int write_length;

	mh = (MsgHeader *) buffer;
	
	/*if (update_buffer_max == 0 && tlmwatch_max > 0) {
		log_tlmwatch("SEND_BUFFER: %48X",buffer);
	}*/
	
	/*if (update_buffer_max == 1 && tlmwatch_max > 0) {

		log_tlmwatch("SEND_BUFFER: ip=%16s  size=%ld  class=%d  msgnum=%d  (len=%d  node=%s) (len=%d  proc=%s)",
			mh->ip,mh->message_size,mh->class_id,mh->message_number,
			mh->node_name_len,mh->node_name,
			mh->process_name_len,mh->process_name);	
	}*/
		
	if (update_buffer_max == 48000) {
		/* do not send if buffersize in header is invalid */
		if (mh->message_size > update_buffer_max) {
			log_event_err("SEND_BUFFER: not sent (buffer_size=%d  size_in_header=%ld  max=%d)",
				buffer_size,mh->message_size,update_buffer_max);
			return;
		}
	}
	
				
	/***
	if (datatype == DATATYPE_PSEUDO && debug_pseudo) {
		log_event("SEND_BUFFER: ip=%16s  size=%ld  class=%d  msgnum=%d  (len=%d  node=%s) (len=%d  proc=%s)",
			mh->ip,mh->message_size,mh->class_id,mh->message_number,
			mh->node_name_len,mh->node_name,
			mh->process_name_len,mh->process_name);	
	}
	***/
			
	/* don't send  buffer if not processing datatype */			
	if (datatype == DATATYPE_GVA && ftp_mode == FTP_PSEUDO_ONLY) {
		/*log_event("GVA buffer not sent to GAS");*/
		return;
	}
	if (datatype == DATATYPE_PSEUDO && ftp_mode == FTP_GVA_ONLY) {
		/*log_event("PSU buffer not sent to GAS");*/
		return;
	}
	
					
	/* CRC Check */
	ub_crc = GAIM_calculate_checksum(buffer_size,buffer);	
	/* Send the buffer */
	write_length = net_write(ubs_socket,buffer,buffer_size);
	/* update stats in shared memory */
	gaimstat->buffers_sent++;

	if (debug_gva && datatype == DATATYPE_PB_PSEUDO) 
		log_event("(send_buffer) DATATYPE_PB_PSEUDO writelen=%d  datatype=%d",write_length,datatype);
	
	
	/***DEBUG_START 
	log_event("UBG_send_buffer: sent buffer to UBS (%d to date)",gaimstat->buffers_sent);	
	if (datatype == DATATYPE_GVA && debug_gva && gva_count<=10) {
		log_event("send_buffer: writelen=%d  datatype=%d  ftp_mode=%d  debug_psu=%d   debug_gva=%d",
			write_length,datatype,ftp_mode,debug_pseudo,debug_gva);
	}
	DEBUG_END*/
	

} /* end UBG_send_buffer */
