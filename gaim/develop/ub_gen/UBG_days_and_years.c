/*  
@(#)  File Name: UBG_days_and_years.c  Release 1.1  Date: 00/11/10, 13:25:58
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_days_and_years.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles       Initial creation

********************************************************************************
	Invocation:
        
		UBG_days_and_years(int *day,int *year);
        
	Parameters:

		day - I/O
			Input: The number of days since 1970-001-00:00:00
			Output: The day of the current year

		year - O
			The year from the day number

********************************************************************************
	Functional Description:

		This routine converts Julian Days from 1970-001-00:00:00 to 
		Year/Julian Day
	
*******************************************************************************/

/**
	
PDL: 
	Set the current yeasr to 1970.
	DOWHILE the current number of days is more than 365 (366 if the current 
	 year is a leap year)
		Subtract the number of days in the current year from the current number 
		 of days.
		Increment the year.
	ENDDO

**/


void UBG_days_and_years(int *days,int *years) {

	int ly=0;
		/* Is the next year a leap year? */

	int local_days=*days,local_years;
		/* Local copies of the inputs */


	local_years=1970;
		/* Start with 1970 */

	while (local_days>=365+ly) {
			/* While there is still more days left than in a year */

		local_days-=365+ly;
			/* Subtract the days of the current year */

		local_years+=1;
			/* Incremement the year */

		if (local_years%4==0) ly=1;
		else ly=0;
			/* Check to see if this next year is a leap year */
	}

	local_days=local_days+1;
		/* Convert from "0 to 364" to "1 to 365" */

	*days=local_days;
	*years=local_years;

}
