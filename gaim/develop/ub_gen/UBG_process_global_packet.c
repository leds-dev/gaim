/*  
@(#)  File Name: UBG_process_global_packet.c  Release 1.8  Date: 07/04/07, 15:27:39
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_process_global_packet.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
	 	 8.4         mar18-2004  K. Dolinh	 	 Add log_event, log_debug
		 11										 change call to UBG_send_data
		 			 mar25-2007					 

********************************************************************************
	Invocation:
        
		UBG_process_global_packet(int bytes,char *packet)
        
	Parameters:

		nytes - I
			The size of the global packet from PUB.

		frame_buffer - I
			The global packet from PUB.

********************************************************************************
	Functional Description:

		This routine takes the global list from PUB and formats it to be 
		converted to an update buffer.
	
*******************************************************************************/

/**

PDL:
	DOFOR each global in the packet
		Extract the time, data size, global index, and status flags.
		IF the global index is valid THEN
			DOCASE for the global data type
				CASE 1: If the global variable is a Long Integer
					Mark the point as an integer in the Update Buffer.
				ENDCASE

				CASE 2: If the global variable is a double precision floating 
				 point.
					Mark the point as a floating point value in the Update 
					 Buffer.
				ENDCASE

				CASE 3: If the global variable is a string
					Send a long int zero as the data value.  There are no 
					 previsions in the Update Buffer to support a data type of 
					 STRING.
				ENDCASE
			ENDDO
		ENDIF

		IF the current global variable is the first one in the packet THEN
			Initialize the Update Buffer.
		ENDIF
	
		Copy the time, status, global index, and value into the Update Buffer.
	ENDDO

	CALL UBG_set_data_header_status
	Group all the global variable information into a block for the Update 
	 Buffer.
	CALL UBG_create_buffer_to_send
	CALL UBG_send_buffer

**/

#include "db.h"
#include "db_util.p"
#include "outp_util.h"
#include "util_log.h"
#include "UBG_structures.h"

/* Prototype */
void dump_the_packet(char *pkt,int bytes);
	
/* global variables */
extern int debug_gva;

void UBG_process_global_packet(int bytes,char *packet) {

char *packet_ptr;
	/* Where are we in the packet? */

short total_globals;
	/* How many globals are in this packet? */

int i;
	/* Counter */

struct timeval global_time;
	/* Time stamp */

short global_size;
	/* Size of global structure */

int global_index;
	/* Global variable index number */

unsigned int global_status;
	/* Global variable status flags */

struct gvar *gid;
struct global_var *gv;
	/* Global Varible DB Pointer */

double raw_value;
	/* extracted global converted to double */

unsigned int temp_long;
	/* temporary long value */

char temp_char[1024];
	/* Temporary character buffer */

char message[1024],message1[1024];
	/* Error message array */

int frame_ms,frame_day,frame_year;
	/* Time Variables */

char *buffer_to_send;
	/* The update buffer to be sent. */

int offset_ms,state_table_number;
	/* More update buffer information */

short int num_bytes;
	/* How long is the data field? */

char short_buff[10];
char hex_buffer[64];
	/* buffer for hex dump of data */

int int_or_float=0;
	/* 0=Integer Raw, 1=Float Raw, 2=Byte Array */

char int_or_float_letter[3]={'I','F','B'};
	/* Is the point an integer, float, or byte array? */

int full_buffer_length=0;
	/* The length of the full update buffer */


packet_ptr=packet;
	/* Set the packet index pointer to the packet start -- the 2-byte 
		packet ID was skipped over in the calling function. */

total_globals=*(short *)packet_ptr;
packet_ptr+=2;
	/* Extract the global count out of the packet.  */

update_buffer.data_header.status=0;
	/* No status info for globals (?) */

/* added by lata to initialize frame_type */
update_buffer.data_header.frame_type = UBG_NO_CODE;

update_buffer.number_of_mnemonics=total_globals;
	/* Set Mnemonic Counter */

update_buffer.message_header.message_size=0;
	/* Initialize the message length to zero */

state_table_number=0;
	/* No state table for globals */


for (i=0; i<total_globals; i++) {
		/* For each global in the packet */

	/* *********************************************
	   * First extract information from PUB packet *
	   ********************************************* */

/*		dump_the_packet(packet_ptr); */

	memcpy(&global_time,packet_ptr,8);
	packet_ptr+=8;
		/* Extract sample time */

	memcpy(&global_size,packet_ptr,2);
	packet_ptr+=2;
		/* Extract data length */


	memcpy(&global_index,packet_ptr,4);
	packet_ptr+=4;
	global_size-=4;
		/* Extract global variable index */

	memcpy(&global_status,packet_ptr,4);
	packet_ptr+=4;
	global_size-=4;
		/* Extract global variable status flags */

	num_bytes=global_size;
		/* How many bytes of data? */

	gid=(gvar *)stream_def->dbhead->gv_head->gv_array[global_index];
	gv = (global_var *)gid->global_var;

	if (gid) if (gid->global_var) {
	
		switch (gid->global_var->global_type) {
			case GLOBAL_LONG:		
				memcpy(&temp_long,packet_ptr,global_size);
				outp_uint(&temp_long);
				raw_value=(double)temp_long;
				int_or_float=0;
				memcpy(&update_buffer.data_body[i].raw_value[0],&temp_long,num_bytes);
					
				if (debug_gva) log_event("(process_global_packet) LONG index=%d  name=(%) value=%g",
					global_index, gv->global_name,raw_value);
				
				break;	

			case GLOBAL_DOUBLE:
				memcpy(&raw_value,packet_ptr,global_size);
				outp_double(&raw_value);
				int_or_float=1;
				memcpy(&update_buffer.data_body[i].raw_value[0],&raw_value,num_bytes);
					
				if (debug_gva) log_event("(process_global_packet) DOUBLE index=%d  name=(%) value=%g",
					global_index, gv->global_name,raw_value);
				
				break;			

			case GLOBAL_STRING:
				/* For strings, send value 0L until it is 
							determined what to do. 
				temp_long=0;
				raw_value=0.0;
				num_bytes=4;
				memcpy(&update_buffer.data_body[i].raw_value[0],&temp_long,
					num_bytes);
						

				memcpy(temp_char,packet_ptr,global_size);
				int_or_float=2;
				*/
				break;

			default:
				log_event_err("process_global_packet: unknown global type (type=%i)",gid->global_var->global_type);
				break;
		}	
		
	} else {
	
		/* Set the undefined GID to 0L */
		log_event_err("process_global_packet: unknown global var (index=%i) set to 0",global_index);
		temp_long=0;
		raw_value=0.0;
		int_or_float=0;
		num_bytes=4;
		memcpy(&update_buffer.data_body[i].raw_value[0],&temp_long,num_bytes);
				
	}

	packet_ptr+=global_size;
		/* point to the start of the next record */

	/* ***********************************
	   * Next, fill in the update buffer *
	   *********************************** */

	if (i==0) {
			/* We need to init the data_header when the first global is 
				analyzed */

		frame_ms=(global_time.tv_sec%86400)*1000+global_time.tv_usec/1000;
			/* Calculate the number of milliseconds since the start of the 
				day */
		frame_day=global_time.tv_sec/86400;
			/* Calculate the day of the current frame. */

		UBG_days_and_years(&frame_day,&frame_year);
			/* Convert from days to year-day */

		update_buffer.data_header.year=frame_year;
		update_buffer.data_header.day=frame_day;
		update_buffer.data_header.milliseconds=frame_ms;
			/* Modify the data header with the base time. */
	}

	offset_ms=(global_time.tv_sec%86400)*1000+global_time.tv_usec/1000;
	offset_ms-=update_buffer.data_header.milliseconds; 
		/* Calculate the difference between the global time and the buffer
			start time */

	if (offset_ms<0) offset_ms+=86400000; 
		/* If the number is negative, it means midnight passed in the 
			middle of the global packet.  We need to add a days worth of 
			milliseconds to get the correct offset. */

	update_buffer.data_body[i].time_offset=(unsigned short)(offset_ms);
		/* Convert from long int to unsigned short int */

	update_buffer.data_body[i].status_flags=0;
		/* Init the status flags. */

	update_buffer.data_body[i].mnemonic_index=global_index;
		/* DB Index for variable */

	update_buffer.data_body[i].status_flags|=
		byte_to_mask_translation[num_bytes];
			/* Set the bits to represent the data length */

	update_buffer.tlm_or_pseudo[i]=0;
		/* This is a global point */

	if (getenv("INCLUDE_DEBUG_DECOM_INFO") && debug_on) {
			/* If variable is defined and debug is on */

		strcpy(hex_buffer,getenv("INCLUDE_DEBUG_DECOM_INFO"));
		if ((hex_buffer[0]=='Y' || hex_buffer[0]=='G') && debug_on) {
				/* If we want to include all the decom information about
					the current point, be forewarned that this option yields
					megabytes and megabytes of debug information! */

			for (i=0; i<num_bytes; i++) {
				sprintf(short_buff,"%8.8X",
					(short)(update_buffer.data_body[i].raw_value[i]));
				if (i==0) strcpy(hex_buffer,&short_buff[6]);
				else strcat(hex_buffer,&short_buff[6]);
			}

			sprintf(message1,", msOffset: %i",offset_ms);
			sprintf(message,
				"Global pt %s(%i), Size: %i%c bytes, Rw: %lf, HX: %s%s",
				gid->global_var->global_name,global_index,
				mask_to_byte_translation[UBG_GET_DATA_MASK(
					update_buffer.data_body[i].status_flags)],
				int_or_float_letter[int_or_float],raw_value,hex_buffer,
				message1);
			log_debug(message);
				/* Which point did we receive? */
		}
	}
}

UBG_set_data_header_status();
	/* Set the status bnits in the data header */

update_buffer.data_header.data_size=
	SIZE_DATA_HEADER_BASE+FORCED_MAX_DB_NAME_LEN;
for (i=0; i<update_buffer.number_of_mnemonics; i++)
	update_buffer.data_header.data_size+=SIZE_DATA_BODY_BASE+
		mask_to_byte_translation[UBG_GET_DATA_MASK(
			update_buffer.data_body[i].status_flags)];
				/* Calculate the size of the buffer that will be sent. */

buffer_to_send=(char *)calloc(sizeof(update_buffer.message_header)+
	update_buffer.data_header.data_size+256,sizeof(char));
		/* Allocate space for the update buffer.  The total space allocated 
			will be the size of the message header plus the size of the 
			data header plus the size of each mnemonics in the buffer.  
			The extra 256 bytes allocated allow for strings to fill in
			the spots of char pointers. */

UBG_create_buffer_to_send(&full_buffer_length,buffer_to_send,0);
	/* Create the actual buffer to send. */

UBG_send_buffer(DATATYPE_GLOBAL,full_buffer_length,buffer_to_send);
	/* Send the buffer */

update_buffer.message_header.message_number++;
	/* Increment message number for next message */

free(buffer_to_send);
	/* Free up memory */

}

void dump_the_packet(char *packet,int bytes) {

int i;
char msg[20480],m2[20480];

strcpy(msg,"Packet:");

for (i=0; i<bytes-4; i+=2) {
	sprintf(m2," %4.4X",*(unsigned short *)(packet+i));
	strcat(msg,m2);
}

if (bytes<4) strcat(msg,"TOO SHORT!");

log_debug(msg);
}
