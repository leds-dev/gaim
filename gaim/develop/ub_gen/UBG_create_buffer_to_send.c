/*  
@(#)  File Name: UBG_create_buffer_to_send.c  Release 1.6  Date: 07/03/25, 09:12:15
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_new_frame_cb.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        02/00       S. Scoles       Initial creation
	 8.4         mar18-2004  K. Dolinh	 Add log_event, log_debug
	 8.8         apr19-2004                  fix db_len check (make it LE)

********************************************************************************
	Invocation:
        
		UBG_create_buffer_to_send(int *buffer_size, char *buffer)
        
	Parameters:

		buffer_size - O
			The exact size of the update buffer.

		buffer - I/O
			A pointer to the memory area allocated for the update buffer to 
			send.  

		type - I
			0=Telemetry Point or Global Sample
			1=Pseudotelemetry Point
			2=End of hour/End of data 

********************************************************************************
	Functional Description:

		This routine formats the actual update buffer from the update buffer 
		structures.
	
*******************************************************************************/

/**

PDL:
	Cpy the destination IP Address into the buffer.
	Create the message header in the buffer.
	Create the data header in the buffer.
	Copy the data body to the buffer.
	Modify a couple fields to represnt the 'true' update buffer.

**/

#include "UBG_structures.h"
	/* Update_Buffer_Generator structures */

void UBG_create_buffer_to_send(int * buffer_size,char *buffer,int type) {

	char *index_into_buffer;
		/* A pointer that keeps track of the next location to copy to in
			the buffer */

	int i;
		/* Loop Variable */

	short short_data = 0;
	int long_data = 0;

	char *start_buffer;
		/* The actual start of the Update Buffer (no IP Address) */

	char message[256];
		/* Error message */

	int db_name_length;
		/* DB Name Length */


/* ************************
   * Initialize variables *
   ************************ */

	index_into_buffer=buffer;
		/* Initialize a variable pointer which keeps track of the next memcpy 
			location in our buffer */

	*buffer_size=0;
		/* Force buffer size to zero */


/* *************************************************************************
   * Copy the destination archive IP address into the update buffer packet *
   ************************************************************************* */

	memcpy(index_into_buffer,archive_ip_address,16);
	index_into_buffer+=16;
	*buffer_size+=16;

	start_buffer=index_into_buffer;
		/* Record the start of the actual buffer */

/* **************************************************
   * Copy the message header into the update buffer *
   ************************************************** */

	memcpy(index_into_buffer,&update_buffer.message_header.message_size,
		sizeof(update_buffer.message_header.message_size));
	index_into_buffer+=sizeof(update_buffer.message_header.message_size);
	*buffer_size+=sizeof(update_buffer.message_header.message_size);
		/* No byte swap needed -- this is swapped elsewhere */

	memcpy(index_into_buffer,&update_buffer.message_header.class_id,
		sizeof(update_buffer.message_header.class_id));
	index_into_buffer+=sizeof(update_buffer.message_header.class_id);
	*buffer_size+=sizeof(update_buffer.message_header.class_id);
		/* No byte swap needed -- this is swapped elsewhere */

  short_data = update_buffer.message_header.message_number;
	memcpy((char*)&update_buffer.message_header.message_number, (char*)(&short_data),
		sizeof(update_buffer.message_header.message_number));
	memcpy(index_into_buffer,&update_buffer.message_header.message_number,
		sizeof(update_buffer.message_header.message_number));
	index_into_buffer+=sizeof(update_buffer.message_header.message_number);
	*buffer_size+=sizeof(update_buffer.message_header.message_number);

	memcpy(index_into_buffer,&update_buffer.message_header.node_name_len,
		sizeof(update_buffer.message_header.node_name_len));
	index_into_buffer+=sizeof(update_buffer.message_header.node_name_len);
	*buffer_size+=sizeof(update_buffer.message_header.node_name_len);
		/* No byte swap needed -- this is swapped elsewhere */

	if (strlen(update_buffer.message_header.node_name)>0) {
		memcpy(index_into_buffer,update_buffer.message_header.node_name,
			strlen(update_buffer.message_header.node_name));
		index_into_buffer+=strlen(update_buffer.message_header.node_name);
	} 
	*buffer_size+=strlen(update_buffer.message_header.node_name);

	memcpy(index_into_buffer,&update_buffer.message_header.process_name_len,
		sizeof(update_buffer.message_header.process_name_len));
	index_into_buffer+=sizeof(update_buffer.message_header.process_name_len);
	*buffer_size+=sizeof(update_buffer.message_header.process_name_len);
		/* No byte swap needed -- this is swapped elsewhere */

	if (strlen(update_buffer.message_header.process_name)>0) {
		memcpy(index_into_buffer,update_buffer.message_header.process_name,
			strlen(update_buffer.message_header.process_name));
		index_into_buffer+=strlen(update_buffer.message_header.process_name);
	} 
	*buffer_size+=strlen(update_buffer.message_header.process_name);


/* ***********************************************
   * Copy the data header into the update buffer *
   *********************************************** */

	update_buffer.data_header.database_name_len=FORCED_MAX_DB_NAME_LEN;
		/* Force the length of the database name to a fixed value -- The GOES 
			Archive Software REQUIRES a fixed length string.  The GOES Shared 
			Archive CANNOT handle variable length strings. */

  short_data = update_buffer.data_header.data_size;
	memcpy((char*)&update_buffer.data_header.data_size, (char*)(&short_data), 
		sizeof(update_buffer.data_header.data_size));
  short_data = update_buffer.data_header.database_name_len;
	memcpy((char*)&update_buffer.data_header.database_name_len, (char*)(&short_data), 
		sizeof(update_buffer.data_header.database_name_len));
	memcpy(index_into_buffer,&update_buffer.data_header,6);
	index_into_buffer+=6;
		/* data_size, gtacs_name, scid, minor_frame_number.  */

	for (i=0; i<FORCED_MAX_DB_NAME_LEN; i++) *(index_into_buffer+i)=0;
		/* Nullify the memory location so the string can become automatically
			null-terminated. */

	db_name_length=strlen(update_buffer.data_header.database_name);
	strcpy(&update_buffer.data_header.database_name[db_name_length-3],"gsa");
		/* Rename the name of the database to the GOES Archive DB Name.  This
			involves just a change of the three-letter extension from .lis to 
			.gsa.  This was a request by John Raynor of Lockheed-Martin on
			November 2, 2000.  NOTE: THIS CHANGE VIOLATES THE ICD PARAMETERS.  
			But, because Lockheed-Martin was already 3+ months behind schedule 
			at this point in time and Integral Systems had finished their GAIM
			ahead of schedule, it was a whole lot faster for Integral Systems 
			to make the change to the GAIM rather than have Lockheed-Martin 
			make the change in their code.  So, the change of extension from 
			.lis to .gsa now takes place on the GAIM end of the communications
			rather on the GOES Shared Archive end of the communications as 
			it is specified in the ICD. */

	if (strlen(update_buffer.data_header.database_name)<=
			FORCED_MAX_DB_NAME_LEN-1) {
		memcpy(index_into_buffer,update_buffer.data_header.database_name,
			strlen(update_buffer.data_header.database_name));
	} else {
		memcpy(index_into_buffer,update_buffer.data_header.database_name,
			FORCED_MAX_DB_NAME_LEN-1);
		log_event_err("the GOES Shared Archive cannot handle variable length strings");
		log_event_err("DB name (%s) must be truncated to %i chars (%s)",
			update_buffer.data_header.database_name,
			FORCED_MAX_DB_NAME_LEN-1,index_into_buffer);
	}
	index_into_buffer+=FORCED_MAX_DB_NAME_LEN;
		/* Database Name -- FORCED to 31 chars plus NULL because the GOES 
			Shared Archive does not support variable length strings.  Any 
			changes to the length of the database name must also be done to 
			the GOES Shared Archive.  If a DB name is less than 31 chars, 
			it will be null padded through the 32nd byte. */

	if (type!=2) {
    short_data = update_buffer.data_header.year;
		memcpy((char*)&update_buffer.data_header.year, (char*)(&short_data), 
			sizeof(update_buffer.data_header.year));
    short_data = update_buffer.data_header.day;
		memcpy((char*)&update_buffer.data_header.day, (char*)(&short_data), 
			sizeof(update_buffer.data_header.day));
    long_data = update_buffer.data_header.milliseconds;
		memcpy((char*)&update_buffer.data_header.milliseconds, (char*)(&long_data), 
			sizeof(update_buffer.data_header.milliseconds));
	}
  short_data = update_buffer.data_header.status;
	memcpy((char*)&update_buffer.data_header.status, (char*)(&short_data), 
		sizeof(update_buffer.data_header.status));
	memcpy(index_into_buffer,&update_buffer.data_header.year,10);
	index_into_buffer+=10;
		/* year, day, milliseconds, status. */

  short_data = update_buffer.data_header.data_size;
	memcpy((char*)&update_buffer.data_header.data_size, (char*)(&short_data), 
		sizeof(update_buffer.data_header.data_size));
			/* Need to byte swap back to original form */

	*buffer_size+=update_buffer.data_header.data_size;
		/* This was previously calculated and includes the size of the data 
			header as well as all the data bodies */


/* ***************************************************
   * Copy all the data bodies into the update buffer *
   *************************************************** */

	for (i=0; i<update_buffer.number_of_mnemonics; i++) {
		if (update_buffer.tlm_or_pseudo[i]==type) {
			memcpy(index_into_buffer,&update_buffer.data_body[i],
				SIZE_DATA_BODY_BASE+mask_to_byte_translation[UBG_GET_DATA_MASK(
					update_buffer.data_body[i].status_flags)]);
			index_into_buffer+=SIZE_DATA_BODY_BASE+
				mask_to_byte_translation[UBG_GET_DATA_MASK(
					update_buffer.data_body[i].status_flags)];
		}
	}


/* ***********************************
   * Update UpdateBuffer information *
   *********************************** */

	update_buffer.message_header.message_size=(*buffer_size)-4;
		/* Since the NT software is set up very strangely.  This "message size"
			is the size of the complete update buffer message minus the size of
			the message_size field. */

	update_buffer.message_header.message_size-=16;
		/* Subtract the IP Address bytes from the message size. */

  long_data = update_buffer.message_header.message_size;
	memcpy((char*)&update_buffer.message_header.message_size, (char*)(&long_data), 
		sizeof(update_buffer.message_header.message_size)); 
	memcpy(start_buffer,&update_buffer.message_header.message_size,4);
	memcpy(index_into_buffer,&update_buffer.data_header.year,10);
		/* The first 4 bytes of the update buffer contain the message size */

}
