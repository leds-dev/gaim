/*
@(#)  File Name: UBG_prototypes.h  Release 1.1  Date: 07/03/25, 09:14:30
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         INCLUDE         :       UBG_prototypes.c
         EXE. NAME       :       
         PROGRAMMER      :       K. Dolinh

         VER.        DATE        BY              COMMENT

		 11          Mar08-2007  K. Dolinh       Created
		 			 
		 
********************************************************************************
         Functional Description:

         This header file defines prototype of subroutines in
         the N-Q_Interface_Manager process.

*******************************************************************************/

#include <unistd.h>

#include "list.p"

/*struct GasEventMsg {		         
	GasMsgHeader  msg_hdr;
};
typedef struct GasEventMsg GasEventMsg; */

#define DATATYPE_FRAME  1
#define DATATYPE_PSEUDO	2
#define DATATYPE_GVA	3
#define DATATYPE_EOH	4
#define DATATYPE_GLOBAL 5
#define DATATYPE_PB_PSEUDO	6
#define DATATYPE_PB_GVA		7

#define FTP_PSEUDO_ONLY 	1
#define FTP_GVA_ONLY    	2
#define FTP_PSEUDO_GVA 		3
#define FTP_PSEUDO_GVA_SAMETIME 4

int  UBG_ftp_start_job (int datatype);
void UBG_make_pseudo_buffer_from_cp (int datatype, Pseudo_struct *pseudo, int timestamp, int pseudo_index);
void UBG_process_pseudo_packet (int datatype, int bytes, char *packet);
void UBG_send_buffer (int datatype, int buffer_size, char *buffer);
int UBG_wait_forever(void);
void UBG_init_stream(void);
int UBG_initialize_epoch(char *new_database_name);
void UBG_byte_swap(char *string,int numbytes);
void UBG_tcpip_setup(void);
int UBG_ftp_pseudo_checkpoint_file(void);
int UBG_ftp_gva_checkpoint_file(void);
void UBG_days_and_years(int *days,int *years);
int UBG_filter_non_gsa_mnemonics(int number_of_points, char *output_name);
void UBG_init_globals(void);
void UBG_create_buffer_to_send(int * buffer_size,char *buffer,int type);
void UBG_set_data_header_status(void);
void UBG_process_tlm_frame(int frame_size,char *frame_buffer);
void UBG_process_global_packet(int bytes,char *packet);
int UBG_process_database_packet(int bytes,char *packet);
void UBG_process_gtacs_packet(int bytes,char *packet);
void UBG_process_eoh_eoi_packet(void);
void log_tlmwatch (char *fmt,...);
void GAIM_quicksort_char(int n, char **array, int *Z);
int GAIM_calculate_checksum(int buffer_length,char *buffer);
int GAIM_binarysearch_char(int n, char **A, char *k);
void GAIM_quicksort_int(int n, int *array, int *associated_data);

void create_eventfile (void);
void dump_the_packet(char *pkt,int bytes);
void log_event (char *fmt,...);
void log_event_err (char *fmt,...);
void log_debug (char *fmt,...);
void log_err   (char *fmt,...);
void log_daily_statistics (int year,int day);
