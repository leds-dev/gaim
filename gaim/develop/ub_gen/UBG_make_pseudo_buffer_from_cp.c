/*  
@(#)  File Name: UBG_make_pseudo_buffer_from_cp.c  Release 1.5  Date: 07/03/25, 09:12:25
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_make_pseudo_buffer_from_cp.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        05/00       S. Scoles       Initial creation
	 	 11	     	 03/07			 			 set data quality flag in buffer

********************************************************************************
	Invocation:
        
		UBG_make_pseudo_buffer_from_cp(current_pseudo,timestamp,pseudo_index)
        
	Parameters:

		current_pseudo - I
			A structure containing one pseudomnemonic value -- 
			the one currently being read from the checkpoint file.

		timestamp - I
			The time stamp to use for each point 

		pseudo_index - I
			This is the database index for the pseudo point.

********************************************************************************
	Functional Description:

		This routine processes the pseudomnemonic checkpoint file.
	
*******************************************************************************/

/**

PDL: 
	IF the new pseudotelemetry point will overflow our buffer THEN
		Copy the current buffer to another memory location.
		CALL UBG_process_pseudo_packet.
		Reset the buffer.
	ENDIF

	Add the pseudotelemetry point to the current buffer.

**/

extern int debug_pseudo;
extern int debug_gva;
extern int gva_count;

#include "UBG_structures.h"

void UBG_make_pseudo_buffer_from_cp(
		int datatype,
		Pseudo_struct *pseudo,
		int timestamp,
		int pseudo_index) 

{
	/*DEBUG_START
	if (datatype==DATATYPE_PSEUDO && debug_pseudo) {
		log_event("PSU (%32.32s)  value = %g",pseudo->mnemonic,pseudo->value);
	}

	if (datatype==DATATYPE_GVA && gva_count==1) {
		log_event("GVA (%32.32s) tag=%d  time=%d  value=%g  cp_len=%d",
			pseudo->mnemonic,pseudo_index,timestamp,pseudo->value,pseudo_cp_length);
	}
	DEBUG_END*/
	
	/* Where are we currently located? */
	char *temp_ptr=&pseudo_cp_buffer[pseudo_cp_length];
		
	/* Always assume zero microseconds */
	int zero = 0;
	
	/* data quality flags */
	int flags = 0;
		
	/* If the packet will become larger than the maximum size, send it and 
		reset parameters */
	if (pseudo_cp_length+32>MAX_PACKET_BUFFER_SIZE) {
		memcpy(pseudo_cp_buffer,&pseudo_cp_counter,2);
		UBG_process_pseudo_packet(datatype,pseudo_cp_length,pseudo_cp_buffer);
		temp_ptr=pseudo_cp_buffer;
		pseudo_cp_length=0;
	}

	/* If this is the first time into this routine, we need to skip over 
	   the count location for pseudos. */
	if (pseudo_cp_length==0) {
		temp_ptr+=2;
		pseudo_cp_length+=2;
		pseudo_cp_counter=0;
	}

	/* Copy the time. */
	memcpy(temp_ptr,&timestamp,4);
	temp_ptr+=4;
	memcpy(temp_ptr,&zero,4);
	temp_ptr+=4;
		
	/* Copy the pseudomnemonic index */
	memcpy(temp_ptr,&pseudo_index,4);
	temp_ptr+=4;
		
	
	/*** set data quality flags in buffer **/

/*** db_params.h
 enum point_status {
00240: STALE_VALUE,
00241: SC_VALUE,
00242: RAW_VALUE,
00243: LINEAR_VALUE,
00244: EU_VALUE,
00245: LIMITS_VALUE,
00246: STATE_VALUE,
00247: CONTEXT_CHANGED,
00248: QUESTIONABLE_VALUE,
00249: NEED_RED_ACK,
00250: RED_ACK,
00251: YELLOW_ACK,
00252: ACK_BITS,
00253: CONTEXT_BITS,
00254: MAX_STATUS
00255: };
00256: typedef enum point_status point_status;
***/

/*** 
00042: unsigned long status_mask[MAX_STATUS] = {
00043: 0x00000001, /* Update is stale. 
00044: 0x00000002, /* Spacecraft value decommutated. 
00045: 0x00000004, /* Raw value computed. 
00046: 0x00000008, /* Linear EU value compute
00047: 0x00000010, /* Polynomial EU value computed. 
00048: 0x00000020, /* Limits value computed.
00049: 0x00000040, /* State value computed. 
00050: 0x00000080, /* Context changed. 
00051: 0x00000100, /* Point came from questionable frame. 
00052: 0x00000200, /* Point needs red ack. 
00053: 0x00000400, /* Point has red ack. 
00054: 0x00000800, /* Point has yellow ack. 
00055: 0x00000E00, /* Point ack bits. 
00056: 0x00000078 /* context dependent bits 
***/

	flags = 0;
	if (pseudo->flags & status_mask[QUESTIONABLE_VALUE]) flags |= UBG_QUESTIONABLE_QUALITY;

	memcpy(temp_ptr,&flags,4);
	temp_ptr+=4;

	/* Copy the value field from the checkpoint file into the buffer */
	memcpy(temp_ptr,&(pseudo->value),8);
	temp_ptr+=8;

	/* Increment byte counter point counter for next point */
	pseudo_cp_length+=24;
	pseudo_cp_counter++;


}
