/*  
@(#)  File Name: UBG_set_data_header_status.c  Release 1.7  Date: 07/04/16, 08:41:47
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_set_data_header_status.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
	 	 9A2         mar02-2005  K. Dolinh       Set bit rate in status
	 	 9AP	     mar16-2005		 	 		 Build 9A Prime
		 11          Apr13-2006                  - Increment stats in shared mem

********************************************************************************
	Invocation:
        
		UBG_set_data_header_status()
        
	Parameters:

		NONE

********************************************************************************
	Functional Description:

		This routine sets the flags in the data header to the last known values 
		from either the command line or telemetry points.
	
*******************************************************************************/

/**

PDL:
	Set the stream id.
	Set the frame type.
	Set the merge flag.
	Set the prime flag.
	Set the stream type.
	Set the quality bit.

**/

/* Structures, defines, and statics */
#include "UBG_structures.h"
#include "../include/util_log.h"

/* global variables used */
extern int	pcm1_rate;
extern int	pcm2_rate;
extern GaimStat *gaimstat;
	
void UBG_set_data_header_status(void) {

/* set bit rate */
if (frame_type_value==PCM1_FRAME_TYPE) {
	if (pcm1_rate==1) update_buffer.data_header.status|=UBG_PCM_1KBPS;
	else update_buffer.data_header.status|=UBG_PCM_4KBPS;
} else if (frame_type_value==PCM2_FRAME_TYPE) {  
	if (pcm2_rate==1) update_buffer.data_header.status|=UBG_PCM_1KBPS;
	else update_buffer.data_header.status|=UBG_PCM_4KBPS;		 
}		

/* Set the merge type, if this is a merge (PUB) request. */
if (merge_type[0]=='A') {
	update_buffer.data_header.status|=UBG_AUTO_MERGE;
	gaimstat->buffers_automerge++;
} else if (merge_type[0]=='M') {
	update_buffer.data_header.status|=UBG_MANUAL_MERGE;
	gaimstat->buffers_manualmerge++;
} 
			
/* Set the prime/nonprime flag, if this is a SDR request. */
if (prime_nonprime_stream[0]=='N') 
	update_buffer.data_header.status|=UBG_NON_PRIME;
else if (prime_nonprime_stream[0]=='P') 
	update_buffer.data_header.status|=UBG_PRIME;

if (stream_type[0]=='S') 
	update_buffer.data_header.status|=UBG_SIMULATED_STREAM;
else if (stream_type[0]=='R') 
	update_buffer.data_header.status|=UBG_REAL_STREAM;

/* If the frame is bad, set status. */
if (quality_bit_value!=0) {
	update_buffer.data_header.status|=UBG_BAD_QUALITY;
	gaimstat->buffers_badquality++;
}	

/* increment buffers count in shared memory */

if (update_buffer.data_header.frame_type == UBG_DWELL_CODE)
	gaimstat->buffers_dwell++;
else if (update_buffer.data_header.frame_type == UBG_PCM1_CODE) 
	gaimstat->buffers_normal++;
else if (update_buffer.data_header.frame_type == UBG_PCM2_CODE) 
	gaimstat->buffers_normal++;
else if (update_buffer.data_header.frame_type == UBG_WBI_CODE) 
	gaimstat->buffers_wbi++;
else if (update_buffer.data_header.frame_type == UBG_WBS_CODE) 
	gaimstat->buffers_wbs++;
else if (update_buffer.data_header.frame_type == UBG_SXI_CODE) 
	gaimstat->buffers_sxi++;

gaimstat->buffers_all = gaimstat->buffers_normal + 
						gaimstat->buffers_dwell +
						gaimstat->buffers_wbi + 
						gaimstat->buffers_wbs + 
						gaimstat->buffers_sxi +
						gaimstat->buffers_pseudo ;	
	
} /* end UBG_set_data_header_status */

