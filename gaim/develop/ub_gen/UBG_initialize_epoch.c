/*  
@(#)  File Name: UBG_initialize_epoch.c  Release 1.12  Date: 07/04/07, 15:02:27
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_initialize_epoch.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
         V2.0        04/02       A. Raj          Correct sprintf's
	     8.4         mar18-2004  K. Dolinh	     Add log_event, log_debug
	                 mar22-2004                  fix log_event call
		 10A         sep09-2005                  Add Initialization of array gsa_tag
		             sep28-2005                  return error if no gsa file   
	 	 10B	     jan12-2006		 	 		 Build 10B
		 11          oct02-2006                  get stream id before load_db call
				 
********************************************************************************
	Invocation:
        
		failure=UBG_initialize_epoch(char *new_database_name)
        
	Parameters:

		new_database_name - I
			The name of the new database to load.

		failure - O
			-1=Failed Initialization, 0=Successful Initialization

********************************************************************************
	Functional Description:

		This routine shuts down the TLM and GV utilities (when needed), loads 
		the new database, and restarts the TLM and GV utils with the new 
		database.
	
*******************************************************************************/

/**

PDL: 
	IF a new database needs to be loaded THEN
		Terminate the telemetry and global utility utilities.
		Delete the current database from memory.
	ENDIF

	Find disk version of the new database.

	IF the database was found on the disk THEN
		Load the new database.
	ENDIF

	IF the database loaded successfully THEN
		Start the EPOCH global variable and telemetry utilities.
	ENDIF

	IF both the global variable and telemetry utilities were successfully 
	 started THEN
		Add all telemetry points to the decommutation list.
		Look up the database index of the fololowing mnemonics: TELEMETRY_RATE,
		 STREAM_SOURCE, PCM_STREAM_ID, MINOR_FRAME_COUNT, QUALITY_BIT, and 
		 TELEMETRY_FORMAT.
		IF no .gaim file exisits THEN
			CALL UBG_filter_non_gsa_mnemonics
		ENDIF
		Read the .gaim file associated with the .lis file.
		CALL UBG_init_globals.
	ENDIF

	RETURN an error status.

**/

#include <string.h>
#include <fcntl.h>
#include "path_util.p"
#include "gv_util.p"
#include "tlm.p"
#include "strmsrv.p"
#include "UBG_structures.h"

extern void UBG_decom_callback(point *pid,unsigned long reason);
	/* Decom callback prototype */

int UBG_initialize_epoch(char *new_database_name) {

	char message[1024];
		/* Debug messages */

	int status;
		/* Status flag */

	int i;
		/* Counter */

	int number_of_mnems_in_db=0;
		/* Number of mnemonics total */

	LIST db_points;
		/* Linked List used to count points. */

	int retval=0;

	FILE *db_test;

	char mess[256];

	char output_name[256];

	int needs_sorting=0;

	/* variables used in processing gsa file */
	char 	gsa_filename[1024];
	char 	gsa_text[1024];
	char 	gsa_mnemonic[1024];
	FILE 	*gsa_fileptr = NULL;
	int		gsa_tag_total = 0;
	int  	n;
	int		stream_id;
	char 	*tok1;
	char	*tok2;

if (update_buffer.data_body!=0) {
		/* If we have already initialized the pointers and utilities, we 
			need to stop everything before changing databases */

	term_tlm_util(stream_def->dbhead);
	term_gv_util(stream_def->dbhead);
	delete_db(stream_def->dbhead);
		/* Terminate the telemetry and global variable utilities */

	free(stream_def->database);
	stream_def->database=0;
	free(stream_def);
	stream_def=0;
	free(update_buffer.data_header.database_name);
	update_buffer.data_header.database_name=0;
	free(update_buffer.data_body);
	update_buffer.data_body=0;
	free(update_buffer.tlm_or_pseudo);
	update_buffer.tlm_or_pseudo=0;
	free(gsa_index_list);
	gsa_index_list=0;
	free(gsa_index_mode);
	gsa_index_mode=0;
		/* Free all pointers we need to reallocate */

}

strcpy(database_name,new_database_name);
	/* Copy the new database name */

if (database_name[0]!='/' && database_name[0]!=0) {
		/* If path not given */

	if (path_onREPORTS(database_name,NULL))
		strcpy(database_name,path_onREPORTS(database_name,NULL));
	else {
		sprintf(message,"error finding database '%s'",database_name);
		log_event_err(message);
		retval=-1;
		strcat(error_message,message);
	}
} else if (database_name[0]==0) {
	sprintf(message,"no database name given");
	log_event_err(message);
	retval=-1;
	strcat(error_message,message);
} else {
	db_test=fopen(database_name,"r");
	if (!db_test) {
		i=strlen(database_name);
		while (database_name[i]!='/' && i>=0) i--;
		i++;
		if (path_onREPORTS(&database_name[i],NULL)) {
			sprintf(mess,"error finding %s",database_name);
			strcpy(database_name,path_onREPORTS(&database_name[i],NULL));
			sprintf(message,"%s  using %s instead",mess,database_name);
			log_event_err(message);
		} else {
			sprintf(message,"cannot find database '%s'",database_name);
			log_event_err(message);
			retval = -1;
			strcat(error_message,message);
		}

	} else fclose(db_test);
}

if (!retval) {
	/* Allocate memory for the stream definition structure */
	stream_def=(struct streamdef *)calloc(1,sizeof(struct streamdef));
	
	/* get database name */
	stream_def->database=(char *)calloc(512,sizeof(char));
	strcpy(stream_def->database,database_name);
	
	/* get stream id if not playback (no stream name) */
	if (strlen(stream_name) > 0) stream_id = stream_index(stream_name,NULL);
	if (stream_id >= 0) stream_def->stream_id = stream_id;
		
	if (load_db (&stream_def->dbhead, stream_def, NULL)) {
		sprintf(message,"load_db error database=%s  stream=(%s) stream_id=%d",database_name,stream_name,stream_def->stream_id);
		log_event_err(message);
		retval=-1;
		strcat(error_message,message);
		stream_def->dbhead=0;
	} else {
		log_event("loaded database=%s  stream=(%s)  stream_id=%d",database_name,stream_name,stream_def->stream_id);
	}
}

if (!retval) {
	/* Init the EPOCH global variable utility */
	status = init_gv_util(stream_def->dbhead);
	if (status) {
		sprintf(message,"init_gv_util error = %i",status);
		log_event_err(message);
		retval = -1;
		strcat(error_message,message);
	}
}

if (!retval) {
	/* Initialize the EPOCH telemetry utility */
	status = init_tlm_util(stream_def->dbhead,O_RDONLY|stream_def->io_mode,
		"Update_Buffer_Generator");			
	if (status) {
		sprintf(message,"init_tlm_util error = %i",status);  /* ARAJ */
		log_event_err(message);
		strcat(error_message,message);
		retval=-1;
	}
}


if (!retval) {
	add_all_points(stream_def->dbhead,(void (*)(void *, long unsigned int))UBG_decom_callback,ON_SAMPLE);
		/* Add every point from the database to the decommutation 
			list */

	i=strlen(database_name)-1;
	while (i>0 && database_name[i]!='/') i--;
		/* Chop off the path name from the database name */

	if (database_name[i]=='/') i++;
		/* If i is 0, but database_name[0]!='/', then assume the 
			database_name variable only contained the filename and 
			not the path and do not increment i. */

	update_buffer.data_header.database_name=
		(char *)calloc(strlen(database_name)+1,sizeof(char));
	strcpy(update_buffer.data_header.database_name,&database_name[i]);
	update_buffer.data_header.database_name_len=
		strlen(update_buffer.data_header.database_name);
			/* Copy and count only the file name.  Not the full path. */

	update_buffer.number_of_mnemonics=0;

	telemetry_rate_index=-1;
	db_points=(LIST)HeadOfList(stream_def->dbhead->points);
	while (db_points!=(void *)NULL) {
		if (strcmp("TELEMETRY_RATE",TLM_MNEMONIC(stream_def->dbhead,
			number_of_mnems_in_db))==0) telemetry_rate_index=
				((point *)(stream_def->dbhead->tlm_head->point_array[
					number_of_mnems_in_db]))->tlm_point_index;
						/* Find the index of the TELEMETRY_RATE point */

		if (strcmp("STREAM_SOURCE",TLM_MNEMONIC(stream_def->dbhead,
			number_of_mnems_in_db))==0) frame_type_index=
				((point *)(stream_def->dbhead->tlm_head->point_array[
					number_of_mnems_in_db]))->tlm_point_index;
						/* Find the index of the STREAM_SOURCE point */

		if (strcmp("PCM_STREAM_ID",TLM_MNEMONIC(stream_def->dbhead,
			number_of_mnems_in_db))==0) pcm_stream_id_index=
				((point *)(stream_def->dbhead->tlm_head->point_array[
					number_of_mnems_in_db]))->tlm_point_index;
						/* Find the index of the PCM_STREAM_ID point */

		if (strcmp("MINOR_FRAME_COUNT",TLM_MNEMONIC(stream_def->dbhead,
			number_of_mnems_in_db))==0) minor_frame_count_index=
				((point *)(stream_def->dbhead->tlm_head->point_array[
					number_of_mnems_in_db]))->tlm_point_index;
						/* Find the index of the MINOR_FRAME_COUNT point */

		if (strcmp("QUALITY_BIT",TLM_MNEMONIC(stream_def->dbhead,
			number_of_mnems_in_db))==0) quality_bit_index=
				((point *)(stream_def->dbhead->tlm_head->point_array[
					number_of_mnems_in_db]))->tlm_point_index;
						/* Find the index of the QUALITY_BIT point */

		if (strcmp("TLM_FORMAT",TLM_MNEMONIC(stream_def->dbhead,
			number_of_mnems_in_db))==0) {
         tlm_format_index=
				((point *)(stream_def->dbhead->tlm_head->point_array[
					number_of_mnems_in_db]))->tlm_point_index;
						/* Find the index of the TLM_FORMAT point */
    }

		number_of_mnems_in_db++;
		db_points=db_points->next;

	}
		/* Count the number of points in the database */

	mnemonic_count=number_of_mnems_in_db;
	mnemonic_list=(char **)calloc(mnemonic_count,sizeof(char *));
	mnemonic_indices=(int *)calloc(mnemonic_count,sizeof(int));
	for (i=0; i<mnemonic_count; i++) {
		mnemonic_list[i]=(char *)calloc(40,sizeof(char));
		strcpy(mnemonic_list[i],TLM_MNEMONIC(stream_def->dbhead,i));
		mnemonic_indices[i]=((point *)(stream_def->dbhead->tlm_head->
			point_array[i]))->tlm_point_index+1;
				/* The +1 seems to be important... */
		if (i>0) if (strcmp(mnemonic_list[i],mnemonic_list[i-1])<0) 
			needs_sorting=-1;
	}
		/* Allocate space for the mnemonic list and copy mnemonic 
				names and indices */

	if (needs_sorting) 
		GAIM_quicksort_char(mnemonic_count,mnemonic_list,mnemonic_indices);
			/* Sort, but only if necessary */

		log_debug("total points in database = %i",number_of_mnems_in_db);
			/* Display the final count */	

		if (telemetry_rate_index == -1) {
			log_event_err("no TELEMETRY_RATE found. using 4Kbps or last valid rate");
		}

	strcpy(output_name,database_name);
	output_name[strlen(output_name)-3]=0;
	strcat(output_name,"gaim");
		/* Copy the database name, chop off the .lis extension, and add a 
			.gaim extension */

//	db_test = fopen(output_name,"rb");
	db_test = fopen("/home/gtacsops/g13_normal_20200701.gaim","rb");
	if (!db_test) {
		log_debug("filter out non .gsa mnemonics and create .gaim 'keep index' file");
		retval = UBG_filter_non_gsa_mnemonics(number_of_mnems_in_db,output_name);
//		db_test = fopen(output_name,"rb");
		db_test = fopen("/home/gtacsops/g13_normal_20200701.gaim","rb");
	} 

	if (db_test) {
	
		fread(&mnemonic_to_gsa_count,sizeof(int),1,db_test);
		if (mnemonic_to_gsa_count>0) {
			gsa_index_list=(int *)calloc(mnemonic_to_gsa_count,sizeof(int));
			gsa_index_mode=(int *)calloc(mnemonic_to_gsa_count,sizeof(int));
			fread(gsa_index_list,sizeof(int),mnemonic_to_gsa_count,db_test);
			fread(gsa_index_mode,sizeof(int),mnemonic_to_gsa_count,db_test);
		}
		fclose(db_test);
		log_event("using %i (out of %i) mnemonics for GAS",
			mnemonic_to_gsa_count,number_of_mnems_in_db);
			
	} else if (!db_test && retval==0) {
		log_event_err("cannot open GSA index file %s",output_name);
		retval = -1;
	}

	if (mnemonic_to_gsa_count <= 0) {
		log_event_err("invalid mnemonic count %i from GSA index file %s",
			mnemonic_to_gsa_count,output_name);
		retval=-1;
	}

	if (!retval) {
		update_buffer.data_body=
			(struct ubg_non_dwell_data_body *)calloc(number_of_mnems_in_db,
			sizeof(struct ubg_non_dwell_data_body));
				/* Allocate space for the Update Buffer Data Body */

		update_buffer.tlm_or_pseudo=(char *)calloc(number_of_mnems_in_db,
			sizeof(char));
				/* Allocate space for the telemetry/pseudo flag */

		UBG_init_globals();
			/* Initialize new globals. */
	}
}

/***
 *** initialize array gsa_tag with TAG values from *.gsa file 
 *** example of entries from file is shown below 
 ***
 ***    BeginMnemonic 30V_LVC_CRNT_SHRE_RATIO;
 ***    Tag=1;
 ***    Length=64;
 ***    ...
 ***    BeginMnemonic Z_CTCU_RT_UPLINK_CHA_ST;
 ***    Tag=9306;
 ***    Length=3;
 ***    ...
 ***/

gsa_tag_total = 0;

/* open gsa file */
sprintf(gsa_filename,"%s",database_name);
i = strlen(gsa_filename) - 4;
if (i > 0) {

	sprintf(&gsa_filename[i],"%s",".gsa");
	/* open gsa file */
	gsa_fileptr = fopen(gsa_filename,"r");
	if (!gsa_fileptr) {
		log_event_err("cannot open GSA file (%s)",gsa_filename);
		retval = -1;
	} else {
	
		/* read file until eof */
		while (gsa_fileptr) {
			/* read a line from file */
			if (fgets(&gsa_text[0],sizeof(gsa_text),gsa_fileptr) == NULL) break;
			
			/* get first 2 tokens in line */
			tok1 = (char *)strtok(gsa_text," =;\n");
			tok2 = (char *)strtok(NULL," =;\n");

			/* save mnemonic name */
			if (tok1 && !strcasecmp(tok1,"BeginMnemonic") && tok2) {
				strncpy(gsa_mnemonic,tok2,sizeof(gsa_mnemonic));
			}

			/* convert tag value */
			if (tok1 && !strcasecmp(tok1,"Tag") && tok2) {
				gsa_tag_total++;
				n = atoi(tok2);
				if (n > 0 && n < GSA_TAG_MAX) {
					gsa_tag[n] = 1;
					/*log_debug("gsa_tag[%5.5d]=%d for POINT=(%s)",n,gsa_tag[n],gsa_mnemonic);*/
				} else {
					log_event_err("cannot process GSA TAG=(%s) POINT=(%s)",tok2,gsa_mnemonic);
				}
			}	
		}
	log_event("Initialized %d tags from GSA file (%S)",gsa_tag_total,gsa_filename);				
	}
	
}

log_event("initialize_epoch completed. retval = %d",retval);

return retval;
	/* Success!  Or Failure... */
}
