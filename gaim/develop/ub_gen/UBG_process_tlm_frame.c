/*
@(#)  File Name: UBG_process_tlm_frame.c  Release 1.20  Date: 07/06/04, 16:42:49
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         SOURCE          :       UBG_process_tlm_frame.c
         EXE. NAME       :       Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles

         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation
         8.5         apr01-2004  K. Dolinh       Add log_event, log_debug
	 	 8.8         apr22-2004                  make sxi mistagged msg log_debug
	 	 8.B1        sep22-2004                  - new values for frame type SXI and WB
	                                         	 - add types MRSS...
         8.B3        oct05-2004                  remove unneeded dwell check
	 9.2         feb11-2005                  change debug msg
	 9A1         feb25-2005                  disable pcm1 timeout check
	 9A2         mar02-2005                  New schema using PCMx_FORMAT
	 9AP	     mar16-2005		 	         Build 9A Prime
	 			 apr25-2005                  - Use log_tlmwatch
				 may10-2005                  - do not send if mnemonics=0 
				 aug26-2005                  Build 10A
				                             - check point NORMAL_DWELL_MODE
											 - comment out processing of dwell format 3
		 11		 Aug18-2006		 	         Build 11
		 									 - update frame stat in shared mem
		 									 - change call to UBG_send_buffer											 
		 12		 May31-2006		 	         Build 12 
		 									 - process 2 new SXI frame types
		 	 
********************************************************************************
	Invocation:

		UBG_process_tlm_frame(int frame_size,char *frame_buffer)

	Parameters:

		frame_size - I
			The size of the frame packet.

		frame_buffer - I
			The frame packet from PUB.

********************************************************************************
	Functional Description:

		This routine takes the frame from PUB and formats it to be decommutated.

*******************************************************************************/

/**

PDL:
	Copy the time from the current tlemetry frame into the Update Buffer.
	Decommutate the telemetry frame.
	CALL UBG_set_data_header_status
	Set the telemetry frame type and status based upon the data extracted from
	 the frame.

	IF the Update_Buffer_Generator is processing merge data THEN
		Mark the frame as "send it now".
	ELSEIF the Update_Buffer_Generator is processing prime-prime real-time data
	 THEN
		Mark this frame as "send it now".
		Determine the type of data in this frame.
		IF the frame type has changed THEN
			Set the value of the EPOCH global variable GAIM_FORMAT_RTxx
			 (xx=scid) to the new data type.
		ENDIF
	ELSE
		Get the current value of GAIM_FORMAT_RTxx from EPOCH.
		IF GAIM_FORMAT_RTxx is 'No Data' THEN
			Mark this frame as "send it now".
		ELSEIF GAIM_FORMAT_RTxx is 'Dwell Format #' THEN
			Mark this frame as "send it now".
		ELSEIF GAIM_FORMAT_RTxx is 'Normal Format #' and the current frame type
		 is dwell data THEN
			Mark this frame as "send it now".
		ELSEIF GAIM_FORMAT_RTxx is 'Normal Format #' and the current frame type
		 is normal data THEN
			Mark this frame as "do not send".
		ELSEIF GAIM_FORMAT_RTxx is 'Not Active' THEN
			Mark this frame as "send it now".
			Write a messasge to the debug log stating that the PRIME-PRIME
			 stream has failed.
		ELSE
			Mark this frame as "send it now".
			Write a message to the debug log stating that an unknown condition
			 has occurred.
		ENDIF
	ENDIF

	IF the frame is marked as "send it now" THEN
		CALL UBG_create_buffer_to_send
		CALL UBG_send_buffer
	ENDIF

**/

#include <sys/timeb.h>

/* EPOCH Libraries */
#include "strmutil.p"
#include "db.h"
#include "tlm.p"
#include "sc.h"

#define PCM_FORMAT_NORMAL_1  0
#define PCM_FORMAT_NORMAL_2  1
#define PCM_FORMAT_NORMAL_3  2
#define PCM_FORMAT_DWELL_1   8
#define PCM_FORMAT_DWELL_2   9
#define PCM_FORMAT_DWELL_3   10

#define PCM_TIMEOUT	10000   /* in millisecs */

/* Update_Buffer_Generator structures */
#include "UBG_structures.h"
#include "../include/util_log.h"
	
/* 
 * data used by tlm watch 
 */
extern char *tlmwatch_buf;
extern int  tlmwatch_max;

/* frame format info */
extern int pcm1_format;
extern int pcm2_format;
extern int normal_dwell_mode;
#define NORMAL_DWELL_MODE_NORMAL 0
#define NORMAL_DWELL_MODE_DWELL  1

/* env processing flags */
extern int frame_bad_timetag;

/* shared mem */
extern GaimStat *gaimstat;

/* ... */
int pcm1_type=-1;
int pcm2_type=-1;
int pcm1_gap = 0;
int pcm2_gap = 0;
int last_pcm1_time=-1;
int last_pcm2_time=-1;

int	prev_frame_end_year = 0;
int	prev_frame_end_day  = 0;
int	prev_frame_end_ms   = 0;

void UBG_process_tlm_frame(int frame_size,char *frame_buffer) {

	int i;
		/* Counters */

	tlm_frame *to_be_decommed;
  char *cptr;
		/* Telemetry frame structure */

	char *buffer_to_send;
		/* The update buffer to be sent. */

	int full_buffer_length=0;
		/* The length of the full update buffer */

	char *frame_buffer_ptr;
		/* Make a pointer that can be modified with out any unpleasant
			side effects. */

	int frame_type;
		/* Frame type */

	int frame_ms,frame_day,frame_year;
		/* Time Variables */

	char message[512];
		/* Debug message */

	int no_pseudos=0;
		/* Set to -1 if PCM2.  Reset to 0 if in test mode (SEND_PSEUDOS is
			defined) */

	int status=0;
		/* Status flag */

	struct timeb time;

	int  j;
	
	/* get current time 
	ftime(&time); */
	
	/* Copy the frame time, so that pseudos can be time stamped correctly */
	frame_buffer_ptr=frame_buffer;
	memcpy(&current_frame_time,frame_buffer_ptr,16);
	frame_buffer_ptr+=16;
	frame_size-=16;	

	/* Calculate the number of milliseconds since the start of the day */
	frame_ms = (current_frame_time.tv_sec%86400)*1000 + (current_frame_time.tv_usec/1000);
	
	/* Calculate the day of the current frame. */
	frame_day=current_frame_time.tv_sec/86400;
	
	/* Convert from days to year-day */	
	UBG_days_and_years(&frame_day,&frame_year);
		
	/* Modify the data header with the base time and zero the stat bits. */
	update_buffer.data_header.year=frame_year;
	update_buffer.data_header.day=frame_day;
	update_buffer.data_header.milliseconds=frame_ms;
	update_buffer.data_header.status=0;
		
	/* Start new update buffer */
	update_buffer.number_of_mnemonics=0;
		
	/* Allocate space for the new frame */
	to_be_decommed = (struct tlm_frame *)calloc(1,frame_size);
			
    /* Since the telemetry frame is sent "as is" with header, quality, etc. */
	memcpy(to_be_decommed,frame_buffer_ptr,frame_size);

	/* Get the current stream_source */
	frame_type_value = (int)((short)to_be_decommed->frame[0]);
		
	/* init format info */
	pcm1_format = -1;
	pcm2_format = -1;
	normal_dwell_mode = NORMAL_DWELL_MODE_NORMAL;
	
	/* update time gap between pcm frames */
	if (last_pcm1_time<0) last_pcm1_time = frame_ms;
	else pcm1_gap = frame_ms - last_pcm1_time;
	if (last_pcm2_time<0) last_pcm2_time = frame_ms;
	else pcm2_gap = frame_ms - last_pcm2_time;
		
	/* check frame overlap if realtime archive and flag set */		
	if (run_mode==2 && prev_frame_end_ms > 0) {
		/* drop frame if frame start_time < stop_time of prev frame */
		if ((frame_day < prev_frame_end_day) ||
		    (frame_day==prev_frame_end_day && frame_ms<prev_frame_end_ms) ) {
			if (frame_bad_timetag == 1) {
				log_tlmwatch("WARNING: possible frame overlap. Frame(type=%d  start_day=%d  start_ms=%d  prev_end_day=%d  prev_end_ms=%d  diff=%d)\n\n",
					frame_type_value,frame_day,frame_ms,prev_frame_end_day,prev_frame_end_ms,(frame_ms - prev_frame_end_ms));
			} else if (frame_bad_timetag == 2) {
				log_tlmwatch("WARNING: frame overlap not processed. Frame(type=%d  start_day=%d  start_ms=%d  prev_end_day=%d  prev_end_ms=%d  diff=%d)\n\n",
					frame_type_value,frame_day,frame_ms,prev_frame_end_day,prev_frame_end_ms,(frame_ms - prev_frame_end_ms));			
			return;
			}
		}
	}
					
	/* Decommutate the frame. */
	decom_frame(stream_def->dbhead,to_be_decommed);

	/* increment frame count in shared mem */
	gaimstat->frames_all++;

	/* Set the status bits in the data header */
	UBG_set_data_header_status();
		
	/* set minor frame count into header */
	update_buffer.data_header.minor_frame_number = (char)((unsigned short)minor_frame_count_value);	
	
	/* set buffer type based on frame type */
    if (frame_type_value==MICRO_DWELL_FRAME_TYPE) {
		update_buffer.data_header.frame_type=UBG_DWELL_CODE;
		update_buffer.data_header.status|=UBG_DWELL_DATA;
		
	} else if (frame_type_value==PCM1_FRAME_TYPE) {
		if (normal_dwell_mode==NORMAL_DWELL_MODE_DWELL) {
			update_buffer.data_header.frame_type=UBG_DWELL_CODE;
			update_buffer.data_header.status|=UBG_DWELL_DATA;
			gaimstat->frames_dwell++;		
		} else {
			update_buffer.data_header.frame_type=UBG_PCM1_CODE;
			update_buffer.data_header.status|=UBG_SOURCE_PCM1;
			update_buffer.data_header.status|=UBG_NORMAL_DATA;
			last_pcm1_time = frame_ms;
			gaimstat->frames_normal++;
		}
	} else if (frame_type_value==PCM2_FRAME_TYPE) {
		if (normal_dwell_mode==NORMAL_DWELL_MODE_DWELL) {
			update_buffer.data_header.frame_type=UBG_DWELL_CODE;
			update_buffer.data_header.status|=UBG_DWELL_DATA;		
			gaimstat->frames_dwell++;
		} else {
			update_buffer.data_header.frame_type=UBG_PCM2_CODE;
			update_buffer.data_header.status|=UBG_SOURCE_PCM2;
			update_buffer.data_header.status|=UBG_NORMAL_DATA;
			last_pcm2_time = frame_ms;
			gaimstat->frames_normal++;
		}
		
	} else if (frame_type_value==IMGWB_FRAME_TYPE) {
			update_buffer.data_header.frame_type=UBG_WBI_CODE;
			gaimstat->frames_wbi++;
			
	} else if (frame_type_value==SNDWB_FRAME_TYPE) {
			update_buffer.data_header.frame_type=UBG_WBS_CODE;
			gaimstat->frames_wbs++;
			
	} else if (frame_type_value==MRSSHK_FRAME_TYPE ||
			   frame_type_value==SXI_SEQ_FRAME_TYPE || 
			   frame_type_value==SXI_HEALTH_FRAME_TYPE ) {
		update_buffer.data_header.frame_type=UBG_SXI_CODE;
			gaimstat->frames_sxi++;
		
	} else if (frame_type_value==AGC_FRAME_TYPE) {
		update_buffer.data_header.frame_type=UBG_AGC_CODE;
	
	} else {
		update_buffer.data_header.frame_type=UBG_NO_CODE;
	}

	update_buffer.data_header.data_size=SIZE_DATA_HEADER_BASE+FORCED_MAX_DB_NAME_LEN;
  /* 16 + 32 */

	/* Calculate the size of the buffer that will be sent. */
	for (i=0; i<update_buffer.number_of_mnemonics; i++) {
		if (update_buffer.tlm_or_pseudo[i]==0) {
			update_buffer.data_header.data_size+=SIZE_DATA_BODY_BASE+
				mask_to_byte_translation[UBG_GET_DATA_MASK(update_buffer.data_body[i].status_flags)];

		}

	}

	/* output frame debug info */	
	if (tlmwatch_max) {
		log_tlmwatch("FRAME(Count=%2.2d  Start=%d.%d  Stop=%d.%d  Len_ms=%d  Type=%2.2d)  BUFFER(Type=%2.2d  Mnemonics=%d)  pcm1_fmt=%d  pcm1_gap=%d  pcm2_fmt=%d  pcm2_gap=%d",
			minor_frame_count_value,frame_day,frame_ms,
			prev_frame_end_day,prev_frame_end_ms,(prev_frame_end_ms - frame_ms),
			frame_type_value,
			update_buffer.data_header.frame_type,update_buffer.number_of_mnemonics,
			pcm1_format,pcm1_gap,pcm2_format,pcm2_gap);
		tlmwatch_max--;
	}
	
	log_debug("BTS1: num_mnem = %d, dthdr.dsize = %d, mfcnt = %d, frmsize = %d",
		update_buffer.number_of_mnemonics, update_buffer.data_header.data_size,
		update_buffer.data_header.minor_frame_number, frame_size);

	log_debug("MSGHDR:  msg# = %d, nname = %s, pname = %s",
			update_buffer.message_header.message_number,
			update_buffer.message_header.node_name, update_buffer.message_header.process_name);

	log_debug("DATAHDR: datasz = %d, gtacs = %d, scid = %d, frmtype = %d, frm# = %d, dbname = %s",
			update_buffer.data_header.data_size, update_buffer.data_header.gtacs_name,
			update_buffer.data_header.scid, update_buffer.data_header.frame_type,
			update_buffer.data_header.minor_frame_number, update_buffer.data_header.database_name);

	log_debug("DATAHDR: year = %d, day = %d, millisec = %d, status = %x",
			update_buffer.data_header.year, update_buffer.data_header.day,
			update_buffer.data_header.milliseconds, update_buffer.data_header.status);

	/* for test purpose only ..Lata */
	if ((update_buffer.number_of_mnemonics > 970) && (update_buffer.data_header.frame_type != 21))
	{
		log_debug("SXI pcm mis-tagged telemetry frame");
	}

	/* Allocate space for the update buffer.  The total space
		allocated will be the size of the message header plus the
		size of the data header plus the size of each mnemonics in
		the buffer.  The extra 256 bytes allocated allow for
		strings to fill in the spots of char pointers. */

	buffer_to_send=(char *)calloc(sizeof(update_buffer.message_header)+
		update_buffer.data_header.data_size+256,sizeof(char));

	/* If merge data (17 to 32) or non-prime real time (9 to 16), always send the frame. */
	if (process_number_value>8) send_data=-1;
	
	/* If prime prime (1,3,5,7) and not secondary prime (2,4,6,8), always send the frame. */	
	else if ((process_number_value%2)==1) {
	
		/* Yes.  Send this frame! */
		send_data=-1;
		
		if (tlm_format_value != last_tlm_format_value) {

			/* Get current value of GAIM_FORMAT_RTxx and update global var */
			status = poll_point(gaim_format_rt_pid);
				
			if (status != STRM_SUCCESS) {
				log_debug("ERROR(%i) from poll_point GAIM_FORMAT_RT%2.2i: %s",
					status,spacecraft_id_value,stream_err_messages[0-status]);
			} else  {
				strcpy((char *)gaim_format_rt_pid->value,gaim_tlm_mode_names[tlm_format_value]);
				gaim_format_rt_pid->status_bits = 0;
				status = set_point(gaim_format_rt_pid);
			}
			status = flush_sets();
			if (status!=STRM_SUCCESS) {
				log_debug("process_tlm_frame: flush_sets error=%i: %s",status,stream_err_messages[0-status]);
			}
			last_tlm_format_value = tlm_format_value;
		}

	} else {
	
		/* If secondary prime, need to do some additional calculations. */
		/* Get current value of GAIM_FORMAT_RTxx */
		status = poll_point(gaim_format_rt_pid);
		if (status!=STRM_SUCCESS) {

			log_debug("ERROR(%i) from poll_point(%s%2.2i): %s",
				status,"GAIM_FORMAT_RT",spacecraft_id_value,
				stream_err_messages[0-status]);

		} else strcpy(gaim_format_rt_value,(char *)gaim_format_rt_pid->value);
					/* Get the current SCID status */

		/* If the case is not listed, default to not sending the stream */
		send_data = 0;		

		if (gaim_format_rt_value[2]==' ') send_data = -1;
			/* If PRIME-PRIME reads "No Data", send the SECONDARY_PRIME. */
			
		else if (gaim_format_rt_value[2]=='e') send_data = -1;
			/* If PRIME-PRIME reads "Dwell Format x", send the
				SECONDARY_PRIME.  NOTE: This means that if there is ever
				the case where we have dwell data is on both a and b,
				both a and b will be forwarded to the GOES archive, but
				this should never happen according to Steve Walsh as of
				7/26/2000. */
				
		else if (gaim_format_rt_value[2]=='r' && tlm_format_value>4) send_data = -1;
				/* If PRIME-PRIME reads "Normal Format x" AND SECONDARY-PRIME
					has Dwell data on it, send the SECONDARY_PRIME */
					
		else if (gaim_format_rt_value[2]=='r' && tlm_format_value<4) send_data = 0;
				/* If PRIME-PRIME reads "Normal Format x" AND SECONDARY-PRIME
					has Normal data on it, DO NOT send the SECONDARY_PRIME */
					
		else if (gaim_format_rt_value[2]=='t') {
				/* If PRIME-PRIME reads "Not Active", send the SECONDARY_PRIME.
					If this condition happens, the PRIME_PRIME stream has died,
					so we MUST send the SECONDARY-PRIME stream.  But, we also
					should let the user know what is happening. */
			send_data = -1;
			log_debug("PRIME-PRIME stream has terminated!  SECONDARY-PRIME is still active!");

		} else {
				/* unknown combination of tlm, send when in doubt */
			send_data = -1;
			log_debug("Unknown tlm combination seen. frames is being forwarded");
		}
	}

	/* If PCM1 has been gone for more than 10 seconds, and both
	   PCM1 and PCM2 are in NORMAL mode, and this is a PCM2 frame,
	   do not send the frame 
	if (time.time-last_pcm1_time <= PCM1_TIMEOUT &&
		pcm1_type==UBG_NORMAL_DATA && pcm2_type==UBG_NORMAL_DATA &&
		(update_buffer.data_header.status&UBG_SOURCE_PCM2)==UBG_SOURCE_PCM2)
		send_data = 0;
	*/

/* special processing for pcm frames */
if (frame_type_value==PCM1_FRAME_TYPE || frame_type_value==PCM2_FRAME_TYPE) {
	
	/* determine if frame is normal */
	if (pcm1_format==PCM_FORMAT_NORMAL_1 || pcm1_format==PCM_FORMAT_NORMAL_2 || pcm1_format==PCM_FORMAT_NORMAL_3)
		pcm1_type==UBG_NORMAL_DATA;
	if (pcm2_format==PCM_FORMAT_NORMAL_1 || pcm2_format==PCM_FORMAT_NORMAL_2 || pcm2_format==PCM_FORMAT_NORMAL_3)
		pcm2_type==UBG_NORMAL_DATA;

	/* if this is pcm1 normal and there was pcm2 normal in the last N seconds, do not send frame */
	if (pcm1_type==UBG_NORMAL_DATA && pcm2_gap <= PCM_TIMEOUT) {
		send_data = 0;		
	}
	
	/* if this is pcm1 in dwell fmt 3, send data as dwell buffer 			
	if (pcm1_format==PCM_FORMAT_DWELL_3) {
		update_buffer.data_header.frame_type=UBG_DWELL_CODE;
		update_buffer.data_header.status|=UBG_DWELL_DATA;
	}*/
}

/* do not send if buffer contains no mnemonic */
if (update_buffer.number_of_mnemonics <= 0) send_data = 0;

if (tlmwatch_max) {
	j = sizeof(update_buffer.message_header)+update_buffer.data_header.data_size+256;
	log_tlmwatch("PID=%d  SEND_FLAG=%d  BufferStatus=x%4.4X  BufferType=%d  ms=%d  mnemonics=%4.4d  buffer_len=%d\n\n",
			process_number_value,send_data,
			update_buffer.data_header.status,
			update_buffer.data_header.frame_type,
			update_buffer.data_header.milliseconds,
			update_buffer.number_of_mnemonics,j);
}	
		
/* Do we need to send this frame? */
if ((send_data) && (buffer_to_send != (void *)NULL)) {

	/* Create the actual buffer to send. */
	UBG_create_buffer_to_send(&full_buffer_length,buffer_to_send,0);

	/* send the buffer */
	UBG_send_buffer(DATATYPE_FRAME,full_buffer_length,buffer_to_send);

	/* Increment message number for next message */
	update_buffer.message_header.message_number++;

} else {
	/* Do not wait, since we are not sending. */
	waiting_for_response = 0;
}

/* No pseudo to send yet */
send_pseudo = 0;

/* Free up memory */
free(buffer_to_send);

/* Free up memory */
free(to_be_decommed);


}
