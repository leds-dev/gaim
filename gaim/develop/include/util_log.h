/*
@(#)  File Name: util_log.h  Release 1.4  Date: 07/03/25, 08:01:55
*/

/***
   data used by utility routines: log_event, log_debug, log_err
   see source code in "util_log.c" 

   version: 10A1  Sep05-2005      add var logfile_day	
          : 11    Aug18-2006      add shared memory for gaim stats	  

***/
#ifndef UTIL_LOG_H
#define UTIL_LOG_H

#include <stdio.h>
#include <sys/types.h>

void create_eventfile (void);
void log_event (char *fmt,...);
void log_event_err (char *fmt,...);
void log_debug (char *fmt,...);
void log_err   (char *fmt,...);
void log_daily_statistics (int year,int day);

static int  logfile_day = 0;
static int  logfile_open_err = 0;
static char logfile_name[1024];
static char logfile_savename[1024];
static char logfile_msgid[16] = "???";
static FILE *logfile_ptr = NULL;

static int  errfile_max = 1024000;
static int  errfile_open_err = 0;
static char errfile_name[1024];
static char errfile_savename[1024];
static char errfile_msgid[16] = "???";
static FILE *errfile_ptr = NULL;

static char *gaim_home=NULL;
extern int debug;

/* gaim statistics are stored in shared memory 
   created by IFM and attached by other processes 
   there are GAIM_SHM_ITEMS slots allocated  
   slot 0   to N   -> sc13 to scXX prime
   slot N+1 to ... -> sc13 to scXX nonprime
    ...
   the last slot is used when the scid is invalid 
   */
   
#define GAIM_SHM_ITEMS   20

struct GaimStat {
	int scid;
	char stream[16];
	int prime;
	int frames_all;
	int frames_normal;
	int frames_dwell;
	int frames_wbi;
	int frames_wbs;
	int frames_sxi;	
	int files_pseudo;
	int buffers_all;
	int buffers_normal;
	int buffers_dwell;
	int buffers_wbi;
	int buffers_wbs;
	int buffers_sxi;
	int buffers_pseudo;	
	int buffers_automerge;
	int buffers_manualmerge;
	int buffers_badquality;
	int buffers_sent;
	int errs;
};
typedef struct GaimStat GaimStat;

static int  shmid;
static key_t key = 11440;	/* just a random number */
static char *shm;
static GaimStat gaimstat_bitbucket; /* used as bit bucket if attach shared memory err */

GaimStat *gaim_shm_create (void);
GaimStat *gaim_shm_attach (int scid, char prime);

#endif
