/*  
@(#)  File Name: GAIM_update_buffer_defines.h  Release 1.2  Date: 00/11/01, 12:59:30
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         HEADER          :       GAIM_update_buffer_defines.h
         EXE. NAME       :       Playback_Update_Buffer, Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation

********************************************************************************
         Functional Description:

         This header file contains defines used by Playback_Update_Buffer and 
		 Update_Buffer_Generator.
	
*******************************************************************************/

#define UBG_FRAME_PKT_NUMBER   0x0000
#define UBG_GLOBAL_PKT_NUMBER  0x0001
#define UBG_PSEUDO_PKT_NUMBER  0x0002
#define UBG_DBASE_PKT_NUMBER   0x0003
#define UBG_GTACS_PKT_NUMBER   0x0004
#define UBG_EOH_EOI_PKT_NUMBER 0x0005

#define UBG_NUM_PKT_NUMS 6

static short packet_ids[UBG_NUM_PKT_NUMS]={UBG_FRAME_PKT_NUMBER, 
	UBG_GLOBAL_PKT_NUMBER, UBG_PSEUDO_PKT_NUMBER, UBG_DBASE_PKT_NUMBER, 
	UBG_GTACS_PKT_NUMBER, UBG_EOH_EOI_PKT_NUMBER};

static int init_messages=UBG_NUM_PKT_NUMS-5;
	/* How many initialization messages are there? */

static char scnames[22][8]={"nogoes","nogoes","nogoes","nogoes","nogoes",
	"nogoes","nogoes","nogoes","nogoes","nogoes","nogoes","nogoes","nogoes",
	"goes13","goes14","goes15","goes16","goes17","goes18","goes19","goes20",
	"goes21"};

#define MAX_SDR_PROCESSES 16
#define MAX_PUB_PROCESSES 16
#define START_SDR_CHANNEL 1
#define START_PUB_CHANNEL 17
	/* Set up 32 sockets - 1 through 16 for SDR, 17 through 32 for PUB */

#define ERROR_MESSAGE_HEADER "z!z!z!z!"
	/* Instead of "goodbye" message "zzzzzzzz", this is sent when there is 
		an error. */

