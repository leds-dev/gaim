/*
@(#)  File Name: util_log.c  Release 1.6  Date: 07/04/07, 14:35:39
*/

/*** source code for gaim utility routines: log_event, log_debug, log_err
     see data definition in "util_log.h"
     all gaim processes write to the same log file but each event msg
     carries a 3-char process id (IFM,CGT,SDR,...)

   log_event:     write a formatted message to the log file
   log_event_err: same as log_event, but message contains a error tag
   log_debug:     same as log_event, but message contains a debug tag and
                  is only output if debug is enabled


   version: 8.1	  mar02-2004
   version: 8.2   mar04-2004      use logfile_msgid instead of SW_ID
   version: 8.8   apr21-2004      modify printf text when renaming file
                                  need to add semaphore lock
   version: 8.B   sep23-2004      check open status when renaming file
   version: 9.C   jul18-2005      add file-locking, create "gaim.log" as link 
								  add create_eventfile 
   version: 10A1  Sep05-2005      add semaphore file ../output/TMP/newlog_IFM,...							  
          : 11    Aug18-2006      add shared memory for gaim stats
		  		  Apr03-2007	  fix print statement in log_daily__statistics
	  
***/

/* #include <sys/ucb_file.h> */
#include <errno.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

int debug=0;

/***
 *** only called by Interface Manager to create new log file
 ***/
 
void create_eventfile (void)
{
char	cmd[1024];
int status;
time_t current_time;
struct tm current_time_str;
int		i;
int  year,day;

/* get current time */
status = time(&current_time);
current_time_str = *localtime(&current_time);

/* write debug msg 
log_err("checking event file:  logfile_day=%d  hour=%d\n",
	logfile_day,current_time_str.tm_hour); */

/* close log file if new day 
/*if (logfile_day == current_time_str.tm_min) {}*/
if (logfile_day != current_time_str.tm_yday+1) { 

	/* write daily stats to logfile */
	year = current_time_str.tm_year+1900;
	day = current_time_str.tm_yday;
	if (logfile_ptr) log_daily_statistics(year,day);
	/* close logfile */
	log_event("\nclosing GAIM log file (%s)",logfile_name);	
	fclose(logfile_ptr);
    logfile_ptr = NULL;
    logfile_open_err = 0;
}

/* open logfile if no file descriptor */
if (logfile_ptr == NULL && logfile_open_err == 0) {

		/* save filename as YYYY_DDD_HHMMSS.log */
		/* sprintf(logfile_name,"%s/output/gaim.log",gaim_home); */
		sprintf(logfile_name,"%s/output/%4.4i_%3.3i_%2.2i%2.2i%2.2i.log",
			gaim_home,
            current_time_str.tm_year+1900,
			current_time_str.tm_yday+1,
            current_time_str.tm_hour,
			current_time_str.tm_min,
            current_time_str.tm_sec);

		/* save day logfile is opened */
		/* logfile_day = current_time_str.tm_min+5; */
		logfile_day = current_time_str.tm_yday+1;

			
		/* create new log file */
		logfile_ptr = fopen(logfile_name,"w");
		if (logfile_ptr == NULL) {
			log_err("error creating GAIM log file (%s)",logfile_name);
			logfile_open_err++;
			return;
		} 
		
		/* remove old link */
		sprintf(cmd,"rm %s/output/gaim.log",gaim_home); 
		system(cmd); 
					
		/* create "gaim.log" link to current log file */
		sprintf(cmd,"ln -s %s %s/output/gaim.log",logfile_name,gaim_home);
		system(cmd); 
		
		/* close file */
		fclose(logfile_ptr);
    	logfile_ptr = NULL;
    	logfile_open_err = 0;	
				
		/* create newlog semaphore for all processes */
		system("touch /tmp/gaim.newlog.IFM"); 
		system("touch /tmp/gaim.newlog.CGT"); 
		system("touch /tmp/gaim.newlog.UBS"); 
		
		for (i=1; i<40; i++) {
			sprintf(cmd,"touch /tmp/gaim.newlog.SDR.%d",i);
			system(cmd);
			sprintf(cmd,"touch /tmp/gaim.newlog.UBG.%d",i);
			system(cmd);
			sprintf(cmd,"touch /tmp/gaim.newlog.PUB.%d",i);
			system(cmd);										 		
		}
		
		/* force write to new event file */
		log_event("opened new GAIM log file (%s)",logfile_name);

}

} /* end create_eventfile */



void log_event (char *fmt,...)
{
static  char    outbuf[1024];
        int     len;
        va_list ap;

int status;
time_t current_time;
struct tm current_time_str;
fpos_t filelen;
char timetag[32];
int 	locked = 0;

char	cmd[1024];
char	newlog[1024];
struct stat tmp_stat;
/*
if (logfile_ptr) {
	/* get lock, will block until successful 
	if (!flock(logfile_ptr,LOCK_EX)) {
		log_err("[%s] log_event get file lock error\n",logfile_msgid);
		return;
	}
}
*/

/* get current time */
status = time(&current_time);
current_time_str = *localtime(&current_time);

/* format message text into outbuf */
if (fmt) {
	va_start (ap, fmt);
	if (!strchr(fmt, '%'))
		strcpy(outbuf, fmt);
	else
		vsprintf(outbuf, fmt, ap);
	va_end(ap);
} else {
	return;
}

len = strlen(outbuf);
if (len > 0 && outbuf[len-1] == '\n') {
	outbuf[len-1] = '\0';
 	len--;
}

sprintf(newlog,"/tmp/gaim.newlog.%s",logfile_msgid); 
/*status = stat(newlog,&tmp_stat);
log_err("STAT(%s) = %d",newlog,status);*/

if (stat(newlog,&tmp_stat) == 0) {
	/* new log file semaphore found, remove it */
	sprintf(cmd,"rm %s",newlog); 
	system(cmd); 
	log_err("*** removed semaphore (%s)",newlog);
	if (logfile_ptr) fclose(logfile_ptr);
	logfile_ptr = NULL;		
}
			

/* open logfile if no file descriptor */
if (logfile_ptr == NULL && logfile_open_err == 0) {
	sprintf(logfile_name,"%s/output/gaim.log",gaim_home);
	logfile_ptr = fopen(logfile_name,"a");
	if (logfile_ptr == NULL) {
		log_err("cannot open GAIM logfile (%s)",logfile_name);
		logfile_open_err++;
		return;
	} 
	/*log_err("opened GAIM logfile %s",logfile_name);*/
}

/* write event message to stdout if no logfile */
if (logfile_ptr==NULL) { 
    printf("%4.4i-%3.3i-%2.2i:%2.2i:%2.2i  [%s] %s\n",
            current_time_str.tm_year+1900,current_time_str.tm_yday+1,
            current_time_str.tm_hour,current_time_str.tm_min,
            current_time_str.tm_sec,
			logfile_msgid,outbuf);
	return;
}
			
/* write event message to log file */
status = fprintf(logfile_ptr,"%4.4i-%3.3i-%2.2i:%2.2i:%2.2i  [%s] %s\n",
            current_time_str.tm_year+1900,current_time_str.tm_yday+1,
            current_time_str.tm_hour,current_time_str.tm_min,
            current_time_str.tm_sec,
			logfile_msgid,outbuf);
status = fflush(logfile_ptr);

/*
filelen = 0;
status = fgetpos(logfile_ptr,&filelen);
*/

/***
if (status > 0) {
	/* flush buffer 
	fflush(logfile_ptr);
	/* release file lock 
	if (!flock(logfile_ptr,LOCK_UN)) 
		log_err("[%s] log_event release file lock error\n",logfile_msgid); 
} else {
	/* write error 
    logfile_ptr = NULL;
    logfile_open_err = 0;
	log_err("log_event: fprintf error=%d\n",status);	
}
***/

} /* end log_event */

/***
 *** LOG_EVENT_ERR
 ***/

void log_event_err (char *fmt,...)
{
static  char    outbuf[1024];
static  char    newbuf[1024];
        int     len;
        va_list ap;

/* format message text into outbuf */
if (fmt) {
	va_start (ap, fmt);
	if (!strchr(fmt, '%'))
		strcpy(outbuf, fmt);
	else
		vsprintf(outbuf, fmt, ap);
	va_end(ap);
} else {
	return;
}

len = strlen(outbuf);
if (len > 0 && outbuf[len-1] == '\n') {
	outbuf[len-1] = '\0';
 	len--;
}

sprintf(newbuf,"(ERR) %s",outbuf);
log_event(newbuf);

} /* end log_event_err */


/***
 *** LOG_DEBUG
 ***/

void log_debug (char *fmt,...)
{
static  char    outbuf[1024];
static  char    newbuf[1024];
        int     len;
        va_list ap;

/* return if not in debug mode */
if (!debug) return;

/* format message text into outbuf */
if (fmt) {
	va_start (ap, fmt);
	if (!strchr(fmt, '%'))
		strcpy(outbuf, fmt);
	else
		vsprintf(outbuf, fmt, ap);
	va_end(ap);
} else {
	return;
}

len = strlen(outbuf);
if (len > 0 && outbuf[len-1] == '\n') {
	outbuf[len-1] = '\0';
 	len--;
}

sprintf(newbuf,"(DBG) %s",outbuf);
log_event(newbuf);

} /* end log_debug */



void log_err (char *fmt,...)
{
static  char    outbuf[1024];
        int     len;
        va_list ap;

        int status;
        time_t current_time;
        struct tm current_time_str;


/* Format message text */

if (fmt) {
	va_start (ap, fmt);
	if (!strchr(fmt, '%'))
		strcpy(outbuf, fmt);
	else
		vsprintf(outbuf, fmt, ap);
	va_end(ap);
} else {
	return;
}

len = strlen(outbuf);
if (len > 0 && outbuf[len-1] == '\n') {
	outbuf[len-1] = '\0';
 	len--;
}

/* write error message to stdout */

status = time(&current_time);
current_time_str = *localtime(&current_time);
printf("%4.4i-%3.3i-%2.2i:%2.2i:%2.2i  [%s] *** %s\n",
            current_time_str.tm_year+1900,current_time_str.tm_yday+1,
            current_time_str.tm_hour,current_time_str.tm_min,
            current_time_str.tm_sec,
	    logfile_msgid,outbuf);
fflush(stdout);

} /* end log_err */


/***
 *** create gaim shared memory
 ***/
GaimStat * gaim_shm_create(void)
{
GaimStat *gaimstat,*s;
unsigned long	i;
int		  offset;

log_event("creating (%d * %d = %d ) bytes of shared memory",
	GAIM_SHM_ITEMS,sizeof(GaimStat),
	GAIM_SHM_ITEMS*sizeof(GaimStat));

/* create shared memory */
gaimstat = NULL;	
if ((shmid = shmget(key,GAIM_SHM_ITEMS*sizeof(GaimStat),IPC_CREAT | 0666)) <  0) {
	/* error creating shared memory */
	log_event_err("error creating shared memory");	
}

sleep(1);

/* attach shared memory */
if ((shm = shmat(shmid,NULL,0)) == (char *) -1) {
	log_event_err("error attaching created shared memory");
} else {
	gaimstat = (GaimStat *)shm;
	/* init values in shared mem */
	for (i=0; i<GAIM_SHM_ITEMS; i++) { 
		s = (GaimStat *) ((unsigned long) gaimstat + (unsigned long) sizeof(GaimStat)*i);
		/*log_event("*** TOTAL=%d  i=%d  s = %8.8X  (%8.8X + %8.8X)",GAIM_SHM_ITEMS,i,s,gaimstat,sizeof(GaimStat)*i);*/
		
		if (i < GAIM_SHM_ITEMS/2) {
			s->prime = 1;
			offset = 0;
		} else {
			s->prime = 0;	
			offset = GAIM_SHM_ITEMS/2;
		}				
		/* init structure fields */
		s->scid   = 13 + (i - offset);
		sprintf(s->stream,"NONE ");
		s->frames_all = 0;
		s->frames_normal = 0;
		s->frames_dwell = 0;
		s->frames_wbi = 0;
		s->frames_wbs = 0;
		s->frames_sxi = 0;
		s->files_pseudo = 0;
		s->buffers_all = 0;
		s->buffers_normal = 0;
		s->buffers_dwell = 0;
		s->buffers_wbi = 0;
		s->buffers_wbs = 0;
		s->buffers_sxi = 0;
		s->buffers_pseudo = 0;
		s->buffers_automerge = 0;
		s->buffers_manualmerge = 0;
		s->buffers_badquality = 0;
		s->buffers_sent = 0;

	}	
}
	
return gaimstat;

} /* end gaim_shm_create */



/***
 *** attach to gaim shared memory
 ***/
GaimStat * gaim_shm_attach (int scid,char prime)
{
int	index,offset;
GaimStat *s; 

if (prime == 'P')
	offset = 0;
else
	offset = GAIM_SHM_ITEMS/2;
	
if (scid >= 13 && scid <= 20) 
	index = scid - 13 + offset;
else 
	/* invalid sc, point to last item */
	index = GAIM_SHM_ITEMS/2 + offset;
	
/* create shared memory */
if ((shmid = shmget(key,GAIM_SHM_ITEMS*sizeof(GaimStat), 0666)) <  0) {
	/* error creating shared memory */
	log_event_err("error attaching shared memory");	
}
/* attach shared memory */
if ((shm = shmat(shmid,NULL,0)) == (char *) -1) {
	log_event_err("error attaching shared memory");
	/* point to bit bucket */
	s = (GaimStat *) &gaimstat_bitbucket;
} else {
	/* point to shared memory */
	s = (GaimStat *) ((unsigned long) shm + (unsigned long) index*sizeof(GaimStat)); 
}

/* scid 17 is special test case */
if (scid != 17)
	log_event ("gaim_shm_attach(%d,%c) --> index=%d  sc=%d  prime=%d  stream=%s",
	scid,prime,index,
	s->scid,s->prime,s->stream);	
	
return s;

} /* end gaim_shm_attach*/


/*** 
 *** Write daily statistics to logfile gaim.log
 *** and clear frames and buffers count
 *** 
 ***/
void log_daily_statistics (int year, int day)
{
int  dailyfile_open_err = 0;
char dailyfile_name[1024];
static FILE *dailyfile_ptr = NULL;

int j;
char  line1[1024], line2[1024], line3[1024];
GaimStat  *gaimstat, *s;

/* attach to shared memory */
gaimstat = (GaimStat *)gaim_shm_attach(13,'P');

/* open new daily stat file (YYYY_DDD_gaim.sta) */
sprintf(dailyfile_name,"%s/output/%4.4i_%3.3i_gaim.sta",gaim_home,year,day);
dailyfile_ptr = fopen(dailyfile_name,"w");
if (dailyfile_ptr == NULL) {
	log_err("error creating daily log file (%s)",dailyfile_name);
} 

log_event("\n\n                   *** GAIM Daily Statistics ***\n");
for (j=0; j<GAIM_SHM_ITEMS; j++) { 

	/* get address of shared mem for sc */
	s = (GaimStat *) ((unsigned long) gaimstat + (unsigned long) sizeof(GaimStat)*j);
	
	if (s->scid >= 13 && s->scid <= 17) {
		/* format stat lines */
		sprintf(line1,"SCID=%2.2d  Prime=%1.1d  Stream=%5.5s",
				s->scid,s->prime,s->stream);
		sprintf(line2,"        Frames=%-7d   Normal=%-7d    DWELL=%-6d   WDI=%-6d  WDS=%-6d  SXI=%-6d  Pseudo=%-6d",
				s->frames_all,s->frames_normal,s->frames_dwell,s->frames_wbs,s->frames_wbi,s->frames_sxi,s->files_pseudo);
		sprintf(line3,"       Buffers=%-7d   AMerge=%-7d    MMerge=%-6d   BadQual=%-6d  Sent=%-6d",
				s->buffers_all,s->buffers_automerge,s->buffers_manualmerge,s->buffers_badquality,s->buffers_sent);
		
		/* write stat lines to event log */
		log_event(line1);
		log_event(line2);
		log_event(line3);	
		
		/* write stat lines to daily stat file */
		if (dailyfile_ptr)	fprintf(dailyfile_ptr,"\n%s\n%s\n%s\n",line1,line2,line3);
 
		/* clear statistics */
		s->frames_all = 0;
		s->frames_normal = 0;
		s->frames_dwell = 0;
		s->frames_wbi = 0;
		s->frames_wbs = 0;
		s->frames_sxi = 0;
		s->files_pseudo = 0;
		s->buffers_all = 0;
		s->buffers_normal = 0;
		s->buffers_dwell = 0;
		s->buffers_wbi = 0;
		s->buffers_wbs = 0;
		s->buffers_sxi = 0;
		s->buffers_pseudo = 0;
		s->buffers_automerge = 0;
		s->buffers_manualmerge = 0;
		s->buffers_badquality = 0;
		s->buffers_sent = 0;
	}

}

/* close daily stat file */
if (dailyfile_ptr) {
	fflush(dailyfile_ptr);
	fclose(dailyfile_ptr);					
}

/* release shared memory */


} /* end log_daily__statistics */
