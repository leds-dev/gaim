/*  
@(#)  File Name: GAIM_red_black_tree_support.h  Release 1.1  Date: 00/09/06, 12:32:29
*/

/*******************************************************************************
         Kratos Defense
********************************************************************************

         PROJECT         :       GOES N-Q
         HEADER          :       GAIM_red_black_tree_support.h
         EXE. NAME       :       Playback_Update_Buffer, Update_Buffer_Generator
         PROGRAMMER      :       S. Scoles
	
         VER.        DATE        BY              COMMENT
         V1.0        03/00       S. Scoles       Initial creation

********************************************************************************
         Functional Description:

        This header file contains defines used by red-black trees.
	
*******************************************************************************/

#define MAX_FILENAME_LEN 256

#define GAIM_RBT_RED 0
#define GAIM_RBT_BLACK -1

#define ADD_TO_NEW_NODES 0
#define INIT_NEW_NODES -1

struct red_black_tree_node {
	int color;							/* 0=RED, -1=BLACK */
	int my_index;						/* Numbered from 0 to n-1 */
	struct red_black_tree_node *left;	/* Pointer to Left Child */
	int left_index;						/* my_index of left child */
	struct red_black_tree_node *right;	/* Pointer to Right Child */
	int right_index;					/* my_index of right child */
	struct red_black_tree_node *p;		/* Pointer to Parent */
	int p_index;						/* my_index of parent */
	char key[MAX_FILENAME_LEN];			/* Key -- the searchable string */
};

#ifdef GAIM_MAIN
	int red_black_node_count=0,new_red_black_nodes=0;
	struct red_black_tree_node *ftped_files;
		/* A pointer to the ftp'ed file list, stored as a red-black tree */
#else
extern int red_black_node_count,new_red_black_nodes;
extern struct red_black_tree_node *ftped_files;
#endif

