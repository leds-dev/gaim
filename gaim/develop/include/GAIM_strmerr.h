/*  
@(#)  File Name: GAIM_strmerr.h  Release 1.1  Date: 00/09/06, 12:32:30
*/

/*******************************************************************************

	Original Purpose:

		A copy of strmerr.h that had corrections needed by GAIM.

	Current Purpose: 

		To maintain backward compatibility with the software, now that the
		EPOCH version of this file has been corrected and updated.


*******************************************************************************/

#include "strmerr.h"
