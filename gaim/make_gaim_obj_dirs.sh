###
### MAKE SURE $EPOCH_OBJ is defined correctly
###



cd $EPOCH_OBJ
mkdir gaim
cd gaim
ln -s /export/home0/gtacscm/dev/src/gtacs/gaim/*.sh .

mkdir develop
cd develop
ln -s /export/home0/gtacscm/dev/src/gtacs/gaim/develop/Makefile Makefile

#mkdir dbshrink

NAME="include"
echo Creating links to $NAME
mkdir $NAME
cd $NAME
ln -s /export/home0/gtacscm/dev/src/gtacs/gaim/develop/$NAME/* .
cd ..

NAME="check_gtacs"
echo Creating links to $NAME
mkdir $NAME
cd $NAME
ln -s /export/home0/gtacscm/dev/src/gtacs/gaim/develop/$NAME/* .
cd ..

NAME="if_manager"
echo Creating links to $NAME
mkdir $NAME
cd $NAME
ln -s /export/home0/gtacscm/dev/src/gtacs/gaim/develop/$NAME/* .
cd ..

NAME="playback_ub"
echo Creating links to $NAME
mkdir $NAME
cd $NAME
ln -s /export/home0/gtacscm/dev/src/gtacs/gaim/develop/$NAME/* .
cd ..

NAME="sdata_rcvr"
echo Creating links to $NAME
mkdir $NAME
cd $NAME
ln -s /export/home0/gtacscm/dev/src/gtacs/gaim/develop/$NAME/* .
cd ..

NAME="ub_gen"
echo Creating links to $NAME
mkdir $NAME
cd $NAME
ln -s /export/home0/gtacscm/dev/src/gtacs/gaim/develop/$NAME/* .
ln -s /export/home0/gtacscm/dev/src/toolkit/prototype/db_util.p db_util.p
cd ..

NAME="ub_send"
echo Creating links to $NAME
mkdir $NAME
cd $NAME
ln -s /export/home0/gtacscm/dev/src/gtacs/gaim/develop/$NAME/* .
cd ..

