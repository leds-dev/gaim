###
### To set tcp parameter, enter (as root):
###     ndd -set /dev/tcp <name> <value>
### ex: ndd -set /dev/tcp tcp_xmit_hiwat 146000 
###

echo "\n***** TCP/IP parameters on" `hostname` "\n"
echo "tcp_time_wait_interval = " `ndd /dev/tcp tcp_time_wait_interval`
echo "tcp_max_buf            = " `ndd /dev/tcp tcp_max_buf`
echo "tcp_xmit_lowat         = " `ndd /dev/tcp tcp_xmit_lowat`
echo "tcp_xmit_hiwat         = " `ndd /dev/tcp tcp_xmit_hiwat`
echo "tcp_recv_hiwat         = " `ndd /dev/tcp tcp_recv_hiwat`
echo "tcp_conn_req_max_q     = " `ndd /dev/tcp tcp_conn_req_max_q`
echo ""
