#!/bin/ksh
### This is a ksh script
###
###   install_gaim.sh Bnn
###      where Bnn is the build ID
###
###   example: 
###      install_gaim.sh B12
###   
###   This script must be run from the same location where the tar file
###   GAIM.Bnn.tar.Z is found
###
###   Last updated
###      Sep 01, 2007		Build 12A

if [ $# = 0 ]; then
   echo "\n   *** please specify argument Bnn (Build ID)\n"
   exit
fi

START=`pwd`

###
### Routine to copy system files from remote host
###    arg 1 = Bnn
###

COPY_SYSTEM_FILES ()
{

echo "ENTER remote hostname "
read HOST
echo "\n***copying system files from $HOST\n" 
}


###
### Routine to install new gaim build
###    arg 1 = Bnn
###
INSTALL_GAIM_NEW ()
{
   GAIM=$HOME/gaim.$1
   EPOCH=$HOME/epoch
   EPOCHBIN=$HOME/epoch/bin.$1

   echo "\n*** saving current scripts and cfg files"
   cd $HOME/gaim/bin
   tar cvf /tmp/gaim.bin.tar *.sh *.cfg

   echo "\n*** creating directory $GAIM"
   mkdir $GAIM
   # create new gaim subdirs
   mkdir $GAIM/bin
   mkdir $GAIM/ftp
   mkdir $GAIM/ftp/SAVE
   mkdir $GAIM/output
   mkdir $GAIM/checksumUNIX

   echo "\n*** extracting tar file GAIM.$1.tar"
   cd $START
   uncompress GAIM.$1.tar.Z
   cd $GAIM
   tar xvf $START/GAIM.$1.tar
   
   echo "\n*** creating directory $EPOCHBIN"
   mkdir $EPOCHBIN
   echo "\n*** copying epoch files to $EPOCHBIN"
   cd $GAIM/bin
   cp -p install_db   $EPOCHBIN
   cp -p libXeALT.so  $EPOCHBIN
   cp -p libtcext.so  $EPOCHBIN
   cp -p libsc_ext.so $EPOCHBIN
  
   echo "\n*** Re-creating logical links"
   cd $GAIM/bin
   ln -s $EPOCH/database/reports/gaim.cfg gaim.cfg
   ln -s $EPOCH/database/reports/epoch.cfg epoch.cfg 
   chmod -w *
   chmod +x *.sh
    
   cd $EPOCH
   rm bin
   ln -s bin.$1 bin
   cd $HOME
   rm gaim
   ln -s gaim.$1 gaim

   echo "\n*** To restore scripts and cfg files from previous build"
   echo "ENTER Y to confirm, anything else to skip"
   read Y
   if [ "$Y" = "Y" ]; then
      echo "\n*** restoring scripts and cfg files from previous build"
      cd $HOME/gaim/bin
      tar xvf /tmp/gaim.bin.tar 
   else
      echo "\n*** scripts and cfg files from previous build NOT restored"   
   fi      

   echo "\n*** generate checksums"
   cd $GAIM/checksumUNIX
   rm checksum.gaim.ref
   check_files -l checksum.gaim.input

}


###
### Routine to create epoch dir and subdirs
###    arg 1 = Bnn
###
MAKE_EPOCH_DIRS ()
{
   EPOCH=$HOME/epoch
   
   # create epoch, bin, database directories
   echo "*** creating epoch dir and subdirs"
   mkdir $EPOCH
   mkdir $EPOCH/CONFIG
   mkdir $EPOCH/database
   mkdir $EPOCH/database/reports
   mkdir $EPOCH/output   

   # create epoch output directories
   while [ 1 = 1 ]; do
      echo "\nENTER LAN: (DEV,OPS)"
      read LAN
      if [ "$LAN" = "DEV" ]; then
         echo "*** creating epoch output dirs for $LAN"
         mkdir $EPOCH/output/sgtacs04
         mkdir $EPOCH/output/sgtacs05
		 mkdir $EPOCH/output/cgtacs03
	     break
      elif [ "$LAN" = "OPS" ]; then
         echo "*** creating epoch output dirs for $LAN"
		 mkdir $EPOCH/output/sgtacs01
         mkdir $EPOCH/output/sgtacs02
		 mkdir $EPOCH/output/cgtacs01
		 mkdir $EPOCH/output/cgtacs02
		 mkdir $EPOCH/output/fgtacs01
		 mkdir $EPOCH/output/ggtacs01
         break
      else
         echo "*** invalid LAN $LAN\n"
      fi
   done        
  
}   


###
### main routine
###

OPT1="Create dirs gaim.$1 and epoch/bin.$1 and install"
OPT2="Install gaim executables into current gaim directory"
OPT3="Create new epoch directory and subdirs"
OPT4="Copy system files from another host"
OPT=5;

# forever loop 
while [ "$OPT" != "0" ]; do

   echo "\nENTER Option number or 0 to exit\n"
   echo "   1 = $OPT1"
   echo "   2 = $OPT2"
   echo "   3 = $OPT3"
   echo "   4 = $OPT4"
   echo "   0 = EXIT\n" 
   read OPT

   if [ "$OPT" = "0" ]; then
	  exit
   
   elif [ "$OPT" = "1" ]; then
	  echo "Selection: $OPT1"
      echo "ENTER Y to confirm"
      read Y
      if [ "$Y" = "Y" ]; then
         INSTALL_GAIM_NEW $1
      fi      
	  	
   elif [ "$OPT" = "2" ]; then
	  echo "Selection: $OPT2"
      echo "ENTER Y to confirm"
      read Y
      if [ "$Y" = "Y" ]; then
         echo "INSTALL_GAIM_UPDATE not yet available"
      fi      
	  
   elif [ "$OPT" = "3" ]; then
	  echo "Selection: $OPT3"
      echo "ENTER Y to confirm"
      read Y
      if [ "$Y" = "Y" ]; then
         MAKE_EPOCH_DIRS $1
      fi      
	  
   elif [ "$OPT" = "4" ]; then
	  echo "Selection: $OPT4"
      echo "ENTER Y to confirm"
      read Y
      if [ "$Y" = "Y" ]; then
         COPY_SYSTEM_FILES $1
      fi      
	  
   else
	  echo "*** invalid option selected"

   fi

done;


###
### end of install_gaim.sh
###



