#!/bin/sh
###
###   Invocation: make_gaim_build.sh BuildID
###   ex: make_gaim_build.sh B12A
###
###   Last modified
###      Sep 6, 2007

if [ $# -ne 1 ]; then 
   echo
   echo "Usage: make_gaim_build.sh <release_name>" 
   echo
   exit 1 
fi 

echo
echo making GAIM build $1 ...
echo

# save current directory
START_DIR=`pwd`

echo make temp directories to hold gaim files
mkdir /tmp/$1
mkdir /tmp/$1/checksumUNIX
mkdir /tmp/$1/bin
mkdir /tmp/$1/config
mkdir /tmp/$1/output
mkdir /tmp/$1/ftp

echo get gaim checksum files
cd /tmp/$1/checksumUNIX
cp -p -p $EPOCH_OBJ/gaim/checksumUNIX/* .
chmod +w checksum.gaim.ref
chmod +x check_files

cd /tmp/$1/bin

echo get gaim executable files
cp -p $EPOCH_OBJ/gaim/develop/check_gtacs/Check_GTACS .
cp -p $EPOCH_OBJ/gaim/develop/if_manager/N-Q_Interface_Manager .
cp -p $EPOCH_OBJ/gaim/develop/ub_gen/Update_Buffer_Generator .
cp -p $EPOCH_OBJ/gaim/develop/ub_send/Update_Buffer_Sender .
cp -p $EPOCH_OBJ/gaim/develop/playback_ub/Playback_Update_Buffer .
cp -p $EPOCH_OBJ/gaim/develop/sdata_rcvr/Stream_Data_Receiver .

echo get gaim utilities
cp -p $EPOCH_OBJ/gaim/develop/util/genTAG .
cp -p $EPOCH_OBJ/gaim/develop/util/dumpDEF .
cp -p $EPOCH_OBJ/gaim/develop/util/fixGSA .

echo get gaim shell scripts
cp -p $EPOCH_OBJ/gaim/start_gaim.sh .
cp -p $EPOCH_OBJ/gaim/start_gaim_NO_GLOBALS.sh .
cp -p $EPOCH_OBJ/gaim/check_gaim.sh .
cp -p $EPOCH_OBJ/gaim/stop_gaim.sh .
cp -p $EPOCH_OBJ/gaim/kill_gaim.sh .
cp -p $EPOCH_OBJ/gaim/genTAG.sh .
cp -p $EPOCH_OBJ/gaim/gaim.sh .

echo get libraries
cp -p $EPOCH_BIN/install_db .
cp -p $EPOCH_BIN/libXeALT.so .
cp -p $EPOCH_BIN/libtcext.so .
cp -p $EPOCH_BIN/libsc_ext.so .
#cp -p logmgr.x .
#cp -p gtif_mgr.x .

echo get IPC cleanup script
cp -p $EPOCH_BIN/ipcrm.sh .

cd /tmp/$1/config

echo get configuration files
cp -p $EPOCH_OBJ/gaim/gaim_services .
cp -p $EPOCH_OBJ/database/reports/gaim.cfg .
cp -p $EPOCH_OBJ/database/reports/epoch.cfg .

# (Not needed anymore since using hosts.equiv)
# files needed on gtacs server to enable rsh
#cp -p $EPOCH_CONFIG/rhosts .
#cp -p $EPOCH_CONFIG/netrc .

# files used to init a new epoch user account
cp -p $EPOCH_CONFIG/profile_gtacsops .
cp -p $EPOCH_CONFIG/gtacs.sh .
cp -p $EPOCH_CONFIG/install_userfiles.sh .

echo make tar file /tmp/$1.tar 
cd /tmp/$1
tar cvf ../GAIM.$1.tar *

# move tar file to original user location
mv /tmp/GAIM.$1.tar $START_DIR/GAIM.$1.tar

# remove temp directories
cd /tmp
\rm -rf /tmp/$1

echo
echo tar file $START_DIR/GAIM.$1.tar created...
echo

