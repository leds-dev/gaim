# \%Z\% $database $group $folder\%M\% \%I\% \%E\% \%U\%
###
### invocation:
###     mkdir_output.sh 
###
### This script creates output directories used by EPOCH
###

HOSTNAME=`hostname`
PREFIX=`echo $HOSTNAME | awk '{ printf("%s%s\n", substr($1, 1, 1), substr($1,8,1)) }'`  
GE="_ge"

if [ $HOSTNAME = "gtacsdev" ]
then
        PREFIX="s2"
fi

if [ $HOSTNAME = "goesws2" ]
then
        PREFIX="c2"
fi

if [ -d $EPOCH_OUT ]
then
   echo
   echo "Moving existing output directory to ${EPOCH_OUT}.old"
   mv $EPOCH_OUT $EPOCH_OUT.old
fi

echo 
echo "Creating new ${EPOCH_INSTALL}/output directory structure..."
echo 

mkdir $HOME/gaim
mkdir $EPOCH_DATABASE/chkpt
mkdir $EPOCH_OUT
cd $EPOCH_OUT

mkdir $HOSTNAME
mkdir $HOSTNAME/gaim
mkdir $HOSTNAME/DMF
mkdir $HOSTNAME/EMF
mkdir $HOSTNAME/eui
mkdir $HOSTNAME/dist
mkdir $HOSTNAME/vwr
mkdir $HOSTNAME/50402
mkdir $HOSTNAME/get
mkdir $HOSTNAME/get/rpt
mkdir $HOSTNAME/$PREFIX$GE
mkdir $HOSTNAME/node_mgr
touch $HOSTNAME/node_mgr/messages.log

for SAT in $EPOCH_SATELLITES;\
do ( \
	mkdir $HOSTNAME/DMF/$SAT
	mkdir $HOSTNAME/EMF/$SAT
	mkdir $HOSTNAME/$SAT
	mkdir $HOSTNAME/$SAT/oats
	mkdir $HOSTNAME/$SAT/clk
	mkdir $HOSTNAME/$SAT/gss
	mkdir $HOSTNAME/$SAT/image
	mkdir $HOSTNAME/$SAT/pss
	mkdir $HOSTNAME/$SAT/${PREFIX}rt1
	mkdir $HOSTNAME/$SAT/${PREFIX}rt2
	mkdir $HOSTNAME/$SAT/${PREFIX}rt3
	mkdir $HOSTNAME/$SAT/${PREFIX}rt4
	mkdir $HOSTNAME/$SAT/${PREFIX}rt5
	mkdir $HOSTNAME/$SAT/${PREFIX}rt6
	mkdir $HOSTNAME/$SAT/${PREFIX}_sim
	mkdir $HOSTNAME/$SAT/${PREFIX}_pb
	mkdir $HOSTNAME/$SAT/oats/${PREFIX}rt1
	mkdir $HOSTNAME/$SAT/oats/${PREFIX}rt2
	mkdir $HOSTNAME/$SAT/oats/${PREFIX}rt3
	mkdir $HOSTNAME/$SAT/oats/${PREFIX}rt4
	mkdir $HOSTNAME/$SAT/oats/${PREFIX}rt5
	mkdir $HOSTNAME/$SAT/oats/${PREFIX}rt6
); done;

echo 
echo "Creating ${EPOCH_INSTALL}/coresafe directory..."
echo 

mkdir $EPOCH_INSTALL/coresafe

echo
echo "Done."
echo
