#!/bin/ksh

#  A script to push an SSH key to a remote server.
#
#  usage:  $ ./ssh_push_key.sh <remote_server>
#
#  Or if you have a list of server (extract them from ~/.ssh/known_hosts) 
#  you could # use this list like this:
#
#    $ cat list-of-servers.txt | while read hostname ; do
#    >   ./ssh-push-key.sh "$hostname"
#    > done

# Check if an argument was given.
if [ ! "$1" ] ; then
   echo "Please specify a hostname to distribute the key to."
   exit 1
fi

# Check if all the local files are here.
if [ ! -f ~/.ssh/id_rsa.pub ] ; then
   echo "The local file ~/.ssh/id_rsa.pub is missing. Please create it. (man ssh-keygen)"
   exit 1
fi

# This command sends the key, creates a .ssh dir if required and sets the
# correct permissions.
cat ~/.ssh/id_rsa.pub | ssh -q "$1" "if [ ! -d ~/.ssh/ ] ; \
then mkdir ~/.ssh ; fi ; \
chmod 700 ~/.ssh/ ; cat - >> ~/.ssh/authorized_keys ; \
chmod 600 ~/.ssh/authorized_keys"
