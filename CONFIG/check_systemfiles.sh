#!/bin/bash 
###
### \%Z\% $database $group $folder\%M\% \%I\% \%E\% \%U\%
###
### invocation:
###     check_systemfiles.sh <location> 
###        where
###     location = socc/cda/wbu/sec/fcda
###
### This script compares various unix system files needed by GTACS
### to the versions delivered with the GTACS release
### Must be root to sun this script
###
###
### Modified by: M. Dalal, October 2012.
### Made changes related to Netra and new builds

USAGE="usage: install_systemfiles <socc|wcdas|wbu|sec|fcdas> <ops|dev>"

# Check if the script is being run as root.
if [ `/usr/ucb/whoami` != "root" ]
then
    echo "You must be user 'root' to run this script."
    echo "Try again."
    exit 1
fi

if [ $# != 2 ]
then
  echo "*** $USAGE"
  exit
fi
if [[ $1 != "socc" && $1 != "wcdas" && $1 != "wbu" && $1 != "sec" && $1 != "fcdas" ]]
then
  echo "*** $USAGE"
  exit
fi
if [[ $2 != "ops" && $2 != "dev" ]]
then
  echo "*** $USAGE"
  exit
fi

HOSTNAME=`hostname`
EPOCH_CONFIG=/export/home/gtacsops/epochsw/CONFIG

echo
echo "Checking system files for GTACS-required changes..."
echo

comparefiles() {            # compare two files
   if [ -f $1 ]
   then
      ans=`cmp $1 $2`
      if [ $? != 0 ]
      then
	echo $ans
      else
	echo "ok!"
      fi
   else 
      echo "Required file $1 does not exist..."
   fi
}

/usr/ucb/echo -n "Checking /etc/system..."
grep 'rlim_fd_' /etc/system > /dev/null
if [ $? != 0 ]
then
        echo "Not ok"
else
        echo "ok"
fi

/usr/ucb/echo -n "Checking /etc/inet/services..."
comparefiles /etc/inet/services $EPOCH_CONFIG/etc_inet_services

/usr/ucb/echo -n "Checking /etc/syslog.conf..."
comparefiles /etc/syslog.conf $EPOCH_CONFIG/etc_syslog.conf

/usr/ucb/echo -n "Checking /etc/dfs/dfstab..."
comparefiles /etc/dfs/dfstab $EPOCH_CONFIG/etc_dfs_dfstab_${HOSTNAME}

/usr/ucb/echo -n "Checking /etc/defaultrouter..."
if [ $2 == "dev" ]
then
   comparefiles /etc/defaultrouter $EPOCH_CONFIG/etc_defaultrouter_$1_$2
else
   comparefiles /etc/defaultrouter $EPOCH_CONFIG/etc_defaultrouter_$1
fi
/usr/ucb/echo -n "Checking /etc/inet/ntp.conf..."
comparefiles /etc/inet/ntp.conf  $EPOCH_CONFIG/etc_inet_ntp.conf_$1

/usr/ucb/echo -n "Checking /etc/rc3.d/S99epoch..."
comparefiles /etc/rc3.d/S99epoch $EPOCH_CONFIG/etc_rc3d_S99epoch
/usr/ucb/echo -n "Checking /etc/rc3.d/S99gtacs..."
comparefiles /etc/rc3.d/S99gtacs $EPOCH_CONFIG/etc_rc3d_S99gtacs

# Used In Samba Install 
/usr/ucb/echo -n "Checking /etc/samba/smb.conf..."
if [ $2 == "dev" ]
then
   comparefiles /etc/samba/smb.conf $EPOCH_CONFIG/Samba/samba_etc_sfw_smb.conf_$1_$2
else
   comparefiles /etc/samba/smb.conf $EPOCH_CONFIG/Samba/samba_etc_sfw_smb.conf_$1
fi
/usr/ucb/echo -n "Checking /etc/samba/smbusers.map..."
comparefiles /etc/samba/smbusers.map $EPOCH_CONFIG/Samba/smbusers.map

/usr/ucb/echo -n "Checking /etc/logadm.conf..."
ans=`grep -c '/export/home/gtacsops' /etc/logadm.conf`
if [ $ans != 3 ]
then
	echo "Not ok"
else
	echo "ok"
fi

/usr/ucb/echo -n "Checking application symlinks..."
if [ ! -h /EPOCH_OUTPUT ] || [ ! -h /EPOCH_DATABASE ]
then
	echo "Not ok"
else
	echo "ok"
fi

echo
echo "If differences were detected, log on as root and either "
echo "a)  Run install_systemfiles.sh to replace *ALL* GTACS-related "
echo "    system files with the versions delivered with GTACS, or"
echo "b)  Resolve the differences manually."
echo
echo "Done."
echo

