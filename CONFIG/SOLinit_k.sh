#
# \%Z\% $database $group $folder\%M\% \%I\% \%E\% \%U\%
#
################################################################################
#
#	Purpose: 
#
#		Script to initialize platform-specific EPOCH enviroment for SOLARIS.
#
#	Invocation:
#
#		% . SOLinit_k.sh	(from K Shell)
#
#	Origin:
#
#		Written by D.C.Steyer, Integral Systems, Inc.
#               Modified by M. Dalal, OSO Software, 10/09, Add -xCC to CDEFS
#                           to allow C++-like comments in the code.
#               Modified by M. Dalal, OSO Software, 07/10, Changed the path of 
#                           EPOCH Toolkit dirs.
#               Modified by M. Dalal, OSPO Systems, 08/12, Changes related to
#                           EPOCH 4.12.2.3 server upgrade.
#
################################################################################

if [ `uname -r` == "5.10" ]
then
	export CDEFS="-xc99=none -xCC -Xc -DSYSV -D_SOL_SOURCE -D__EXTENSIONS__ -D_REENTRANT -D_project_${EPOCH_PROJECT}"
else
	export CDEFS="-Xc -DSYSV -D_SOL_SOURCE -D__EXTENSIONS__ -D_project_${EPOCH_PROJECT}"
fi
export CPPDEFS="-D_SOL_SOURCE -D_project_${EPOCH_PROJECT}"
export CFLAGS="-g -DMAX_READERS=100"
export CC=cc
export CXX=g++
export CPP="/usr/ccs/lib/cpp -D_project_${EPOCH_PROJECT}"
export YACCFLAGS=-dvl
export LDFLAGS="-Bdynamic -lrpcsoc -lnsl -lsocket -lgen -lm -lc -ll -lrt"
export LDOPTS="-G"
export ARFLAGS="rv"

export DISPLAY_INCLUDES="-I/usr/dt/include -I/usr/openwin/share/include"
export DISPLAY_LIBS="-Bdynamic -lXm -lXt -lX11"
export MDEPEND="In lieu of KEEP STATE"

# Note that Sol 9+ doesn't work with /usr/ucblib before /usr/lib in path
export LD_LIBRARY_PATH=${EPOCH_BIN}:/opt/SUNWspro/lib:/usr/lib:/usr/openwin/lib:/usr/ucblib:/usr/dt/lib:/opt/sfw/lib:/usr/sfw/lib
export LM_LICENSE_FILE=/etc/opt/licenses/licenses_combined

export EPOCH_PSRC=${EPOCH_DEVELOP}/src/${EPOCH_PROJECT}
export EPOCH_GSRC=${EPOCH_PSRC}/EPOCH_Toolkit/generic
export EPOCH_ASRC=${EPOCH_PSRC}/EPOCH_Toolkit/add_on
export EPOCH_TSRC=${EPOCH_PSRC}/EPOCH_Toolkit/toolkit
export EPOCH_TBIN=${EPOCH_PSRC}/EPOCH_Toolkit/bin
export EPOCH_OBJ=${EPOCH_DEVELOP}/obj
export EPOCH_GOBJ=${EPOCH_DEVELOP}/gobj
export LDDWLEXT="-L${EPOCH_OBJ}/libdwlext -ldwlext"
export LDTCEXT="-L${EPOCH_OBJ}/libtcext -ltcext"
export LDSCEXT="-L$EPOCH_OBJ/libsc -lsc_ext"
export LDGNDMONEXT="-L$EPOCH_OBJ/gndmon -L$EPOCH_BIN -lgndmonext"
if [ -d ${EPOCH_DEVELOP} ]
then
	cd ${EPOCH_DEVELOP}
fi

