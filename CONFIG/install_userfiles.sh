#!/bin/sh 
###
### \%Z\% $database $group $folder\%M\% \%I\% \%E\% \%U\%
###
### invocation:
###     install_userfiles.sh 
###
### This script installs various user files needed by EPOCH
###

echo "Installing user files for Epoch..."
cd $HOME

if [ -d .savedByEpoch ]
then
   touch .savedByEpoch
else
   mkdir .savedByEpoch
fi

if [ -f .profile ]
then
   mv .profile .savedByEpoch
fi
cp $EPOCH_CONFIG/profile_gtacsops .profile

echo "Done.  Log out and log back in for all changes to take effect."

