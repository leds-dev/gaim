#/!/bin/ksh
############################################################
# @(#) goes gtacs CONFIG/mkdir_year.sh 1.1 06/02/08 22:12:31
############################################################
#
# mkdir_year.sh
#
#	Calls mkdir_day.sh once for each
#	day of the year
#
############################################################

day=1
while (( ${day} <= 366  ))
do
	mkdir_day.sh  $day
	day=$(($day+1)) 
done
