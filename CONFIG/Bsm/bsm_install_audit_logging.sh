#!/bin/ksh
#
# bsm_install_audit_logging.sh
#
#  Configure and enable the Solaris Basic Security Module (BSM)
#

PDIR=`pwd`
BSMSRCDIR=${BSMSRCDIR:=$PDIR}
PROG=bsm_install_audit_logging.sh

permission()
{
WHO=`id | cut -f1 -d" "`
if [ ! "$WHO" = "uid=0(root)" ]
then
        form=`gettext "%s: ERROR: you must be super-user to run this script."`
        printf "${form}\n" $PROG
        exit 1
fi
}

permission

if [[ -d ${BSMSRCDIR} ]]
then
	echo "Installing from ${BSMSRCDIR}"
	echo
	read ignore?"Press Enter to continue, CTRL-C to exit..."
else
	echo
	echo "Source Directory ${BSMSRCDIR} does not exist."
	echo "Exiting"
	exit
fi
   
# Install config files

cd /etc/security

echo "Backing up audit confguration files in directory /etc/security/savedByEpoch"

if [ -d savedByEpoch ]
then
   touch savedByEpoch
else
   mkdir savedByEpoch
fi

mv audit_class   savedByEpoch
mv audit_event   savedByEpoch
mv audit_control savedByEpoch

echo "Installing audit logging configuration files..."

cp ${BSMSRCDIR}/bsm_etc_security_audit_class   /etc/security/audit_class
cp ${BSMSRCDIR}/bsm_etc_security_audit_event   /etc/security/audit_event
cp ${BSMSRCDIR}/bsm_etc_security_audit_control /etc/security/audit_control


# Enable BSM

echo "Enabling Basic Security Module..."

./bsmconv

# Install new audit_startup file

#echo "Installing modified audit_startup configuration file..."

#mv audit_startup savedByEpoch
#cp ${BSMSRCDIR}/bsm_etc_security_audit_startup /etc/security/audit_startup


# Add a cron job to the root crontab file to have the audit file
# closed and a new one reopened daily.

echo "Adding cron job to the root crontab file..."

crontab -l root > /tmp/crontab_root
echo "5 3 * * * /usr/sbin/audit -n" >> /tmp/crontab_root
crontab /tmp/crontab_root
rm /tmp/crontab_root


# Adds an entry to the Solaris log rotation tool config file 
# /etc/logadm.conf

echo "Adding entry to logadm.conf-log rotation tool config file..."

logadm -w auditd -A 30d -S 600m -T '/var/audit/*.[0-9]*' -p never

# Manage /var/adm/wtmpx with logadm so it doesn't grow out of control
logadm -w /var/adm/wtmpx -C4 -c -s 25m

# Re-enable volume manager, disabled in bsmconv script
svcadm enable svc:/system/filesystem/volfs:default

echo
echo "Done."
echo
