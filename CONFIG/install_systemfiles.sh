#!/bin/sh 
###
### %Z% $database $group CONFIG/%M% %I% %E% %U%
###
### invocation:
###     install_systemfiles.sh <location> <LAN>
###        where
###     location = socc|wcdas|wbu|sec|fcdas
###     LAN      = ops|dev
###
### This script installs various unix system files needed by EPOCH
### Must be root to run this script
###

USAGE="usage: install_systemfiles <socc|wcdas|wbu|sec|fcdas> <ops|dev>"

if [ $# != 2 ]
then
  echo "*** $USAGE"
  exit
fi
if [[ $1 != "socc" && $1 != "wcdas" && $1 != "wbu" && $1 != "sec" && $1 != "fcdas" ]]
then
  echo "*** $USAGE"
  exit
fi
if [[ $2 != "ops" && $2 != "dev" ]]
then
  echo "*** $USAGE"
  exit
fi

HOSTNAME=`hostname`
EPOCH_INSTALL=$EPOCH_CONFIG/..

echo "Installing system files for Epoch..."
cd /etc

if [ -d savedByEpoch ]
then
   touch savedByEpoch
else
   mkdir savedByEpoch
fi

#mv system savedByEpoch
#cp $EPOCH_CONFIG/etc_system system

# Add required settings to /etc/system file
cp system savedByEpoch
chmod +w /etc/system
echo >> /etc/system
echo "set rlim_fd_cur=1024" >> /etc/system
chmod -w /etc/system


#mv hosts savedByEpoch 
#cp $EPOCH_CONFIG/etc_hosts hosts

mv /etc/inet/services savedByEpoch 
cp $EPOCH_CONFIG/etc_inet_services /etc/inet/services

mv syslog.conf savedByEpoch 
cp $EPOCH_CONFIG/etc_syslog.conf syslog.conf

touch $EPOCH_INSTALL/output/${HOSTNAME}/node_mgr/messages.log
logadm -w $EPOCH_INSTALL/output/${HOSTNAME}/node_mgr/messages.log -p 1d -S 100m -a 'kill -HUP `cat /var/run/syslog.pid`' -t'$dirname/%Y%j%H%M%S.$basename'

if [ -f hosts.equiv ]
then
   mv hosts.equiv savedByEpoch 
fi
cp $EPOCH_CONFIG/etc_hosts.equiv hosts.equiv

mv /etc/dfs/dfstab savedByEpoch
cp $EPOCH_CONFIG/etc_dfs_dfstab_${HOSTNAME} /etc/dfs/dfstab
svcadm enable /network/nfs/server

if [ -f defaultrouter ]
then
   mv defaultrouter savedByEpoch 
fi
if [ $2 == "dev" ]
then
   cp $EPOCH_CONFIG/etc_defaultrouter_$1_$2 defaultrouter
else
   cp $EPOCH_CONFIG/etc_defaultrouter_$1 defaultrouter
fi

if [ -f inet/ntp.conf ]
then
   mv inet/ntp.conf savedByEpoch 
fi
cp $EPOCH_CONFIG/etc_inet_ntp.conf_$1 inet/ntp.conf 

if [ -f inet/ntp.stats ]
then
   mv inet/ntp.stats savedByEpoch 
fi
cp $EPOCH_CONFIG/etc_inet_ntp.stats inet/ntp.stats 

# Samba Stuff
# inetd.conf obsolete in Solaris 10
#if [ -f inet/inetd.conf ]
#then
#    mv inet/inetd.conf savedByEpoch 
#fi
#cp $EPOCH_CONFIG/etc_inet_inetd.conf inet/inetd.conf 

if [ -f sfw/smb.conf ]
then
   mv sfw/smb.conf savedByEpoch 
fi
if [ -f sfw/smbusers.map ]
then
   mv sfw/smbusers.map savedByEpoch 
fi

if [ $2 == "dev" ]
then
	cp $EPOCH_CONFIG/samba_etc_sfw_smb.conf /etc/sfw/smb.conf_$1_$2
	cp $EPOCH_CONFIG/samba_etc_sfw_smbusers.map /etc/sfw/smbusers.map_$2
else
	cp $EPOCH_CONFIG/samba_etc_sfw_smb.conf /etc/sfw/smb.conf_$1
	cp $EPOCH_CONFIG/samba_etc_sfw_smbusers.map /etc/sfw/smbusers.map
fi

#if [ -f rc2.d/S74xntpd]
#then
#   mv rc2.d/S74xntpd savedByEpoch
#fi
#cp $EPOCH_CONFIG/etc_rc2d_S74xntpd rc2.d/S74xntpd
#chmod +x rc2.d/S74xntpd


if [ -f rc3.d/S99epoch ]
then
   mv rc3.d/S99epoch savedByEpoch
fi
cp $EPOCH_CONFIG/etc_rc3d_S99epoch rc3.d/S99epoch
chmod +x rc3.d/S99epoch

if [ -f rc3.d/S99gtacs ]
then
   mv rc3.d/S99gtacs savedByEpoch
fi
cp $EPOCH_CONFIG/etc_rc3d_S99gtacs rc3.d/S99gtacs
chmod +x rc3.d/S99gtacs

ln -s /export/home/gtacsops/epoch/database /EPOCH_DATABASE
ln -s /export/home/gtacsops/epoch/output/${HOSTNAME} /EPOCH_OUTPUT

echo "Done"

