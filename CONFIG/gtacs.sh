#!/bin/sh
#
# \%Z\% $database $group $folder\%M\% \%I\% \%E\% \%U\%
#
################################################################################
#
#	Purpose: 
#
#		Script to set up platform-independent EPOCH enviroment.
#
#	Invocation:
#
#		% . gtacs.sh
#
#	Origin:
#
#		Mark Edwards, Integral Systems, Inc.
#
#	History:
#
#		981211MGE	Created.
# 		July 20, 2009   Modified by D.Robinson   NOAA/SOCC
#                               Created new directory for files that are 
#                               distributed during a software release (epochsw)
#                               Modified the bin, testtools, help, CONFIG, reports
#                               to point to new directory
#
################################################################################

HOSTNAME=`hostname`;export HOSTNAME

NODE_SERVER=3000; export NODE_SERVER
EPOCHNG_CONFIG=`hostname`:5500; export EPOCHNG_CONFIG

#  NEW *********
EPOCH_CONFIG=${EPOCH_SWINSTALL}/CONFIG;export EPOCH_CONFIG
EPOCH_BIN=${EPOCH_SWINSTALL}/bin;export EPOCH_BIN
EPOCH_TESTTOOLS=${EPOCH_SWINSTALL}/testtools;export EPOCH_TESTTOOLS
# **************

EPOCH_DATABASE=${EPOCH_INSTALL}/database;export EPOCH_DATABASE
EPOCH_OUT=${EPOCH_INSTALL}/output;export EPOCH_OUT

PATH=${PATH}:${EPOCH_BIN};export PATH

# ?? what is this??
USER_DATABASE=${EPOCH_INSTALL}/database;export USER_DATABASE
#ARCHIVE_DIR=${EPOCH_OUT}/${HOSTNAME}; export ARCHIVE_DIR
ARCHIVE_DIR=/mnt/GTACS/egtacs01; export ARCHIVE_DIR
EPOCH_OUTPUT=${ARCHIVE_DIR}; export EPOCH_OUTPUT

#user interface removed with EPOCH Server 4.15.2.0
#if [ "${EPOCH_HOST}" = "WKS" ]
#then
#	EPOCH_EDL=${EPOCH_DATABASE}/edl
#	for satellite in ${EPOCH_SATELLITES}
#		do EPOCH_EDL=${EPOCH_EDL}:${EPOCH_DATABASE}/edl/$satellite 
#	done
#	if [ "${EPOCH_DATABASE}" != "${USER_DATABASE}" ]
#	then
#		EPOCH_EDL=${EPOCH_EDL}:${USER_DATABASE}/edl
#		for satellite in ${EPOCH_SATELLITES} 
#			do EPOCH_EDL=${EPOCH_EDL}:${USER_DATABASE}/edl/$satellite
#		done
#	fi
#	EPOCH_EDL=${EPOCH_EDL}:${EPOCH_DATABASE}/edl/ntacts
#	EPOCH_EDL=${EPOCH_EDL}:${EPOCH_DATABASE}/edl/ptp1
#	EPOCH_EDL=${EPOCH_EDL}:${EPOCH_DATABASE}/edl/ptp2
#	EPOCH_EDL=${EPOCH_EDL}:${EPOCH_DATABASE}/edl/PST
#	EPOCH_EDL=${EPOCH_EDL}:${EPOCH_DATABASE}/edl/BSE
#	export EPOCH_EDL
#fi

EPOCH_PROCEDURES=${EPOCH_DATABASE}/procs
export EPOCH_PROCEDURES


#  what about reports????????????????????????????????????????

EPOCH_REPORTS=${EPOCH_DATABASE}/reports
for satellite in ${EPOCH_SATELLITES} 
	do EPOCH_REPORTS=${EPOCH_REPORTS}:${EPOCH_DATABASE}/reports/$satellite
done
if [ "${EPOCH_DATABASE}" != "${USER_DATABASE}" ]
then
	EPOCH_REPORTS=${EPOCH_REPORTS}:${USER_DATABASE}/reports
	for satellite in ${EPOCH_SATELLITES} 
	do EPOCH_REPORTS=${EPOCH_REPORTS}:${USER_DATABASE}/reports/$satellite
	done
fi
export EPOCH_REPORTS

if [ "${EPOCH_DATABASE}" != "${USER_DATABASE}" ] 
then
	for satellite in ${EPOCH_SATELLITES} 
		do EPOCH_DATA=${EPOCH_DATA}:${USER_DATABASE}/data/$satellite 
	done
fi
export EPOCH_DATA

#user interface removed with EPOCH Server 4.15.2.0
#PROJ_ALL_DB_DIRS="database/edl"
PROJ_ALL_DB_DIRS=""
for satellite in ${EPOCH_SATELLITES}
	do PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/edl/$satellite"
done
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/edl/PST"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/edl/ntacts"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/edl/ptp1"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/edl/ptp2"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs"
for satellite in ${EPOCH_SATELLITES}
do 
	PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/$satellite"
	PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/$satellite/procs"
done
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/PST"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/PST/testload"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/PST/testtlm"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/BSE"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/BALL"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/BALL/TLM"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/BALL/UI"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B4"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B5"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B5/testload"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B6"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B6/testload"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B6/TestSTOLdir"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B7"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B8"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B9"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B9/testload"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B10"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B11"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B11/testload"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B12"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B12/testload"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B12A"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/B12A/testload"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/procs/BM"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/help"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/help/goesnq"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/reports"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/PST"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/PST/ptp1"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/PST/ptp2"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/PST/ptp3"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/PST/ptp4"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/PST/ptp5"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/PST/ptp6"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/samples"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/system"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/system/bitmaps"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/system/cortex"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/system/data_src"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/system/encryptors"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/system/mrss"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/system/oats"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/system/oge"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/system/ptp"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/system/rpm"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/system/sps"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/system/streams"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/epv/system/wks"
for satellite in ${EPOCH_SATELLITES} 
do 
	PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/reports/$satellite"
	PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} database/reports/$satellite/LimSets"
done
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} testtools"
PROJ_ALL_DB_DIRS="${PROJ_ALL_DB_DIRS} testtools/att"
export PROJ_ALL_DB_DIRS

LOGFILE_DIR=${EPOCH_HOME}/${EPOCH_PROJECT}/output/${HOSTNAME};export LOGFILE_DIR

#user interface removed with EPOCH Server 4.15.2.0
#if [ "${EPOCH_HOST}" = "WKS" ]
#then
#	VIEWER_LAYOUT=${EPOCH_DATABASE}/edl/viewer_pc.scr;export VIEWER_LAYOUT
#	
#  NEW *****************
#	EPOCH_HELP=${EPOCH_DATABASE}/help; export EPOCH_HELP
#	EPOCH_HELP=${EPOCH_SWINSTALL}/help; export EPOCH_HELP
# ************************
#fi
