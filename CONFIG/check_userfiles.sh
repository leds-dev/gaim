#!/bin/ksh 
###
### \%Z\% $database $group $folder\%M\% \%I\% \%E\% \%U\%
###
### invocation:
###     check_userfiles.sh 
###
### This script compares various user files needed by GTACS
### to the versions supplied with this GTACS release
###

echo
echo "Comparing user files for GTACS-required changes..."
echo

comparefiles() {            # compare two files
   if [ -f $1 ]
   then
      cmp $1 $2
   else 
      echo "Required file $1 does not exist..."
   fi
}

comparefiles $HOME/.profile $EPOCH_CONFIG/profile_gtacsops

echo
echo "If differences were detected, "
echo "a)  Run install_userfiles.sh to replace *ALL* GTACS-related "
echo "    user files with the versions delivered with GTACS, or"
echo "b)  Resolve the differences manually."
echo
echo "Done."
echo
