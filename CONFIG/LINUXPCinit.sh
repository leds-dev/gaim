#!/bin/sh
#
# @(#) epoch source CONFIG/LINUXPCinit.sh 2.42 19/10/21 21:25:05
#
################################################################################
#
#	Purpose: 
#
#		Script to initialize platform-specific EPOCH environment for Linux PC.
#
#	Invocation:
#
#		% source  LINUXPCinit.sh	(from Bourne Shell)
#
#	Origin:
#
#		Original SOLinit.sh written by D.C.Steyer, Integral Systems, Inc.
#		Customized for Linux by Bob Schmertz, Integral Systems, Inc.
#
#	History:
#
#		930423DCS	Created.
#
###############################################################################

if [ -z "${EPOCH_DEV_BASE}" ] ; then
   EPOCH_DEV_BASE="${EPOCH_DEVELOP}/${EPOCH_PROJECT}"
fi

EPOCH_ASRC="${EPOCH_DEV_BASE}/add_on";export EPOCH_ASRC
if [ -d ${EPOCH_DEV_BASE}/c2-enterprise ] ; then
  EPOCH_GSRC="${EPOCH_DEV_BASE}/c2-enterprise";export EPOCH_GSRC
else
  EPOCH_GSRC="${EPOCH_DEV_BASE}/generic";export EPOCH_GSRC
fi
if [ -n "$proj" ] ; then
   EPOCH_PSRC="${EPOCH_DEV_BASE}/${proj}";export EPOCH_PSRC
else
   EPOCH_PSRC="${EPOCH_DEV_BASE}/${EPOCH_PROJECT}";export EPOCH_PSRC
fi
if [ -d ${EPOCH_DEV_BASE}/c2-enterprise ] ; then
  EPOCH_OBJ="${EPOCH_DEV_BASE}/c2-enterprise";export EPOCH_OBJ
  COMMON_OBJ="${EPOCH_DEV_BASE}/c2-common";export COMMON_OBJ
  MEMMGR_OBJ="${EPOCH_DEV_BASE}/memmgr";export MEMMGR_OBJ
else
  EPOCH_OBJ="${EPOCH_DEV_BASE}/obj_${EPOCH_PLATFORM}";export EPOCH_OBJ
  COMMON_OBJ="${EPOCH_OBJ}";export COMMON_OBJ
  MEMMGR_OBJ="${EPOCH_OBJ}/memmgr";export MEMMGR_OBJ
fi
editor="xterm -e vi"
for poss_editor in "nano emacs kedit kwrite gedit nedit"
do
    /usr/bin/which $poss_editor > /dev/null 2>&1
    if [ "$?" = "0" ]; then
		editor=$poss_editor
    fi
done
EPOCH_EDITOR="$editor";export EPOCH_EDITOR

gcc_version=`gcc -dumpversion`
gcc_maj=`echo $gcc_version | cut -d'.' -f1`
gcc_min=`echo $gcc_version | cut -d'.' -f2`
if [ "$gcc_min" = "" ] ; then
  gcc_min=0
fi
gcc_mic=`echo $gcc_version | cut -d'.' -f3`
if [ "$gcc_mic" = "" ] ; then
  gcc_mic=0
fi

CDEFS="-DNPROC_SHMEM=10 -DXM_FILE_SEARCH_SUPPLY -DUSE_FLEX -D_PC_SOURCE -D_FILE_OFFSET_BITS=64 -D_REENTRANT";export CDEFS
CCFLAGS="-D_PC_SOURCE";export CCFLAGS
    # -ansi breaks on some system headers: rpc/rpc.h not compliant?
    # -DSYSV brings out wchar_t conflicts between /usr/X11R6/include/X11/Xlib.h
    # and /usr/lib/gcc-lib/i386-redhat-linux/egcs-2.91.66/include/stddef.h.
    # Don't know what XM_FILE_SEARCH_SUPPLY is (as found in init
    # scripts), but it seems to add functionality rather than take it away, so
    # I've left it here.
    # -- Bob Schmertz
#CDEFS="-D_LIBC -DUSE_BISON -DUSE_FLEX"
CC=gcc;export CC
CXX=g++;export CXX
FPIC="-fPIC";export FPIC
#LDFLAGS="-lrpcsoc -lsocket -lgen -lm -lc -ll";export LDFLAGS
LDFLAGS="-lm -lc -lrt -rdynamic";export LDFLAGS
DISPLAY_LIBS="-lXm -lXt -lX11";export DISPLAY_LIBS
PTHREAD_LIB="-lpthread";export PTHREAD_LIB
CPPDEFS="-DUSE_FLEX -D_PC_SOURCE -D_FILE_OFFSET_BITS=64 -DNPROC_SHMEM=10 -DMAX_READERS=100 -D_PTHREADS -DCPP_COMPILE -D_REENTRANT";export CPPDEFS
CFLAGS="-g -Wimplicit -Wreturn-type -Wstrict-prototypes -Wformat -Wold-style-definition"
if [ $gcc_maj -gt 4 ] ; then
   # gcc 5 or later
   CFLAGS="${CFLAGS} -Waddress -Wmissing-parameter-type "
elif [ $gcc_maj -eq 4 ] && [ $gcc_min -gt 7 ] ; then
   # gcc 4.8+
   CFLAGS="${CFLAGS} -Waddress -Wmissing-parameter-type "
fi
if [ $gcc_maj -gt 6 ] ; then
   # gcc 7 or later
   CFLAGS="${CFLAGS} -Wimplicit-fallthrough"
fi
export CFLAGS
CPPFLAGS="-g -Wall"
if [ $gcc_maj -gt 6 ] ; then
   # gcc 7 or later
   CPPFLAGS="${CPPFLAGS} -Wimplicit-fallthrough -Wempty-body -Wuninitialized"
fi
export CPPFLAGS
CPP="/lib/cpp";export CPP
#CPP="gcc -E";export CPP
LEXFLAGS="-tL";export LEXFLAGS
YACCFLAGS="-dvl";export YACCFLAGS
BISON_VER=3;export BISON_VER
LDOPTS="-shared";export LDOPTS
SKIP_SHARED_LIBS="-Wl,-unresolved-symbols=ignore-in-shared-libs";export SKIP_SHARED_LIBS
LDTCEXT="-L$EPOCH_OBJ/libtcext -ltcext"
LDSCEXT="-L$EPOCH_OBJ/libsc -lsc_ext"
if [ -n "${EPOCH_TOOLKIT}" ] ; then
  LDTCEXT="${LDTCEXT} -L$EPOCH_TOOLKIT/libtcext"
  LDSCEXT="${LDSCEXT} -L$EPOCH_TOOLKIT/libsc"
  PATH="${PATH}:$EPOCH_TOOLKIT/util"
fi
export LDTCEXT
export LDSCEXT
LDXEALT="-L$EPOCH_OBJ/viewer -lXeALT"
LDGNDMONEXT="-L$EPOCH_OBJ/gndmon -lgndmonext"
if [ -n "${EPOCH_BIN}" ] ; then
  LDXEALT="${LDXEALT} -L$EPOCH_BIN"
  LDGNDMONEXT="${LDGNDMONEXT} -L$EPOCH_BIN"
  if [ -n "${LD_LIBRARY_PATH}" ] ; then
    LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${EPOCH_BIN}"
  else
    LD_LIBRARY_PATH="${EPOCH_BIN}"
  fi
fi
export LDXEALT
export LDGNDMONEXT

#Deprecated: /usr/X11R6/include doesn't exist on currently supported Linux platform
#DISPLAY_INCLUDES="-I/usr/X11R6/include";export DISPLAY_INCLUDES
MDEPEND="In lieu of KEEP STATE";export MDEPEND
 
PATH="${EPOCH_GSRC}/util:${PATH}";export PATH
LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/usr/lib";export LD_LIBRARY_PATH
LM_LICENSE_FILE=/etc/opt/licenses/licenses_combined;export LM_LICENSE_FILE

RLM_VERSION=RLM_12.2;export RLM_VERSION

if [ "$BUILDBITS" = "32" ] ; then
	CFLAGS="${CFLAGS} -m32"; export CFLAGS
	CPPFLAGS="${CPPFLAGS} -m32"; export CPPFLAGS
	RLM_PLATFORM=x86_l2;export RLM_PLATFORM
	AM_API_PLATFORM=intel_linux;export AM_API_PLATFORM
else
	RLM_PLATFORM=x64_l1;export RLM_PLATFORM
	AM_API_PLATFORM=intel64_linux;export AM_API_PLATFORM
fi

SOEXT=so;export SOEXT
EXTRA_LIB_DEPS=yes;export EXTRA_LIB_DEPS
OS=Linux;export OS

USER=`/usr/bin/whoami| awk '{ print $1 }'`;export USER

# goes-nop specific changes below this line
EPOCH_OBJ=${EPOCH_DEVELOP}/${EPOCH_PROJECT}; export EPOCH_OBJ
EPOCH_HOST=WKS; export EPOCH_HOST

EPOCH_HOST_MODE=SERVER; export EPOCH_HOST_MODE
EPOCH_INSTALL=${EPOCH_DEVELOP}/deploy; export EPOCH_INSTALL
EPOCH_PSRC=${EPOCH_OBJ}; export EPOCH_PSRC

EPOCH_TOOLKIT=${EPOCH_DEVELOP}/epoch_server/toolkit; export EPOCH_TOOLKIT 
EPOCH_TBIN=${EPOCH_DEVELOP}/epoch_server/bin; export EPOCH_TBIN
RLM_BIN=${EPOCH_DEVELOP}/rlm/bin; export RLM_BIN
UTILITIES_BIN=${EPOCH_DEVELOP}/utilities; export UTILITIES_BIN
if [ -d ${EPOCH_OBJ} ]
then
	cd ${EPOCH_OBJ}
fi

