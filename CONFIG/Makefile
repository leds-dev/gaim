#
# @(#) goes gtacs CONFIG/Makefile 1.59 05/05/11 18:52:24
#
################################################################################
#
#	Purpose: 
#
#		Dependencies and rules for building EPOCH T&C on UNIX.
#
#	Origin:
#
#		Written by W.C.Stratton, Integral Systems, Inc.
#
#	History:
#
#		000621WCS	Created.
#		102909		Modified:  D.Robinson
#				SSGS-1075  New gtacs structure for software releases
#				changed for new epochsw structure and removed
#				database from build. 
#				SSGS-0950  E.Larson changes added new grmon process 
#		060911		Modified:  D.Robinson
#				Added ssgstatus.ksh to make install
#		081312		Modified:  M. Dalal
#				Added new EPOCH 4.12.x libs
#		121515		Modified: D.Robinson
#				added new IMMS executables
#
#		06302016	Modified: D. Robinson
#				Added new file for epoch bin for IMMS (pointthrow_gnd)
#
################################################################################

SHELL = /bin/sh

DIRLIST=`find * \( -type d ! -name 'sccs' -a ! -name 'SCCS' \) -print`

#
#   PROJ_CORE_DIRS defines the Source Directories for
#   the EPOCH core 
#
PROJ_CORE_DIRS = \
	libstrm \
	include \
	prototype \
	rules \
	lib \
	libeui \
	libeval \
	libgem \
	libinsnec \
	libmgr \
	libtcext \
	libsc \
	libxpm \
	util \
	bin \
	node_manager \
	stream_manager \
	stream_rcvr \
	stream_server \
	archive \
	archive_manager \
	clcw_sim \
	cltu \
	cxctrl \
	cxmon \
	cxrng \
	cxtc \
	cxtlm \
	cxtms \
	cmd \
	cmdsim \
	cop1 \
	csplit \
	gmt \
	gndmon \
	procedure \
	tlmgen \
	tlmmon \
	eui_manager \
	viewer
#
#    PROJ_CORE_BINS defines the core Epoch binary executable modules
#    for Workstation Hosts.
#
PROJ_CORE_BINS = \
        archive/archive \
	archive/convert \
	archive_manager/archive_manager.csh \
	archive_manager/archive_manager.env \
	archive_manager/convert.csh \
	archive_manager/convert.pl \
	archive_manager/diskcleaner.csh \
	archive_manager/diskcleaner.pl \
	archive_manager/remove_archives.csh \
	archive_manager/remove_archives.pl \
        bin/editfile.sh \
	bin/reorder_derived.pl \
        bin/scrnsnap.sh \
        bin/screen.sh \
        bin/snap.sh \
        bin/txt2stol.pl \
	clcw_sim/clcw_sim \
	cltu/cltu \
        cmd/cmd \
	cmd/cmdmon \
	cmdsim/cmdsim \
	cop1/cop1 \
	csplit/csplit \
	cxctrl/cxctrl \
	cxmon/cxmon \
	cxrng/cxrng \
	cxtc/cxtc \
	cxtlm/cxtlm \
	cxtms/cxtms \
        eui_manager/eui_manager \
        gmt/gmt \
        gndmon/gndmon \
	gndmon/libgndmonext.so \
        lib/libarchmgr_$(EPOCH_PLATFORM).a \
        lib/libmysqlclient_$(EPOCH_PLATFORM).a \
	lib/isi_licd  \
	lib/license  \
	lib/license_info  \
	lib/testfile  \
	lib/testpoint  \
	libeui/testaccess  \
        libsc/install_db \
        libsc/libsc_ext.so \
        libsc/remove_db \
	libstrm/evtrclnt \
	libstrm/evtsclnt \
	libstrm/frame_client \
	libstrm/sched_client \
	libstrm/stol_client \
	libstrm/strmclnt \
	libtcext/libtcext.so \
        node_manager/node_mgr \
	procedure/libprojparse.so \
        procedure/proc \
        procedure/sch \
        procedure/stol_check \
        stream_manager/stream_mgr \
        stream_rcvr/strm_rcvr \
        stream_server/strm_srvr \
        tlmgen/dump \
        tlmgen/dump_raw \
        tlmgen/tlmgen \
        tlmmon/tlmmon \
        util/ipcrm.sh \
        util/stop_epoch.sh \
        util/start_epoch.sh \
	util/stol_line.sh \
	util/unlock_db \
        viewer/libXeALT.so \
        viewer/viewer
#
#	PROJ_TOOLKIT_DIRS and PROJ_TOOLKIT defines the directories and files 
#	needed for an application programmer to create a new stream process
#
PROJ_TOOLKIT_DIRS = \
	ACE_wrappers \
	RetrieverServer \
	corbaAdmin \
	corbaIDL \
	corbaInclude \
	corbaLib \
	corbaSecurity \
	formatter \
	gndmon \
	include \
	java-api \
	lib \
	libepv4 \
	libeui \
	libeval \
	libgem \
	libinsnec \
	libmgr \
	libsc \
	libstrm \
	libtcext \
	procedure \
	prototype \
	rules \
	simpleClients \
	util \
	viewer

PROJ_TOOLKIT = \
	formatter/Make.formatter \
	formatter/Make.project \
	formatter/Make.yacc \
	formatter/Makefile \
	formatter/build_lower_layer_msgs.c \
	formatter/client.c \
	formatter/client.h \
	formatter/cmd_id_util.c \
	formatter/cmd_id_util.h \
	formatter/cmd_routing.c \
	formatter/cmd_routing.h \
	formatter/dir_yacc.y \
	formatter/formatter.c \
	formatter/formatter.h \
	formatter/formatter_ext.h \
	formatter/initialize_connections.c \
	formatter/initialize_lower_layer.c \
	formatter/process_ack_message.c \
	formatter/process_encrypt_error_message.c \
	formatter/process_encrypted_message.c \
	formatter/process_done_message.c \
	formatter/process_error_message.c \
	formatter/process_error_msg.c \
	formatter/process_other_connections.c \
	formatter/process_other_input.c \
	formatter/process_rexmit_message.c \
	formatter/process_status_msg.c \
	formatter/process_timed_events.c \
	formatter/process_xmit_message.c \
	formatter/process_xmit_msg.c \
	formatter/server.c \
	formatter/server.h \
	formatter/setpt.c \
	formatter/timecb_util.c \
	gndmon/Make.project \
	gndmon/dwl_ext.c \
	gndmon/dwl_ext.h \
	include/Command.h \
	include/CommandA.h \
	include/DBPoint.h \
	include/DBPointClass.h \
	include/DBPoints.h \
	include/ByteArray.h \
	include/ByteArrayA.h \
	include/ByteArrayC.h \
	include/ByteArrayClass.h \
	include/HexCommand.h \
	include/HexCommandA.h \
	include/HexCommandC.h \
	include/HexCommandClass.h \
	include/Int.h \
	include/IntA.h \
	include/IntC.h \
	include/IntClass.h \
	include/Make.epoch \
	include/Make.Platform \
	include/Point.h \
	include/PointA.h \
	include/PointC.h \
	include/PointClass.h \
	include/PointData.h \
	include/PointNode.h \
	include/PointNodeA.h \
	include/PointNodeC.h \
	include/PointNodeClass.h \
	include/access.h \
	include/arch_util.h \
	include/ccsds.h \
	include/ccsds_ext.h \
	include/config.h \
	include/cmd_util.h \
	include/cmd_utoke.h \
	include/cmd_y.h \
	include/common.h \
	include/common_utoke.h \
	include/db.h \
	include/db_par_ext.h \
	include/db_params.h \
	include/db_util.h \
	include/dir_y.h \
	include/dw_util.h \
	include/epoch.h \
	include/epoch_par.h \
	include/eu_strucs.h \
	include/eu_util.h \
	include/eval.h \
	include/eval_funcs.h \
	include/evt.h \
	include/evt_util.h \
	include/fork.h \
	include/fparse.h \
	include/funcs.h \
	include/g_exp_y.h \
	include/g_lex.h \
	include/g_macro_y.h \
	include/g_misc_y.h \
	include/g_pctrl.h \
	include/g_token.h \
	include/g_yacc.h \
	include/g_ytoken.h \
	include/gem_vars.h \
	include/gmtv_util.h \
	include/gv.h \
	include/lib_misc.h \
	include/list.h \
	include/log_util.h \
	include/manager.h \
	include/manifest.h \
	include/max.h \
	include/mem.h \
	include/path_util.h \
	include/proc_shmem.h \
	include/ring_util.h \
	include/sdb_util.h \
	include/ss_util.h \
	include/timegen.h \
	include/tlmsim.h \
	include/tlm.h \
	include/tlm_ext.h \
	include/version.h \
	include/xdisplay.h \
	lib/libglex.a \
	lib/libutil.a \
	libeui/libeui.a \
	libeval/libeval.a \
	libgem/libgem.a \
	libinsnec/libinsnec.a \
	libmgr/libmgr.a \
        libsc/Make.project \
	libsc/db_toke.c \
	libsc/db_toke.h \
	libsc/libsc.a \
	libsc/libsc_ext.imp \
	libsc/libsdk.a \
	libsc/tlm_ut_ext.c \
	libstrm/Makefile.$(EPOCH_PLATFORM) \
	libstrm/client_msg.p \
	libstrm/cmd_msg.h \
	libstrm/evtrclnt.c \
	libstrm/evtsclnt.c \
	libstrm/frame_client.c \
	libstrm/id_util.p \
	libstrm/libstrm.a \
	libstrm/net_util.h \
	libstrm/msg.h \
	libstrm/pt_clnt.c \
	libstrm/pt_pseudo1.c \
	libstrm/pt_pseudo2.c \
	libstrm/remote_srvr.c \
	libstrm/sc.h \
	libstrm/sched_client.c \
	libstrm/scs.h \
	libstrm/server_msg.p \
	libstrm/ss.h \
	libstrm/ssm.h \
	libstrm/stol_client.c \
	libstrm/strmclnt.c \
	libstrm/strmerr.h \
	libstrm/strmsrv.h \
	libstrm/strmutil.h \
	libstrm/strmutil.p \
	libstrm/xnet_util.h \
	libtcext/Makefile \
	libtcext/cmd_ext.c \
	libtcext/data_type_ext.h \
	libtcext/eu_lib_ext.c \
	libtcext/evt_ext.c \
	libtcext/evt_ext.h \
	libxpm/libXpm.a \
	procedure/projparse.h \
	prototype/Point.p \
	prototype/cmd_util.p \
	prototype/config.p \
	prototype/convert.p \
	prototype/db_util.p \
	prototype/dev_shmem.p \
	prototype/dw_util.p \
	prototype/env_util.p \
	prototype/evt_util.p \
	prototype/g_lex.p \
	prototype/g_yacc.p \
	prototype/ge_shmem.p \
	prototype/get_phys.p \
	prototype/get_util.p \
	prototype/gv_util.p \
	prototype/libalex.p \
	prototype/list.p \
	prototype/manager.p \
	prototype/path_util.p \
	prototype/read_util.p \
	prototype/ring_util.p \
	prototype/sdb_util.p \
	prototype/shmem_util.p \
	prototype/sring_util.p \
	prototype/strings.p \
	prototype/strlst_util.p \
	prototype/timeval_util.p \
	prototype/tlm.p \
	prototype/tlmsim_util.p \
	prototype/tok_util.p \
	prototype/unconvert.p \
	prototype/utcmet.p \
	rules/g_env_exp.y \
	rules/g_exp.y \
	rules/g_misc.y \
	rules/g_number.y \
	rules/g_pctrl.y \
	rules/g_setenv.y \
	rules/g_ypreced.y \
	util/expand_yacc \
	util/gyacc.sh \
	util/lndir.sh \
	util/mdepend.sh \
	util/menum2.sh \
	viewer/Make.project \
	viewer/XeALT.c \
	viewer/XeALT.h

#
#   PROJ_DB_DIRS defines the Source Directories for
#   the database for new epochsw structure
#
PROJ_DB_DIRS = \
	reports/ \
	reports/goes13 \
	reports/goes14 \
	reports/goes15 \
	reports/goes16 \
	reports/goes17 \
	reports/goes18 

#
#   PROJ_WKS_DIRS defines the Source Directories for
#   the project extensions
#
PROJ_WKS_DIRS = \
	include \
	rules \
	libtcext \
	libavtec \
        libcem \
        lib504 \
        libgtacs \
	libsc \
	arch_mgr \
        avcmd \
        avctrl_mon \
        avtlm \
        bufmon \
        cem \
        cemmon \
        cem_sim \
	clk \
        cload \
        cmdsim \
        csplit \
	cxctrl \
	cxmon \
	cxrng \
	cxtc \
	cxtlm \
        dcp \
	dcr_gen \
	dertlm \
        encrypt \
        gblmon \
	gen_ftok \
        genload \
	getsvr \
        ggsp \
        glmon \
	gndmon \
	grmon \
        memutil \
        mrssif \
        mux \
        oatsif \
        pmif \
        pste \
        rgsp \
        rsmon \
        spsif \
	testtools \
	datt \
        wks
#
#    PROJ_WKS_BINS defines the product extension Epoch binary 
#    executable modules for Workstation Hosts.
#
PROJ_WKS_BINS = \
	libtcext/libtcext.so \
	libsc/libsc_ext.so \
        arch_mgr/arch_mgr \
	avcmd/avcmd \
        avtlm/avtlm \
        avctrl_mon/avctrl_mon \
        bufmon/bufmon \
        cem/cem \
        cemmon/cemmon \
        cem_sim/cem_sim \
	clk/clk \
        cload/cload \
        cmdsim/cmdsim \
        csplit/csplit \
	cxctrl/cxctrl \
	cxmon/cxmon \
	cxrng/cxrng \
	cxtc/cxtc \
	cxtlm/cxtlm \
        dcp/dcp \
	dcr_gen/dcr_gen \
	dertlm/DerTlm \
        encrypt/encrypt \
        gblmon/gblmon \
	gen_ftok/gen_ftok \
        genload/genload \
	getsvr/GETSvr \
        ggsp/ggsp \
        glmon/glmon \
	gndmon/libgndmonext.so \
	grmon/grmon \
        memutil/MemUtil \
        mrssif/mrss_if \
        mux/mux \
        oatsif/oats_if \
        pmif/pm_if \
        pste/pste \
        rgsp/rgsp \
        rsmon/ResMon \
        spsif/sps_if \
	util/convert2abe.sh \
	util/editfile.sh \
	util/sftp.sh \
	util/kill_gtacs.ksh \
	util/check_sys.ksh \
	util/control_GTACS.ksh \
	util/scrubber.ksh \
	util/ssgstatus.ksh \
	util/install_gtacssw.sh \
	util/install_acc.sh \
	util/distribute_gtacssw.sh \
	datt/get_last_hour.ksh \
	datt/oso_convert_ARC_files.ksh \
	datt/oso_convert_ARC_to_DM4.ksh \
	datt/oso_cron_000_00_00_15.ksh \
	datt/oso_crontab.ksh \
	datt/oso_stat \
	datt/deltatime \
	wks/wks

#    PROJ_WKS_TLK_BINS defines the Epoch binary executable modules
#    from the toolkit for Workstation Hosts.
#
PROJ_WKS_TLK_BINS = \
        archive \
        cmd \
        cmdmon \
        convert \
	cxtms \
        dump \
        dump_raw \
        eui_manager \
	frame_client \
        gmt \
	gndmon \
        install_db \
	isi_licd \
	libACE.so.5.7.0 \
	libCorbaAdmin.so.1.0 \
	libCorbaClients.so.1.0 \
	libCorbaSecurity.so.1.0 \
	libCorbaServers.so.1.0 \
	libRetrieverServer.so.1.0 \
	libTAO.so.1.7.0 \
	libTAO_AnyTypeCode.so.1.7.0 \
	libTAO_BiDirGIOP.so.1.7.0 \
	libTAO_CodecFactory.so.1.7.0 \
	libTAO_Codeset.so.1.7.0 \
	libTAO_CosNaming.so.1.7.0 \
	libTAO_IORInterceptor.so.1.7.0 \
	libTAO_IORTable.so.1.7.0 \
	libTAO_Messaging.so.1.7.0 \
	libTAO_ObjRefTemplate.so.1.7.0 \
	libTAO_PI.so.1.7.0 \
	libTAO_PI_Server.so.1.7.0 \
	libTAO_PortableServer.so.1.7.0 \
	libTAO_Security.so.1.7.0 \
	libTAO_Strategies.so.1.7.0 \
	libTAO_Svc_Utils.so.1.7.0 \
	libTAO_Valuetype.so.1.7.0 \
        libXeALT.so \
	libepv4.so.1.0 \
	libv2util.so.1.0 \
        license \
	license_info \
        node_mgr \
        proc \
        remove_db \
        sch \
        stream_mgr \
        strm_rcvr \
        strm_srvr \
        tlmgen \
        tlmmon \
        unlock_db \
        viewer \
	p2dPacking \
	v2p2d \
	pointthrow \
	pointthrow_gnd \
	p2e \
	ipcrm.sh \
	start_epoch.sh \
	stop_epoch.sh \
	scrnsnap.sh \
	snap.sh \
	txt2stol.pl

#    PROJ_WKS_TLK_LIBS defines the Epoch binary libs modules
#    from the toolkit for Workstation Hosts.
#
PROJ_WKS_TLK_LIBS = \
	libACE.so.5.7.0 \
	libCorbaAdmin.so.1.0 \
	libCorbaClients.so.1.0 \
	libCorbaSecurity.so.1.0 \
	libCorbaServers.so.1.0 \
	libRetrieverServer.so.1.0 \
	libTAO.so.1.7.0 \
	libTAO_AnyTypeCode.so.1.7.0 \
	libTAO_BiDirGIOP.so.1.7.0 \
	libTAO_CodecFactory.so.1.7.0 \
	libTAO_Codeset.so.1.7.0 \
	libTAO_CosNaming.so.1.7.0 \
	libTAO_IORInterceptor.so.1.7.0 \
	libTAO_IORTable.so.1.7.0 \
	libTAO_Messaging.so.1.7.0 \
	libTAO_ObjRefTemplate.so.1.7.0 \
	libTAO_PI.so.1.7.0 \
	libTAO_PI_Server.so.1.7.0 \
	libTAO_PortableServer.so.1.7.0 \
	libTAO_Security.so.1.7.0 \
	libTAO_Strategies.so.1.7.0 \
	libTAO_Svc_Utils.so.1.7.0 \
	libTAO_Valuetype.so.1.7.0 \
	libepv4.so.1.0 \
	libv2util.so.1.0

all:
	@echo ""
	@echo "Enter one of the following:"
	@echo ""
	@echo "make world            (creates $(EPOCH_HOST) trees and builds all components)"
	@echo "make core             (creates EPOCH core executables)"
	@echo "make toolkit          (creates and populates toolkit directories)"
	@echo "make build_dir        (creates $(EPOCH_HOST) build tree)"
	@echo "make object           (builds all $(EPOCH_HOST) components)"
	@echo "make install          (installs all $(EPOCH_HOST) executables)"
	@echo "make uninstall        (uninstalls all $(EPOCH_HOST) executables)"
	@echo "make clean            (deletes build tree and project and generic source trees)"
	@echo ""


world:
	@echo ""
	@date
	@echo ""
	@make core
	@make toolkit
	@make object
	@make install
	@echo ""
	@date
	@echo ""

core:
	@echo ""
	@echo "\n**** Making the Core Object Modules and Executables ****\n"
	@echo ""
	@make_in_dir () { \
	for i do \
		echo "\n***** Making in $(EPOCH_GSRC)/$$i *****\n" ; \
		cd $(EPOCH_GSRC)/$$i; \
		if [ -f Makefile ]; \
		then \
			make ; \
		fi \
	done } ; \
	make_in_dir $(PROJ_CORE_DIRS); \

toolkit:
	@if [ ! -d $(EPOCH_TSRC) ]; then \
		mkdir $(EPOCH_TSRC); \
	fi; \
	cd $(EPOCH_TSRC);\
	for i in $(PROJ_TOOLKIT_DIRS); \
	do ( \
		if [ ! -d $(EPOCH_TSRC)/$$i ]; then \
			mkdir $(EPOCH_TSRC)/$$i; \
		else \
			\rm -f $(EPOCH_TSRC)/$$i/*; \
		fi; \
	); done; \
	for i in $(PROJ_TOOLKIT); \
	do ( \
		if [ $(EPOCH_HOST_MODE) = SERVER ]; then \
			cp $(EPOCH_GSRC)/$$i $$i ; \
		else \
			ln -s $(EPOCH_GSRC)/$$i $$i ; \
		fi; \
	); done;

	@if [ ! -d $(EPOCH_TBIN) ]; then \
		mkdir $(EPOCH_TBIN); \
		chmod g+w $(EPOCH_TBIN); \
	else \
		\rm -f $(EPOCH_TBIN)/* $(EPOCH_TBIN)/.[a-z]*; \
	fi; \
	cd $(EPOCH_TBIN);\
	copy_bin_dir () { \
	for i do \
		if [ $(EPOCH_HOST_MODE) = DEVELOP ]; \
		then \
			ln -s $(EPOCH_GSRC)/$$i . ;\
		else \
			cp $(EPOCH_GSRC)/$$i . ; \
		fi;\
	done } ; \
	copy_bin_dir $(PROJ_CORE_BINS)

build_dir:
	if [ ! -d $(EPOCH_OBJ) ]; \
	then \
		mkdir $(EPOCH_OBJ); \
	else \
		echo ""; \
		echo "***** Removing links from $(EPOCH_OBJ) *****"; \
		echo ""; \
		find $(EPOCH_OBJ) -type l -exec rm {} \; ;\
	fi
	@echo ""
	@echo "***** Linking $(EPOCH_PSRC) to $(EPOCH_OBJ) *****"
	@echo ""
	@-cd $(EPOCH_OBJ); lndir.sh $(EPOCH_PSRC) .
	@echo ""
	@echo "***** Linking $(EPOCH_TSRC) to $(EPOCH_OBJ) *****"
	@echo ""
	@-cd $(EPOCH_OBJ); lndir.sh $(EPOCH_TSRC) .
	@echo ""

object:
	@make build_dir
	@echo ""
	@echo "\n**** Making the Object Modules and Executables ****\n"
	@echo ""
	@make_in_dir () { \
	for i do \
		echo "\n***** Making in $(EPOCH_OBJ)/$$i *****\n" ; \
		cd $(EPOCH_OBJ)/$$i; \
		if [ -f Makefile ]; \
		then \
			make ; \
		fi \
	done } ; \
	if [ $(EPOCH_HOST) = FEP ]; \
	then \
		make_in_dir $(PROJ_FE_DIRS); \
	elif [ $(EPOCH_HOST) = WKS ]; \
	then \
		make_in_dir $(PROJ_WKS_DIRS); \
	else \
		cd $(EPOCH_OBJ) ; \
		make_in_dir $(DIRLIST); \
	fi
install:
	@echo ""
	@echo "Populating $(EPOCH_INSTALL) installation tree"
	@echo ""
	@if [ ! -d $(EPOCH_INSTALL) ]; then \
		mkdir $(EPOCH_INSTALL); \
	fi
	@if [ $(EPOCH_HOST) = WKS ]; \
	then \
		if [ $(EPOCH_HOST_MODE) = SERVER -o $(EPOCH_HOST_MODE) = DEVELOP ]; \
		then \
			make wks_server_install; \
		else \
			make wks_client_install; \
		fi; \
	else \
		if [ $(EPOCH_HOST_MODE) = SERVER -o $(EPOCH_HOST_MODE) = DEVELOP ]; \
		then \
			make fep_server_install; \
		else \
			make fep_client_install; \
		fi; \
	fi
	@if [ -f Make.inst_clean ]; \
	then \
		sh Make.inst_clean ; \
	fi

wks_server_src_install:
	@if [ ! -d $(EPOCH_SRC) ]; then\
		mkdir $(EPOCH_SRC);\
		mkdir $(EPOCH_SRC)/libstrm;\
	else \
		\rm -f $(EPOCH_SRC)/libstrm/*;\
	fi;\
	cd $(EPOCH_SRC)/libstrm;\
	if [ $(EPOCH_HOST_MODE) = SERVER ]; then \
		cp $(EPOCH_TSRC)/libstrm/* . ; \
	else \
		ln -s $(EPOCH_TSRC)/libstrm/* . ; \
	fi
wks_server_bin_install:
	@if [ ! -d $(EPOCH_INSTALL)/bin ]; then \
		mkdir $(EPOCH_INSTALL)/bin; \
		chmod g+w $(EPOCH_INSTALL)/bin; \
	else \
		\rm -f $(EPOCH_INSTALL)/bin/* $(EPOCH_INSTALL)/bin/.[a-z]*; \
	fi; \
	cd $(EPOCH_INSTALL)/bin;\
	copy_bin_dir () { \
	for i do \
		if [ $(EPOCH_HOST_MODE) = DEVELOP ]; \
		then \
			ln -s $(EPOCH_OBJ)/$$i . ;\
		else \
			cp $(EPOCH_OBJ)/$$i . ; \
		fi;\
	done } ; \
	copy_bin_dir $(PROJ_WKS_BINS)
	cd $(EPOCH_INSTALL)/bin;\
	copy_tlk_dir () { \
	for i do \
		if [ $(EPOCH_HOST_MODE) = DEVELOP ]; \
		then \
			ln -s $(EPOCH_TBIN)/$$i . ;\
		else \
			cp $(EPOCH_TBIN)/$$i . ; \
		fi;\
	done } ; \
	copy_tlk_dir $(PROJ_WKS_TLK_BINS)

wks_server_system_install:
	@if [ -d $(EPOCH_INSTALL)/CONFIG ]; then \
		\rm -rf $(EPOCH_INSTALL)/CONFIG/* $(EPOCH_INSTALL)/CONFIG/.[a-zA-Z]*;\
	else \
		mkdir $(EPOCH_INSTALL)/CONFIG; \
	fi;\
	cd $(EPOCH_INSTALL)/CONFIG ; \
	cp -r $(EPOCH_PSRC)/CONFIG/* . ; \

	@if [ -d $(EPOCH_INSTALL)/testtools ]; then \
		\rm -rf $(EPOCH_INSTALL)/testtools/* $(EPOCH_INSTALL)/testtools/.[a-zA-Z]*;\
	else \
		mkdir $(EPOCH_INSTALL)/testtools; \
	fi;\
	cd $(EPOCH_INSTALL)/testtools ; \
	cp -r $(EPOCH_PSRC)/testtools/* . ; \

wks_server_database_install:
	@if [ ! -d $(EPOCH_DATABASE) ]; then mkdir $(EPOCH_DATABASE); fi; \
	cd $(EPOCH_DATABASE);\
	copy_db_dir () { \
	for i do \
		if [ ! -d $(EPOCH_INSTALL)/$$i ]; then \
			mkdir $(EPOCH_INSTALL)/$$i ; \
		fi; \
		cd $(EPOCH_INSTALL)/$$i ; \
		if [ $(EPOCH_HOST_MODE) = SERVER ]; then \
			cd $(EPOCH_INSTALL)/$$i;\
			for j in `ls $(EPOCH_OBJ)/$$i`;\
			do (\
				if [ ! -d $(EPOCH_OBJ)/$$i/$$j ]; then \
					cp -fp $(EPOCH_OBJ)/$$i/$$j . ;\
				fi;\
			); done ;\
		else \
			ln -fs $(EPOCH_OBJ)/$$i/* . ; \
		fi ; \
		rm -f Makefile ; \
	done } ; \
	copy_db_dir $(PROJ_DB_DIRS)
#	copy_db_dir $(PROJ_ALL_DB_DIRS)

wks_server_reports_install:
	@if [ ! -d $(EPOCH_INSTALL)/reports ]; then mkdir $(EPOCH_INSTALL)/reports; fi; \
	cd $(EPOCH_INSTALL)/reports;\
	copy_db_dir () { \
	for i do \
		if [ ! -d $(EPOCH_INSTALL)/$$i ]; then \
			mkdir $(EPOCH_INSTALL)/$$i ; \
		fi; \
		cd $(EPOCH_INSTALL)/$$i ; \
		if [ $(EPOCH_HOST_MODE) = SERVER ]; then \
			cd $(EPOCH_INSTALL)/$$i;\
			for j in `ls $(EPOCH_OBJ)/database/$$i`;\
			do (\
				if [ ! -d $(EPOCH_OBJ)/database/$$i/$$j ]; then \
					cp -fp $(EPOCH_OBJ)/database/$$i/$$j . ;\
				fi;\
			); done ;\
		else \
			ln -fs $(EPOCH_OBJ)/database/$$i/* . ; \
		fi ; \
		rm -f Makefile ; \
	done } ; \
	copy_db_dir $(PROJ_DB_DIRS)
#	copy_db_dir $(PROJ_ALL_DB_DIRS)


wks_server_output_install:
	@umask 7;\
	if [ ! -d $(EPOCH_OUT) ]; then mkdir $(EPOCH_OUT); chmod g+w $(EPOCH_OUT); fi; \
	make_output_dir () { \
	for i do \
		if [ ! -d $(EPOCH_OUT)/$$i ]; then mkdir $(EPOCH_OUT)/$$i; fi; \
	done } ; \

	make_output_dir $(EPOCH_FES); \
	make_channel_dir() { \
	for k do \
		if [ ! -d $$k ]; then mkdir $$k ; fi; \
	done }; \
	make_schedule_dir() { \
	mkdir schedules ; \
	for m do \
		if [ ! -d schedules/$$m ]; then mkdir schedules/$$m ; fi; \
	done }; \
	make_sat_dir() { \
	for j do \
		mkdir $(EPOCH_OUT)/$(HOSTNAME)/$$j ; \
		cd $(EPOCH_OUT)/$(HOSTNAME)/$$j ; \
		make_channel_dir $(TLM_CHANNELS) ; \
		make_schedule_dir database fls frame fsp star cls cmd cp sto ram imc ; \
	done }; \
	if [ ! -d $(EPOCH_OUT)/$(HOSTNAME) ]; then \
		mkdir $(EPOCH_OUT)/$(HOSTNAME) ; \
		mkdir $(EPOCH_OUT)/$(HOSTNAME)/vwr ; \
		mkdir $(EPOCH_OUT)/$(HOSTNAME)/eui ; \
		make_sat_dir $(EPOCH_SATELLITES); \
	fi

wks_server_cots_install:
	@if [ -d $(EPOCH_INSTALL)/COTS ]; then \
		\rm -rf $(EPOCH_INSTALL)/COTS/* $(EPOCH_INSTALL)/COTS/.[a-zA-Z]*;\
	else \
		mkdir $(EPOCH_INSTALL)/COTS; \
	fi;\

	@if [ -d $(EPOCH_PSRC)/COTS ]; then \
		find $(EPOCH_PSRC)/COTS \( -type l -o -type f \) -exec cp {} $(EPOCH_INSTALL)/COTS \; ;\
	fi;\

	@if [ -d $(EPOCH_PSRC)/NT-software/ptp ]; then \
		cp -r $(EPOCH_PSRC)/NT-software/ptp $(EPOCH_INSTALL)/COTS; \
	fi;\

wks_server_checksum_install:
	@if [ -d $(EPOCH_INSTALL)/checksumUNIX ]; then \
		\rm -f $(EPOCH_INSTALL)/checksumUNIX/* $(EPOCH_INSTALL)/checksumUNIX/.[a-zA-Z]*;\
	else \
		mkdir $(EPOCH_INSTALL)/checksumUNIX; \
	fi;\
	find $(EPOCH_PSRC)/checksumUNIX \( -type l -o -type f \) -exec cp {} $(EPOCH_INSTALL)/checksumUNIX \; ;\

wks_server_install:
	make wks_server_src_install
	make wks_server_bin_install
	make wks_server_system_install
#	make wks_server_database_install
	make wks_server_reports_install
#	make wks_server_output_install
	make wks_server_checksum_install
#	make wks_server_cots_install

wks_client_bin_install:
	@if [ ! -d $(EPOCH_BIN) ]; then \
		mkdir $(EPOCH_BIN); \
		chmod g+w $(EPOCH_BIN); \
	else \
		\rm -f $(EPOCH_BIN)/* $(EPOCH_BIN)/.[a-z]*; \
	fi; \
	cd $(EPOCH_BIN);\
	rcp $(EPOCH_WKS_SERVER):$(EPOCH_BIN)/* .

wks_client_install:
	make wks_client_bin_install

	@if [ ! -d $(EPOCH_CONFIG) ]; then \
		mkdir $(EPOCH_CONFIG);\
	fi

	@if [ ! -d $(EPOCH_DATABASE) ]; then\
		mkdir $(EPOCH_DATABASE);\
	fi

	@if [ ! -d $(EPOCH_TOOLKIT) ]; then\
		mkdir $(EPOCH_TOOLKIT);\
	fi

	@umask 7;\
	if [ ! -d $(EPOCH_OUT) ]; then mkdir $(EPOCH_OUT); chmod g+w $(EPOCH_OUT); fi; \
	make_output_dir () { \
	for i do \
		if [ ! -d $(EPOCH_OUT)/$$i ]; then mkdir $(EPOCH_OUT)/$$i; fi; \
	done } ; \
	make_output_dir $(EPOCH_FES) eui vwr; \
	make_channel_dir() { \
	for k do \
		if [ ! -d $$k ]; then mkdir $$k ; fi; \
	done }; \
	make_sat_dir() { \
	for j do \
		mkdir $(EPOCH_OUT)/$(HOSTNAME)/$$j ; \
		cd $(EPOCH_OUT)/$(HOSTNAME)/$$j ; \
		make_channel_dir $(TLM_CHANNELS) ; \
	done }; \
	if [ ! -d $(EPOCH_OUT)/$(HOSTNAME) ]; then \
		mkdir $(EPOCH_OUT)/$(HOSTNAME) ; \
		make_sat_dir $(EPOCH_SATELLITES); \
	fi

fep_server_bin_install:
	@if [ ! -d $(EPOCH_BIN) ]; then \
		mkdir $(EPOCH_BIN); \
		chmod g+w $(EPOCH_BIN); \
	else \
		\rm -f $(EPOCH_BIN)/* $(EPOCH_BIN)/.[a-z]*; \
	fi; \
	cd $(EPOCH_BIN);\
	copy_bin_dir () { \
	for i do \
		if [ $(EPOCH_HOST_MODE) = DEVELOP ]; \
		then \
			ln -s $(EPOCH_OBJ)/$$i . ;\
		else \
			cp $(EPOCH_OBJ)/$$i . ; \
		fi;\
	done } ; \
	copy_bin_dir $(PROJ_FE_BINS)
	cd $(EPOCH_BIN);\
	copy_tlk_dir () { \
	for i do \
		if [ $(EPOCH_HOST_MODE) = DEVELOP ]; \
		then \
			ln -s $(EPOCH_TBIN)/$$i . ;\
		else \
			cp $(EPOCH_TBIN)/$$i . ; \
		fi;\
	done } ; \
	copy_tlk_dir $(PROJ_TLK_FE_BINS)

fep_server_install:
	make fep_server_bin_install
	@if [ -d $(EPOCH_CONFIG) ]; then \
		\rm -f $(EPOCH_CONFIG)/* $(EPOCH_CONFIG)/.[a-zA-Z]*;\
	else \
		mkdir $(EPOCH_CONFIG); \
	fi;\
	cp -r $(EPOCH_PSRC)/CONFIG $(EPOCH_CONFIG)  ;\

	@if [ ! -d $(EPOCH_DATABASE) ]; then mkdir $(EPOCH_DATABASE); fi; \
	cd $(EPOCH_DATABASE);\
	copy_db_dir () { \
	for i do \
		if [ ! -d $(EPOCH_HOME)/$(EPOCH_PROJECT)$(EPOCH_RELEASE)/$$i ]; then \
			mkdir $(EPOCH_HOME)/$(EPOCH_PROJECT)$(EPOCH_RELEASE)/$$i ; \
		fi; \
		cd $(EPOCH_HOME)/$(EPOCH_PROJECT)$(EPOCH_RELEASE)/$$i ; \
		if [ $(EPOCH_HOST_MODE) = SERVER ]; then \
			for j in $(EPOCH_SATELLITES);\
			do (\
				if [ -d $(EPOCH_OBJ)/$$i/$$j ]; then \
					if [ ! -d $(EPOCH_HOME)/$(EPOCH_PROJECT)$(EPOCH_RELEASE)/$$i/$$j ]; then \
						mkdir $(EPOCH_HOME)/$(EPOCH_PROJECT)$(EPOCH_RELEASE)/$$i/$$j; \
					fi; \
					cd $(EPOCH_HOME)/$(EPOCH_PROJECT)$(EPOCH_RELEASE)/$$i/$$j; \
					find $(EPOCH_OBJ)/$$i/$$j \( -type l -o -type f \) -exec cp -fp {} . \; ;\
				fi; \
			); done ; \
			cd $(EPOCH_HOME)/$(EPOCH_PROJECT)$(EPOCH_RELEASE)/$$i;\
			for j in `ls $(EPOCH_OBJ)/$$i`;\
			do (\
				if [ ! -d $(EPOCH_OBJ)/$$i/$$j ]; then \
					cp -fp $(EPOCH_OBJ)/$$i/$$j . ;\
				fi;\
			); done ;\
		else \
			ln -fs $(EPOCH_OBJ)/$$i/* . ; \
		fi ; \
		rm -f Makefile ; \
	done } ; \
#	copy_db_dir $(PROJ_ALL_DB_DIRS)
	copy_db_dir $(PROJ_DB_DIRS)

	@if [ ! -d $(EPOCH_OUT) ]; then \
		mkdir $(EPOCH_OUT);\
		chmod g+w $(EPOCH_OUT);\
		if [ $(EPOCH_HOST_MODE) != DEVELOP ]; \
		then \
			ln -s /archive $(EPOCH_OUT)/$(HOSTNAME);\
		else \
			mkdir $(EPOCH_OUT)/$(HOSTNAME);\
			chmod g+w $(EPOCH_OUT)/$(HOSTNAME);\
		fi;\
		make_channel_dir() { \
		for k do \
			if [ ! -d $$k ]; then \
				mkdir $$k ;\
				chmod g+w $$k ;\
			 fi; \
		done }; \
		make_sat_dir() { \
		for j do \
			if [ ! -d $(EPOCH_OUT)/$(HOSTNAME)/$$j ]; then \
				mkdir $(EPOCH_OUT)/$(HOSTNAME)/$$j ;\
				chmod g+w $(EPOCH_OUT)/$(HOSTNAME)/$$j ;\
			fi;\
			cd $(EPOCH_OUT)/$(HOSTNAME)/$$j ; \
			make_channel_dir $(TLM_CHANNELS) ; \
		done }; \
		make_sat_dir $(EPOCH_SATELLITES) ; \
	fi

fep_client_bin_install:
	@if [ ! -d $(EPOCH_BIN) ]; then \
		mkdir $(EPOCH_BIN); \
		chmod g+w $(EPOCH_BIN); \
	else \
		\rm -f $(EPOCH_BIN)/* $(EPOCH_BIN)/.[a-z]* ; \
	fi; \
	cd $(EPOCH_BIN);\
	rcp $(EPOCH_FEP_SERVER):$(EPOCH_BIN)/* .

fep_client_install:
	make fep_client_bin_install
	@if [ ! -d $(EPOCH_CONFIG) ]; then \
		mkdir $(EPOCH_CONFIG);\
	fi

	@if [ ! -d $(EPOCH_DATABASE) ]; then mkdir $(EPOCH_DATABASE); fi

	@if [ ! -d $(EPOCH_OUT) ]; then \
		mkdir $(EPOCH_OUT);\
		chmod g+w $(EPOCH_OUT);\
		ln -s /archive $(EPOCH_OUT)/$(HOSTNAME);\
		make_channel_dir() { \
		for k do \
			if [ ! -d $$k ]; then \
				mkdir $$k ; \
				chmod g+w $$k ; \
			fi; \
		done }; \
		make_sat_dir() { \
		for j do \
			if [ ! -d $(EPOCH_OUT)/$(HOSTNAME)/$$j ]; then \
				mkdir $(EPOCH_OUT)/$(HOSTNAME)/$$j ; \
				chmod g+w $(EPOCH_OUT)/$(HOSTNAME)/$$j ; \
			fi;\
			cd $(EPOCH_OUT)/$(HOSTNAME)/$$j ; \
			make_channel_dir $(TLM_CHANNELS) ; \
		done }; \
		make_sat_dir $(EPOCH_SATELLITES) ; \
	fi

uninstall:
	@echo ""
	@echo "Removing files from $(EPOCH_INSTALL) installation tree"
	@echo ""
	@\rm -rf $(EPOCH_INSTALL)/BIN
	@\rm -rf $(EPOCH_INSTALL)/CONFIG
	@\rm -rf $(EPOCH_TOOLKIT)

clean:
	@make_clean_in_dir () { \
	for i do \
		echo "\n***** Making clean in $(EPOCH_OBJ)/$$i *****\n" ; \
		cd $(EPOCH_OBJ)/$$i; \
		if [ -f Makefile ]; \
		then \
			make clean; \
		fi \
	done } ; \
	if [ $(EPOCH_HOST) = FEP ]; \
	then \
		make_clean_in_dir $(PROJ_FE_DIRS); \
	elif [ $(EPOCH_HOST) = WKS ]; \
	then \
		make_clean_in_dir $(PROJ_WKS_DIRS); \
	else \
		cd $(EPOCH_OBJ) ; \
		make_clean_in_dir $(DIRLIST); \
	fi
	@make clean_misc

clean_misc:
	find . -name '*%' -exec rm {} \;
	find . -name core -exec rm {} \;


