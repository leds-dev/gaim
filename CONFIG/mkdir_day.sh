if [ $# = 0 ] 
then
  echo "*** usage: mkdir_day <day of year>"
  exit
fi

DOY=`printf "%03d\n" $1`

cd $EPOCH_DATABASE/procs

for SAT in $EPOCH_SATELLITES;\
do ( \
	if [ ! -d $SAT/day$DOY ]
	then
		mkdir $SAT/day$DOY
	fi
	
	if [ ! -d $SAT/day$DOY/frame ]
	then
		mkdir $SAT/day$DOY/frame
	fi
	
	if [ ! -d $SAT/day$DOY/oats ]
	then
		mkdir $SAT/day$DOY/oats
	fi
	
	if [ ! -d $SAT/day$DOY/star ]
	then
		mkdir $SAT/day$DOY/star
	fi
	
	if [ ! -d $SAT/day$DOY/procs ]
	then
		mkdir $SAT/day$DOY/procs
	fi

); done;
