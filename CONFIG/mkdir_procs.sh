cd $EPOCH_DATABASE/procs

for SAT in $EPOCH_SATELLITES;\
do ( \
	if [ ! -d $SAT/cls ]
	then
		mkdir $SAT/cls
	fi
	
	if [ ! -d $SAT/rtcs ]
	then
		mkdir $SAT/rtcs
	fi
	
	if [ ! -d $SAT/frame ]
	then
		mkdir $SAT/frame
	fi
	
	if [ ! -d $SAT/ms ]
	then
		mkdir $SAT/ms
	fi
	
	if [ ! -d $SAT/data ]
	then
		mkdir $SAT/data
	fi
	
	if [ ! -d $SAT/data/output ]
	then
		mkdir $SAT/data/output
	fi
	
); done;
